<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::get('/', function () {
//    return redirect(app()->getLocale());
//});


Auth::routes(['verify' => true]);

Route::get('/', 'Frontend\FrontendController@index')->name('home');

Route::get('/set/currency', 'Controller@setCurrency')->name('setCurrency');
//Route::match(array('GET', 'POST'), '/set-currency', 'Controller@setCurrency')->name('setCurrency');

/* login module routes */

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('userlogin');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('userlogout');

// Registration Routes...

Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset.token');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');

Route::post('email/resend', 'Auth\ResendVerificationController@resend')->name('verification.resend');


//Route::get('/category/{category}', 'Frontend\FrontendController@categoriesPage')->name('categoriesPage');

Route::get('/faq', 'Controller@faq')->name('page_faq');
Route::get('/citieswedeliver', 'Controller@citieswedeliver')->name('page_citieswedeliver');
Route::match(array('GET', 'POST'), '/contact-us', 'HomeController@Contactus')->name('page_conatct');

Route::get('/about-us', 'Controller@about')->name('page_about');
Route::get('/privacy', 'Controller@privacy')->name('page_privacy');
Route::get('/returnpolicy', 'Controller@returnpolicy')->name('page_returnpolicy');
Route::get('/termsofuse', 'Controller@termsofuse')->name('page_termsofuse');

Route::get('/corevalues', 'Controller@corevalues')->name('page_corevalues');
Route::get('/career', 'Controller@career')->name('page_career');
Route::get('/sitemap', 'Controller@sitemap')->name('sitemap');
// end pages
Route::post('/subscription/save', 'Frontend\FrontendController@subscriptionSave')->name('subscriptionSave');

Route::prefix('blog')->group(function () {

    Route::get('/', 'Controller@bloglist')->name('bloglist');
    Route::get('/view/{id}', 'Controller@blogView')->name('blogView');

});

Route::match(array('GET', 'POST'), 'cart/thank-you', 'Controller@cartThankYouPage')->name('cartThankYouPage');
Route::match(array('GET', 'POST'), 'services/thank-you', 'Controller@serviceThankYouPage')->name('serviceThankYouPage');
Route::match(array('GET', 'POST'), 'ammachethivanta/thank-you', 'Controller@ammachethivantaThankYouPage')->name('ammachethivantaThankYouPage');


//Route::prefix('user')->group(function () {
//
//});

Route::group(['prefix' => 'location-{locale}'], function() {

    Route::get('/{category}/{type?}', 'Frontend\FrontendController@categoriesPage')->name('categoriesPage');
    Route::get('/{category}/{foritem}/filter/', 'Frontend\FrontendController@categoriesSplPage')->name('categoriesSplPage');
});


Route::get('/product/{category}/{product}/{sku?}/{otp?}', 'Frontend\FrontendController@productPage')->name('productPage');
//Route::get('/service/{service}', 'Frontend\FrontendController@servicePage')->name('servicePage');

Route::match(array('GET', 'POST'), '/service/{service?}', 'Frontend\FrontendController@servicePage')->name('servicePage');

Route::match(array('GET', 'POST'), '/amma-chethi-vanta/', 'Frontend\FrontendController@ammaChethiVanta')->name('ammaChethiVanta');


Route::get('/cart', 'Frontend\FrontendController@cartPage')->name('cartPage');
Route::get('/cart/address', 'Frontend\UserController@cartAdressPage')->name('cartAdressPage');

// Cart operation

// Add to Cart
Route::match(array('GET', 'POST'), '/ajax/addToCart', 'Frontend\FrontendController@addToCart')->name('addToCart');
// Add to Cart for only one product
Route::match(array('GET', 'POST'), '/product/addTocart', 'Frontend\FrontendController@buyProduct')->name('buyProduct');


Route::match(array('GET', 'POST'), '/checkout', 'Frontend\UserController@checkoutPage')->name('checkoutPage');

Route::get('/payment/success', 'Frontend\UserController@paymentSuccessPage')->name('paymentSuccessPage');


// Send Enquiry
Route::match(array('GET', 'POST'), '/product/send-enquiry', 'Frontend\FrontendController@productSendEnquiry')->name('productSendEnquiry');
Route::match(array('GET', 'POST'), '/product/send-enquiry/verify', 'Frontend\FrontendController@productSendEnquiryVerify')->name('productSendEnquiryVerify');
// End Send Enquiry

Route::prefix('cart')->group(function () {

});


Route::prefix('user')->group(function () {

    // Add to Cart
    Route::match(array('GET', 'POST'), '/ajax/checkCouponcodes', 'Frontend\UserController@checkCouponcodes')->name('checkCouponcodes');


    // Add to wish list
    Route::match(array('GET', 'POST'), '/profile', 'Frontend\UserController@index')->name('userprofile');
    // Delete User Image

    Route::match(array('GET', 'POST'), '/manage/password', 'Frontend\UserController@changePassword')->name('userChangePassword');
    Route::match(array('GET', 'POST'), '/orders/{status?}', 'Frontend\UserController@orders')->name('orders');
    Route::match(array('GET', 'POST'), '/order/details/{order}', 'Frontend\UserController@orderDetails')->name('orderDetails');
//    Route::match(array('GET', 'POST'), '/order/invoice/{order}', 'Frontend\UserController@orderInvoice')->name('orderInvoice');
    Route::match(array('GET', 'POST'), '/unprocessed/orders', 'Frontend\UserController@unProcessedOrders')->name('unProcessedOrders');

    Route::match(array('GET', 'POST'), '/addres-book', 'Frontend\UserController@userAddressBook')->name('userAddressBook');
//    Route::match(array('GET', 'POST'), '/addres-book/remove', 'Frontend\UserController@userAddressBookRemove')->name('userAddressBookRemove');

    Route::match(array('GET', 'POST'), '/manage/addres-book', 'Frontend\UserController@userAddAddressBook')->name('userAddAddressBook');
    Route::match(array('GET', 'POST'), '/wish-list', 'Frontend\UserController@wishList')->name('wishList');

    Route::match(array('GET', 'POST'), '/payment/confirmation', 'Frontend\UserController@paymentConfirmation')->name('paymentConfirmation');
    Route::match(array('GET', 'POST'), '/cart/orders/save', 'Frontend\UserController@saveOrders')->name('saveOrders');

//    Route::match(array('GET', 'POST'), '/service/payment/paypal', 'Frontend\PaymentController@payWithpaypal')->name('paywithpaypal');
//    Route::match(array('GET', 'POST'), '/service/payment/status/save/', 'Frontend\PaymentController@getPaymentStatus')->name('savePaymentStatus');


    Route::prefix('payment')->group(function () {

        Route::match(array('GET', 'POST'), 'services/pay/{order}', 'PaymentController@servicePayWithpaypal')->name('payWithpaypal');
        Route::match(array('GET', 'POST'), '/status', 'PaymentController@getPaymentStatus')->name('getPaymentStatus');


    });


    Route::match(array('GET', 'POST'), '/addres-book/default-addres', 'Frontend\UserController@makeDefaultAddress')->name('makeDefaultAddress');
    Route::match(array('GET', 'POST'), '/service/invoices/{action?}/{id?}', 'Frontend\UserController@ServiceInvoices')->name('ServiceInvoices');
    Route::match(array('GET', 'POST'), '/service/invoice/{id?}', 'Frontend\UserController@showServiceInvoice')->name('showServiceInvoice');

    Route::match(array('GET', 'POST'), '/occassion-reminder', 'Frontend\UserController@occassionReminder')->name('occassionReminder');

    Route::match(array('GET', 'POST'), '/cart/service/paypal/{ref_id}', 'PaymentController@payWithpaypalCart')->name('payWithpaypalCart');
    Route::match(array('GET', 'POST'), '/cart/payment/status/save/', 'PaymentController@getPaymentCartStatus')->name('getPaymentCartStatus');

    Route::match(array('GET', 'POST'), '/order/invoice/{id?}', 'Frontend\UserController@showOrderInvoice')->name('showOrderInvoice');


//    Route::match(array('GET', 'POST'), 'cart/thank-you/{id?}', 'Frontend\UserController@cartThankYouPage')->name('cartThankYouPage');
//    Route::match(array('GET', 'POST'), 'services/thank-you/{id?}', 'Frontend\UserController@serviceThankYouPage')->name('serviceThankYouPage');
//    Route::match(array('GET', 'POST'), 'ammachethivanta/thank-you/{id?}', 'Frontend\UserController@ammachethivantaThankYouPage')->name('ammachethivantaThankYouPage');


    Route::prefix('cart')->group(function () {

    });


    Route::prefix('ammachethivanta')->group(function () {
        Route::match(array('GET', 'POST'), '/address/page', 'Frontend\UserController@ammachethivantaAddressPage')->name('ammachethivanta_address_page');
        Route::match(array('GET', 'POST'), '/confirm', 'Frontend\UserController@acvorderConfirmation')->name('acvorderConfirmation');
        Route::match(array('GET', 'POST'), '/amma/orders/save', 'Frontend\UserController@ammachethivantaOrdersSave')->name('ammachethivantaOrdersSave');
        Route::match(array('GET', 'POST'), '/payment/{id}', 'PaymentController@payWithPaypalAmmachethiVanta')->name('payWithPaypalAmmachethiVanta');
        Route::match(array('GET', 'POST'), '/payment/status/{id}', 'PaymentController@payWithPaypalAmmachethiVantaStatus')->name('payWithPaypalAmmachethiVantaStatus');
        Route::match(array('GET', 'POST'), '/orders', 'Frontend\UserController@AmmachethiVantaOrders')->name('AmmachethiVantaOrders');
        Route::match(array('GET', 'POST'), '/orders/view/{id}', 'Frontend\UserController@AmmachethiVantaOrderView')->name('AmmachethiVantaOrderView');
        Route::match(array('GET', 'POST'), '/order/view/invoice/{id}', 'Frontend\UserController@AmmachethiVantaInvoice')->name('AmmachethiVantaInvoice');
        Route::match(array('GET', 'POST'), '/saveOrders/intoSessions', 'Frontend\UserController@saveRecordsIntoSessions')->name('saveRecordsIntoSessions');
    });


    Route::match(array('GET', 'POST'), '/rating/add', 'Frontend\FrontendController@addNewRating')->name('addNewRating');

});


Route::prefix('store')->group(function () {


    Route::get('/', 'Store\PreVendorsController@index')->name('vendor_home');
    Route::get('/register-seller', 'Store\PreVendorsController@registerSeller')->name('register_seller');
    Route::match(array('GET', 'POST'), '/register-details', 'Store\PreVendorsController@registerSellerDetails')->name('registerSellerDetails');

    Route::get('/faq', 'Store\PreVendorsController@sellerFaq')->name('seller_faq');
    Route::get('/pricing', 'Store\PreVendorsController@sellerPricing')->name('seller_pricing');
    Route::get('/benefits', 'Store\PreVendorsController@sellerBenefits')->name('seller_benefits');


    /* login module routes */
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('vendorlogin');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/logout', 'Auth\LoginController@logout')->name('vendorlogout');

    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('vendorRegister');
    Route::post('register', 'Auth\RegisterController@register');


    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('vendor.password.reset');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('vendor.password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('vendor.password.reset.token');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('vendor.password.update');

    Route::get('email/verify', 'Auth\VerificationController@show')->name('vendor.verification.notice');
    Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('vendor.verification.verify');
    Route::post('email/resend', 'Auth\ResendVerificationController@resend')->name('vendor.verification.resend');


    Route::match(array('GET', 'POST'), '/seller-pre-dashbord', 'Store\VendorsController@sellerPreDashbord')->name('sellerPreDashbord');
    Route::match(array('GET', 'POST'), '/seller-dashbord', 'Store\VendorsController@sellerDashbord')->name('sellerDashbord');


    Route::match(array('GET', 'POST'), '/seller-listings', 'Store\VendorsController@sellerListings')->name('seller_listings');
    Route::match(array('GET', 'POST'), '/seller-listings/view/{id?}', 'Store\VendorsController@sellerListingsView')->name('seller_listingsView');
    Route::match(array('GET', 'POST'), '/seller-listings/manage/{id?}', 'Store\VendorsController@sellerNewListings')->name('seller_new_listings');


    Route::get('/orders/new-orders', 'Store\VendorsController@sellerOrdersNewList')->name('sellerOrdersNewList');
    Route::get('/orders/completed', 'Store\VendorsController@sellerOrdersCompletedList')->name('sellerOrdersCompletedList');
    Route::get('/orders/view/new/{id}', 'Store\VendorsController@sellerOrdersNewListView')->name('sellerOrdersNewListView');
    Route::get('/orders/view/completed/{id}', 'Store\VendorsController@sellerOrdersCompletedListView')->name('sellerOrdersCompletedListView');

    Route::match(array('GET', 'POST'), '/orders/status/{id}', 'Store\VendorsController@vendorStatusUpdateByVendor')->name('vendorStatusUpdateByVendor');


    Route::get('/payments/overview', 'Store\VendorsController@paymentsOverview')->name('paymentsOverview');
    Route::get('/payments/invoice', 'Store\VendorsController@paymentsInvoice')->name('paymentsInvoice');
    Route::get('/payments/invoice-view', 'Store\VendorsController@paymentsInvoiceView')->name('paymentsInvoiceView');

    Route::match(array('GET', 'POST'), '/profile', 'Store\VendorsController@profileAccount')->name('profileAccount');
    Route::match(array('GET', 'POST'), '/business', 'Store\VendorsController@BusinessDetails')->name('BusinessDetails');
    Route::match(array('GET', 'POST'), '/settings', 'Store\VendorsController@vendorSettings')->name('vendorSettings');


//        Route::match(array('GET', 'POST'), '/change/password', 'Store\VendorsController@changePassword')->name('vendorChangePassword');


    /* Products module routes */
//    Route::match(array('GET', 'POST'), '/seller-listings', 'Store\ProductsController@sellerListings')->name('sellerListings');
//    Route::match(array('GET', 'POST'), '/seller-listings/manage/{id?}', 'Store\ProductsController@addNewProducts')->name('addVendorNewProducts');

//    Route::post('ajax/checkProductAlias', 'Store\ProductsController@checkProductAlias')->name('checkProductAlias');


});


Route::prefix('admin')->group(function () {

    Route::match(array('GET', 'POST'), '/settings', 'Admin\SettingsController@index')->name('admin_settings');

    Route::match(array('GET', 'POST'), '/product/enquiries', 'Admin\AdminController@product_enquiries')->name('product_enquiries');
    Route::post('/enquiry/delete', 'Admin\AdminController@enquiryDelete')->name('enquiryDelete');

    /* login module routes */
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('adminLogin');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

    /* Dashboard routes */
    Route::get('/dashboard', 'Admin\AdminController@index')->name('dashboard');

    /* Change Password */
    Route::match(array('GET', 'POST'), '/change/password', 'Admin\AdminController@changePassword')->name('changePassword');

    /* update Profile */
    Route::match(array('GET', 'POST'), '/update/profile', 'Admin\AdminController@profile')->name('profile');


    /* Countries module routes */
    Route::match(array('GET', 'POST'), '/countries', 'Admin\CountriesController@index')->name('countries');
    Route::match(array('GET', 'POST'), '/countries/manage/{id?}', 'Admin\CountriesController@addNewCountries')->name('addNewCountries');

    /* Coupons module routes */
    Route::match(array('GET', 'POST'), '/coupons', 'Admin\CouponsController@index')->name('coupons');
    Route::match(array('GET', 'POST'), '/coupons/manage/{id?}', 'Admin\CouponsController@addNewCoupons')->name('addNewCoupons');


    Route::match(array('GET', 'POST'), '/states', 'Admin\CountriesController@States')->name('States');
    Route::match(array('GET', 'POST'), '/states/manage', 'Admin\CountriesController@addNewStates')->name('addNewState');
    Route::match(array('GET', 'POST'), '/states/edit', 'Admin\CountriesController@editStates')->name('editStates');

    Route::match(array('GET', 'POST'), '/cities', 'Admin\CountriesController@Cities')->name('Cities');
    Route::match(array('GET', 'POST'), '/city/manage', 'Admin\CountriesController@addNewCity')->name('addNewCity');
    Route::match(array('GET', 'POST'), '/city/edit', 'Admin\CountriesController@editCity')->name('editCity');


    /* Amma chethi vanta module routes */
    Route::match(array('GET', 'POST'), '/ammachethivanta', 'Admin\AmmachethivantaController@index')->name('ammachethivanta');
    Route::match(array('GET', 'POST'), '/ammachethivanta/manage/{id?}', 'Admin\AmmachethivantaController@addNewRecords')->name('addNewRecords');
    Route::post('ajax/deleteimage', 'Admin\AmmachethivantaController@deleteimage')->name('deleteimage');
    Route::match(array('GET', 'POST'), '/sku/options/remove', 'Admin\AmmachethivantaController@removeskuOptions')->name('removeskuOptions');
    Route::match(array('GET', 'POST'),'ammachethivanta/orders', 'Admin\AmmachethivantaController@orders')->name('ammachethivantaorders');
    Route::get('ammachethivanta/orderDetails/{id}', 'Admin\AmmachethivantaController@orders')->name('ammachethivantaOrdersDetails');
    Route::post('ammachethivanta/orderDetails/change/{id}', 'Admin\AmmachethivantaController@changeOrderStatus')->name('ammachethivanta_changeOrderStatus');


    /* Banners module routes */
    Route::match(array('GET', 'POST'), '/service/banners', 'Admin\ServiceBannersController@index')->name('serviceBanners');
    Route::match(array('GET', 'POST'), '/service/banners/manage/{id?}', 'Admin\ServiceBannersController@addNewBanners')->name('addNewServiceBanners');
    Route::post('ajax/deleteservicebannerimage', 'Admin\ServiceBannersController@deleteBannerimage')->name('deleteServiceBannerimage');

    /* Banners module routes */
    Route::match(array('GET', 'POST'), '/banners', 'Admin\BannersController@index')->name('banners');
    Route::match(array('GET', 'POST'), '/banners/manage/{id?}', 'Admin\BannersController@addNewBanners')->name('addNewBanners');
    Route::post('ajax/deletebannerimage', 'Admin\BannersController@deleteBannerimage')->name('deleteBannerimage');

    Route::post('ajax/deletebgbannerimage', 'Admin\BannersController@deleteBgBannerimage')->name('deleteBgBannerimage');

    /* Blogs module routes */
    Route::match(array('GET', 'POST'), '/blogs', 'Admin\BlogsController@index')->name('blogs');
    Route::match(array('GET', 'POST'), '/blogs/manage/{id?}', 'Admin\BlogsController@addNewBlogs')->name('addNewBlogs');
    Route::post('ajax/deleteblogimage', 'Admin\BlogsController@deleteBlogimage')->name('deleteBlogimage');

    /* Categories module routes */
    Route::match(array('GET', 'POST'), '/categories', 'Admin\CategoriesController@index')->name('categories');
    Route::match(array('GET', 'POST'), '/categories/manage/{id?}', 'Admin\CategoriesController@addNewCategories')->name('addNewCategories');
    Route::post('ajax/deletecategoryimage', 'Admin\CategoriesController@deleteCategoryimage')->name('deleteCategoryimage');
    Route::post('ajax/checkNewCategoryAlias', 'Admin\CategoriesController@checkNewCategoryAlias')->name('checkNewCategoryAlias');


    Route::post('ajax/checkCouponcode', 'Admin\CouponsController@checkCouponcode')->name('checkCouponcode');


    /* Products module routes */
    Route::match(array('GET', 'POST'), '/products', 'Admin\ProductsController@index')->name('admin_products');
    Route::match(array('GET', 'POST'), '/products/manage/{id?}', 'Admin\ProductsController@addNewProducts')->name('addNewProducts');
    Route::match(array('GET', 'POST'), '/products/view/{id?}', 'Admin\ProductsController@AdminProductView')->name('AdminProductView');
    Route::post('ajax/deleteproductimage', 'Admin\ProductsController@deleteproductimage')->name('deleteproductimage');

    Route::post('ajax/checkProductAlias', 'Admin\ProductsController@checkProductAlias')->name('checkProductAlias');

    Route::match(array('GET', 'POST'), '/products/extraInfo/{id?}', 'Admin\ProductsController@productExtraInfo')->name('productExtraInfo');

    Route::match(array('GET', 'POST'), '/products/import/{id?}', 'Admin\ProductsController@sellerImportListings')->name('sellerImportListings');


    Route::match(array('GET', 'POST'), '/products/extraInfo/manage/{id?}/{extraid?}', 'Admin\ProductsController@addProductExtraInfo')->name('addProductExtraInfo');

    Route::post('ajax/deleteproductextraimage', 'Admin\ProductsController@deleteProductExtraimage')->name('deleteProductExtraimage');


    //product approve
    Route::post('/product/approve', 'Admin\ProductsController@ProductApprove')->name('ProductApprove');
    Route::post('/vendor/approve', 'Admin\ProductsController@VendorVerify')->name('VendorVerify');


    /* Users module routes */
    Route::match(array('GET', 'POST'), '/users', 'Admin\UsersController@index')->name('users');
    /* Users Address module routes */

    Route::match(array('GET', 'POST'), '/user/Address/{id}', 'Admin\UsersController@userAddress')->name('userAddress');
    /* Users Orders module routes */
    Route::match(array('GET', 'POST'), '/user/orders/{id}', 'Admin\UsersController@userOrders')->name('userAdminOrders');

    Route::match(array('GET', 'POST'), '/user/ordersDetails/{id}', 'Admin\UsersController@userOrdersDetails')->name('userOrdersDetails');
    Route::match(array('GET', 'POST'), '/user/ordersStatus/{id}', 'Admin\UsersController@orderStatusUpdate')->name('orderStatusUpdate');
    /* Users Orders module routes */
    Route::match(array('GET', 'POST'), '/user/wishlist/{id}', 'Admin\UsersController@userWishlist')->name('userWishlist');
    /* Orders module routes */
    Route::match(array('GET', 'POST'), '/orders', 'Admin\UsersController@orders')->name('adminorders');
    Route::match(array('GET', 'POST'), '/reports', 'Admin\UsersController@reports')->name('adminreports');
    Route::match(array('GET', 'POST'), '/reviews', 'Admin\UsersController@Reviews')->name('Reviews');
    /* Product images routes */
    Route::match(array('GET', 'POST'), '/product/images/{id?}', 'Admin\ProductsController@addProductImages')->name('addProductImages');

//    Route::get('types/ajax/{id}','Admin\ProductsController@showCategoryTypes')->name('showCategoryTypes');;


    Route::prefix('service')->group(function () {

        Route::match(array('GET', 'POST'), '/page/{service}/{id?}', 'Admin\ServicesController@Service')->name('admin_service');
        Route::match(array('GET', 'POST'), '/view/{service}/{id?}', 'Admin\ServicesController@ServiceView')->name('admin_service_view');
        Route::match(array('GET', 'POST'), '/invoice/{service}/{id?}', 'Admin\ServicesController@invoice')->name('admin_service_invoice');



        Route::match(array('GET', 'POST'), '/{service}/documents/{id?}', 'Admin\ServicesController@ServiceDocuments')->name('admin_service_documents');
        Route::match(array('GET', 'POST'), '/documentsemailsend/{id?}', 'Admin\ServicesController@ServiceDocumentsEmailSend')->name('admin_service_documents_send');
        Route::post('/servicedoc/delete', 'Admin\ServicesController@ServiceDocumentDelete')->name('ServiceDocumentDelete');

//        Route::match(array('GET', 'POST'), '/{service}/{id?}', 'Admin\ServicesController@Medical')->name('Medical');
//        Route::match(array('GET', 'POST'), '/summerEnrichment/{id?}', 'Admin\ServicesController@summerEnrichment')->name('summerEnrichment');
//        Route::match(array('GET', 'POST'), '/tutor/{id?}', 'Admin\ServicesController@Tutor')->name('Tutor');
//        Route::match(array('GET', 'POST'), '/property/{id?}', 'Admin\ServicesController@Property')->name('Property');
//        Route::match(array('GET', 'POST'), '/visa/{id?}', 'Admin\ServicesController@Visa')->name('Visa');
//        Route::match(array('GET', 'POST'), '/insurance/{id?}', 'Admin\ServicesController@Insurance')->name('Insurance');
//        Route::match(array('GET', 'POST'), '/realEstate/{id?}', 'Admin\ServicesController@realEstate')->name('realEstate');
//        Route::match(array('GET', 'POST'), '/webDevelopement/{id?}', 'Admin\ServicesController@webDevelopement')->name('webDevelopement');


        Route::post('/invoice/ganarate', 'Admin\ServicesController@adminServiceCreateInvoice')->name('adminServiceCreateInvoice');
        Route::post('/action/delete', 'Admin\ServicesController@ServiceDelete')->name('ServiceDelete');

        Route::match(array('GET', 'POST'), '/show/invoice/{id?}', 'Admin\ServicesController@showInvoice')->name('showInvoice');
        Route::match(array('GET', 'POST'), '/multi/invoice/{id?}', 'Admin\ServicesController@ServiceInvoice')->name('ServiceInvoice');

    });


    Route::match(array('GET', 'POST'), '/store', 'Admin\AdminController@vendors')->name('vendorslist');
    Route::post('/store/delete', 'Admin\AdminController@storeDelete')->name('storeDelete');
    Route::match(array('GET', 'POST'), '/store/account/{id}', 'Admin\AdminController@AccountDetails')->name('admin_vendor_account_details');
    Route::match(array('GET', 'POST'), '/store/business/{id}', 'Admin\AdminController@BusinessDetails')->name('BusinessDetailsInfo');
    Route::match(array('GET', 'POST'), '/store/paymentinfo/{id}', 'Admin\AdminController@paymentInfo')->name('paymentinfo');
    Route::match(array('GET', 'POST'), '/store/products/{id}', 'Admin\AdminController@VendorProducts')->name('VendorProducts');

    Route::match(array('GET', 'POST'), '/store/orders/{id}', 'Admin\UsersController@vendorAdminOrders')->name('vendorAdminOrders');
    Route::match(array('GET', 'POST'), '/store/ordersDetails/{id}', 'Admin\UsersController@vendorOrdersDetails')->name('vendorOrdersDetails');
    Route::match(array('GET', 'POST'), '/store/ordersStatus/{id}', 'Admin\UsersController@vendorStatusUpdate')->name('vendorStatusUpdate');


    Route::post('/accountDetails/submit', 'Admin\AdminController@AccountDetailsApprove')->name('AccountDetailsApprove');
    Route::post('/businessDetails/submit', 'Admin\AdminController@BusinessDetailsApprove')->name('BusinessDetailsApprove');


    Route::get('subscriptions', 'Admin\AdminController@subscriptions')->name('subscriptions');


});




Route::prefix('ajax')->group(function () {

    Route::post('deleteuserimage', 'frontend\UserController@deleteUserimage')->name('deleteUserimage');

    Route::post('ajax/checkProductAlias', 'AjaxController@checkProductAlias')->name('checkProductAlias');

    Route::match(array('GET', 'POST'), 'get-types', 'AjaxController@showCategoryTypes')->name('showCategoryTypes');

    Route::match(array('GET', 'POST'), 'get-states', 'AjaxController@showStates')->name('showStates');
    Route::match(array('GET', 'POST'), 'get-cities', 'AjaxController@showCities')->name('showCities');

    Route::match(array('GET', 'POST'), '/seller-listings/options/remove', 'AjaxController@removeProductOptions')->name('seller_remove_options');


    // update From Cart
    Route::match(array('GET', 'POST'), '/ajax/update/item/', 'Frontend\FrontendController@updateItemFromCart')->name('updateItemFromCart');
// Remove From Cart
    Route::match(array('GET', 'POST'), '/ajax/remove/item/', 'Frontend\FrontendController@removeItemFromCart')->name('removeItemFromCart');
//Add to wishlist
    Route::match(array('GET', 'POST'), '/ajax/addToWishlist', 'Frontend\UserController@addToWishList')->name('addToWishList');

    Route::match(array('GET', 'POST'), '/api/ajax/editor/imageupload', 'Controller@editorimageupload')->name('editorImageupload');
//Ajax
    Route::match(array('GET', 'POST'), 'ajax/product/image/remove', 'AjaxController@productImageRemove')->name('VendordeleteProductimage');

    Route::match(array('GET', 'POST'), 'ajax/search/products', 'AjaxController@search')->name('searchProductInfo');


    Route::match(array('GET', 'POST'), '/getammachethivantaoptions/', 'AjaxController@getammachethivantaoptions')->name('getammachethivantaoptions');

    Route::match(array('GET', 'POST'), '/curencyConvert', 'AjaxController@ajaxCurencyConvert')->name('ajaxCurencyConvert');




    Route::get('ammchathivanta_wightbased_price','AjaxController@ajax_ammchathivanta_wightbased_price')->name('ajax_ammchathivanta_wightbased_price');






});





//Route::get('/home', 'HomeController@index')->name('home');
