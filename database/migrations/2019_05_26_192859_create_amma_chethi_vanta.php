<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmmaChethiVanta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ammachethivanta', function (Blueprint $table) {
            $table->bigIncrements('ac_id');
            $table->string('ac_image')->nullable();
            $table->string('ac_name')->nullable();
            $table->longText('ac_description')->nullable();
            $table->integer('ac_weight')->default('1');
            $table->string('ac_status')->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ammachethivanta');
    }
}
