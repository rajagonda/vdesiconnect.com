<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSkusColumsToOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            //
            $table->string('oitem_item_sku_id')->nullable();
            $table->string('oitem_item_sku_vendor_price')->nullable();
            $table->string('oitem_item_sku_vdc_commission_type')->nullable();
            $table->string('oitem_item_sku_vdc_commission_value')->nullable();
            $table->string('oitem_item_sku_vdc_final_price')->nullable();
            $table->string('oitem_item_sku_store_discount_type')->nullable();
            $table->string('oitem_item_sku_store_discount_value')->nullable();
            $table->string('oitem_item_sku_store_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function (Blueprint $table) {
            //
            $table->dropColumn('oitem_item_sku_id');
            $table->dropColumn('oitem_item_sku_vendor_price');
            $table->dropColumn('oitem_item_sku_vdc_commission_type');
            $table->dropColumn('oitem_item_sku_vdc_commission_value');
            $table->dropColumn('oitem_item_sku_vdc_final_price');
            $table->dropColumn('oitem_item_sku_store_discount_type');
            $table->dropColumn('oitem_item_sku_store_discount_value');
            $table->dropColumn('oitem_item_sku_store_price');
        });
    }
}
