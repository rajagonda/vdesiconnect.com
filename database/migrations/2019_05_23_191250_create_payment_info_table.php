<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_info', function (Blueprint $table) {
            $table->bigIncrements('pi_id');
            $table->integer('pi_vendor_id')->nullable();
            $table->float('pi_current_balance')->nullable();
            $table->float('pi_paying_amount')->nullable();
            $table->float('pi_balance_amount')->nullable();
            $table->date('pi_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_info');
    }
}
