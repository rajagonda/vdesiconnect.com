<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('s_id');
            $table->string('site_logo')->nullable();
            $table->string('vendor_logo')->nullable();
            $table->string('admin_logo')->nullable();
            $table->string('admin_email')->nullable();
            $table->string('admin_name')->nullable();
            $table->string('address')->nullable();
            $table->string('tolfree_email')->nullable();
            $table->string('social_fb')->nullable();
            $table->string('social_tw')->nullable();
            $table->string('social_in')->nullable();
            $table->string('currency_usd_to_inr')->nullable();
            $table->string('ammachethi_vanta_price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
