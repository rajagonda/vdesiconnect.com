<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartCouponsUsageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_coupons_usage', function (Blueprint $table) {
            $table->bigIncrements('cu_id');
            $table->integer('cu_user_id');
            $table->string('cu_coupan_code');
            $table->string('cu_order_id')->nullable();
//            $table->bigIncrements('cu_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_coupons_usage');
    }
}
