<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSkus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_skus', function (Blueprint $table) {
            $table->bigIncrements('sku_id');
            $table->integer('sku_product_id')->nullable();
            $table->string('sku_no')->nullable();
            $table->string('sku_type')->comment('like weight,color,flowers')->nullable();
            $table->string('sku_option')->comment('like qty ex: 2kg,3kg, 5flowers, 5ltrs')->nullable();
            $table->decimal('sku_vendor_price')->comment('like vendor Price ')->nullable();

            $table->string('sku_vdc_commission_type')->comment('like fixed')->nullable();
            $table->decimal('sku_vdc_commission_value')->comment('like Price')->nullable();
            $table->decimal('sku_vdc_final_price')->comment('like Price')->nullable();

            $table->string('sku_store_discount_type')->comment('like fixed')->nullable();
            $table->decimal('sku_store_discount_value')->comment('like Price')->nullable();

            $table->decimal('sku_store_price')->comment('like Price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_skus');
    }
}
