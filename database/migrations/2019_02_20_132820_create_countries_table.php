<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('country_id');
            $table->string('country_name')->nullable();
            $table->string('country_code')->nullable();
            $table->string('country_phone_prefix')->nullable();
            $table->string('country_currency')->nullable();
            $table->string('country_language')->nullable();
            $table->text('country_weight_description')->nullable();
            $table->string('country_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
