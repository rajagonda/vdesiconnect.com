<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('category_id');
            $table->string('category_name');
            $table->string('category_alias');
            $table->string('category_parent_id')->default(0);
            $table->integer('category_shipped')->default(0);
            $table->string('category_options')->comment('type,others')->nullable();
            $table->string('category_image')->nullable();
            $table->string('category_order')->default(0);
            $table->longText('category_meta_title')->nullable();
            $table->longText('category_meta_keywords')->nullable();
            $table->longText('category_meta_description')->nullable();
            $table->string('category_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
