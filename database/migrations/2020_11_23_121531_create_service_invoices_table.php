<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('service_id')->nullable();
            $table->integer('s_user_id')->nullable();
            $table->string('s_duration')->nullable();
            $table->date('s_duedate')->nullable();
            $table->string('s_no_of_days')->nullable();
            $table->string('s_unit_price')->nullable();
            $table->string('s_unit_type')->nullable();
            $table->string('s_sub_total')->nullable();
            $table->string('s_tax')->nullable();
            $table->string('s_total')->nullable();
            $table->string('s_description')->nullable();
            $table->string('s_status')->nullable()->comment('new,pending,paid');
            $table->string('s_payment_invoice_num')->nullable();
            $table->string('s_payment_transaction_id')->nullable();
            $table->longText('s_payment_payer')->nullable();
            $table->longText('s_payment_transactions')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_invoices');
    }
}
