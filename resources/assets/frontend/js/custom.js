require('../../../js/app');
// require('./bootstrap.bundle.min');
require('./easyresponsivetabs');
require('./simplegallery.min');
require('./responcive_table');
// require('./accordian.min');
// require('./../modules/contact');

$(document).ready(function(){
    $(".usernav-icon").click(function(){
        $("#userNav-mobile").slideToggle();
    });
});


    
    //scroll top when click on footer upperdirection arrow
    $(function() {

        //add class to onscroll  to header to fix top
        $(window).scroll(function() {
            if ($(this).scrollTop() > 50) {
                $(".fixed-top").addClass("fixed-theme");
            } else {
                $(".fixed-top").removeClass("fixed-theme");
            }
        });

        //add class to body
        $(window).scroll(function(){
            if($(this).scrollTop() > 50 ){
                $("body").addClass("mbody");
            } else {
                $("body").removeClass("mbody");
            }
        });

        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('#movetop').fadeIn('fast');
            } else {
                $('#movetop').fadeOut('fast');
            }
        });
        $('#movetop').on('click', function() {
            $("html").animate({
                scrollTop: 0
            }, 500);
            return false;
        });

        $('[data-toggle="popover"]').popover({
            html: true,
            content: function() {
                return $('#popover-content').html();
            }
        });


        $('#subscriptionForm').validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            // errorPlacement: function (error, e) {
            //     e.parents('.row').append(error);
            // },
            // highlight: function (e) {
            //     // $(e).closest('.row').removeClass('has-success has-error').addClass('has-error');
            //     $(e).closest('.text-danger').remove();
            // },
            // success: function (e) {
            //     // You can use the following if you would like to highlight with green color the input after successful validation!
            //     e.closest('.row').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            //     e.closest('.text-danger').remove();
            // },
            rules: {
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                email: {
                    // required: 'Enter email',
                    email: 'Enter a valid email'
                }
            },
            submitHandler: function (form) {
                $('.subscriptionMessage').html('');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '/subscription/save',
                    type: 'POST',
                    data: $(form).serialize(),
                    success: function (response) {
                        if (response!= 0) {
                            $('.subscriptionMessage').html('<div class="bg-success">Thank you for your participation</div>');
                        } else {
                            $('.subscriptionMessage').html('<div class="bg-danger">This email already subscribed...</div>');
                        }
                        $('#subscriptionForm')[0].reset();
                    }
                });

                return false; // required to block normal submit since you used ajax
            }
        });

    });






//product detail gallery thumbnail plugin
// $(function(){
//
//
//     //input file
//     document.querySelector("html").classList.add('js');
//     var fileInput = document.querySelector(".input-file"),
//         button = document.querySelector(".input-file-trigger"),
//         the_return = document.querySelector(".file-return");
//     button.addEventListener("keydown", function(event) {
//         if (event.keyCode == 13 || event.keyCode == 32) {
//             fileInput.focus();
//         }
//     });
//     button.addEventListener("click", function(event) {
//         fileInput.focus();
//         return false;
//     });
//     fileInput.addEventListener("change", function(event) {
//         the_return.innerHTML = this.value;
//     });
//
// });



//accordian
// Hiding the panel content. If JS is inactive, content will be displayed
$( '.panel-content' ).hide();

// Preparing the DOM

// -- Update the markup of accordion container
$( '.accordion' ).attr({
  role: 'tablist',
  multiselectable: 'true'
 });

// -- Adding ID, aria-labelled-by, role and aria-labelledby attributes to panel content
$( '.panel-content' ).attr( 'id', function( IDcount ) {
  return 'panel-' + IDcount;
});
$( '.panel-content' ).attr( 'aria-labelledby', function( IDcount ) {
  return 'control-panel-' + IDcount;
});
$( '.panel-content' ).attr( 'aria-hidden' , 'true' );
// ---- Only for accordion, add role tabpanel
$( '.accordion .panel-content' ).attr( 'role' , 'tabpanel' );

// -- Wrapping panel title content with a <a href="">
$( '.panel-title' ).each(function(i){

  // ---- Need to identify the target, easy it's the immediate brother
  $target = $(this).next( '.panel-content' )[0].id;

  // ---- Creating the link with aria and link it to the panel content
  $link = $( '<a>', {
    'href': '#' + $target,
    'aria-expanded': 'false',
    'aria-controls': $target,
    'id' : 'control-' + $target
  });

  // ---- Output the link
  $(this).wrapInner($link);

});

// Optional : include an icon. Better in JS because without JS it have non-sense.
$( '.panel-title a' ).append('<span class="icon">+</span>');

// Now we can play with it
$( '.panel-title a' ).click(function() {

  if ($(this).attr( 'aria-expanded' ) == 'false'){ //If aria expanded is false then it's not opened and we want it opened !

    // -- Only for accordion effect (2 options) : comment or uncomment the one you want

    // ---- Option 1 : close only opened panel in the same accordion
    //      search through the current Accordion container for opened panel and close it, remove class and change aria expanded value
    $(this).parents( '.accordion' ).find( '[aria-expanded=true]' ).attr( 'aria-expanded' , false ).removeClass( 'active' ).parent().next( '.panel-content' ).slideUp(200).attr( 'aria-hidden' , 'true');

    // Option 2 : close all opened panels in all accordion container
    //$('.accordion .panel-title > a').attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200);

    // Finally we open the panel, set class active for styling purpos on a and aria-expanded to "true"
    $(this).attr( 'aria-expanded' , true ).addClass( 'active' ).parent().next( '.panel-content' ).slideDown(200).attr( 'aria-hidden' , 'false');

  } else { // The current panel is opened and we want to close it

    $(this).attr( 'aria-expanded' , false ).removeClass( 'active' ).parent().next( '.panel-content' ).slideUp(200).attr( 'aria-hidden' , 'true');;

  }
  // No Boing Boing
  return false;
});


/*
//input type file
$('.new_Btn').bind("click" , function () {
    $('#html_btn').click();
});*/
