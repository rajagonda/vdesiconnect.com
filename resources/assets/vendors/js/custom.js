require('../../../js/app');
// require('./bootstrap.bundle.min');
require('./jquery.basictable.min');
// require('./editor');
require('./imageuploadify.min');
require('./front');


//accordian

$(function () {
    $('.panel-content').hide();

    $('.accordion').attr({
        role: 'tablist',
        multiselectable: 'true'
    });

    $('.panel-content').attr('id', function (IDcount) {
        return 'panel-' + IDcount;
    });
    $('.panel-content').attr('aria-labelledby', function (IDcount) {
        return 'control-panel-' + IDcount;
    });
    $('.panel-content').attr('aria-hidden', 'true');
    $('.accordion .panel-content').attr('role', 'tabpanel');

    $('.panel-title').each(function (i) {

        $target = $(this).next('.panel-content')[0].id;

        $link = $('<a>', {
            'href': '#' + $target,
            'aria-expanded': 'false',
            'aria-controls': $target,
            'id': 'control-' + $target
        });

        $(this).wrapInner($link);

    });

    $('.panel-title a').append('<span class="icon">+</span>');

// Now we can play with it
    $('.panel-title a').click(function () {

        if ($(this).attr('aria-expanded') == 'false') { //If aria expanded is false then it's not opened and we want it opened !
            $(this).parents('.accordion').find('[aria-expanded=true]').attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200).attr('aria-hidden', 'true');

            $(this).attr('aria-expanded', true).addClass('active').parent().next('.panel-content').slideDown(200).attr('aria-hidden', 'false');

        } else { // The current panel is opened and we want to close it

            $(this).attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200).attr('aria-hidden', 'true');

        }
        return false;
    });


    $('#table').basictable();

    $('.table-breakpoint').basictable({
        breakpoint: 768
    });

    $('#table-container-breakpoint').basictable({
        containerBreakpoint: 485
    });

    $('#table-swap-axis').basictable({
        swapAxis: true
    });

    $('#table-force-off').basictable({
        forceResponsive: false
    });

    $('#table-no-resize').basictable({
        noResize: true
    });

    $('#table-two-axis').basictable();

    $('#table-max-height').basictable({
        tableWrapper: true
    });

    $('[data-toggle="tooltip"]').tooltip();

    // $("#txtEditor").Editor();

    // $('input[type="file"]').imageuploadify();


//input type file
    $(".custom-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
})
