@extends('layouts.vendor')


@section('content')
    @include('store.include.header')

    <main>
        <!-- home banner -->
        <div class="homebanner w-100 position-relative d-flex">
            <div class="articlebanner position-relative w-50 m-auto text-center">
                <h1 class="fbold">List out your inventory and <span class="flight"> start selling  with us </span></h1>
                <p class="flight">A comprehensive showcase of all your products grabs the attention of audience towards Vdesiconnect. </p>
                <p class="banbtn">
                    <a href="{{route('vendorRegister')}}">Start Selling</a>
                    <a href="{{route('seller_benefits')}}">Know Benefits</a>
                </p>
            </div>
        </div>
        <!--/ home banner -->
    </main>
    <!--/ main -->

    <!-- highlets -->
    <div class="homehighlets">
        <!-- container -->
        <div class="container border-bottom pb-4">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-4 col-md-4  text-center highlets-column">
                    <figure class="m-auto"><img src="/vendors/images/bank.svg" alt="" title="" class="svg"></figure>
                    <article>
                        <h2 class="pb-2">Economical Business Strategies </h2>
                        <p>Our experienced and qualified business services reach out to customers at nominal costs.</p>
                    </article>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-4 col-md-4 text-center highlets-column">
                    <figure class="m-auto"><img src="/vendors/images/growth.svg" alt="" title="" class="svg"></figure>
                    <article>
                        <h2 class="pb-2">Optimized for enhanced growth </h2>
                        <p>Sign up for Vdesiconnect and make your products to get noticed all across the boards. </p>
                    </article>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-4 col-md-4 text-center highlets-column">
                    <figure class="m-auto"><img src="/vendors/images/delivery-truck.svg" alt="" title="" class="svg">
                    </figure>
                    <article>
                        <h2 class="pb-2">Dedicated and Steadfast pickup services </h2>
                        <p>Streamlined pickup services with Vdesiconnect will allow people to never miss out a sale.</p>
                    </article>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ highlets -->

    <!-- how to sell -->
    <div class="sell-process">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row justify-content-center">
                <div class="col-lg-8 text-center">
                    <article class="title pb-4">
                        <h3>Get to know how Vdesiconnect shows  <span>a way to put up your products for sale </span></h3>
                        <p>Be more concentrated on what you desire to sell and the rest are managed by us.</p>
                    </article>
                </div>
            </div>
            <!--/ row -->

            <!-- row -->
            <div class="row processrow">
                <!-- col -->
                <div class="col-lg-5 col-sm-6">
                    <img src="/vendors/images/processregistration.svg" alt="" title="" class="img-fluid">
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-2 text-center dnonetab">
                    <figure class="icon mx-auto"><img src="/vendors/images/svg/gift.svg" class="svg"></figure>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-5 col-sm-6 align-self-center">
                    <article class="flowarticle">
                        <h4><span>Step 1:</span> Get yourself registered with Vdesiconnect and create a catalogue of your products.</h4>
                        <ul>
                            <li>Create a business account and tabulate the products to be sold</li>
                            <li>Let those products to be marketed either on your private brand or other prevailing brand</li>
                            <li>Receive inputs on self-servicing</li>
                            <li>Initiate the process of selling </li>
                        </ul>
                    </article>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->

            <!-- row -->
            <div class="row processrow">
                <!-- col -->
                <div class="col-lg-5 order-lg-3 col-sm-6 order-sm-last">
                    <img src="/vendors/images/processsupport.svg" alt="" title="" class="img-fluid">
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-2 order-lg-2 text-center dnonetab">
                    <figure class="icon mx-auto"><img src="/vendors/images/svg/group.svg" class="svg"></figure>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-5 col-sm-6 align-self-center">
                    <article class="flowarticle">
                        <h4><span>Step 2:</span> Seek assistance from experienced service providers</h4>
                        <ul>
                            <li>Handle all the documentation and cataloguing process to be done under the assistance of specialized service providers  </li>
                            <li>Allow every corner of the product to be clearly visible to audience. </li>
                        </ul>
                    </article>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->

            <!-- row -->
            <div class="row processrow">
                <!-- col -->
                <div class="col-lg-5 col-sm-6">
                    <img src="/vendors/images/processdeliverytruck.svg" alt="" title="" class="img-fluid">
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-2 text-center dnonetab">
                    <figure class="icon mx-auto"><img src="/vendors/images/svg/delivery-truck.svg" class="svg"></figure>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-5 col-sm-6 align-self-center">
                    <article class="flowarticle">
                        <h4><span>Step 3:</span> Get hold of all orders and plan for pickup</h4>
                        <ul>
                            <li>Once catalogued, millions of users can have their hand to access your products.</li>
                            <li>Organize your online business through the help of Seller Panel or Seller Zone Mobile Applications.</li>
                        </ul>
                    </article>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->

            <!-- row -->
            <div class="row processrow">
                <!-- col -->
                <div class="col-lg-5 col-sm-6 order-lg-3 order-sm-last">
                    <img src="/vendors/images/processgrowbusiness.svg" alt="" title="" class="img-fluid">
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-2 order-lg-2 text-center dnonetab">
                    <figure class="icon mx-auto"><img src="/vendors/images/svg/diagram.svg" class="svg"></figure>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-5 col-sm-6 align-self-center">
                    <article class="flowarticle">
                        <h4><span>Step 4:</span> Get your payments and make the most of your online sales </h4>
                        <ul>
                            <li>With the integration of exemplary payment gateways, Vdesiconnect makes your payment to be received quickly. </li>
                            <li>Aggrandize your online business services with reduced interest and collateral free loans</li>
                        </ul>
                    </article>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/container -->
    </div>
    <!--/ how to sell-->

    <!-- introduction -->
    <div class="introhome">
        <div class="container-fluid pl-0">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6 col-md-6">
                    <img src="/vendors/images/introimg.jpg" class="img-fluid" alt="" title="">
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-6 col-md-6 align-self-center p-3">
                    <article class="title">
                        <h3>Vdesiconnect  <span>A one-stop solution to sell your gifts</span></h3>
                        <p>Set out your online store through Vdesiconnect and be with extended scope of customers</p>
                    </article>
                    <p class="text-justify">Showing a hassle-free platform to sell your gifts, Vdesiconnect aspires to establish its brand locally and internationally. Integrated with qualified services and features, Srinivas and Saritha commenced Vdesiconnect.com in February 2019. We might have registered our products on a number of marketplaces, but we are with the thought to personalize our brand and to build long-lasting relationship with our clientele. </p>
                    <p class="text-justify">We move with deeper insights to manage every facet of your business starting from product listing, managed orders, selling online, branding and payments. </p>
                    <a class="greenlink" href="{{route('vendorRegister')}}">Start Selling</a>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
    </div>
    <!--/ introduction-->


@endsection