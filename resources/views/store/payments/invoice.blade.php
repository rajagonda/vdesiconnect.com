@extends('layouts.vendor')


@section('content')


    @include('store.include.login_header')

    <!-- dashboard main -->
    <main class="dbmain">

        <!-- page content -->
        <div class="page-content d-flex align-items-stretch">
            <!-- content inner -->
            <div class="content-inner">
                <!-- page header -->
                <div class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">Invoices / Reports </h2>
                    </div>
                </div>
                <!--/ page header -->
                <!-- Breadcrumb-->
                <div class="breadcrumb-holder container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="db-home.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="payments-overview.php">Payments Overview</a></li>
                        <li class="breadcrumb-item active">Invoices / Reports </li>
                    </ul>
                </div>
                <!--/ Breadcrumb-->
                <!-- page contentmain-->
                <div class="content-main p-3">

                    <!-- container-fluid-->
                    <div class="container-fluid">

                        <!-- page header -->
                        <div class="card p-2 mb-1">
                            <div class="container-fluid">
                                <div class="row justify-content-between">
                                    <!-- col -->
                                    <div class="col-lg-4 col">
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown">
                                                Report Type
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#">Invoices</a>
                                                <a class="dropdown-item" href="#">Payment Reports</a>
                                                <a class="dropdown-item" href="#">Tax Reports</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->

                                    <!-- col -->
                                    <div class="col-lg-4 col text-right  align-self-center">
                                        <a class="greenlink" href="javascript:void(0)" data-toggle="modal" data-target="#creat-report">+ Create New Report</a>
                                    </div>
                                    <!--/ col -->
                                </div>
                            </div>
                        </div>
                        <!--/ page header -->

                        <!-- row -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <table class="table table-breakpoint">
                                        <thead>
                                        <tr>
                                            <th>
                                                <input id="" type="checkbox" value="" class="checkbox-template">
                                            </th>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Sub Type</th>
                                            <th>Date Range</th>
                                            <th>Size</th>
                                            <th>Date Created</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>

                                        <tbody>

                                        <tr>
                                            <td>
                                                <input id="" type="checkbox" value="" class="checkbox-template">
                                            </td>
                                            <td>0dd9e9af-41b1-476c-a7e0-9ccf73dceca5</td>
                                            <td>Invoices</td>
                                            <td>Commission Invoice Transaction Details</td>
                                            <td>2019-03-01 - 2019-03-31</td>
                                            <td>14.3 KB</td>
                                            <td>Mar 31, 2019 18:03:00</td>
                                            <td class="options">
                                                <a href="{{route('paymentsInvoiceView')}}" data-toggle="tooltip" title="Download Report" ><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <input id="" type="checkbox" value="" class="checkbox-template">
                                            </td>
                                            <td>0dd9e9af-41b1-476c-a7e0-9ccf73dceca5</td>
                                            <td>Invoices</td>
                                            <td>Commission Invoice Transaction Details</td>
                                            <td>2019-03-01 - 2019-03-31</td>
                                            <td>14.3 KB</td>
                                            <td>Mar 31, 2019 18:03:00</td>
                                            <td class="options">
                                                <a href="{{route('paymentsInvoiceView')}}" data-toggle="tooltip" title="Download Report" ><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <input id="" type="checkbox" value="" class="checkbox-template">
                                            </td>
                                            <td>0dd9e9af-41b1-476c-a7e0-9ccf73dceca5</td>
                                            <td>Invoices</td>
                                            <td>Commission Invoice Transaction Details</td>
                                            <td>2019-03-01 - 2019-03-31</td>
                                            <td>14.3 KB</td>
                                            <td>Mar 31, 2019 18:03:00</td>
                                            <td class="options">
                                                <a href="{{route('paymentsInvoiceView')}}" data-toggle="tooltip" title="Download Report" ><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <input id="" type="checkbox" value="" class="checkbox-template">
                                            </td>
                                            <td>0dd9e9af-41b1-476c-a7e0-9ccf73dceca5</td>
                                            <td>Invoices</td>
                                            <td>Commission Invoice Transaction Details</td>
                                            <td>2019-03-01 - 2019-03-31</td>
                                            <td>14.3 KB</td>
                                            <td>Mar 31, 2019 18:03:00</td>
                                            <td class="options">
                                                <a href="{{route('paymentsInvoiceView')}}" data-toggle="tooltip" title="Download Report" ><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <input id="" type="checkbox" value="" class="checkbox-template">
                                            </td>
                                            <td>0dd9e9af-41b1-476c-a7e0-9ccf73dceca5</td>
                                            <td>Invoices</td>
                                            <td>Commission Invoice Transaction Details</td>
                                            <td>2019-03-01 - 2019-03-31</td>
                                            <td>14.3 KB</td>
                                            <td>Mar 31, 2019 18:03:00</td>
                                            <td class="options">
                                                <a href="{{route('paymentsInvoiceView')}}" data-toggle="tooltip" title="Download Report" ><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--/ row -->
                    </div>
                    <!-- container - fluid -->

                </div>
                <!--/ page content main-->
            </div>
            <!--/ content inner -->
        </div>
        <!--/ page content -->
    </main>
    <!--/ dashboard main -->




    <!-- The Modal -->
    <div class="modal" id="creat-report">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Generate Report</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <!-- form -->
                    <form>
                        <!-- form group -->
                        <div class="form-group">
                            <label>Report Type</label>
                            <select class="form-control">
                                <option>Type</option>
                                <option>Invoices</option>
                                <option>Payment Reports</option>
                                <option>Tax Reports</option>
                            </select>
                        </div>
                        <!--/ form group -->
                        <!-- form group -->
                        <div class="form-group">
                            <label>Report Sub Type</label>
                            <select class="form-control">
                                <option>Sub Type</option>
                                <option>Commission Invoices</option>
                                <option>Commission Invoices Transaction Details</option>
                            </select>
                        </div>
                        <!--/ form group -->
                    </form>
                    <!--/ form -->
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="greenlink">Create Report</button>
                    <button type="button" class="whitelink" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

    </div>
    <!--/ the modal-->


@endsection