@extends('layouts.vendor')


@section('content')


    @include('store.include.login_header')

    <!-- dashboard main -->
    <main class="dbmain">

        <!-- page content -->
        <div class="page-content d-flex align-items-stretch">
            <!-- content inner -->
            <div class="content-inner">
                <!-- page header -->
                <div class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">OD11502564193385600</h2>
                    </div>
                </div>
                <!--/ page header -->
                <!-- Breadcrumb-->
                <div class="breadcrumb-holder container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="db-home.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="total-outstanding-postpaid.php">Total Outstanding Postpaid</a></li>
                        <li class="breadcrumb-item active">Order ID will Be here </li>
                    </ul>
                </div>
                <!--/ Breadcrumb-->
                <!-- Forms Section-->
                <!-- page content main-->
                <div class="content-main p-3">
                    <!-- container-fluid-->
                    <div class="container-fluid">
                        <!-- row -->
                        <div class="row pb-3">
                            <div class="col-lg-12 text-right">
                                <input type="button" value="Back to Post paid Orders" class="greenlink" onclick="window.location.href='total-outstanding-postpaid.php'">
                            </div>
                        </div>
                        <!--/ row -->
                        <!-- row -->
                        <div class="row">
                            <!-- col left -->
                            <div class="col-lg-6">
                                <!-- card -->
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="border-bottom py-3">Order Details</h5>
                                        <!-- row -->
                                        <div class="row py-2" >
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Order Date</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <p class="text-justify">Mar 24, 2019</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2" >
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Order Status</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <p class="text-justify"><span class="greencolor">Shipped</span></p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2" >
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Order Id</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <p class="text-justify"><span class="greencolor">OD115025641933856000</span></p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2" >
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Buyer Details</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <p class="text-justify">Saurav Jyoti Bhuyan, HDFC Bank. R.K.B. Road. Near Thana Chariali. Dibrugarh Dibrugarh - 786001, Assam</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2" >
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Mode of Payment</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <p class="text-justify">Payment on Delivery</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2" >
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Image Thumbnail</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <p class="text-justify"><img width="100" src="db/img/cake05.jpg"></p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->
                                    </div>
                                </div>
                                <!--/ card -->

                                <!-- card Order Cancellation Refund -->
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="border-bottom py-3">Sale Details</h5>
                                        <!-- row -->
                                        <div class="row py-2" >
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Sale Amount</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <p class="text-justify">Rs: 1909.00</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2" >
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Order Item Value</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <p class="text-justify"><span class="greencolor">Rs: 1,909.00</span></p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2" >
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Transaction Details</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <p class="text-justify"><span class="greencolor"></span></p>
                                                <div class="row py-2">
                                                    <div class="col-lg-4">Sale Amount</div>
                                                    <div class="col-lg-8">Rs: 1,909.00</div>
                                                </div>
                                                <div class="row py-2">
                                                    <div class="col-lg-4">TCS Amount</div>
                                                    <div class="col-lg-8"><span class="redcolor">Rs: -16.178</span></div>
                                                </div>
                                                <div class="row py-2">
                                                    <div class="col-lg-4">Marketplace Fee</div>
                                                    <div class="col-lg-8"><span class="redcolor">Rs: -594.00</span></div>
                                                </div>
                                                <div class="row py-2">
                                                    <div class="col-lg-4">Taxes</div>
                                                    <div class="col-lg-8"><span class="redcolor">Rs: -106.00</span></div>
                                                </div>
                                                <div class="row py-2">
                                                    <div class="col-lg-4">Amount</div>
                                                    <div class="col-lg-8"><span class="greencolor fbold">Rs: 1,191.81</span></div>
                                                </div>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2" >
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Expect Physical Delivery</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <p class="text-justify">Refund</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2" >
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Reason</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <p class="text-justify">Order Cancelled</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->
                                    </div>
                                </div>
                                <!--/ card Order Cancellation Refund -->
                            </div>
                            <!--/ col left -->
                            <!-- col right -->
                            <div class="col-lg-6">
                                <!-- card -->
                                <div class="card">
                                    <!-- card body -->
                                    <div class="card-body">
                                        <h5 class="border-bottom py-3">Order Tracking Details</h5>
                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Order Received</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p>Mar 27, 2019, Wednesday</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Shipment Dispatched</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p>Mar 29, 2019, Friday</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Settlement Due Date</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p><span class="lightgray">Apr 17, 2019, Wednesday</span></p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->
                                    </div>
                                    <!--/ card body -->
                                </div>
                                <!--/ card -->
                            </div>
                            <!--/ col right -->
                        </div>
                        <!--/ row -->


                    </div>
                    <!-- container - fluid -->
                </div>
                <!--/ page content main-->
            </div>
            <!--/ content inner -->
        </div>
        <!--/ page content -->
    </main>
    <!--/ dashboard main -->


@endsection