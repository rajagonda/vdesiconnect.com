@extends('layouts.vendor')


@section('content')

    @include('store.include.login_header')

    <!-- dashboard main -->
    <main class="dbmain">

        <!-- page content -->
        <div class="page-content d-flex align-items-stretch">
            <!-- content inner -->
            <div class="content-inner">
                <!-- page header -->
                <div class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">Payments Overview</h2>
                    </div>
                </div>
                <!--/ page header -->
                <!-- Breadcrumb-->
                <div class="breadcrumb-holder container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="db-home.php">Home</a></li>
                        <li class="breadcrumb-item active">Payments Overview </li>
                    </ul>
                </div>
                <!--/ Breadcrumb-->
                <!-- page contentmain-->
                <div class="content-main p-3">
                    <!-- container-fluid-->
                    <div class="container-fluid">
                        <!-- row -->
                        <div class="row paymentspage">

                            <!-- col-->
                            <div class="col-lg-4">
                                <div class="card p-4 mb-1">
                                    <h4 class="h4 border-bottom py-2 mb-2">Next Payment</h4>
                                    <p>Estimated value of next payment. This may change due to returns that come in before the next payout.</p>

                                    <table class="table mt-3 noborder ">
                                        <tr>
                                            <td>Postpaid </td>
                                            <td><a href="javascript:void(0)">Rs: 0.00</a></td>
                                        </tr>
                                        <tr>
                                            <td>Pre paid </td>
                                            <td><a href="javascript:void(0)">Rs: 1,207.12</a></td>
                                        </tr>
                                        <tr class="border-top">
                                            <td><h5 class="h5">Total</h5> </td>
                                            <td>Rs: 1,207.12 </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- col-->

                            <!-- col-->
                            <div class="col-lg-4">
                                <div class="card p-4 mb-1">
                                    <h4 class="h4 border-bottom py-2 mb-2">Total Outstanding Payments</h4>
                                    <p>Total amount you are to receive from Vdesiconnect for dispatched orders. It includes the 'Next Payment' amount shown above.</p>

                                    <table class="table mt-3 noborder ">
                                        <tr>
                                            <td>Postpaid </td>
                                            <td><a href="total-outstanding-postpaid.php">Rs: 1,703.00</a></td>
                                        </tr>
                                        <tr>
                                            <td>Pre paid </td>
                                            <td><a href="javascript:void(0)">Rs: 1,207.12</a></td>
                                        </tr>
                                        <tr class="border-top">
                                            <td><h5 class="h5">Total</h5> </td>
                                            <td>Rs: 2,910.12</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- col-->

                            <!-- col-->
                            <div class="col-lg-4">
                                <div class="card p-4 mb-1">
                                    <h4 class="h4 border-bottom py-2 mb-2">Last Payment</h4>
                                    <p>These payments have been initiated and may take up to 48 hours to reflect in your bank account.</p>

                                    <table class="table mt-3 noborder ">
                                        <tr>
                                            <td>Postpaid </td>
                                            <td><a href="javascript:void(0)">Rs: 0.00</a></td>
                                        </tr>
                                        <tr>
                                            <td>Pre paid </td>
                                            <td><a href="javascript:void(0)">Rs: 0.00</a></td>
                                        </tr>
                                        <tr class="border-top">
                                            <td><h5 class="h5">Total</h5> </td>
                                            <td>Rs: 0.00</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- col-->

                        </div>
                        <!--/ row -->

                    </div>
                    <!-- container - fluid -->
                </div>
                <!--/ page content main-->
            </div>
            <!--/ content inner -->
        </div>
        <!--/ page content -->
    </main>
    <!--/ dashboard main -->


@endsection