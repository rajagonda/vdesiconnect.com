@extends('layouts.vendor')


@section('content')

    @include('store.include.header')



    <!-- main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="flight">Pricing</h1>
                        <p>Get up-to-the-minute update on all the pricing scenarios</p>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page main -->
        <div class="subpage-main">
            <div class="homehighlets py-0">
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- left column-->
                        <div class="col-lg-12">
                            <h2>How the pricing scenario of Vdesiconnect is structured up?</h2>
                            <p class="text-justify">Vdesiconnect offers quick payments with the consolidation of exemplary payment gateways. Ensuring with on-time deliveries, we endeavour to attract the sight of many customers. From the day of dispatching your products, those will be at your doorstep within the next 7-15 business days. Add desired products to the cart and we charge small amount of money on all the successful orders. </p>

                            <h2>Structure</h2>

                            <!-- row -->
                            <div class="row py-3">
                                <!-- col -->
                                <div class="col-lg-3 col-sm-6 text-center highlets-column pricingcol">
                                    <figure class="m-auto"><img src="/vendors/images/svg/money.svg" alt="" title="" class="svg"></figure>
                                    <article>
                                        <h2 class="pb-2">Settlement Amount </h2>
                                        <p>In a period of 7-15 days from the date of product dispatch, money will be credited to your bank account.</p>
                                    </article>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-3 col-sm-6 text-center highlets-column pricingcol">
                                    <figure class="m-auto"><img src="/vendors/images/svg/shopping.svg" alt="" title="" class="svg"></figure>
                                    <article>
                                        <h2 class="pb-2">Order Item Value </h2>
                                        <p>Both Selling and Shipping costs paid by the consumer and discount provided by the supplier will not be added up.</p>
                                    </article>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-3 col-sm-6 text-center highlets-column pricingcol">
                                    <figure class="m-auto"><img src="/vendors/images/svg/tag.svg" alt="" title="" class="svg"></figure>
                                    <article>
                                        <h2 class="pb-2">Market Place Fee </h2>
                                        <p>A total amount of shipping cost, fixed charges and selling commission.</p>
                                    </article>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-3 col-sm-6 text-center highlets-column pricingcol">
                                    <figure class="m-auto"><img src="/vendors/images/svg/percentage.svg" alt="" title="" class="svg"></figure>
                                    <article>
                                        <h2 class="pb-2">GST on Market Place Fee </h2>
                                        <p>As per the rules, we charge 18% of Marketplace fee.</p>
                                    </article>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ row -->
                            <article class="py-2">
                                <h2>Market Place Fee</h2>
                                <p>In detail view of Marketplace fee </p>
                            </article>
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>S.No:</th>
                                    <th>Type of Fee</td>
                                    <th>Description</th>
                                </tr>
                                </thead>

                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Commission fee </td>
                                    <td>As per the product category and sub-category, it is the proportion of order item value</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Shipping fee </td>
                                    <td>Measured as per product weight and shipping location</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Collection fee </td>
                                    <td>Charge levied on payment gateways or cash collection during every sale</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Fixed fee </td>
                                    <td>It is the small fee that we charge on every transaction</td>
                                </tr>
                                </tbody>

                            </table>

                            <article class="py-2">
                                <h2>Shipping Fee</h2>
                            </article>

                            <p class="text-justify pb-2">Providing business grade solutions for many of the wholesalers, we make sure to deliver your products at the scheduled dates. To reach out to large businesses, Vdesiconnect move ahead with delivery of services through professional logistic providers and exclude the shipping cost from the marketing price in before the complete payment.  </p>
                            <p class="text-justify pb-2">Shipping fee is charged on the product based on actual weight or volumetric weight, whichever is more. This is to be charged on products those are of less weight but occupy more shopping space.</p>
                            <p class="text-justify pb-2">Every new customer to our site will be considered as Bronze seller, and then for every quarterly revision and based on the performance metrics, they will be considered as either Silver or Gold sellers. </p>

                        </div>
                        <!--/ left column -->
                    </div>
                    <!-- row -->
                </div>
            </div>
        </div>
        <!--/ sub page main-->

    </main>
    <!--/ main -->



@endsection