<div class="dbheader">
    <nav class="navbar navbar-expand-sm">
        <a class="navbar-brand" href="{{route('sellerPreDashbord')}}"><img src="/vendors/images/db/logothumb.svg"
                                                                           alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('sellerPreDashbord')}}">Home <span
                                class="sr-only">(current)</span></a>
                </li>

                @if (vendorValidation(Auth::guard('store')->user()->id))
{{--                @if (isset($profileAccount) && $profileAccount->va_status=='approved' && isset($profileBusiness) && $profileBusiness->vb_status=='approved')--}}


                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Listings</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{route('seller_listings')}}">All Listings</a>
                            <a class="dropdown-item" href="{{route('seller_new_listings')}}">New Listing</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Orders
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{route('sellerOrdersNewList')}}">Active Orders</a>
                            <a class="dropdown-item" href="{{route('sellerOrdersCompletedList')}}">Completed Orders</a>
                            {{--<a class="dropdown-item" href="{{route('sellerOrdersList', ['type'=>'Returns'])}}">Returns</a>--}}
                            {{--<a class="dropdown-item" href="{{route('sellerOrdersList', ['type'=>'Cancellations'])}}">Cancellations</a>--}}
                        </div>
                    </li>

                @endif



                {{--<li class="nav-item dropdown">--}}
                {{--<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                {{--Payment--}}
                {{--</a>--}}
                {{--<div class="dropdown-menu" aria-labelledby="navbarDropdown">--}}
                {{--<a class="dropdown-item" href="{{route('paymentsOverview')}}">Payments Overview</a>--}}
                {{--<a class="dropdown-item" href="{{route('paymentsInvoice')}}">Invoices / Reports</a>--}}
                {{--</div>--}}
                {{--</li>--}}
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {!! Auth::guard('store')->user()->name !!}
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{route('profileAccount')}}">Account</a>
                        <a class="dropdown-item" href="{{route('BusinessDetails')}}">Business Details</a>
                        <a class="dropdown-item" href="{{route('vendorSettings')}}">Settings</a>
                        <a class="dropdown-item" href="{{ route('vendorlogout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('vendorlogout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>


                    </div>
                </li>
            </ul>
        </div>
    </nav>
</div>