<footer>
    <!-- top footer -->
    <div class="top-footer">
        <!-- container-->
        <div class="container">
            <div class="row">
                <!-- col -->
                <div class="col-lg-8 col-md-9">
                    <ul class="nav">
                        <li class="nav-item"><a href="{{route('vendor_home')}}" class="nav-link">Home</a></li>
                        <li class="nav-item"><a href="{{route('seller_pricing')}}" class="nav-link">Pricing</a></li>
                        <li class="nav-item"><a href="{{route('seller_faq')}}" class="nav-link">Faq’s</a></li>
                        <li class="nav-item"><a href="{{route('seller_benefits')}}" class="nav-link">Benefits</a></li>
                        <li class="nav-item"><a href="{{route('home')}}" target="_blank" class="nav-link">www.vdesiconnect.com</a></li>
                    </ul>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-4 col-md-3">
                    <ul class="nav float-right">
                        <li class="nav-item"><a href="javascript:void(0)" class="nav-link"><img src="/vendors/images/svg/facebook.svg" class="svg"></a></li>
                        <li class="nav-item"><a href="javascript:void(0)" class="nav-link"><img src="/vendors/images/svg/twitter-logo-silhouette.svg" class="svg"></a></li>
                        <li class="nav-item"><a href="javascript:void(0)" class="nav-link"><img src="/vendors/images/svg/linkedin-logo.svg" class="svg"></a></li>
                    </ul>
                </div>
                <!--/ col -->
            </div>
        </div>
        <!--/ container -->
    </div>
    <!-- top footer -->
    <!-- bottom footer -->
    <div class="bottom-footer text-center">
        <p>All Copy rights Reserved @ vdesiconnect.  All Rights Reserved</p>
    </div>
    <!--/ bottom footer -->
</footer>