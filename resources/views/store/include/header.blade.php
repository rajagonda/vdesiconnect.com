<header>
    <div class="customcontainer">
        <nav class="navbar navbar-expand-lg ">
            <a class="navbar-brand" href="{{route('vendor_home')}}"><img src="/vendors/images/logo.svg" alt="" title=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('vendor_home')}}">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('seller_faq')}}">Faq's</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('seller_pricing')}}">Pricing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('seller_benefits')}}">Benefits</a>
                    </li>
                </ul>
                <div class="form-inline my-2 my-lg-0">
                    <a href="{{route('vendorlogin')}}" class="whitelink">Login</a>
                    <a href="{{route('vendorRegister')}}" class="greenlink">Start Selling</a>
                </div>
            </div>
        </nav>
    </div>
</header>