@extends('layouts.vendor')


@section('content')

    @include('store.include.login_header')


    <!-- dashboard main -->
    <main class="dbmain">

        <!-- page content -->
        <div class="page-content d-flex align-items-stretch">
            <!-- content inner -->
            <div class="content-inner">
                <!-- page header -->
                <div class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">Account Information</h2>
                    </div>
                </div>
                <!--/ page header -->
                <!-- Breadcrumb-->
                <div class="breadcrumb-holder container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('sellerPreDashbord')}}">Home</a></li>
                        <li class="breadcrumb-item active">Account Details</li>
                    </ul>
                </div>
                @if (Session::has('flash_message'))
                    <br/>
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ Session::get('flash_message' ) }}</strong>
                    </div>
            @endif
            <!--/ Breadcrumb-->
                <!-- page contentmain-->
                <div class="content-main p-3">
                    <!-- container-fluid-->
                    <div class="container-fluid">
                        <!-- row -->
                        <div class="row paymentspage">

                            <!-- col-->
                            <div class="col-lg-4 col-md-6">
                                <div class="card p-4 mb-1 articles">
                                    <h4 class="h4 border-bottom py-2 mb-2">Display information <span
                                                class="float-right"><a href="javascript:void(0)" data-toggle="modal"
                                                                       data-target="#edit-display">Edit</a></span></h4>

                                    <div class="article-in">
                                        <div class="item">
                                            <h3>Display Name</h3>
                                            <small>{{(@$profile->va_display_name) ? @$profile->va_display_name:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>Business Description</h3>
                                            <small>{{(@$profile->va_business_desc) ? @$profile->va_business_desc:'--'}}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- col-->

                            <!-- col-->
                            <div class="col-lg-4 col-md-6">
                                <div class="card p-4 mb-1 articles">
                                    <h4 class="h4 border-bottom py-2 mb-2">Pick up Address <span class="float-right"><a
                                                    href="javascript:void(0)" data-toggle="modal"
                                                    data-target="#edit-address">Edit</a></span></h4>


                                    <div class="article-in">
                                    <p>Where from collection executive pick the product</p>
                                        <div class="item">
                                            <h3>Address Line 01</h3>
                                            <small>{{(@$profile->va_address_1) ? @$profile->va_address_1:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>Address Line 02</h3>
                                            <small>{{(@$profile->va_address_2) ? @$profile->va_address_2:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>State</h3>
                                            <small>{{(@$profile->va_state) ? @$profile->va_state:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>City</h3>
                                            <small>{{(@$profile->va_city) ? @$profile->va_city:'--'}}</small>
                                        </div>
                                        <div class="item">
                                            <h3>Pincode</h3>
                                            <small>{{(@$profile->va_pincode) ? @$profile->va_pincode:'--'}}</small>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- col-->

                            <!-- col-->
                            <div class="col-lg-4 col-md-6">
                                <div class="card p-4 mb-1 articles">
                                    <h4 class="h4 border-bottom py-2 mb-2">Contact Details</h4>

                                    <div class="article-in">
                                        <div class="item">
                                            <h3>Your Name</h3>
                                            <small>{{(@$userdetails->name) ? @$userdetails->name:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>Mobile Number</h3>
                                            <small>{{(@$userdetails->mobile) ? @$userdetails->mobile:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>Email Address</h3>
                                            <small>{{(@$userdetails->email) ? @$userdetails->email:'--'}}</small>
                                        </div>

                                        {{--<div class="item">--}}
                                            {{--<h3>Preferred Time to Call</h3>--}}
                                            {{--<small>{{(@$profile->va_time_from) ? @$profile->va_time_from:'--'}}--}}
                                                {{--to {{(@$profile->va_time_to) ? @$profile->va_time_to:'--'}}</small>--}}
                                        {{--</div>--}}

                                    </div>
                                </div>
                            </div>
                            <!-- col-->


                        </div>
                        <!--/ row -->

                    </div>
                    <!-- container - fluid -->
                </div>
                <!--/ page content main-->
            </div>
            <!--/ content inner -->
        </div>
        <!--/ page content -->
    </main>
    <!--/ dashboard main -->



    <!-- Modal for Edit Display Information -->
    <div class="modal" id="edit-display">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Display Information</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form method="POST" id="products" action="{{ route('profileAccount') }}"
                          accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="va_id" value="{{@$profile->va_id}}">
                        <div class="form-group">
                            <label>Display Name</label>
                            <input type="text" class="form-control mt-1" name="va_display_name"
                                   value="{{(@$profile->va_display_name) ? @$profile->va_display_name:''}}"
                                   placeholder="Ex: Company Name will be here">
                        </div>

                        <div class="form-group">
                            <label>Business Description</label>
                            <textarea class="form-control mt-1" name="va_business_desc"
                                      rows="10">{{(@$profile->va_business_desc) ? @$profile->va_business_desc:''}}</textarea>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="greenlink">Save</button>
                        </div>

                    </form>
                </div>
                <!-- Modal footer -->

                <!--/ modal footer -->
            </div>
        </div>
    </div>
    </div>
    <!--/ Modal for Edit Display Information-->

    <!-- Modal for Edit Address Information -->
    <div class="modal" id="edit-address">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Pickup Address</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form method="POST" id="products" action="{{ route('profileAccount') }}"
                          accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="va_id" value="{{@$profile->va_id}}">
                        <div class="form-group">
                            <label>Address Line 01</label>
                            <input type="text" name="va_address_1"
                                   value="{{(@$profile->va_address_1) ? @$profile->va_address_1:''}}"
                                   class="form-control mt-1" placeholder="Address line 01">
                        </div>

                        <div class="form-group">
                            <label>Address Line 02</label>
                            <input type="text" name="va_address_2"
                                   value="{{(@$profile->va_address_2) ? @$profile->va_address_2:''}}"
                                   class="form-control mt-1" placeholder="Address line 02">
                        </div>

                        <div class="form-group">
                            <label>State</label>
                            <select class="form-control" name="va_state">
                                <option value="Telangana" {{ (!empty(old('va_state')) && old('va_state')=='Telangana')  ? 'selected' : (((@$profile) && (@$profile->va_state == 'Telangana')) ? 'selected' : '') }}>
                                    Telangana
                                </option>
                                <option value="Andhra Pradesh" {{ (!empty(old('va_state')) && old('va_state')=='Andhra Pradesh')  ? 'selected' : (((@$profile) && (@$profile->va_state == 'Andhra Pradesh')) ? 'selected' : '') }}>
                                    Andhra Pradesh
                                </option>
                                <option value="Tamil Nadu" {{ (!empty(old('va_state')) && old('va_state')=='Tamil Nadu')  ? 'selected' : (((@$profile) && (@$profile->va_state == 'Tamil Nadu')) ? 'selected' : '') }}>
                                    Tamil Nadu
                                </option>
                                <option value="Karnataka" {{ (!empty(old('va_state')) && old('va_state')=='Karnataka')  ? 'selected' : (((@$profile) && (@$profile->va_state == 'Karnataka')) ? 'selected' : '') }}>
                                    Karnataka
                                </option>
                                <option value="Tamil Nadu" {{ (!empty(old('va_state')) && old('va_state')=='Kerala')  ? 'selected' : (((@$profile) && (@$profile->va_state == 'Kerala')) ? 'selected' : '') }}>
                                    Kerala
                                </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>City</label>

                            <input type="text" name="va_city"
                                   value="{{(@$profile->va_city) ? @$profile->va_city:''}}"
                                   class="form-control mt-1" placeholder="Enter City Name">
{{--                            <select class="form-control" name="va_city">--}}
{{--                                <option value="Hyderabad" {{ (!empty(old('va_city')) && old('va_city')=='Hyderabad')  ? 'selected' : (((@$profile) && (@$profile->va_city == 'Hyderabad')) ? 'selected' : '') }}>--}}
{{--                                    Hyderabad--}}
{{--                                </option>--}}
{{--                                <option value="Secunderabad" {{ (!empty(old('va_city')) && old('va_city')=='Secunderabad')  ? 'selected' : (((@$profile) && (@$profile->va_city == 'Secunderabad')) ? 'selected' : '') }}>--}}
{{--                                    Secunderabad--}}
{{--                                </option>--}}
{{--                            </select>--}}
                        </div>

                        <div class="form-group">
                            <label>Pincode</label>
                            <input type="number" name="va_pincode"
                                   value="{{(@$profile->va_pincode) ? @$profile->va_pincode:''}}"
                                   class="form-control mt-1" placeholder="Ex:500072">
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="greenlink">Save</button>
                        </div>

                    </form>
                </div>
                <!-- Modal footer -->

                <!--/ modal footer -->
            </div>
        </div>
    </div>
    </div>
    <!--/ Modal for Edit Address Information-->

    <!-- Modal for Edit Contact Information -->
    <div class="modal" id="edit-contact">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Contact Details</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form method="POST" id="products" action="{{ route('profileAccount') }}"
                          accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="va_id" value="{{@$profile->va_id}}">
                        <div class="form-group">
                            <label>Your Name</label>
                            <input type="text" name="va_name" value="{{(@$profile->va_name) ? @$profile->va_name:''}}"
                                   class="form-control mt-1" placeholder="Name will be here">
                        </div>

                        <div class="form-group">
                            <label>Mobile Number</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                <span class="input-group-text">
                                        India
                                </span>
                                </div>
                                <input type="number" name="va_mobile"
                                       value="{{(@$profile->va_mobile) ? @$profile->va_mobile:''}}"
                                       class="form-control mt-1" placeholder="Ex: +91 9642123254">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="text" name="va_email"
                                   value="{{(@$profile->va_email) ? @$profile->va_email:''}}" class="form-control mt-1"
                                   placeholder="praveennandipati@gmail.com">
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>From</label>
                                    <select class="form-control" name="va_time_from">
                                        <option value="09:00 hrs" {{ (!empty(old('va_time_from')) && old('va_time_from')=='09:00 hrs')  ? 'selected' : (((@$profile) && (@$profile->va_time_from == '09:00 hrs')) ? 'selected' : '') }}>
                                            09:00 hrs
                                        </option>
                                        <option value="10:00 hrs" {{ (!empty(old('va_time_from')) && old('va_time_from')=='10:00 hrs')  ? 'selected' : (((@$profile) && (@$profile->va_time_from == '10:00 hrs')) ? 'selected' : '') }}>
                                            10:00 hrs
                                        </option>
                                        <option value="11:00 hrs" {{ (!empty(old('va_time_from')) && old('va_time_from')=='11:00 hrs')  ? 'selected' : (((@$profile) && (@$profile->va_time_from == '11:00 hrs')) ? 'selected' : '') }}>
                                            11:00 hrs
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>To</label>
                                    <select class="form-control" name="va_time_to">
                                        <option value="09:00 hrs" {{ (!empty(old('va_time_to')) && old('va_time_to')=='09:00 hrs')  ? 'selected' : (((@$profile) && (@$profile->va_time_to == '09:00 hrs')) ? 'selected' : '') }}>
                                            09:00 hrs
                                        </option>
                                        <option value="10:00 hrs" {{ (!empty(old('va_time_to')) && old('va_time_to')=='10:00 hrs')  ? 'selected' : (((@$profile) && (@$profile->va_time_to == '10:00 hrs')) ? 'selected' : '') }}>
                                            10:00 hrs
                                        </option>
                                        <option value="11:00 hrs" {{ (!empty(old('va_time_to')) && old('va_time_to')=='11:00 hrs')  ? 'selected' : (((@$profile) && (@$profile->va_time_to == '11:00 hrs')) ? 'selected' : '') }}>
                                            11:00 hrs
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="greenlink">Save</button>
                        </div>
                    </form>
                </div>
                <!-- Modal footer -->

                <!--/ modal footer -->
            </div>
        </div>
    </div>
    </div>
    <!--/ Modal for Edit Contact Information-->



@endsection
