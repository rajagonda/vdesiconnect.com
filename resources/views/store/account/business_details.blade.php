@extends('layouts.vendor')


@section('content')

    @include('store.include.login_header')


    <!-- dashboard main -->
    <main class="dbmain">

        <!-- page content -->
        <div class="page-content d-flex align-items-stretch">
            <!-- content inner -->
            <div class="content-inner">
                <!-- page header -->
                <div class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">Business Information</h2>
                    </div>
                </div>
                <!--/ page header -->
                <!-- Breadcrumb-->
                <div class="breadcrumb-holder container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('sellerPreDashbord')}}">Home</a></li>
                        <li class="breadcrumb-item active">Business Details </li>
                    </ul>
                </div>
                <!--/ Breadcrumb-->
                <!-- page contentmain-->


                {{ laravelReturnMessageShow() }}

                <div class="content-main p-3">
                    <!-- container-fluid-->
                    <div class="container-fluid">
                        <!-- row -->
                        <div class="row paymentspage">

                            <!-- col-->
                            <div class="col-lg-4 col-md-6">
                                <div class="card p-4 mb-1 articles">
                                    <h4 class="h4 border-bottom py-2 mb-2">Business Details <span class="float-right"><a href="javascript:void(0)" data-toggle="modal" data-target="#edit-business-details">Edit</a></span></h4>

                                    <div class="article-in">

                                        <div class="item">
                                            <h3>Business Name</h3>
                                            <small>{{(@$profile->vb_name) ? @$profile->vb_name:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>TAN</h3>
                                            <small>{{(@$profile->vb_tan) ? @$profile->vb_tan:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>GSTIN / Provisional ID Number*</h3>
                                            <small>{{(@$profile->vb_gst) ? @$profile->vb_gst:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>Registered Business Address</h3>
                                            <small>{{(@$profile->vb_address1) ? @$profile->vb_address1:'--'}},
                                                {{(@$profile->vb_address2) ? @$profile->vb_address2:''}},
                                                {{(@$profile->vb_state) ? @$profile->vb_state:''}},
                                                {{(@$profile->vb_city) ? @$profile->vb_city:''}},
                                                {{(@$profile->vb_pincode) ? @$profile->vb_pincode:''}}
                                            </small>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- col-->

                            <!-- col-->
                            <div class="col-lg-4 col-md-6">
                                <div class="card p-4 mb-1 articles">
                                    <h4 class="h4 border-bottom py-2 mb-2">Bank Details <span class="float-right"><a href="javascript:void(0)" data-toggle="modal" data-target="#bank-details">Edit</a></span></h4>

                                    <div class="article-in">
                                        <div class="item">
                                            <h3>Account holder name</h3>
                                            <small>{{(@$profile->vb_account_name) ? @$profile->vb_account_name:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>Account number</h3>
                                            <small>{{(@$profile->vb_account_number) ? @$profile->vb_account_number:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>Bank name</h3>
                                            <small>{{(@$profile->vb_bank) ? @$profile->vb_bank:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>Branch</h3>
                                            <small>{{(@$profile->vb_branch) ? @$profile->vb_branch:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>IFSC</h3>
                                            <small>{{(@$profile->vb_ifsc) ? @$profile->vb_ifsc:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>State</h3>
                                            <small>{{(@$profile->vb_bank_state) ? @$profile->vb_bank_state:'--'}}</small>
                                        </div>
                                        <div class="item">
                                            <h3>City</h3>
                                            <small>{{(@$profile->vb_bank_city) ? @$profile->vb_bank_city:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>Business Type</h3>
                                            <small>{{(@$profile->vb_type) ? @$profile->vb_type:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>PAN</h3>
                                            <small>{{(@$profile->vb_pan) ? @$profile->vb_pan:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>Address Proof</h3>
                                            <small> @if(isset($profile->vb_address_proof))
                                                    @php
                                                        $scode='';
                                                        $image= (@$profile->vb_address_proof) ? '<img src="/uploads/vendor/proofs/thumbs/'.@$profile->vb_address_proof.'" width="50px" />' : '-';
                                                    @endphp
                                                    @if($image!='-')
                                                        <div class="imagediv">
                                                            <a data-fancybox="" class="popupimages"
                                                               href="{{url('/uploads/vendor/proofs/'.@$profile->vb_address_proof)}}">
                                                                {!! $image !!}
                                                            </a>
                                                        </div>
                                                     @endif
                                                   @else
                                                        --
                                                   @endif
                                            </small>
                                        </div>

                                        <div class="item">
                                            <h3>Cancelled Cheque</h3>
                                            <small> @if(isset($profile->vb_cheque))
                                                    @php
                                                        $scode='';
                                                        $image= (@$profile->vb_cheque) ? '<img src="/uploads/vendor/proofs/thumbs/'.@$profile->vb_cheque.'" width="50px" />' : '-';
                                                    @endphp
                                                    @if($image!='-')
                                                        <div class="imagediv">
                                                            <a data-fancybox="" class="popupimages"
                                                               href="{{url('/uploads/vendor/proofs/'.@$profile->vb_cheque)}}">
                                                                {!! $image !!}
                                                            </a>
                                                        </div>
                                                    @endif
                                                @else
                                                    --
                                                @endif

                                            </small>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- col-->

                        </div>
                        <!--/ row -->

                    </div>
                    <!-- container - fluid -->
                </div>
                <!--/ page content main-->
            </div>
            <!--/ content inner -->
        </div>
        <!--/ page content -->
    </main>
    <!--/ dashboard main -->



    <!-- Modal for Business Details -->
    <div class="modal" id="edit-business-details">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Business Details</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form method="POST" id="businessdetailsmodel" action="{{ route('BusinessDetails') }}"
                          accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="vb_id" value="{{@$profile->vb_id}}">
                        <div class="form-group">
                            <label>Business Name</label>
                            <input type="text" name="vb_name" value="{{(@$profile->vb_name) ? @$profile->vb_name:''}}" class="form-control mt-1" placeholder="Ex: Company Name will be here">
                        </div>

                        <div class="form-group">
                            <label>TAN</label>
                            <input type="text" name="vb_tan" value="{{(@$profile->vb_tan) ? @$profile->vb_tan:''}}" class="form-control mt-1" placeholder="Tan Number">
                        </div>

                        <div class="form-group">
                            <label>GSTIN / Provisional ID Number</label>
                            <input type="text" name="vb_gst" value="{{(@$profile->vb_gst) ? @$profile->vb_gst:''}}" class="form-control mt-1" placeholder="GSTIN / Provisional ID Number">
                        </div>

                        <div class="form-group">
                            <label>Address Line 01</label>
                            <input type="text" name="vb_address1" value="{{(@$profile->vb_address1) ? @$profile->vb_address1:''}}" class="form-control mt-1" placeholder="Plot No:/ House No:">
                        </div>

                        <div class="form-group">
                            <label>Address Line 02</label>
                            <input type="text" name="vb_address2" value="{{(@$profile->vb_address2) ? @$profile->vb_address2:''}}" class="form-control mt-1" placeholder="Street Name / Colony Name">
                        </div>

                        <div class="form-group">
                            <label>State</label>
                            <select class="form-control" name="vb_state">
                                <option value="Telangana" {{ (!empty(old('vb_state')) && old('vb_state')=='Telangana')  ? 'selected' : (((@$profile) && (@$profile->vb_state == 'Telangana')) ? 'selected' : '') }}>Telangana</option>
                                <option value="Andhra Pradesh" {{ (!empty(old('vb_state')) && old('vb_state')=='Andhra Pradesh')  ? 'selected' : (((@$profile) && (@$profile->vb_state == 'Andhra Pradesh')) ? 'selected' : '') }}>Andhra Pradesh</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>City</label>
                            <select class="form-control" name="vb_city">
                                <option value="Hyderabad" {{ (!empty(old('vb_city')) && old('vb_city')=='Hyderabad')  ? 'selected' : (((@$profile) && (@$profile->vb_city == 'Hyderabad')) ? 'selected' : '') }}>Hyderabad</option>
                                <option value="Secunderabad" {{ (!empty(old('vb_city')) && old('vb_city')=='Secunderabad')  ? 'selected' : (((@$profile) && (@$profile->vb_city == 'Secunderabad')) ? 'selected' : '') }}>Secunderabad</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Pin Code</label>
                            <input type="text" id="vb_pincode" name="vb_pincode" size="6" maxlength="6" value="{{(@$profile->vb_pincode) ? @$profile->vb_pincode:''}}" class="form-control mt-1" placeholder="Enter Pin Number">
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="greenlink">Save</button>
                        </div>

                    </form>
                </div>
                <!-- Modal footer -->

                <!--/ modal footer -->
            </div>
        </div>
    </div>
    </div>
    <!--/ Modal for Edit Display Information-->

    <!-- Modal for Bank Information -->
    <div class="modal" id="bank-details">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Bank Details</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <small class="redcolor pb-2">Bank details can be updated a maximum of 5 times in a year. Please note that changing bank account will result in your upcoming payments being blocked until the new bank account is verified.</small>


                        <form method="POST" id="products" action="{{ route('BusinessDetails') }}"
                              accept-charset="UTF-8" class="form-horizontal pt-3" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="vb_id" value="{{@$profile->vb_id}}">
                        <div class="form-group">
                            <label>Account holder name</label>
                            <input type="text" name="vb_account_name" value="{{(@$profile->vb_account_name) ? @$profile->vb_account_name:''}}"  class="form-control mt-1" placeholder="Account Holder Name">
                        </div>

                        <div class="form-group">
                            <label>Account Number</label>
                            <input type="number" name="vb_account_number" value="{{(@$profile->vb_account_number) ? @$profile->vb_account_number:''}}" class="form-control mt-1" placeholder="Account Number">
                        </div>

                            <div class="form-group">
                                <label>Bank Name</label>
                                <input name="vb_bank" value="{{(@$profile->vb_bank) ? @$profile->vb_bank:''}}" type="text" class="form-control mt-1" placeholder="Bank Name">

                            </div>

                        <div class="form-group">
                            <label>IFSC Code</label>
                            <input type="text" name="vb_ifsc" value="{{(@$profile->vb_ifsc) ? @$profile->vb_ifsc:''}}" class="form-control mt-1" placeholder="Enter IFSC Code">
                        </div>





                            <div class="form-group">
                                <label>Branch</label>
                                <input name="vb_branch" value="{{(@$profile->vb_branch) ? @$profile->vb_branch:''}}" type="text" class="form-control mt-1" placeholder="Branch">

                            </div>



                            <div class="form-group">
                                <label>State</label>
                                <select class="form-control" name="vb_bank_state">
                                    <option value="Telangana" {{ (!empty(old('vb_bank_state')) && old('vb_bank_state')=='Telangana')  ? 'selected' : (((@$profile) && (@$profile->vb_bank_state == 'Telangana')) ? 'selected' : '') }}>Telangana</option>
                                    <option value="Andhra Pradesh" {{ (!empty(old('vb_bank_state')) && old('vb_bank_state')=='Andhra Pradesh')  ? 'selected' : (((@$profile) && (@$profile->vb_bank_state == 'Andhra Pradesh')) ? 'selected' : '') }}>Andhra Pradesh</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>City</label>
                                <select class="form-control" name="vb_bank_city">
                                    <option value="Hyderabad" {{ (!empty(old('vb_bank_city')) && old('vb_bank_city')=='Hyderabad')  ? 'selected' : (((@$profile) && (@$profile->vb_bank_city == 'Hyderabad')) ? 'selected' : '') }}>Hyderabad</option>
                                    <option value="Secunderabad" {{ (!empty(old('vb_bank_city')) && old('vb_bank_city')=='Secunderabad')  ? 'selected' : (((@$profile) && (@$profile->vb_bank_city == 'Secunderabad')) ? 'selected' : '') }}>Secunderabad</option>
                                </select>
                            </div>

                        <h4 class="border-bottom py-1 mb-4">KYC Documents</h4>

                        <div class="form-group">
                            <label>Business Type</label>
                            <select class="form-control" name="vb_type">
                                <option value="Private Limited Company" {{ (!empty(old('vb_type')) && old('vb_type')=='Private Limited Company')  ? 'selected' : (((@$profile) && (@$profile->vb_type == 'Private Limited Company')) ? 'selected' : '') }}>Private Limited Company</option>
                                <option value="Proprietor" {{ (!empty(old('vb_type')) && old('vb_type')=='Proprietor')  ? 'selected' : (((@$profile) && (@$profile->vb_type == 'Proprietor')) ? 'selected' : '') }}>Proprietor</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Personal Pan</label>
                            <input name="vb_pan" value="{{(@$profile->vb_pan) ? @$profile->vb_pan:''}}" type="text" class="form-control mt-1" placeholder="PAN Number">

                        </div>

                        <div class="form-group mt-4">
                            <label>Address proof</label>
                            <input type="file" name="vb_address_proof"  class="form-control mt-1" placeholder="Attach Address proof">
                            @if(isset($profile->vb_address_proof))
                                @php
                                    $scode='';
                                    $image= (@$profile->vb_address_proof) ? '<img src="/uploads/vendor/proofs/thumbs/'.@$profile->vb_address_proof.'" width="50px" />' : '-';
                                @endphp
                                @if($image!='-')
                                    <div class="imagediv">
                                        <a data-fancybox="" class="popupimages"
                                           href="{{url('/uploads/vendor/proofs/'.@$profile->vb_address_proof)}}">
                                            {!! $image !!}
                                        </a>
                                    </div>
                                @endif
                            @endif

                        </div>

                        <div class="form-group mt-4">
                            <label>Cancelled Cheque</label>
                            <input type="file" name="vb_cheque"  class="form-control mt-1" placeholder="Cancelled Cheque">
                            @if(isset($profile->vb_cheque))
                                @php
                                    $scode='';
                                    $image= (@$profile->vb_cheque) ? '<img src="/uploads/vendor/proofs/thumbs/'.@$profile->vb_cheque.'" width="50px" />' : '-';
                                @endphp
                                @if($image!='-')
                                    <div class="imagediv">
                                        <a data-fancybox="" class="popupimages"
                                           href="{{url('/uploads/vendor/proofs/'.@$profile->vb_cheque)}}">
                                            {!! $image !!}
                                        </a>
                                    </div>
                                @endif
                            @endif

                        </div>

                            <div class="modal-footer">
                                <button type="submit" class="greenlink">Save</button>
                            </div>

                    </form>
                </div>
                <!-- Modal footer -->

                <!--/ modal footer -->
            </div>
        </div>
    </div>
    </div>
    <!--/ Modal for Bank Information-->





@endsection


@section('footer_scripts')

    <script>
        $(function () {

            $('[id^=vb_pincode]').keypress(validateNumber);

            $('#businessdetailsmodel').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
// You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    vb_pincode: {
                        required: true,
                        number: true,
                        maxlength: 6
                    },
                },
                messages: {
                    vb_pincode: {
                        required: 'Please Enter Pincode',
                        maxlength: "Max 6 numbers only",
                        number: "Numbers only enter"
                    }
                },
            });






        });



        $(document).ready(function(){
            $('[class^=numbersonly]').keypress(validateNumber);
        });

        function validateNumber(event) {
            var key = window.event ? event.keyCode : event.which;
            if (event.keyCode === 8 || event.keyCode === 46) {
                return true;
            } else if ( key < 48 || key > 57 ) {
                return false;
            } else {
                return true;
            }
        };



    </script>

@endsection