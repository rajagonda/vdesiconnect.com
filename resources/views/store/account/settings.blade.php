@extends('layouts.vendor')


@section('content')
    @include('store.include.login_header')


    <!-- dashboard main -->
    <main class="dbmain">

        <!-- page content -->
        <div class="page-content d-flex align-items-stretch">
            <!-- content inner -->
            <div class="content-inner">
                <!-- page header -->
                <div class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">Profile Settings</h2>
                    </div>
                </div>
                <!--/ page header -->
                <!-- Breadcrumb-->
                <div class="breadcrumb-holder container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('sellerPreDashbord')}}">Home</a></li>
                        <li class="breadcrumb-item active">Profile Settings </li>
                    </ul>
                </div>
                <!--/ Breadcrumb-->
                <!-- page contentmain-->
                @if (Session::has('flash_message'))
                    <br/>
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ Session::get('flash_message' ) }}</strong>
                    </div>
                @endif
                <div class="content-main p-3">
                    <!-- container-fluid-->
                    <div class="container-fluid">
                        <!-- row -->
                        <div class="row paymentspage">

                            <!-- col-->
                            <div class="col-lg-4 col-md-6">
                                <div class="card p-4 mb-1 articles">
                                    <h4 class="h4 border-bottom py-2 mb-2">Login Details <span class="float-right"><a href="javascript:void(0)" data-toggle="modal" data-target="#change-password">Change Password</a></span></h4>

                                    <div class="article-in">
                                        <div class="item">
                                            <h3>Name</h3>
                                            <small>{{(@$userdetails->name) ? @$userdetails->name:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>Mobile Number</h3>
                                            <small>{{(@$userdetails->mobile) ? @$userdetails->mobile:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>Your Email address</h3>
                                            <small>{{(@$userdetails->email) ? @$userdetails->email:'--'}}</small>
                                        </div>

                                        <div class="item">
                                            <h3>Password</h3>
                                            <small>**********</small>
                                        </div>

                                        <br/>
                                        <br/>

                                      <span class="float-right"><a href="javascript:void(0)" data-toggle="modal" data-target="#edit-contact">Edit</a></span>


</div>
</div>
</div>
<!-- col-->

</div>
<!--/ row -->

</div>
<!-- container - fluid -->
</div>
<!--/ page content main-->
</div>
<!--/ content inner -->
</div>
<!--/ page content -->
</main>
<!--/ dashboard main -->




<!-- Modal for Business Details -->
<div class="modal" id="change-password">
<div class="modal-dialog">
<div class="modal-content">
<!-- Modal Header -->
<div class="modal-header">
<h4 class="modal-title">Change Password</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<!-- Modal body -->
<div class="modal-body">
<form method="POST" id="changePassword"
action="{{ route('vendorSettings') }}"
accept-charset="UTF-8" class="form-horizontal">
{{ csrf_field() }}
    <input type="hidden" name="id">
<div class="form-group">
<label>Enter old password</label>
<input type="password" id="password" name="password" value="{{ !empty(old('password')) ? old('password') : '' }}" placeholder="Enter Old Password" class="form-control">
@if ($errors->has('password'))
<span class="text-danger help-block">{{ $errors->first('password') }}</span>
@endif
</div>

<div class="form-group">
<label>Enter New password</label>
<input type="password" id="new_password" name="new_password" value="{{ !empty(old('new_password')) ? old('new_password') : '' }}" placeholder="Enter New password" class="form-control">
@if ($errors->has('new_password'))
<span class="text-danger help-block">{{ $errors->first('new_password') }}</span>
@endif
</div>

<div class="form-group">
<label>Confirm New password</label>
<input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password" value="{{ !empty(old('confirm_password')) ? old('confirm_password') : '' }}">
@if ($errors->has('confirm_password'))
<span class="text-danger help-block">{{ $errors->first('confirm_password') }}</span>
@endif
</div>

<div class="modal-footer">
<button type="submit" class="greenlink">Save</button>
</div>
</form>
</div>
<!-- Modal footer -->

<!--/ modal footer -->
</div>
</div>
</div>
</div>
<!--/ Modal for Edit Display Information-->


    <div class="modal" id="edit-contact">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Contact Details</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form method="POST" id="userdetails" action="{{ route('vendorSettings') }}"
                          accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{@$userdetails->id}}">
                        <div class="form-group">
                            <label>Your Name</label>
                            <input type="text" name="name" value="{{(@$userdetails->name) ? @$userdetails->name:''}}"
                                   class="form-control mt-1" placeholder="Name will be here">
                        </div>

                        <div class="form-group">
                            <label>Mobile Number</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                <span class="input-group-text">
                                        India
                                </span>
                                </div>
                                <?php
                                $mobilenum=explode('-',$userdetails->mobile);
                                ?>
                                <input type="text" name="mobile"
                                       value="{{(@$mobilenum[1]) ? @$mobilenum[1]:''}}"
                                       class="form-control mt-1" placeholder="Ex: 9642123254">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="text" name="email"
                                   value="{{(@$userdetails->email) ? @$userdetails->email:''}}" class="form-control mt-1"
                                   placeholder="Email">
                        </div>


                        <div class="modal-footer">
                            <button type="submit" class="greenlink">Save</button>
                        </div>
                    </form>
                </div>
                <!-- Modal footer -->

                <!--/ modal footer -->
            </div>
        </div>
    </div>


@endsection

@section('footer_scripts')

<script>
$(function () {

$('#changePassword').validate({
ignore: [],
errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
errorElement: 'div',
errorPlacement: function (error, e) {
e.parents('.form-group').append(error);
},
highlight: function (e) {
$(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
$(e).closest('.text-danger').remove();
},
success: function (e) {
// You can use the following if you would like to highlight with green color the input after successful validation!
e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
e.closest('.text-danger').remove();
},
rules: {
password: {
required: true
},
new_password: {
required: true,
maxlength: 12,
minlength: 6
},
confirm_password: {
required: true,
equalTo: "#new_password",
maxlength: 12,
minlength: 6
}
},
messages: {
password: {
required: 'Please enter password'
},
new_password: {
required: 'Please enter new password'
},
confirm_password: {
required: 'Please confirm password',
equalTo: "The confirm password and new password must match."
}
},
});


    $('#userdetails').validate({
        ignore: [],
        errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function (error, e) {
            e.parents('.form-group').append(error);
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.text-danger').remove();
        },
        success: function (e) {
// You can use the following if you would like to highlight with green color the input after successful validation!
            e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            e.closest('.text-danger').remove();
        },
        rules: {
            name: {
                required: true
            },
            mobile: {
                required: true,
                number:true
            },
            email: {
                required: true,
            }
        },
        messages: {
            name: {
                required: 'Please enter Name'
            },
            mobile: {
                required: 'Please enter Mobile',
                number: 'Numbers only'
            },
            email: {
                required: 'Please enter Email',
            }
        },
    });


});

</script>

@endsection