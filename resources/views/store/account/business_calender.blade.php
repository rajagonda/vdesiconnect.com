@extends('layouts.vendor')


@section('content')

    @include('store.include.login_header')


    <!-- dashboard main -->
    <main class="dbmain">

        <!-- page content -->
        <div class="page-content d-flex align-items-stretch">
            <!-- content inner -->
            <div class="content-inner">
                <!-- page header -->
                <div class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">Business Calendar</h2>
                    </div>
                </div>
                <!--/ page header -->
                <!-- Breadcrumb-->
                <div class="breadcrumb-holder container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="db-home.php">Home</a></li>
                        <li class="breadcrumb-item active">Business Calendar </li>
                    </ul>
                </div>
                <!--/ Breadcrumb-->
                <!-- page contentmain-->
                <div class="content-main p-3">
                    <!-- container-fluid-->
                    <div class="container-fluid">
                        <!-- row -->
                        <div class="row paymentspage">

                            <!-- col-->
                            <div class="col-lg-4">
                                <div class="card p-4 mb-1 articles">
                                    <h4 class="h4 border-bottom py-2 mb-2">Business Calendar <span class="float-right"><a href="javascript:void(0)" data-toggle="modal" data-target="#edit-business-calender">Edit</a></span></h4>

                                    <div class="article-in">
                                        <div class="item">
                                            <h3>Working Time</h3>
                                            <small>11:00 AM to 07:00 PM</small>
                                        </div>

                                        <div class="item">
                                            <h3>Weekly Off</h3>
                                            <small>Sunday</small>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- col-->

                        </div>
                        <!--/ row -->

                    </div>
                    <!-- container - fluid -->
                </div>
                <!--/ page content main-->
            </div>
            <!--/ content inner -->
        </div>
        <!--/ page content -->
    </main>
    <!--/ dashboard main -->



    <!-- Modal for Business Details -->
    <div class="modal" id="edit-business-calender">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Business Calendar</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label>Working Hours From</label>
                            <div class="d-flex">
                                <select class="form-control">
                                    <option>01:00</option>
                                    <option>01:30</option>
                                    <option>02:00</option>
                                </select>
                                <select class="form-control mx-2">
                                    <option>AM</option>
                                    <option>PM</option>
                                </select>
                                <span class="align-self-center px-2">to</span>
                                <select class="form-control">
                                    <option>01:00</option>
                                    <option>01:30</option>
                                    <option>02:00</option>
                                </select>
                                <select class="form-control mx-2">
                                    <option>AM</option>
                                    <option>PM</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Weekly off</label>
                            <select class="form-control">
                                <option>Sunday</option>
                                <option>Monday</option>
                                <option>Tuesday</option>
                                <option>Wednesday</option>
                                <option>Thursday</option>
                                <option>Friday</option>
                                <option>Saturday</option>
                            </select>
                        </div>

                    </form>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="greenlink">Save</button>
                </div>
                <!--/ modal footer -->
            </div>
        </div>
    </div>
    </div>
    <!--/ Modal for Edit Display Information-->





@endsection