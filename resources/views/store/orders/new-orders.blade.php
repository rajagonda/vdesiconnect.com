@extends('layouts.vendor')


@section('content')

    @include('store.include.login_header')



    <!-- dashboard main -->
    <main class="dbmain">

        <!-- page content -->
        <div class="page-content d-flex align-items-stretch">
            <!-- content inner -->
            <div class="content-inner">
                <!-- page header -->
                <div class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">My Orders</h2>
                    </div>
                </div>
                <!--/ page header -->
                <!-- Breadcrumb-->
                <div class="breadcrumb-holder container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('sellerPreDashbord')}}">Home</a></li>
                        <li class="breadcrumb-item active">My Orders </li>
                    </ul>
                </div>
                <!--/ Breadcrumb-->
                <!-- page contentmain-->
                <div class="content-main p-3">

                    <!-- container-fluid-->
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card listcard">



                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Reference Number</th>
                                            <th scope="col">Address</th>
                                            <th scope="col">Total</th>
                                            <th scope="col">Date</th>
                                            <th scope="col">Payment Status</th>
                                            <th scope="col">Order Status</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($orders)>0)
                                            @foreach($orders as $item)
                                                @php
                                                    $address=unserialize($item->getOrder->order_delivery_address);
                                                @endphp
                                                <tr>
                                                    <th scope="row">{{ $loop->iteration }}</th>
                                                    <td>{{ $item->getOrder->order_reference_number }}</td>
                                                    <td>{{ $address['ua_name'] }},{{ $address['ua_address'] }}
                                                        ,{{ $address['ua_city'] }},{{ $address['ua_state'] }}
                                                        ,{{ $address['ua_country'] }}</td>
                                                    <td><i class="fa fa-inr"></i>{{ $item->oitem_sub_total }}</td>
                                                    <td>{{ \Carbon\Carbon::parse($item->getOrder->created_at)->format('d/m/Y
                                            H:i:s')
                                            }}</td>
                                                    <td>{{ $item->getOrder->order_status }}</td>
                                                    <td>{{ $item->getOrder->order_status }}</td>
                                                    <td>

                                                        <a class="dropdown-item"
                                                           href="{{ route('sellerOrdersNewListView',['id'=>$item->oitem_id]) }}"><i
                                                                    class="fa fa-eye"></i> View</a>

                                                    </td>

                                                </tr>



                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="8">No records found</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                                                 aria-live="polite">Showing {{ $orders->firstItem() }}
                                                to {{ $orders->lastItem() }} of {{ $orders->total() }} entries
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-right">
                                            <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                                                {!! $orders->appends(['filters' => Request::get('filters'),'search' => Request::get('search')])->render() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ row -->
                    </div>
                    <!-- container - fluid -->

                </div>
                <!--/ page content main-->
            </div>
            <!--/ content inner -->
        </div>
        <!--/ page content -->
    </main>
    <!--/ dashboard main -->

@endsection