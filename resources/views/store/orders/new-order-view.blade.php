@extends('layouts.vendor')

@section('header_styles')


    <style type="text/css">


        ol, ul {
            list-style: none;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
        }

        caption, th, td {
            text-align: left;
            font-weight: normal;
            vertical-align: middle;
        }

        q, blockquote {
            quotes: none;
        }

        q:before, q:after, blockquote:before, blockquote:after {
            content: "";
            content: none;
        }

        a img {
            border: none;
        }

        article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary {
            display: block;
        }

        body {
            font-family: 'Poppins', sans-serif;
            font-weight: 300;
            font-size: 13px;
            margin: 0;
            padding: 0;
        }

        body a {
            text-decoration: none;
            color: inherit;
        }

        body a:hover {
            color: inherit;
            opacity: 0.7;
        }

        body .container {
            /*width: 900px;*/
            margin: 0 auto;
            padding: 0 20px;
        }

        body .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        body .left {
            float: left;
        }

        body .right {
            float: right;
        }

        body .helper {
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }

        body .no-break {
            page-break-inside: avoid;
        }

        header {
            margin-top: 20px;
            margin-bottom: 50px;
        }

        header figure {
            float: left;
            width: 60px;
            height: 60px;
            margin-right: 15px;
            text-align: center;
        }

        header .company-address {
            float: left;
            max-width: 150px;
            line-height: 1.7em;
        }

        header .company-address .title {
            color: #8BC34A;
            font-weight: 400;
            font-size: 1.5em;
            text-transform: uppercase;
        }

        header .company-contact {
            float: right;
            height: 60px;
            padding: 0 10px;
            background-color: #8BC34A;
            color: white;
        }

        header .company-contact span {
            display: inline-block;
            vertical-align: middle;
        }

        header .company-contact .circle {
            width: 20px;
            height: 20px;
            background-color: white;
            border-radius: 50%;
            text-align: center;
        }

        header .company-contact .circle img {
            vertical-align: middle;
        }

        header .company-contact .phone {
            height: 100%;
            margin-right: 20px;
        }

        header .company-contact .email {
            height: 100%;
            min-width: 100px;
            text-align: right;
        }

        .date {
            line-height: 20px;
        }

        section .details {
            margin-bottom: 55px;
        }

        section .details .client {
            width: 50%;
            line-height: 20px;
        }

        section .details .client .name {
            color: #8BC34A;
        }

        section .details .data {
            width: 50%;
            text-align: right;
        }

        section .details .title {
            margin-bottom: 15px;
            color: #8BC34A;
            font-size: 3em;
            font-weight: 400;
            text-transform: uppercase;
        }

        section table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;

        }

        section table .qty, section table .unit, section table .total {
            width: 15%;
        }

        section table .desc {
            width: 55%;
        }

        section table thead {
            display: table-header-group;
            vertical-align: middle;
            border-color: inherit;
        }

        section table thead th {
            padding: 5px 10px;
            background: #8BC34A;
            border-bottom: 5px solid #FFFFFF;
            border-right: 4px solid #FFFFFF;
            text-align: right;
            color: white;
            font-weight: 400;
            text-transform: uppercase;
        }

        section table thead th:last-child {
            border-right: none;
        }

        section table thead .desc {
            text-align: left;
        }

        section table thead .qty {
            text-align: center;
        }

        section table tbody td {
            padding: 10px;
            background: #E8F3DB;
            color: #000;
            text-align: right;
            border-bottom: 5px solid #FFFFFF;
            border-right: 4px solid #E8F3DB;
        }

        section table tbody td:last-child {
            border-right: none;
        }

        section table tbody h3 {
            margin-bottom: 5px;
            color: #8BC34A;
            font-weight: 600;
            font-size: 15px;
        }

        section table tbody .desc {
            text-align: left;
            line-height: 20px;
            font-size: 13px;
        }

        section table tbody .qty {
            text-align: center;
        }

        section table.grand-total {
            margin-bottom: 45px;
        }

        section table.grand-total td {
            padding: 5px 10px;
            border: none;
            color: #777777;
            text-align: right;
            line-height: 25px;
        }

        section table.grand-total .desc {
            background-color: transparent;
        }

        section table.grand-total tr:last-child td {
            font-weight: 600;
            color: #8BC34A;
            font-size: 1.18181818181818em;
        }

        footer {
            margin-bottom: 20px;
        }

        footer .thanks {
            margin-bottom: 40px;
            color: #8BC34A;
            font-size: 1.16666666666667em;
            font-weight: 600;
        }

        footer .notice {
            margin-bottom: 25px;
            line-height: 22px;
        }

        footer .end {
            padding-top: 5px;
            border-top: 2px solid #8BC34A;
            text-align: center;
        }

    </style>

    <link href="//cdn.kendostatic.com/2013.1.319/styles/kendo.common.min.css" rel="stylesheet"/>
    <link href="//cdn.kendostatic.com/2013.1.319/styles/kendo.default.min.css" rel="stylesheet"/>

@endsection


@section('content')

    @include('store.include.login_header')



    <!-- dashboard main -->
    <main class="dbmain">

        <!-- page content -->
        <div class="page-content d-flex align-items-stretch">
            <!-- content inner -->
            <div class="content-inner">
                <!-- page header -->
                <div class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">Order View</h2>
                    </div>
                </div>
                <!--/ page header -->
                <!-- Breadcrumb-->
                <div class="breadcrumb-holder container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('sellerPreDashbord')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('sellerOrdersNewList')}}">My Orders</a></li>
                        <li class="breadcrumb-item active">Order View </li>
                    </ul>
                </div>
                <!--/ Breadcrumb-->
                <!-- Forms Section-->
                <!-- page content main-->
                <div class="content-main p-3">
                    <!-- container-fluid-->
                    <div class="container-fluid">
                        <!-- row -->
                        <div class="row pb-3">
                            <div class="col-lg-12 text-right">
                                <input type="button" value="Back to Orders" class="greenlink" onclick="window.location.href='{{route('sellerOrdersNewList')}}'">
                            </div>
                        </div>
                        <!--/ row -->
                        <!-- row -->
                        <div class="row">
                            <div class="col-lg-12">
                                <?php
                                $order_currency = $orders->order_currency;
//                              dump($orders);
                                ?>
                                @if (Session::has('flash_message'))
                                    <br/>
                                    <div class="alert alert-success alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>{{ Session::get('flash_message' ) }}</strong>
                                    </div>
                                @endif
                                <div class="card">
                                    <div class="card-header">
                                        <strong class="card-title">Order Details</strong>

                                        <form method="post" id="editServices"
                                              action="{{ route('vendorStatusUpdateByVendor',['id'=>$orders->oitem_id]) }}"
                                              class="form-horizontal editServices"
                                              autocomplete="off"
                                              enctype="multipart/form-data">
                                            {{ csrf_field() }}


                                            <input type="hidden" name="oitem_id"
                                                   value="{{$orders->oitem_id}}"/>

                                            <div class="row">

                                                <div class="col-4  ">

                                                    <div class="form-group">
                                                        <label for="hf-email"
                                                               class=" form-control-label">Status Update</label>
                                                        <select class="form-control"
                                                                id="oitem_status"
                                                                type="text"
                                                                placeholder="Contact Person"
                                                                value=""
                                                                name="oitem_status">
                                                            <option value="">--Select Status</option>
                                                            <option value="Paid" {{ isset($orders) && ($orders->oitem_status == 'Paid') ? 'selected' : ''}}>Paid</option>
                                                            <option value="Shipped" {{ isset($orders) && ($orders->oitem_status == 'Shipped') ? 'selected' : ''}}>Shipped</option>
                                                            <option value="OutForDelivery" {{ isset($orders) && ($orders->oitem_status == 'OutForDelivery') ? 'selected' : ''}}>Out for delivery</option>
                                                            <option value="Delivered" {{ isset($orders) && ($orders->oitem_status == 'Delivered') ? 'selected' : ''}}>Delivered</option>
                                                        </select>

                                                    </div>
                                                </div>
                                                <input type="submit"
                                                       class="btn btn-primary"
                                                       value="Update"/>

                                            </div>



                                        </form>

                                        <div class="btn-group" role="group" aria-label="Basic example" style="float: right;">
                                            <a href="#" class="btn btn-secondary {{ isset($orders) && ($orders->oitem_status ==
                                'Paid') ? 'active' : ''}}">Paid</a>
                                            <a href="#" class="btn btn-secondary {{ isset($orders) && ($orders->oitem_status == 'Shipped') ? 'active' : ''}}">Shipped</a>
                                            <a href="#" class="btn btn-secondary {{ isset($orders) && ($orders->oitem_status == 'OutForDelivery') ? 'active' : ''}}">Out for delivery</a>
                                            <a href="#" class="btn btn-secondary {{ isset($orders) && ($orders->oitem_status == 'Delivered') ? 'active' : ''}}">Delivered</a>
                                        </div>

                                    </div>
                                    <div class="card-body table-responsive">


<!--                                        --><?php
//                                        dump($orders);
//                                        ?>


                                        <section>
                                            <div class="container">
                                                <div class="details clearfix row">
                                                    <div class="client left col-md-4">
                                                        <?php
                                                        $userAddress = unserialize($orders->getOrder->order_delivery_address);

//                                                                                                                    dd($userAddress);
                                                        ?>

                                                        <p>Shipping TO:</p>
                                                        <p class="name">
                                                            <strong style="font-size:16px;">
                                                                {{--{!!  !!}--}}
                                                                {{$userAddress['ua_name']}},
                                                            </strong>
                                                        </p>
                                                        <p>
                                                            {{--Plot No:91, Ganapathi Nivas, Allwyn colony 1, Hyderabad--}}



                                                            {{$userAddress['ua_address']}},
                                                            {{$userAddress['ua_landmark']}},
                                                            @if(isset($userAddress['ua_city']))
                                                            {{isset($userAddress['ua_city'])?$userAddress['ua_city']:''}},
                                                            @endif
                                                            {{$userAddress['ua_state']}},
                                                            {{$userAddress['ua_country']}},
                                                            {{$userAddress['ua_pincode']}},
                                                            {{$userAddress['ua_phone']}},

                                                        </p>
                                                        <a href="mailto:{{$userAddress['ua_email']}}">
                                                            {{$userAddress['ua_email']}}
                                                        </a>
                                                    </div>
                                                    <div class="  col-md-4">

                                                        <?php
                                                        $userAddressBilling = unserialize($orders->getOrder->order_billing_address);

//                                                                                                                    dd($userAddressBilling);
                                                        ?>

                                                        <p>Billing Adress:</p>
                                                        <p class="name">
                                                            <strong style="font-size:16px;">
                                                                {{--{!!  !!}--}}
                                                                {{$userAddressBilling['ua_name']}},
                                                            </strong>
                                                        </p>
                                                        <p>
                                                            {{--Plot No:91, Ganapathi Nivas, Allwyn colony 1, Hyderabad--}}



                                                            {{$userAddressBilling['ua_address']}},
                                                            @if(isset($userAddressBilling['ua_landmark']))
                                                                {{isset($userAddressBilling['ua_landmark'])?$userAddressBilling['ua_landmark']:''}},
                                                            @endif
                                                            @if(isset($userAddressBilling['ua_city']))
                                                                {{isset($userAddressBilling['ua_city'])?$userAddressBilling['ua_city']:''}},
                                                            @endif
                                                            {{$userAddressBilling['ua_state']}},
                                                            {{$userAddressBilling['ua_country']}},
                                                            {{$userAddressBilling['ua_pincode']}},
                                                            {{$userAddressBilling['ua_phone']}},

                                                        </p>
                                                            @if(isset($userAddressBilling['ua_email']))
                                                                <a href="mailto:{{$userAddressBilling['ua_email']}}">
                                                                    {{$userAddressBilling['ua_email']}}
                                                                </a>
                                                            @endif




                                                    </div>

                                                    <div class="  col-md-4">
                                                        {{--<div class="title">Invoice</div>--}}
                                                        <div class="date">
                                                            Date of Order: {{$orders->getOrder->created_at}}<br>
                                                            {{--Due Date: 30/06/2014--}}
                                                        </div>
                                                    </div>
                                                </div>

                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <thead>
                                                    <tr>
                                                        <th class="desc">Image</th>
                                                        <th class="desc">Product Name</th>
                                                        <th class="qty">Quantity</th>
                                                        <th class="unit">Price</th>
                                                        <th class="unit">Delevery Charge</th>
                                                        <th class="total">Total</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $subtotal = array();

                                                    ?>

                                                    {{--                                        @if (count($orders->orderItems)> 0)--}}
                                                    {{--                                            @foreach($orders->orderItems AS $items)--}}

                                                    <?php
                                                    //                                                                dump($items);
                                                    ?>

                                                    <tr>
                                                        <td class="desc"> <?php
                                                            if (isset($orders->getProduct->productImages[0])) {
                                                                $item_img = url('/uploads/products/' . $orders->getProduct->productImages[0]->pi_image_name);

                                                            } else {
                                                                $item_img = '/frontend/images/no-image.png';
                                                            }


                                                            ?>
                                                            <img src="{{$item_img}}" alt="" title="" class="img-fluid"
                                                                 width="150px">
                                                        </td>
                                                        <td class="desc">
                                                            <h6>
                                                                {{ $orders->oitem_product_name  }}

                                                                {{--Service of Medical Supporting--}}
                                                            </h6>

                                                            <p>Gift Message: <small>{{ $orders->oitem_message }}</small></p>

                                                            <p>
                                                                <b>Name On The gift:</b>
                                                                {{ $orders->oitem_nameongift  }}
                                                            </p>

                                                            <?php

//                                                            dump($orders);
                                                            ?>


                                                            @if (isset($orders->oitem_message) && $orders->oitem_message != '')

                                                                <p class="py-3">
                                                                    <b>
                                                                        User Gift Message.

                                                                    </b> <br />
                                                                    {{ $orders->oitem_message }}
                                                                    {{--                                                        {{  (isset($orderItems->oitem_delivery_date))?\Carbon\Carbon::parse($orderItems->oitem_delivery_date)->add('3 days')->format('d-m-Y') :'' }}--}}
                                                                </p>

                                                            @endif


                                                            @if (isset($orders->oitem_delivery_date) && $orders->oitem_delivery_date != ''
                                                            || isset($orders->oitem_delivery_time) && $orders->oitem_delivery_time != '')

                                                                <p class="py-3">
                                                                    <b>
                                                                        User Request Delivery time.

                                                                    </b> <br />
                                                                    {{ $orders->oitem_delivery_date }}
                                                                    {{ $orders->oitem_delivery_time }}

                                                                    {{--                                                        {{  (isset($orderItems->oitem_delivery_date))?\Carbon\Carbon::parse($orderItems->oitem_delivery_date)->add('3 days')->format('d-m-Y') :'' }}--}}
                                                                </p>

                                                            @endif

                                                        </td>
                                                        <td class="qty">
                                                            {{ $orders->oitem_qty  }}</td>
                                                        <td class="unit">
                                                            {!! currencySymbol($order_currency)  !!}
                                                            {{  $orders->oitem_product_price }}

                                                        </td>

                                                        <td class="no-break">



                                                            {!! currencySymbol($order_currency)  !!}
                                                            {{ $orders->oitem_delivery_charge  }}


                                                        </td>

                                                        <td class="no-break">


                                                            <?php
                                                            $subtotalItem = $orders->oitem_sub_total + $orders->oitem_delivery_charge;

                                                            $subtotal[] = $subtotalItem;

                                                            ?>
                                                                {!! currencySymbol($order_currency)  !!}
                                                            {{ $subtotalItem  }}


                                                        </td>
                                                    </tr>
                                                    {{--@endforeach--}}
                                                    {{--@endif--}}


                                                    </tbody>
                                                </table>
                                                <div class="no-break">
                                                    <table class="grand-total">
                                                        <tbody>
                                                        <tr>
                                                            <td class="desc"></td>
                                                            <td class="qty"></td>
                                                            <td class="unit">SUBTOTAL:</td>
                                                            <td class="total">

                                                                {!! currencySymbol($order_currency)  !!}

                                                                {{ array_sum($subtotal)   }}

                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="desc"></td>
                                                            <td class="qty"></td>
                                                            <td class="unit">
                                                                TAX {{$orders->s_tax}}%:
                                                            </td>
                                                            <td class="total">
                                                                <?php
                                                                $taxAmount = array_sum($subtotal) / 100 * $orders->s_tax;
                                                                ?>
                                                                    {!! currencySymbol($order_currency)  !!}
                                                                {{ $taxAmount   }}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="desc"></td>
                                                            <td class="unit" colspan="2">GRAND TOTAL:</td>
                                                            <td class="total">
                                                                <?php
                                                                $total = array_sum($subtotal) + $taxAmount;
                                                                ?>
                                                                    {!! currencySymbol($order_currency)  !!}
                                                                {{ $total   }}
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </section>




                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--/ row -->


                    </div>
                    <!-- container - fluid -->
                </div>
                <!--/ page content main-->
            </div>
            <!--/ content inner -->
        </div>
        <!--/ page content -->
    </main>
    <!--/ dashboard main -->


@endsection