@extends('layouts.vendor')


@section('content')

    @include('store.include.header')

    <!-- main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="flight">Benefits</h1>
                        <p>Get the latest insights and trends on selling Gifts online.</p>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page main -->
        <div class="subpage-main">
            <div class="homehighlets py-0">
                <div class="container">

                    <!-- row -->
                    <div class="row">
                        <!-- col --->
                        <div class="col-lg-6">
                            <img src="/vendors/images/benefitsimg01.jpg" class="img-fluid" alt="">
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 align-self-center">
                            <h2>Expertise involved with smart way of selling</h2>
                            <p class="text-justify pb-2">Customers perpetually look into the products those holds enhanced quality and genuine product reviews. We desire to manage our website embedded with absolute reviews and ratings collected from various customers. This holds the trust of offering utmost quality of products which turns temporary customers into permanent customers.</p>
                            <h2>Captivating Storefront</h2>
                            <p class="text-justify pb-2">A thought to set up online store might be so easy, but the convoluted section is involved in its implementation. Vdesiconnect reaches to you with intuitive dashboard that allows customers to move ahead with smart cataloguing. Every product is as close as you feel the product is in hand. We present every product with attractive images, detailed descriptions and easy sort out facilities where these all take hold of the attention of customers.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row py-5">
                        <!-- col --->
                        <div class="col-lg-6 order-lg-last">
                            <img src="/vendors/images/benefitsimg02.jpg" class="img-fluid" alt="">
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 align-self-center">
                            <h2>Extended collaboration with multiple catalogue partners</h2>
                            <p class="text-justify pb-2">Partnering up various catalogue partners assists in the implementation and development of alluring product catalogues. Think on the marketing scenarios for your website where the rest to be managed by us. We strive to provide superior product catalogues those turn the sight of customer to visit our website.</p>
                            <h2>Hassle free pick-up and delivery services</h2>
                            <p class="text-justify pb-2">Vdesiconnect ensures to deliver your products as per the scheduled dates. With the assistance of professional logistics networks, we set out to deliver your quickly and safely. We move through streamlined order fulfilments running around 200 pick-up locations and 10,000+ delivery resources.</p>

                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row">
                        <!-- col --->
                        <div class="col-lg-6">
                            <img src="/vendors/images/benefitsimg03.jpg" class="img-fluid" alt="">
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 align-self-center">
                            <h2>Managed Products</h2>
                            <p class="text-justify pb-2">Organize your whole inventory with Vdesiconnect. We let you to search for extensive number of products those are categorized as type, size, colour, brand and many other. With a branded site, you can explore through multiple products where every product its own detailed description, features and specifications. </p>
                            <h2>Enhanced customer reach</h2>
                            <p class="text-justify pb-2">Get your products to be advertised in our couch and we take it to the sight of customers with our own marketing tactics. We go with successful marketing strategies those keep up with surge of customers. Involve your customers and reassure sales with the option to leave SEO-friendly product reviews and feedbacks on your website. </p>

                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row py-5">
                        <!-- col --->
                        <div class="col-lg-6 order-lg-last">
                            <img src="/vendors/images/benefitsimg04.jpg" class="img-fluid" alt="">
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 align-self-center">
                            <h2>Stay with augmented businesses</h2>
                            <p class="text-justify pb-2">At Vdesiconnect, we assure you maximal returns with less investments. As because Vdesiconnect Fulfilment service provides make use of our state-of-the-art fulfilment centres at minimal prices. Get your products to be stored in our fulfilment centres and we hold the responsibility to take care of your whole inventory.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

                </div>
            </div>
        </div>
        <!--/ sub page main-->

    </main>
    <!--/ main -->


@endsection