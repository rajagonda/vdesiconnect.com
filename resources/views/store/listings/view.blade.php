@extends('layouts.vendor')


@section('content')

    @include('store.include.login_header')

    <!-- dashboard main -->
    <main class="dbmain">

        <!-- page content -->
        <div class="page-content d-flex align-items-stretch">
            <!-- content inner -->
            <div class="content-inner">
                <!-- page header -->
                <div class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">{{$product->p_name}}</h2>
                    </div>
                </div>
                <!--/ page header -->
                <!-- Breadcrumb-->
                <div class="breadcrumb-holder container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('sellerPreDashbord')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('seller_listings')}}">Listings</a></li>
                        <li class="breadcrumb-item active">{{$product->p_name}}</li>
                    </ul>
                </div>
                <!--/ Breadcrumb-->
                <!-- Forms Section-->
                <!-- page content main-->
                <div class="content-main p-3">
                    <!-- container-fluid-->
                    <div class="container-fluid">
                        <!-- row -->
                        <div class="row pb-3">
                            <div class="col-lg-12 text-right">
                                <input type="button" value="Edit Product" class="greenlink"
                                       onclick="window.location.href='{{route('seller_new_listings',['id'=>$product->p_id])}}'">
                                <input type="button" value="Back to listing" class="whitelink"
                                       onclick="window.location.href='{{route('seller_listings')}}'">
                            </div>
                        </div>
                        <!--/ row -->
                        <!-- row -->
                        <div class="row">
                            <!-- col left -->
                            <div class="col-lg-6">
                                <!-- card -->
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="border-bottom py-3">Product Details</h5>
                                        <!-- row -->

                                        <div class="row py-2">
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Product Name</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <p class="text-justify">{{$product->p_name}}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>

                                        <div class="row py-2">
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Product Overview</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <p class="text-justify">{!! $product->p_overview !!}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Image Galleery</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <!-- row image gallery -->
                                                <div class="row images-listview">
                                                    @if(count($product_images)>0)
                                                        @foreach($product_images as $item)
                                                            <div class="col-md-2 px-1 position-relative imagediv_{{ $item->pi_id }}">
                                                                <img class="img-fluid imagecover position-relative"
                                                                     height="150"
                                                                     src="/uploads/products/thumbs/{{ $item->pi_image_name }}"/>

                                                                {{--<a href="#" id="{{$item->pi_id}}"--}}
                                                                   {{--data-token="{{ csrf_token() }}"--}}
                                                                   {{--class="delete_product_image btn btn-danger btn-xs position-absolute"--}}
                                                                   {{--onclick="return confirm('Confirm delete?')"><i  class="fa fa-trash" aria-hidden="true"></i> </a>--}}

                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <div class="col-md-2">
                                                            No Images found
                                                        </div>
                                                    @endif
                                                </div>


                                                <!--/ row iamge gallery -->
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="border-bottom py-3">Product Specifications</h5>
                                        <!-- row -->

                                        <div class="row py-2">

                                            <div class="col-lg-12">
                                                <p class="text-justify"> <p>{!!  $product->p_specifications !!}</p></p>
                                            </div>
                                            <!--/ col -->
                                        </div>

                                    </div>
                                </div>
                                <!--/ card -->
                            </div>
                            <!--/ col left -->
                            <!-- col right -->
                            <div class="col-lg-6">
                                <!-- card -->
                                <div class="card">
                                    <!-- card body -->
                                    <div class="card-body">
                                        <h5 class="border-bottom py-3">Product primary Details</h5>

                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Product Type</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p>{{ (isset($product->getSubCategory->category_name))? $product->getSubCategory->category_name:'' }}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Product Spl</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p>{{ (isset($product->getSplCat->category_name))? $product->getSplCat->category_name:'' }}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2">


                                            @switch($product->p_cat_id)
                                                @case(1)
                                                <div class="col-lg-3 form-control-label">Dishtype</div>
                                                <div class="col-lg-9"><p>{{$product->p_dishtype}}</p></div>

                                                <div class="col-lg-3 form-control-label">Weight</div>
                                                <div class="col-lg-9">
                                                    <p> {{$product->p_weight}} {{$product->p_weight_type}}</p></div>

                                                @break
                                                @case(2)

                                                <div class="col-lg-3 form-control-label">Color</div>
                                                <div class="col-lg-9"><p>{{$product->p_color}}</p></div>

                                                @break
                                                @case(3)

                                                @break
                                                @case(4)

                                                <div class="col-lg-3 form-control-label">Weight</div>
                                                <div class="col-lg-9">
                                                    <p>{{$product->p_weight}} {{$product->p_weight_type}}</p></div>

                                                @break
                                                @case(5)

                                                <div class="col-lg-3 form-control-label">Weight</div>
                                                <div class="col-lg-9">
                                                    <p>{{$product->p_weight}} {{$product->p_weight_type}}</p></div>

                                                @break
                                                @case(6)
                                                <div class="col-lg-3 form-control-label">Weight</div>
                                                <div class="col-lg-9">
                                                    <p>{{$product->p_weight}} {{$product->p_weight_type}}</p></div>
                                                @break
                                                @case(7)
                                                7
                                                @break
                                                @case(8)
                                                8
                                                @break
                                            @endswitch

                                        </div>
                                        <!--/ row -->


                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Availability</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p>{{ ($product->p_availability=='in_stock') ? 'In Stock':'Out of Stock'}}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->



                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Quality Information</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p>{!!  $product->p_quality_care_info !!}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->


                                    </div>
                                    <!--/ card body -->
                                </div>
                                <!--/ card -->
                            </div>



                            <!--/ col right -->


                        </div>
                        <!--/ row -->
                        <div class="row">
                            <!-- col right -->
                            <div class="col-md-6">
                                <!-- card -->
                                <div class="card">
                                    <!-- card body -->
                                    <div class="card-body">
                                        <h5 class="border-bottom py-3">Product SKUs</h5>

                                        <table class="table table-breakpoint">
                                            <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Type</th>
                                                <th>Option</th>
                                                <th>Value</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(isset($product->productSKUs))
                                                @if(count($product->productSKUs)>0)
                                                    @foreach($product->productSKUs as $options)
                                                        <tr>
                                                            <td>{{ $loop->iteration }}</td>
                                                            <td>{{ $options->sku_type }}</td>
                                                            <td>{{ $options->sku_option }}</td>
                                                            <td> {!! currencySymbol('INR') !!} {{ $options->sku_vendor_price }}</td>

                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                    <td colspan="4">No SKUs Found</td>

                                                    </tr>
                                                @endif
                                            @else
                                                <tr>
                                                    <td colspan="4">No SKUs Found</td>

                                                </tr>
                                            @endif
                                            </tbody>

                                        </table>


                                    </div>
                                    <!--/ card body -->
                                </div>
                                <!--/ card -->
                            </div>
                            <!--/ col right -->
                        </div>


                    </div>
                    <!-- container - fluid -->
                </div>
                <!--/ page content main-->
            </div>
            <!--/ content inner -->
        </div>
        <!--/ page content -->
    </main>
    <!--/ dashboard main -->

@endsection

@section('footer_scripts')


    @include('store.listings.new_scripts')




@endsection

