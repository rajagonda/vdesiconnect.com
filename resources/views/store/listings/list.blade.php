@extends('layouts.vendor')


@section('content')

    @include('store.include.login_header')



    <!-- dashboard main -->
    <main class="dbmain">

        <!-- page content -->
        <div class="page-content d-flex align-items-stretch">
            <!-- content inner -->
            <div class="content-inner">
                <!-- page header -->
                <div class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">Listing Management</h2>
                    </div>
                </div>
                <!--/ page header -->
                <!-- Breadcrumb-->
                <div class="breadcrumb-holder container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('sellerPreDashbord')}}">Home</a></li>
                        <li class="breadcrumb-item active">All Listings</li>
                    </ul>
                </div>
                <!--/ Breadcrumb-->
                <!-- page contentmain-->
                <div class="content-main p-3">

                    <!-- container-fluid-->
                    <div class="container-fluid">
                        <!-- page header -->
                        <div class="card p-2 mb-1">
                            <div class="container-fluid">
                                <div class="row">
                                    <!-- col -->
                                    <div class="col-md-8">
                                    <form method="GET" action="{{ route('seller_listings') }}" accept-charset="UTF-8"
                                          class="navbar-form navbar-right w-100" role="search" autocomplete="off">
                                        <input type="hidden" name="search" value="search">
                                        <div class="row">                                            &nbsp;&nbsp;
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-3 col-6">
                                                        <input type="text" class="form-control" name="product_name"  placeholder="Enter Product Name to Search..." value="{{ request('product_name') }}">
                                                    </div>
                                                    <div class="col-md-3 col-6">
                                                        <select name="search_category" id="search_category"
                                                                class="form-control">
                                                            <option value="">Select Category</option>
                                                            <?php
                                                            $categories = getCategories(); ?>
                                                            @foreach($categories as $ks=>$s)
                                                                <option value="{{ $ks }}" {{ (app('request')->input('search_category')==$ks)  ? 'selected' : ''}}>
                                                                    {{ $s }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3 col-6">
                                                        <select class="form-control" name="p_status" id="p_status">
                                                            <option value="">Select Status</option>
                                                            <option value="0" {{ (app('request')->input('p_status')=='0')  ? 'selected' : ''}}>Pending Approval</option>
                                                            <option value="1" {{ (app('request')->input('p_status')=='1')  ? 'selected' : ''}}>Approved</option>
                                                            <option value="2" {{ (app('request')->input('p_status')=='2')  ? 'selected' : ''}}>Rejected</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3 col-6">
                                                        <div class="form-group">

                                                            <button class="btn btn-primary" type="submit">
                                                                <i class="fa fa-search"></i> Get Details
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    </div>



                                    <!-- col -->
                                    <div class="col-lg-4 col text-right  align-self-center">
                                        <a class="greenlink" href="{{route('seller_new_listings')}}">+ Add New</a>
                                    </div>
                                    <!--/ col -->
                                </div>
                            </div>
                        </div>
                        <!--/ page header -->
                        <!-- row -->


                        <div class="row">
                            <div class="col-lg-12">
                                @if (Session::has('flash_message'))
                                    <br/>
                                    <div class="alert alert-success alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>{{ Session::get('flash_message' ) }}</strong>
                                    </div>
                                @endif
                                <div class="card listcard">
                                    <table class="table table-breakpoint">
                                        <thead>
                                        <tr>
                                            {{--<th>--}}
                                            {{--<input id="" type="checkbox" value="" class="checkbox-template">--}}
                                            {{--</th>--}}
                                            <th>Product Unique Id</th>
                                            <th>Product Picture</th>
                                            <th>Product Name</th>
                                            <th>Category</th>


                                            <th>View on Website</th>
                                            <th>Approval Status</th>
                                            <th>Options</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if(count($product_list) > 0)
                                            @foreach($product_list AS $product_item)

                                                <?php
                                                //                                                                                                dump($product_item);
                                                ?>

                                                <tr>
                                                    {{--<td>--}}
                                                    {{--<input id="" type="checkbox" value="" class="checkbox-template">--}}
                                                    {{--</td>--}}
                                                    <td>
                                                        {{ $product_item->p_unique_id }}
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $images = getImagesByProduct($product_item->p_id);

                                                        //                                                        dump($images);

                                                        //                                                        dd($images);

                                                        if (!empty($images) && count($images) > 0) {
                                                            $image_src = $images[0];
                                                        } else {
                                                            $image_src = 'https://dummyimage.com/600x400/f2f2f2/000000';
                                                        }

                                                        ?>

                                                        <a href="{{route('seller_listingsView',['id'=>$product_item->p_id])}}">
                                                            <img
                                                                    src="{{$image_src}}"
                                                                    alt=""
                                                                    class="img-thumb"></a>
                                                    </td>
                                                    <td>
                                                        <a href="{{route('seller_listingsView',['id'=>$product_item->p_id])}}">
                                                            {{$product_item->p_name}}
                                                        </a>
                                                    </td>
                                                    <td>

                                                        {{getCategory($product_item->p_cat_id)->category_name}}

                                                    </td>


                                                    <td>


                                                        @if ($product_item->p_status == 1)
                                                            <a href="{{route('productPage',['category'=>getCategory($product_item->p_cat_id)->category_alias,'product'=>$product_item->p_alias])}}"
                                                               target="_blank" data-toggle="tooltip"
                                                               title="View on vdesiconnect.com">
                                                                <i class="fa fa-desktop" aria-hidden="true"></i>
                                                            </a>
                                                        @endif


                                                    </td>
                                                    <td>
                                                        {{ productStatus($product_item->p_status) }}
                                                    </td>
                                                    <td class="options">

                                                        <a href="{{ route('seller_listingsView',['id'=>$product_item->p_id])}}"
                                                           data-toggle="tooltip" title="View">
                                                            View
                                                        </a>&nbsp;&nbsp;


                                                        <a href="{{ route('seller_new_listings',['id'=>$product_item->p_id])}}"
                                                           data-toggle="tooltip" title="Edit">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        </a>

                                                        {{--<a href="javascript:void(0)" data-toggle="tooltip" title="Delete Product">--}}
                                                        {{--<i class="fa fa-trash"  aria-hidden="true"></i>--}}
                                                        {{--</a>--}}

                                                        {{--<form method="POST" id="products"--}}
                                                        {{--action="{{ route('admin_products') }}"--}}
                                                        {{--accept-charset="UTF-8" class="form-horizontal"--}}
                                                        {{--style="display:inline">--}}
                                                        {{--{{ csrf_field() }}--}}
                                                        {{--<input type="hidden" name="p_id"--}}
                                                        {{--value="{{ $item->p_id }}"/>--}}
                                                        {{--<button type="submit" class="dropdown-item"--}}
                                                        {{--title="Delete Product"--}}
                                                        {{--onclick="return confirm(&quot;Confirm delete?&quot;)">--}}
                                                        {{--<i class="fa fa-trash"></i> Delete--}}
                                                        {{--</button>--}}

                                                        {{--</form>--}}


                                                        {{--<a href="javascript:void(0)" data-toggle="tooltip" title="Deactive">--}}
                                                        {{--<i class="fa fa-eye-slash" aria-hidden="true"></i>--}}
                                                        {{--</a>--}}
                                                    </td>
                                                </tr>

                                            @endforeach

                                        @else
                                            <tr>
                                                <td colspan="9" align="center">

                                                    No results

                                                </td>

                                            </tr>

                                        @endif


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--/ row -->
                    </div>
                    <!-- container - fluid -->

                </div>
                <!--/ page content main-->
            </div>
            <!--/ content inner -->
        </div>
        <!--/ page content -->
    </main>
    <!--/ dashboard main -->



@endsection