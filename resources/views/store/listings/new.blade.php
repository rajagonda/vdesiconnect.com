@extends('layouts.vendor')

@section('header_styles')

    <style>

    </style>
@endsection

@section('content')

    @include('store.include.login_header')


    <!-- dashboard main -->
    <main class="dbmain">

        <!-- page content -->
        <div class="page-content d-flex align-items-stretch">
            <!-- content inner -->
            <div class="content-inner">
                <!-- page header -->
                <div class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">Create New Listing</h2>
                    </div>
                </div>
                <!--/ page header -->
                <!-- Breadcrumb-->
                <div class="breadcrumb-holder container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('sellerPreDashbord')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('seller_listings')}}">Listings</a></li>
                        <li class="breadcrumb-item active">New Listing</li>
                    </ul>
                </div>
                <!--/ Breadcrumb-->
                <!-- Forms Section-->
                <!-- page content main-->
                <div class="content-main p-3">
                    <!-- container-fluid-->
                    <div class="container-fluid">
                        <!-- row -->

                        @if (Session::has('flash_message'))
                            <br/>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>{{ Session::get('flash_message' ) }}</strong>
                            </div>
                        @endif


                        <form method="POST" id="adProducts" action="{{ route('seller_new_listings') }}"
                              accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="p_id" value="{{ $product_id }}">
                            <input type="hidden" value="">
                            {{--<input type="hidden"--}}
                            {{--class="edit_current_product_alias"--}}
                            {{--value="{{ (isset($product->p_alias)) ? $product->p_alias : old('p_alias')}}"/>--}}

                            <div class="row">
                                <!-- col -->
                                <div class="col-lg-6 col-md-6 ">


                                    <div class="card">
                                        <div class="card-header">
                                            Product Details
                                        </div>
                                        <div class="card-body">

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">

                                                        <label for="hf-email" class=" form-control-label">Product
                                                            name</label>


                                                        <input class="form-control titleCreateAlias" id="p_name"
                                                               type="text"
                                                               placeholder="Product name"
                                                               name="p_name"
                                                               value="{{ !empty(old('p_name')) ? old('p_name') : ((($product) && ($product->p_name)) ? $product->p_name : '') }}">
                                                        @if ($errors->has('p_name'))
                                                            <span class="text-danger help-block">{{ $errors->first('p_name') }}</span>
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="display: none">
                                                <div class="col-lg-12">
                                                    <div class="form-group">

                                                        <label for="hf-email" class=" form-control-label">Product
                                                            Alias</label>


                                                        <input class="form-control" id="p_alias" type="text" readonly
                                                               placeholder="Product Alias"
                                                               name="p_alias"
                                                               value="">
                                                        @if ($errors->has('p_alias'))
                                                            <span class="text-danger help-block">{{ $errors->first('p_alias') }}</span>
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">

                                                <div class="col-lg-4">

                                                    <div class="form-group">

                                                        <label for="hf-email"
                                                               class="form-control-label">Category</label>


                                                        <select name="p_cat_id" id="p_cat_id" class="form-control">
                                                            <option value="">Select</option>
                                                            <?php
                                                            $categories = getCategoriesByID(); ?>
                                                            @foreach($categories as $ks=>$s)
                                                                <option value="{{ $ks }}" {{ (!empty(old('p_cat_id')) && old('p_cat_id')==$ks)  ? 'selected' : ((($product) && ($product->p_cat_id == $ks)) ? 'selected' : '') }}>
                                                                    {{ $s }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('p_cat_id'))
                                                            <span class="text-danger help-block">{{ $errors->first('p_cat_id') }}</span>
                                                        @endif

                                                    </div>
                                                </div>


                                                <!--/ col -->

                                                @if(!empty($product))
                                                    <input type="hidden" class="p_sub_cat_id"
                                                           value="{{ $product->p_sub_cat_id }}">
                                                    <input type="hidden" class="p_item_spl"
                                                           value="{{ $product->p_item_spl }}">
                                            @endif


                                            <!-- col -->
                                                <div class="col-lg-4">
                                                    <div class="form-group">

                                                        <label for="hf-email" class="form-control-label">Sub
                                                            Category</label>


                                                        <select name="p_sub_cat_id" id="p_sub_cat_id"
                                                                class="form-control">
                                                            <option value="">Select Sub Cat</option>

                                                        </select>
                                                        @if ($errors->has('p_sub_cat_id'))
                                                            <span class="text-danger help-block">{{ $errors->first('p_sub_cat_id') }}</span>
                                                        @endif

                                                    </div>
                                                </div>
                                                <!--/ col -->

                                                <div class="col-lg-4">
                                                    <div class="form-group">

                                                        <label for="hf-email" class="form-control-label itemspecial">Flavour</label>


                                                        <select name="p_item_spl" id="p_item_spl"
                                                                class="form-control">
                                                            <option value="">Select Specials</option>
                                                        </select>
                                                        @if ($errors->has('p_item_spl'))
                                                            <span class="text-danger help-block">{{ $errors->first('p_item_spl') }}</span>
                                                        @endif

                                                    </div>
                                                </div>


                                            </div>

                                            <div class="row">

                                                <div class="col-lg-12" id="cake" style="display: none">

                                                    <div class="row">
                                                        <div class="col-lg-4">

                                                            <label>Dish type</label>
                                                            <div class="input-group">

                                                                <div class="form-check-inline">
                                                                    <label class="form-check-label">
                                                                        <input type="radio" class="form-check-input"
                                                                               value="Egg"
                                                                               name="p_dishtype" {{ !empty(old('p_dishtype')) ? old('p_dishtype') : ((($product) && ($product->p_dishtype=='Egg')) ? 'checked=""': '') }} />
                                                                        Egg
                                                                    </label>
                                                                </div>
                                                                <div class="form-check-inline">
                                                                    <label class="form-check-label">
                                                                        <input type="radio" class="form-check-input"
                                                                               value="Egg Less"
                                                                               name="p_dishtype" {{ !empty(old('p_dishtype')) ? old('p_dishtype') : ((($product) && ($product->p_dishtype=='Egg Less')) ? 'checked=""': '') }}>Egg
                                                                        Less
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            @if ($errors->has('p_dishtype'))
                                                                <span class="text-danger help-block">{{ $errors->first('p_dishtype') }}</span>
                                                            @endif
                                                        </div>

                                                        <div class="col-lg-4">

                                                        </div>
                                                        <!--/ col -->

                                                    </div>

                                                </div>
                                                <div class="col-lg-12" id="flowers" style="display: none">


                                                </div>
                                                <div class="col-lg-12" id="gifts" style="display: none">

                                                </div>
                                                <div class="col-lg-4" id="chocolates" style="display: none">


                                                </div>
                                                <div class="col-lg-4" id="jewllary" style="display: none">


                                                </div>
                                                <div class="col-lg-4" id="millets" style="display: none">


                                                </div>
                                            </div>


                                            <!-- row -->
                                            <div class="row">

                                                <div class="col-lg-4">
                                                    <div class="form-group">

                                                        <label for="hf-email" class=" form-control-label">Product
                                                            Availability</label>


                                                        <select name="p_availability" id="p_availability"
                                                                class="form-control">
                                                            <?php
                                                            $availability = availability(); ?>
                                                            @foreach($availability as $ks=>$s)
                                                                <option value="{{ $ks }}" {{ (!empty(old('p_availability')) && old('p_availability')==$ks)  ? 'selected' : ((($product) && ($product->p_availability == $ks)) ? 'selected' : '') }}>{{ $s }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('p_availability'))
                                                            <span class="text-danger help-block">{{ $errors->first('p_availability') }}</span>
                                                        @endif

                                                    </div>
                                                </div>
                                                <!--/ col -->


                                            </div>


                                            {{--<div class="form-group">--}}
                                            {{--<label class="form-control-label">Product Overview</label>--}}
                                            {{--<!-- editor-->--}}
                                            {{--<textarea id="txtEditor"></textarea>--}}
                                            {{--<!-- editor-->--}}
                                            {{--</div>--}}


                                        </div>
                                    </div>


                                    <div class="card">
                                        <div class="card-header">
                                            Product Price SKU Info
                                        </div>
                                        <div class="card-body">
                                            <div class="row">

                                                <table class="table table-bordered" id="dynamicTable">
                                                    <tr>

                                                        <th>SKU TYPE</th>
                                                        <th>SKU OPTION</th>
                                                        <th>SKU VALUE</th>
                                                        <th>Action</th>
                                                    </tr>

                                                    <?php
                                                    $j = 0;
                                                    ?>
                                                    @if(isset($product->productSKUs))
                                                        @if(count($product->productSKUs)>0)
                                                            @foreach($product->productSKUs as $options)
                                                                <?php
                                                                $j = $loop->iteration;
                                                                ?>

                                                                <tr id="{{ $options->sku_id }}">
                                                                    <input type="hidden"
                                                                           name="product_options[{{ $loop->iteration }}][for]"
                                                                           value="edit"/>
                                                                    <input type="hidden"
                                                                           name="product_options[{{ $loop->iteration }}][sku_id]"
                                                                           value="{{$options->sku_id }}"/>


                                                                    <td class="form-group">
                                                                        <div class="input-group">
                                                                        <select name="product_options[{{ $loop->iteration }}][sku_type]"
                                                                                class="form-control">

                                                                            @foreach(skuTypes() AS $skuTypes)
                                                                                <option @if($skuTypes==$options->sku_type) selected @endif>{{$skuTypes}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        </div>

                                                                    </td>

                                                                    <td class="form-group">
                                                                        <input type="text"
                                                                               name="product_options[{{ $loop->iteration }}][sku_option]"
                                                                               placeholder="Enter your Qty"
                                                                               class="form-control"
                                                                               value="{{ $options->sku_option }}"/>
                                                                    </td>


                                                                    <td class="form-group">
                                                                        <div class="input-group">
                                                                            <div class="input-group-prepend">
                                                                                <div class="input-group-text">
                                                                                    {!! currencySymbol('INR')  !!}
                                                                                </div>
                                                                            </div>
                                                                            <input type="text"
                                                                                   name="product_options[{{ $loop->iteration }}][sku_vendor_price]"
                                                                                   placeholder="Enter your Price"
                                                                                   class="form-control"
                                                                                   value="{{ $options->sku_vendor_price }}"/>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        @if($j==1)
                                                                            *
                                                                        @else
                                                                        <button type="button"
                                                                                class="btn btn-danger remove-tr">Remove
                                                                        </button>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    @else
                                                    <tr>
                                                        <input type="hidden" name="product_options[{{ $j+1 }}][for]"
                                                               value="new"/>
                                                        <input type="hidden" name="product_options[{{ $j+1 }}][sku_id]"
                                                               value=""/>

                                                        <td class="form-group">
                                                            <div class="input-group">
                                                            <select name="product_options[{{ $j+1 }}][sku_type]"
                                                                    class="form-control sku_type">
                                                                @foreach(skuTypes() AS $skuTypes)
                                                                    <option>{{$skuTypes}}</option>
                                                                @endforeach
                                                            </select>
                                                            </div>

                                                        </td>
                                                        <td class="form-group">
                                                            <input type="text"
                                                                   name="product_options[{{ $j+1 }}][sku_option]"
                                                                   placeholder="Ex: 2kg, 3kg, 5flowers, 5ltrs"
                                                                   class="form-control sku_option"/>
                                                        </td>
                                                        <td class="form-group">

                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <div class="input-group-text">
                                                                        {!! currencySymbol('INR')  !!}
                                                                    </div>
                                                                </div>

                                                                <input type="text"
                                                                       name="product_options[{{ $j+1 }}][sku_vendor_price]"
                                                                       placeholder="Enter Value or Price"
                                                                       class="form-control sku_vendor_price"/>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            *
                                                        </td>
                                                    </tr>
                                                    @endif

                                                </table>

                                                <div class="col-md-12 text-right">
                                                    <button type="button" name="add" id="add"
                                                            class="btn btn-success">Add New
                                                    </button>
                                                </div>


                                            </div>
                                        </div>
                                    </div>


                                    <div class="card">
                                        <div class="card-header">
                                            Product Gallery Info
                                        </div>
                                        <div class="card-body">

                                            <!-- image gallery  -->
                                            <div class="img-gallery ">
                                                <div class="form-group">
                                                    <label class="form-control-label">Product Gallery </label>
                                                    <div class="input-group">
                                                        <input style="padding-bottom:33px" class="form-control {{ (($product_images) && (count($product_images)>0)) ? '' : 'productimage'}}"
                                                               id="productimage" type="file"
                                                               name="productimages[]" multiple
                                                               accept="image/jpg, image/jpeg,image/png"/>
                                                    </div>
                                                    @if ($errors->has('productimages[]'))
                                                        <span class="text-danger">{{ $errors->first('productimages[]') }}</span>
                                                    @endif
                                                    <small>
                                                        <span>Required Minimum 1 Image, Maximum 5 Images</span> <span>Width=578px, height=578px</span> <span>Each Image should be Less than 120 kb</span>
                                                    </small>
                                                </div>

                                                <div class="row images-listview">

                                                    @if(count($product_images)>0)
                                                        @foreach($product_images as $item)
                                                            <div class="col-md-2 imagediv_{{ $item->pi_id }}">
                                                                <img class="img-fluid imagecover position-relative"
                                                                     height="150"
                                                                     src="/uploads/products/thumbs/{{ $item->pi_image_name }}"/>
                                                                <a href="" id="{{$item->pi_id}}"
                                                                   class="delete_product_image btn btn-danger p-1 btn-xs position-absolute"
                                                                   style="top:0" ;
                                                                   onclick="return confirm('Confirm delete?')"><i
                                                                            class="fa fa-trash"
                                                                            aria-hidden="true"></i> </a>
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <div class="col-md-12">
                                                            No Images found
                                                        </div>
                                                    @endif
                                                </div>

                                            </div>
                                            <!--/ image gallery -->

                                        </div>
                                    </div>


                                </div>

                                <!--/ col -->
                                <!-- col -->
                                <div class="col-lg-6 col-md-6">
                                    <!-- card -->
                                    <div class="card">
                                        <!-- card body -->
                                        <div class="card-header">
                                            Product Options
                                        </div>
                                        <div class="card-body">


                                            <div class="form-group">

                                                <label for="hf-email" class=" form-control-label">
                                                    Product Overview
                                                </label>


                                                <div class="input-group">
                                                <textarea name="p_overview" id="p_overview" rows="5"
                                                          placeholder="Product Overview..."
                                                          class="form-control ckeditor">{!! !empty(old('p_overview')) ? old('p_overview') : ((($product) && ($product->p_overview)) ? $product->p_overview : '') !!}</textarea>
                                                    @if ($errors->has('p_overview'))
                                                        <span class="text-danger help-block">{{ $errors->first('p_overview') }}</span>
                                                    @endif

                                                </div>
                                            </div>


                                            <div class="form-group">

                                                <label for="hf-email" class=" form-control-label">Product
                                                    Specifications</label>

                                                <div class="input-group">
                                                <textarea name="p_specifications" id="p_specifications" rows="5"
                                                          placeholder="Product Specifications..."
                                                          class="form-control ckeditor">{!! !empty(old('p_specifications')) ? old('p_specifications') : ((($product) && ($product->p_specifications)) ? $product->p_specifications : '') !!}</textarea>
                                                    @if ($errors->has('p_specifications'))
                                                        <span class="text-danger help-block">{{ $errors->first('p_specifications') }}</span>
                                                    @endif

                                                </div>
                                            </div>


                                            <div class="form-group">

                                                <label for="hf-email" class=" form-control-label">Product
                                                    Quality Info</label>


                                                <textarea name="p_quality_care_info" id="p_quality_care_info"
                                                          rows="5"
                                                          placeholder="Product Quality Info Care..."
                                                          class="form-control ckeditor">{!! !empty(old('p_quality_care_info')) ? old('p_quality_care_info') : ((($product) && ($product->p_quality_care_info)) ? $product->p_quality_care_info : '') !!}</textarea>
                                                @if ($errors->has('p_quality_care_info'))
                                                    <span class="text-danger help-block">{{ $errors->first('p_quality_care_info') }}</span>
                                                @endif

                                            </div>


                                            <div clas="col-lg-12 py-4">


                                                <div class="row">


                                                    <div class="col-lg-4">

                                                    </div>

                                                    <div class="col-lg-8 text-right">
                                                        <br/>
                                                        <button type="submit" class="greenlink ">
                                                            <i class="fa fa-dot-circle-o"></i> Submit
                                                        </button>
                                                    </div>

                                                </div>


                                            </div>


                                        </div>

                                    </div>

                                </div>

                            </div>
                        </form>

                        <!--/ col -->
                        <!--/ row -->

                    </div>
                    <!-- container - fluid -->
                </div>
                <!--/ page content main-->
            </div>
            <!--/ content inner -->
        </div>
        <!--/ page content -->
    </main>
    <!--/ dashboard main -->

@endsection

@section('footer_scripts')


    @include('store.listings.new_scripts')




@endsection