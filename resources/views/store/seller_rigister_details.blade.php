@extends('layouts.vendor')


@section('content')
    <!-- register -->
    <div class="register w-100 d-flex">
        <div class="registerin m-auto p-5">

            <!-- logo -->
            <div class="logosection text-center my-3">
                <a href="{{route('vendor_home')}}"><img class="logoregister" src="/vendors/images/logo.svg"></a>
            </div>
            <!--/ logo -->

            <form class="sellerform w-75 mx-auto" action="{{route('sellerPreDashbord')}}" method="post">
            {{ csrf_field() }}

            <!-- row -->
                <div class="row mb-2">
                    <div class="col-lg-12 text-center py-3">
                        <h2 class="h5 flight mb-0">Please Set Your Business and Pickup Location </h2>
                        <p>Our logistics partner will pick your packages from this location</p>
                    </div>
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row py-3">

                    <!-- col-->
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Business Name</label>
                            <input type="text" class="form-control" placeholder="Ex: My Business name">
                        </div>
                    </div>
                    <!--/col -->

                    <!-- col-->
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Select Selling Category</label>
                            <select class="form-control">
                                <option>Select Category</option>
                                <option>Cakes</option>
                                <option>Flower Bouquets</option>
                                <option>Gifts</option>
                                <option>Chocklates</option>
                                <option>Jewellery</option>
                                <option>Millets</option>
                                <option>All Catetgories</option>
                            </select>
                        </div>
                    </div>
                    <!--/col -->

                    <!-- col-->
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Where are from Operating</label>
                            <select class="form-control">
                                <option>Select Place</option>
                                <option>Home</option>
                                <option>Retail Store</option>
                                <option>Warehouse</option>
                            </select>
                        </div>
                    </div>
                    <!--/col -->

                    <!-- col-->
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Address Line 01</label>
                            <input type="text" class="form-control" placeholder="Address Line 01">
                        </div>
                    </div>
                    <!--/col -->

                    <!-- col-->
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Address Line 02</label>
                            <input type="text" class="form-control" placeholder="Address Line 02">
                        </div>
                    </div>
                    <!--/col -->

                    <!-- col-->
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Land Mark</label>
                            <input type="text" class="form-control" placeholder="Ex: Near Hospital / Temple/ Bank">
                        </div>
                    </div>
                    <!--/col -->

                    <!-- col-->
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Pincode</label>
                            <input type="text" class="form-control" placeholder="Pincode">
                        </div>
                    </div>
                    <!--/col -->

                    <!-- col-->
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Counrty</label>
                            <input type="text" class="form-control" placeholder="India" disabled>
                        </div>
                    </div>
                    <!--/col -->

                    <!-- col-->
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>State</label>
                            <input type="text" class="form-control" placeholder="Telangana" disabled>
                        </div>
                    </div>
                    <!--/col -->

                    <!-- col-->
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>City</label>
                            <input type="text" class="form-control" placeholder="Hyderabad" disabled>
                        </div>
                        <p>Currently Giving Service Hydearbad Only</p>
                    </div>
                    <!--/col -->
                </div>
                <!--/ row -->

                <input type="submit"  class="greenlink mt-2"
                       value="Proceed">
                {{--<input type="button"  class="greenlink mt-2" value="Cancel">--}}

            </form>
        </div>
    </div>
    <!--/ register -->


@endsection