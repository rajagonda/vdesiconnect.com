@extends('layouts.vendor')


@section('content')

    @include('store.include.header')

    <!-- main -->
    <main class="subpage">
        <!-- sub page header -->
        <div class="subpage-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="flight">Frequently Asked Questions</h1>
                        <p>Your Question Our Answer</p>
                    </div>
                </div>
            </div>
        </div>
        <!--/ sub page header -->

        <!-- sub page main -->
        <div class="subpage-main">
            <div class="container">
                <div class="row">
                    <!-- column -->
                    <div class="col-lg-6">
                        <div class="accordion">
                            <h3 class="panel-title">Why to partner up with Vdesivendor seller hub?</h3>
                            <div class="panel-content">
                                <p>Accepting what our customer’s claim about us, we move forward with all the successful modifications suggested by them. With the thought to bring every brand into limelight, we ship your products to huge extent of customers nationally and internationally. </p>
                            </div>

                            <h3 class="panel-title">How many products are to be catalogued to move ahead with selling?</h3>
                            <div class="panel-content">
                                <p>Atleast one product has to be listed in the bag to continue with the procedure of selling in Vdesiconnect</p>
                            </div>

                            <h3 class="panel-title">How do people go with selling of products in Vdesiconnect?</h3>
                            <div class="panel-content">
                                <p>Register with vdesiconnect.com</p>
                                <p>List out all your products to be sold under corresponding product types</p>
                                <p>After receiving an order, get it packed and label it as ‘Ready to Dispatch’: Our delivery partners will collect the product and let it be delivered to the customer.</p>
                                <p>	After the successful dispatch of product, we make your payment to be settled down within the period of 7-15 business days.</p>
                            </div>

                            <h3 class="panel-title">Who will be the decision maker for the product price?</h3>
                            <div class="panel-content">
                                <p>As a seller, he/she decides the cost of the products.</p>
                            </div>

                            <h3 class="panel-title">Do Vdesiconnect allows cancelling of registered account?</h3>
                            <div class="panel-content">
                                <p>Yes, Vdesiconnect allows to stopover your selling at any time. Either in the case that you have availed any of the services, get in touch with the seller to deactivate your account. </p>
                            </div>

                            <h3 class="panel-title">What does Vdesiconnect Affiliate Program mean to?</h3>
                            <div class="panel-content">
                                <p>It is the successful way in gaining increased sight of customers to your website. Placing of product related posters or links on your website	that refer customers to direct users to Vdesiconnect.com. </p>
                            </div>

                            <h3 class="panel-title">When will be the refund money credited back?</h3>
                            <div class="panel-content">
                                <p>As per the policies and standards decided by Vdesiconnect.com, it might take 7-12 business days to credit back your money.</p>
                            </div>

                            <h3 class="panel-title">What are the types of payment accepted in Vdesiconnect?</h3>
                            <div class="panel-content">
                                <p>Once done with adding of products to the bag, we offer multiple kinds of payment such as</p>
                                <ul>
                                    <li>Credit or Debit card</li>
                                    <li>Netbanking</li>
                                    <li>COD (Cash On Delivery)</li>
                                    <li>Connected mobile wallet payments</li>
                                    <li>EMI options and few other</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--/ column -->

                    <!-- column -->
                    <div class="col-lg-6">

                        <div class="accordion">
                            <h3 class="panel-title">How do people can know the status of their orders?</h3>
                            <div class="panel-content">
                                <p>The streamlined process of order tracking in Vdesiconnect.com allow people to know the status of their orders. The process is as follows:</p>
                                <ul>
                                    <li>Log in to the website</li>
                                    <li>Click My Orders tab where you will be redirected to the page that shows the list of all your orders.</li>
                                    <li>Every order is shown with the option of “Track order”</li>
                                </ul>
                            </div>

                            <h3 class="panel-title">Should I need to courier my products to Vdesiconnect?</h3>
                            <div class="panel-content">
                                <p>Absolutely not, We take care of shipping process of your products. Get your products to be packed up and let those be ready for dispatch. Our professional logistics partner will collect your products and deliver to customers.</p>
                            </div>

                            <h3 class="panel-title">Who can sell on Vdesiconnect?</h3>
                            <div class="panel-content">
                                <p>People who are with genuine products can start their selling process in Vdesiconnect.com</p>
                            </div>

                            <h3 class="panel-title">Can people get their products to be replaced?</h3>
                            <div class="panel-content">
                                <p>You can get your product to be replaced within the period of 7 days from the day of receipt of your order.</p>
                            </div>

                            <h3 class="panel-title">Shall Vdesiconnect accept EMIs?</h3>
                            <div class="panel-content">
                                <p>Vdesiconnect.com also provides the option of EMI where people can choose the plan for either 3 months/6 months/ 9 months or 12 months. </p>
                            </div>

                            <h3 class="panel-title">How we manage all your orders?</h3>
                            <div class="panel-content">
                                <p>With the comprehensive dashboard, sellers can easily manage their entire orders.</p>
                            </div>
                        </div>
                    </div>
                    <!--/ column -->
                </div>
            </div>
        </div>
        <!--/ sub page main-->
    </main>
    <!--/ main -->



@endsection