@extends('layouts.vendor')


@section('header_styles')
@endsection
@section('content')

    @include('store.include.login_header')


    <!-- dashboard main -->
    <main class="dbmain">

        <!-- page content -->
        <div class="page-content d-flex align-items-stretch">
            <!-- content inner -->
            <div class="content-inner">
                <!-- page header -->
                <div class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">Dashboard</h2>
                    </div>
                </div>
                <!--/ page header -->
                <!-- page contentmain-->
                <div class="content-main">


                    <!-- Dashboard Counts Section-->
                    <section class="dashboard-counts ">


                        <div class="container-fluid">

                            @if (vendorValidation(Auth::guard('store')->user()->id))
                            @else
                                <div class="alert alert-danger">
                                    <strong>Notice!:</strong>
                                    Your Account Not Verified from Admin, Please complete all Fields in Below Account,
                                    Business Details, Once you get approval from Admin you Can Start Manage Products.
                                    {{--<a href="#" class="alert-link">read this message</a>.--}}
                                </div>
                            @endif

                            <div class="row bg-white has-shadow">
                                <!-- Item -->
                                <div class="col-xl-4 col-sm-6">
                                    <div class="item d-flex align-items-center">
                                        <div class="icon bg-green"><i class="icon-padnote"></i></div>
                                        <div class="title"><span>Total Orders</span>
                                            <div class="progress">
                                                <div role="progressbar" style="width: 70%; height: 4px;"
                                                     aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"
                                                     class="progress-bar bg-orange"></div>
                                            </div>
                                        </div>
                                        <div class="number"><strong><a
                                                        href="{{route('sellerOrdersNewList')}}">{{$vendorOrderscount}}</a></strong>
                                        </div>
                                    </div>
                                </div>
                                <!-- Item -->
                                <div class="col-xl-4 col-sm-6">
                                    <div class="item d-flex align-items-center">
                                        <div class="icon bg-green"><i class="icon-bill"></i></div>
                                        <div class="title"><span>Total Non Approved Listings</span>
                                            <div class="progress">
                                                <div role="progressbar" style="width: 40%; height: 4px;"
                                                     aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                                     class="progress-bar bg-orange"></div>
                                            </div>
                                        </div>
                                        <div class="number"><strong><a
                                                        href="{{url('store/seller-listings?search=search&product_name=&search_category=&p_status=0')}}">{{$ProductsNonApprove}}</a></strong>
                                        </div>
                                    </div>
                                </div>
                                <!-- Item -->
                                <div class="col-xl-4 col-sm-6">
                                    <div class="item d-flex align-items-center">
                                        <div class="icon bg-green"><i class="icon-check"></i></div>
                                        <div class="title"><span>Total Approved Listings</span>
                                            <div class="progress">
                                                <div role="progressbar" style="width: 50%; height: 4px;"
                                                     aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                                     class="progress-bar bg-orange"></div>
                                            </div>
                                        </div>
                                        <div class="number"><strong><a
                                                        href="{{url('store/seller-listings?search=search&product_name=&search_category=&p_status=1')}}">{{$ProductsApprove}}</a></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Dashboard counts Section    -->

                    <!-- dashboard cateogoreis -->
                    <section class="dbcategories">
                        <div class="container-fluid">
                            <div class="row">

                            <?php
                            //                            dump($profileAccount);
                            //                            dump($profileBusiness);
                            ?>



                            <!-- col -->

                                {{--@dd$profileAccount);--}}
                                <div class="col-lg-4 text-center">
                                    <div class="dbcol bg-white has-shadow pt-5 position-relative">
                                        {{-- @if(count(@$profileAccount)>0)--}}


                                        @if(isset($profileAccount->va_status))
                                            {!! vendorDashbordAccountStatus($profileAccount->va_status); !!}
                                            @else
                                            <span class="label-red position-absolute">Pending</span>
                                        @endif

                                        <img src="/vendors/images/db/boardicon01.png">
                                        <article class="p-4">
                                            <h4>Account</h4>
                                            <p class="pb-1">You need to provide your Address and Contact Information</p>
                                        </article>
                                        <div class="border-top py-4">
                                            <a class="greenlink" href="{{route('profileAccount')}}">Add / Edit
                                                Information</a>
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-4 text-center">
                                    <div class="dbcol bg-white has-shadow pt-5 position-relative">


                                        @if(isset($profileBusiness->vb_status))
                                            {!! vendorDashbordBusinessStatus($profileBusiness->vb_status); !!}
                                        @else
                                            <span class="label-red position-absolute">Pending</span>
                                        @endif




                                        <img src="/vendors/images/db/boardicon02.png">
                                        <article class="p-4">
                                            <h4>Business Details</h4>
                                            <p class="pb-1">You Need to Provide your Bank Account Details for Financial
                                                Transactinos. </p>
                                        </article>
                                        <div class="border-top py-4">
                                            <a class="greenlink" href="{{route('BusinessDetails')}}">Add / Edit
                                                Information</a>
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->


                                <!-- col -->
                                <div class="col-lg-4 text-center">
                                    <div class="dbcol bg-white has-shadow pt-5 position-relative">
                                        <img src="/vendors/images/db/boardicon03.png">
                                        <article class="p-4">
                                            <h4>Settings</h4>
                                            <p class="pb-1">You Can Manage Your Logistics Settings and Other
                                                Settings</p>
                                        </article>
                                        <div class="border-top py-4">
                                            <a class="greenlink" href="{{route('vendorSettings')}}">Add / Edit
                                                Information</a>
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->

                            </div>
                        </div>
                    </section>
                    <!--/ dashboard categories -->

                    <!-- order status section -->
                    <div class="status-section">
                        <div class="container-fluid">
                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-lg-4">
                                    <div class="recent-updates card">
                                        <div class="card-header">
                                            <h3 class="h4">My Orders</h3>
                                        </div>
                                        <div class="card-body no-padding">
                                            <!-- Item-->
                                            <div class="item d-flex justify-content-between">
                                                <div class="info d-flex">
                                                    <div class="title">
                                                        <h5>Processing Orders</h5>
                                                        <p>New Orders which are Received Orders Recently</p>
                                                    </div>
                                                </div>
                                                <div class="date text-right"><strong>{{$processingorders}}</strong>
                                                </div>
                                            </div>
                                            <!-- Item-->
                                            <div class="item d-flex justify-content-between">
                                                <div class="info d-flex">
                                                    <div class="title">
                                                        <h5>Completed Orders</h5>
                                                        <p>Orders which are Delivered & Payment Completed </p>
                                                    </div>
                                                </div>
                                                <div class="date text-right"><strong>{{$completedorders}}</strong></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->


                                <!-- col -->
                                <div class="col-lg-4">
                                    <div class="recent-updates card">
                                        <div class="card-header">
                                            <h3 class="h4">Listings</h3>
                                        </div>
                                        <div class="card-body no-padding">
                                            <!-- Item-->
                                            <div class="item d-flex justify-content-between">
                                                <div class="info d-flex">
                                                    <div class="title">
                                                        <h5>Approved Products</h5>
                                                        <p>Active List Products</p>
                                                    </div>
                                                </div>
                                                <div class="date text-right"><strong>{{$ProductsApprove}}</strong></div>
                                            </div>
                                            <!-- Item-->
                                            <div class="item d-flex justify-content-between">
                                                <div class="info d-flex">
                                                    <div class="title">
                                                        <h5>Pending Approval Products</h5>
                                                        <p>Ready for Active Listings</p>
                                                    </div>
                                                </div>
                                                <div class="date text-right"><strong>{{$ProductsNonApprove}}</strong>
                                                </div>
                                            </div>
                                            <!-- Item  -->
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ row -->
                        </div>
                    </div>
                    <!--/ order status ections -->
                </div>
                <!--/ page content main-->
            </div>
            <!--/ content inner -->
        </div>
        <!--/ page content -->
    </main>
    <!--/ dashboard main -->

@endsection

@section('footer_scripts')


@endsection