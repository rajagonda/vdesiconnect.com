<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Vdesi Connect Admin Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">


    <link rel="stylesheet" href="/admin/css/style_default.css" id="theme-stylesheet">


    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>
<div class="page login-page">
    <div class="container">
        <div class="form-outer text-center d-flex align-items-center">
            <div class="form-inner">
                <div class="logo text-uppercase"><span>Vdesiconnect</span><strong class="text-primary">Dashboard</strong>
                </div>
                {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore--}}
                    {{--et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>--}}

                @if (Session::has('flash_message'))
                    <br/>
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{!! Session::get('flash_message' ) !!}</strong>
                    </div>
                @endif
                <form method="post" class="text-left form-validate" action="{{ route('adminLogin') }}">
                    @csrf
                    <div class="form-group-material">
                        <input id="login-username" type="text" name="email" required
                               data-msg="Please enter your username"
                               class="input-material {{ $errors->has('email') ? ' is-invalid' : '' }}"
                               value="{{ old('email') }}">
                        <label for="login-username" class="label-material">{{ __('E-Mail Address') }}</label>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif

                    </div>
                    <div class="form-group-material">
                        <input id="login-password" type="password" name="password" required
                               data-msg="Please enter your password" class="input-material {{ $errors->has('password') ? ' is-invalid' : '' }}">
                        <label for="login-password" class="label-material">
                            {{ __('Password') }}
                        </label>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group text-center">

                        <button id="login"  class="btn btn-primary" type="submit">Login</button>
                        {{--<a id="login" href="index.html" class="btn btn-primary">Login</a>--}}
                        <!-- This should be submit button but I replaced it with <a> for demo purposes-->
                    </div>
                </form>






            </div>
            <div class="copyrights text-center">
                {{--<p>Design by <a href="https://bootstrapious.com" class="external">Bootstrapious</a></p>--}}
                <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </div>
        </div>
    </div>
</div>

<!-- JavaScript files-->
<script src="/admin/js/adminScripts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
</body>
</html>

