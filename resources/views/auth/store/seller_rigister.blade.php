@extends('layouts.vendor')



@section('header_styles')
    <style>

        footer{
            display:none;
        }

    </style>


@endsection

@section('content')

    <!-- register -->
    <div class="register w-100 d-flex">
        <div class="registerin m-auto p-5">
            <div class="logosection text-center">
                <a href="{{route('vendor_home')}}"><img class="logoregister" src="/vendors/images/logo.svg"></a>
            </div>
            <div class="row">
                <div class="col-md-6 align-self-center">
                    <h2 class="h5 flight py-4 text-center">All you Need to have is:</h2>
                    <div class="row">
                        <div class="col-lg-6 text-center regcol">
                            <img src="/vendors/images/svg/tax2.svg" class="svg">
                            <h6 class="h6 pt-3">GST Identification number </h6>
                            <p>Be with the unique GSTIN number issued by the corresponding state authorities which helps to track every financial transaction.</p>
                        </div>
                        <div class="col-lg-6 text-center regcol">
                            <img src="/vendors/images/svg/bank2.svg" class="svg">
                            <h6 class="h6 pt-3">Bank Account Details</h6>
                            <p>To move ahead with the electronic payments, we ask for the bank account details where those are completely secure</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 text-center regcol">
                            <img src="/vendors/images/svg/card.svg" class="svg">
                            <h6 class="h6 pt-3">Pan Card</h6>
                            <p>To stay back from tax circumventions, we request to provide pan card details which connects all the financial transaction done by the individual or entity.</p>
                        </div>
                        <div class="col-lg-6 text-center regcol">
                            <img src="/vendors/images/svg/gift2.svg" class="svg">
                            <h6 class="h6 pt-3">Products to Sell</h6>
                            <p>We list out hundreds of potential products to sell online. </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">

                    {{ laravelReturnMessageShow() }}


                    <h2 class="h5 flight py-4 text-center">Create Your Seller Account</h2>
                    <form method="POST" action="{{ route('vendorRegister') }}" class="pt-4" id="vendorregister" autocomplete="off">
                        {{ csrf_field() }}
                        <input type="hidden" name="role" value="405">
                        <input type="hidden" name="status" value="active">
                        <div class="form-group">
                            <label>Name</label>
                            <input id="name" type="text" placeholder="Name"
                                   class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                                   value="{{ old('name') }}">
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Email ID</label>
                            <input type="text" placeholder="Email Address"
                                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                   name="email" value="{{ old('email') }}" autocomplete="off" />
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{!! $errors->first('email') !!}</strong>
                       </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input id="password" type="password" name="password" placeholder="Enter Your Password"
                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} " autocomplete="off" />
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input id="password-confirm" type="password" placeholder="Enter Your Confirm Password"
                                   class="form-control" name="password_confirmation">
                        </div>



                        <div class="form-group">
                            <label>Mobile Number</label>

                            <div class="input-group">

                                <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <select name="mobile_prefix" class="">
                                        <option value="">Select</option>
{{--                                        @if(sizeof(getCountrycodes())>0)--}}
{{--                                            @foreach(getCountrycodes() as $key=>$value)--}}
{{--                                                <option value="{{ $key }}">{{ $value }}</option>--}}
{{--                                            @endforeach--}}
{{--                                        @endif--}}

                                        <option value="91"> India</option>

                                    </select>

                                </span>
                                </div>


                                <input type="number" placeholder="Phone Number" name="mobile"  class="form-control mb-1">
                            </div>

                        </div>






                        <p class="py-2">If you have read and agree to the <a href="javascript:void(0)" data-toggle="modal" data-target="#termsregister">Terms and Conditions</a>, Please continue</p>
                        <input type="submit" class="greenlink mt-2" value="Continue">
                        <a class="greenlink mt-2" href="{{route('vendor_home')}}">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--/ register -->


    <!-- Modal -->
    <div class="modal fade" id="termsregister" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Terms & Conditions</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h4 class="h4">Seller Terms & Conditions</h4>
                    <p class="text-justify pb-2">We are so abide that customers make use of our website so comfortable and easy. Before having prior shopping in our website we desire to have a glance on the terms and conditions.</p>
                    <p class="text-justify pb-2">Our shopping site vdesiconnect.com holds good quality of products that consists of (apparel, personal care, electronics, household products and many).</p>
                    <p class="text-justify pb-2">To vend your products on Vdesiconnect, one has to hold registered account as Seller. Upon the registration process, we contend that you follow the terms and conditions as stated by us. Get unlimited shopping experience by complying with the valid rules, ordinances and guidelines.</p>
                    <p>Having an agreement with our terms and conditions will disclose any type of changes that we made any time in our website. If you are in line with our agreement it entails as a legal relationship between you and Vdesiconnect.</p>
                    <p>We welcome you to order various kinds of products and we make sure of having delivery at your specified location. The correct deliveries also enhance the customer support for our website.</p>
                    <p>The terms of service that we abide by are:</p>
                    <p>1. Payment options: we have options of COD and online payments. In case of COD we let you to provide payment only if the customer flatters with good quality and feels better with the price of the product.</p>
                    <p>2. Policy of refund: We are so assure of our great quality. But by any chance if some find any glitch in the delivered product you can go with the option of “Cancellation Order”. In this synopsis the product that is in defect is taken back and your payment will be refunded back.</p>
                    <p>3. Intimacies: We make sure that our customers maintain confidentiality of our data such as not to copy the content, images and videos and make use for their individual projects.</p>
                    <p>4. Product details and other descriptions: We do high end efforts to meet the delights of our customers by meeting their expectations. The MRP price for the products may vary sometimes for certain products. But we try to deliver products at somewhat less price.</p>
                    <p>5. Liability Issues: we are not responsible for your wrongly ordered products. And also we feel not answerable to any loss of data, images or any other information before and after the use of our site.</p>
                    <p>6. Delivery Options: We let you come across the option of “Track your order”. This helps for the customers to know the package and shipping details of your products. This option also let you know when the product is going to be delivered.</p>
                    <p>7. Shipment charges: The cost price for any product does not include any shipment charges. If any happens it will be already described in the product details and type of payment options you are going to choose.</p>
                    <p>8. wrong deliveries: If by chance any retailer of our site delivers wrong products you are requested to notify regarding the wrongly dispatched product and ask him to replace with the desired product.</p>
                    <p>9. Disclaimer: Even though all effort are made to offer precise, in progress and consistent information, you should be familiar with the chance that flaws may exist in the data that is in our Website. Vdesiconnect specifically reject any warranty of the precision, consistency, or appropriateness of any data that is provided in this Website. Charges for goods are subject to alter without any prior note. </p>
                    <p>10. Thank you for making use of our website and we are happy with your orders.</p>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('footer_scripts')


    <script>
        $(function () {

            console.log("gdsgsdag");

            $('#vendorregister').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    name: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        maxlength: 20,
                        minlength: 6
                    },
                    password_confirmation: {
                        required: true,
                        equalTo: "#password",
                        maxlength: 20,
                        minlength: 6
                    },
                    mobile_prefix: {
                        required: true
                    },
                    mobile: {
                        required: true,
                        number: true,
                        maxlength: 10,
                        minlength: 10
                    }
                },
                messages: {
                    name: {
                        required: 'Please enter name'
                    },
                    email: {
                        required: 'Please enter email',
                        email: "Please enter Valid Email"
                    },
                    password: {
                        required: 'Please enter new password'
                    },
                    password_confirmation: {
                        required: 'Please confirm password',
                        equalTo: "The confirm password and new password must match."
                    },
                    mobile_prefix: {
                        required: 'Please Select Mobile Prefix'

                    },
                    mobile: {
                        required: 'Please Enter Mobile Number',
                        number: 'Please Enter Numbers Only',
                        maxlength: "Enter {0} Numbers Only",
                        minlength: "Enter {0} Numbers Only"
                    }
                },
            });
        });
    </script>
@endsection