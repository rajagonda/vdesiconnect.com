
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Vdesi Connect Vendor Portal</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">


    <link rel="stylesheet" href="/vendors/css/style.css" id="theme-stylesheet">


    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body class="loginbody">
<!-- login -->
<div class="login w-100 d-flex">
    <div class="m-auto login-in">
        <div class="row align-self-center">
            <div class="col-lg-6 loginleft align-self-center ">
                <article class="p-3">
                    <h1 class="fwhite pb-3">Forgot Password</h1>
                    <p class="fwhite">Enter your email address to receive a link for creating a new password</p>
                    <p class="py-2"><a href="{{route('vendor_home')}}">Go to Home page</a></p>
                </article>
            </div>
            <div class="col-lg-6 logincol text-center ">
                <a href="javascript:void(0)"><img class="logologin" src="/vendors/images/logo.svg"></a>
                @if (Session::has('status'))
                    <br/>
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ Session::get('status' ) }}</strong>
                    </div>
                @endif
                <form class="login-form w-75  h-100 mx-auto" method="POST" action="{{ route('vendor.password.email') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input id="email" type="text" placeholder="Email Address"
                               class="form-control mb-4{{ $errors->has('email') ? ' is-invalid' : '' }}"  name="email"
                               value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert"> <strong>{{ $errors->first('email') }}</strong>   </span>
                        @endif
                        <input type="submit" value="Submit" class="greenlink" >
                    </div>
                    <p>Please provide the same email address with which you registered on Vdesi Connect Markeplace</p>
                    <p class="pt-2"><a href="{{route('vendorlogin')}}">Already an Account?</a></p>
                </form>
            </div>
        </div>
    </div>
</div>
<!--/ login-->


<!-- script files -->
<script src="/admin/js/adminScripts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<!--/ script files -->

</body>
</html>