


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Vdesi Connect Vendor Portal</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">


    <link rel="stylesheet" href="/vendors/css/style.css" id="theme-stylesheet">


    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body class="loginbody">
<!-- login -->
<div class="login w-100 d-flex">
    <div class="m-auto login-in">
        <div class="row align-self-center">
            <div class="col-lg-6 loginleft align-self-center ">
                <article class="p-3">
                    <h1 class="fwhite pb-3">Forgot Password</h1>
                    <p class="fwhite">Enter your email address to receive a link for creating a new password</p>
                    <p class="py-2"><a href="{{route('vendor_home')}}">Go to Home page</a></p>
                </article>
            </div>
            <div class="col-lg-6 logincol text-center ">
                <a href="javascript:void(0)"><img class="logologin" src="/vendors/images/logo.svg"></a>
                @if (Session::has('flash_message'))
                    <br/>
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ Session::get('flash_message' ) }}</strong>
                    </div>
                @endif
                <form method="POST" class="login-form w-75  h-100 mx-auto" action="{{ route('vendor.password.update') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-group">
                        <label>Enter your Registered Email Address<span class="mand">*</span></label>
                        <input id="email" type="text" placeholder="Email Address"
                               class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                               value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert"> <strong>{{ $errors->first('email') }}</strong>   </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>New Password</label>
                        <input id="password" type="password"
                               placeholder="New Password"
                               class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                               name="password"
                               required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input id="password-confirm" type="password" class="form-control"
                               name="password_confirmation"
                               required>
                        @if ($errors->has('password_confirmation'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <input type="submit" value="Reset Password" class="greenlink">

                </form>
            </div>
        </div>
    </div>
</div>
<!--/ login-->


<!-- script files -->
<script src="/admin/js/adminScripts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<!--/ script files -->

</body>
</html>