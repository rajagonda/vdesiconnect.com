<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Vdesi Connect Vendor Portal</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">


    <link rel="stylesheet" href="/vendors/css/style.css" id="theme-stylesheet">


    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body class="loginbody">
<div class="login w-100 d-flex">
    <div class="m-auto login-in">
        <div class="row align-self-center">
            <div class="col-lg-6 col-md-6 loginleft align-self-center ">
                <article class="p-3">
                    <h1 class="fwhite pb-3">Get utmost shopping experience</h1>
                    <p class="fwhite">Modernized features and services added to your online store. Make your cart to be filled up with the products you love and let we manage all the rest. Start selling online from here and impress your customers through fascinating online stores.</p>
                    <p class="py-2"><a href="{{route('vendor_home')}}">Go to Home page</a></p>
                </article>
            </div>
            <div class="col-lg-6 col-md-6 logincol text-center align-self-center">
                <a href="javascript:void(0)"><img class="logologin" src="/vendors/images/logo.svg"></a>
                @if (Session::has('flash_message'))
                    <br/>
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{!! Session::get('flash_message' ) !!}</strong>
                    </div>
                @endif

                <form method="post" class="login-form w-75 h-100 m-auto " action="{{ route('vendorlogin') }}">
                    @csrf
                    <div class="form-group">
                        <input id="login-username" type="text" name="email" required
                               placeholder="Email"
                               class="form-control mb-4 {{ $errors->has('email') ? ' is-invalid' : '' }}"
                               value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                        <input id="login-password" type="password" name="password" required
                               placeholder="Please enter your password" class="form-control mb-4 {{ $errors->has('password') ? ' is-invalid' : '' }}">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                        <input id="login" class="greenlink" type="submit" value="Login">
                        <a class="greenlink" href="{{route('vendor_home')}}">Cancel</a>

                    </div>
                    <p class="pt-2"><a href="{{ route('vendor.password.reset') }}">Forgot Password?</a></p>
                    <p class="pt-2">You Do not have Seller Account? <a href="{{route('vendorRegister')}}">Register Today</a></p>


                </form>


            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="resenactivationlink" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Send Activation link</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('vendor.verification.resend') }}">
                <div class="modal-body">
                    {{ csrf_field() }}
                    <input type="hidden" value="405" name="role">
                    <div class="form-group">
                        <label>Enter your Registered Email Address<span class="mand">*</span></label>
                        <input id="email" type="text" placeholder="Email Address"
                               class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                               value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback"
                                  role="alert"> <strong>{{ $errors->first('email') }}</strong>   </span>
                        @endif
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Send link</button>
                </div>
            </form>
        </div>
    </div>
</div>



<!-- JavaScript files-->
<script src="/admin/js/adminScripts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
</body>
</html>





