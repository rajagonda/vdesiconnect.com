@extends('frontend.layout')
@section('title', $title)


@section('header_styles')

@endsection


@section('content')
    <main>
        <!-- div login -->
        <div class="sign">
            <div class="signin w-100">
                <div class="brandlogo text-center">
                    <a href="index.php"><img src="/frontend/images/logo.svg" alt="" title="" class="img-fluid"></a>
                </div>
                <article class="text-center">
                    <h5 class="pb-1">Reset Your Password</h5>
                    <p>Enter your details below</p>
                </article>
                @if (Session::has('flash_message'))
                    <br/>
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ Session::get('flash_message' ) }}</strong>
                    </div>
                @endif
                <form method="POST" action="{{ route('password.update') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-group">
                        <label>Enter your Registered Email Address<span class="mand">*</span></label>
                        <input id="email" type="text" placeholder="Email Address"
                               class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"  name="email"
                               value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert"> <strong>{{ $errors->first('email') }}</strong>   </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>New Password</label>
                        <input id="password" type="password"
                               placeholder="New Password"
                               class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                               name="password"
                               required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input id="password-confirm" type="password" class="form-control"
                               name="password_confirmation"
                               required>
                        @if ($errors->has('password_confirmation'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <input type="submit" value="Reset Password" class="btn w-100 my-3">

                </form>

            </div>
        </div>
        <!--/ div login -->
    </main>
@endsection

@section('footerScripts')
@endsection