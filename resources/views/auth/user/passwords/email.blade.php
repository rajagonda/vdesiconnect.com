@extends('frontend.layout')
@section('title', $title)


@section('header_styles')

@endsection


@section('content')
    <main>
        <!-- div login -->
        <div class="sign">
            <div class="signin w-100">
                <div class="brandlogo text-center">
                    <a href="index.php"><img src="/frontend/images/logo.svg" alt="" title="" class="img-fluid"></a>
                </div>
                <article class="text-center">
                    <h5 class="pb-1">Forgot password?</h5>
                    <p>Enter your details below</p>
                </article>
                @if (Session::has('status'))
                    <br/>
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ Session::get('status' ) }}</strong>
                    </div>
                @endif
                <form class="formsign" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Enter your Registered Email Address<span class="mand">*</span></label>
                        <input id="email" type="text" placeholder="Email Address"
                               class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"  name="email"
                               value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert"> <strong>{{ $errors->first('email') }}</strong>   </span>
                        @endif
                    </div>
                    <input type="submit" value="Submit" class="btn w-100 my-3">
                    <p class="text-center">Back to <a href="{{ route('userlogin') }}" class="fgreen">Login ?</a></p>

                </form>

            </div>
        </div>
        <!--/ div login -->
    </main>
@endsection

@section('footerScripts')
@endsection