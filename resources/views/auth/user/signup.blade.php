@extends('frontend.layout')
@section('title', $title)


@section('header_styles')

@endsection


@section('content')
    <main>
        <!-- div login -->
        <div class="sign mx-auto">
            <div class="signin w-100">
                <div class="brandlogo text-center">
                    <a href="{{route('home')}}"><img src="/frontend/images/logo.svg" alt="" title="" class="img-fluid"></a>
                </div>
                <article class="text-center">
                    <h5 class="pb-1">Sign Up</h5>
                    <p>Enter your details below</p>
                </article>
                @if (Session::has('flash_message'))
                    <br/>
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ Session::get('flash_message' ) }}</strong>
                    </div>
                @endif
                <form method="POST" action="{{ route('register') }}" class="pt-4" id="register" autocomplete="off">
                    {{ csrf_field() }}
                    <input type="hidden" name="role" value="1">
                    <input type="hidden" name="status" value="active">
                    <div class="form-group">
                        <label>Name<span class="mand">*</span></label>
                        <input id="name" type="text" placeholder="Name"
                               class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                               value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Email Address<span class="mand">*</span></label>
                        <input type="text" placeholder="Email Address"
                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                               name="email" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{!! $errors->first('email') !!}</strong>
                       </span>
                        @endif
                    </div>


                    <div class="form-group position-relative">
                        <label>Password<span class="mand">*</span></label>
                        <input id="password" type="password" name="password" placeholder="Enter Your Password"
                               class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} ">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    <!-- popover-->
                    {{--<a href="#" class="infodiv" data-toggle="popover" title="Password must contain"><i--}}
                    {{--class="fas fa-info-circle"></i></a>--}}
                    <!--/ popover -->
                        <!-- show password icon -->
                    {{--<span class="showpw"><i class="far fa-eye"></i></span>--}}
                    <!--/ show password  icon -->

                        <!-- loaded popover content -->
                        <div id="popover-content" style="display: none">
                            <ul class="list-group custom-popover">
                                <li class="list-group-item"><span>At least 6 Characters</span></li>
                                <li class="list-group-item"><span>At least 1 Upper case letter (A - Z)</span></li>
                                <li class="list-group-item"><span>At least 1 Lower case Letter (a - z)</span></li>
                                <li class="list-group-item"><span>At least 1 Number (0 - 9)</span></li>
                                <li class="list-group-item"><span>One specl character like !, $, # ....</span></li>
                            </ul>
                        </div>
                        <!--/ loaded popover content -->
                    </div>
                    <div class="form-group position-relative">
                        <label>Confirm Password<span class="mand">*</span></label>
                        <input id="password-confirm" type="password" placeholder="Enter Your Confirm Password"
                               class="form-control" name="password_confirmation">
                    {{--<!-- show password icon --><span class="showpw"><i class="far fa-eye"></i></span>--}}
                    <!--/ show password  icon -->
                    </div>

                    <div class="form-group">
                        <label for="mobileNo">Mobile Number<span class="mand">*</span></label>


                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" style="height:50px; padding:0;">
                                    <select name="mobile_prefix" class="form-control">
                                        <option value="">Select</option>
                                        @if(sizeof(getCountrycodes())>0)
                                            @foreach(getCountrycodes() as $key=>$value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        @endif
                                    </select>

                                </span>
                            </div>
                            <input id="mobileNo" type="text" placeholder="Mobile"
                                   class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile"
                                   value="{{ old('mobile') }}">
                            @if ($errors->has('mobile'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('mobile') }}</strong>
                             </span>
                            @endif
                        </div>


                    </div>

                    <div class="form-group text-center my-1">
                        <input class="btn w-100 my-3" type="submit" value="SIGN UP"></div>
                    <p class="text-center">Already have an account ? <a href="{{route('userlogin')}}" class="fgreen">Sign
                            in</a></p>
                </form>


                {{--<form class="pt-4">--}}
                {{--<div class="form-group">--}}
                {{--<label>First Name <span class="mand">*</span></label>--}}
                {{--<input type="text" placeholder="Enter Your First Name" class="form-control">--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                {{--<label>Last Name</label>--}}
                {{--<input type="text" placeholder="Enter Your Last Name" class="form-control">--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                {{--<label>Email Address<span class="mand">*</span></label>--}}
                {{--<input type="text" placeholder="Enter Your Email Address" class="form-control">--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                {{--<label>Enter Password<span class="mand">*</span></label>--}}
                {{--<input type="password" placeholder="Create Password" class="form-control">--}}
                {{--</div>--}}
                {{--<div class="form-group mb-0">--}}
                {{--<label>Confirm Password<span class="mand">*</span></label>--}}
                {{--<input type="password" placeholder="Confirm Password" class="form-control">--}}
                {{--</div>--}}
                {{--<input type="submit" value="Register With us" class="btn w-100 my-3">--}}
                {{--<p class="text-center">Already have an account ? <a href="login.php" class="fgreen">Sign in</a></p>--}}
                {{--</form>--}}
            </div>
        </div>
        <!--/ div login -->
    </main>

@endsection

@section('footerScripts')
    <script>
        $(function () {

            $('#register').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    name: {
                        required: true
                    },
                    email: {
                        required: true
                    },
                    password: {
                        required: true,
                        maxlength: 20,
                        minlength: 6
                    },
                    password_confirmation: {
                        required: true,
                        equalTo: "#password",
                        maxlength: 20,
                        minlength: 6
                    },
                    mobile_prefix: {
                        required: true
                    },
                    mobile: {
                        required: true,
                        number: true
                    }
                },
                messages: {
                    name: {
                        required: 'Please enter name'
                    },
                    email: {
                        required: 'Please enter email'
                    },
                    password: {
                        required: 'Please enter new password'
                    },
                    password_confirmation: {
                        required: 'Please confirm password',
                        equalTo: "The confirm password and new password must match."
                    },
                    mobile_prefix: {
                        required: 'Please Select Mobile Country Code'
                    },
                    mobile: {
                        required: 'Please Enter Mobile Number',
                        number: 'Please Enter Numbers Only'
                    }
                },
            });
        });
    </script>
@endsection