@extends('frontend.layout')
@section('title', $title)


@section('header_styles')

@endsection


@section('content')
    <main>
        <!-- div login -->
        <div class="sign">
            <div class="signin w-100">
                <div class="brandlogo text-center">
                    <a href="{{route('home')}}"><img src="/frontend/images/logo.svg" alt="" title="" class="img-fluid"></a>
                </div>
                <article class="text-center">
                    <h5 class="pb-1">Signin</h5>
                    <p>Enter your details below</p>
                </article>
                @if (Session::has('flash_message'))
                    <br/>
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{!! Session::get('flash_message' ) !!}</strong>
                    </div>
                @endif
                <form method="POST" action="{{ route('userlogin') }}" aria-label="{{ __('Login') }}" class="pt-4"
                      id="login">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Email<span class="mand">*</span></label>
                        <input id="email" type="text" placeholder="Email Address"
                               class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                               value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert"> <strong>{{ $errors->first('email') }}</strong>   </span>
                        @endif
                    </div>

                    <div class="form-group mb-0">
                        <label>Password<span class="mand">*</span></label>
                        <input id="password" type="password" placeholder="Enter Your Password"
                               class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                               name="password">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <p class="text-right"><a href="{{ route('password.reset') }}">Forgot Password?</a></p>
                    <input type="submit" value="login" class="btn w-100 my-3">
                    <p class="text-center">Don’t Have an Account? <a href="{{ route('register') }}" class="fgreen">Sign
                            up</a></p>

                </form>

            </div>
        </div>
        <!--/ div login -->
    </main>
    <!-- Button trigger modal -->


    <!-- Modal -->
    <div class="modal fade" id="resenactivationlink" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Send Activation link</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ route('verification.resend') }}">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <input type="hidden" value="1" name="role">
                        <div class="form-group">
                            <label>Enter your Registered Email Address<span class="mand">*</span></label>
                            <input id="email" type="text" placeholder="Email Address"
                                   class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                   value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback"
                                      role="alert"> <strong>{{ $errors->first('email') }}</strong>   </span>
                            @endif
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send link</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection

@section('footerScripts')
@endsection