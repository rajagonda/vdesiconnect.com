@extends('frontend.layout')
@section('title', $title)
@section('meta_keywords', $meta_keywords)
@section('meta_description', $meta_description)
@section('headerStyles')
@endsection
@section('content')

    <?php

//            dump($service_type);

    if ($service_type != '') {
        switch ($service_type) {
            case 'medical-assistance':
                ?>
                @include('frontend.service.medical')
                <?php
            break;
            case 'international-courier':
                ?>
                @include('frontend.service.international-courier')
                <?php
            break;
                case 'franchise-enquiry':
                ?>
                @include('frontend.service.franchise-enquiry')
                <?php
            break;
            case 'online-radio':
                ?>
                @include('frontend.service.onlineRadio')
                <?php
            break;
                case 'web-developement':
                ?>
                @include('frontend.service.web-developement')
                <?php
            break;
            case 'summer-enrichment':
                ?>
                @include('frontend.service.summer-enrichment')
                <?php
            break;
            case 'property-management':
                ?>
                @include('frontend.service.property-management')
                <?php
            break;
            case 'online-tutor':
                ?>
                @include('frontend.service.onlineTutor')
                <?php
            break;
            case 'real-estate':
                ?>
                @include('frontend.service.real-estate')
                <?php
            break;
            case 'visa-support-services':
                ?>
                @include('frontend.service.visaSupportServices')
                <?php
            break;
            case 'visitors-insurance':
                ?>
                @include('frontend.service.visitorsInsurance')
                <?php
            break;
                case 'event-organization':
                ?>
                @include('frontend.service.event-organization');
                <?php

            break;
            default;
                ?>
                @include('frontend.service.all');
                <?php
            break;

        }
    }
    ?>

@endsection
@section('footerScripts')
@endsection