<section class="saleproducts">
    <!-- container -->
    <div class="container">
        <!-- title row -->
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <article class="hometitle">
                    <h3 class="px20">New Arrival</h3>
                </article>
            </div>
        </div>
        <!--/ title row -->
        <div class="row">
            <div class="col-lg-12">
            <?php
            if (isset($newArrival) && count($newArrival) > 0) {

                $newArrivalData = array();

                foreach ($newArrival AS $cat_product) {
                    if (count($cat_product->productSKUs) > 0) {
                        $newArrivalData[] = $cat_product;
                    }
                }
            }
            ?>


            <!-- tab products -->

                <!-- Nav tabs -->
                <!-- tab list -->
                <ul class="nav nav-tabs">

                    @if (isset($newArrivalData) && count($newArrivalData) > 0)
                        <li class="nav-item">
                            {{--<a class="nav-link active" data-toggle="tab" href="#new-arrival"></a>--}}
                        </li>
                    @endif

                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" data-toggle="tab" href="#best-sellers">Best Seller </a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" data-toggle="tab" href="#featured">Featured</a>--}}
                    {{--</li>--}}
                </ul>
                <!--/ tab list -->
                <!-- tab container -->
                <div class="tab-content">

                    @if (isset($newArrivalData) && count($newArrivalData) > 0)
                        <div class="tab-pane container active" id="new-arrival">
                            <!-- new arrival-->
                            <div class="row">


                                @if (count($newArrivalData) > 0)
                                    @foreach($newArrivalData AS $newArrival_item)
                                        <?php
                                        $priceSKU = '';

                                        $newArrival_item_image_url = '';
                                        if (count($newArrival_item->productImages) > 0) {
                                            $imageID = array_first($newArrival_item->productImages)->pi_id;
                                            $newArrival_item_image_url = getImagesById($imageID);
                                            if ($newArrival_item_image_url == '') {
                                                $newArrival_item_image_url = '/frontend/images/noproduct-available.jpg';
                                            }
                                        } else {
                                            $newArrival_item_image_url = '/frontend/images/noproduct-available.jpg';
                                        }

                                        //                                    dump($newArrival_item);

                                        if (count($newArrival_item->productSKUs) > 0) {

                                            $priceSKU = $newArrival_item->productSKUs[0];
                                        }

                                        ?>

                                        @if (!empty($priceSKU))


                                            <div class="col-lg-3 col-6 text-center">
                                                <div class="productitem">
                                                    <figure>
                                                        <a href="{{ route('productPage', ['category'=>$newArrival_item->getCategory->category_alias,'product'=>$newArrival_item->p_alias]) }}">
                                                            <img src="{{ $newArrival_item_image_url }}"
                                                                 alt="{{$newArrival_item->p_name}}"
                                                                 title="{{$newArrival_item->p_name}}"
                                                                 class="img-fluid">
                                                        </a>
                                                        <span class="cattag flowers">

                                                    {{  $newArrival_item->getCategory->category_name  }}

                                                </span>
                                                        <div class="hover">
                                                            <ul class="nav">
                                                                <li>

                                                                    <a href="javascript:void(0)" data-toggle="tooltip"
                                                                       data-id="{{ $newArrival_item->p_id }}"
                                                                       data-userid="{{ Auth::id() }}"
                                                                       data-placement="bottom" title="Add to Wishlist"
                                                                       class="productaddwishlist{{ $newArrival_item->p_id }} addToWishList {{in_array($newArrival_item->p_id,$wishlistProducts) ? 'likeactive':''}} ">
                                                                        <span class="icon-heartuser icomoon"></span>

                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="{{ route('productPage', ['category'=>$newArrival_item->getCategory->category_alias,'product'=>$newArrival_item->p_alias]) }}"
                                                                       data-toggle="tooltip"
                                                                       data-placement="bottom" title="View More"><span
                                                                                class="icon-external-link icomoon"></span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </figure>
                                                    <article>
                                                        <a class="proname"
                                                           href="{{ route('productPage', ['category'=>$newArrival_item->getCategory->category_alias,'product'=>$newArrival_item->p_alias]) }}">
                                                            {{ substr($newArrival_item->p_name,0,20).'. . .' }}
                                                        </a>
                                                        <p>

                                                            {!! currencySymbol(getCurency()) !!}  {{ curencyConvert(getCurency(), $priceSKU->sku_store_price)  }}


                                                            @if ($priceSKU->sku_vdc_final_price != 0)
                                                                <small class="oldprice">
                                                                    {!! currencySymbol(getCurency()) !!}  {{ curencyConvert(getCurency(), $priceSKU->sku_vdc_final_price)  }}
                                                                </small>
                                                            @endif


                                                        </p>
                                                        {{--<a href="javascript:void(0)" class="btnlist">Add to Cart </a>--}}
                                                    </article>
                                                </div>
                                            </div>
                                        @endif


                                    @endforeach

                                @endif


                            </div>
                            <!--/ new arrival -->

                        </div>
                    @endif
               {{--     <div class="tab-pane container fade" id="best-sellers">
                        <!-- best selles -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-3 col-6 text-center">
                                <div class="productitem">
                                    <figure>
                                        <a href="javascript:void(0)"><img
                                                    src="/frontend/images/data/cakes/cake08.jpg"
                                                    alt="" title="" class="img-fluid"></a>
                                        <span class="cattag chocklate">Chocklates</span>
                                        <div class="hover">
                                            <ul class="nav">
                                                <li><a href="javascript:void(0)" data-toggle="tooltip"
                                                       data-placement="bottom" title="Add to Wishlist"><span
                                                                class="icon-heartuser icomoon"></span></a></li>
                                                <li><a href="productdetail.php" data-toggle="tooltip"
                                                       data-placement="bottom" title="View More"><span
                                                                class="icon-external-link icomoon"></span></a></li>
                                            </ul>
                                        </div>
                                    </figure>
                                    <article>
                                        <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                        <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                        <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                    </article>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-lg-3 col-6 text-center">
                                <div class="productitem">
                                    <figure>
                                        <a href="javascript:void(0)"><img
                                                    src="/frontend/images/data/cakes/cake07.jpg"
                                                    alt="" title="" class="img-fluid"></a>
                                        <span class="cattag chocklate">Chocklates</span>
                                        <div class="hover">
                                            <ul class="nav">
                                                <li><a href="javascript:void(0)" data-toggle="tooltip"
                                                       data-placement="bottom" title="Add to Wishlist"><span
                                                                class="icon-heartuser icomoon"></span></a></li>
                                                <li><a href="productdetail.php" data-toggle="tooltip"
                                                       data-placement="bottom" title="View More"><span
                                                                class="icon-external-link icomoon"></span></a></li>
                                            </ul>
                                        </div>
                                    </figure>
                                    <article>
                                        <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                        <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                        <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                    </article>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-lg-3 col-6 text-center">
                                <div class="productitem">
                                    <figure>
                                        <a href="javascript:void(0)"><img
                                                    src="/frontend/images/data/fashion/fashion06.jpg" alt=""
                                                    title=""
                                                    class="img-fluid"></a>
                                        <span class="cattag fashion">Boutique</span>
                                        <div class="hover">
                                            <ul class="nav">
                                                <li><a href="javascript:void(0)" data-toggle="tooltip"
                                                       data-placement="bottom" title="Add to Wishlist"><span
                                                                class="icon-heartuser icomoon"></span></a></li>
                                                <li><a href="productdetail.php" data-toggle="tooltip"
                                                       data-placement="bottom" title="View More"><span
                                                                class="icon-external-link icomoon"></span></a></li>
                                            </ul>
                                        </div>
                                    </figure>
                                    <article>
                                        <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                        <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                        <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                    </article>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-lg-3 col-6 text-center">
                                <div class="productitem">
                                    <figure>
                                        <a href="javascript:void(0)"><img
                                                    src="/frontend/images/data/fashion/fashion05.jpg" alt=""
                                                    title=""
                                                    class="img-fluid"></a>
                                        <span class="cattag fashion">Boutique</span>
                                        <div class="hover">
                                            <ul class="nav">
                                                <li><a href="javascript:void(0)" data-toggle="tooltip"
                                                       data-placement="bottom" title="Add to Wishlist"><span
                                                                class="icon-heartuser icomoon"></span></a></li>
                                                <li><a href="productdetail.php" data-toggle="tooltip"
                                                       data-placement="bottom" title="View More"><span
                                                                class="icon-external-link icomoon"></span></a></li>
                                            </ul>
                                        </div>
                                    </figure>
                                    <article>
                                        <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                        <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                        <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                    </article>
                                </div>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ best sellers -->
                    </div>--}}

                    {{--  <div class="tab-pane container fade" id="featured">
                          <!-- featured-->
                          <div class="row">
                              <!-- col -->
                              <div class="col-lg-3 col-6 text-center">
                                  <div class="productitem">
                                      <figure>
                                          <a href="javascript:void(0)"><img
                                                      src="/frontend/images/data/flowers/flower05.jpg" alt="" title=""
                                                      class="img-fluid"></a>
                                          <span class="cattag flowers">Flowers</span>
                                          <div class="hover">
                                              <ul class="nav">
                                                  <li><a href="javascript:void(0)" data-toggle="tooltip"
                                                         data-placement="bottom" title="Add to Wishlist"><span
                                                                  class="icon-heartuser icomoon"></span></a></li>
                                                  <li><a href="productdetail.php" data-toggle="tooltip"
                                                         data-placement="bottom" title="View More"><span
                                                                  class="icon-external-link icomoon"></span></a></li>
                                              </ul>
                                          </div>
                                      </figure>
                                      <article>
                                          <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                          <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                          <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                      </article>
                                  </div>
                              </div>
                              <!--/ col -->

                              <!-- col -->
                              <div class="col-lg-3 col-6 text-center">
                                  <div class="productitem">
                                      <figure>
                                          <a href="javascript:void(0)"><img src="/frontend/images/data/cakes/cake07.jpg"
                                                                            alt="" title="" class="img-fluid"></a>
                                          <span class="cattag chocklate">Chocklates</span>
                                          <div class="hover">
                                              <ul class="nav">
                                                  <li><a href="javascript:void(0)" data-toggle="tooltip"
                                                         data-placement="bottom" title="Add to Wishlist"><span
                                                                  class="icon-heartuser icomoon"></span></a></li>
                                                  <li><a href="productdetail.php" data-toggle="tooltip"
                                                         data-placement="bottom" title="View More"><span
                                                                  class="icon-external-link icomoon"></span></a></li>
                                              </ul>
                                          </div>
                                      </figure>
                                      <article>
                                          <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                          <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                          <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                      </article>
                                  </div>
                              </div>
                              <!--/ col -->

                              <!-- col -->
                              <div class="col-lg-3 col-6 text-center">
                                  <div class="productitem">
                                      <figure>
                                          <a href="javascript:void(0)"><img
                                                      src="/frontend/images/data/flowers/flower06.jpg" alt="" title=""
                                                      class="img-fluid"></a>
                                          <span class="cattag flowers">Flowers</span>
                                          <div class="hover">
                                              <ul class="nav">
                                                  <li><a href="javascript:void(0)" data-toggle="tooltip"
                                                         data-placement="bottom" title="Add to Wishlist"><span
                                                                  class="icon-heartuser icomoon"></span></a></li>
                                                  <li><a href="productdetail.php" data-toggle="tooltip"
                                                         data-placement="bottom" title="View More"><span
                                                                  class="icon-external-link icomoon"></span></a></li>
                                              </ul>
                                          </div>
                                      </figure>
                                      <article>
                                          <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                          <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                          <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                      </article>
                                  </div>
                              </div>
                              <!--/ col -->

                              <!-- col -->
                              <div class="col-lg-3 col-6 text-center">
                                  <div class="productitem">
                                      <figure>
                                          <a href="javascript:void(0)"><img src="/frontend/images/data/cakes/cake06.jpg"
                                                                            alt="" title="" class="img-fluid"></a>
                                          <span class="cattag cakes">Cakes</span>
                                          <div class="hover">
                                              <ul class="nav">
                                                  <li><a href="javascript:void(0)" data-toggle="tooltip"
                                                         data-placement="bottom" title="Add to Wishlist"><span
                                                                  class="icon-heartuser icomoon"></span></a></li>
                                                  <li><a href="productdetail.php" data-toggle="tooltip"
                                                         data-placement="bottom" title="View More"><span
                                                                  class="icon-external-link icomoon"></span></a></li>
                                              </ul>
                                          </div>
                                      </figure>
                                      <article>
                                          <a class="proname" href="productdetail.php">Cake Name will be here</a>
                                          <p>Rs: 7,800 <span class="oldprice">Rs: 9,200</span></p>
                                          <a href="javascript:void(0)" class="btnlist">Add to Cart </a>
                                      </article>
                                  </div>
                              </div>
                              <!--/ col -->
                          </div>
                          <!--/ featured -->
                      </div>--}}

                </div>
                <!--/ tab container -->
                <!--/ new tabs ends -->
            </div>
        </div>
        <!--/ container -->
</section>
