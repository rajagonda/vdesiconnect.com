@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')


    <!--main -->
    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Careers with us</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a>Career </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container stpage">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <img src="/frontend/images/careerimg01.jpg" alt="Make your next career step with us" title="Make your next career step with us" class="img-fluid">

                        </div>
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <h5 class="sectitle flight py-2">Make your next  <span class="fbold">career step with us</span></h5>
                            <p class="text-justify">With the extensive gratitude shown by our employment towards the development of VdesiConnect, we are thankful to our staff. We are strongly committed to onboard ambitious, knowledgeable and imaginative workforce into our team. We do welcome people who interpret the requirements of the clientele and being more communicative. </p>
                            <p>Send your Updated CV to <a class="forange" href="mailto:career@vdesiconnect.com">career@vdesiconnect.com</a></p>
                        </div>
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row py-3">
                        <!-- col -->
                        <div class="col-lg-6 col-md-6 order-lg-last order-md-last">
                            <img src="/frontend/images/career02.jpg" alt="Shows the way for communicative atmosphere" title=" Shows the way for communicative atmosphere " class="img-fluid">
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 col-md-6 align-self-center ">
                            <h5 class="sectitle flight py-2">Shows the way for  <span class="fbold">communicative atmosphere</span></h5>
                            <p class="text-justify">We stand out on the strategy of assisting people who request for support from their agnates. Our exertions all donate to the augmentation of VdesiConnect and creating a world where anyone can belong anywhere. </p>
                            <p>We look forward hearing from you.</p>
                        </div>
                        <!--/ col -->

                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
                <!-- career banner -->
                <div class="careerban dtnone">
                    <img src="/frontend/images/careerbanner.jpg" alt="" title="" class="img-fluid w-100">
                </div>
                <!--/ career banner -->
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row py-4 justify-content-center">
                        <div class="col-lg-8 text-center">
                            <h5 class="sectitle flight py-2">Live your <span class="fbold">best life</span></h5>
                            <p>There’s life at work and life outside of work. We want everyone to be healthy, travel often, get time to give back, and have the financial resources and support they need.</p>

                        </div>
                    </div>
                    <!--/ row -->


                    <!-- row -->
                    <div class="row py-4">
                        <!-- col -->
                        <div class="col-lg-4 col-md-6 text-center careercol">
                            <figure class="svgimg py-2">
                                <span class="icon-health icomoon"></span>
                            </figure>
                            <article>
                                <h5 class="h5">Comprehensive health plans</h5>
                                <p>Covering the complete spectrum of medical requirements, VdesiConnect endeavours to offer comprehensive coverage plans to cater the health necessities of many individuals.</p>
                            </article>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-4 col-md-6 text-center careercol">
                            <figure class="svgimg py-2">
                                <span class="icon-appointment icomoon"></span>
                            </figure>
                            <article>
                                <h5 class="h5">Paid volunteer time</h5>
                                <p>Geared up towards endorsing status and reputation, we think on providing paid volunteer time for our staff which further helps to drive revenues and sustainability.</p>
                            </article>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-4 col-md-6 text-center careercol">
                            <figure class="svgimg py-2">
                                <span class="icon-dish icomoon"></span>
                            </figure>
                            <article>
                                <h5 class="h5">Healthy food and snacks</h5>
                                <p>We exhilarate one’s attention by stocking up with healthy and delicious food and snacks that creates a place for better culture.</p>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-4 col-md-6 text-center careercol">
                            <figure class="svgimg py-2">
                                <span class="icon-baby icomoon"></span>
                            </figure>
                            <article>
                                <h5 class="h5">Generous parental and family leave</h5>
                                <p>Addition of multiple perks to an employee will create an atmosphere which is long-lasting. Thinking on family relations, we let individuals to go with parental and family leaves.</p>
                            </article>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-4 col-md-6 text-center careercol">
                            <figure class="svgimg py-2">
                                <span class="icon-instruction icomoon"></span>
                            </figure>
                            <article>
                                <h5 class="h5">Learning and development</h5>
                                <p>VdesiConnect shows a great scope for people to learn more on various technologies as per the industry trends and upgradations. This shows the way to empower their skills and develop professionally.</p>
                            </article>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-4 col-md-6 text-center careercol ">
                            <figure class="svgimg py-2">
                                <span class="icon-honeymoon icomoon"></span>
                            </figure>
                            <article>
                                <h5 class="h5">Annual travel and experiences credit</h5>
                                <p>Plan a travel with your loved and bag up your experiences as VdesiConnect offers multiple benefits every financial year.</p>
                            </article>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main -->

@endsection
@section('footerScripts')
@endsection