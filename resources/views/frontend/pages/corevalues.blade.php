@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')


    <!--main -->
    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Core Values</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a>Core Values </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container stpage">
                    <!-- row -->
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <h5 class="sectitle fbold py-3 text-uppercase fgreen">Connecting the inside out </h5>
                            <p>We are always delighted to have a pride in services that we offer. We move with few core values those stand as cornerstone for our success. These values show what seems to be more crucial for us as a crew and business. We show a way to connect with the external resources.</p>
                        </div>
                    </div>
                    <!--/ row -->
                    <!-- row -->
                    <div class="row whitebox p-2 pb-0">
                        <div class="col-lg-6 pl-0 col-sm-6">
                            <img src="/frontend/images/corevalues01.jpg" alt="Be Relentless" title="Be Relentless" class="img-fluid w-100">
                        </div>
                        <div class="col-lg-6 col-sm-6 align-self-center">
                            <h5 class="sectitle py-3">Be <span class="fbold">Relentless </span></h5>
                            <p class="text-justify">Success comes in your way when you go with relentless efforts. Take the punches those are in your way and let those all be kicked off when you stay with persistent confidence and assurance. </p>
                        </div>
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row whitebox p-2 pb-0 my-3">
                        <div class="col-lg-6 pr-0 col-sm-6 order-lg-last">
                            <img src="/frontend/images/corevalues02.jpg" alt="Own Outcomes" title="Own Outcomes" class="img-fluid w-100">
                        </div>
                        <div class="col-lg-6 col-sm-6 align-self-center">
                            <h5 class="sectitle py-3">Own  <span class="fbold"> outcomes  </span></h5>
                            <p class="text-justify">Staying back from the complications in the path might not show you positive outcomes in the field you choose with. When you are with your own exertions, you can look for your own outcomes.  </p>
                        </div>
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row whitebox p-2 pb-0 my-3">
                        <div class="col-lg-6 pl-0 col-sm-6">
                            <img src="/frontend/images/corevalues03.jpg" alt="Always Be Curious" title="Always Be Curious" class="img-fluid w-100">
                        </div>
                        <div class="col-lg-6 col-sm-6 align-self-center">
                            <h5 class="sectitle py-3">Always Be <span class="fbold">Curious</span></h5>
                            <p class="text-justify">Curiosity matters the most in achieving business success. Curiosity fuels up your thoughts in moving towards attaining the achievements you desire for.</p>
                        </div>
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row whitebox p-2 pb-0 my-3">
                        <div class="col-lg-6 col-sm-6 pr-0 order-lg-last">
                            <img src="/frontend/images/corevalues04.jpg" alt="Don’t disrespect" title="Don’t disrespect" class="img-fluid w-100">
                        </div>
                        <div class="col-lg-6 col-sm-6 align-self-center">
                            <h5 class="sectitle py-3">Don’t <span class="fbold">disrespect</span></h5>
                            <p class="text-justify">Developing a culture of what is anticipated regarding reverential communication without disrespect is the essential key that opens the door to see the shades of success. VdesiConnect moves with reverence thoughts to be in a healthy environment. </p>
                        </div>
                    </div>
                    <!--/ row -->


                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main -->

@endsection
@section('footerScripts')
@endsection