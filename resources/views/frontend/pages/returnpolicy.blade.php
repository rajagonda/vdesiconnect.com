@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')

    <!--main -->
    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Return Policy</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a>Return Policy </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container stpage">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-12" >
                            <h5 class="sectitle py-3">Quick and simplified returns <span class="fbold">at VdesiConnect </span></h5>
                            <p class="text-justify">We at VdesiConnect endeavor to provide extended quality and services of product to our clientele. In case, when you are not satisfied with the product, we have shown you an option of returning the products to info@vdesiconnect.com</p>
                            <p class="text-justify"><strong>Items are subjected to return only when they are not in good condition, might be materially damaged or misplaced with other product components, or in the case when the product completely differs from the specified product description in the details page</strong>Colors and minor design variations may be there in the actual product and the product displayed on the web site.  <strong> We will not accept returns in this case of minor variations in color or design of the product.</strong></p>

                            <h5 class="sectitle py-3"><span class="fbold">Refund</span></h5>
                            <p class="text-justify">The refund process is initiated once the product is received to us. To move with this process, the product that is returned should be in unused condition, must be with price tags and invoices, failing of these might not trigger for refund processing.</p>
                            <p class="text-justify">After the initiation of the refund process, the product-specific amount will be directly credited to the account through the similar mode of transaction that the item got purchased. Returns or Exchanges are not accepted by VdesiConnect after a period of 7 days from the day that item got delivered. <strong> The delivery charges to return the item should be borne by the customer only. </strong></p>
                            <p class="text-justify">VdesiConnect holds no responsibility for any of the incorrect transactions where those might rise as because of erroneous details submitted by customers. Clients hold entire responsibility for the provided information.</p>
                        </div>
                    </div>
                    <!--/ row -->

                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main -->


@endsection
@section('footerScripts')
@endsection