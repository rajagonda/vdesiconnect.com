@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')


    <!--main -->
    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">About us</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a>About </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container stpage">
                    <!-- row -->
                    <div class="row justify-content-center">
                        <div class="col-lg-10 text-center">
                            <img src="/frontend/images/about01.png" alt="WELCOME TO VDESI CONNECT" title="WELCOME TO VDESI CONNECT" class="img-fluid">

                            <h4 class="py-4">WELCOME TO VDESI CONNECT</h4>
                            <p>Rooted with the belief that providing extended support for the people who seek assistance in various services is the undeniable right rather than to go with good earnings. VdesiConnect arrives with the thought of serving people across various sectors as per the requirements of clientele.  We and our extension of crew will work for you and esteemed of maintaining long-lasting relationships with our clientele. </p>
                        </div>
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row py-3">
                        <!-- col -->
                        <div class="col-lg-6 align-self-center col-sm-6">
                            <h5 class="sectitle flight py-2">Support <span class="fbold">Small Business</span></h5>
                            <p class="text-justify">Apart from offering diversified services, we also stand as support for smaller businesses to follow with their desired ambitions. Absolutely, when one initiates a business, they might go in the right or wrong path. So, VdesiConnect assists those kinds of businesses to be in the right path by sharing our thoughts and experience thus enhancing the profitability of small-scale businesses.</p>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 col-sm-6 align-self-center">
                            <img src="/frontend/images/about02.jpg" alt="We Support Small Business" title="We Support Small Business" class="img-fluid">
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main -->

@endsection
@section('footerScripts')
@endsection