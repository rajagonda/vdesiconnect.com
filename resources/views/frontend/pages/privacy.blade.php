@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')


    <!--main -->
    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Privacy Policy</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a>Privacy Policy </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container stpage">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-6">
                            <h5 class="sectitle py-3">Privacy Policy of  <span class="fbold"> VdesiConnect</span></h5>
                            <p class="text-justify">We www.vdesiconnect.com are utmost loyal to your privacy and confidentiality. Having no prior permissions or authorizations we no on transfer information, vend or undo your personal information to any third parties. </p>

                            <p class="text-justify">Utilizing our services shows that people are abide by the accumulation and utilization of data in connection with this strategy. We in no way disclose client’s information to other parties and we do stand by the data as depicted in Privacy Policy.</p>

                            <p class="text-justify">The terminology of this Privacy Policy holds indistinguishable implications from the Terms and Conditions of www.vdesiconnect.com only if generally characterized in this Privacy Policy. With the assistance of the Privacy Policy Template and the Privacy Policy Generator from Terms Feed together with the Refund Policy Template.</p>

                            <h5 class="sectitle py-3">Information <span class="fbold">Collection and Use</span></h5>
                            <p class="text-justify">For a superior encounter while utilizing our Service, we may expect you to give us certain by and by recognizable data, including yet not restricted to your name, telephone number, and postal location. The data that we gather will be utilized to contact or distinguish you.</p>

                            <p class="text-justify">We may ask for your personal details to offer the services that you request. We collect this information to redirect you to various payment gateways. We assure to hold enhanced data protection and authorized security language.</p>

                            <h5 class="sectitle py-3">Log Date</h5>
                            <p class="text-justify">We need to advise you that at whatever point you visit our Service, we gather data that your program sends to us that is called Log Data. This Log Data may incorporate data, for example, your PC’s Internet Protocol (“IP”) address, program rendition, pages of our Service that you visit, the time and date of your visit, the time spent on those pages, and different measurements.</p>

                            <h5 class="sectitle py-3">Cookies</h5>
                            <p class="text-justify">Treats are documents with little measure of information that is generally utilized a mysterious exceptional identifier. These are sent to your program from the site that you visit and are put away on your PC’s hard drive.</p>
                            <p class="text-justify">Our site utilizes these “treats” to gathering data and to improve our Service. You have the choice to either acknowledge or reject these treats, and know when a treat is being sent to your PC. On the off chance that you decline our treats, you will most likely be unable to utilize a few segments of our Service.</p>

                            <h5 class="sectitle py-3">Service Providers</h5>
                            <p class="text-justify">We may employ third-party companies and individuals due to the following reasons:</p>
                            <ul class="pagelist">
                                <li>To facilitate our Service;</li>
                                <li>To provide the Service on our behalf;</li>
                                <li>To perform Service-related services; or</li>
                            </ul>


                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-6">
                            <p>To assist us in analyzing how our Service is used.</p>
                            <p class="text-justify">We want to inform our Service users that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.</p>

                            <h5 class="sectitle py-3">Security</h5>
                            <p class="text-justify">We esteem your trust in giving us your Personal Information, in this way we are endeavoring to utilize economically adequate methods for ensuring it. Yet, recall that no strategy for transmission over the web, or technique for electronic capacity is 100% secure and solid, and we can&#39;t ensure its supreme security.</p>

                            <h5 class="sectitle py-3">Links to Other Sites</h5>
                            <p class="text-justify">Our Service may contain connections to different destinations. In the event that you click on an outsider connection, you will be coordinated to that site. Note that these outer destinations are not worked by us. Consequently, we emphatically encourage you to survey the Privacy Policy of these sites. We have no influence over, and accept no accountability for the substance, protection approaches, or practices of any outsider locales or administrations.</p>
                            <h5 class="sectitle py-3">Children’s Privacy</h5>
                            <p class="text-justify">Our Services don’t address anybody younger than 13. We don’t purposely gather individual recognizable data from youngsters under 13. For the situation we find that a kid under 13 has given us individual data, we promptly erase this from our servers. On the off chance that you are a parent or watchman and you know that your tyke has given us individual data, it would be ideal if you get in touch with us so we will most likely do vital activities.</p>
                            <h5 class="sectitle py-3">Changes to This Privacy Policy</h5>
                            <p class="text-justify">We may refresh our Privacy Policy occasionally. In this manner, we encourage you to survey this page occasionally for any changes. We will inform you of any progressions by posting the new Privacy Policy on this page. These progressions are taking effect right now, after they are posted on this page.</p>
                            <h5 class="sectitle py-3">Contact Us</h5>
                            <p class="text-justify">Feel free to be in contact with us regarding any questions or suggestions of our Privacy Policy.</p>

                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main -->


@endsection
@section('footerScripts')
@endsection