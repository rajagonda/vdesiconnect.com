@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')


    <!--main -->
    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Terms of Use</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a>Terms of Use </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container stpage">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-12" >
                            <h5 class="sectitle  py-3">Terms of<span class="fbold"> use</span></h5>
                            <p class="text-justify">We thank every individual for making their valuable presence to our website. Your utilization of products and services from our website are entirely circumscribed by the suitable terms and conditions we follow.</p>
                            <p class="text-justify">Either the individual may be guest user or registered user, one should follow up with our terms and conditions to be back from any kind of unauthorized accessibility and illegal issues.</p>
                            <p class="text-justify">Any individual either a visitor or permanent customer can reach to the access for website and website’s products. But a visitor may not access all the segments of the website such as promotional offers/certain benefits and coupons and these may change from time to time at the solitary discretion of the website. </p>
                            <p class="text-justify"><strong>We will be using your contact number or email to send the payment confirmation, order status, tracking number of your shipment, feature products & promotions. </strong></p>
                            <p class="text-justify">Our website does not stand by the product warranties like product life-time, identity and fidelity. We offer a great platform for the sale of services and products and is often agreed that the contract existing between Buyers and Sellers is a completely lawful.</p>
                            <p class="text-justify">Any kind of images and videos for products are for reference only and they may vary from the actual product that is displayed.</p>
                            <p class="text-justify">We hold the right to change or update these terms of usage at any time.</p>
                        </div>

                    </div>
                    <!--/ row -->

                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main -->


@endsection
@section('footerScripts')
@endsection