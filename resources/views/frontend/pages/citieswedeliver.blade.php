@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')

    <!--main -->
    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Cities We Deliver</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}"> Home </a></li>
                                <li><a> Cities we Deliver </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container stpage">
                    <!-- row -->
                    <div class="row justify-content-center pb-3">

                        <!-- col -->
                        <div class="col-lg-12 col-sm-12 citycol">
                            <div class="whitebox">
                                <div class="row">
                                    <div class="col-lg-2 col-sm-4">
                                        <img src="frontend/images/cityhydimg.jpg" class="img-fluid h-100 w-100" alt="Hyderabad Location Deliver Service">
                                    </div>
                                    <div class="col-lg-10 col-sm-8 align-self-center">
                                        <article class="pr-4 cityarticle">
                                            <h4 class="h4 text-uppercase pb-0 mb-0">Hyderabad</h4>
                                            <p><small class="fgreen">Telangana State, India</small></p>
                                            <p class="text-justify">Drowned in reputation, recognition and facets of various cultures and traditions, Hyderabad is cited as the home for many organizations to develop their businesses. VdesiConnect also initiates its business in Hyderabad to reach more into the market in a quick way and grab the attention of customers. </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-12 col-sm-12 citycol">
                            <div class="whitebox">
                                <div class="row">
                                    <div class="col-lg-2 col-sm-4">
                                        <img src="frontend/images/restofindia.jpg" class="img-fluid h-100 w-100" alt="Hyderabad Location Deliver Service">
                                    </div>
                                    <div class="col-lg-10 col-sm-8 align-self-center">
                                        <article class="pr-4 cityarticle">
                                            <h4 class="h4 text-uppercase pb-2 mb-0">Rest of India</h4>
                                            {{--<p><small class="fgreen">Telangana State, India</small></p>--}}
                                            <p class="text-justify">We will deliver Gifts, Millets, Silver Items, Fashion Jewellery, Sweets, Pickles, Boutique items any where in India. Cakes and Flowers are delivered only to Hyderabad for now. </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-12 col-sm-12 citycol">
                            <div class="whitebox">
                                <div class="row">
                                    <div class="col-lg-2 col-sm-4">
                                        <img src="frontend/images/restofworld.jpg" class="img-fluid h-100 w-100" alt="Hyderabad Location Deliver Service">
                                    </div>
                                    <div class="col-lg-10 col-sm-8 align-self-center">
                                        <article class="pr-4 cityarticle">
                                            <h4 class="h4 text-uppercase pb-2 mb-0">Rest of the world</h4>
                                            {{--<p><small class="fgreen">Telangana State, India</small></p>--}}
                                            <p class="text-justify">We will deliver Gifts, Fashion Jewellery, Sweets, Pickles, Millets, Silver items, Boutique items any where in the world subject to that country’s customs limitations. Cakes and Flowers will not be delivered to other places in the world except Hyderbad/Secunderabad, India.</p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--/ col -->
                        <div class="col-lg-12 col-sm-12 py-4 text-center">
                            <h5 class="h5">We will be serving more cities soon. Please <a href="{{route('page_conatct')}}">contact us</a> if you are interested in our Franchise Options</h5>
                        </div>
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main -->

@endsection
@section('footerScripts')
@endsection