@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')

@endsection
@section('content')

    <?php
    $siteSettings = vdcSettings();
    ?>

    <!--main -->
    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Contact us</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a>Contact </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container stpage">
                    <!-- row -->
                    <div class="row">
                        <!-- left column -->
                        <div class="col-lg-8 col-md-7">
                            <h5 class="sectitle pb-3">Drop us <span class="fbold">Message</span></h5>
                            <!-- form -->

                            {{ laravelReturnMessageShow() }}

                            <form class="formpage" id="contactform" method="post" action="{{route('page_conatct')}}">
                            {{ csrf_field() }}
                            <!-- row -->
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input type="text" name="first_name" placeholder="First Name"
                                                   class="form-control" value="{{formValidationValue('first_name')}}"/>
                                            {{formValidationError($errors, 'first_name')}}

                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input type="text" name="last_name" placeholder="Last Name"
                                                   class="form-control" value="{{formValidationValue('last_name')}}"/>
                                            {{formValidationError($errors, 'last_name')}}
                                        </div>
                                    </div>
                                </div>
                                <!--/ row -->

                                <!-- row -->
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input type="text" name="email" placeholder="Email Address"
                                                   class="form-control" value="{{formValidationValue('email')}}"/>
                                            {{formValidationError($errors, 'email')}}
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            {{--<label>asdasd</label>--}}
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <select name="mobile_prefix" class="">
                                        <option value="">Select</option>
                                        @if(sizeof(getCountrycodes())>0)
                                            @foreach(getCountrycodes() as $key=>$value)
                                                <option
                                                    {{ $key==formValidationValue('mobile_prefix') ? 'selected':'' }}  value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    {{formValidationError($errors, 'mobile_prefix')}}

                                </span>
                                                </div>
                                                <input type="text" name="phone_number" placeholder="Phone Number"
                                                       class="form-control"
                                                       value="{{formValidationValue('phone_number')}}"/>
                                                {{formValidationError($errors, 'phone_number')}}
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <!--/ row -->

                                <!-- row -->
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input type="text" name="subject" placeholder="Subject"
                                                   value="{{formValidationValue('subject')}}"
                                                   class="form-control">
                                            {{formValidationError($errors, 'subject')}}
                                        </div>
                                    </div>
                                </div>
                                <!--/ row -->

                                <!-- row -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Message </label>
                                            <textarea class="form-control"
                                                      name="message">{{formValidationValue('subject')}}</textarea>
                                            {{formValidationError($errors, 'message')}}
                                        </div>
                                    </div>
                                </div>
                                <!-- row -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">


                                            <div class="g-recaptcha"
                                                 data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
                                            <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha"
                                                   id="hiddenRecaptcha">

                                            {{--                                         {{ env('GOOGLE_RECAPTCHA_KEY') }}--}}
                                            {{--                                            @if(env('GOOGLE_RECAPTCHA_KEY'))--}}
                                            {{--                                                <div class="g-recaptcha"--}}
                                            {{--                                                     data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">--}}
                                            {{--                                                </div>--}}
                                            {{--                                            @endif--}}
                                        </div>
                                    </div>
                                </div>
                                <!--/ row -->
                                <div class="row">
                                    <div class="col-lg-4">
                                        <input type="submit" value="Send Message" class="greenlink">
                                    </div>
                                </div>
                            </form>
                            <!--/ form -->
                        </div>
                        <!--/ left column -->
                        <!-- right column -->
                        <div class="col-lg-4 col-md-5 mpt-15">
                            <div class="whitebox p-3">
                                <table class="table-contact">
                                    <tr>
                                        <td>
                                            <span class="iconcontact"><span class="icon-email icomoon"></span></span>
                                        </td>
                                        <td>
                                            <h6 class="h6">Email</h6>
                                            <p>
                                                {{ $siteSettings->admin_email }}
                                            </p>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <span class="iconcontact"><span class="icon-call icomoon"></span></span>
                                        </td>
                                        <td>


                                            <h6 class="h6">Phone Number</h6>
                                            <p>
                                                {{ $siteSettings->tolfree_email }}, +1 862-812-7929
                                            </p>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <span class="iconcontact"><span
                                                    class="icon-placeholder icomoon"></span></span>
                                        </td>
                                        <td>
                                            <h6 class="h6">Contact Address India</h6>
                                            <p>
                                                {{ $siteSettings->address }}
                                                {{--Plot No: 10/2, Arora Colony, Housing B oard colony, bengaluru, Karnataka--}}
                                                {{--- 50072--}}
                                            </p>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <h6 class="h6">Contact Address USA</h6>
                                            <p>
                                                3514 Del Amo Blvd, Torrance, CA-90503.
                                            </p>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        <!--/ right column -->

                        {{--<div class="col-lg-12">
                            <div class="map mt-3">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d15226.408319014055!2d78.43539315000001!3d17.43087385!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1550742172879"
                                        width="100%" height="400" frameborder="0" style="border:0"
                                        allowfullscreen></iframe>
                            </div>
                        </div>--}}
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main -->

@endsection
@section('footerScripts')
    <script src='https://www.google.com/recaptcha/api.js?render={{ env('GOOGLE_RECAPTCHA_KEY') }}' async defer></script>


    <script>
        $(function () {


            // $.validator.addMethod("iv_recapcha_valid", function (value, element) {
            //     if (grecaptcha.getResponse() == '') {
            //         return true;
            //     } else {
            //         return false;
            //     }
            // }, 'No message needed, grecaptcha excecuted');


            $('#contactform').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    first_name: {
                        required: true
                    },
                    last_name: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    mobile_prefix: {
                        required: true
                    },
                    phone_number: {
                        required: true,
                        number: true
                    },
                    subject: {
                        required: true
                    },
                    message: {
                        required: true
                    },

                    // hiddenRecaptcha: {
                    //     iv_recapcha_valid: true
                    // }
                    hiddenRecaptcha: {
                        required: function () {
                            if (grecaptcha.getResponse() == '') {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }

                },
                messages: {
                    first_name: {
                        required: 'Enter First Name'
                    },
                    last_name: {
                        required: 'Enter Last Name'
                    },
                    email: {
                        required: 'Enter Email',
                        email: 'Enter Valid Email'
                    },
                    mobile_prefix: {
                        required: 'Select Code'
                    },
                    phone_number: {
                        required: 'Enter Phone Number',
                        number: 'Enter Numbers Only'
                    },
                    subject: {
                        required: 'Enter Subject'
                    },
                    message: {
                        required: 'Enter Message'
                    },
                    hiddenRecaptcha: {
                        required: 'Select Captcha'
                    }

                }
            });


        });

    </script>

@endsection
