@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')


    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Faq's</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a>Faq </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container stpage">
                    <!-- row -->
                    <div class="row">
                        <!-- column -->
                        <div class="col-lg-6">
                            <div class="accordion faqaccordion">
                                <h3 class="panel-title">What is VdesiConnect Affiliate program?1.	What is VdesiConnect Affiliate program?</h3>
                                <div class="panel-content">
                                    <p>We do go with the process of affiliate programming where it is a kind pf electronic programming that consists of a web advertiser and an employed webmaster. As an affiliate, webmaster posts the enterprise related advertisements in their specifically owned websites.</p>
                                </div>

                                <h3 class="panel-title">Why do one shop with VdesiConnect?</h3>
                                <div class="panel-content">
                                    <p>The achievement of every business lies in the customer retention and quality of products. We do provide extended quality of products and services that makes every customer to shop with us.</p>
                                </div>

                                <h3 class="panel-title">How do people track their orders?</h3>
                                <div class="panel-content">
                                    <p>With the account you have created in VdesiConnect, login to the website and go with the option of My Orders/Your Orders. Here, you can select the tracking option for your product, and it shows the complete details of the specified product.</p>
                                </div>

                                <h3 class="panel-title">How secure are the personal and bank account details are managed?</h3>
                                <div class="panel-content">
                                    <p>Adopting with the upgraded versions of various security technologies, we owe not to disclose any of the sensitive information to other third parties?</p>
                                </div>

                                <h3 class="panel-title">Are there any delivery/shipping charges applicable for the product?</h3>
                                <div class="panel-content">
                                    <p>Delivery and shipping charges changes from every location to location. Providing the Zipcode/Pincode and full delivery address will give you the accurate delivery charges at check out.</p>
                                </div>

                                <h3 class="panel-title">What if when customer comes across any issue in ordering a product?</h3>
                                <div class="panel-content">
                                    <p>Our crew will assist you when you face any issue in placing an order, you can directly reach to us at <a href="mailto:info@vdesiconnect.com">info@vdesiconnect.com</a></p>
                                </div>

                                <h3 class="panel-title">How does one know whether they have ordered for a product or not?</h3>
                                <div class="panel-content">
                                    <p>Within the few minutes/hours of time, you will be receiving a message to your registered mobile number and email id in the account stating that your order is successfully placed.</p>
                                </div>


                            </div>
                        </div>
                        <!--/ column -->

                        <!-- column -->
                        <div class="col-lg-6">
                            <div class="accordion faqaccordion">
                                <h3 class="panel-title">Why to register with VdesiConnect?</h3>
                                <div class="panel-content">
                                    <p>Registering with vdesiConnect.com provides your own account where you can manage your own orders, product tracking details, your rewards, and can also avail to multiple benefits.</p>
                                </div>

                                <h3 class="panel-title">Does this website show an option for changing password?</h3>
                                <div class="panel-content">
                                    <p>This the most common scenario that every individual face. So, to be away from this complication, we have provided an option for changing their forgotten password so that they can create the new one and can easily access their account.</p>
                                </div>

                                <h3 class="panel-title">What will be the payment mode in VdesiConnect?</h3>
                                <div class="panel-content">
                                    <p>One can go with payments through debit/credit cards or by COD. We maintain a secured payment gateway where it safeguards the information related with the cards. </p>
                                </div>

                                <h3 class="panel-title">For what places do VdesiConnect deliver products?</h3>
                                <div class="panel-content">
                                    <p>At present, we manage our deliveries around the locations of Hyderabad, and we are in endeavour to extend the scope of locations where our products are delivered to other places too.</p>
                                </div>

                                <h3 class="panel-title">What happens when the receiver does not respond at the time of product delivery?</h3>
                                <div class="panel-content">
                                    <p>The order will be cancelled, and the same information will be informed to the person through SMS and email.</p>
                                </div>

                                <h3 class="panel-title">Do they impose any additional charges for delivery on Sundays or on Holidays?</h3>
                                <div class="panel-content">
                                    <p>No, no additional charges are applicable.</p>
                                </div>

                                <h3 class="panel-title">Is there any chance for ordering “Out of Stock” products?</h3>
                                <div class="panel-content">
                                    <p>Regrettably, we don’t provide any option to buy the products those are unavailable.</p>
                                </div>

                                <h3 class="panel-title">Is it safe to shop with VdesiConnect.com?</h3>
                                <div class="panel-content">
                                    <p>Absolutely, managed with many security and authentication methodologies, we show a safe path for our clientele in enjoying the e-commerce shopping?</p>
                                </div>
                            </div>
                        </div>
                        <!--/ column -->


                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row pt-4">
                        <div class="col-lg-12 text-center">
                            <div class="whitebox p-5">
                                <a href="{{route('userlogin')}}" CLASS="greenlink px-5">LOGIN</a>
                                <p class="pt-5">Login to see your order details, track order, cancel order return / replace order etc.</p>
                            </div>
                        </div>
                    </div>
                    <!--/ row -->

                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main -->


@endsection
@section('footerScripts')
@endsection