@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')


    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Payment Success</h1>
                            </article>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!--left col -->
                        <!-- successful article -->
                        <div class="col-lg-12 text-center successpayment py-4 border-bottom">
                            <p><span class="icon-tick-inside-circle icomoon"></span></p>
                            <h5 class="sectitle fbold pt-1 mb-0 pb-0">
                                <h5 class="sectitle  py-3">
                                    <span class="fbold fgreen">Payment Success, Thank You</span>
                                </h5>
                            </h5>
                            <p>
                                Thank you for Choose Vdesiconnect Product, Check your invoice in your profile
                            </p>

{{--                            {{ Request::segment(2) }}--}}

                            {{--@if(Request::segment(2)=='cart')--}}
                                {{--<a href="{{route('showOrderInvoice',['id'=>$invoice->order_id])}}"class="greenlink">Show Invoice</a>--}}
                            {{--@elseif(Request::segment(2)=='services')--}}
                                {{--<a href="{{route('showServiceInvoice',['id'=>$invoice->s_id])}}"class="greenlink">Show Invoice</a>--}}
                            {{--@else--}}
                                {{--<a href="{{route('AmmachethiVantaOrderView',['id'=>$invoice->ac_order_id])}}"class="greenlink">Show Invoice</a>--}}
                            {{--@endif--}}



                            {{--<a href="javascript:void(0)"class="greenlink">View Invoice</a>--}}
                            <a href="{{route('home')}}"class="whitebtn">Continue Shopping</a>
                        </div>
                        <!--/ successful article -->
                        <!--/left col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>

@endsection
@section('footerScripts')
@endsection