@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')

    <!--main -->
    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Blog</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{ route('home') }}">Home</a></li>
                                <li><a>Blog </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container stpage">
                    <!-- row -->
                    <div class="row">
                    @if(count($blogs)>0)
                        @foreach($blogs as $blog)
                            <!-- col -->
                                <div class="col-lg-6 blogcol col-sm-6">
                                    <figure><a href="{{route('blogView',['id'=>$blog->blog_id])}}"><img   src="/uploads/blogs/{{ $blog->blog_image }}" alt="{{ $blog->blog_title }}" title="{{ $blog->blog_title }}" class="img-fluid"></a></figure>
                                    <article class="text-center">
                                        <a href="{{route('blogView',['id'=>$blog->blog_id])}}">{{ $blog->blog_title }}</a>
                                        <p><span>{{ ($blog->created_at) ? \Carbon\Carbon::parse($blog->created_at)->format('F j, Y') : '-'  }} </span><span>By {{ $blog->getUser->name }} </span></p>
                                        <a href="{{route('blogView',['id'=>$blog->blog_id])}}" class="linkblog">READ MORE</a>
                                    </article>
                                </div>
                                <!--/ col -->
                            @endforeach
                        @else
                            <div class="col-lg-6 blogcol col-sm-6">
                                Coming Soon
                            </div>
                        @endif
                    </div>
                    <!--/ row -->
                    @if(count($blogs)>0)
                        <div class="row">
                            <div class="col-md-10">
                                <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                                     aria-live="polite">Showing {{ $blogs->firstItem() }}
                                    to {{ $blogs->lastItem() }} of {{ $blogs->total() }} entries
                                </div>
                            </div>
                            <div class="col-md-1 text-right">
                                <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                                    {!! $blogs->appends(['filters' => Request::get('filters')])->render() !!}
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main -->

@endsection
@section('footerScripts')
@endsection