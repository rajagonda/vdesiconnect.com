@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')

    <!--main -->
    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Blog</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a href="{{ route('bloglist') }}">Blogs </a></li>
                                <li><a>{{ $blog->blog_title }}</a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container stpage">
                    <!-- row -->
                    <div class="row">
                        <!-- col-lg-8-->
                        <div class="col-lg-8 blogdetail">
                            <figure>
                                <img src="/uploads/blogs/{{ $blog->blog_image }}" alt="{{ $blog->blog_title }}" title="{{ $blog->blog_title }}"  class="img-fluid w-100">
                            </figure>
                            <article>
                                <h3>{{ $blog->blog_title }}</h3>
                                <p class="det">
                                    <span>{{ ($blog->created_at) ? \Carbon\Carbon::parse($blog->created_at)->format('F j, Y') : '-'  }}</span><span>By {{ $blog->getUser->name }} </span>
                                </p>
                                <p class="text-justify">{!! $blog->blog_description !!}</p>
                            </article>
                        </div>
                        <!--/ col-lg-8 -->

                        <!-- col lg 4-->
                        <div class="col-lg-4 recentnews">
                            <h3 class="h3 border-none">Recent News & Updates</h3>
                            <!-- row -->
                            <div class="row">
                            @if(count($related_blogs)>0)
                                @foreach($related_blogs as $blogs)
                                    <!-- col -->
                                        <div class="col-lg-12 col-sm-6">
                                            <div class="recentcol">
                                                <figure>
                                                    <a href="javascript:void(0)"><img  src="/uploads/blogs/{{ $blogs->blog_image }}" alt="{{ $blogs->blog_title }}"  title="{{ $blogs->blog_title }}"  class="img-fluid"></a>
                                                    <span class="position-absolute">{{ ($blogs->created_at) ? \Carbon\Carbon::parse($blogs->created_at)->format('F j, Y') : '-'  }}</span>
                                                </figure>
                                                <article>
                                                    <a href="{{route('blogView',['id'=>$blogs->blog_id])}}">{{ $blogs->blog_title }}</a>
                                                </article>
                                            </div>
                                        </div>
                                        <!--/ col -->
                                    @endforeach
                                @else
                                    <div class="col-lg-12 col-sm-6">
                                        <div class="recentcol">
                                            Coming Soon
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <!--/ row -->
                        </div>
                        <!--/ col lg 4 -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main -->

@endsection
@section('footerScripts')
@endsection