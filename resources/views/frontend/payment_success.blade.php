@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')


    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Payment Success</h1>
                            </article>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!--left col -->
                        <!-- successful article -->
                        <div class="col-lg-12 text-center successpayment py-4 border-bottom">
                            <p><span class="icon-tick-inside-circle icomoon"></span></p>
                            <h5 class="sectitle fbold pt-1 mb-0 pb-0">
                                <h5 class="sectitle  py-3">
                                    <span class="fbold fgreen">Payment Success</span>
                                </h5>
                            </h5>
                            <p>
                                Thank you! Your Payment of
                                {!! currencySymbol('USD')  !!} {{ $latest_order->order_total_price }} has been received
                            </p>
                            <?php
//                            dump($latest_order);
                            ?>

                            <p>
                                <span>Order ID : {{ $latest_order->order_reference_number }}</span>
                                <span class="px-4">Transaction ID : {{ $latest_order->order_payment_transaction_id }}</span>
                            </p>
                            <p clas="fbold">Click on the following Button, You Can Track the Order Status, Payment Details of Product  from Order Booking to Delivery at your doorstep</p>
                            <a href="{{route('orderDetails',['id'=>$latest_order->order_id])}}"class="greenlink">Track Order</a>
                            {{--<a href="javascript:void(0)"class="greenlink">View Invoice</a>--}}
                            <a href="{{route('home')}}"class="whitebtn">Continue Shopping</a>
                        </div>
                        <!--/ successful article -->
                        <!--/left col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>

@endsection
@section('footerScripts')
@endsection