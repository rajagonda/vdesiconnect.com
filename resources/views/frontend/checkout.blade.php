@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')


    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Review Order</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{ route('home') }}">Home</a></li>
                                <li><a href="{{ route('cartPage') }}">My Cart </a></li>
                                <li><a>Review Order </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="whiterow mb-3 pb-0">
                        <div class="row">
                            <div class="col-lg-6 col-6">
                                <h4 class="pl-2">Cart Items ({{ Cart::getContent()->count() }})</h4>
                            </div>
                            <div class="col-lg-6 col-6 text-right">
                                {{--<h5 class="pr-2">{{ currencySymbol('USD') }} {{ Cart::getSubTotal() }}</h5>--}}
                            </div>
                        </div>
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row">
                        <!--left col -->
                        <div class="col-lg-8 col-sm-7">
                            <!-- row -->
                            <div class="row">

                            @if(Cart::getContent()->count()>0)
                                <?php
                                $totalweight = 0;
                                ?>
                                @foreach( Cart::getContent()->sort() as $item)

                                    <?php

//dump($item);

                                    $weightInfomation = sortProductOption($item->attributes->options, 'weight');

//                                    dd($weightInfomation);

                                    //                                    dump($item->attributes->options);
                                    //                                            dump(productWightFarmart( $weightInfo, 'calc'));
                                    //                                    dd($weightInfo);

                                    $weightInfo = productWightFarmart( $weightInfomation, 'calc');

                                    $weightData = $weightInfo['weight'];
                                    $weightType = $weightInfo['type'];

//                                    dd($weightInfo);


                                    $weight = $item->quantity * (float)$weightData;

                                    $totalweight += $weight;
                                    $product = getProduct($item->id);
                                    $image_url = '';
                                    if (count($product->productImages) > 0) {
                                        $imageID = array_first($product->productImages)->pi_id;
                                        $image_url = getImagesById($imageID);
                                        if ($image_url == '') {
                                            $image_url = '/frontend/images/noproducts.jpg';
                                        }
                                    } else {
                                        $image_url = '/frontend/images/noproducts.jpg';
                                    }

                                    ?>

                                    <!-- small column -->
                                        <div class="col-lg-12">
                                            <div class="smallcol mb-3 checkcol">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-3">
                                                        <figure>
                                                            <a href="{{ route('productPage', ['category'=>$product->getCategory->category_alias,'product'=>$product->p_alias]) }}">
                                                                <img src="{{ $image_url }}" alt=""
                                                                     title="" class="img-fluid w-100">
                                                            </a>
                                                        </figure>
                                                    </div>
                                                    <div class="col-lg-9 col-md-9">
                                                        @if ($item->attributes->delivery_date!='' || $item->attributes->delivery_time )
                                                            <p class="fgreen text-right">Delivery by
                                                                {{$item->attributes->delivery_date}}
                                                                {{$item->attributes->delivery_time}}
                                                            </p>
                                                        @endif
                                                        <article>
                                                            <h5>
                                                                <a href="{{ route('productPage', ['category'=>$product->getCategory->category_alias,'product'=>$product->p_alias]) }}">
                                                                    {{ $item->name }}
                                                                </a>
                                                            </h5>
                                                            <p>
                                                                GIft Message:
                                                                <small>{{$item->attributes->message}}</small>
                                                            </p>
                                                            <p>
                                                                Options:
                                                                <small>
                                                                    {{ sortProductOptions($item->attributes->options) }}
                                                                </small>
                                                            </p>
                                                            <?php
                                                            //                                                            dump($item->attributes);
                                                            ?>


                                                        </article>

                                                        <div class="row mpx-15">
                                                            <div class="col-lg-2 col-4">
                                                                <span class="price">
                                                                   <small> Price</small>

                                                                    <span class="d-block w-100 cur">  {!! currencySymbol($item->attributes->currency) !!} {{ $item->price  }}</span>



                                                                </span>


                                                            </div>

                                                            <div class="col-lg-2 col-4">
                                                                 <span class="price">
                                                                <small>Qty</small>
                                                                <span class="d-block w-100 cur"> {{$item->quantity}}</span>
                                                                 </span>
                                                            </div>
                                                            <div class="col-lg-2 col-4">
                                                                 <span class="price">
                                                                <small>Weight</small>
                                                                <span class="d-block w-100 cur">
{{--{{ $weightInfomation }}--}}
                                                                    {{$weightData}} {{ $weightType }}
                                                                </span>
                                                                 </span>
                                                            </div>

                                                            <div class="col-lg-2 col-4">
                                                                <span class="price">
                                                                   <small>Sub Total</small>
                                                                    <span class="d-block w-100 cur">
                                                                         {!! currencySymbol($item->attributes->currency) !!}

                                                                        {{  $item->price * $item->quantity }}
                                                                    </span>
                                                                </span>
                                                            </div>

                                                            {{--                                                            <div class="col-lg-3">--}}
                                                            {{--                                                                <small>Delivery Charge</small>--}}

                                                            {{--                                                                <span class="d-block w-100 cur">--}}

                                                            {{--                                                                      {!! currencySymbol($item->attributes->currency) !!}--}}

                                                            {{--                                                                    {{ $item->attributes->delivery_charge  }}--}}

                                                            {{--                                                                    --}}{{--                                                                    {{ $item->price }}--}}
                                                            {{--                                                                </span>--}}
                                                            {{--                                                            </div>--}}

                                                            <div class="col-lg-3 col-4">
                                                                <span class="price">
                                                                   <small>Total Price</small>

                                                                    <?php
                                                                    //                                                                    dump(Cart::getContent());
                                                                    ?>

                                                                    {{--                                                                    {{Cart::getTotal()}}--}}
                                                                    {{--                                                                    {{($item->quantity *$item->price) }}--}}

                                                                    <span class="d-block w-100 cur">
                                                                        {!! currencySymbol($item->attributes->currency) !!}

                                                                        {{  $item->getPriceSumWithConditions() }}</span>

                                                                    {{--                                                                    {{ $item->price }}--}}
                                                                </span>
                                                            </div>


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                        <!-- small column -->
                                    @endforeach
                                @else
                                    <div class="col-lg-12">
                                        No Data Found
                                    </div>
                                @endif


                                <?php

                                //                                dump(session('siteCountryinfo'));
                                //                                dump(session('siteCountryinfo')->country_name);
                                ?>


                                <div class="col-lg-12 pt-2">


                                    <div class="row">

                                        <div class="col-lg-12 ">

                                            <div class="col-lg-12 pt-2 whitebox text-right">

                                                <div class="row  ">

                                                    <div class="col-sm-8 p-1 col-8">
                                                        Weight Summery
                                                    </div>
                                                    <div class="col-sm-4 col-4">
                                                        {{ $totalweight }}Kgs
                                                    </div>
                                                </div>
                                                <div class="row ">

                                                    <div class="col-sm-8 p-1 col-8">
                                                        Total Amount
                                                    </div>
                                                    <div class="col-sm-4 col-4">
                                                        {!! currencySymbol(getCurency()) !!}
                                                        {{ Cart::getTotal() }}
                                                    </div>
                                                </div>

                                                <div class="row ">
                                                    <div class="col-sm-8 p-1 col-8">
                                                        Shipping Amount ({{ session('siteCountryinfo')->country_name }})
                                                    </div>
                                                    <div class="col-sm-4 col-4">
                                                        <?php
                                                        $paymentAmountCurency = getCurency();
                                                        $shippingAmount = caliclateShippingAmount($totalweight, $address_info->ua_country);
                                                        ?>
                                                        {!! currencySymbol($paymentAmountCurency) !!}
                                                        {{ $shippingAmount}}
                                                    </div>
                                                </div>

                                                <div class="row border-light border-bottom">
                                                    <div class="col-sm-8 p-1 col-8">
                                                        Discount Amount
                                                    </div>
                                                    <div class="col-sm-4 col-4 couponDiscount">
                                                        --
                                                    </div>
                                                </div>

                                                <div class="row ">
                                                    <div class="col-sm-8 p-1 col-8">
                                                        <h5 class="greencolor">Final Amount</h5>
                                                    </div>
                                                    <div class="col-sm-4 col-4">
                                                        <?php
                                                        $paymentAmount = caliclateShippingAmount($totalweight, $address_info->ua_country);

                                                        $paymentAmount = $paymentAmount + Cart::getTotal();

                                                        ?>


                                                        <h5 class="forange">
                                                            {!! currencySymbol($paymentAmountCurency) !!}
                                                            <span class="finaleAmount">
                                                                 {{ $paymentAmount }}
                                                            </span>
                                                        </h5>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--/ row -->
                                <!-- address column -->
                                <div class="col-lg-12">
                                    <div class="whitebox p-3 mt-3">
                                        <p class="fblue">Shipping Address</p>
                                        <p>{{ $address_info->ua_name }}</p>
                                        <p>{{ $address_info->ua_address }}
                                            , {{ $address_info->ua_landmark }},
                                            {{ $address_info->ua_city }},{{ $address_info->ua_state }}
                                            ,{{ $address_info->getCountry->country_name }}
                                            - {{ $address_info->ua_pincode }} <a
                                                    href="{{ route('cartAdressPage') }}">Change
                                                Address</a></p>
                                        <p>Phone: {{ $address_info->ua_phone }}</p>
                                    </div>
                                </div> 
                                <!-- address column -->
                               
                            </div>
                            <!--/ row -->
                        </div>
                        <!--/left col -->
                        <!-- right cart -->
                        <div class="col-lg-4 col-sm-5">

                        <!-- white box-->

                            <div class="whitebox p-3">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h5>Enter Coupan Code </h5>
                                        <div class="form-group">                                                                                           
                                            <input type="text" name="coupons"  class="form-control checkcoupon"/>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <button class="greenlink w-100" name="Check"  type="button" id="checkcoupon"> APPLY  </button>
                                            </div>
                                            <p class="couponmessage pb-0 pt-1"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ white box -->

                            <div class="whitebox p-2 mt-3">

                            <form class="mb-2 billingAddress" method="POST" id="billingAddress"
                                  action="{{ route('saveOrders') }}"
                                  accept-charset="UTF-8">
                                {{ csrf_field() }}

                                <div class="col-lg-12 pt-2">

                                    <div class="billingCheck">
                                        <div>
                                            <input type="radio" name="addr" value="0"/>
                                            Same as shipping address
                                        </div>
                                        <div>
                                            <input type="radio" name="addr" value="1" checked/>
                                            Use a different billing address
                                        </div>
                                    </div>


                                    <div class="addresBox">
                                        <h5 class="modal-title">Add Billing Address</h5>
                                        <!-- Modal body -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Name</label>
                                                    <input type="text" placeholder="Write Your Name" required  class="form-control" name="ua_name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="mobileNo">Mobile Number<span class="mand">*</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding:0;">
                                                            <select name="mobile_prefix" class="form-control" required>
                                                                <option value="">Select</option>
                                                                @if(sizeof(getCountrycodes())>0)
                                                                    @foreach(getCountrycodes() as $key=>$value)
                                                                        <option value="{{ $key }}">{{ $value }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </span>
                                                        </div>
                                                        <input id="mobileNo" type="number" placeholder="Mobile"
                                                               required
                                                               class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}"
                                                               name="ua_phone"
                                                               value="{{ old('mobile') }}">
                                                        @if ($errors->has('mobile'))
                                                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('mobile') }}</strong>
                                     </span>
                                                        @endif
                                                    </div>


                                                </div>


                                                <div class="form-group">
                                                    <label>Select Country</label>
                                                    <select name="ua_country" id="ua_country_add" required
                                                            class="form-control ua_country" data-country=""
                                                            data-state="">
                                                        <option value="">--Select Country--</option>
                                                        <?php
                                                        $countries = getCountries(); ?>
                                                        @foreach($countries as $ks=>$s)
                                                            <option value="{{ $ks }}">
                                                                {{ $s }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Select State</label>

                                                    <select class="form-control ua_state" required
                                                            name="ua_state">
                                                        <option value="">--Select State--</option>
                                                    </select>

                                                    {{--<input type="text" placeholder="State Name" class="form-control" name="ua_state">--}}
                                                </div>


                                                <div class="form-group citybox" style="display: none;">
                                                    <label>Select City</label>

                                                    <select class="form-control ua_city" name="ua_city">
                                                        <option value="">--Select City--</option>
                                                    </select>


                                                </div>

                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <textarea class="form-control"
                                                              placeholder="Enter Your Address"
                                                              name="ua_address"></textarea>
                                                </div>


                                                <div class="form-group">
                                                    <label>Pincode / Zip</label>
                                                    <input type="number" placeholder="Area Pincode" required
                                                           class="form-control" name="ua_pincode">
                                                    <small>Entering a Land Mark will help us in identifying your
                                                        location faster
                                                    </small>
                                                </div>

                                            </div>
                                        </div>
                                    </div>


                                </div>


                                <div class="p-3">
                                    @include('frontend._partials.checkout_right')

                                    <input type="hidden" class="checkcoupontotal" value="{{ $paymentAmount }}">
                                    <h3 class="text-center">

                                        Payble Amount


                                        <h2 class="forange h2 text-center">
                                            {!! currencySymbol(getCurency()) !!}
                                            <span class="finaleAmount">
                                                {{ $paymentAmount }}
                                            </span>
                                         </h2>


                                    </h3>


                                    <input type="hidden" name="address_id" value="{{ $address_id }}">
                                    <input type="hidden" name="couponcode" id="couponcode">
                                    <input type="hidden" name="payment_curency" value="{{$paymentAmountCurency}}">
                                    <input type="hidden" name="coupondiscount" id="couponDiscount">
                                    <input type="hidden" name="amount_payable" id="couponDiscounttotal">
                                    <input type="hidden" name="totalweight" value="{{ $totalweight }}">
                                    <input type="hidden" name="shippingprice"
                                           value="{{ $shippingAmount }}">
                                    <input type="hidden" name="totalprice"
                                           value="{{ $paymentAmount }}">


                                    <button type="submit" class="greenlink w-100">


                                        Proceed to Payment
                                    </button>


                                    <br/>
                                    <br/>

                                    <a href="{{route('home')}}" class="whitebtn text-center w-100">Continue Shopping
                                    </a>


                                </div>

                            </form>
                            </div>

                        </div>
                        <!--/ right cart -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main -->
    <input type="hidden" class="couponcountry" value="{{ getCountry() }}">

@endsection
@section('footerScripts')

    <script>

        $(function () {

            $('#checkcoupon').on('click', function () {
                var coupon = $('.checkcoupon').val();
                var total = $('.checkcoupontotal').val();
                var couponcode = $('.checkcoupon').val();
                var couponcountry = $('.couponcountry').val();
                $('.errormessage').text('');
                if (coupon != '') {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                        url: '{{ route('checkCouponcodes') }}',
                        type: 'POST',
                        data: 'coupon=' + coupon + '&total=' + total + '&country=' + couponcountry,
                        success: function (response) {

                            response = JSON.parse(response);

                            console.log(response);

                            if (response.status == 1) {
                                $('.couponmessage').html(response.msg);

                                $('.couponDiscount').html('--');
                                $('#couponDiscount').val(0);
                                $('#couponcode').val('');
                                $('.finaleAmount').text(parseFloat(total));
                                $('#couponDiscounttotal').val(parseFloat(total));

                            } else if (response.status == 2) {
                                $('.couponmessage').html(response.msg);


                                $('.couponDiscount').html('--');
                                $('#couponDiscount').val(0);
                                $('#couponcode').val('');
                                $('.finaleAmount').text(parseFloat(total));
                                $('#couponDiscounttotal').val(parseFloat(total));


                            } else if (response.status == 3) {
                                $('.couponmessage').html(response.msg);


                                $('.couponDiscount').html('--');
                                $('#couponDiscount').val(0);
                                $('#couponcode').val('');
                                $('.finaleAmount').text(parseFloat(total));
                                $('#couponDiscounttotal').val(parseFloat(total));
                            } else if (response.status == 200) {

                                $('.couponDiscount').html(response.curency_symble + response.price);


                                // $('.couponDiscount').text(response.price);
                                $('#couponDiscount').val(response.price);
                                $('#couponcode').val(couponcode);
                                $('.finaleAmount').text(parseFloat(total) - parseFloat(response.price));
                                $('#couponDiscounttotal').val(parseFloat(total) - parseFloat(response.price));
                                $('.couponmessage').html(response.msg);

                            } else {

                            }
                        }
                    });
                } else {
                    $('.couponmessage').text('Please add valid coupon code');
                }
            });

            $('.billingAddress').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    ua_name: {
                        required: function (element) {
                            return $("input[name='addr']:checked").val() == 1;
                        }
                    },
                    ua_address: {
                        required: function (element) {
                            return $("input[name='addr']:checked").val() == 1;
                        }
                    },

                    ua_city: {
                        required: function (element) {
                            return $("#ua_country_add option:selected").val() == "1";
                        }
                    },

                    ua_state: {
                        required: function (element) {
                            return $("input[name='addr']:checked").val() == 1;
                        }
                    },
                    ua_email: {
                        required: function (element) {
                            return $("input[name='addr']:checked").val() == 1;
                        }
                    },
                    mobile_prefix: {
                        required: function (element) {
                            return $("input[name='addr']:checked").val() == 1;
                        }
                    },
                    ua_phone: {
                        required: function (element) {
                            return $("input[name='addr']:checked").val() == 1;
                        },
                        number: true,
                        minlength: 10,
                        maxlength: 15
                    },
                    ua_country: {
                        required: function (element) {
                            return $("input[name='addr']:checked").val() == 1;
                        }
                    },
                    ua_pincode: {
                        required: function (element) {
                            return $("input[name='addr']:checked").val() == 1;
                        }
                    }
                },
                messages: {
                    ua_name: {
                        required: 'Please enter name'
                    },
                    ua_address: {
                        required: 'Please enter address'
                    },

                    ua_city: {
                        required: 'Please enter city'
                    },
                    ua_state: {
                        required: 'Please enter state'
                    },
                    ua_email: {
                        required: 'Please enter email'
                    },
                    mobile_prefix: {
                        required: 'Please Select Code',
                    },
                    ua_phone: {
                        required: 'Please enter phone number',
                        number: 'Please enter valid phone number',
                        minlength: 'Please enter min 10 numbers',
                        maxlength: 'Please enter max 15 numbers'
                    },
                    ua_country: {
                        required: 'Please select country'
                    },
                    ua_pincode: {
                        required: 'Please enter pincode'
                    }
                },
            });


            $('.ua_country').on('change', function () {
                var addresFormID = $(this).parents('form').attr('id');
                selectcountry(addresFormID, 'country')
            });

            $('.ua_state').on('change', function () {
                var addresFormID = $(this).parents('form').attr('id');
                console.log(addresFormID);
                selectcountry(addresFormID, 'state')
            });

            function checkBillingAdress() {


                var billType = $("input[name='addr']:checked").val();
                if (billType == 0) {

                    $('.addresBox').hide();
                } else {

                    $('.addresBox').show();

                }
            }


            $('.billingCheck input').on('change', function () {
                checkBillingAdress();
            });


            checkBillingAdress();


        })


    </script>

@endsection