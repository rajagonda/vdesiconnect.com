@extends('frontend.layout')
@section('title', $title)


@section('header_styles')

    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"/>
@endsection


@section('content')


    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">International Courier</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}"> Home </a></li>
                                <li><a href="{{route('servicePage')}}"> Services </a></li>
                                <li><a> International Courier </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->


                <div class="container stpage">
                    @if (Session::has('flash_message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                <!-- row -->
                    <div class="row">
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <img src="/frontend/images/international-courier.jpg" alt="International Courier" title="International Courier" class="img-fluid">
                        </div>
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <h5 class="sectitle flight pb-1">International <span class="fbold"> Courier </span> </h5>
                            <p class="text-justify">Vdesiconnect international courier and cargo services share the proud moment of being one of the preferred international courier services. As far as the import or export is concerned, <span class="fbold">‘we become quite prominent with our delivery services as we adopt the system of economy and express courier system’. </span> We are predominantly proactive when it comes to ferrying consignment whether it is ferrying a small package or a heavy weight.</p>
                            <h5 class="sectitle flight pb-1">Our <span class="fbold"> Motto </span> </h5>
                            <p class="text-justify">We passionately stand by the customer, when they need us the most for any kind of domestic and international shipping:</p>
                            <ul class="listcontent pt-3">
                                <li>	We ship to all the countries from India. There might be restrictions on some products to some countries which are subjected to custom clearances ex. Millets to Middle East </li>
                                <li>	We ship 30+kgs of consignment, the minimum shipment starts from 1 kg and we have no upper limit. </li>
                            </ul>
                        </div>
                    </div>
                    <!--/ row -->

                        <div class="row">
                            <div class="col-lg-12">
                                <p>Types of courier services which Vdesiconnect offers</p>
                                <p>The customers have an option to choose Vdesiconnect’s courier services based on the need and specificity</p>

                                <ul class="listcontent pt-3">
                                    <li> <span class="fbold">The Economy Courier: </span> The Economy courier is the type of courier which delivers the consignment within 8-10 business days.</li>
                                    <li> <span class="fbold">The Express Courier: </span> The Express Courier is the type of courier which delivers the consignment within 3-5 business days </li>
                                    <li> <span class="fbold">The Customized Courier:  </span> The Customized courier as the name suggests, we do work outside the purview of our regular shipment. Through customized courier, we work exclusively with customers with reference to their specific products or services and help them in getting that. We will shop on behalf of our customers if they need any other products outside our website. For example if they need us to buy some dresses or pooja items or medicines etc., we will buy for them and include in the shipment we deliver along with our website products. The preferred product gets delivered through us to any part of the world in 8-10 business days</li>

                                </ul>
                            </div>
                        </div>

                    <!-- row -->
                    <div class="row py-3">
                        <div class="col-lg-12">
                            <h5 class="sectitle flight pb-3">How it <span class="fbold">Works</span></h5>
                        </div>
                    </div>
                    <!--/ row -->
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="processcol text-center">
                                <h5 class="fgreen text-uppercase"><span>1</span>Login and submit request</h5>
                                <figure>
                                    <span class="icon-login icomoon"></span>
                                </figure>
                                <p>Login and provide the information in all the fields an submit the request.</p>
                            </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="processcol text-center processarrow">
                                <h5 class="fgreen text-uppercase"><span>2</span>Review Request</h5>
                                <figure>
                                    <span class="icon-writing icomoon"></span>
                                </figure>
                                <p>The crew of every department analyse the request and collect the required information.</p>
                            </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="processcol text-center processarrow">
                                <h5 class="fgreen text-uppercase"><span>3</span>Contact & Quote</h5>
                                <figure>
                                    <span class="icon-support icomoon"></span>
                                </figure>
                                <p>: Our crew will connect with you and consider your requirements, assist with the suitable resources and pricing.</p>
                            </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="processcol text-center processarrow">
                                <h5 class="fgreen text-uppercase"><span>4</span>Payment</h5>
                                <figure>
                                    <span class="icon-wallet icomoon"></span>
                                </figure>
                                <p>Sends invoice for the customer endorsement and payment.</p>
                            </div>
                        </div>
                        <!--/ col -->

                    </div>
                    <!--/ row -->


                    @if (Auth::guard('web')->check())

                        <div class="whitebox p-4">
                            <!-- row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <h5 class="sectitle flight pb-1">Contact us & <b class="fbold"> Send Your
                                            Requirement </b></h5>
                                    <p>Our Executive will follow following Contact details, Please Give your Genuine
                                        Contact Details</p>
                                </div>
                            </div>
                            <!--/ row -->


                            @include('frontend.service.includs.form')

                        </div>

                    @else

                        <div class="whitebox p-4 my-3">
                            <div class="row justify-content-center">
                                <div class="col-lg-6 text-center">
                                    <h5 class="sectitle flight pb-3">Login <span class="fbold">&amp; Send Request</span></h5>
                                    <p>Login and Submit Request with all your Personal Details to Communicate with you Easily and You Can Track Payment Details, Invoice Details Etc.</p>
                                    <a href="{{route('userlogin')}}" class="greenlink">Login and Send Request</a>
                                </div>
                            </div>
                        </div>

                    @endauth

                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>

@endsection

@section('footerScripts')
    <script>
        $(function () {


            $('#service_medicle').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    s_contact_person: {
                        required: true
                    },
                    s_address_line_1: {
                        required: true
                    },
                    s_address_line_2: {
                        required: true
                    },
                    s_country: {
                        required: true
                    },
                    s_state: {
                        required: true
                    },
                    s_city: {
                        required: true
                    },
                    s_options: {
                        required: true
                    },
                    s_date: {
                        required: true
                    },
                    s_time: {
                        required: true
                    },
                    s_email: {
                        required: true,
                        email: true
                    },
                    s_phone: {
                        required: true
                    },
                    s_msg: {
                        required: true
                    }
                },
                messages: {
                    s_contact_person: {
                        required: 'Contact Person is required'
                    },
                    s_address_line_1: {
                        required: 'Addres line 1 is required'
                    },
                    s_address_line_2: {
                        required: 'Addres line 1 is required'
                    },
                    s_country: {
                        required: 'country is required'
                    },
                    s_state: {
                        required: 'State is required'
                    },
                    s_city: {
                        required: 'City is required'
                    },
                    s_options: {
                        required: 'Select Medicle Service'
                    },
                    s_date: {
                        required: 'Date is required'
                    },
                    s_time: {
                        required: 'Time is required'
                    },
                    s_email: {
                        required: 'Enter email',
                        email: 'Enter a valid email'
                    },
                    s_phone: {
                        required: 'Phone number is required',
                    },
                    s_msg: {
                        required: 'Message is required',
                    }

                }
            });


        });

    </script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>

    <script>
        $(function () {
            $('.datepicker').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                yearRange: "-100:+0"
            });
        });
    </script>

{{--    @include('frontend.user.scripts')--}}

    <script>
        $(function () {
            $('.ua_country').on('change', function () {
                var addresFormID = $(this).parents('form').attr('id');
                selectcountry(addresFormID, 'country')
            });
        });
    </script>



@endsection


