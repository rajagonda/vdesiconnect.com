@extends('frontend.layout')
@section('title', $title)


@section('header_styles')

    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"/>
@endsection


@section('content')



    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Online Tutor</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}"> Home </a></li>
                                <li><a href="{{route('servicePage')}}"> Services </a></li>
                                <li><a> Online Tutor </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container stpage">

                    @if (Session::has('flash_message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                <!-- row -->
                    <div class="row">
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <img src="/frontend/images/onlinetutor01.jpg" alt="Get the best online tutoring in your City" title="Get the best online tutoring in your City" class="img-fluid">
                        </div>
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <h5 class="sectitle flight pb-3">Welcome to a new way of learning  <span class="fbold">through online tutoring</span></h5>
                            <p class="text-justify"> In today’s context, the process of teaching and learning has undergone a dramatic change, but the modest reality is that ‘The methodology of teaching has remained the same since decades’. The traditional and classroom based learning is slowly getting obsolete. </p>
                            <p>With the convergence of technology, online tutoring has been literally transformed as a powerful learning curve for aspirants with engaging content which integrates itself with illustrations. Above all, online tutoring becomes the preferred choice which is made available at his/her doorstep convenience.</p>
                        </div>
                    </div>
                    <!--/ row -->

                        <!-- row -->
                        <div class="row pt-4">
                            <!-- col 12 -->
                            <div class="col-lg-12">
                                <h5 class="sectitle flight pb-3">We incorporate the imaginable  <span class="fbold"> area of expertise </span></h5>
                                <p>As the fact goes by, there are almost close to 40,000 tutors across the country and the numbers keep increasing day-in and day-out. Vdesiconnect have been in the process to identify the best online teachers, home tutors or the domain experts who have their own unique way to train the aspirant with their chosen subject and also help them through their assignment help.</p>
                                <h5 class="sectitle flight pb-3">We have online tutors on-board who train various aspirants in the subjects concerned with:</h5>
                                <ul class="listcontent">
                                    <li>Mathematics</li>
                                    <li>Science</li>
                                    <li>Online Chess Training</li>
                                    <li>Music: Keyboard, Guitar, Vocal etc.</li>
                                    <li>Others</li>
                                </ul>

                                <h5 class="sectitle flight pb-3">Our Philosophy</h5>
                                <p>Our philosophy since inception has been centered around to connect the right online tutor thereby enabling and imparting a new way of teaching paradigm.</p>
                                <p>The success ratio that Vdesiconnect so far has been quite flawless, <span class="fbold">‘we have been able to successfully create a talent pool with a handful of subject matter experts (SME) to impart online education which is very impeccable’</span></p>

                            </div>
                            <!--/ col 12-->
                        </div>
                        <!--/ row -->

                    <!-- row -->
                    <div class="row py-3">
                        <div class="col-lg-12">
                            <h5 class="sectitle flight pb-3">How it <span class="fbold">Works</span></h5>
                        </div>
                    </div>
                    <!--/ row -->
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="processcol text-center">
                                    <h5 class="fgreen text-uppercase"><span>1</span>Login/Registration</h5>
                                    <figure>
                                        <span class="icon-login icomoon"></span>
                                    </figure>
                                    <p>The aspirant needs to register himself in order to avail online tutoring and get connected to the best available teachers online. </p>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="processcol text-center processarrow">
                                    <h5 class="fgreen text-uppercase"><span>2</span>Allocation of Tutor</h5>
                                    <figure>
                                        <span class="icon-writing icomoon"></span>
                                    </figure>
                                    <p>Once the aspirant registers himself in the web portal, he or she would be guided through the free structure which incurs for a particular online tutoring. The backend team allocates the right tutor after the payment is remitted.</p>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="processcol text-center processarrow">
                                    <h5 class="fgreen text-uppercase"><span>3</span>Personalized Lesson Planse</h5>
                                    <figure>
                                        <span class="icon-support icomoon"></span>
                                    </figure>
                                    <p>Based on the particular subject, the backend team plans for a personalized lesson plans. The personalized lesson plan would also include the total duration of the course and the average time taken by the tutor with reference to the subject.</p>
                                </div>
                            </div>
                            <!--/ col -->

                        </div>
                        <!--/ row -->

                    <!-- row -->
                    <div class="row py-5">
                        <!-- col -->
                        <div class="col-lg-6 order-lg-last col-md-6 align-self-center">
                            <img src="/frontend/images/onlinetutor02.jpg" alt="VdesiConnect online Tutor Services" title="VdesiConnect online Tutor Services" class="img-fluid">
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 align-self-center col-md-6">
                            <h5 class="sectitle flight pb-3">What makes   <span class="fbold">Vdesiconnect more unique</span>
                            </h5>
                            <ul class="listcontent">
                                <li>We are reputed to bring about conceptual learning and our tutors are the subject matter experts.  </li>
                                <li>The aspirant can have the best of the online tutoring with flexible timings. </li>
                                <li>The backend teams at Vdesiconnect become more flexible to allocate the right tutor for your subject. </li>
                                <li>Most of the subjects are being taught one-one thru Skype.  </li>
                                <li>The aspirant can seamlessly connect to his online tutor, for any doubt clarification </li>
                                <li>Vdesiconnect persistently helps you to achieve better grades in your chosen subject. </li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

                    @if (Auth::guard('web')->check())
                        <div class="whitebox p-4">
                            <!-- row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <h5 class="sectitle flight pb-1">Contact us & <b class="fbold"> Send Your
                                            Requirement </b></h5>
                                    <p>Our Executive will follow following Contact details, Please Give your Genuine Contact Details</p>
                                </div>
                            </div>
                            <!--/ row -->


                            @include('frontend.service.includs.form')

                        </div>
                    @else

                        <div class="whitebox p-4 my-3">
                            <div class="row justify-content-center">
                                <div class="col-lg-6 text-center">
                                    <h5 class="sectitle flight pb-3">Login <span class="fbold">&amp; Send Request</span>
                                    </h5>
                                    <p>Login and Submit Request with all your Personal Details to Communicate with you Easily and You Can Track Payment Details, Invoice Details Etc.</p>
                                    <a href="{{route('userlogin')}}" class="greenlink">Login and Send Request</a>
                                </div>
                            </div>
                        </div>

                    @endauth


                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>

@endsection


@section('footerScripts')
    <script>
        $(function () {


            $('#contactform').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    s_contact_person: {
                        required: true
                    },
                    s_address_line_1: {
                        required: true
                    },
                    s_address_line_2: {
                        required: true
                    },
                    s_country: {
                        required: true
                    },
                    s_state: {
                        required: true
                    },
                    s_city: {
                        required: true
                    },
                    s_options: {
                        required: true
                    },
                    s_date: {
                        required: true
                    },
                    s_time: {
                        required: true
                    },
                    s_email: {
                        required: true,
                        email: true
                    },
                    s_phone: {
                        required: true
                    },
                    s_msg: {
                        required: true
                    }
                },
                messages: {
                    s_contact_person: {
                        required: 'Contact Person is required'
                    },
                    s_address_line_1: {
                        required: 'Addres line 1 is required'
                    },
                    s_address_line_2: {
                        required: 'Addres line 1 is required'
                    },
                    s_country: {
                        required: 'country is required'
                    },
                    s_state: {
                        required: 'State is required'
                    },
                    s_city: {
                        required: 'City is required'
                    },
                    s_options: {
                        required: 'Select Tutor'
                    },
                    s_date: {
                        required: 'Date is required'
                    },
                    s_time: {
                        required: 'Time is required'
                    },
                    s_email: {
                        required: 'Enter email',
                        email: 'Enter a valid email'
                    },
                    s_phone: {
                        required: 'Phone number is required',
                    },
                    s_msg: {
                        required: 'Message is required',
                    }

                }
            });


        });

    </script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>

    <script>
        $(function () {
            $('.datepicker').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                yearRange: "-100:+0"
            });
        });
    </script>

    <script>
        $(function () {
            $('.ua_country').on('change', function () {
                var addresFormID = $(this).parents('form').attr('id');
                selectcountry(addresFormID, 'country')
            });
        });
    </script>




@endsection