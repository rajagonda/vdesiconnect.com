<main>
    <!-- sub apge -->
    <section class="subpage">
        <!-- sub page header -->
        <section class="subpageheader">
            <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
            <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <article class="pagetitle">
                            <h1 class="px20 py20">Online Radio</h1>
                        </article>
                        <!-- brudcrumb -->
                        <ul class="brcrumb">
                            <li><a href="{{route('home')}}"> Home </a></li>
                            <li><a href="{{route('servicePage')}}"> Services </a></li>
                            <li><a> Online Radio </a></li>
                        </ul>
                        <!--/ brudcrumb -->
                    </div>
                </div>
            </div>
        </section>
        <!--/ sub page header -->
        <!--sub page main -->
        <section class="subpagemain">
            <!-- container -->
            <div class="container stpage">
                <!-- row -->
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <img src="/frontend/images/puc.jpg" alt="" title="" class="img-fluid">
                    </div>
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ sub page main -->
    </section>
    <!--/ sub page -->
</main>