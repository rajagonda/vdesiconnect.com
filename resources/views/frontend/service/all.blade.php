@extends('frontend.layout')
@section('title', $title)


@section('header_styles')

    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"/>
@endsection


@section('content')


    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20"> All Services</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}"> Home </a></li>
                                <li><a> Services </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->


                <div class="container stpage">
                    @if (Session::has('flash_message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                @endif
                <!-- row -->
                    <div class="row">

                        @if(count(serviceInfoData()))
                            @foreach(serviceInfoData() AS $serviceInfo)

                                <div class="col-lg-4 col-sm-6">


                                    <div class="service-col">

                                        <a class="" href="{{route('servicePage', ['service'=>$serviceInfo])}}">
                                            <h4 class="h4 flight">
                                                {{ wordSort($serviceInfo) }}
                                            </h4>


                                            <?php
                                            switch ($serviceInfo) {
                                            case 'medical-assistance':
                                            ?>

                                            <img class="img-fluid"  src="{{ url('/frontend/images/medicalservicesimg01.jpg') }}">
                                           <p>We have been offering stipulated solutions to our clients who seek assistance for the medical services</p>
                                            <?php
                                            break;


                                            case 'online-tutor':
                                            ?>
                                            <img class="img-fluid" src="{{ url('/frontend/images/onlinetutor01.jpg') }}">
                                            <p>VdesiConnect alleviates the life of every individual by supporting them with the associated online tutors</p>
                                            <?php
                                            break;

                                            case 'property-management':
                                            ?>
                                            <img class="img-fluid" src="{{ url('/frontend/images/propertymanagement01.jpg') }}">
                                            <p>VdesiConnect endows in bringing impactful property management services to the reach of our clientele</p>
                                            <?php
                                            break;

                                            case 'visa-support-services':
                                            ?>
                                            <img class="img-fluid" src="{{ url('/frontend/images/visaservice02img.jpg') }}">
                                            <p>At VdesiConnect, we provide visa support services for people of multiple nationalities who are in the aspiration to travel to any nation..</p>
                                            <?php
                                            break;

                                            case 'visitors-insurance':
                                            ?>
                                            <img class="img-fluid" src="{{ url('/frontend/images/visitorsinsurance01.jpg') }}">
                                            <p>We are always in the exertion to develop all-inclusive insurance services tailored in the direction to adapt with the client requirements</p>
                                            <?php
                                            break;

                                            case 'real-estate':
                                            ?>
                                            <img class="img-fluid" src="{{ url('/frontend/images/realestate-service-img.jpg') }}">
                                            <p>We show a complete contemporary direction for your real estate services. </p>
                                            <?php
                                            break;

                                            case 'summer-enrichment':
                                            ?>
                                            <img class="img-fluid" src="{{ url('/frontend/images/summerinrichment01.jpg') }}">
                                            <p>Feeling Kids are staying Home and doing nothing in this Summer. </p>
                                            <?php
                                            break;

                                            case 'web-developement':
                                            ?>
                                            <img class="img-fluid" src="{{ url('/frontend/images/webdev01.jpg') }}">
                                            <p>Our passion for work results in complete personalization that aims to the excellent user experiences. </p>
                                            <?php
                                            break;

                                            case 'event-organization':
                                            ?>
                                            <img class="img-fluid" src="{{ url('/frontend/images/event-organization.jpg') }}">
                                            <p>Dedicated to facilitating smooth events, Vdesiconnect has been providing professional Master of Ceremonies services. </p>
                                            <?php
                                            break;

                                            case 'online-radio':
                                            ?>
                                            <img class="img-fluid" src="{{ url('/frontend/images/puc.jpg') }}">
                                            <p>We will Coming Soon</p>
                                            <?php
                                            break;

                                            case 'international-courier':
                                            ?>
                                            <img class="img-fluid" src="{{ url('/frontend/images/international-courier.jpg') }}">
                                            <p>Vdesiconnect International Courier and Cargo offers a unique service to send parcels to anywhere in the world from India. </p>
                                            <?php
                                            break;


                                            case 'franchise-enquiry':
                                            ?>
                                            <img class="img-fluid" src="{{ url('/frontend/images/franchise.jpg') }}">
                                            <p>The completion of this form does not obligate you or Vdesiconnect Private Limited in any way and does not form a binding contract.  </p>
                                            <?php
                                            break;





                                            default:
                                            ?>
                                            <img class="img-fluid" src="{{ url('/frontend/images/puc.jpg') }}">

                                            <p> all text</p>

                                            <?php

                                            break;
                                            }
                                            ?>

                                        </a>

                                    </div>


                                </div>




                            @endforeach

                        @endif


                    </div>
                    <!--/ row -->


                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>

@endsection

@section('footerScripts')
    <script>
        $(function () {


            $('#service_medicle').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    s_contact_person: {
                        required: true
                    },
                    s_address_line_1: {
                        required: true
                    },
                    s_address_line_2: {
                        required: true
                    },
                    s_country: {
                        required: true
                    },
                    s_state: {
                        required: true
                    },
                    s_city: {
                        required: true
                    },
                    s_options: {
                        required: true
                    },
                    s_date: {
                        required: true
                    },
                    s_time: {
                        required: true
                    },
                    s_email: {
                        required: true,
                        email: true
                    },
                    s_phone: {
                        required: true
                    },
                    s_msg: {
                        required: true
                    }
                },
                messages: {
                    s_contact_person: {
                        required: 'Contact Person is required'
                    },
                    s_address_line_1: {
                        required: 'Addres line 1 is required'
                    },
                    s_address_line_2: {
                        required: 'Addres line 1 is required'
                    },
                    s_country: {
                        required: 'country is required'
                    },
                    s_state: {
                        required: 'State is required'
                    },
                    s_city: {
                        required: 'City is required'
                    },
                    s_options: {
                        required: 'Select Medicle Service'
                    },
                    s_date: {
                        required: 'Date is required'
                    },
                    s_time: {
                        required: 'Time is required'
                    },
                    s_email: {
                        required: 'Enter email',
                        email: 'Enter a valid email'
                    },
                    s_phone: {
                        required: 'Phone number is required',
                    },
                    s_msg: {
                        required: 'Message is required',
                    }

                }
            });


        });

    </script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>

    <script>
        $(function () {
            $('.datepicker').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                yearRange: "-100:+0"
            });
        });
    </script>

    {{--    @include('frontend.user.scripts')--}}

    <script>
        $(function () {
            $('.ua_country').on('change', function () {
                var addresFormID = $(this).parents('form').attr('id');
                selectcountry(addresFormID, 'country')
            });
        });
    </script>



@endsection


