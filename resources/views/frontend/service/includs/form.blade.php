<!-- service form -->
<form method="POST" id="service_medicle" autocomplete="off"
      action="{{route('servicePage', ['service'=>$service_type])}}"
      accept-charset="UTF-8"
      class="serviceform">
    {{ csrf_field() }}

    <input type="hidden" value="{{$service_type}}" name="s_type">

    <div class="row">
        <!-- col -->
        <div class="col-lg-4">
            <div class="form-group">
                <label>Contact Person Name</label>
                <div class="input-group">
                    <input name="s_contact_person" type="text"
                           placeholder="Enter Contact Person Name" class="form-control">
                </div>
            </div>
        </div>
        <!--/ col -->

        <!-- col -->
        <div class="col-lg-4">
            <div class="form-group">
                <label>Address Line 1</label>
                <div class="input-group">
                    <input name="s_address_line_1" type="text" placeholder="Address Line 01"
                           class="form-control">
                </div>
            </div>
        </div>
        <!--/ col -->

        <!-- col -->
        <div class="col-lg-4">
            <div class="form-group">
                <label>Address Line 2</label>
                <div class="input-group">
                    <input name="s_address_line_2" type="text" placeholder="Address Line 02"
                           class="form-control">
                </div>
            </div>
        </div>
        <!--/ col -->

        <!-- col -->
        <div class="col-lg-4">
            <div class="form-group">
                <label>Service Required Country</label>
                <div class="input-group">
                    <select name="s_country" class="form-control ua_country">
                        <option value="">--Select Country--</option>
                        <?php
                        $countries = getCountries(); ?>
                        @foreach($countries as $ks=>$s)
                            <option value="{{ $ks }}">
                                {{ $s }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <!--/ col -->

        <!-- col -->
        <div class="col-lg-4">
            <div class="form-group">
                <label>Select State</label>
                <div class="input-group">
                    <select class="form-control ua_state" required name="s_state">
                        <option value="">--Select State--</option>
                    </select>
                </div>
            </div>
        </div>
        <!--/ col -->

        <!-- col -->
        <div class="col-lg-4">
            <div class="form-group">
                <label>City</label>
                <div class="input-group">
                    <input name="s_city" type="text" required
                           placeholder="City" class="form-control">

                </div>
            </div>
        </div>
        <!--/ col -->

        <!-- col -->
        <div class="col-lg-4">
            <div class="form-group">
                <label>Select {{wordSort($service_type)}} Service</label>
                <div class="input-group">
                    <select name="s_options" class="form-control">
                        <option value="">--Select--</option>
                        @if(count($service_options)> 0)
                            @foreach($service_options AS $serviceName)
                                <option value="{{ $serviceName }}">{{ $serviceName }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <!--/ col -->

        <!-- col -->
        <div class="col-lg-4">
            <div class="form-group">
                <label class="w-100">Date of Service Required</label>
                <div class="input-group">
                    <input name="s_date" class="form-control datepickerFront"/>
                </div>
            </div>
        </div>
        <!--/ col -->

        <!-- col -->
        <div class="col-lg-4">
            <div class="form-group">
                <label>Preferred Time to Contact</label>
                <div class="input-group">
                    <select class="form-control" name="s_time">
                        @if(count(preferTimeings())> 0)
                            <option>--</option>
                            @foreach(preferTimeings() AS $preferTimeing)
                                <option>{{$preferTimeing}}</option>
                            @endforeach
                        @endif

                    </select>

                    {{--                                            <input name="s_time" type="text" placeholder="Ex: 10 AM"--}}
                    {{--                                                   class="form-control">--}}
                </div>
            </div>
        </div>
        <!--/ col -->

        <!-- col -->
        <div class="col-lg-4">
            <div class="form-group">
                <label>Contact Email</label>
                <div class="input-group">
                    <input name="s_email" type="email" placeholder="Enter Correct Email Address"
                           class="form-control">
                </div>
            </div>
        </div>
        <!--/ col -->

        <!-- col -->
        <div class="col-lg-4">
            <div class="form-group">
                <label>Contact Phone</label>
                <div class="input-group">

                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <select name="se_mobile_prefix" required
                                    class="">
                                <option value="">Select</option>
                                @if(sizeof(getCountrycodes())>0)
                                    @foreach(getCountrycodes() as $key=>$value)
                                        <option  value="{{ $key }}">+{{ $value }}</option>
                                    @endforeach
                                @endif
                            </select>

                        </span>
                    </div>

                    <input name="s_phone" type="number" maxlength="12" minlength="9" placeholder="Concerned Contact Number"
                           class="form-control">
                </div>
            </div>
        </div>
        <!--/ col -->

        <!-- col -->
        <div class="col-lg-12">
            <div class="form-group">
                <label>Message </label>
                <div class="input-group">
                <textarea name="s_msg" class="form-control" rows="5"
                          placeholder="Enter your Message"></textarea>
                </div>
            </div>
        </div>
        <!--/ col -->
    </div>
    <!--/ row -->

    <!-- row -->
    <div class="row pt-3">
        <div class="col-lg-4">
            <input type="submit" value="Submit Request" class="greenlink">
        </div>
    </div>
    <!--/ row -->
</form>
<!--/ service form -->