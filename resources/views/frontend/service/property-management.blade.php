@extends('frontend.layout')
@section('title', $title)


@section('header_styles')

    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"/>
@endsection


@section('content')



    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Property Management</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}"> Home </a></li>
                                <li><a href="{{route('servicePage')}}"> Services </a></li>
                                <li><a> Property Management </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container stpage">
                    @if (Session::has('flash_message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                <!-- row -->
                    <div class="row">
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <img src="/frontend/images/propertymanagement01.jpg" alt="Property Management" title="Property Management" class="img-fluid">
                        </div>
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <h5 class="sectitle flight pb-3"> Property <span class="fbold"> management</span>
                            </h5>
                            <p class="text-justify">Vdesiconnect, an accomplished property management consultant helps you in maintaining your decent home, be it an apartment duplex, villa. Rest assured we can be with you for this crucial decision.</p>
                            <p class="text-justify">As a property management service consultant, value your property and constantly ensure that we provide an array of comprehensive services which can significantly bring about a reduction in the maintenance cost and further ensuring "rentals which benefits both the tenant and owners"</p>


                        </div>
                    </div>
                    <!--/ row -->

                        <!-- row -->
                        <div class="row pt-4">
                            <div class="col-lg-12">
                                <h6 class="h6 fbold">Take hold of our gratifying services in property management</h6>
                                <p class="text-justify">Vdesiconnect endows in bringing impactful property management services to the reach of our clientele. We engage in offering professional methods towards management of the building and the services that we provide are of:</p>
                                <ul class="listcontent">
                                    <li>Rent</li>
                                    <li>Maintenance</li>
                                    <li>Vacate/Cleaning</li>
                                    <li>Property Taxes</li>
                                </ul>
                                <p class="text-justify">We clearly assist you with the initiatives from the bottom of every service. We take the helm of offering affordable pricings, resolving customer service, and sustaining accounting approaches.</p>
                            </div>
                        </div>
                        <!--/ row -->

                    <!-- row -->
                    <div class="row py-3">
                        <div class="col-lg-12">
                            <h5 class="sectitle flight pb-3">How it <span class="fbold">Works</span></h5>
                        </div>
                    </div>
                    <!--/ row -->
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="processcol text-center">
                                <h5 class="fgreen text-uppercase"><span>1</span>Login and submit request</h5>
                                <figure>
                                    <span class="icon-login icomoon"></span>
                                </figure>
                                <p>Login and provide the information in all the fields an submit the request.</p>
                            </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="processcol text-center processarrow">
                                <h5 class="fgreen text-uppercase"><span>2</span>Review Request</h5>
                                <figure>
                                    <span class="icon-writing icomoon"></span>
                                </figure>
                                <p>The crew of every department analyse the request and collect the required
                                    information.</p>
                            </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="processcol text-center processarrow">
                                <h5 class="fgreen text-uppercase"><span>3</span>Contact & Quote</h5>
                                <figure>
                                    <span class="icon-support icomoon"></span>
                                </figure>
                                <p>Our crew will connect with you and consider your requirements, assist with the
                                    suitable resources and pricing.</p>
                            </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="processcol text-center processarrow">
                                <h5 class="fgreen text-uppercase"><span>4</span>Payment</h5>
                                <figure>
                                    <span class="icon-wallet icomoon"></span>
                                </figure>
                                <p>Sends invoice for the customer endorsement and payment.</p>
                            </div>
                        </div>
                        <!--/ col -->

                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row py-5">
                        <!-- col -->
                        <div class="col-lg-6 order-lg-last col-md-6 align-self-center">
                            <img src="/frontend/images/propertymanagement02.jpg" alt="leading property management service with Vdesiconnect" title="leading property management service with Vdesiconnect" class="img-fluid">
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <h5 class="sectitle flight pb-3">How Vdesiconnect helps <span class="fbold">in managing the property</span> </h5>
                            <p class="text-justify">We will take care of your property without any hassles for you. We will tour your property for the prospective tenants, clarify and clear all their concerns about the property, convey your terms & conditions to them. Also, we collect rent on time and deposit in your respective account.</p>
                            <ul class="listcontent">
                                <li>We are instrumental in repair work such as electrical, plumbing etc in the property before it is being handed over to the tenants for their occupancy. </li>
                                <li>We also take care of periodic filing of property taxes; submit the bills for any reimbursement. </li>
                            </ul>
                            <p class="text-justify">Our services will help you in carrying out essential tasks to protect you from facing extended vacancies. Every landlord moves with the pleasant and fruitful experiences as we expunge all the inconveniences. We endorse in showing the extreme quality of property management services that let clientele that their property is in the right hands.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

                    @if (Auth::guard('web')->check())

                        <div class="whitebox p-4">
                            <!-- row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <h5 class="sectitle flight pb-1">Contact us & <b class="fbold"> Send Your
                                            Requirement </b></h5>
                                    <p>Our Executive will follow following Contact details, Please Give your Genuine
                                        Contact Details</p>
                                </div>
                            </div>
                            <!--/ row -->

                            @include('frontend.service.includs.form')
                        </div>

                    @else

                        <div class="whitebox p-4 my-3">
                            <div class="row justify-content-center">
                                <div class="col-lg-6 text-center">
                                    <h5 class="sectitle flight pb-3">Login <span class="fbold">&amp; Send Request</span>
                                    </h5>
                                    <p>Login and Submit Request with all your Personal Details to Communicate with you
                                        Easily and You Can Track Payment Details, Invoice Details Etc.</p>
                                    <a href="{{route('userlogin')}}" class="greenlink">Login and Send Request</a>
                                </div>
                            </div>
                        </div>

                    @endauth


                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>

@endsection


@section('footerScripts')
    <script>
        $(function () {


            $('#contactform').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    s_contact_person: {
                        required: true
                    },
                    s_address_line_1: {
                        required: true
                    },
                    s_address_line_2: {
                        required: true
                    },
                    s_country: {
                        required: true
                    },
                    s_state: {
                        required: true
                    },
                    s_city: {
                        required: true
                    },
                    s_options: {
                        required: true
                    },
                    s_date: {
                        required: true
                    },
                    s_time: {
                        required: true
                    },
                    s_email: {
                        required: true,
                        email: true
                    },
                    s_phone: {
                        required: true
                    },
                    s_msg: {
                        required: true
                    }
                },
                messages: {
                    s_contact_person: {
                        required: 'Contact Person is required'
                    },
                    s_address_line_1: {
                        required: 'Addres line 1 is required'
                    },
                    s_address_line_2: {
                        required: 'Addres line 1 is required'
                    },
                    s_country: {
                        required: 'country is required'
                    },
                    s_state: {
                        required: 'State is required'
                    },
                    s_city: {
                        required: 'City is required'
                    },
                    s_options: {
                        required: 'Select Property Service'
                    },
                    s_date: {
                        required: 'Date is required'
                    },
                    s_time: {
                        required: 'Time is required'
                    },
                    s_email: {
                        required: 'Enter email',
                        email: 'Enter a valid email'
                    },
                    s_phone: {
                        required: 'Phone number is required',
                    },
                    s_msg: {
                        required: 'Message is required',
                    }

                }
            });


        });

    </script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>

    <script>
        $(function () {
            $('.datepicker').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                yearRange: "-100:+0"
            });
        });
    </script>
    <script>
        $(function () {
            $('.ua_country').on('change', function () {
                var addresFormID = $(this).parents('form').attr('id');
                selectcountry(addresFormID, 'country')
            });
        });
    </script>



@endsection