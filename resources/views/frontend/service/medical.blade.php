@extends('frontend.layout')
@section('title', $title)


@section('header_styles')

    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"/>
@endsection


@section('content')


    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Medical Assistance</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}"> Home </a></li>
                                <li><a href="{{route('servicePage')}}"> Services </a></li>
                                <li><a> Medical Assistance </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container stpage">
                    @if (Session::has('flash_message'))
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                <!-- row -->
                    <div class="row">
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <img src="/frontend/images/medicalservicesimg01.jpg" alt="We are open to medical requests" title="We are open to medical requests" class="img-fluid">
                        </div>
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <h5 class="sectitle flight pb-3">Your destination for prompt  <span class="fbold">medical attention </span></h5>
                            <p>At some point of time in our lives, we all might have been through a situation of health crisis; which puts an individual in a frantic situation, this is especially more felt with senior citizens, parents whose sons & daughters are settled abroad.
                            </p>
                            <p>They land up in a state of panic and ultimately puts them in a clueless condition of where to go for an instant medical checkup. Under this situation, the <span class="fbold"> Medical Assistance </span> would become the need of the hour and if the assistance is being guided properly through the chosen <span class="fbold"> home health care professionals </span> who can guide them about the online system of comprehensive health care, this could be nothing short of a <span class="fbold">‘Blessings in Disguise’</span>
                            </p>

                        </div>
                    </div>
                    <!--/ row -->
                    <!-- row -->
                    <div class="row pt-4">
                        <div class="col-lg-12">
                            <h5 class="sectitle flight pb-3">The tangible medical solutions  <span class="fbold"> made possible through Vdesiconnect </span></h5>
                            <p>For millions of people who become desperate and seek an instant medical solution, Vdesiconnect are the home health care professionals who become responsible to provide a tangible platform online which assists an individual in  <span class="fbold">‘coordinating with the right doctor and series of right diagnostic tests’</span> which could ascertain the health problem and possibly enabling them to recover themselves from illnesses.
                            </p>
                            <p>Apart from that as a <span class="fbold"> home health agency </span>, we help the individual in obtaining the medicines and last but not least, we finally upload the medical health records onto the website for the digital storage of their medical health records for more transparency and future retrieval with just a click of a button.
                            </p>
                        </div>
                    </div>
                    <!--/ row -->
                    <!-- row -->
                    <div class="row py-1">
                        <div class="col-lg-12">
                            <h5 class="sectitle flight pb-3">We care for you at every step of your   <span class="fbold"> request and assist with following medical services:  </span></h5>
                            <ul class="listcontent">
                                <li>Wellness check ups </li>
                                <li>Diagnostic Services</li>
                                <li>Dental checkups (Root Canal, Dental Implants, Fillings etc) </li>
                                <li>Vision (Laser Surgery, Vision check ups, glasses etc.)</li>
                                <li>Physiotherapy</li>
                            </ul>
                        </div>
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row pt-5">
                        <div class="col-lg-12">
                            <h5 class="sectitle flight pb-3">Modus  <span class="fbold">Operandi-Vdesi Connect</span></h5>
                        </div>
                    </div>
                    <!--/ row -->
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="processcol text-center">
                                <h5 class="fgreen text-uppercase"><span>1</span>Login and Submit Requests</h5>
                                <figure>
                                    <span class="icon-login icomoon"></span>
                                </figure>
                                <p>The individuals need to register through the platform and furnish the credentials of the type of medical services intended and submit the request.         </p>
                            </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-md-6 col-sm-6 ">
                            <div class="processcol text-center processarrow">
                                <h5 class="fgreen text-uppercase"><span>2</span>Review Requests</h5>
                                <figure>
                                    <span class="icon-writing icomoon"></span>
                                </figure>
                                <p>The backend crew of Vdesiconnect analyze  the request of the individual and review the required medical service </p>
                            </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-md-6 col-sm-6 ">
                            <div class="processcol text-center processarrow">
                                <h5 class="fgreen text-uppercase"><span>3</span>Contact and Quote</h5>
                                <figure>
                                    <span class="icon-support icomoon"></span>
                                </figure>
                                <p>The backend crew after the review of your submitted query, will gather the details from you and would assist you with suitable resources and pricing for the same.
                                </p>
                            </div>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="processcol text-center processarrow">
                                <h5 class="fgreen text-uppercase"><span>4</span>Payment</h5>
                                <figure>
                                    <span class="icon-wallet icomoon"></span>
                                </figure>
                                <p>Vdesiconnect crew will contact you, gather details, send an invoice for payment for this service. Once the payment is made, our staff member will further assist your parents/relatives for hospital visit, medicines, lab tests, doctor suggestions if any etc.
                                </p>
                                <p>The registered member would need to pay for the treatments incurred.</p>

                            </div>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row py-5">
                        <!-- col -->
                        <div class="col-lg-6 order-lg-last col-md-6 align-self-center">
                            <img src="/frontend/images/medicalservicesimg02.jpg" alt="" title="" class="img-fluid">
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-6 col-md-6 align-self-center">
                            <h5 class="sectitle flight pb-3">Medical establishments and healthcare providers harnesses deeper engagement
                            </h5>
                            <p>Healthcare providers in the marketplace can also harness the potential of Vdesiconnect when it comes to build their presence. Vdesiconnects helps in growing medical establishments with more and more patient engagement.
                            </p>

                            <h5 class="sectitle flight pb-3">How Vdesiconnect  <span class="fbold">functions</span></h5>
                            <ul class="listcontent">
                                <li>The dedicated staff are always proactive with reference to picking the patients from their residence and accompany them to the nearest hospital.
                                </li>
                                <li>The staff gets involved in arranging the medical check-ups, diagnostics and drop the patient back to the residence.
                                </li>
                                <li>Vdesiconnect portal becomes a transparent medium which includes the doctor’s suggestions pertaining to the particular patient. Above all, all the medical records of the patient is kept safe in the portal for visibility and future retrieval
                                </li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->

                    @if (Auth::guard('web')->check())
                        <div class="whitebox p-4">
                            <!-- row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <h5 class="sectitle flight pb-1">Contact us & <b class="fbold"> Send Your  Requirement </b></h5>
                                    <p>Our Executive will follow following Contact details, Please Give your Genuine Contact Details</p>
                                </div>
                            </div>
                            <!--/ row -->
                            <?php
                            ?>
                            @include('frontend.service.includs.form')
                        </div>
                    @else

                        <div class="whitebox p-4 my-3">
                            <div class="row justify-content-center">
                                <div class="col-lg-6 text-center">
                                    <h5 class="sectitle flight pb-3">Login <span class="fbold">&amp; Send Request</span></h5>
                                    <p>Login and Submit Request with all your Personal Details to Communicate with you  Easily and You Can Track Payment Details, Invoice Details Etc.</p>
                                    <a href="{{route('userlogin')}}" class="greenlink">Login and Send Request</a>
                                </div>
                            </div>
                        </div>

                    @endauth

                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>

@endsection

@section('footerScripts')
    <script>
        $(function () {


            $('#service_medicle').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    s_contact_person: {
                        required: true
                    },
                    s_address_line_1: {
                        required: true
                    },
                    s_address_line_2: {
                        required: true
                    },
                    s_country: {
                        required: true
                    },
                    s_state: {
                        required: true
                    },
                    s_city: {
                        required: true
                    },
                    s_options: {
                        required: true
                    },
                    s_date: {
                        required: true
                    },
                    s_time: {
                        required: true
                    },
                    s_email: {
                        required: true,
                        email: true
                    },
                    s_phone: {
                        required: true
                    },
                    s_msg: {
                        required: true
                    }
                },
                messages: {
                    s_contact_person: {
                        required: 'Contact Person is required'
                    },
                    s_address_line_1: {
                        required: 'Addres line 1 is required'
                    },
                    s_address_line_2: {
                        required: 'Addres line 1 is required'
                    },
                    s_country: {
                        required: 'country is required'
                    },
                    s_state: {
                        required: 'State is required'
                    },
                    s_city: {
                        required: 'City is required'
                    },
                    s_options: {
                        required: 'Select Medicle Service'
                    },
                    s_date: {
                        required: 'Date is required'
                    },
                    s_time: {
                        required: 'Time is required'
                    },
                    s_email: {
                        required: 'Enter email',
                        email: 'Enter a valid email'
                    },
                    s_phone: {
                        required: 'Phone number is required',
                    },
                    s_msg: {
                        required: 'Message is required',
                    }

                }
            });


        });

    </script>

    {{--    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"--}}
    {{--            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="--}}
    {{--            crossorigin="anonymous"></script>--}}



    {{--    @include('frontend.user.scripts')--}}

    <script>
        $(function () {
            $('.ua_country').on('change', function () {
                var addresFormID = $(this).parents('form').attr('id');
                selectcountry(addresFormID, 'country')
            });
        });
    </script>



@endsection


