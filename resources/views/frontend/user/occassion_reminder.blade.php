@extends('frontend.layout')
@section('title', $title)


@section('header_styles')

@endsection


@section('content')


    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Occassions Reminders</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li>
                                    <a href="{{route('userprofile')}}">{{ \Illuminate\Support\Facades\Auth::user()->name }}</a>
                                </li>

                                <li><a>Occassions Reminders</a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- left navigation -->
                        <div class="col-lg-3 userleftnav">
                            @include('frontend.user.usermenu')
                        </div>
                        <!--/ left navigation -->
                        <!-- right side profile -->
                        <div class="col-lg-9">
                            <div class="whitebox rightprofile p-3">
                                <h5 class="sectitle fbold pb-3">Occassions Reminders </h5>
                                <!-- row -->
                                <div class="row">

                                    <?php
                                    //                                    dump($OccassionReminder);
                                    ?>

                                    @if (count($OccassionReminder)> 0)
                                        @foreach($OccassionReminder AS $reminder)
                                            <div class="col-lg-6 col-md-6">
                                                <div class="whitebox occasion p-3">
                                                    <h5 class="fbold"> {{ $reminder->or_name }} <span
                                                                class="float-right forange">{{ $reminder->or_date }}</span>
                                                    </h5>
                                                    <p class="py-3">
                                                        {{ $reminder->or_desc }}
                                                    </p>


                                                    <form class="" method="post" action="">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="action" value="remove"/>
                                                        <input type="hidden" name="reminder_id"
                                                               value="{{$reminder->or_id}}"/>
                                                        <a class="greenlink" href="{{ route('home') }}">
                                                            Send Gift
                                                        </a>
                                                        <button class="whitebtn">
                                                            Remove from List
                                                        </button>
                                                    </form>

                                                </div>
                                            </div>
                                        @endforeach
                                    @endif


                                </div>
                                <!--/ row -->
                                <!-- row -->
                                <div class="row pt-4">
                                    <div class="col-lg-12">
                                        <h5 class="fbold h5">Create a Reminder (Note: All fields are Mandatory)</h5>

                                    {{ laravelReturnMessageShow() }}

                                    <!-- form -->
                                        <form class="OccassionsForm" method="post" action="">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="action" value="create"/>
                                            <!-- row -->
                                            <div class="row">
                                                <!-- col -->
                                                <div class="col-lg-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label>Name of Reminder</label>
                                                        <input name="or_name" type="text"
                                                               placeholder="Ex: Ravi Wedding Anniversary"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <!-- col -->

                                                <!-- col -->
                                                <div class="col-lg-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label>Select Occassion</label>
                                                        <select class="form-control" name="or_type">
                                                            <option value="">Select Occassion</option>
                                                            <option>Wedding Anniversary</option>
                                                            <option>Birthday</option>
                                                            <option>Other Occassion</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!-- col -->
                                            </div>
                                            <!--/ row -->

                                            <!-- row -->
                                            <div class="row">
                                                <!-- col -->
                                                <div class="col-lg-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="w-100">Date of Occassion</label>
                                                        <div class="input-group">
                                                            <input type="text" name="or_date"
                                                                   class="form-control datepicker" placeholder="DD">
                                                        </div>

                                                    </div>
                                                </div>
                                                <!-- col -->

                                                <!-- col -->
                                                <div class="col-lg-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label>Remind </label>
                                                        <div class="input-group">
                                                            <select class="form-control" name="or_remind">
                                                                <option value="">Select</option>
                                                                <option>1 Day Before</option>
                                                                <option>2 Days Before</option>
                                                                <option>3 Days Before</option>
                                                                <option>4 Days Before</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- col -->
                                            </div>
                                            <!--/ row -->

                                            <!-- row -->
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label>Reminding Subject</label>
                                                        <div class="input-group">
                                                            <textarea name="or_desc" class="form-control"
                                                                      placeholder="Write About Your Occassion"
                                                                      style="height:120px;"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <input type="submit" value="Set Reminder" class="greenlink">
                                                    <input type="reset" value="Reset" class="greenlink">
                                                </div>
                                            </div>
                                            <!--/ row -->


                                        </form>
                                        <!--/ form -->
                                    </div>
                                </div>
                                <!--/ row -->
                            </div>
                        </div>
                        <!--/ right side profile -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>


@endsection




@section('footerScripts')
    <script>
        $(function () {

            $('.OccassionsForm').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    or_name: {
                        required: true
                    },
                    or_type: {
                        required: true
                    },
                    or_date: {
                        required: true,
                    },

                    or_remind: {
                        required: true
                    },
                    or_desc: {
                        required: true,
                    },
                    messages: {
                        or_name: {
                            required: 'Please enter name'
                        },
                        or_type: {
                            required: 'Please Select Type'
                        },
                        or_date: {
                            required: 'Please Select Date'
                        },

                        or_remind: {
                            required: 'Please Select Remind '
                        },
                        or_desc: {
                            required: 'Please Enter Description'
                        }
                    },
                });
        });
    </script>
@endsection