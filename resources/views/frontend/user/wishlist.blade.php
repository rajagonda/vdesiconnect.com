@extends('frontend.layout')
@section('title', $title)


@section('header_styles')

@endsection


@section('content')


    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">My Wishlist</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li>
                                    <a href="{{route('userprofile')}}">{{ \Illuminate\Support\Facades\Auth::user()->name }}</a>
                                </li>
                                <li><a>My Wishlist Products</a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- left navigation -->
                        <div class="col-lg-3 userleftnav">
                            @include('frontend.user.usermenu')
                        </div>
                        <!--/ left navigation -->
                        <!-- right side profile -->


                        @if (Session::has('flash_message'))
                            <br/>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>{{ Session::get('flash_message' ) }}</strong>
                            </div>
                        @endif

                        <div class="col-lg-9">
                            <div class="whitebox rightprofile p-3">
                                <h5 class="sectitle fbold pb-3">My Wishlist</h5>
                                <!-- row -->
                                <div class="row">

                                    <?php
                                    //                                    dump($wishlistProducts);
                                    ?>

                                    @if (count($wishlistProducts)> 0)
                                        @foreach($wishlistProducts AS $wishlistProduct)
                                            @if(isset($wishlistProduct->getProduct))
                                            <?php
//                                                    dump($wishlistProduct->getProduct);


                                            if (count($wishlistProduct->getProduct->productImages) > 0) {
                                                $imageID = array_first($wishlistProduct->getProduct->productImages)->pi_id;
                                                $image_url = getImagesById($imageID);
                                                if ($image_url == '') {
                                                    $image_url = '/frontend/images/noproduct-available.jpg';
                                                }
                                            } else {
                                                $image_url = '/frontend/images/noproduct-available.jpg';
                                            }


                                            if (count($wishlistProduct->getProduct->productSKUs) > 0) {

                                                $priceSKU = $wishlistProduct->getProduct->productSKUs[0];
                                            }

                                            $product_url = route('productPage', ['category' => $wishlistProduct->getProduct->getCategory->category_alias, 'product' => $wishlistProduct->getProduct->p_alias]);
//                                            $product_url = '';
//
                                            ?>

                                                <?php
//                                                dump($wishlistProduct);
                                                ?>

                                            <div class="col-lg-12">
                                                <div class="smallcol mb-2 position-relative">
                                                    {{--<span class="outofstock">--}}
                                                    {{--<img src="/frontend/images/sold-outimg.png" alt=""--}}
                                                    {{--title="">--}}
                                                    {{--</span>--}}
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-4 col-4">

                                                            <figure>
                                                                <a href="{{ $product_url }}">
                                                                    <img
                                                                            src="{{ $image_url }}"
                                                                            alt="" title=""
                                                                            class="img-fluid w-100">
                                                                </a>
                                                            </figure>
                                                        </div>
                                                        @if($wishlistProduct->getProduct->p_availability=='out_of_stock')
                                                            <span class="outofstock"><img src="/frontend/images/sold-outimg.png" alt="" title=""></span>
                                                        @endif
                                                        <div class="col-lg-9 col-md-9 col-sm-8 col-8 pl-0">
                                                            <article class="pt-2">
                                                                <h5>
                                                                    <a href="{{$product_url}}">
                                                                        {{ $wishlistProduct->getProduct->p_name }}

                                                                    </a>
                                                                </h5>


                                                                {{ ($wishlistProduct->getProduct->p_availability=='in_stock') ? 'In Stock':'Out of Stock'}}


                                                                {{--<p>In publishing and graphic design, lorem ipsum is a--}}
                                                                {{--placeholder text commonly used to demonstrate the--}}
                                                                {{--visual--}}
                                                                {{--form of a document. </p>--}}
                                                            </article>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <span class="oldprice">
                                                                        {!! currencySymbol('USD')  !!} {{ curencyConvert('USD', $priceSKU->sku_vdc_final_price)  }}
                                                                    </span>
                                                                    <span class="price">
                                                                        {!! currencySymbol('USD')  !!} {{ curencyConvert('USD', $priceSKU->sku_store_price)  }}

                                                                    </span>
                                                                    {{--<span class="price">(-25%)</span>--}}
                                                                </div>
                                                            </div>
                                                            <ul class="addstosmallcol nav">
                                                                @if($wishlistProduct->getProduct->p_availability=='in_stock')
                                                                <li>
                                                                    <a href="{{$product_url}}">
                                                                        <span class="icon-online-shopping-cart icomoon"></span>
                                                                        Add to Cart
                                                                    </a>
                                                                </li>
                                                                @else
                                                                    <li>
                                                                        <a href="javascript:void(0)">
                                                                            <span class="icon-online-shopping-cart icomoon"></span>
                                                                            Out of Stock
                                                                        </a>
                                                                    </li>
                                                                @endif
                                                                <li>
                                                                    <a href="javascript:void(0)" data-toggle="tooltip" data-view="list"
                                                                       data-id="{{ $wishlistProduct->getProduct->p_id }}"
                                                                       data-userid="{{ Auth::id() }}"
                                                                       data-placement="bottom" title="Add to Wishlist"
                                                                       class="productaddwishlist{{ $wishlistProduct->getProduct->p_id }} addToWishLists ">
                                                                        <span class="icon-bin icomoon"></span> Remove

                                                                    </a>

                                                                    {{--<a class="addToWishList" data-view="list"--}}
                                                                       {{--data-id="{{$wishlistProduct->getProduct->p_id}}"--}}
                                                                       {{--data-userid="{{ Auth::id() }}"--}}
                                                                       {{--href="javascript:void(0)">--}}
                                                                        {{--<span class="icon-bin icomoon"></span>--}}
                                                                        {{--Remove from Wishlist--}}
                                                                    {{--</a>--}}
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            @endif
                                        @endforeach
                                    @endif


                                </div>
                                <!--/ row -->
                            </div>
                        </div>
                        <!--/ right side profile -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>


@endsection




@section('footerScripts')
    <script>
        $(function () {


            $('.addToWishLists').on('click', function () {
                // console.log('asd')
                console.log('asd')

                var id = $(this).data('id');
                var userid = $(this).data('userid');
                var view = $(this).data('view');
                if (userid) {

                    var confirmRemobeWishlist = confirm('Do you want to really Remove Product?');
                    if (confirmRemobeWishlist) {

                        if (id != '') {
                            $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                                url: '{{ route('addToWishList') }}',
                                type: 'POST',
                                data: 'id=' + id,
                                success: function (response) {
                                    if (response.status == 1) {
                                        $('.productaddwishlist' + id).addClass("likeactive");
                                        // $('#addtowlist').modal('show');
                                        $('.addToWishList').addClass('likeactive');
                                    } else if (response.status == 2) {
                                        $('.productaddwishlist' + id).removeClass("likeactive");
                                        // $('#removefromwlist').modal('show');
                                        $('.addToWishList').removeClass('likeactive');
                                    } else {
                                        alert("Try again");
                                    }
                                    $('#wishlistcontents').text(response.count);

                                    if (view == 'list') {
                                        location.reload();
                                    }

                                }
                            });
                        }

                    } else {
                        // alert('false');
                    }


                } else {

                    var url = "{{route('userlogin')}}";
                    window.location = url;

                    // alert("Please login to add products to wishlist");
                }

            });



            $('#changePassword').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    password: {
                        required: true
                    },
                    new_password: {
                        required: true,
                        maxlength: 12,
                        minlength: 6
                    },
                    confirm_password: {
                        required: true,
                        equalTo: "#new_password",
                        maxlength: 12,
                        minlength: 6
                    }
                },
                messages: {
                    password: {
                        required: 'Please enter password'
                    },
                    new_password: {
                        required: 'Please enter new password'
                    },
                    confirm_password: {
                        required: 'Please confirm password',
                        equalTo: "The confirm password and new password must match."
                    }
                },
            });
        });

    </script>
@endsection