<script>
    $(function () {

        $('.custom-file-input input[type="file"]').change(function(e){
            $(this).siblings('input[type="text"]').val(e.target.files[0].name);
        });

        $('.ui-datepicker').datepicker({
            changeMonth: true,
            changeYear: true,
            // minDate: 0,
            dateFormat: 'dd-mm-yy',
            yearRange: "-100:+0"
        });


        $('.userAddress').each(function () {
            $(this).validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    ua_name: {
                        required: true
                    },
                    ua_address: {
                        required: true
                    },
                    // ua_landmark: {
                    //     required: true
                    // },
                    // ua_city: {
                    //     required: true
                    // },
                    ua_city: {
                        required: function(element){
                            return $("#ua_country_add option:selected").val() == "1";
                        }
                    },

                    ua_state: {
                        required: true
                    },
                    ua_email: {
                        required: true
                    },
                    mobile_prefix: {
                        required: true
                    },
                    ua_phone: {
                        required: true,
                        number: true,
                        minlength: 10,
                        maxlength: 15
                    },
                    ua_country: {
                        required: true
                    },
                    ua_pincode: {
                        required: true
                    }
                },
                messages: {
                    ua_name: {
                        required: 'Please enter name'
                    },
                    ua_address: {
                        required: 'Please enter address'
                    },
                    // ua_landmark: {
                    //     required: 'Please enter landmark'
                    // },
                    ua_city: {
                        required: 'Please enter city'
                    },
                    ua_state: {
                        required: 'Please enter state'
                    },
                    ua_email: {
                        required: 'Please enter email'
                    },
                    mobile_prefix: {
                        required: 'Please Select Code',
                    },
                    ua_phone: {
                        required: 'Please enter phone number',
                        number: 'Please enter valid phone number',
                        minlength: 'Please enter min 10 numbers',
                        maxlength: 'Please enter max 15 numbers'
                    },
                    ua_country: {
                        required: 'Please select country'
                    },
                    ua_pincode: {
                        required: 'Please enter pincode'
                    }
                },
            });
        })
        $('.delete_user_image').on('click', function () {
            var image = $(this).attr('id');
            if (image != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{route('deleteUserimage')}}',
                    type: 'POST',
                    data: {'image': image},
                    success: function (response) {
                        $('.imagediv' + image).remove();
                        $('.imghidebox').show();
                    }
                });
            }
        });
        $('#profileInfo').validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            errorPlacement: function (error, e) {
                e.parents('.form-group').append(error);
            },
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.text-danger').remove();
            },
            success: function (e) {
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.text-danger').remove();
            },
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true
                },

                mobile_prefix: {
                    required: true
                },
                mobile: {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 12
                },
                gender: {
                    required: true
                },
                dob: {
                    required: true
                },
            },
            messages: {
                name: {
                    required: 'Enter enter name'
                },
                email: {
                    required: 'Please enter email'
                },

                mobile_prefix: {
                    required: 'Please Select Code'
                },
                mobile: {
                    required: 'Please enter mobile',
                    number: 'Please enter Numbers Only',
                    minlength: 'minimun {0}',
                    maxlength: "Maximaun {0}"
                },
                gender: {
                    required: 'Please select gender'
                },
                dob: {
                    required: 'Please select DOB'
                },
            },
        });
    });


</script>