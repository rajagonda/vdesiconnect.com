@extends('frontend.layout')
@section('title', $title)


@section('header_styles')

@endsection


@section('content')


    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images//pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Change Password</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}">Home</a></li>
                                {{--<li><a href="user-profileinformation.php">{!! Auth::user()->name !!} </a></li>--}}
                                <li><a>Change Password </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- left navigation -->
                        <div class="col-lg-3 userleftnav">


                            @include('frontend.user.usermenu')

                        </div>
                        <!--/ left navigation -->
                        <!-- right side profile -->
                        <div class="col-lg-9">


                            {{ laravelReturnMessageShow() }}

                            <div class="whitebox rightprofile p-3">
                                <h5 class="sectitle fbold pb-3">Change Password</h5>

                                <form method="POST" id="changePassword"
                                      action="{{ route('userChangePassword') }}"
                                      accept-charset="UTF-8" class="form-horizontal">
                                {{ csrf_field() }}
                                <!-- row -->
                                    <div class="row">
                                        <!-- col -->
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Current Password</label>
                                                <input type="password" id="password" name="password"
                                                       placeholder="Enter Current Password"
                                                       value="{{ !empty(old('password')) ? old('password') : '' }}"
                                                       class="form-control">
                                                @if ($errors->has('password'))
                                                    <span class="text-danger help-block">{{ $errors->first('password') }}</span>
                                                @endif
                                            </div>


                                            <div class="form-group">
                                                <label>New Password</label>
                                                <input type="password" id="new_password"
                                                       name="new_password"
                                                       value="{{ !empty(old('new_password')) ? old('new_password') : '' }}"
                                                       placeholder="Enter New password"
                                                       class="form-control">
                                                @if ($errors->has('new_password'))
                                                    <span class="text-danger help-block">{{ $errors->first('new_password') }}</span>
                                                @endif
                                            </div>

                                            <div class="form-group">
                                                <label>Confirm New Password</label>
                                                <input type="password" class="form-control"
                                                       id="confirm_password"
                                                       name="confirm_password"
                                                       placeholder="Confirm Password"
                                                       value="{{ !empty(old('confirm_password')) ? old('confirm_password') : '' }}">
                                                @if ($errors->has('confirm_password'))
                                                    <span class="text-danger help-block">{{ $errors->first('confirm_password') }}</span>
                                                @endif
                                            </div>

                                            <div class="form-group">
                                                <input type="submit" value="Submit" class="greenlink w-100">
                                            </div>
                                        </div>
                                        <!--/ col -->

                                        <!-- col -->

                                        <!--/ col -->
                                    </div>

                                </form>
                                <!--/ row -->
                            </div>
                        </div>
                        <!--/ right side profile -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>


    <!--main Starts-->

    <!--/main Ends-->

@endsection




@section('footerScripts')
    <script>
        $(function () {

            $('#changePassword').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    password: {
                        required: true
                    },
                    new_password: {
                        required: true,
                        maxlength: 12,
                        minlength: 6
                    },
                    confirm_password: {
                        required: true,
                        equalTo: "#new_password",
                        maxlength: 12,
                        minlength: 6
                    }
                },
                messages: {
                    password: {
                        required: 'Please enter password'
                    },
                    new_password: {
                        required: 'Please enter new password'
                    },
                    confirm_password: {
                        required: 'Please confirm password',
                        equalTo: "The confirm password and new password must match."
                    }
                },
            });
        });

    </script>
@endsection