@extends('frontend.layout')
@section('title', $title)


@section('header_styles')

@endsection


@section('content')


    <?php
//    dump($invoiceLists);
    ?>

    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">My Service Invoices</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li>
                                    <a href="{{route('userprofile')}}">{{ \Illuminate\Support\Facades\Auth::user()->name }}</a>
                                </li>

                                <li><a>My Service Invoices</a></li>
                                {{--<li><a>All Orders </a></li>--}}
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- left navigation -->
                        <div class="col-lg-3 userleftnav">
                            @include('frontend.user.usermenu')
                        </div>
                        <!--/ left navigation -->
                        <!-- right side profile -->
                        <div class="col-lg-9 ">
                            <div class="whitebox rightprofile p-3">
                                <h5 class="sectitle fbold pb-3">My Service Invoices</h5>
                                <!-- row -->
                                <div class="row">
                                    <!-- small column -->

                                    <?php
//                                    dd($invoiceLists);

                                    ?>

                                    <div class="table-responsive">
                                        <table class="table table-striped" width="100%">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Service Type</th>
                                                <th>Due date</th>
                                                <th>Unit Type</th>
                                                {{--                                            <th>Email</th>--}}
                                                {{--                                            <th>Phone</th>--}}
                                                {{--                                            <th>Message</th>--}}
                                                <th>description</th>
                                                <th>Requested On</th>
                                                <th>Payment Status</th>
                                                <th>Invoice</th>
                                                {{--                                            <th>Action</th>--}}
                                            </tr>
                                            </thead>
                                            <tbody>



                                            @if(count($invoiceLists)> 0)

                                                @foreach($invoiceLists AS $list)

                                                    <tr>
                                                        <td>#</td>
                                                        <td>{{ $list->getService->s_type }}</td>
                                                        <td>{{ $list->s_duedate }}</td>
                                                        <td>{{ $list->s_unit_type }}</td>
                                                        {{--                                                    <td>{{ $list->getService->s_email }}</td>--}}
                                                        {{--                                                    <td>{{ $list->getService->s_phone }}</td>--}}
                                                        <td>{{ $list->s_description }}</td>

                                                        <td>{{ date('d-m-Y', strtotime($list->created_at)) }}</td>
                                                        <td>{!! serviceInvoiceStatus($list->s_status) !!}</td>
                                                        <td>
                                                            {{--                                                        @if($list->s_status!='')--}}
                                                            {{--                                                            ------}}
                                                            {{--                                                        @else--}}
                                                            <a href="{{route('ServiceInvoices',['action'=>'viewInvoice','id'=>$list->id])}}"
                                                               class="dropdown-item">
                                                                <i class="fa fa-file"></i> Invoice
                                                            </a>
                                                            {{--                                                        @endif--}}
                                                        </td>
                                                        {{--                                                    <td>Action</td>--}}
                                                    </tr>

                                                @endforeach
                                            @endif


                                            </tbody>
                                        </table>
                                    </div>


{{--                                    <div class="table-responsive">--}}
{{--                                        <table class="table table-striped">--}}
{{--                                            <thead>--}}
{{--                                            <tr>--}}
{{--                                                <th scope="col">#</th>--}}
{{--                                                <th scope="col">Contact Person</th>--}}
{{--                                                <th scope="col">Type</th>--}}
{{--                                                <th scope="col">Date & Time</th>--}}
{{--                                                <th scope="col">Requested On</th>--}}
{{--                                                <th scope="col">Invoice</th>--}}
{{--                                                <th scope="col">Documents</th>--}}
{{--                                            </tr>--}}
{{--                                            </thead>--}}
{{--                                            <tbody>--}}
{{--                                            @if(count($invoiceLists)>0)--}}
{{--                                                @foreach($services as $item)--}}
{{--                                                    <tr>--}}
{{--                                                        <th scope="row">{{ $item->s_id }}</th>--}}
{{--                                                        <td>{{$item->s_contact_person}}</td>--}}
{{--                                                        <td>{{$item->s_options}}</td>--}}
{{--                                                        <td>{{$item->s_date}} , {{$item->s_time}}</td>--}}
{{--                                                        <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i:s')}}</td>--}}

{{--                                                        <td>--}}
{{--                                                            <a href="{{ route('ServiceInvoices', ['action'=>'view', 'id'=>$item->s_id]) }}">--}}
{{--                                                                View--}}
{{--                                                            </a>--}}

{{--                                                            <a href="{{ route('showServiceInvoice',['id'=>$item->s_id]) }}"--}}
{{--                                                               class="dropdown-item">--}}
{{--                                                                <i class="fa fa-file"></i> Invoice--}}
{{--                                                            </a>--}}

{{--                                                        </td>--}}
{{--                                                        <td>--}}
{{--                                                            @if(count($item->getServiceDocuments)>0)--}}
{{--                                                                @foreach($item->getServiceDocuments as $docs)--}}
{{--                                                                    <a href="{{url('/uploads/service_documents/'.$docs->sd_doc_name)}}">{{ $docs->sd_title }}</a>--}}
{{--                                                                    <hr/>--}}
{{--                                                                @endforeach--}}
{{--                                                            @else--}}
{{--                                                                ------}}
{{--                                                            @endif--}}
{{--                                                        </td>--}}
{{--                                                    </tr>--}}
{{--                                                @endforeach--}}
{{--                                            @else--}}
{{--                                                <tr>--}}
{{--                                                    <td colspan="8">No records found</td>--}}
{{--                                                </tr>--}}
{{--                                            @endif--}}

{{--                                            </tbody>--}}
{{--                                        </table>--}}
{{--                                    </div>--}}


                                    <!-- small column -->


                                </div>
                                <!--/ row -->
                            </div>
                        </div>
                        <!--/ right side profile -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>

@endsection




@section('footerScripts')
@endsection
