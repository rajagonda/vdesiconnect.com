<h6>{!! Auth::user()->name !!} <span class="icon-menu icomoon usernav-icon"></span></h6>
<div class="whitebox" id="userNav-mobile">

    <ul>
        <li class="{{ ($active_menu=='profile')?'active':''  }}">
            <a href="{{route('userprofile')}}"><span class="icon-userprofile icomoon"></span>Profile Information</a>
        </li>
        <li class="{{ ($active_menu=='changePassword')?'active':''  }}">
            <a href="{{route('userChangePassword')}}"><span class="icon-lock icomoon"></span>Change Password</a>
        </li>
        <li class="{{ ($active_menu=='orders')?'active':''  }}">
            <a href="{{route('orders',['status'=>'paid'])}}"><span class="icon-gift2 icomoon"></span>My Orders History</a>
        </li>
        <li class="{{ ($active_menu=='userAddressBook')?'active':''  }}">
            <a href="{{route('userAddressBook')}}"><span class="icon-addressuser icomoon"></span>Address Book</a>
        </li>
        <li class="{{ ($active_menu=='wishList')?'active':''  }}">
            <a href="{{route('wishList')}}"><span class="icon-heartwhite icomoon"></span>My Wish List</a>
        </li>
        <li class="{{ ($active_menu=='occassionReminder')?'active':''  }}">
            <a href="{{route('occassionReminder')}}"><span class="icon-appointment icomoon"></span>Occasion Reminder</a>
        </li>
        <li class="{{ ($active_menu=='service-invoice')?'active':''  }}">
            <a href="{{route('ServiceInvoices')}}"><span class="icon-appointment icomoon"></span>Service Invoices</a>
        </li>

        <li class="{{ ($active_menu=='ammachethi-vanta-orders')?'active':''  }}">
            <a href="{{route('AmmachethiVantaOrders')}}"><span class="icon-appointment icomoon"></span>Amma chethi vanta Orders</a>
        </li>
       {{-- <li class="">
            <a href="index.php"><span class="icon-logout icomoon"></span>Logout</a>
        </li>--}}
    </ul>
</div>

