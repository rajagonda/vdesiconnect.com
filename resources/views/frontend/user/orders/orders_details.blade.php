@extends('frontend.layout')
@section('title', $title)


@section('header_styles')

@endsection


@section('content')



    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Order History</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a href="{{route('userprofile')}}">{!! Auth::user()->name !!}</a></li>
                                <li><a href="{{route('orders')}}">Orders </a></li>
                                <li>
                                    <a> {{ $orderDetails->order_reference_number }}</a>
                                </li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- left navigation -->
                        <div class="col-lg-3 userleftnav">
                            @include('frontend.user.usermenu')
                        </div>
                        <!--/ left navigation -->
                        <!-- right side profile -->
                        <div class="col-lg-9">
                            <div class="whitebox rightprofile p-3">
                                <h5 class="sectitle fbold pb-3">Order History <a class="fgreen float-right bankclink" href="{{route('orders')}}">
                                        <span class="icon-left-arrow icomoon"></span>Back to Orders </a>
                                </h5>

                            <?php
                            $order_currency = $orderDetails->order_currency;
//                            dump($orderDetails);
                            ?>

                            <!-- gray block -->
                                <div class="grayblock">
                                    <ul class="row primarydetails">
                                        <li class="col-lg-4 col-sm-6 col-md-4">
                                            <h6>Order number </h6>
                                            <p>
                                                <a href="{{ route('orderDetails', ['id'=>$orderDetails->order_reference_number]) }}">
                                                    {{ $orderDetails->order_reference_number }}
                                                </a>
                                            </p>
                                        </li>
                                        <li class="col-lg-4 col-sm-6 col-md-4">


                                            <h6>Order Date & Time </h6>
                                            <p>
                                                {{ \Carbon\Carbon::parse( $orderDetails->created_at)->format('d-m-Y h:m:s a') }}

                                            </p>
                                        </li>
                                        <li class="col-lg-4 col-sm-6 col-md-4">
                                            <h6>Payment Info</h6>
                                            <p>
                                                Pay With
                                                <b>
                                                    {{ $orderDetails->order_payment_mode }}
                                                </b>

                                                <br/>

                                                {!! PayapPaymentStatusShow($orderDetails->order_payment_status) !!}

                                            </p>
                                        </li>
                                    </ul>


                                    <ul class="row primarydetails">
                                        <li class="col-lg-3 col-sm-6 col-md-4">
                                            <h6>Products </h6>
                                            <p>
                                                Products Price :
                                                {!! currencySymbol($order_currency) !!}
                                                {{  $orderDetails->order_sub_total_price }}

                                            </p>
                                        </li>

                                        <li class="col-lg-3 col-sm-6 col-md-4">
                                            <h6>Shipping Charges </h6>
                                            <span class="forange fbold">
                                                {!! currencySymbol($order_currency) !!}
                                                {{  $orderDetails->order_shipping_price }}

                                            </span>
                                        </li>


                                        <li class="col-lg-3 col-sm-6 col-md-4">
                                            <h6>Coupon Discount</h6>
                                            <p>

                                                {{--                                                <br/>--}}
                                                {{--                                                Sub Total Price :--}}
                                                {{--                                                {!! currencySymbol($order_currency) !!}--}}
                                                {{--                                                {{  $orderDetails->order_sub_total_price }}--}}


                                                Coupon Price : {!! currencySymbol($order_currency) !!}
                                                {{  $orderDetails->order_coupon_discount_amount }}
                                                <br/>

                                                code: {{  $orderDetails->order_coupon_code }}
                                                <br/>
                                                Value: {{  $orderDetails->order_coupon_discount }}  {{  ($orderDetails->order_coupon_discount_type=='Percentage')?'%':$orderDetails->order_coupon_discount_type }}

                                            </p>
                                        </li>

                                        <li class="col-lg-3 col-sm-6 col-md-4">
                                            <h6>Payble Amount </h6>
                                            <span class="forange fbold">
                                                {!! currencySymbol($order_currency) !!}
                                                {{  $orderDetails->order_total_price }}
                                            </span>
                                        </li>

                                    </ul>

                                    <?php
                                    // dump($orderDetails->orderItems);
                                    ?>

                                </div>
                                <!--/ gray block -->


                                @if (!empty($orderDetails->orderItems))
                                    @foreach($orderDetails->orderItems AS $orderItems)

                                        <?php
                                        if (isset($orderItems->getProduct->productImages[0])) {
                                            $item_img = url('/uploads/products/' . $orderItems->getProduct->productImages[0]->pi_image_name);

                                        } else {
                                            $item_img = '/frontend/images/no-image.png';
                                        }

                                        // dump($orderItems);
                                        ?>

                                        <div class="row py-2">
                                            <!-- col -->
                                            <div class="col-lg-2 col-md-2 col-6 col-sm-4">
                                                <figure class="imgproduct">
                                                    <a href="javascript:void(0)">
                                                        <img src="{{$item_img}}" alt="" title="" class="img-fluid">
                                                    </a>
                                                </figure>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-6 col-md-5 col-6 col-sm-8">
                                                <h6 class="pb-2">
                                                    {{ $orderItems->oitem_product_name  }}
                                                </h6>
                                                <p>
                                                    <b>Gift Message:</b>
                                                    {{ $orderItems->oitem_message  }}
                                                </p>

                                                <p>
                                                    <b>Name On The gift:</b>
                                                    {{ $orderItems->oitem_nameongift  }}
                                                </p>
                                                <p>
                                                    <b>Options:</b>
                                                    {{ sortProductOptions($orderItems->oitem_product_options)  }}
                                                </p>


                                                {{--<a href="javascript:void(0)" class="whitebtn">Paynow</a>--}}
                                                {{--<a href="javascript:void(0)" class="whitebtn">return / replace</a>--}}
                                            </div>
                                            <div class="col-lg-4 col-md-5">
                                                <div>
                                                    Price
                                                    : {!! currencySymbol($order_currency) !!} {{  $orderItems->oitem_sub_total  }}
                                                </div>

                                                <div>

                                                    @if (isset($orderItems->oitem_delivery_date) && $orderItems->oitem_delivery_date != '')

                                                        <p>
                                                            <b>
                                                                User Request Delivery time.

                                                            </b> <br/>
                                                            {{ $orderItems->oitem_delivery_date }} /
                                                            {{ $orderItems->oitem_delivery_time }}

                                                            {{--                                                        {{  (isset($orderItems->oitem_delivery_date))?\Carbon\Carbon::parse($orderItems->oitem_delivery_date)->add('3 days')->format('d-m-Y') :'' }}--}}
                                                        </p>
                                                    @endif

                                                </div>

                                                <div>
                                                    Delevery Charge
                                                    : {!! currencySymbol($order_currency) !!} {{  $orderItems->oitem_delivery_charge  }}
                                                </div>
                                                <div>
                                                    Total

                                                    <span class="forange fbold"> : {!! currencySymbol($order_currency) !!} {{  $orderItems->oitem_sub_total +$orderItems->oitem_delivery_charge }} </span>

                                                </div>
                                            </div>
                                            <!--/ col -->


                                            @if($orderDetails->order_payment_status == 'approved')



                                                <?php

                                                switch ($orderDetails->order_status) {
//                                                case '':
//                                                    break;
//                                                case '':
//                                                    break;
                                                    default:
                                                        $status_bar = 50;
                                                        $status_bar_name = $orderDetails->order_status;
                                                        break;
                                                }
                                                ?>


                                                <?php
                                                //                                        dump($orderItems->oitem_status);
                                                ?>

                                                {{--                                                {{ $orderItems->oitem_status }}--}}
                                                {{--                                                {{ $orderDetails->order_status }}--}}



                                                {!! orderItemStatus($orderItems->oitem_status); !!}



                                            @endif
                                        </div>
                                @endforeach
                            @endif


                            <!-- row -->

                                <!--/ row -->
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <h6 class="h6 ">Status:
                                            <span class="fgreen">
                                                {!! OrderStatus($orderDetails->order_status) !!}
                                            </span>
                                        </h6>
                                    </div>
                                    {{--<div class="col-lg-6 col-md-6 text-right">--}}
                                    {{--<h6 class="h6 ">Delivered on:--}}
                                    {{--<span class="fgreen">11 July 2019</span>--}}
                                    {{--</h6>--}}
                                    {{--</div>--}}
                                </div>
                                <!-- order status -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="grayblock  p-2 tw100 ">
                                            <p class="fgreen fbold">Shipping Information</p>
                                            <p>
                                                <?php
                                                $address_info = unserialize($orderDetails->order_delivery_address);
                                                ?>
                                                {{  implode(', ', $address_info) }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-6 justify-content-center text-right">
                                        <a href="{{route('showOrderInvoice', [$orderDetails->order_id])}}"
                                           class="btn btn-success">View invoice</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ right side profile -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>

    <div class="modal fade" id="reviewpopup">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Write Review</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form class="formreview">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" placeholder="Write Your Name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Write Review</label>
                            <textarea class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Select Rating</label>
                            <select class="form-control">
                                <option>Star 1</option>
                                <option>Star 2</option>
                                <option>Star 3</option>
                                <option>Star 4</option>
                                <option>Star 5</option>
                            </select>
                        </div>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-success">Submit Review</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
                <!-- article -->
                <div class="articlereview p-3">
                    <h5 class="h5">How to write customer-friendly reviews?</h5>
                    <div>
                        <p>Product centricity: Concentrate only on product feedback and DO NOT include service or seller
                            feedback. Reviews with such content will NOT BE made live
                        </p>
                        <p>Simple language: Keep the language simple, light and in your own words</p>
                        <p>Be crisp: If the details run long, break the review into readable short paragraphs and
                            bullets</p>
                        <p>No profanity: Profanity, copyrighted comm ents or any copied content must be avoided</p>
                        <p>Share your experience: Explain what you liked and disliked about the product. The most
                            helpful reviews are the ones that help readers know exactly what to expect.</p>
                        <p>Pros & cons: Select or add appropriate pros & cons to give a quick summary of your review to
                            the reader</p>
                    </div>
                </div>
                <!--/ article -->
            </div>
        </div>
    </div>


@endsection




@section('footerScripts')
    <script>
        $(function () {

            $('#changePassword').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    password: {
                        required: true
                    },
                    new_password: {
                        required: true,
                        maxlength: 12,
                        minlength: 6
                    },
                    confirm_password: {
                        required: true,
                        equalTo: "#new_password",
                        maxlength: 12,
                        minlength: 6
                    }
                },
                messages: {
                    password: {
                        required: 'Please enter password'
                    },
                    new_password: {
                        required: 'Please enter new password'
                    },
                    confirm_password: {
                        required: 'Please confirm password',
                        equalTo: "The confirm password and new password must match."
                    }
                },
            });
        });

    </script>
@endsection