@extends('frontend.layout')
@section('title', $title)


@section('header_styles')




    <style type="text/css">


        ol, ul {
            list-style: none;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
        }

        caption, th, td {
            text-align: left;
            font-weight: normal;
            vertical-align: middle;
        }

        q, blockquote {
            quotes: none;
        }

        q:before, q:after, blockquote:before, blockquote:after {
            content: "";
            content: none;
        }

        a img {
            border: none;
        }

        article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary {
            display: block;
        }

        body {
            font-family: 'Poppins', sans-serif;
            font-weight: 300;
            font-size: 13px;
            margin: 0;
            padding: 0;
        }

        body a {
            text-decoration: none;
            color: inherit;
        }

        body a:hover {
            color: inherit;
            opacity: 0.7;
        }

        body .container {
            /*width: 900px;*/
            margin: 0 auto;
            padding: 0 20px;
        }

        body .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        body .left {
            float: left;
        }

        body .right {
            float: right;
        }

        body .helper {
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }

        body .no-break {
            page-break-inside: avoid;
        }

        header figure {
            float: left;
            width: 60px;
            height: 60px;
            margin-right: 15px;
            text-align: center;
        }

        header .company-address {
            float: left;
            max-width: 150px;
            line-height: 1.7em;
        }

        header .company-address .title {
            color: #8BC34A;
            font-weight: 400;
            font-size: 1.5em;
            text-transform: uppercase;
        }

        header .company-contact {
            float: right;
            height: 60px;
            padding: 0 10px;
            background-color: #8BC34A;
            color: white;
        }

        header .company-contact span {
            display: inline-block;
            vertical-align: middle;
        }

        header .company-contact .circle {
            width: 20px;
            height: 20px;
            background-color: white;
            border-radius: 50%;
            text-align: center;
        }

        header .company-contact .circle img {
            vertical-align: middle;
        }

        header .company-contact .phone {
            height: 100%;
            margin-right: 20px;
        }

        header .company-contact .email {
            height: 100%;
            min-width: 100px;
            text-align: right;
        }

        .date {
            line-height: 20px;
        }

        section .details {
            margin-bottom: 55px;
        }

        section .details .client {
            width: 50%;
            line-height: 20px;
        }

        section .details .client .name {
            color: #8BC34A;
        }

        section .details .data {
            width: 50%;
            text-align: right;
        }

        section .details .title {
            margin-bottom: 15px;
            color: #8BC34A;
            font-size: 3em;
            font-weight: 400;
            text-transform: uppercase;
        }

        section table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;

        }

        section table .qty, section table .unit, section table .total {
            width: 15%;
        }

        section table .desc {
            width: 55%;
        }

        section table thead {
            display: table-header-group;
            vertical-align: middle;
            border-color: inherit;
        }

        section table thead th {
            padding: 5px 10px;
            background: #8BC34A;
            border-bottom: 5px solid #FFFFFF;
            border-right: 4px solid #FFFFFF;
            text-align: right;
            color: white;
            font-weight: 400;
            text-transform: uppercase;
        }

        section table thead th:last-child {
            border-right: none;
        }

        section table thead .desc {
            text-align: left;
        }

        section table thead .qty {
            text-align: center;
        }

        section table tbody td {
            padding: 10px;
            background: #E8F3DB;
            color: #000;
            text-align: right;
            border-bottom: 5px solid #FFFFFF;
            border-right: 4px solid #E8F3DB;
        }

        section table tbody td:last-child {
            border-right: none;
        }

        section table tbody h3 {
            margin-bottom: 5px;
            color: #8BC34A;
            font-weight: 600;
            font-size: 15px;
        }

        section table tbody .desc {
            text-align: left;
            line-height: 20px;
            font-size: 13px;
        }

        section table tbody .qty {
            text-align: center;
        }

        section table.grand-total {
            margin-bottom: 45px;
        }

        section table.grand-total td {
            padding: 5px 10px;
            border: none;
            color: #777777;
            text-align: right;
            line-height: 25px;
        }

        section table.grand-total .desc {
            background-color: transparent;
        }

        section table.grand-total tr:last-child td {
            font-weight: 600;
            color: #8BC34A;
            font-size: 1.18181818181818em;
        }

        footer {
            margin-bottom: 20px;
        }

        footer .thanks {
            margin-bottom: 40px;
            color: #8BC34A;
            font-size: 1.16666666666667em;
            font-weight: 600;
        }

        footer .notice {
            margin-bottom: 25px;
            line-height: 22px;
        }

        footer .end {
            padding-top: 5px;
            border-top: 2px solid #8BC34A;
            text-align: center;
        }

    </style>

    <link href="//cdn.kendostatic.com/2013.1.319/styles/kendo.common.min.css" rel="stylesheet"/>
    <link href="//cdn.kendostatic.com/2013.1.319/styles/kendo.default.min.css" rel="stylesheet"/>
@endsection


@section('content')



    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">My Service Invoices</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="user-profileinformation.php">{!! Auth::user()->name !!}</a></li>
                                <li><a href="{{route('orders')}}">Orders</a></li>
                                <li><a>Invoice View </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- left navigation -->
                        <div class="col-lg-3 col-md-4 userleftnav">
                            @include('frontend.user.usermenu')
                        </div>
                        <!--/ left navigation -->
                        <!-- right side profile -->
                        <div class="col-lg-9 col-md-8">
                            <div class="whitebox rightprofile p-3">
                            {{--<h5 class="sectitle fbold pb-3">My Service Invoices</h5>--}}
                            <!-- row -->
                                <div class="row">
                                    <!-- small column -->

                                    <?php

                                    //                                        dd($orders);

                                    $order_currency = $invoice->order_currency;

                                    //                                    dump($invoice);

                                    ?>

                                    <div class="col-12">
                                        @if (Session::has('flash_message'))
                                            <br/>
                                            <div class="alert alert-success alert-dismissable">
                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                <strong>{{ Session::get('flash_message' ) }}</strong>
                                            </div>
                                        @endif
                                        @if (Session::has('flash_message_error'))
                                            <br/>
                                            <div class="alert alert-danger alert-dismissable">
                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                <strong>{{ Session::get('flash_message_error' ) }}</strong>
                                            </div>
                                        @endif
                                        <div class="row mb-3">
                                            <div class="col-6 text-left">
                                                <div class="buttonsgroup ">

                                                    <button onClick="printdiv('pdfgenerate');"> Print</button>

                                                    <button id="cmd" class="btn btn-success"> Download</button>
                                                </div>
                                            </div>
                                            <div class="col-6 text-right">
                                                {{--<div class="buttonsgroup ">
                                                    @if($invoice->s_status=='approved')
                                                        <a href="javascript:void(0)" class="btn btn-success">Paid</a>
                                                    @else
                                                    <a href="{{ route('payWithpaypal',['order'=>$invoice->s_id]) }}" class="btn btn-success"> Paynow</a>
                                                    @endif
                                                </div>--}}
                                            </div>
                                        </div>

                                        <div id="pdfgenerate">
                                            <header class="clearfix">
                                                <div class="container">


                                                    <figure>
                                                        <img class="logo" src="/frontend/images/logo-thumb.svg" alt="">
                                                    </figure>
                                                    <div class="company-address">
                                                        <h2 class="title"><b>Vdesi Connect</b></h2>
                                                        <p>
                                                            {!! vdcSettings()->address !!}
                                                        </p>
                                                    </div>
                                                    <div class="company-contact">


                                                        <div class="phone left">
                                                        <span class="circle">
                                                            <img src="data:image/svg+xml;charset=utf-8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+DQo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zOnNrZXRjaD0iaHR0cDovL3d3dy5ib2hlbWlhbmNvZGluZy5jb20vc2tldGNoL25zIg0KCSB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjkuNzYycHgiIGhlaWdodD0iOS45NThweCINCgkgdmlld0JveD0iLTQuOTkyIDAuNTE5IDkuNzYyIDkuOTU4IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IC00Ljk5MiAwLjUxOSA5Ljc2MiA5Ljk1OCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8dGl0bGU+RmlsbCAxPC90aXRsZT4NCjxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPg0KPGcgaWQ9IlBhZ2UtMSIgc2tldGNoOnR5cGU9Ik1TUGFnZSI+DQoJPGcgaWQ9IklOVk9JQ0UtMSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTMwMS4wMDAwMDAsIC01NC4wMDAwMDApIiBza2V0Y2g6dHlwZT0iTVNBcnRib2FyZEdyb3VwIj4NCgkJPGcgaWQ9IlpBR0xBVkxKRSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMzAuMDAwMDAwLCAxNS4wMDAwMDApIiBza2V0Y2g6dHlwZT0iTVNMYXllckdyb3VwIj4NCgkJCTxnIGlkPSJLT05UQUtUSSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMjY3LjAwMDAwMCwgMzUuMDAwMDAwKSIgc2tldGNoOnR5cGU9Ik1TU2hhcGVHcm91cCI+DQoJCQkJPGcgaWQ9Ik92YWwtMS1feDJCXy1GaWxsLTEiPg0KCQkJCQk8cGF0aCBpZD0iRmlsbC0xIiBmaWxsPSIjOEJDMzRBIiBkPSJNOC43NjUsMTIuMzc1YzAuMDIsMC4xNjItMC4wMjgsMC4zMDMtMC4xNDMsMC40MjJMNy4yNDYsMTQuMTkNCgkJCQkJCWMtMC4wNjIsMC4wNy0wLjE0MywwLjEzMy0wLjI0MywwLjE4MmMtMC4xMDEsMC4wNDktMC4xOTcsMC4wOC0wLjI5NSwwLjA5NGMtMC4wMDcsMC0wLjAyOCwwLTAuMDYyLDAuMDA0DQoJCQkJCQljLTAuMDM0LDAuMDA1LTAuMDgsMC4wMDgtMC4xMzQsMC4wMDhjLTAuMTMxLDAtMC4zNDMtMC4wMjMtMC42MzUtMC4wNjhjLTAuMjkzLTAuMDQ1LTAuNjUxLTAuMTU4LTEuMDc2LTAuMzM2DQoJCQkJCQljLTAuNDI0LTAuMTgyLTAuOTA0LTAuNDUxLTEuNDQyLTAuODA5Yy0wLjUzNi0wLjM1Ny0xLjEwOS0wLjg1Mi0xLjcxNi0xLjQ3OWMtMC40ODEtMC40ODQtMC44OC0wLjk1LTEuMTk4LTEuMzkzDQoJCQkJCQlDMC4xMjgsOS45NS0wLjEyNSw5LjU0MS0wLjMxOSw5LjE2NGMtMC4xOTMtMC4zNzYtMC4zMzgtMC43MTctMC40MzQtMS4wMjNjLTAuMDk3LTAuMzA2LTAuMTYxLTAuNTctMC4xOTUtMC43OTINCgkJCQkJCWMtMC4wMzUtMC4yMjEtMC4wNS0wLjM5NC0wLjA0Mi0wLjUyMWMwLjAwNy0wLjEyNiwwLjAxLTAuMTk3LDAuMDEtMC4yMTFjMC4wMTQtMC4wOTksMC4wNDQtMC4xOTgsMC4wOTMtMC4zMDENCgkJCQkJCWMwLjA0OS0wLjEwMSwwLjEwOC0wLjE4NCwwLjE3Ni0wLjI0N2wxLjM3NS0xLjQwM2MwLjA5Ny0wLjA5OCwwLjIwNi0wLjE0NywwLjMzLTAuMTQ3YzAuMDksMCwwLjE2OSwwLjAyNiwwLjIzOCwwLjA3OQ0KCQkJCQkJQzEuMyw0LjY0OCwxLjM1OSw0LjcxNCwxLjQwNiw0Ljc5MWwxLjEwNiwyLjE0MWMwLjA2MiwwLjExNCwwLjA4LDAuMjM1LDAuMDUyLDAuMzdDMi41MzgsNy40MzYsMi40NzgsNy41NDgsMi4zODksNy42NA0KCQkJCQkJTDEuODgzLDguMTU3QzEuODY5LDguMTcxLDEuODU2LDguMTk0LDEuODQ2LDguMjI2QzEuODM1LDguMjU2LDEuODMsOC4yODMsMS44Myw4LjMwNGMwLjAyNywwLjE0NywwLjA5LDAuMzE3LDAuMTg3LDAuNTA3DQoJCQkJCQljMC4wODIsMC4xNjksMC4yMSwwLjM3NSwwLjM4MiwwLjYxOGMwLjE3MiwwLjI0MywwLjQxNywwLjUyMSwwLjczNCwwLjgzOWMwLjMxMSwwLjMyMiwwLjU4NSwwLjU3NCwwLjgyOCwwLjc1NQ0KCQkJCQkJYzAuMjQsMC4xNzgsMC40NDMsMC4zMDksMC42MDQsMC4zOTVjMC4xNjIsMC4wODUsMC4yODYsMC4xMzUsMC4zNzIsMC4xNTRsMC4xMjgsMC4wMjRjMC4wMTUsMCwwLjAzOC0wLjAwNiwwLjA2Ny0wLjAxNg0KCQkJCQkJYzAuMDMyLTAuMDEsMC4wNTQtMC4wMjEsMC4wNjctMC4wMzdsMC41ODgtMC42MTJjMC4xMjUtMC4xMTIsMC4yNy0wLjE2OCwwLjQzNi0wLjE2OGMwLjExNywwLDAuMjA3LDAuMDIxLDAuMjc3LDAuMDYxaDAuMDENCgkJCQkJCWwxLjk5NSwxLjIwM0M4LjY1MSwxMi4xMiw4LjczNywxMi4yMzQsOC43NjUsMTIuMzc1TDguNzY1LDEyLjM3NXoiLz4NCgkJCQk8L2c+DQoJCQk8L2c+DQoJCTwvZz4NCgk8L2c+DQo8L2c+DQo8L3N2Zz4NCg=="
                                                                 alt="">
                                                            <span class="helper"></span></span>
                                                            <a href="tel:602-519-0450">+91-9100311263</a>
                                                            <span class="helper"></span>
                                                        </div>
                                                        <div class="email right">
                                    <span class="circle">
                                        <img
                                                src="data:image/svg+xml;charset=utf-8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+DQo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zOnNrZXRjaD0iaHR0cDovL3d3dy5ib2hlbWlhbmNvZGluZy5jb20vc2tldGNoL25zIg0KCSB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjE0LjE3M3B4Ig0KCSBoZWlnaHQ9IjE0LjE3M3B4IiB2aWV3Qm94PSIwLjM1NCAtMi4yNzIgMTQuMTczIDE0LjE3MyIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwLjM1NCAtMi4yNzIgMTQuMTczIDE0LjE3MyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSINCgk+DQo8dGl0bGU+ZW1haWwxOTwvdGl0bGU+DQo8ZGVzYz5DcmVhdGVkIHdpdGggU2tldGNoLjwvZGVzYz4NCjxnIGlkPSJQYWdlLTEiIHNrZXRjaDp0eXBlPSJNU1BhZ2UiPg0KCTxnIGlkPSJJTlZPSUNFLTEiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC00MTcuMDAwMDAwLCAtNTUuMDAwMDAwKSIgc2tldGNoOnR5cGU9Ik1TQXJ0Ym9hcmRHcm91cCI+DQoJCTxnIGlkPSJaQUdMQVZMSkUiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDMwLjAwMDAwMCwgMTUuMDAwMDAwKSIgc2tldGNoOnR5cGU9Ik1TTGF5ZXJHcm91cCI+DQoJCQk8ZyBpZD0iS09OVEFLVEkiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDI2Ny4wMDAwMDAsIDM1LjAwMDAwMCkiIHNrZXRjaDp0eXBlPSJNU1NoYXBlR3JvdXAiPg0KCQkJCTxnIGlkPSJPdmFsLTEtX3gyQl8tZW1haWwxOSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTE3LjAwMDAwMCwgMC4wMDAwMDApIj4NCgkJCQkJPHBhdGggaWQ9ImVtYWlsMTkiIGZpbGw9IiM4QkMzNEEiIGQ9Ik0zLjM1NCwxNC4yODFoMTQuMTczVjUuMzQ2SDMuMzU0VjE0LjI4MXogTTEwLjQ0LDEwLjg2M0w0LjYyNyw2LjAwOGgxMS42MjZMMTAuNDQsMTAuODYzDQoJCQkJCQl6IE04LjEyNSw5LjgxMkw0LjA1LDEzLjIxN1Y2LjQwOUw4LjEyNSw5LjgxMnogTTguNjUzLDEwLjI1M2wxLjc4OCwxLjQ5M2wxLjc4Ny0xLjQ5M2w0LjAyOSwzLjM2Nkg0LjYyNEw4LjY1MywxMC4yNTN6DQoJCQkJCQkgTTEyLjc1NSw5LjgxMmw0LjA3NS0zLjQwM3Y2LjgwOEwxMi43NTUsOS44MTJ6Ii8+DQoJCQkJPC9nPg0KCQkJPC9nPg0KCQk8L2c+DQoJPC9nPg0KPC9nPg0KPC9zdmc+DQo="
                                                alt="">
                                        <span class="helper"></span></span>
                                                            <a href="mailto:company@example.com">info@vdesiconnect.com</a>
                                                            <span class="helper"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </header>

                                            <section>
                                                <div class="container">
                                                    <div class="details clearfix row">
                                                        <div class="client left col-md-4">
                                                            <?php
                                                            $userAddress = unserialize($invoice->order_delivery_address);
                                                            ?>

                                                            <p>Shipping:</p>
                                                            <p class="name">
                                                                <strong style="font-size:16px;">
                                                                    {{--{!!  !!}--}}
                                                                    {{$userAddress['ua_name']}},
                                                                </strong>
                                                            </p>
                                                            <p>
                                                                {{--Plot No:91, Ganapathi Nivas, Allwyn colony 1, Hyderabad--}}


                                                                {{  implode(', ', $userAddress) }}


                                                                {{-- {{$userAddress['ua_address']}},
                                                                 {{$userAddress['ua_landmark']}},
                                                                 {{$userAddress['ua_city']}},
                                                                 {{$userAddress['ua_state']}},
                                                                 {{$userAddress['ua_country']}},
                                                                 {{$userAddress['ua_pincode']}},
                                                                 {{$userAddress['ua_phone']}},--}}

                                                            </p>
                                                            <a href="mailto:{{$userAddress['ua_email']}}">
                                                                {{$userAddress['ua_email']}}
                                                            </a>
                                                        </div>

                                                        <div class="client left col-md-4">
                                                            <?php
                                                            $userAddressBilling = unserialize($invoice->order_billing_address);
                                                            ?>

                                                            <p>Billing:</p>
                                                            <p class="name">
                                                                <strong style="font-size:16px;">
                                                                    {{--{!!  !!}--}}
                                                                    {{$userAddressBilling['ua_name']}},
                                                                </strong>
                                                            </p>
                                                            <p>
                                                                {{--Plot No:91, Ganapathi Nivas, Allwyn colony 1, Hyderabad--}}

                                                                {{  implode(', ', $userAddressBilling) }}


                                                                {{--                                                                {{ $userAddressBilling['ua_address'] }},--}}
                                                                {{--                                                                {{ $userAddressBilling['ua_landmark'] }},--}}
                                                                {{--                                                                {{$userAddressBilling['ua_city']}},--}}
                                                                {{--                                                                {{$userAddressBilling['ua_state']}},--}}
                                                                {{--                                                                {{$userAddressBilling['ua_country']}},--}}
                                                                {{--                                                                {{$userAddressBilling['ua_pincode']}},--}}
                                                                {{--                                                                {{$userAddressBilling['ua_phone']}},--}}


                                                            </p>


                                                            @if(isset($userAddressBilling['ua_email']))

                                                                <a href="mailto:{{$userAddressBilling['ua_email']}}">
                                                                    {{$userAddressBilling['ua_email']}}
                                                                </a>
                                                            @endif

                                                        </div>

                                                        <div class="data  col-md-4">
                                                            <div class="title">Invoice</div>
                                                            <div class="date">
                                                                <p>INVOICE NO:
                                                                    <strong>VDC-CART-{{ $invoice->order_id }}</strong>
                                                                </p>
                                                                Date of Invoice: {{$invoice->created_at}}<br>
                                                                {{--                                                                Due Date: {{$invoice->s_duedate}}--}}
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <thead>
                                                        <tr>
                                                            <th class="desc">Product Name</th>
                                                            <th class="qty">Quantity</th>
                                                            <th class="unit">Price</th>
                                                            <th class="unit">Delivery Charges</th>
                                                            <th class="total">Total</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        $subtotal = array();

                                                        ?>

                                                        @if (count($invoice->orderItems)> 0)
                                                            @foreach($invoice->orderItems AS $items)

                                                                <?php
                                                                //                                                                dump($items);
                                                                ?>

                                                                <tr>
                                                                    <td class="desc">
                                                                        <h6>
                                                                            {{ $items->oitem_product_name  }}

                                                                            {{--Service of Medical Supporting--}}
                                                                        </h6>

                                                                        {{ sortProductOptions($items->oitem_product_options)  }}

                                                                    </td>
                                                                    <td class="qty">
                                                                        {{ $items->oitem_qty  }}</td>
                                                                    <td class="unit">

                                                                        {!! currencySymbol($order_currency) !!}
                                                                        {{  $items->oitem_product_price }}

                                                                    </td>

                                                                    <td class="no-break">

                                                                        {!! currencySymbol($order_currency) !!}  {{  $items->oitem_delivery_charge  }}


                                                                    </td>

                                                                    <td class="no-break">


                                                                        <?php
                                                                        $subtotalItem = $items->oitem_sub_total + $items->oitem_delivery_charge;
                                                                        //                                                                        $subtotalItem = $items->oitem_sub_total + $items->oitem_delivery_charge;

                                                                        $subtotal[] = $subtotalItem;

                                                                        ?>

                                                                        {!! currencySymbol($order_currency) !!}
                                                                        {{ $subtotalItem  }}


                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endif


                                                        </tbody>
                                                    </table>
                                                    <div class="no-break">
                                                        <table class="grand-total">
                                                            <tbody>
                                                            <tr>
                                                                <td class="" width="40%"></td>
                                                                <td class="" colspan="2">SUBTOTAL:</td>
                                                                <td class="">
                                                                    {!! currencySymbol($order_currency) !!}

                                                                    {{--                                                                    {{  array_sum($subtotal)   }}--}}

                                                                    {{  $invoice->order_sub_total_price   }}

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class=""></td>

                                                                <td class="" colspan="2">
                                                                    Shipping :
                                                                </td>
                                                                <td class="">

                                                                    {!! currencySymbol($order_currency) !!}

                                                                    {{  $invoice->order_shipping_price   }}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class=""></td>
                                                                <td class="" colspan="2">
                                                                    TAX {{$invoice->s_tax}}%:
                                                                </td>
                                                                <td class="">
                                                                    <?php
                                                                    $taxAmount = array_sum($subtotal) / 100 * $invoice->s_tax;
                                                                    ?>
                                                                    {!! currencySymbol($order_currency) !!}

                                                                    {{  $taxAmount   }}
                                                                </td>
                                                            </tr>


                                                            <tr>
                                                                <td class=""></td>
                                                                <td class="" colspan="2">
                                                                    Total:
                                                                </td>
                                                                <td class="">
                                                                    <?php
                                                                    $taxotalAmount = $taxAmount + $invoice->order_shipping_price + $invoice->order_sub_total_price;
                                                                    ?>
                                                                    {!! currencySymbol($order_currency) !!}

                                                                    {{  $taxotalAmount   }}
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td class=""></td>
                                                                <td class="" colspan="2">
                                                                    Coupon Discount :
                                                                </td>
                                                                <td class="">

                                                                    - {!! currencySymbol($order_currency) !!}

                                                                    {{  $invoice->order_coupon_discount_amount   }}
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td class=""></td>
                                                                <td class="" colspan="2">GRAND TOTAL:</td>
                                                                <td class="">
                                                                    <?php
                                                                    $total = array_sum($subtotal) + $taxAmount;
                                                                    ?>
                                                                    {!! currencySymbol($order_currency) !!}

                                                                    {{  $invoice->order_total_price  }}
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </section>

                                            <footer>
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-8">
                                                            <div class="thanks">Thank you!</div>
                                                            {{--<div class="notice">--}}
                                                            {{--<div>NOTICE:</div>--}}
                                                            {{--<div>A finance charge of 1.5% will be made on unpaid--}}
                                                            {{--balances after 30 days.--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            <div class="end">Invoice was created on a computer and is
                                                                valid
                                                                without the signature and
                                                                seal.
                                                            </div>
                                                        </div>
                                                        <div class="col-4">
                                                            <div class="notice">
                                                                <div>Payment Status:</div>
                                                                <div class="thanks">
                                                                    {!! PayapPaymentStatusShow($invoice->order_payment_status) !!}
                                                                    {{--                                                                    {{$invoice->order_payment_status}} --}}

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </footer>
                                        </div>


                                    </div>


                                    <!-- small column -->


                                </div>
                                <!--/ row -->
                            </div>
                        </div>
                        <!--/ right side profile -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>

@endsection




@section('footerScripts')
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
     <script>
         $(function () {
             var doc = new jsPDF();
             var specialElementHandlers = {
                 '#editor': function (element, renderer) {
                     return true;
                 }
             };

             $('#cmd').click(function () {
                 doc.fromHTML($('#pdfgenerate').html(), 15, 15, {
                     'width': 170,
                     'elementHandlers': specialElementHandlers
                 });
                 doc.save('invoice.pdf');
             });
         })
     </script>--}}

    {{--<script src="https://kendo.cdn.telerik.com/2017.2.621/js/jszip.min.js"></script>--}}
    <script src="https://kendo.cdn.telerik.com/2019.1.220/js/kendo.all.min.js"></script>
    <script src="https://jasonday.github.io/printThis/printThis.js"></script>



    <script>


        function ExportPdf() {


            kendo.drawing
                .drawDOM("#pdfgenerate",
                    {
                        // forcePageBreak: ".page-break", // add this class to each element where you want manual page break
                        paperSize: "A4",
                        margin: {top: "1cm", bottom: "1cm"},
                        scale: 0.8,
                        height: 500,
                        // template: $("#page-template").html(),
                        keepTogether: ".prevent-split"
                    })
                .then(function (group) {
                    kendo.drawing.pdf.saveAs(group, "Exported-order-{{$invoice->order_reference_number}}.pdf")
                });


        }


        function printdiv(divID) {

            // $('#advanced').on("click", function () {
            $('#' + divID).printThis({
                // importCSS: true,
                importStyle: true,
                // base: false ,
                // pageTitle: "title",
                // selector:{
                //     loadCSS: "/frontend/css/print_invoice.css",
                //
                // }

                // header: "<h1>Look at all of my kitties!</h1>",
                // base: "https://jasonday.github.io/printThis/"
            });
            // });

        }


        $(function () {


            $('#cmd').click(function () {
                ExportPdf();
            })
        })

    </script>
@endsection