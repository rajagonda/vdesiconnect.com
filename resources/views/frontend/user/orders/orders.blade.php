@extends('frontend.layout')
@section('title', $title)


@section('header_styles')

@endsection


@section('content')



    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">My order History</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a href="{{route('userprofile')}}"> {{ Auth::user()->name }} </a></li>
                                {{--<li><a href="user-myorders.php">Orders </a></li>--}}
                                <li><a>Orders </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- left navigation -->
                        <div class="col-lg-3 userleftnav">
                            @include('frontend.user.usermenu')
                        </div>
                        <!--/ left navigation -->
                        <!-- right side profile -->
                        <div class="col-lg-9">
                            <div class="whitebox rightprofile p-3">
                                <!-- tab -->
                                <div class="myorderhistory">
                                    <div class="taborderhistory">

                                        @include('frontend.user.orders.orders_nav')
                                        <div class="resp-tabs-container hor_1 p-0">
                                            <!-- Delivered -->
                                            <div class="">
                                                <h4 class="coltitle py-0">
                                                    @if($active_status=='all')
                                                        All Orders
                                                    @elseif($active_status=='paid')
                                                        Successfull Orders
                                                    @else
                                                        Payment Failed
                                                    @endif

                                                </h4>
                                                <!-- user product -->
                                                @if (!empty($all_orders))
                                                    @foreach($all_orders AS $order)


                                                        <?php
                                                        $order_currency = $order->order_currency;
                                                        //                                                        dump($order_currency);
//                                                        dump($order);
                                                        ?>

                                                        <div class="userproduct position-relative">
                                                            <ul class="row primarydetails">

                                                                <li class="col-lg-3 col-6 col-md-4">
                                                                    <h6>Order Reference Number </h6>
                                                                    <p>{{ $order->order_reference_number }}</p>
                                                                </li>

                                                                <li class="col-lg-3 col-6 col-md-4">
                                                                    <h6>Order Date & Time </h6>
                                                                    <p>
                                                                        {{ \Carbon\Carbon::parse( $order->created_at)->format('d-m-Y h:m:s a') }}
                                                                    </p>
                                                                </li>

                                                                <li class="col-lg-3 col-6 col-md-4">
                                                                    <h6>Order Price </h6>
                                                                    <p>


                                                                        {!! currencySymbol($order_currency) !!}
                                                                        {{  $order->order_total_price  }} </br>


                                                                    </p>
                                                                </li>

                                                                <li class="col-lg-3 col-12">
                                                                    <a href="{{ route('orderDetails', ['id'=>$order->order_reference_number]) }}"
                                                                       class="whitebtn float-right">
                                                                        Order Details
                                                                        {{-- {{ dump($order->order_reference_number) }}--}}
                                                                    </a>
                                                                </li>

                                                            </ul>
                                                            <!-- row -->

                                                            @if (!empty($order->orderItems))
                                                                @foreach($order->orderItems AS $orderItems)

                                                                    <div class="row pb-1">
                                                                        <!-- col -->
                                                                        <div class="col-lg-2 col-md-3 col-6 col-sm-3">
                                                                            <?php
                                                                            if (isset($orderItems->getProduct->productImages[0])) {
                                                                                $item_img = url('/uploads/products/' . $orderItems->getProduct->productImages[0]->pi_image_name);

                                                                            } else {
                                                                                $item_img = '/frontend/images/no-image.png';
                                                                            }
                                                                            ?>
                                                                            <figure class="imgproduct mb-0">
                                                                                <a href="javascript:void(0)"> <img
                                                                                            src="{{$item_img}}" alt=""
                                                                                            title="" class="img-fluid"></a>
                                                                            </figure>
                                                                        </div>
                                                                        <!--/ col -->
                                                                        <!-- col -->
                                                                        <div class="col-lg-6  col-6 col-sm-6">
                                                                            <h6>
                                                                                Name: {{   $orderItems->oitem_product_name }}
                                                                            </h6>
                                                                            <small>

                                                                                {{ sortProductOptions($orderItems->oitem_product_options)  }}
                                                                            </small>
                                                                            {{--<p>Gift Message   : {{ $orderItems->oitem_message  }} </p>--}}
                                                                            <p>
                                                                                Item Quantity
                                                                                : {{ $orderItems->oitem_qty  }}
                                                                            </p>

                                                                            {{--  <a href="{{route('orderDetails',['id'=>$latest_order->order_id])}}" class="whitebtn">Order Details</a>--}}
                                                                            {{--                                                                            <div class="pt-3">--}}
                                                                            {{--                                                                                <h6>Payment Status :<span--}}
                                                                            {{--                                                                                            style="color:red;">{!! PayapPaymentStatusShow($order->order_payment_status) !!} </span>--}}
                                                                            {{--                                                                                </h6>--}}

                                                                            {{--                                                                            </div>--}}

                                                                        </div>
                                                                        <!--/ col -->
                                                                        <!-- col -->
                                                                        <div class="col-lg-4  col-12 col-sm-3 text-right">
                                                                            <h2 class="h3 forange">
                                                                                {!! currencySymbol($order_currency) !!}
                                                                                {{  $orderItems->oitem_sub_total +$orderItems->oitem_delivery_charge  }} </br>
                                                                            </h2>
                                                                        </div>

                                                                        <div class="col-lg-12">
                                                                            <p class="pt-3">
                                                                                {{--Replacement was allowed till--}}
                                                                                {{--                                                                                {{  (isset($orderItems->oitem_delivery_date) && !empty($orderItems->oitem_delivery_date))?\Carbon\Carbon::parse($orderItems->oitem_delivery_date)->add('3 days')->format('d-m-Y') :'' }}--}}
                                                                                @if(!empty($orderItems->oitem_delivery_date) || $orderItems->oitem_delivery_time)
                                                                                    <span class="pl-3 fgray">Delivered on

                                                                                        {{ $orderItems->oitem_delivery_date  }}
                                                                                        / {{ $orderItems->oitem_delivery_time  }}

                                                                                </span>
                                                                                @endif
                                                                            </p>
                                                                        </div>
                                                                    </div>

                                                                @endforeach
                                                            @endif


                                                        </div>

                                                @endforeach

                                            @endif




                                            <!-- /user product -->
                                            </div>
                                            <!--/ Delivered -->

                                        </div>
                                    </div>
                                </div>
                                <!--/ tab -->
                            </div>
                        </div>
                        <!--/ right side profile -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>


@endsection




@section('footerScripts')
    <script>
        $(function () {

            $('#changePassword').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    password: {
                        required: true
                    },
                    new_password: {
                        required: true,
                        maxlength: 12,
                        minlength: 6
                    },
                    confirm_password: {
                        required: true,
                        equalTo: "#new_password",
                        maxlength: 12,
                        minlength: 6
                    }
                },
                messages: {
                    password: {
                        required: 'Please enter password'
                    },
                    new_password: {
                        required: 'Please enter new password'
                    },
                    confirm_password: {
                        required: 'Please confirm password',
                        equalTo: "The confirm password and new password must match."
                    }
                },
            });
        });

    </script>
@endsection