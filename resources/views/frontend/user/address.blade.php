@extends('frontend.layout')
@section('title', $title)


@section('header_styles')

@endsection


@section('content')


    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">My Address</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li>
                                    <a href="{{route('userprofile')}}">{{ \Illuminate\Support\Facades\Auth::user()->name }}</a>
                                </li>

                                <li><a>My Address</a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- left navigation -->
                        <div class="col-lg-3 userleftnav">
                            @include('frontend.user.usermenu')
                        </div>
                        <!--/ left navigation -->
                        <!-- right side profile -->
                        <div class="col-lg-9">
                            @if (Session::has('flash_message'))
                                <br/>
                                <div class="alert alert-success alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>{{ Session::get('flash_message' ) }}</strong>
                                </div>
                            @endif
                            <div class="whitebox rightprofile p-3">
                                <!-- row -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="sectitle fbold pb-3">My Address </h5>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="float-right pb-3">
                                            <a data-toggle="modal" data-target="#newaddress" href="javascript:void(0)"  class="addbtn modelbtn"> + Create New </a>
                                        </div>
                                    </div>




                                    @if (count($userAddresses)> 0)
                                        @foreach($userAddresses AS $adds)
                                            <div class="col-lg-6 col-sm-6">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <?php
//                                                                                                                dump($adds);
                                                        ?>

                                                        {{--                                                        {{ $userAddresse->ua_defult }}--}}

                                                        <div><b>Name:</b> {{ $adds->ua_name }}</div>
                                                        <div><b>Address:</b>
                                                            {{ $adds->ua_address }},
                                                            @if( isset( $adds->getCity->city_name ) ){{ isset($adds->getCity->city_name) ?  $adds->getCity->city_name:''}}, @endif
                                                            @if( isset( $adds->getState->state_name ) ){{ isset($adds->getState->state_name) ?  $adds->getState->state_name:''}}. @endif
                                                        </div>
                                                        <div><b>Email:</b> {{ $adds->ua_email }}</div>
                                                        <div><b>Phone:</b> {{ $adds->ua_phone }}</div>
                                                        <div><b>Pin:</b> {{ $adds->ua_pincode }}</div>

                                                        <div class="col-md-12">
                                                            <div class="float-right">
                                                                <form class="float-left mt-3 ml-2" method="POST" autocomplete="off" onsubmit="return confirm('Confirm delete?');"
                                                                      id="useraddress"
                                                                      action="{{ route('userAddressBook') }}"
                                                                      accept-charset="UTF-8">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="flag" value="3"/>
                                                                    <input type="hidden" name="ua_id"
                                                                           value="{{ $adds->ua_id }}"/>


                                                                    <a class="greenlink modelbtn"
                                                                       href="javascript:void(0)"
                                                                       data-toggle="modal"
                                                                       data-target="#updateAddress_{{ $adds->ua_id }}">
                                                                        <span class="icon-edit icomoon"></span>
                                                                        Edit
                                                                    </a>


                                                                    <button class="greenlink " type="submit"><span class="icon-bin icomoon "></span>Delete </button>
                                                                </form>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>

                                            <div class="modal fade" id="updateAddress_{{ $adds->ua_id }}">
                                                <div class="modal-dialog  modal-lg">

                                                    <div class="modal-content">

                                                        @include('frontend.user.includs.update_adress_form')
                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach
                                    @endif


                                </div>
                                <!--/ row -->
                            </div>
                        </div>
                        <!--/ right side profile -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>


    <div class="modal fade" id="newaddress">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                @include('frontend.user.includs.adress_form')
            </div>
        </div>
    </div>


@endsection




@section('footerScripts')
    <script>
        $(function () {





            $('.ua_country').on('change', function () {
                var addresFormID = $(this).parents('form').attr('id');
                selectcountry(addresFormID, 'country')
            });

            $('.ua_state').on('change', function () {
                var addresFormID = $(this).parents('form').attr('id');
                console.log(addresFormID);
                selectcountry(addresFormID, 'state')
            });


            $('.modelbtn').on('click', function () {
                var modelId = $(this).data('target');
                console.log(modelId + " => model")

                $(modelId).on('shown.bs.modal', function () {
                    var addresFormID = $(modelId).find('form').attr('id');
                    selectcountry(addresFormID, 'country')
                })

            });


        });

    </script>
@endsection