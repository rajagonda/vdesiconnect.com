@extends('frontend.layout')
@section('title', $title)


@section('header_styles')

@endsection


@section('content')

    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Profile Information</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{ route('home') }}">Home</a></li>
                                <li><a href="javascript:void(0)">{!! Auth::user()->name !!}</a></li>
                                <li><a>Profile Information </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- left navigation -->
                        <div class="col-lg-3 userleftnav">
                            @include('frontend.user.usermenu')
                        </div>
                        <!--/ left navigation -->
                        <script>

                        </script>
                        <!-- right side profile -->
                        <div class="col-lg-9">
                            <form method="POST" id="profileInfo" action="{{ route('userprofile') }}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ laravelReturnMessageShow() }}
                                <div class="whitebox rightprofile p-3">
                                    <div class="row">
                                        <div class="col-lg-3 col-6 pic col-sm-4">
                                            @if(($user) && ($user->image))
                                                <div class="position-releative imagediv{{ $user->id }}">
                                                    <img src="/uploads/users/thumbs/{{$user->image}}" class="img-fluid profilepic d-block" alt="{!! Auth::user()->name !!}">

                                                    <a href="#" id="{{$user->id}}" class="delete_user_image"
                                                       onclick="return confirm(&quot;Confirm delete?&quot;)">  <i  class="fa fa-trash" aria-hidden="true"></i>
                                                    </a>
                                                </div>

                                            @else
                                                <img src="/frontend/images/data/profiledummypic.jpg" class="img-fluid profilepic w-100" alt="Dummy Profile Picture">
                                            @endif

                                            <div class="imghidebox" style="display: none">
                                                <img src="/frontend/images/data/profiledummypic.jpg"
                                                     class="img-fluid profilepic w-100">
                                            </div>

                                            {{--<a href="javascript:void(0)" class="new_Btn"><span class="icon-photo-camera icomoon"></span> Upload Photo</a>--}}
                                            {{--<input type="file" name="image" value="Upload Photo"/>--}}
                                                <div class="custom-file-input">
                                                    <input type="file" name="image">
                                                    <input type="text">
                                                    <input type="button" value="Add Picture">
                                                </div>


                                        </div>
                                        <div class="col-lg-7 col-sm-8">
                                            <h5 class="sectitle fbold pb-3">Profile Information</h5>
                                            <div class="form-group">
                                                <label>Write Your Name</label>
                                                <input type="text" name="name"
                                                       value="{{ !empty(old('name')) ? old('name') : ((($user) && ($user->name)) ? $user->name : '') }}"
                                                       class="form-control" placeholder="Name will be here">
                                                @if ($errors->has('name'))
                                                    <span class="text-danger help-block">{{ $errors->first('name') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label class="w-100">Gender Male / Female</label>
                                                <span><input type="radio" name="gender"
                                                             value="Male" {{ (!empty(old('gender')) && old('gender')=="Male")  ? 'selected' : ((($user) && ($user->gender == "Male")) ? 'checked' : '') }}><span
                                                            class="pl-1">Male</span></span>
                                                <span class="pl-3"><input type="radio" name="gender"
                                                                          value="Female" {{ (!empty(old('gender')) && old('gender')=="Female")  ? 'selected' : ((($user) && ($user->gender == "Female")) ? 'checked' : '') }}><span
                                                            class="pl-1">Female</span></span>
                                            </div>
                                            <div class="form-group" >
                                                <label>Date of Birth</label>
                                                <input type="text" placeholder="dob"
                                                       class="form-control ui-datepicker" name="dob"
                                                       value="{{ !empty(old('dob')) ? old('dob') : ((($user) && ($user->dob)) ? \Carbon\Carbon::parse($user->dob)->format('d-m-Y') : '') }}">
                                                @if ($errors->has('dob'))
                                                    <span class="text-danger help-block">{{ $errors->first('dob') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <?php
                                                $sortmobile = explode('-', $user->mobile);
                                                ?>

                                                <label>Phone Number</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <select name="mobile_prefix" class="">
                                        <option value="">Select</option>
                                        @if(sizeof(getCountrycodes())>0)
                                            @foreach(getCountrycodes() as $key=>$value)
                                                <option {{ ($sortmobile[0]==$key)?'selected':'' }}  value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        @endif
                                    </select>

                                </span>
                                                    </div>


                                                    <input type="text" class="form-control" name="mobile"
                                                           value="{{ !empty(old('mobile')) ? old('mobile') : ((isset($user) && isset($sortmobile[1])) ? $sortmobile[1] : '') }}"
                                                           class="form-control">
                                                </div>

                                                @if ($errors->has('mobile'))
                                                    <span class="text-danger help-block">{{ $errors->first('mobile') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label>Email Address</label>
                                                <input type="text" name="email" readonly
                                                       value="{{ !empty(old('email')) ? old('email') : ((($user) && ($user->email)) ? $user->email : '') }}"
                                                       class="form-control" placeholder="Enter Your Email">
                                            </div>
                                            {{--<div class="form-group">--}}
                                            {{--<label>Address Line 1</label>--}}
                                            {{--<input type="text" class="form-control"--}}
                                            {{--placeholder="Ex: H.No / Street Number / Street Name">--}}
                                            {{--</div>--}}
                                            {{--<div class="form-group">--}}
                                            {{--<label>Address Line 2</label>--}}
                                            {{--<input type="text" class="form-control"--}}
                                            {{--placeholder="Location name will be here">--}}
                                            {{--</div>--}}
                                            {{--<div class="form-group">--}}
                                            {{--<label>Country</label></label>--}}
                                            {{--<select class="form-control">--}}
                                            {{--<option>India</option>--}}
                                            {{--<option>United States of America</option>--}}
                                            {{--</select>--}}
                                            {{--</div>--}}
                                            {{--<div class="form-group">--}}
                                            {{--<label>State</label></label>--}}
                                            {{--<select class="form-control">--}}
                                            {{--<option>Telangana</option>--}}
                                            {{--<option>Andhra Pradesh</option>--}}
                                            {{--</select>--}}
                                            {{--</div>--}}
                                            {{--<div class="form-group">--}}
                                            {{--<label>City</label></label>--}}
                                            {{--<select class="form-control">--}}
                                            {{--<option>Hyderabad</option>--}}
                                            {{--<option>Secunderabad</option>--}}
                                            {{--</select>--}}
                                            {{--</div>--}}
                                            {{--<div class="form-group">--}}
                                            {{--<label>Pin Number</label>--}}
                                            {{--<input type="text" class="form-control" placeholder="Ex: 522265">--}}
                                            {{--</div>--}}
                                            <div class="form-group">
                                                <input type="submit" value="Submit" class="greenlink w-100">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--/ right side profile -->
                            </form>
                        </div>
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>

@endsection




@section('footerScripts')

    @include('frontend.user.scripts')
@endsection