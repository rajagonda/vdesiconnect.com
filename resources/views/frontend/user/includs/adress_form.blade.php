<form method="POST" action="{{ route('userAddAddressBook') }}" autocomplete="off"
      accept-charset="UTF-8" class="userAddress" id="userAddress_add" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="ua_id">
    <input type="hidden" name="flag" value="1">
    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title">Add New Delivery Address</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
        <div class="row">
            <div class="col-md-6">

                <div class="form-group">
                    <label>Name</label>
                    <input type="text" placeholder="Write Your Name" required class="form-control" name="ua_name">
                </div>



                <div class="form-group">
                    <label>Email</label>
                    <input type="text" placeholder="Enter email"   required class="form-control" name="ua_email">
                </div>

                {{--<div class="form-group">--}}
                    {{--<label>Phone Number</label>--}}
                    {{--<input type="text" placeholder="Enter Phone Number" required class="form-control"--}}
                           {{--name="ua_phone">--}}
                {{--</div>--}}

                <div class="form-group">
                    <label for="mobileNo">Mobile Number<span class="mand">*</span></label>


                    <div class="input-group">
                        <div class="input-group-prepend">
                                <span class="input-group-text" style="padding:0;">
                                    <select name="mobile_prefix" class="form-control" required>
                                        <option value="">Select</option>
                                        @if(sizeof(getCountrycodes($country))>0)
                                            @foreach(getCountrycodes($country) as $key=>$value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </span>
                        </div>
                        <input id="mobileNo" type="number" placeholder="Mobile" required
                               class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="ua_phone"
                               value="{{ old('mobile') }}">
                        @if ($errors->has('mobile'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('mobile') }}</strong>
                             </span>
                        @endif
                    </div>


                </div>




                <div class="form-group">
                    <label>Select Country</label>
                    <select name="ua_country" id="ua_country_add"  required class="form-control ua_country" data-country="" data-state="" >
                        <option value="">--Select Country--</option>
                        <?php
                        $countries = getAllCountries($country); ?>
                        @foreach($countries as $ks=>$s)
                            <option value="{{ $ks }}">
                                {{ $s }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Select State</label>

                    <select class="form-control ua_state" required name="ua_state">
                        <option value="">--Select State--</option>
                    </select>

                    {{--<input type="text" placeholder="State Name" class="form-control" name="ua_state">--}}
                </div>

            </div>
            <div class="col-md-6">

                <div class="form-group citybox"  style="display: none;">
                    <label>Select City</label>

                    <select class="form-control ua_city" name="ua_city">
                        <option value="">--Select City--</option>
                    </select>
                    <small>If Select Others, Please Specify Complete Address with City Name in below Address box.</small>


                </div>

                {{--<div class="form-group citybox" style="display: none;">--}}
                    {{--<label>City</label>--}}
                    {{--<input type="text" placeholder="City" class="form-control"--}}
                           {{--name="ua_city">--}}
                {{--</div>--}}



                <div class="form-group">
                    <label>Address</label>
                    <textarea class="form-control" placeholder="Enter Your Address" name="ua_address"></textarea>
                </div>

                <div class="form-group">
                    <label>Landmark</label>
                    <input type="text" placeholder="Land Mark (Ex: Near Hospital)" class="form-control"
                           name="ua_landmark">
                </div>

                <div class="form-group">
                    <label>Pincode / Zip</label>
                    <input type="number" placeholder="Area Pincode" required class="form-control" name="ua_pincode">
                    <small>Entering a Land Mark will help us in identifying your location faster</small>
                </div>

            </div>
        </div>










        <div class="form-group">
            <input type="checkbox" name="ua_defult"
                   value="1">
            <label>Default Address</label>
        </div>

    </div>
    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="submit" class="btn btn-success">Add Address</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
    </div>
</form>