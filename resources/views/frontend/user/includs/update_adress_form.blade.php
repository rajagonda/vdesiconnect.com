<form method="POST"
      action="{{ route('userAddAddressBook') }}"
      accept-charset="UTF-8"
      class="form-horizontal userAddress" autocomplete="off"
      enctype="multipart/form-data" id="userAddress_{{ $adds->ua_id }}">
    {{ csrf_field() }}
    <input type="hidden" name="ua_id"
           value="{{ $adds->ua_id }}">
    <input type="hidden" name="flag" value="1">
    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title">Update Delivery Address</h4>
        <button type="button" class="close"
                data-dismiss="modal">&times;
        </button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">
        <?php
//        dump($adds->ua_state);
        ?>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" placeholder="Write Your Name"
                           class="form-control" name="ua_name"
                           value="{{ !empty(old('ua_name')) ? old('ua_name') : ((($adds) && ($adds->ua_name)) ? $adds->ua_name : '') }}">
                </div>


                <div class="form-group">
                    <label>Email</label>
                    <input type="text" placeholder="Enter email"
                           class="form-control" name="ua_email"
                           value="{{ !empty(old('ua_email')) ? old('ua_email') : ((($adds) && ($adds->ua_email)) ? $adds->ua_email : '') }}">
                </div>

                {{--<div class="form-group">--}}
                    {{--<label>Phone Number</label>--}}
                    {{--<input type="text"--}}
                           {{--placeholder="Enter Phone Number"--}}
                           {{--class="form-control"--}}
                           {{--name="ua_phone"--}}
                           {{--value="{{ !empty(old('ua_phone')) ? old('ua_phone') : ((($adds) && ($adds->ua_phone)) ? $adds->ua_phone : '') }}">--}}
                {{--</div>--}}

                <div class="form-group">
                    <label for="mobileNo">Mobile Number<span class="mand">*</span></label>

                  @if(isset($adds->ua_phone))

                      <?php

                            $phonenum=explode('-',$adds->ua_phone);

                      ?>


                  @endif



                    <div class="input-group">
                        <div class="input-group-prepend">
                                <span class="input-group-text" style="height:38px; padding:0;">
                                    <select name="mobile_prefix" class="form-control" required>
                                        <option value="">Select</option>
                                        @if(sizeof(getCountrycodes(1))>0)
                                            @foreach(getCountrycodes(1) as $key=>$value)
                                                <option value="{{ $key }}" {{(@$phonenum[0] == $key) ? 'selected' :
                                                ''}} >{{ $value }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </span>
                        </div>
                        <input id="mobileNo" type="number" placeholder="Mobile" required
                               class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="ua_phone"
                               value="{{@$phonenum[1]}}">
                        @if ($errors->has('mobile'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('mobile') }}</strong>
                             </span>
                        @endif
                    </div>


                </div>


                <div class="form-group">
                    <label>Select Country</label>
                    <select name="ua_country" data-country="{{$adds->ua_country}}" class="form-control ua_country">
                        <option value="">--Select Country--</option>
                        <?php
                        $countries = getCountries(1); ?>
                        @foreach($countries as $ks=>$s)
                            <option value="{{ $ks }} " {{ (!empty(old('ua_country')) && old('ua_country')==$ks)  ? 'selected' : ((($adds) && ($adds->ua_country == $ks)) ? 'selected' : '') }}>
                                {{ $s }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Select State</label>
                    <select class="form-control ua_state" name="ua_state"  data-state="{{$adds->ua_state}}">
                        <option value="">--Select State--</option>

                    </select>

                    {{--<input type="text" placeholder="State Name"--}}
                    {{--class="form-control" name="ua_state"--}}
                    {{--value="{{ !empty(old('ua_state')) ? old('ua_state') : ((($adds) && ($adds->ua_state)) ? $adds->ua_state : '') }}">--}}
                    {{----}}

                </div>


            </div>
            <div class="col-md-6">

                <div class="form-group citybox" style="display: none;">
                    <label>Select City</label>


                    <select class="form-control ua_city" name="ua_city"  data-city="{{$adds->ua_city}}"><option value="">--Select City--</option>

                    </select>

                    {{--<input type="text" placeholder="City Name"--}}
                           {{--class="form-control" name="ua_city"--}}
                           {{--value="{{ !empty(old('ua_city')) ? old('ua_city') : ((($adds) && ($adds->ua_city)) ? $adds->ua_city : '') }}">--}}

                </div>

                {{--<div class="form-group citybox" style="display: none;">--}}
                    {{--<label>City</label>--}}
                    {{--<input type="text" placeholder="City" class="form-control" value="{{ !empty(old('ua_city')) ? old('ua_city') : ((($adds) && ($adds->ua_city)) ? $adds->ua_city : '') }}"--}}
                           {{--name="ua_city">--}}
                {{--</div>--}}

                <div class="form-group">
                    <label>Address</label>
                    <textarea class="form-control" placeholder="Enter Your Address" name="ua_address">{{ !empty(old('ua_address')) ? old('ua_address') : ((($adds) && ($adds->ua_address)) ? $adds->ua_address : '') }}</textarea>
                </div>
                <div class="form-group">
                    <label>Pincode / Zip</label>
                    <input type="text" placeholder="Area Pincode"
                           class="form-control" name="ua_pincode"
                           value="{{ !empty(old('ua_pincode')) ? old('ua_pincode') : ((($adds) && ($adds->ua_pincode)) ? $adds->ua_pincode : '') }}">
                </div>

                <div class="form-group">
                    <label>Landmark</label>
                    <input type="text"
                           placeholder="Land Mark (Ex: Near Hospital)"
                           class="form-control"
                           name="ua_landmark"
                           value="{{ !empty(old('ua_landmark')) ? old('ua_landmark') : ((($adds) && ($adds->ua_landmark)) ? $adds->ua_landmark : '') }}">
                    <small>Entering a Land Mark will help us in identifying your location faster</small>
                </div>


            </div>
        </div>


    </div>
    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="submit" class="btn btn-success">Update
            Address
        </button>
        <button type="button" class="btn btn-danger"
                data-dismiss="modal">Cancel
        </button>
    </div>
</form>