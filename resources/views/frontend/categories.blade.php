@extends('frontend.layout')
@section('title', $title)
@section('meta_keywords', $meta_keywords)
@section('meta_description', $meta_description)
@section('headerStyles')
@endsection
@section('content')

    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">

                            <?php
//                                                        dump($cat_name);
//                                                        dump($cat_option);
                            ?>


                            @if (isset($cat_name))

                                <article class="pagetitle">
                                    <h1 class="px20 py20">{{$cat_name->category_name}}</h1>
                                </article>
                                <!-- brudcrumb -->
                                <ul class="brcrumb">
                                    <li><a href="{{route('home')}}">Home</a></li>
                                    <li>
                                        @if (!isset($cat_option->category_alias))

                                            {{$cat_name->category_name}}
                                        @else
                                            <a href="{{route('categoriesPage',['category'=>$cat_name->category_alias, 'local'=>getCountryID('country_name')])}}">{{$cat_name->category_name}}</a>

                                        @endif

                                    </li>
                                    @if(!empty($cat_option))
                                        <li>
                                            {{--<a href="{{route('categoriesPage',['category'=>$cat_name->category_alias,'type'=>$cat_option->category_alias])}}">--}}
                                            <a>
                                                {{$cat_option->category_name}}

                                            </a>
                                            {{--</a>--}}
                                        </li>
                                    @endif
                                </ul>
                                <!--/ brudcrumb -->
                            @endif
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- filter container -->
                <div class="container">


                    @if(catagoriesNotes($cat_alias))

                        <div class="alert alert-info">
                            <strong>Note:</strong> {{ catagoriesNotes($cat_alias) }}.
                        </div>


                @endif



                <?php
                if (count($cat_products) > 0) {

                    $catItems = array();

                    foreach ($cat_products AS $cat_product) {
                        if (count($cat_product->productSKUs) > 0) {
                            $catItems[] = $cat_product;
                        }


                    }
                }
                ?>
                <?php
//                                dump($cat_products);
//                                dump($cat_products->toArray()['total']);
                ?>

                <!-- row -->
                    <div class="row filterrow py-2">
                        <div class="col-lg-10 col-8">
                            @if (isset($cat_name) && !empty($cat_name) && isset($catItems))
                                <?php
                                if (!isset($cat_option->category_alias)) {
                                    $pageTitle = $cat_name->category_name;

                                } else {

                                    $pageTitle = $cat_option->category_name;
                                }
                                ?>
                                <h5 class="mb-0">
                                    {{ $pageTitle }}
                                    (
                                    {{ $cat_products->toArray()['total'] }}

{{--                                    {{count($catItems)}}--}}
                                    Items
                                    )
                                </h5>
                            @endif
                        </div>
                        <div class="col-lg-2 col-4 text-right sortcol">
                            <?php
                            $getfilters = request()->get('filters')
                            ?>
{{--                            {{ $getfilters }}--}}

                            <select name="order" class="form-control filters">
                                <option {{ ($getfilters=='order_desc')?'selected':'' }}  value="order_desc">
                                    Latest to Older
                                </option>
                                <option {{ ($getfilters=='order_asc')?'selected':'' }}  value="order_asc">
                                    Older to Latest
                                </option>
                                <option {{ ($getfilters=='price_asc')?'selected':'' }}  value="price_asc">
                                    Price Low to High
                                </option>
                                <option {{ ($getfilters=='price_desc')?'selected':'' }}  value="price_desc">
                                    Price High to Low
                                </option>
                            </select>


                        </div>
                    </div>
                    <!--/ row -->
                </div>
                <!--/ filter container -->
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">


                        @if (isset($catItems) && count($catItems)> 0)

                            @foreach($catItems AS $cat_product)


                                <?php
                                //                                dump($cat_product);
                                ?>
                                <?php
                                $priceSKU = '';

                                $image_url = '';

                                if (count($cat_product->productImages) > 0) {
                                    $imageID = array_first($cat_product->productImages)->pi_id;
                                    $image_url = getImagesById($imageID);
                                    if ($image_url == '') {
                                        $image_url = '/frontend/images/noproduct-available.jpg';
                                    }
                                } else {
                                    $image_url = '/frontend/images/noproduct-available.jpg';
                                }



                                if (count($cat_product->productSKUs) > 0) {

                                    $priceSKU = $cat_product->productSKUs[0];
                                }

                                //                                                dump(array_first($cat_product->productImages)->pi_id);
                                ?>

                                @if (!empty($priceSKU))

                                    <div class="col-lg-3 col-6 col-md-4 text-center">
                                        <div class="productitem">
                                            <figure>

                                                <a href="{{ route('productPage', ['category'=>$cat_alias,'product'=>$cat_product->p_alias]) }}">

                                                    <img src="{{$image_url}}" alt="{{$cat_product->p_name}}"
                                                         title="{{$cat_product->p_name}}"
                                                         class="img-fluid @if($cat_product->p_availability=='out_of_stock') outofstock-img @endif"/>


                                                </a>
                                                <div class="hover">
                                                    <ul class="nav">
                                                        <li>

                                                            <a href="javascript:void(0)" data-toggle="tooltip"
                                                               data-id="{{ $cat_product->p_id }}"
                                                               data-userid="{{ Auth::id() }}"
                                                               data-placement="bottom" title="Add to Wishlist"
                                                               class="productaddwishlist{{ $cat_product->p_id }} addToWishList {{in_array($cat_product->p_id,$wishlistProducts) ? 'likeactive':''}} ">
                                                                <span class="icon-heartuser icomoon"></span>

                                                            </a>
                                                        </li>

                                                        <li>
                                                            <a href="{{ route('productPage', ['category'=>$cat_alias,'product'=>$cat_product->p_alias]) }}"
                                                               data-toggle="tooltip"
                                                               data-placement="bottom" title="View More"><span
                                                                        class="icon-external-link icomoon"></span></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </figure>
                                            @if($cat_product->p_availability=='out_of_stock')
                                                <span class="outofstock"><img src="/frontend/images/sold-outimg.png"
                                                                              alt="" title=""></span>
                                            @endif
                                            <article>
                                                <a class="proname"
                                                   href="{{ route('productPage', ['category'=>$cat_alias,'product'=>$cat_product->p_alias]) }}">

                                                    {{ substr($cat_product->p_name,0,20).'. . .' }}
                                                </a>
                                                <p>

                                                    {!! currencySymbol(getCurency()) !!}  {{ curencyConvert(getCurency(), $priceSKU->sku_store_price)  }}


                                                    @if ($priceSKU->sku_vdc_final_price != 0)
                                                        <small class="oldprice">
                                                            {!! currencySymbol(getCurency()) !!}  {{ curencyConvert(getCurency(), $priceSKU->sku_vdc_final_price)  }}
                                                        </small>
                                                    @endif


                                                </p>


                                                <a href="{{ route('productPage', ['category'=>$cat_alias,'product'=>$cat_product->p_alias]) }}"
                                                   class="btnlist">Add to Cart
                                                </a>
                                            </article>
                                        </div>
                                    </div>
                                @endif

                            @endforeach


                        @else

                            <div class="col-lg-12 text-center">
                                <img src="/frontend/images/no-product-found.jpg" class="img-fluid" alt="">
                            </div>

                        @endif


                    </div>

                    @if($cat_name->category_id=='143' || $cat_name->category_id=='144')
                        <h2 class="h3 py-4 text-center flight">More Rakhis coming soon, please revisit again for better pricing and options</h2>
                    @endif
                    <div class="text-center align-content-center">
                        {!! $cat_products->appends(['filters' => Request::get('filters')])->render() !!}
                    </div>


                    <!-- row -->
                </div>


                <!--/ container -->
            </section>


            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>





    <div class="modal" id="addtowlist">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Success</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    Product Added to Your Wish List Successfully
                </div>

            </div>
        </div>
    </div>
    <div class="modal" id="removefromwlist">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Success</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    Product deleted from Your Wish List Successfully
                </div>

            </div>
        </div>
    </div>


@endsection
@section('footerScripts')
    <script>

        function addParameterToURL(_url, _key, _value) {
            var param = _key + '=' + escape(_value);

            var sep = '&';
            if (_url.indexOf('?') < 0) {
                sep = '?';
            } else {
                var lastChar = _url.slice(-1);
                if (lastChar == '&') sep = '';
                if (lastChar == '?') sep = '';
            }
            _url += sep + param;

            return _url;
        }


        $(function () {
            $('.filters').on('change', function () {

                // var getHost = 'http://127.0.0.1:8000';
                var getHost = 'https://'+window.location.hostname;
                var getPath = window.location.pathname;

                var currentURL = getHost + getPath;
                // console.log(currentURL);

                var getvarUrl = addParameterToURL(currentURL, 'filters', $(this).val());
                // var currenturls = document.URL;
                // var params = currenturls.extract();


                // console.log(currenturls);
                console.log(getvarUrl);

                // console.log(getvarUrl);
                window.location.href = getvarUrl;


            });
        });
    </script>

@endsection