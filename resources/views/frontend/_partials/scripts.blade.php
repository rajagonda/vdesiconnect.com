<script type="text/javascript">window.$crisp = [];
    window.CRISP_WEBSITE_ID = "a629e327-463b-427d-aa62-cd52761319cc";
    (function () {
        d = document;
        s = d.createElement("script");
        s.src = "https://client.crisp.chat/l.js";
        s.async = 1;
        d.getElementsByTagName("head")[0].appendChild(s);
    })();
</script>


@include('global.scripts')

<script src="/frontend/js/custom.js"></script>


<script type="text/javascript">
    $(function () {



        $("#saveRestrictions").validate({
            rules: {
                // country: {
                //     required: true
                // },

            },
            messages: {
                // country: {
                //     required: 'Please enter Year',
                // }
            },
            submitHandler: function (form) {
                console.log($(form).serialize());

                $.ajax({
                    type: "POST",
                    url: "https://web.archive.org/web/20181229235151/http://www.skvapes.com/ajax/save/restrictions",
                    data: $(form).serialize(),
                    success: function (response) {
                        console.log(response.status);

                        if (response.status == 1) {
                            $('#myModal').modal('toggle');
                        } else {
                            $('.ageerror').html('Online Purchaser Must Have 21 and Above age .')
                        }


                    }
                });
                return false; // required to block normal submit since you used ajax
            }
        });



        $("#selectCounty").modal({
            backdrop: 'static',
            keyboard: false
        });




        $("#searchinpt").autocomplete({
            source: "{{route('searchProductInfo')}}",

            minLength: 1,
            select: function (event, ui) {
                /*
                var url = ui.item.id;
                if(url != '') {
                  location.href = '...' + url;
                }
                */
            },
            html: true,
            open: function (event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);

            }
        })
            .autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li>" +

                "<div>" +
                "<a href='" + item.product_url + "'>" +
                "<img width='50' height='50' style='float: left; margin: 10px' src='" + item.image + "'>" +

                "<span style='float: left; margin: 10px'>" +
                "<span style='padding: 0px 0px 10px 0px !important;display: block;'>" + item.name + "</span>" +
                "<div style='font-size: 11px;'>" +
                "MRP <span class='linethrough'>" + item.real_price + "</span> &nbsp;" +
                "Offer Price <span>" + item.sele_price + "</span>" +
                "</div>" +
                "</span>" +
                "</a>" +
                "<div style='clear: both'></div>" +
                "</div>" +

                "</li>").appendTo(ul);
        };





        $('.addToWishList').on('click', function () {
            // console.log('asd')

            var id = $(this).data('id');
            var userid = $(this).data('userid');
            var view = $(this).data('view');
            if (userid) {

                // var confirmRemobeWishlist = confirm('Please confirm');
                // if (confirmRemobeWishlist) {

                if (id != '') {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                        url: '{{ route('addToWishList') }}',
                        type: 'POST',
                        data: 'id=' + id,
                        success: function (response) {
                            if (response.status == 1) {
                                $('.productaddwishlist' + id).addClass("likeactive");
                                $('#addtowlist').modal('show');
                                $('.addToWishList').addClass('likeactive');
                            } else if (response.status == 2) {
                                $('.productaddwishlist' + id).removeClass("likeactive");
                                $('#removefromwlist').modal('show');
                                $('.addToWishList').removeClass('likeactive');
                            } else {
                                alert("Try again");
                            }
                            $('#wishlistcontents').text(response.count);

                            if (view == 'list') {
                                location.reload();
                            }

                        }
                    });
                }

                // } else {
                //     // alert('false');
                // }


            } else {

                var url = "{{route('userlogin')}}";
                window.location = url;

                // alert("Please login to add products to wishlist");
            }

        });


        $('.datepicker').datepicker({
            changeMonth: true,
            changeYear: true,
            format: 'dd-mm-yyyy'
        });

        $('.datepickerFront').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            minDate: new Date()
        });

    });


    function notifications($title) {
        $.notify({
// options
            icon: 'glyphicon glyphicon-warning-sign',
            title: $title,
            message: '',

        }, {
            type: "success",
            allow_dismiss: true,
            placement: {
                from: "top",
                align: "center"
            },
            delay: 500,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            }
        });
    }


</script>
