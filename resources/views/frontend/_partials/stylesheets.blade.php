<link rel="icon" type="image/png" sizes="32x32" href="/frontend/images/favicon-32x32.png">


<!-- style sheets -->
<link rel="stylesheet" href="/frontend/css/style.css">
<!-- fav icon -->
<link rel="apple-touch-icon" sizes="57x57" href="/frontend/images/fav/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/frontend/images/fav/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/frontend/images/fav/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/frontend/images/fav/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/frontend/images/fav/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/frontend/images/fav/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/frontend/images/fav/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/frontend/images/fav/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/frontend/images/fav/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192" href="/frontend/images/fav/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/frontend/images/fav/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/frontend/images/fav/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/frontend/images/fav/favicon-16x16.png">


<style>


    .mbody  .ui-widget.ui-widget-content {

    }

    .ui-widget.ui-widget-content {
        z-index: 1035 !important;
        height: 300px;
        overflow: auto;
        position: fixed;
    }

    .ui-widget-content .ui-state-active {
        background: #0b2e13;
        border-color: #0b2e13;
        color: #FFF;
    }
</style>