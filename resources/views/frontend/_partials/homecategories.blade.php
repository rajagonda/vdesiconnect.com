<div id="parentHorizontalTab">
    <ul class="resp-tabs-list hor_1">

        @foreach(getCategoriesById() as $catkey=>$category)
            <li>{{ $category }}</li>
        @endforeach

    </ul>
    <div class="resp-tabs-container hor_1">
    @foreach(getCategoriesById() as $catkey=>$category)

        @php
            $products=getProductsByCategory($catkey);
        @endphp
        @if(count($products)>0)
            <!-- gift articles -->
                <div>
                    <div class="row">

                        @foreach($products as $pkey=>$product)
                            <div class="col-lg-3 col-sm-6 col-md-4">
                                <div class="singleproduct">
                                    <figure class="text-center position-relative">
                                        <a href="#"><img class="img-fluid" src="/frontend/images/data/gift01.png" alt=""
                                                         title=""></a>
                                        <span class="discount position-absolute">-18%</span>
                                        <!-- hover -->
                                        <div class="prohover">
                                            <ul>
                                                <li><a href="#" data-toggle="tooltip" data-placement="bottom" data-id="{{ $product->product_id }}"
                                                       title="Add to Cart" class="addToCart"><i class="fas fa-shopping-cart"></i></a></li>
                                                <li><a href="productdetail.php" data-toggle="tooltip"
                                                       data-placement="bottom"
                                                       title="View More"><i class="fas fa-external-link-alt"></i></a>
                                                </li>
                                                <li><a href="#" data-toggle="tooltip" data-placement="bottom"
                                                       title="Add to Wish list" data-id="{{ $product->product_id }}" data-userid="{{ Auth::id() }}" class="productaddwishlist{{ $product->product_id }} addToWishList @if(in_array($product->product_id,$wishlistProducts))likeactive @endif"><i class="far fa-heart"></i></a></li>
                                            </ul>
                                        </div>
                                        <!--/ hover-->
                                    </figure>
                                    <article class="text-center py-2">
                                        <a class="d-block text-center" href="productdetail.php">{{ $product->product_name }}</a>
                                        <ul class="text-center">
                                            <li class="oldprice"><span><i class="fas fa-rupee-sign"></i>{{ $product->product_real_price }}</span>
                                            </li>
                                            <li><i class="fas fa-rupee-sign"></i>{{ $product->product_price }}</li>
                                        </ul>
                                    </article>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @else

                <div>
                    <div class="row">

                        No Products Found

                    </div>
                </div>

            @endif
        <!--/ Gift Articles-->
        @endforeach

    </div>
</div>
<!--/tab-->