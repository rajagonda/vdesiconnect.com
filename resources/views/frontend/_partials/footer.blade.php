@if (empty(session('siteCurencyModel')))
    <!-- on page loading windows -->



    <div class="modal fade sel-country" id="selectCounty" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title d-block text-center w-100" id="exampleModalLabel">
                        Select Country
                    </h5>
                </div>
                <div class="modal-body mx-5 pt-4 pb-5 text-center">
                    <p>Select Country to Deliver your order</p>
                    <form action="{{route('setCurrency')}}" method="get" >
                        <?php
                        $currentCountry = getCountry()
                        ?>
                        <input type="hidden" name="siteCurencyModel" value="ok" />

                        <select name="newcurrency" id="newcurrency"  class="form-control">
                            <?php
                            $countries = getCountries(); ?>
                            @foreach($countries as $ks=>$s)
                                <option {{($currentCountry==$ks)?'selected':''}} value="{{ $ks }}">
                                    {{ $s }}
                                </option>
                            @endforeach
                        </select>

                        <input type="SUBMIT" class="btn btn-info w-100 mt-3 greenlink">
                        {{--<button type="submit" name="submit">Submit</button>--}}
                    </form>
                </div>
            </div>
        </div>
    </div>





@endif







<footer>
    <!-- news letter footer -->
    <section class="newsletterfooter">
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6 mtcenter col-sm-6">
                    <h5>Subscribe News letter</h5>
                    <p>Good things come to those who sign up for our newsletter! </p>
                    <form method="POST" id="subscriptionForm" action="{{ route('subscriptionSave') }}"
                          accept-charset="UTF-8"
                          class="contactform">
                        {{ csrf_field() }}
                    <div class="newsform">
                        <div class="row">

                            <div class="col-lg-9 col-sm-7">
                                <input type="text" name="email" placeholder="Enter Your Email">
                            </div>
                            <div class="col-lg-3 col-sm-5">
                                <input type="submit" value="Subscribe">
                            </div>

                        </div>
                    </div>
                        <div class="subscriptionMessage"></div>
                    </form>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-6 col-sm-6 mtcenter ">
                    <div class="footersocial">
                        <h5>Follow us on Social</h5>
                        <p>You Can Get keep updates through social media </p>
                        <ul class="nav">
                            <li><a href="https://www.facebook.com/vdesi.connect" target="_blank"><span class="icon-facebook-logo icomoon"></span></a></li>
                            <li><a href="https://twitter.com/vdesiconnect" target="_blank"><span class="icon-twitter-logo icomoon"></span></a></li>
                            <li><a href="https://www.linkedin.com/in/vdesi-connect-030b9418a/
" target="_blank"><span class="icon-linkedin-logo icomoon"></span></a></li>
                            <li><a href="https://www.instagram.com/vdesiconnect5/?hl=en" target="_blank"><span class="icon-instagram icomoon"></span></a></li>
                        </ul>
                    </div>
                </div>
                <!-- col -->
            </div>
            <!--/ row -->
        </div>
    </section>
    <!--/ news letter footer-->
    <!-- navigation footer -->
    <section class="navfooter py-3">
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                {{--<div class="col-lg-4 col-md-12 footercol">--}}
                    {{--<h5>Vdesi Connect </h5>--}}
                    {{--<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.     <a href="about.php"> Read more</a>--}}
                    {{--</p>--}}
                {{--</div>--}}
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-3 col-6 col-md-3 footercol">
                    <h5>Company </h5>
                    <ul>
                        <li><a href="{{route('page_about')}}">About</a></li>
                        <li><a href="{{route('page_corevalues')}}">Core Values</a></li>
                        <li><a href="{{route('bloglist')}}">Blog</a></li>
                        <li><a href="{{route('page_career')}}">Career</a></li>
                        {{--<li><a href="/sitemap.xml" target="_blank">Sitemap</a></li>--}}
                        <li><a href="{{route('page_conatct')}}">Contact us</a></li>
                        <li><a href="{{route('page_faq')}}">Faq</a></li>
                        <li><a href="{{route('page_citieswedeliver')}}">Cities We Deliver</a></li>
                    </ul>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-3 col-6 col-md-3 footercol">
                    <h5>OUR POLICIES </h5>
                    <ul>
                        <li><a href="{{route('page_privacy')}}">Privacy Policy</a></li>
                        <li><a href="{{route('page_returnpolicy')}}">Return Policy</a></li>
                        <li><a href="{{route('page_termsofuse')}}">Terms of  Use</a></li>
                    </ul>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-3 col-6 col-md-3 footercol">
                    <h5>CATEGORIES </h5>
                    <ul>
                        @if (count(getCategory())> 0)

                            @foreach(getCategory() AS $category)
                                <li><a href="{{route('categoriesPage', ['category'=>$category->category_alias, 'local'=>getCountryID('country_name')])}}">
                                        {{$category->category_name}}
                                    </a></li>
                            @endforeach

                        @endif


                    </ul>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-3 col-6 col-md-3 footercol">
                    <h5>Services </h5>
                    <ul>

                        @if(count(serviceInfoData()))
                            @foreach(serviceInfoData() AS $serviceInfo)
                                <li class="">
                                    <a class="nav-link"
                                       href="{{route('servicePage', ['service'=>$serviceInfo])}}">
                                        {{ wordSort($serviceInfo) }}

                                    </a>
                                </li>
                            @endforeach

                        @endif


                       {{-- <li >
                            <a href="{{route('servicePage', ['service'=>'medical'])}}" >Medical
                                Assistance</a>
                        </li>
                        <li >
                            <a href="{{route('servicePage', ['service'=>'online-tutor'])}}" >Online
                                Tutor</a>
                        </li>

                        <li >
                            <a href="{{route('servicePage', ['service'=>'property-management'])}}" >Property
                                Management</a>
                        </li>

                        <li >
                            <a href="{{route('servicePage', ['service'=>'real-estate'])}}">Real Estate</a>
                        </li>
                        <li >
                            <a href="{{route('servicePage', ['service'=>'visa-support-services'])}}" >Visa Services
                                (UK, US, UAE, etc)</a>
                        </li>
                        <li >
                            <a href="{{route('servicePage', ['service'=>'visitors-insurance'])}}" >Insurance
                                (Health &amp; Visitors)
                            </a>
                        </li>
                        <li >
                            <a href="{{route('servicePage', ['service'=>'web-developement'])}}" >Web Development
                            </a>
                        </li>
                        <li >
                            <a href="{{route('servicePage', ['service'=>'online-radio'])}}" >Online
                                Radio</a>
                        </li>
--}}
                        {{--<li><a href="onlinetutor.php">Online Tutors</a></li>--}}
                        {{--<li><a href="medicalservices.php">Medical Services</a></li>--}}
                        {{--<li><a href="visaservices.php">Visa Suport Services</a></li>--}}
                        {{--<li><a href="propertymanagement.php">Property Management</a></li>--}}
                        {{--<li><a href="onlineradio.php">Online Radio</a></li>--}}
                        {{--<li><a href="visitorsinsurance.php">Visitors Insurance</a></li>--}}
                    </ul>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
    </section>
    <!--/ navigation footer -->
    <!-- copy rights -->
    <div class="copyrights">
        <a id="movetop" class="" href="javascript:void(0)"><span class="icon-up-arrow-key icomoon"></span></a>
        <div class="container">
            <div class="row">
                <!-- col -->
                <div class="col-lg-6 mtcenter col-sm-6">
                    <p>All Rights & Copyright 2019 vdesiconnect.com</p>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-6 col-sm-6 text-right dmnone">
                    <img src="/frontend/images/cards.png" alt="" title="" class="img-fluid">
                </div>
                <!--/ col -->
            </div>
        </div>
    </div>
    <!--/ copty rights -->
</footer>