@if(isset($item->attributes->currency))

<h6 class="pb-2">Cart Items ({{ Cart::getContent()->count() }}) Items</h6>

<ul style="display: none">
    @if(Cart::getContent()->count()>0)
        @foreach( Cart::getContent() as $item)
            <li>
                <p class="pb-0">{{ $item->name }}</p>
                <p>
                    <span>Quantity : {{ $item->quantity }}	</span>
                    <span
                            class="float-right">
                        {!! currencySymbol($item->attributes->currency) !!}
                        {{ $item->price }}
                    </span>
                </p>
                <p class="pb-0"><span>Delivery Charges:  	</span> <span
                            class="float-right">
                        {!! currencySymbol($item->attributes->currency) !!}
                        {{ ($item->attributes->has('delivery_charge') ? $item->attributes->delivery_charge.' * '.$item->quantity : 0) }}
                    </span>

            </li>
        @endforeach
        <li>
            <p class="pb-0"><span class="fgreen fbold">Total: 	</span> <span
                        class="float-right  fgreen">
                    {!! currencySymbol($item->attributes->currency) !!}

                    {{ Cart::getSubTotal() }}</span>
            </p>

        </li>
        @if($item->attributes->message!='' || $item->attributes->delivery_date!='' || $item->attributes->delivery_time!='')
            <li>
                @if($item->attributes->message!='')
                    <p class="pb-0"><span class="fgreen fbold">Gift Message: 	</span> <span
                                class="float-right  fgreen">{{ $item->attributes->message }}</span>
                    </p>
                @endif
                @if($item->attributes->delivery_date!='')
                    <p class="pb-0"><span class="fgreen fbold">Delivery Date: 	</span> <span
                                class="float-right  fgreen">{{ $item->attributes->delivery_date }}</span>
                    </p>
                @endif
                @if($item->attributes->delivery_time!='')
                    <p class="pb-0"><span class="fgreen fbold">Delivery Time: 	</span> <span
                                class="float-right  fgreen">{{ $item->attributes->delivery_time }}</span>
                    </p>
                @endif
            </li>
        @endif
    @else
        <li>No products found</li>
    @endif
</ul>


<h4 class="fgreen fbold py-2 h4">Total<span
            class="float-right fgreen fbold">


            {!! currencySymbol($item->attributes->currency) !!} {{  Cart::getSubTotal() }}


    </span>
</h4>
<h4 class="fgreen fbold py-2 h4">
    Discount {!! currencySymbol($item->attributes->currency) !!}
    <span class="float-right fgreen fbold ">
         0
    </span>
</h4>
<h4 class="fgreen fbold py-2 h4">You Pay {!! currencySymbol($item->attributes->currency) !!} <span
            class="float-right fgreen fbold ">

       {{  Cart::getSubTotal() }}

    </span>
</h4>
@endif