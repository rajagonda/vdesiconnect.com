<?php
$siteSettings = vdcSettings();
?>
<header class="fixed-top">
    <!-- top header -->
    <section class="topheader">
        <section class="headercontainer">
            <!-- container fluid-->
            <div class="container-fluid">
                <!-- row  -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-3 col-sm-10 align-self-center">
                        <ul class="nav leftnav">
                            <li class="nav-item contactnumber">Call us {{ $siteSettings->tolfree_email }}, +1 646-241-3704</li>
                        </ul>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-9 pr-0 align-self-center">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <button class="navbar-toggler" type="button" data-toggle="collapse"
                                    data-target="#navbarNavtop" aria-controls="navbarNav" aria-expanded="false"
                                    aria-label="Toggle navigation">
                                <span class="icon-menu icomoon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarNavtop">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item currency">
                                        <form action="{{route('setCurrency')}}" method="get">
                                            <?php
                                            $currentCountry = getCountry()
                                            ?>
{{--                                            {{ csrf_field() }}--}}
                                            {{--  County :--}}
                                            <input type="hidden" name="siteCurencyModel" value="ok" />

                                            <select name="newcurrency" id="newcurrency" onchange="this.form.submit()"
                                                    class="form-control">
                                                <?php
                                                $countries = getCountries(); ?>
                                                @foreach($countries as $ks=>$s)
                                                    <option {{($currentCountry==$ks)?'selected':''}} value="{{ $ks }}">
                                                        {{ $s }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            {{--  {{ $currentCurrency }}--}}
                                            {{-- <select name="newcurrency" onchange="this.form.submit()">--}}
                                                {{--<option {{($currentCurrency=='USD')?'selected':''}} >USD</option>--}}
                                                {{-- <option {{($currentCurrency=='INR')?'selected':''}}>INR</option>--}}
                                            {{--  </select>--}}
                                        </form>
                                    </li>
                                    <li class="nav-item active">
                                        <a class="nav-link" href="{{route('home')}}">Home </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('page_citieswedeliver')}}">Cities We  Deliver </a></li>
                                    <li class="nav-item">
                                        <a class="nav-link" target="_blank" href="{{route('vendor_home')}}">
                                            Become Vendor
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('page_conatct')}}">Contact</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('bloglist')}}">Blog</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('page_faq')}}">Faq</a>
                                    </li>

                                    <li class="nav-item social">
                                        <a href="{{ $siteSettings->social_fb }}" target="_blank"><span
                                                    class="icon-facebook-logo icomoon"></span>
                                        </a>
                                        <a href="{{ $siteSettings->social_tw }}" target="_blank"><span
                                                    class="icon-twitter-logo icomoon"></span>
                                        </a>
                                        <a href="{{ $siteSettings->social_in }}" target="_blank"><span
                                                    class="icon-linkedin-logo icomoon"></span>
                                        </a>
                                        <a href="https://www.instagram.com/vdesiconnect5/?hl=en" target="_blank"><span
                                                    class="icon-instagram icomoon"></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container fluid-->
        </section>
    </section>
    <!--/ top header -->
    <!-- middle header -->
    <section class="middle-header">
        <section class="headercontainer">
            <div class="container-fluid">
                <nav class="navbar navbar-light px-0 py-0">
                    <!-- row -->
                    <div class="row w-100">
                        <div class="col-lg-3 col-md-6">
                            <a class="navbar-brand pb-0" href="{{route('home')}}">
                                <img src="/frontend/images/logo.svg" alt="" title="">
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 align-self-center dmnone">
                            <!-- sarch -->
                            <div class="headersearch position-relative">
                                <input type="text" id="searchinpt" placeholder="Search Your Gift Here">
                                <span class="icon-search icomoon"></span>

                            </div>
                            <!--/ search -->
                        </div>
                        <div class="col-lg-5 text-right align-self-center pr-0">
                            <div class="navbar-collapse" id="navbarNavDropdown">
                                <ul class="navbar-nav nav">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('cartPage')}}">
                                            <span class="icon-online-shopping-cart icomoon"></span>Cart <span
                                                    class="vol" id="cartcontents">{{ Cart::getContent()->count() }}</span> </a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('wishList') }}"><span
                                                    class="icon-heartwhite icomoon"></span> Wishlist <span class="vol"  id="wishlistcontents"> @auth{{ count(getWishlistProducts()) }}@else
                                                    0 @endauth</span>
                                        </a>
                                    </li>

                                    @if (Auth::guard('web')->check())
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="icon-userprofile icomoon"></span> {{ substr(Auth::user()->name,0,10).'. . .' }}
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                <a class="dropdown-item" href="{{route('userprofile')}}">Profile
                                                    Information</a>
                                                <a class="dropdown-item" href="{{route('userChangePassword')}}">Change
                                                    Password</a>
                                                <a class="dropdown-item" href="{{route('ServiceInvoices')}}">My Service
                                                    Invoices</a>
                                                <a class="dropdown-item" href="{{route('orders',['status'=>'paid'])}}">My Orders</a>
                                                <a class="dropdown-item" href="{{route('wishList')}}">My Wishlist</a>

                                                <a href="{{ route('userlogout') }}" class="dropdown-item"
                                                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                                                <form id="logout-form" action="{{ route('userlogout') }}" method="POST"
                                                      style="display: none;"> {{ csrf_field() }} </form>
                                            </div>
                                        </li>
                                    @else
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{route('userlogin')}}"><span  class="icon-userprofile icomoon"></span> Login </a>
                                        </li>
                                    @endauth

                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--/ row -->
                </nav>
            </div>
        </section>
    </section>
    <!-- middle header -->
    <!-- bottom navbar -->
    <section class="bottomnavbar">
        <section class="headercontainer">
            <!-- nav bar-->
            <div id="navbar" role="navigation" class="navbar navbar-expand-lg">
                <div class="container-fluid pl-0">
                    <button type="button" data-toggle="collapse" data-target="#navigation"
                            class="navbar-toggler btn-template-outlined"><span class="icon-menu icomoon"></span>Product
                        Navigation
                    </button>
                    <div id="navigation" class="navbar-collapse collapse">
                        <?php
                        // dump(getCategory())
                        ?>
                        <ul class="nav navbar-nav">
                            <!-- ========== Professional Services ==================-->
                            <li class="nav-item dropdown {{ $active_menu == 'servicePage' ?'active':''}}">
                                <a href="javascript: void(0)" data-toggle="dropdown" class="dropdown-toggle"> Services <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu">
                                    @if(count(serviceInfoData()))
                                        <?php
                                        $serviceInfoDataLimit = array_slice(serviceInfoData(), 0, 5, true);
                                        ?>

                                        @foreach($serviceInfoDataLimit AS $serviceInfo)
                                            {{--{{  $sub_active_menu==$serviceInfo?'active':'' }}--}}
                                            <li class="dropdown-item {{  $sub_active_menu==$serviceInfo?'active':'' }} ">
                                                <a class="nav-link"
                                                   href="{{route('servicePage', ['service'=>$serviceInfo])}}">
                                                    {{ wordSort($serviceInfo) }}

                                                </a>
                                            </li>
                                        @endforeach

                                    @endif


                                        <li class="dropdown-item {{  $sub_active_menu=='allserveice'?'active':'' }} ">
                                            <a class="nav-link"
                                               href="{{route('servicePage')}}">
                                               More Services <i class="fa fa-plus"></i>

                                            </a>
                                        </li>

                                </ul>
                            </li>
                            <!-- ========== Professional Services ==================-->

                            @if (count(getCategory())> 0)
                                @foreach(getCategory() AS $category)
                                    <?php
                                    $countryShippedMenu = category_shipped($category->category_shipped);

//                                    dump($countryShippedMenu);

//                                    if (isset($countryShippedMenu[getCountry()])) {
//                                        echo 'ok';
//                                    } else {
//                                    }
                                    ?>
                                    @if (isset($countryShippedMenu[getCountry()]))
                                    <li class="nav-item dropdown menu-large  {{ $active_menu == $category->category_alias ?'active':''}}">
                                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                            {{$category->category_name}}

                                            <b class="caret"></b>
                                        </a>
                                        <ul class="dropdown-menu megamenu">
                                            <li>
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        <img src="{{url('/uploads/categories/'.$category->category_image)}}"
                                                             alt="" class="img-fluid d-none d-lg-block">
                                                    </div>

                                                    <div class="col-lg-10">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <h5>{{ catSubSortByTypes($category->category_id) }}</h5>
                                                                <ul class="list-unstyled mb-3 row">
                                                                    <li class="nav-item col-lg-4">
                                                                        <a href="{{route('categoriesPage', ['category'=>$category->category_alias, 'local'=>getCountryID('country_name')])}}"
                                                                           class="nav-link">All</a>
                                                                    </li>
                                                                    @foreach($category->getSubCategoryTypes AS $subcategoryTypes)
                                                                        <li class="nav-item col-lg-4">
                                                                            <a href="{{route('categoriesPage', ['category'=>$category->category_alias, 'type'=>$subcategoryTypes->category_alias, 'local'=>getCountryID('country_name')])}}"
                                                                               class="nav-link">{{$subcategoryTypes->category_name}}
                                                                            </a>
                                                                        </li>
                                                                    @endforeach


                                                                    @if($category->category_alias=='home-foods')


                                                                        <li class="nav-item  col-lg-4">
                                                                            <a href="{{ route('ammaChethiVanta') }}" class="nav-link">
                                                                                Amma Chethi Vanta
                                                                            </a>
                                                                        </li>


                                                                                {{--<li class="nav-item col-lg-4">--}}
                                                                                    {{--<a href="#" class="nav-link">--}}
                                                                                        {{--test link--}}
                                                                                    {{--</a>--}}
                                                                                {{--</li>--}}
                                                                        @endif


                                                                </ul>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                @if (count($category->getSubCategoryOthers)> 0)
                                                                    <h5>
                                                                        {{ catSubSortNames($category->category_id) }}
                                                                    </h5>
                                                                    <ul class="list-unstyled mb-3  row">
                                                                        @foreach($category->getSubCategoryOthers AS $subcategoryOthers)
                                                                            <li class="nav-item col-lg-4">
                                                                                <a href="{{route('categoriesSplPage', ['category'=>$category->category_alias, 'foritem'=>$subcategoryOthers->category_alias, 'local'=>getCountryID('country_name')])}}"
                                                                                   class="nav-link">{{$subcategoryOthers->category_name}}</a>
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>

                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>



                                        </ul>

                                    </li>



                                    @endif



                                @endforeach


                            @endif

                                {{--<li class="nav-item   {{ $active_menu == 'amma-chethi-vanta' ?'active':''}}">--}}
                                    {{--<a href="{{ route('ammaChethiVanta') }}" class="dropdown-toggle noic">--}}
                                        {{--Amma Chethi Vanta--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                        </ul>

                    </div>
                </div>
            </div>
            <!--/ nav bar -->
        </section>
    </section>
    <!--/ bottom navbar-->
</header>
