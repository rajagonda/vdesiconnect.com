<ul class="myaccountleftnav">
    <li class="ltnavactive"><a href="account-profileinfo.php"><i class="fas fa-user"></i> Profile Information</a></li>
    <li><a href="account-changepassword.php"><i class="fas fa-unlock-alt"></i> Change Password</a></li>
    <li><a href="account-myorders.php"><i class="fas fa-check-circle"></i> My Orders</a></li>
    <li><a href="account-cancelreturns.php"><i class="fas fa-times-circle"></i> Cancel/Returns</a></li>
    <li><a href="account-addressbook.php"><i class="fas fa-map-marker-alt"></i> Address Book</a></li>
    <li><a href="mywishlist.php"><i class="fas fa-heart"></i> My Wish List</a></li>
    {{--<li><a href="index.php"><i class="fas fa-user"></i> Logout</a></li>--}}
</ul>
