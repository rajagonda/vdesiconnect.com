@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')


    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">My Cart</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{ route('home') }}">Home</a></li>
                                <li><a>My Cart List </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container">
                    <!-- row -->

                    <?php
                    $shippingAmountList = shippingPriceList(session('siteCountryinfo')->country_id);

                    if(is_array($shippingAmountList) && count($shippingAmountList)> 0){

                        $shippingAmountList = array_chunk($shippingAmountList, 5);

                        if(isset($shippingAmountList[0])){
                            $shippingAmountList = $shippingAmountList[0];
                        }

                        array_unshift($shippingAmountList, '');

                        unset($shippingAmountList[0]);
                    }
//                        dd($shippingAmountList);

                    ?>
                    @if(is_array($shippingAmountList) && count($shippingAmountList)>0)

                        <?php

                        $barPercent = 100 / count($shippingAmountList);

                        $barTotalweight = 0;
                        if (Cart::getContent()->count() > 0) {
                            foreach (Cart::getContent()->sort() as $item) {
                                $weightInfo = sortProductOption($item->attributes->options, 'weight');

//                                dd($weightInfo);

                                $weight = $item->quantity * (float)$weightInfo;

                                $barTotalweight += $weight;
                            }
                        }

                        ?>

                        Shipping Charges ({{ session('siteCountryinfo')->country_name }})

                        <div class="progress progress-cart">

                            @foreach($shippingAmountList AS $wight=>$shippingAmount)
                                <?php
                                $listPrice = curencyConvert(getCurency(), $shippingAmount);
                                ?>
                                <div class="progress-bar   {{ ($barTotalweight >= $wight)?'add-weight':'color-bar' }} "
                                     style="width: {{$barPercent}}%">
                                    {{ $wight }}Kgs

{{--                                    {{ productWightFarmart($wight) }}--}}


                                    {{--  / {!! currencySymbol(getCurency()) !!}{{  $listPrice }}--}}

                                    @if( $barTotalweight == $wight)
                                        *
                                    @endif

                                </div>
                            @endforeach


                        </div>

                            <?php
                            $moreKgs = $barTotalweight - 5;

                            if ($moreKgs < 0) {
                            ?>
                            <h5>
                                select  {{ abs($moreKgs) }} more kgs get better discount
                            </h5>
                            <?php
                            } else {
                                $moreKgs = '';
                            }
                            ?>



                    @endif


                    <div class="whiterow mb-3">
                        <div class="row">
                            <div class="col-lg-6 col-6">
                                <h4 class="pl-3 mb-0">Cart Items ({{ Cart::getContent()->count() }})</h4>
                            </div>
                            <div class="col-lg-6 col-6 text-right">
                                <h5 class=" pr-3 mb-0">
                                    {{--                                    {{ currencySymbol('USD') }} {{ Cart::getSubTotal() }}--}}
                                    {{--                                    {{ currencySymbol('USD') }} {{ curencyConvert('USD', Cart::getSubTotal() ) }}--}}
                                </h5>
                            </div>
                        </div>
                    </div>
                    <!--/ row -->


                    <!-- row -->
                    <div class="row">
                        <!--left col -->
                        <div class="col-lg-9 col-sm-8">
                            <!-- row -->


                            <div class="row">

                            <?php

                            //                                                        Cart::clear();



                            //                            sort(Cart::getContent())

                            //                            dd(Cart::getContent());





                            ?>

                            <?php
                            $totalweight = 0;
                            ?>

                            @if(Cart::getContent()->count()>0)

                                @foreach( Cart::getContent()->sort() as $item)



                                    <?php

                                    $weightInfo = sortProductOption($item->attributes->options, 'weight');






                                    $weight = $item->quantity * (float)$weightInfo;

                                    $totalweight += $weight;

                                    $product = getProduct($item->id);

                                    $image_url = '';

                                    if (count($product->productImages) > 0) {
                                        $imageID = array_first($product->productImages)->pi_id;
                                        $image_url = getImagesById($imageID);

                                        if ($image_url == '') {
                                            $image_url = '/frontend/images/noproducts.jpg';
                                        }


                                    } else {
                                        $image_url = '/frontend/images/noproducts.jpg';
                                    }

//dump($totalweight);

                                    ?>

                                    <!-- small column -->
                                        <div class="col-lg-12">


                                            <div class="smallcol mb-3">
                                                <div class="row">
                                                    <div class="col-lg-3 col-sm-3 pl-3">
                                                        <figure>
                                                            <a href="{{ route('productPage', ['category'=>$product->getCategory->category_alias,'product'=>$product->p_alias]) }}">
                                                                <img src="{{ $image_url }}" alt="" title=""
                                                                     class="img-fluid w-100">
                                                            </a>
                                                        </figure>
                                                    </div>
                                                    <div class="col-lg-9 col-sm-9 ">
                                                        <p class="fgreen text-right mb-0">
                                                            @if ($item->attributes->delivery_date!='' || $item->attributes->delivery_time )
                                                                Delivery by
                                                                {{$item->attributes->delivery_date}}
                                                                {{$item->attributes->delivery_time}}
                                                            @endif
                                                            {{-- {{ (!empty($item->attributes->delivery_date))? $item->attributes->delivery_date : '--' }}--}}
                                                            {{-- {{ (isset($item->attributes->delivery_time))?$item->attributes->delivery_time:'--' }}--}}


                                                            {{--2th Jan 2019--}}
                                                        </p>
                                                        <div class="row">
                                                            <div class="col-md-12">

                                                                <?php
                                                                //                                                                dump($item->attributes);
                                                                //                                                                dump(ProductCategoryShipped($item->id));
                                                                ?>

                                                                <article class="pb-1 mb-1 border-bottom">
                                                                    <h5>
                                                                        <a href="{{ route('productPage', ['category'=>$product->getCategory->category_alias,'product'=>$product->p_alias]) }}">
                                                                            {{ $item->name }}
                                                                        </a>
                                                                    </h5>
                                                                    <p>
                                                                        <strong>GIft Message:</strong>
                                                                        <span class="fblue">{{$item->attributes->message}}</span>
                                                                        {{--{!! ($item->attributes->has('overview') ? str_limit($item->attributes->overview, 150) : '') !!}--}}
                                                                    </p>
                                                                    <p>
                                                                        <strong>Options:</strong>
                                                                        <span class="fblue">
                                                                           <?php
                                                                            //                                                                            dump(sortProductOptions($item->attributes->options))
                                                                            ?>
                                                                            {{ sortProductOptions($item->attributes->options) }}
                                                                        </span>
                                                                    </p>


                                                                </article>

                                                            </div>

                                                        </div>


                                                        <div class="row">
                                                            <div class="col-md-4 col-4 ">
                                                                <span class="price">
                                                                   <small> Price</small>
                                                                    <span class="d-block w-100 cur">
                                                                        {!! currencySymbol($item->attributes->currency) !!}  {{ $item->price  }}
                                                                    </span>
                                                                </span>
                                                            </div>
                                                            <div class="col-md-4 col-4">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <small class="w-100">Qty</small>
                                                                        <?php
                                                                        $qtys = range(1, 10)
                                                                        ?>
                                                                        <select name=""
                                                                                class="ml-0 itemQty_{{ $item->id }}"
                                                                                onchange="updatecarts({{ $item->id }})">
                                                                            @foreach($qtys AS $qtyes)
                                                                                <option value="{{$qtyes}}" {{ ($qtyes==$item->quantity)?'selected':'' }}>{{$qtyes}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-md-3 d-none">
                                                                <span class="price">
                                                                 <small>Shipped To </small>
                                                                    <span class="d-block  ">

                                                                        <?php
                                                                        $countryShipped = productCategoryShipped($item->id);
                                                                        ?>

                                                                        <select name="category_shipped"
                                                                                id="category_shipped"
                                                                                onchange="updatecarts({{ $item->id }})"
                                                                                class="form-control countryShipped_{{ $item->id }}">

                                                                            @foreach($countryShipped as $ks=>$s)
                                                                                <option {{ ($item->attributes->countryShipped == $ks)?'selected':'' }}  value="{{ $ks }}">
                                                                                    {{ $s }}
                                                                                </option>
                                                                            @endforeach
                                                                        </select>

                                                                    </span>
                                                                </span>
                                                            </div>

{{--                                                            <div class="col-md-3 ">--}}
{{--                                                                <span class="price">--}}
{{--                                                                 <small>Delivery Charge </small>--}}
{{--                                                                    <span class="d-block w-100 cur">--}}
{{--                                                                        {!! currencySymbol($item->attributes->currency) !!}  {{ ($item->attributes->delivery_charge) ? ( $item->attributes->delivery_charge ) : '0' }}--}}

{{--                                                                    </span>--}}
{{--                                                                </span>--}}
{{--                                                            </div>--}}
                                                            <div class="col-md-4 col-4">
                                                                <span class="price">
                                                                    <small>Total Price</small>
                                                                    <span class="d-block w-100 cur">
                                                                        {!! currencySymbol($item->attributes->currency) !!} {{  $item->getPriceSumWithConditions()  }}
                                                                    </span>
                                                                </span>
                                                            </div>

                                                        </div>
                                                        <ul class="addstosmallcol nav">
                                                            <li>
                                                                <a href="javascript:void(0)" class="removeCartItem"
                                                                   data-id="{{ $item->id }}">
                                                                    <span class="icon-bin icomoon"></span> Remove
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- small column -->
                                    @endforeach
                                @else
                                    <div class="col-lg-12">
                                        <img src="/frontend/images/noproducts.jpg" alt=""
                                             title="" class="img-fluid"/>
                                    </div>
                                @endif
                            </div>
                            <!--/ row -->
                        </div>
                        <!--/left col -->
                        <!-- right cart -->


                        <div class="col-lg-3 col-sm-4">
                            <div class="rtcart">
                                @include('frontend._partials.checkout_right')

                                <?php





                                $validationResults = cartItemsValidationForCheckOut(Cart::getContent());



                                ?>


                                @if(Cart::getContent()->count()>0)

                                    @if($validationResults=='true')

                                        <a href="{{route('cartAdressPage')}}" class="greenlink w-100 my-2">Checkout</a>
                                    @else
                                        <a href="#" class="btn btn-light w-100 my-2">Checkout</a>
                                    @endif



                                @endif

                                <a href="{{route('home')}}" class="whitebtn text-center w-100">Continue Shopping</a>

                                <br/>
                                <br/>

                                <h6>
                                    <a href="" data-toggle="modal" data-target="#shippingChargesModel">
                                        Shipping Charges Information
                                    </a>
                                </h6>


                                <!-- Modal -->
                                <div class="modal fade" id="shippingChargesModel" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">
                                                    Shipping Charges ({{ session('siteCountryinfo')->country_name }})

                                                </h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">


                                                <?php


                                                $paymentAmount = caliclateShippingAmount($totalweight, session('siteCountryinfo')->country_id);

                                                //                                $paymentAmount = $paymentAmount + Cart::getTotal();
                                                //                                $paymentAmount = $paymentAmount + Cart::getTotal();



                                                $shippingAmountList = shippingPriceList(session('siteCountryinfo')->country_id);

                                                ?>




                                                @if(is_array($shippingAmountList) && count($shippingAmountList)>0)



                                                    <table class="table table-bordered table-striped">
                                                        @foreach($shippingAmountList AS $wight=>$shippingAmount)
                                                            <?php
                                                            $listPrice = curencyConvert(getCurency(), $shippingAmount);
                                                            ?>

                                                            <tr>
                                                                <td>
                                                                    <?php

                                                                        $itemweightData=  productWightFarmart($wight);

//                                                                    dd( productWightFarmart($wight));
                                                                    ?>
                                                                    {{   $itemweightData['weight'] }}
                                                                    {{   $itemweightData['type'] }}
{{--                                                                    {{ $wight }}Kgs--}}
                                                                </td>
                                                                <td>
                                                                    {!! currencySymbol(getCurency()) !!}{{  $listPrice }}

                                                                    @if( $totalweight == $wight)
                                                                        *
                                                                    @endif


                                                                </td>
                                                            </tr>
                                                        @endforeach


                                                    </table>

                                                @else

                                                    <p>
                                                        Free Shipping
                                                    </p>

                                                @endif

                                            </div>

                                        </div>
                                    </div>
                                </div>


                                <p class="py-3">The price and availability of items at Vdesiconnect are subject to
                                    change. The shopping cart is a temporary place to store a list of your items and
                                    reflects each item's most recent price.</p>
                            </div>
                        </div>
                        <!--/ right cart -->
                    </div>


                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main -->



@endsection
@section('footerScripts')
    <script>
        function updatecarts(rowid) {
            var id = rowid;
            var qty = $('.itemQty_' + id).val();
            var countryShipped = $('.countryShipped_' + id).val();

            if (id != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{ route('updateItemFromCart') }}',
                    type: 'POST',
                    data: {'id': id, 'qty': qty, 'countryShipped': countryShipped},
                    // data: 'id=' + id + '&qty=' + qty,
                    success: function (response) {
                        location.reload();
                    }
                });
            }
        }

        $('.removeCartItem').on('click', function () {
            var id = $(this).data('id');

            if (id != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{ route('removeItemFromCart') }}',
                    type: 'POST',
                    data: 'id=' + id,
                    success: function (response) {
                        location.reload();
                    }
                });
            }
        });
    </script>
@endsection