@extends('frontend.layout')
@section('title', $title)
@section('meta_keywords', $meta_keywords)
@section('meta_description', $meta_description)
@section('headerStyles')
@endsection
@section('content')



    <main>
    @if(count($banners)>0)
        <!-- home page carousel -->
            <section class="homecarousel">
                <!-- main slider -->
                <div id="carouselExampleIndicators" class="carousel slide homeslider" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @foreach($banners as $banner)
                            <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->iteration - 1 }}"
                                class="{{ $loop->iteration == 1 ?'active':'' }}"></li>
                        @endforeach
                    </ol>

                    <div class="carousel-inner" role="listbox">
                    @foreach($banners as $banner)
                        <!-- Slide One - Set the background image for this slide in the line below -->
                            <div class="carousel-item {{ $loop->iteration==1?'active':'' }}"
                                 style="background-image: url('/uploads/banners/{{ $banner->banner_bg_image }}')">
                                <!-- row -->
                                <div class="row">
                                    <!-- col -->
                                    <div class="col-lg-6 col-sm-6 col-md-6 align-self-center car-image">
                                        <a style="cursor: pointer" href="{{(isset($banner->banner_link))?$banner->banner_link:'javascript:void(0)'}}">
                                            <img src="/uploads/banners/{{ $banner->banner_image }}" alt="{{ $banner->banner_title }}" title="" class="img-fluid"></a>
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-lg-4 col-sm-6 col-md-6 align-self-center">
                                        <div class="carousel-caption  p-3">
                                            <h1 class="h1 fbold">{{ $banner->banner_title }}</h1>
                                            <p class="fwhite">{!! $banner->banner_description !!} </p>
                                            <h5 class="mb-3"><a href="{{(isset($banner->banner_link))?$banner->banner_link:'javascript:void(0)'}}">Read More</a></h5>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                </div>
                                <!--/ row -->
                            </div>
                        @endforeach

                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="icon-left-arrow-key icomoon"></span> <span class="sr-only">Previous</span> </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="icon-keyboard-right-arrow-button icomoon"></span> <span class="sr-only">Next</span>
                    </a>
                </div>
                <!--/ main slider -->
            </section>
            <!--/ home page carousel -->
    @endif

    <!-- main carousel bottom -->
        <section class="carbottom">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-3 col-sm-6 order-sm-1 order-md-1 order-lg-1">
                        <figure class="figsliderbot ">
                            <a href="{{route('categoriesPage',['category'=>'chocolates', 'local'=>getCountryID('country_name')])}}">
                                <img src="/frontend/images/data/chocklatessliderbottom.jpg" alt="Celebrate Occassion With Chocolates"
                                        class="img-fluid w-100"></a>
                            <article class="chock text-center">
                                <p>Celebrate Occassion With Gift </p>
                                <h2>Chocolates</h2>
                            </article>
                        </figure>
                    </div>
                @if(count($serviceBanners)>0)
                    <!-- col lg 6-->
                        <div class="col-lg-6 pr-0 pl-0 col-12 servicecar order-sm-3 order-md-3 order-lg-2 ">
                            <!-- services carousel -->
                            <div id="carouselservices" class="carousel slide homeservicescar" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    @foreach($serviceBanners as $serviceBanner)
                                        <li data-target="#carouselservices" data-slide-to="{{ $loop->iteration - 1 }}"
                                            class="{{ $loop->iteration == 1 ?'active':'' }}"></li>
                                    @endforeach
                                </ol>
                                <div class="carousel-inner">
                                    @foreach($serviceBanners as $serviceBanner)

                                        <?php
                                        $titleinfo = explode(',', $serviceBanner->banner_title)
                                        ?>
                                        <div class="carousel-item {{ $loop->iteration == 1 ?'active':'' }}">
                                            <a href="{{ $serviceBanner->banner_link }}">
                                                <img class="d-block w-100 img-fluid"
                                                     src="/uploads/service_banners/{{ $serviceBanner->banner_image }}"
                                                     alt="{{ ($titleinfo[0]) ? $titleinfo[0] : ''  }}" title=""/>
                                            </a>
                                            <div class="carousel-caption">
                                                <h3>
                                                    @if (count($titleinfo)> 1)

                                                        <span>
                                                        {{ ($titleinfo[0]) ? $titleinfo[0] : ''  }}
                                                    </span>
                                                        {{ ($titleinfo[1]) ? $titleinfo[1] : ''  }}

                                                    @else
                                                        <span>
                                                        {{ ($titleinfo[0]) ? $titleinfo[0] : ''  }}
                                                    </span>

                                                    @endif


                                                </h3>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#carouselservices" role="button"
                                   data-slide="prev">
                                    <span class="icon-left-arrow-key"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselservices" role="button"
                                   data-slide="next">
                                    <span class="icon-keyboard-right-arrow-button"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            <!--/ services carousel -->
                        </div>

                @endif

                <!--/ col lg 6-->
                    <div class="col-lg-3 col-sm-6 sliderrt order-sm-2 order-md-2 order-lg-3">
                        <figure class="figsliderbot ">
                            <a href="{{route('categoriesPage',['category'=>'flowers', 'local'=>getCountryID('country_name')])}}"><img
                                        src="/frontend/images/data/boutiquesliderbottom.jpg" alt="Flowers"
                                        title="" class="img-fluid"></a>
                            <article class="bou text-center">
                                <p>Visit and gift your Dear </p>
                                <h2>Flowers</h2>
                            </article>
                        </figure>
                    </div>
                </div>
                <!--/row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ main carousel bottom-->


        <!-- popular categories -->
        <section class="popularcat">
            <div class="container">
                <!-- title row -->
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <article class="hometitle">
                            <h3 class="px20 py20">Popular Categories</h3>
                        </article>
                    </div>
                </div>

                <?php
                //                dump($categories_products);
                ?>


                @if (count($categories_products) > 0)

                    @foreach($categories_products AS $categorieData)


                        <?php
                        $countryShippedList = category_shipped($categorieData->category_shipped);
                        ?>
                        @if (isset($countryShippedList[getCountry()]))

                                  @if (count($categorieData->getProducts) > 0)

                            <?php
                            if (count($categorieData->getProducts) > 0) {

                                $catData = array();

                                foreach ($categorieData->getProducts AS $cat_items) {
                                    if (count($cat_items->productSKUs) > 0) {
                                        $catData[] = $cat_items;
                                    }


                                }
                            }
                            ?>


                            @if (!empty($catData))
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="titlecat">
                                            <h4 class="float-left pl-4">
                                                {{ $categorieData->category_name }}
                                            </h4>
                                            <a href="{{route('categoriesPage', ['category'=>$categorieData->category_alias, 'local'=>getCountryID('country_name')])}}" class="float-right my-1 mr-2">View All</a>
                                        </div>
                                    </div>
                                </div>
                                <!--/ caterory title row -->
                                <!-- products row -->
                                <div class="row pb-4">

                                    @if (count($categorieData->getProducts)> 0)
                                        @foreach($categorieData->getProducts AS $product)
                                            <?php
                                            $priceSKU = '';

                                            $image_url = '';

                                            if (count($product->productImages) > 0) {
                                                $imageID = array_first($product->productImages)->pi_id;
                                                $image_url = getImagesById($imageID);
                                                if ($image_url == '') {
                                                    $image_url = '/frontend/images/noproduct-available.jpg';
                                                }
                                            } else {
                                                $image_url = '/frontend/images/noproduct-available.jpg';
                                            }
                                            if (count($product->productSKUs) > 0) {

                                                $priceSKU = $product->productSKUs[0];
                                            }

                                            //                                        dump($priceSKU);

                                            ?>


                                            @if (!empty($priceSKU))


                                                <div class="col-lg-3 text-center col-6">
                                                    <div class="productitem">
                                                        <figure>
                                                            <a href="{{ route('productPage', ['category'=>$categorieData->category_alias,'product'=>$product->p_alias]) }}">
                                                                <img src="{{ $image_url  }}" alt="{{$product->p_name}}"  title="{{$product->p_name}}" class="img-fluid">
                                                            </a>
                                                            <div class="hover">
                                                                <ul class="nav">
                                                                    {{--<li><a href="javascript:void(0)" data-toggle="tooltip"--}}
                                                                    {{--data-placement="bottom"--}}
                                                                    {{--title="Add to Wishlist"><span--}}
                                                                    {{--class="icon-heartuser icomoon"></span></a>--}}
                                                                    {{--</li>--}}

                                                                    <li>

                                                                        <a href="javascript:void(0)"
                                                                           data-toggle="tooltip"
                                                                           data-id="{{ $product->p_id }}"
                                                                           data-userid="{{ Auth::id() }}"
                                                                           data-placement="bottom"
                                                                           title="Add to Wishlist"
                                                                           class="productaddwishlist{{ $product->p_id }} addToWishList {{in_array($product->p_id,$wishlistProducts) ? 'likeactive':''}} ">
                                                                            <span class="icon-heartuser icomoon"></span>

                                                                        </a>
                                                                    </li>


                                                                    <li>
                                                                        <a href="{{ route('productPage', ['category'=>$categorieData->category_alias,'product'=>$product->p_alias]) }}"
                                                                           data-toggle="tooltip"
                                                                           data-placement="bottom"
                                                                           title="View More"><span
                                                                                    class="icon-external-link icomoon"></span></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </figure>
                                                        <article>
                                                            <a class="proname"
                                                               href="{{ route('productPage', ['category'=>$categorieData->category_alias,'product'=>$product->p_alias]) }}">

                                                                {{ substr($product->p_name,0,20).'. . .' }}

                                                            </a>
                                                            <p>

                                                                {!! currencySymbol(getCurency()) !!}   {{ curencyConvert(getCurency(), $priceSKU->sku_store_price)  }}

                                                                @if ($priceSKU->sku_vdc_final_price != 0)
                                                                    <small class="oldprice">
                                                                        {!! currencySymbol(getCurency()) !!}  {{ curencyConvert(getCurency(), $priceSKU->sku_vdc_final_price)  }}
                                                                    </small>
                                                                @endif

                                                            </p>
                                                            {{--<a href="javascript:void(0)" class="btnlist">Add to Cart </a>--}}
                                                        </article>
                                                    </div>
                                                </div>
                                            @endif

                                        @endforeach
                                    @endif


                                </div>
                            @endif

                                @endif
                        @endif
                    @endforeach


                @endif


            </div>
        </section>
        <!--/ popular categories -->

        <!-- single product banner -->
        <section class="singleproductbanner" style="background-image: url('/frontend/images/singlebanner.jpg')">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <article>
                            <p>Celebrates this festival SEASON with</p>
                            <h3>Chocolates</h3>
                            <a href="{{route('categoriesPage',['category'=>'chocolates', 'local'=>getCountryID('country_name')])}}">View All</a>
                        </article>
                    </div>
                </div>
            </div>
        </section>
        <!--/ single product banner -->

    <?php //include 'homesaleproducts.php' ?>


    @include('frontend.modules.homesaleproducts')

    <!--/ sale products tab-->
        <!-- our services -->
        <section class="homeservices">
            <div class="container">
                <!-- title row -->
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <article class="hometitle">
                            <h3 class="px20 py20">Our Services</h3>
                        </article>
                        <p style="margin-top:-25px;">We gratify ourselves for taking pride in offering divergent professional services that results in reliable customer support.</p>
                    </div>
                </div>
                <!--/ title row -->
                <!-- row -->
                <div class="row py-4 mpb-0 tpb-0">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-6 pr-0 dmnone dtnone">
                                <img src="/frontend/images/homemedicalimg.jpg" alt="" title=""
                                     class="img-fluid thumbimg">
                            </div>
                            <div class="col-lg-6 pl-0 mpx-15 tpl15">
                                <div class="linkcol med">
                                    <img src="/frontend/images/medimg.png" alt="Medical Services" title="Medical Services" class="linkcolthumb">
                                    <article>
                                        <h5><a href="{{route('servicePage', ['service'=>'medical'])}}">Medical Services</a></h5>
                                        <p>Making sure of enhanced quality and hygienic medical services, VdesiConnect is comprehensively involved in delivering world class medical facilities. </p>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- col -->
                    <div class="col-lg-3">
                        <div class="linkcol property">
                            <img src="/frontend/images/propertythumb.png" alt="Property  Management" title="Property  Management" class="linkcolthumb">
                            <article>
                                <h5><a href="{{route('servicePage', ['service'=>'property-management'])}}">Property  Management</a></h5>
                                <p>VdesiConnect endows in bringing impactful property management services to the reach of our clientele.</p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3">
                        <div class="linkcol visa">
                            <img src="/frontend/images/visathumb.png" alt="Visa Service" title="Visa Service" class="linkcolthumb">
                            <article>
                                <h5><a href="{{route('servicePage', ['service'=>'visa-support-services'])}}">Visa
                                        Service</a></h5>
                                <p>Provide visa support services for people of multiple nationalities who are in the aspiration to travel to any nation across the world. </p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row pb-5">
                    <!-- col -->
                    <div class="col-lg-3">
                        <div class="linkcol radio">
                            <img src="/frontend/images/onlineradiothumb.png" alt="Online Radio" title="Online Radio" class="linkcolthumb">
                            <article>
                                <h5><a href="{{route('servicePage', ['service'=>'online-radio'])}}">Online Radio</a>
                                </h5>
                                <p>Online Radio Will Coming Soon, Get Ready to Listen Online Radio</p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-6 pr-0 dmnone dtnone">
                                <img src="/frontend/images/onlinetutorimg.png" alt="Online Tutor" title="Online Tutor"  class="img-fluid thumbimg">
                            </div>
                            <div class="col-lg-6 pl-0 mpx-15 tpl15">
                                <div class="linkcol tutor">
                                    <img src="/frontend/images/onlinetutorthumb.png" alt="Online Tutor" title="Online Tutor" class="linkcolthumb">
                                    <article>
                                        <h5><a href="{{route('servicePage', ['service'=>'online-tutor'])}}">Online Tutor</a></h5>
                                        <p>VdesiConnect alleviates the life of every individual by supporting them with the associated online tutors. </p>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3">
                        <div class="linkcol insurance">
                            <img src="/frontend/images/visitorsthumb.png" alt="Visitors Insurance" title="Visitors Insurance" class="linkcolthumb">
                            <article>
                                <h5><a href="{{route('servicePage', ['service'=>'visitors-insurance'])}}">Visitors Insurance</a></h5>
                                <p>We are always in the exertion to develop all-inclusive insurance services tailored in the direction to adapt with the client requirements. </p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
        </section>
        <!--/ our services -->

        <!--terms -->
        <section class="hometerms py-4">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-4 text-center termscol col-sm-4">
                        <img src="/frontend/images/hometerms01.png" alt="100 % SECURE PAYMENTS" title="100 % SECURE PAYMENTS">
                        <h5>100 % SECURE PAYMENTS</h5>
                        <p>Moving your card details to a much more secure places</p>
                    </div>
                    <div class="col-lg-4 col-sm-4 text-center termscol">
                        <img src="/frontend/images/hometerms02.png" alt="Trust Pay" title="Trust Pay">
                        <h5>Trust Pay</h5>
                        <p>100% Payment Protection. Easy Return policy</p>
                    </div>
                    <div class="col-lg-4 col-sm-4 text-center termscol">
                        <img src="/frontend/images/hometerms03.png" alt="24/7 HELP CENTER" title="24/7 HELP CENTER">
                        <h5>24/7 HELP CENTER</h5>
                        <p>Got a question? Look no further. Browse our FAQs or submit your query here.</p>
                    </div>
                </div>
                <!--/ row -->
            </div>
        </section>
        <!--/ terms -->
    </main>

    <div class="modal" id="addtowlist">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Success</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    Product Added to Your Wish List Successfully
                </div>

            </div>
        </div>
    </div>
    <div class="modal" id="removefromwlist">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Success</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    Product deleted from Your Wish List Successfully
                </div>

            </div>
        </div>
    </div>

@endsection
@section('footerScripts')
@endsection