<div class="apruvels_box" style="">


    <div class="card">


        @if($product_info->p_availability=='in_stock')

            <div class="card-body p-2">
                <div class="form-group">
                    {{--<label>Gift Message will show on your gift </label>--}}
                    <textarea class="form-control message"
                              placeholder="Write Custom Message On the gift to show on tag"></textarea>
                </div>



                <div class="form-group mb-0 ">
                    {{--<label>Delivery Date & Time</label>--}}
                    <div class="row">
                        <div class="form-group col-sm-6 col-lg-4 col-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <label>Your Name On Gift</label>
                                    <div class="input-group">


                                        <select class="form-control nameongift"
                                                name="nameongift">

                                            <option>yes</option>
                                            <option>No</option>


                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group col-sm-6 col-lg-4 col-6">
                            <label> Select Date </label>
                            <div class="input-group">
                                <input type="text"
                                       placeholder="Select Delivery Date"
                                       class="form-control w-50 float-left deliverydate datepicker_hidepastdates">
                            </div>
                        </div>

                        <div class="form-group col-sm-6 col-lg-4 col-6">
                            <label>select time</label>
                            <div class="input-group">

                                <select class="form-control deliverytime"
                                        name="s_time">
                                    @if(count(preferTimeings())> 0)
                                        <option>--</option>
                                        @foreach(preferTimeings() AS $preferTimeing)
                                            <option>{{$preferTimeing}}</option>
                                        @endforeach
                                    @endif

                                </select>
                            </div>


                        </div>
                    </div>

                </div>
            </div>

        @endif

    </div>


    <!-- button -->

    @if ($storePrice > 0)

        <div class="buttonsgroup pb-2">

            @if($product_info->p_availability=='in_stock')
                <button data-id="{{ $product_info->p_id }}"
                        class="addToCartInProduct float-left">
                    <span class="icon-online-shopping-cart icomoon"></span>
                    Add to cart
                </button>

                <form class="buyProductForm float-left mx-1"
                      action="{{ route('buyProduct') }}"
                      id="" method="GET">

                    <input type="hidden" name="id" value="{{ $product_info->p_id }}"/>
                    <input type="hidden" name="qty" class="buyproductqty"/>
                    <input type="hidden" name="message" class="buyproductmessage"/>
                    <input type="hidden" name="price" class="buyproduct_productPrice"/>
                    <input type="hidden" name="options" class="buyproduct_options"/>
                    <input type="hidden" name="delivery_date" class="buyproductdelivery_date"/>
                    <input type="hidden" name="delivery_time" class="buyproductdelivery_time"/>
                    <input type="hidden" name="delivery_charge" class="buyproductdelivery_charge"/>
                    <input type="hidden" name="countryShipped" class="countryShipped"/>
                    <input type="hidden" name="nameongift" class="buy_nameongift"/>

                    <a href="#" class="buyproduct greenlink">Gift Now</a>

                </form>
            @else

                {{--<button data-id="{{ $product_info->p_id }}"--}}
                {{--class="float-left"><span--}}
                {{--class="icon-online-shopping-cart icomoon"></span>Out of--}}
                {{--Stock--}}
                {{--</button>--}}

            @endif
            <button data-id="{{ $product_info->p_id }}"
                    data-userid="{{ Auth::id() }}"
                    class="productaddwishlist{{ $product_info->p_id }} addToWishList @if(in_array($product_info->p_id,$wishlistProducts))likeactive @endif">
                <span class="icon-heartuser icomoon"></span>

                {{--{{  in_array($product_info->p_id,$wishlistProducts) ?'Remove to wishlist':'Add to wishlist' }}--}}


            </button>

        </div>

@endif


