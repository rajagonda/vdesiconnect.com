@extends('frontend.layout')
@section('title', $title)
@section('meta_keywords', $meta_keywords)
@section('meta_description', $meta_description)
@section('headerStyles')
@endsection
@section('content')


    @if ($product_info == '')

        <main>
            <!-- sub apge -->
            <section class="subpage pt-5">

                <div class="text-center">

                    <h4 class='text-center'>Product Not Avalable Currently.</h4>

                    <a href="{{ route('home') }}">
                        Go to Home
                    </a>


                </div>


            </section>
        </main>



    @else



        <?php

        $apruvalCats = array(
            'fashion-jewellery',
            'silver-items',
            'boutique',
        );


        //        dd($product_info);

        if (count($product_info->productSKUs) > 0) {

            $sortingSkus = product_skus('sort', $product_info->productSKUs);

            $priceData = getProduct_Price_calc($sortingSkus, $getparams);

            $vdcPrice = $priceData['vdcPrice'];
            $storePrice = $priceData['storePrice'];
            //    dd($sortingSkus);


        }

        $validateCountry = productOrderValiidation($product_info);


        //            dd($sortingSkus);



        //            dd('');



        ?>

        <main>
            <!-- sub apge -->
            <section class="subpage">
                <!-- sub page header -->
                <section class="subpageheader">
                    <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                    <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8 text-center">
                                <article class="pagetitle">
                                    <h1 class="px20 py20">
                                        {{$product_info->getCategory->category_name}}

                                    </h1>
                                </article>
                                <!-- brudcrumb -->
                                <ul class="brcrumb">
                                    <li>
                                        <a href="{{route('home')}}">Home</a>
                                    </li>
                                    @if(!empty($product_info->getCategory))
                                        <li>
                                            <a href="{{route('categoriesPage',['category'=>$product_info->getCategory->category_alias, 'local'=>getCountryID('country_name')])}}">
                                                {{$product_info->getCategory->category_name}}
                                            </a>
                                        </li>
                                    @endif
                                    @if(!empty($product_info->getSubCategory))
                                        <li>
                                            <a href="{{route('categoriesPage',['category'=>$product_info->getCategory->category_alias,'type'=>$product_info->getSubCategory->category_alias, 'local'=>getCountryID('country_name')])}}">
                                                {{$product_info->getSubCategory->category_name}}
                                            </a>
                                        </li>
                                    @endif
                                    <li>
                                        <a>{{$product_info->p_name}} </a>
                                    </li>
                                </ul>
                                <!--/ brudcrumb -->
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ sub page header -->
                <!--sub page main -->
                <section class="subpagemain">
                    <!-- container -->
                    <div class="container">
                        @if (Session::has('flash_message'))
                            <br/>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>{{ Session::get('flash_message' ) }}</strong>
                            </div>
                        @endif

                        @if(catagoriesNotes($product_info->getCategory->category_alias))

                            <div class="alert alert-info">
                                <strong>Note:</strong> {{ catagoriesNotes($product_info->getCategory->category_alias) }}
                                .
                            </div>
                        @endif
                    <!-- page detail top block -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-sm-6">

                                <?php
                                //                                dump($product_info->productImages);
                                //                            dump($product_info->getCategory);
                                ?>

                                <section class="simplegallery gallery">
                                    @if($product_info->p_availability=='out_of_stock')
                                        <span class="outofstock"><img src="/frontend/images/sold-outimg.png"
                                                                      alt="Our of Stock" title=""></span>
                                    @endif
                                    <div class="content">
                                        @if(sizeof($product_info->productImages)>0)
                                            @foreach($product_info->productImages as $productImages)
                                                <img src="{{url('/uploads/products/'.$productImages->pi_image_name)}}"
                                                     @if($loop->first==1) @else style="display:none"
                                                     @endif  class="image_{{$productImages->pi_id}}"
                                                     alt="{{$product_info->p_name}}"/>
                                            @endforeach
                                        @else
                                            <img src="{{url('/frontend/images/noproduct-available.jpg')}}"
                                                 class="image_1"
                                                 alt="No Product"/>
                                        @endif

                                    </div>

                                    <div class="thumbnail">
                                        @if(sizeof($product_info->productImages)>0)
                                            @foreach($product_info->productImages as $productImages)
                                                <div class="thumb">
                                                    <a href="#" rel="{{$productImages->pi_id}}">
                                                        <img src="{{url('/uploads/products/'.$productImages->pi_image_name)}}"
                                                             id="thumb_{{$productImages->pi_id}}"
                                                             alt="{{$product_info->p_name}}"/>
                                                    </a>
                                                </div>
                                            @endforeach
                                        @else

                                        @endif

                                    </div>
                                </section>
                            </div>
                            <!--/ col -->
                            <!-- col 6-->
                            <div class="col-sm-6">
                                <div class="productbasic-info">
                                    <h2>{{$product_info->p_name}} </h2>
                                    <div class="priceproduct pt-3">
                                        <h3>

                                            <?php
                                            //                                        dump($sortingSkus);
                                            //                                        dump($priceData);
                                            ?>
                                            <span class="mainprice">
                                             {!! currencySymbol(getCurency()) !!}  {{ curencyConvert(getCurency(), $storePrice) }}

                                            </span>
                                            <input type="hidden" name="productPrice" class="productPrice"
                                                   value="{{ $storePrice }}">

                                            @if ($vdcPrice != '')
                                                <span class="oldprice">
                                            {!! currencySymbol(getCurency()) !!}  {{ curencyConvert(getCurency(), $vdcPrice) }}

                                        </span>

                                                {{-- <span class="percentage">

                                                     @if ($priceSKU->sku_store_discount_type == 'percentage')
                                                         {{ number_format($priceSKU->sku_store_discount_value, 0) }}% Off
                                                     @else
                                                         {{ currencySymbol('USD') }} {{ curencyConvert('USD', $priceSKU->sku_store_discount_value) }}
                                                         Off
                                                     @endif
                                             </span>--}}
                                            @endif
                                        </h3>
                                    </div>
                                    <table class="detailtable">

                                        <?php
                                        //                                        dump($sortingSkus);
                                        ?>

                                        @if (count($sortingSkus) > 0)
                                            @foreach($sortingSkus AS $sortingName => $sortingSku)

                                                <tr>
                                                    <td>{{ $sortingName  }}</td>
                                                    <td>:</td>
                                                    <td>

                                                        <?php
                                                        //                                                    dump($product_info->productSKUs);
                                                        ?>

                                                        <?php
                                                        //                                                                                                            dd($sortingSku[255 ]->sku_option)

                                                        //                                                        $weightinfo = productWightFarmart($sortingSku[255]->sku_option);
                                                        //                                                                           dd($skusData->sku_option);
                                                        //                                                        dd($weightinfo);


                                                        ?>

                                                        <select name="{{$sortingName}}" class="select_sku">

                                                            @if(count($sortingSku) > 0)
                                                                @foreach($sortingSku AS $skusData)

                                                                    <?php
                                                                    $skuitemPrice = curencyConvert(getCurency(), $skusData->sku_store_price);
                                                                    ?>



                                                                    <option data-option="{{ $skusData->sku_option }}"
                                                                            {{ isset($_GET[$sortingName]) && $_GET[$sortingName] == $skusData->sku_id  ?'selected':''}}  value="{{ $skusData->sku_id }}">

                                                                        {{--                                                                    @if ( ($skusData->sku_store_price != '' && $skusData->sku_store_price != 0 ) )--}}

                                                                        {{  $skusData->sku_option }}
                                                                        @if($sortingName=='weight')

                                                                            <?php
                                                                            $weightinfo = productWightFarmart($skusData->sku_option);

                                                                            ?>


                                                                            @if(is_array($weightinfo))
{{--                                                                                {{$weightinfo['weight'] }}--}}
                                                                                {{$weightinfo['type'] }}
                                                                            @else
                                                                                {{productWightFarmart( $skusData->sku_option) }}

                                                                            @endif

                                                                        @endif


                                                                        @if($skuitemPrice != 0)
                                                                            -
                                                                            {!! currencySymbol(getCurency()) !!}

                                                                            {{ $skuitemPrice }}
                                                                        @endif



                                                                        {{--                                                                    @endif--}}




                                                                        {{--==>{{$skusData->sku_store_price}}--}}
                                                                    </option>



                                                                @endforeach

                                                            @endif


                                                        </select>

                                                    </td>
                                                </tr>

                                            @endforeach
                                        @endif


                                        {{--<tr>--}}
                                        {{--<td>Quantity</td>--}}
                                        {{--<td>:</td>--}}
                                        {{--<td>--}}
                                        {{--<input type="hidden" name="qty" value="1" class="productqty"/>--}}
                                        {{--1--}}
                                        {{--<select style="width:50px;">--}}
                                        {{--<option>1</option>--}}
                                        {{--<option>2</option>--}}
                                        {{--<option>3</option>--}}
                                        {{--<option>4</option>--}}
                                        {{--</select>--}}
                                        {{--</td>--}}
                                        {{--</tr>--}}


                                        <tr style="display: none;">
                                            <td>Delivery Charges</td>
                                            <td>:</td>
                                            <td>

                                                @if ($product_info->p_delivery_charges == 0)
                                                    Free Delivery
                                                    <?php
                                                    $product_delivery_charge = 0;
                                                    ?>
                                                @else
                                                    {!! currencySymbol('USD')  !!} {{ curencyConvert('USD', $product_info->p_delivery_charges) }}
                                                    <?php
                                                    $product_delivery_charge = 0;
                                                    ?>
                                                @endif
                                                <input type="hidden" class="delivery_charge"
                                                       value="{{ $product_info->p_delivery_charges }}"/>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Product Type</td>
                                            <td>:</td>
                                            <td> {{ (isset($product_info->getSubCategory->category_name))? $product_info->getSubCategory->category_name:'' }}</td>
                                        </tr>

                                        @if ((isset($product_info->getSplCat->category_name)))
                                            <tr>
                                                <td>
                                                    {{ catSubSortNames($product_info->getCategory->category_id) }}
                                                </td>
                                                <td>:</td>
                                                <td> {{ $product_info->getSplCat->category_name }}</td>
                                            </tr>
                                        @endif


                                        @switch($product_info->p_cat_id)
                                            @case(1)

                                            <tr>
                                                <td>Dishtype</td>
                                                <td>:</td>
                                                <td>
                                            <span class="fgreen">

                                                 {{$product_info->p_dishtype}}
                                            </span>
                                                </td>
                                            </tr>


                                            @break

                                        @endswitch


                                        @if(!in_array($product_info->getCategory->category_alias, $apruvalCats))
                                            <tr>
                                                <td>Availability</td>
                                                <td>:</td>
                                                <td>
                                                    @if($product_info->p_availability=='in_stock')
                                                        <span class="fgreen">
                                                    In Stock
                                                </span>
                                                    @else
                                                        <span class="" style="color: red">
                                                    Out of Stock
                                                </span>
                                                    @endif
                                                    {{--<span class="fgreen">--}}

                                                    {{--{{ ($product_info->p_availability=='in_stock') ? 'In Stock':'Out of Stock'}}--}}
                                                    {{--</span>--}}
                                                </td>
                                            </tr>

                                        @endif


                                        <tr>
                                            <td>Shipped In</td>
                                            <td>:</td>
                                            <td>

                                                {{  $validateCountry['info'] }}
                                                <input name="category_shipped" id="category_shipped"
                                                       class=" countryShipped" type="hidden"
                                                       value="{{ getCountry() }}"/>
                                            </td>
                                        </tr>

                                    </table>


                                    @if ($validateCountry['code'] == 'true')

                                        @if (in_array($product_info->getCategory->category_alias, $apruvalCats))

                                            @if((!isset($verify_otp_products)) || isset($verify_otp_products) && !in_array($product_info->p_id, $verify_otp_products))

                                                <div class="apruvels_button text-center" style=" ">
                                                    <!-- row -->
                                                    <div class="row">
                                                        <!-- col -->

                                                        <!-- col -->

                                                        <!-- col -->
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <div class="alert alert-info">
                                                                    <strong>Note!:</strong>
                                                                    To Get Information and Availability of this product,
                                                                    Send
                                                                    Enquiry and We will Get back To you Soon.. Click
                                                                    here
                                                                </div>
                                                                <div class="input-group">
                                                                    <button type="button" class="greenlink w-100"
                                                                            data-toggle="modal"
                                                                            data-target="#SendEnquiry">
                                                                        Send Enquiry
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="col-lg-6">
                                                            <div class="alert alert-info">
                                                                <strong>Note!:</strong>
                                                                if you have already Enquiry OTP about this product,
                                                                please
                                                                enter
                                                                OTP and Submit
                                                                {{--<a href="#" class="alert-link">read this message</a>.--}}
                                                            </div>


                                                            {{--                                                            {{ laravelReturnMessageShow() }}--}}

                                                            <form action="{{route('productSendEnquiryVerify')}}"
                                                                  method="post">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="se_product_id"
                                                                       value="{{ $product_info->p_id }}"/>
                                                                <input type="hidden" name="se_user_id"
                                                                       value="{{ \Illuminate\Support\Facades\Auth::id() }}"/>
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <input class="form-control"
                                                                               placeholder="Enter You Received OTP"
                                                                               type="text" required
                                                                               name="se_otp" value=""/>
                                                                        <button type="submit"
                                                                                class="greenlink rounded-0">Submit
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!--/ col -->
                                                    </div>
                                                    <!--/ row -->


                                                    <!-- The Modal -->
                                                    <div class="modal" id="SendEnquiry" data-backdrop="static"
                                                         data-keyboard="false">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <form class="" method="post"
                                                                      action="{{route('productSendEnquiry')}}">
                                                                    {{ csrf_field() }}

                                                                    <input type="hidden" name="se_product_id"
                                                                           value="{{ $product_info->p_id }}"/>
                                                                    <input type="hidden" name="se_user_id"
                                                                           value="{{ \Illuminate\Support\Facades\Auth::id() }}"/>

                                                                    <!-- Modal Header -->
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Send Enquiry</h4>
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal">
                                                                            &times;
                                                                        </button>
                                                                    </div>

                                                                    <!-- Modal body -->
                                                                    <div class="modal-body">
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-left">
                                                                                <div class="form-group">
                                                                                    <label>Name</label>
                                                                                    <div class="input-group">
                                                                                        <?php
                                                                                        $oldData = new stdClass();
                                                                                        $oldData->se_name = (Auth::check()) ? Auth::user()->name : '';
                                                                                        $oldData->se_email = (Auth::check()) ? Auth::user()->email : '';
                                                                                        $oldData->se_phone = (Auth::check()) ? Auth::user()->mobile : '';


                                                                                        ?>
                                                                                        <input type="text"
                                                                                               name="se_name" required
                                                                                               value="{{ formValidationValue('se_name', $oldData)  }}"
                                                                                               class="form-control"/>
                                                                                    </div>
                                                                                    {{ formValidationError($errors, 'se_name') }}
                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <label>Email</label>
                                                                                    <div class="input-group">
                                                                                        <input type="email"
                                                                                               name="se_email" required
                                                                                               value="{{ formValidationValue('se_email', $oldData)  }}"
                                                                                               class="form-control"/>
                                                                                    </div>
                                                                                    {{ formValidationError($errors, 'se_email') }}
                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <label>Phone</label>
                                                                                    <div class="input-group">
                                                                                        <?php
                                                                                        $sortmobile = explode('-', $oldData->se_phone);
                                                                                        ?>


                                                                                        <div class="input-group-prepend">
                                                                                    <span class="input-group-text">
                                                                                        <select name="se_mobile_prefix"
                                                                                                required
                                                                                                class="">
                                                                                            <option value="">Select</option>
                                                                                            @if(sizeof(getCountrycodes())>0)
                                                                                                @foreach(getCountrycodes() as $key=>$value)
                                                                                                    <option {{ ($sortmobile[0]==$key)?'selected':'' }}  value="{{ $key }}">+{{ $value }}</option>
                                                                                                @endforeach
                                                                                            @endif
                                                                                        </select>

                                                                                    </span>
                                                                                        </div>

                                                                                        <input type="number"
                                                                                               minlength="9"
                                                                                               maxlength="12"
                                                                                               class="form-control"
                                                                                               required
                                                                                               name="se_phone"
                                                                                               value="{{ !empty(old('se_phone')) ? old('se_phone') : ((isset($oldData) && isset($sortmobile[1])) ? $sortmobile[1] : '') }}"
                                                                                               class="form-control">

                                                                                    </div>
                                                                                    {{ formValidationError($errors, 'se_phone') }}
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label>Message</label>
                                                                                    <div class="input-group">
                                                                        <textarea name="se_message" required
                                                                                  class="form-control">{{formValidationValue('se_message')}}</textarea>
                                                                                    </div>
                                                                                    {{ formValidationError($errors, 'se_message') }}

                                                                                </div>

                                                                            </div>


                                                                        </div>


                                                                    </div>

                                                                    <!-- Modal footer -->
                                                                    <div class="modal-footer">
                                                                        <button type="submit" class="btn btn-success">
                                                                            Send Now
                                                                        </button>
                                                                        {{--<button type="button" class="btn btn-danger"--}}
                                                                        {{--data-dismiss="modal">--}}
                                                                        {{--Close--}}
                                                                        {{--</button>--}}
                                                                    </div>

                                                                </form>

                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>

                                            @endif


                                            @if(isset($verify_otp_products) && in_array($product_info->p_id, $verify_otp_products))
                                                <div class="alert alert-info">
                                                    <strong>Note!:</strong>
                                                    Product Availability Verified
                                                    {{--<a href="#" class="alert-link">read this message</a>.--}}
                                                </div>

                                                @include('frontend.products.productDetailsOptions')

                                            @endif

                                        @else

                                            @include('frontend.products.productDetailsOptions')

                                        @endif



                                    @else


                                        {{--                                    <h4>--}}
                                        {{--                                        {{ $validateCountry['info']  }}--}}
                                        {{--                                    </h4>--}}

                                    @endif




                                <!--/ buttons -->
                                    {{--<a href="javascript:void(0)"><img src="/frontend/images/socialall.png"></a>--}}
                                </div>
                            </div>
                            <!--/ col 6 -->
                        </div>
                        <!--/ page detail top block -->


                        <!-- product detail tab -->


                        @if(isset($product_info->p_related) && $product_info->p_related!='')
                            <div class="modal fade" id="product_sugistItems" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-xl" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">
                                                Suggested Products
                                            </h5>

                                        </div>
                                        <div class="modal-body m-1 text-center">

                                            <div class="row justify-content-center">
                                                <?php
                                                $proview = explode(',', $product_info->p_related);


                                                //                                                dd($proview);

                                                $p_relatedItems = \App\Models\Products::with('getCategory', 'productImages')
                                                    ->where('p_status', 1)
                                                    ->whereIn('p_id', $proview)
                                                    ->orderBy('created_at', 'DESC')
                                                    ->limit(4)
                                                    ->get();


                                                ?>

                                                <?php
                                                if (count($p_relatedItems) > 0) {

                                                    $p_relatedItemsData = array();

                                                    foreach ($p_relatedItems AS $cat_product) {
                                                        if (count($cat_product->productSKUs) > 0) {
                                                            $p_relatedItemsData[] = $cat_product;
                                                        }


                                                    }
                                                }
                                                ?>


                                                @if (isset($p_relatedItemsData) && count($p_relatedItemsData)> 0)
                                                    @foreach($p_relatedItemsData AS $related_item)

                                                        <?php
                                                        $priceSKU = '';
                                                        $image_url = '';

                                                        if (count($related_item->productImages) > 0) {
                                                            $imageID = array_first($related_item->productImages)->pi_id;
                                                            $image_url = getImagesById($imageID);
                                                            if ($image_url == '') {
                                                                $image_url = '/frontend/images/noproduct-available.jpg';
                                                            }
                                                        } else {
                                                            $image_url = '/frontend/images/noproduct-available.jpg';
                                                        }


                                                        if (count($related_item->productSKUs) > 0) {

//                                            dd($related_item->productSKUs);

                                                            $priceSKU = array_first($related_item->productSKUs);
                                                        }
                                                        ?>
                                                        <div class="col-lg-4 col-6 col-sm-6 text-center">
                                                            <div class="productitem">
                                                                <figure>
                                                                    <a href="{{ route('productPage', ['category'=>$related_item->getCategory->category_alias,'product'=>$related_item->p_alias]) }}">
                                                                        <img src="{{  $image_url }}"
                                                                             alt="{{$related_item->p_name}}"
                                                                             title="{{$related_item->p_name}}"
                                                                             class="img-fluid">
                                                                    </a>
                                                                    <div class="hover">
                                                                        <ul class="nav">
                                                                            <li>
                                                                                <a href="javascript:void(0)"
                                                                                   data-toggle="tooltip"
                                                                                   data-id="{{ $related_item->p_id }}"
                                                                                   data-userid="{{ Auth::id() }}"
                                                                                   data-placement="bottom"
                                                                                   title="Add to Wishlist"
                                                                                   class="productaddwishlist{{ $related_item->p_id }} addToWishList {{in_array($related_item->p_id,$wishlistProducts) ? 'likeactive':''}} ">
                                                                                    <span class="icon-heartuser icomoon"></span>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="{{ route('productPage', ['category'=>$related_item->getCategory->category_alias,'product'=>$related_item->p_alias]) }}"
                                                                                   data-toggle="tooltip"
                                                                                   data-placement="bottom"
                                                                                   title="View More"><span
                                                                                            class="icon-external-link icomoon"></span></a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </figure>
                                                                @if($related_item->p_availability=='out_of_stock')
                                                                    <span class="outofstock"><img
                                                                                src="/frontend/images/sold-outimg.png"
                                                                                alt="" title=""></span>
                                                                @endif
                                                                <article>
                                                                    {{--$cat_alias--}}
                                                                    <a class="proname"
                                                                       href="{{ route('productPage', ['category'=>$related_item->getCategory->category_alias,'product'=>$related_item->p_alias]) }}">
                                                                        {{$related_item->p_name}}
                                                                    </a>
                                                                    <p>
                                                                        <?php
                                                                        // dump($priceSKU);
                                                                        ?>
                                                                        {!! currencySymbol(getCurency()) !!} {{ curencyConvert(getCurency(), $priceSKU->sku_store_price)  }}

                                                                        @if ($priceSKU->sku_vdc_final_price != 0)
                                                                            <small class="oldprice">
                                                                                {!! currencySymbol(getCurency()) !!} {{ curencyConvert(getCurency(), $priceSKU->sku_vdc_final_price)  }}
                                                                            </small>
                                                                        @endif
                                                                    </p>
                                                                    {{--<a href="javascript:void(0)" class="btnlist">Add to Cart </a>--}}
                                                                </article>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            <button class="greenlink" data-dismiss="modal" aria-hidden="true">Close
                                            </button>
                                            <a href="{{ route('cartPage') }}" class="greenlink"> Continue Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="col-lg-12">
                            <div class="producttab">
                                <div class="parentHorizontalTab">
                                    <ul class="resp-tabs-list hor_1">
                                        <li>Overview</li>
                                        <li>Specs</li>
                                        <li>Quality Information</li>
                                        <li>Rating &amp; Review</li>
                                    </ul>
                                    <div class="resp-tabs-container hor_1">
                                        <!-- product review -->
                                        <div class="overview">
                                            <!-- row -->
                                            <div class="row">
                                                {{--<div class="col-lg-4 col-md-5">--}}
                                                {{--<img src="/frontend/images/data/acc05.png" alt="" title=""--}}
                                                {{--class="img-fluid">--}}
                                                {{--</div>--}}
                                                <div class="col-lg-12 col-md-12 align-self-center">
                                                    <h3 class="mb-3">Product Overview</h3>
                                                    <div class="text-justify">
                                                        {!!  $product_info->p_overview !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/ row -->
                                        </div>
                                        <!--/ product review -->
                                        <!--product Speficifations-->
                                        <div class="specifications-product">
                                            <h3 class="mb-3">Product Specifications</h3>
                                            <div class="text-justify">
                                                {!!  $product_info->p_specifications !!}
                                            </div>
                                        </div>
                                        <!--/ product Speficifations -->
                                        <!--Quality Information-->
                                        <div class="quality-info">
                                            <h3 class="mb-3">Quality Care Information</h3>
                                            <div class="text-justify">
                                                {!!  $product_info->p_quality_care_info !!}
                                            </div>
                                        </div>
                                        <!--/ Quality Information -->

                                        <!-- review list -->
                                        <div>

                                            <h3 class="mb-3">
                                                User Review / Ratings

                                                @if (Auth::guard('web')->check())
                                                    <small class="float-right d-inline-block" style="font-size: 12px">
                                                        <a data-toggle="modal" data-target="#reviewpopup"
                                                           href="javascript:void(0)" class="pl-3 d-block-mob">
                                                            Add Review
                                                        </a>
                                                    </small>
                                                @else
                                                    <div class="d-block pt-3" style="font-size: 12px">
                                                        Please <a href="{{ route('userlogin') }}"> Login </a>
                                                        and Submit review
                                                    </div>
                                                @endif
                                            </h3>
                                            @if(count($product_info->productReviews)>0)
                                                <ul class="reviewlist">
                                                    @foreach($product_info->productReviews as $reviews )
                                                        <li class="raterow">
                                                            <p>{{$reviews->rt_comment}}</p>
                                                            <div class="row ">
                                                                <div class="col-lg-6">
                                                                    <p class="fgray">
                                                                        <i>
                                                                            <span>{{ (isset($reviews->getUser->name)) ? $reviews->getUser->name:'--' }}</span>
                                                                            |
                                                                            <span>{{ \Carbon\Carbon::parse ($reviews->created_at)->format('d')}}
                                                                                -{{\Carbon\Carbon::parse($reviews->created_at)->format('M')}}
                                                                                -{{ \Carbon\Carbon::parse($reviews->created_at)->format('Y')}}
                                                                            </span></i>
                                                                    </p>
                                                                </div>

                                                                <div class="col-lg-6 text-right">
                                                                    <span class="star-rating s{{$reviews->rt_rating}}"></span>
                                                                </div>

                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </div>
                                        <!--/ review list -->

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--/ product detail tab -->


                        @if (isset($related_items) && count($related_items)> 0)



                        <!-- related products -->
                            <section class="relatedproducts col-md-12">
                                <!-- title row -->
                                <div class="row justify-content-center">
                                    <div class="col-lg-8 text-center">
                                        <article class="hometitle">
                                            <h3 class="px20 py20">You May Also Like</h3>
                                        </article>
                                    </div>
                                </div>
                                <!--/ title row -->
                                <!-- row -->
                                <div class="row">
                                    <!-- col -->

                                    <?php
                                    if (count($related_items) > 0) {

                                        $relatedItemsData = array();

                                        foreach ($related_items AS $cat_product) {
                                            if (count($cat_product->productSKUs) > 0) {
                                                $relatedItemsData[] = $cat_product;
                                            }


                                        }
                                    }
                                    ?>


                                    @if (count($relatedItemsData)> 0)
                                        @foreach($relatedItemsData AS $related_item)

                                            <?php
                                            $priceSKU = '';
                                            $image_url = '';

                                            if (count($related_item->productImages) > 0) {
                                                $imageID = array_first($related_item->productImages)->pi_id;
                                                $image_url = getImagesById($imageID);
                                                if ($image_url == '') {
                                                    $image_url = '/frontend/images/noproduct-available.jpg';
                                                }
                                            } else {
                                                $image_url = '/frontend/images/noproduct-available.jpg';
                                            }


                                            if (count($related_item->productSKUs) > 0) {

//                                            dd($related_item->productSKUs);

                                                $priceSKU = array_first($related_item->productSKUs);
                                            }


                                            ?>


                                            <div class="col-lg-3 col-6 col-md-4 text-center">
                                                <div class="productitem">
                                                    <figure>
                                                        <a href="{{ route('productPage', ['category'=>$related_item->getCategory->category_alias,'product'=>$related_item->p_alias]) }}">


                                                            <img src="{{  $image_url }}" alt="{{$related_item->p_name}}"
                                                                 title="{{$related_item->p_name}}"
                                                                 class="img-fluid">
                                                        </a>
                                                        <div class="hover">
                                                            <ul class="nav">
                                                                <li>

                                                                    <a href="javascript:void(0)" data-toggle="tooltip"
                                                                       data-id="{{ $related_item->p_id }}"
                                                                       data-userid="{{ Auth::id() }}"
                                                                       data-placement="bottom" title="Add to Wishlist"
                                                                       class="productaddwishlist{{ $related_item->p_id }} addToWishList {{in_array($related_item->p_id,$wishlistProducts) ? 'likeactive':''}} ">
                                                                        <span class="icon-heartuser icomoon"></span>

                                                                    </a>


                                                                </li>
                                                                <li>
                                                                    <a href="{{ route('productPage', ['category'=>$related_item->getCategory->category_alias,'product'=>$related_item->p_alias]) }}"
                                                                       data-toggle="tooltip"
                                                                       data-placement="bottom" title="View More"><span
                                                                                class="icon-external-link icomoon"></span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </figure>
                                                    @if($related_item->p_availability=='out_of_stock')
                                                        <span class="outofstock"><img
                                                                    src="/frontend/images/sold-outimg.png"
                                                                    alt="" title=""></span>
                                                    @endif
                                                    <article>

                                                        {{--$cat_alias--}}

                                                        <a class="proname"
                                                           href="{{ route('productPage', ['category'=>$related_item->getCategory->category_alias,'product'=>$related_item->p_alias]) }}">
                                                            {{$related_item->p_name}}
                                                        </a>
                                                        <p>


                                                            <?php
                                                            //                                                        dump($priceSKU);
                                                            ?>


                                                            {!! currencySymbol(getCurency()) !!} {{ curencyConvert(getCurency(), $priceSKU->sku_store_price)  }}


                                                            @if ($priceSKU->sku_vdc_final_price != 0)
                                                                <small class="oldprice">
                                                                    {!! currencySymbol(getCurency()) !!} {{ curencyConvert(getCurency(), $priceSKU->sku_vdc_final_price)  }}
                                                                </small>
                                                            @endif

                                                        </p>

                                                        {{--<a href="javascript:void(0)" class="btnlist">Add to Cart </a>--}}
                                                    </article>
                                                </div>
                                            </div>

                                        @endforeach

                                    @endif


                                </div>
                                <!--/ row -->
                            </section>
                            <!--/ related products -->

                        @endif
                    </div>
                    <!--/ container -->
                </section>
                <!--/ sub page main -->
            </section>
            <!--/ sub page -->
        </main>



        <!--/ pupup for write review -->
        <div class="modal fade" id="reviewpopup">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h5 class="modal-title">Write Review and help to Customers</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <form class="formreview" method="post" action="{{route('addNewRating')}}">
                    {{ csrf_field() }}
                    <!-- Modal body -->
                        <div class="modal-body">

                            <input type="hidden" name="rt_user_id" value="{{ Auth::id() }}">
                            <input type="hidden" name="rt_product_id" value="{{ $product_info->p_id }}">
                            <div class="form-group">
                                <label>Write Review</label>
                                <div class="input-group">
                                    <textarea name="rt_comment" class="form-control"></textarea>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="input-group">
                                    <div class="stars">
                                        <input type="radio" name="rt_rating" value="1" class="star-1" id="0-star-1"/>
                                        <label class="star-1" for="0-star-1">1</label>
                                        <input type="radio" name="rt_rating" value="2" class="star-2" id="0-star-2"/>
                                        <label class="star-2" for="0-star-2">2</label>
                                        <input type="radio" name="rt_rating" value="3" class="star-3" id="0-star-3"/>
                                        <label class="star-3" for="0-star-3">3</label>
                                        <input type="radio" name="rt_rating" value="4" class="star-4" id="0-star-4"/>
                                        <label class="star-4" for="0-star-4">4</label>
                                        <input type="radio" name="rt_rating" value="5" class="star-5" id="0-star-5"/>
                                        <label class="star-5" for="0-star-5">5</label>
                                        <span></span>
                                    </div>

                                </div>
                            </div>


                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">

                            <button type="submit" name="submit" class="greenlink">Submit Review</button>
                            <button type="button" class="whitebtn" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <!--/ popup for write review -->

        <!-- small popup for confirmation message add to kart -->
        <!-- The Modal -->
        <div class="modal" id="addtocart">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Success</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        Your Product Added to Your Cart List Successfully
                    </div>

                </div>
            </div>
        </div>
        <!--/ small popup for confirmation message add to kart-->

        <!-- small popup for Add to wishlist -->
        <!-- The Modal -->
        <div class="modal" id="addtowlist">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Success</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        Product Added to Your Wish List Successfully
                    </div>

                </div>
            </div>
        </div>
        <div class="modal" id="removefromwlist">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Success</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        Product deleted from Your Wish List Successfully
                    </div>

                </div>
            </div>
        </div>
        <!--/ small popup for Add to wishlist-->
    @endif


@endsection
@section('footerScripts')

    @if ($product_info != '')


        <script>
            $(function () {


                {{--@if (count($errors) > 0)--}}
                {{--$('#SendEnquiry').modal('show');--}}
                {{--@endif--}}



                $('.datepicker_hidepastdates').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'yy-mm-dd',
                    yearRange: "-100:+0",
                    minDate: 4
                });
                // $('.apruvels_box').hide();
                // $('.apruvels_button').show();
            })
        </script>


        @include('frontend.products.productScript')

    @endif

@endsection