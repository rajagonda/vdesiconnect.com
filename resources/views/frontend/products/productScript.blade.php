<script>
    $(function () {

        $('.formreview').validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            errorPlacement: function (error, e) {
                e.parents('.form-group').append(error);
            },
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.text-danger').remove();
            },
            success: function (e) {
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.text-danger').remove();
            },
            rules: {
                rt_comment: {
                    required: true,
                    // minlength: 10,
                    maxlength: 100,
                },
                rt_rating: {
                    required: true,

                },

            },
            messages: {
                rt_comment: {
                    required: 'Enter Your Review',
                    // minlength: 'Enter minimam {0}',
                    maxlength: 'Enter max {0}',
                },
                rt_rating: {
                    required: 'Please select rate',

                },



            }
        });




        $('.addToCartInProduct').on('click', function () {
            var id = $(this).data('id');
            var qty = 1;
            var message = $('.message').val();
            var price = $('.productPrice').val();
            var delivery_charge = $('.delivery_charge').val();
            var deliverydate = $('.deliverydate').val();
            var deliverytime = $('.deliverytime').val();
            var countryShipped = $('.countryShipped').val();
            var nameongift = $('.nameongift').val();

            // alert(countryShipped);

            var options = [];
            $(".detailtable select").each(function () {
                if ($(this).attr('name')) {
                    var optionName = $(this).attr('name');
                    var optionGet = $(this).find('option:selected').attr('data-option');
                    var optionsData = optionName + "/" + optionGet + "/" + $(this).val();
                    // console.log(optionsData);

                    options.push([optionsData]);
                }
            })

            var optionsData = $('.buyproduct_options').val(options).val();

            console.log(optionsData);

            if (id != '' && qty != 0) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{ route('addToCart') }}',
                    type: 'POST',
                    data: {
                        'id': id,
                        'qty': qty,
                        'price': price,
                        'options': optionsData,
                        'message': message,
                        'delivery_date': deliverydate,
                        'delivery_time': deliverytime,
                        'delivery_charge': delivery_charge,
                        'countryShipped': countryShipped,
                        'nameongift': nameongift,
                    },

                    success: function (response) {
                        $('#cartcontents').text(response);

                        var url = "{{route('cartPage')}}";


                        @if($product_info->p_related!='')

                        $('#product_sugistItems').modal({
                            backdrop: 'static',
                            keyboard: false  // to prevent closing with Esc button (if you want this too)
                        })

                        @else

                            window.location = url;

                        @endif






                        // $('#addtocart').modal('show');
                    }
                });
            } else {
                alert('Please fill all Gift Custamase fields');
            }
        });


        $('.select_sku').on('change', function () {


            var options = $('.detailtable :input').serialize();
            // console.log(options);


            var productUrl = "{{ route('productPage', ['category'=>$product_info->getCategory->category_alias,'product'=>$product_info->p_alias]) }}?" + options;

            {{--console.log(productUrl);--}}

            if ($(this).val()) { // require a URL
                window.location = productUrl; // redirect
            }
            return false;
        });


        //Horizontal Tab
        $('.parentHorizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function (event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });

        //Vertical Tab
        $('.parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function (event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo2');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });


        $('.gallery').simplegallery({
            galltime: 400,
            gallcontent: '.content',
            gallthumbnail: '.thumbnail',
            gallthumb: '.thumb'
        });
    });

    $('.buyproduct').click(function () {
        if ($('.productqty').val()) {
            var qty = $('.productqty').val();
        } else {
            var qty = 1;
        }
        $('.buyproductqty').val(qty);
        $('.buyproductmessage').val($('.message').val());
        $('.buyproduct_productPrice').val($('.productPrice').val());
        $('.buyproductdelivery_charge').val($('.delivery_charge').val());
        $('.buyproductdelivery_date').val($('.deliverydate').val());
        $('.buyproductdelivery_time').val($('.deliverytime').val());
        $('.countryShipped').val($('.countryShipped').val());
        $('.buy_nameongift').val($('.nameongift').val());


        var options = [];
        $(".detailtable select").each(function () {
            if ($(this).attr('name')) {
                var optionName = $(this).attr('name');
                var optionGet = $(this).find('option:selected').attr('data-option');
                var optionsData = optionName + "/" + optionGet + "/" + $(this).val();
                console.log(optionsData);

                options.push([optionsData]);
            }
        })

        $('.buyproduct_options').val(options).val();


        $('.buyProductForm').submit();
    });
</script>