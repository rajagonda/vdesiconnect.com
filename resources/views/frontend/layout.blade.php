<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="description" content="@yield('meta_description')">
    <meta name="keywords" content="@yield('meta_keywords')">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <meta name="p:domain_verify" content="91133d70f4c160dbaf1300fc30399912"/> <!-- SEO TAG -->
    <link rel="canonical" href="{{ url()->current() }}" />
    @include('frontend._partials.stylesheets')

    @yield('header_styles')
<!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HT3JVK" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
</head>
<body>
<!--header -->
@include('frontend._partials.header')
<!--/ header-->
<!--main -->
@yield('content')
<!--/ main -->
<!--footer -->
@include('frontend._partials.footer')
<!--/ footer -->
<!-- footer scripts -->
@include('frontend._partials.scripts')
@yield('footerScripts')
<!--/ footer scripts -->


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-143373424-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-143373424-1');
</script>



</body>
</html>