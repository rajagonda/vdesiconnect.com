@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')


    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Ammachethi Vanta Order</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{ route('home') }}">Home</a></li>
                                <li><a href="{{ route('ammaChethiVanta') }}">Ammachethi Vanta</a></li>
                                <li><a>Ammachethi Vanta Order </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="whiterow mb-3 pb-0">
                        <div class="row">
                            <div class="col-lg-6 col-6">
                                <h4 class="pl-2">
                                    Orders
                                </h4>
                            </div>
                            <div class="col-lg-6 col-6 text-right">
                                {{--<h5 class="pr-2">{{ currencySymbol('USD') }} {{ Cart::getSubTotal() }}</h5>--}}
                            </div>
                        </div>
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row">
                        <!--left col -->
                        <div class="col-lg-3 userleftnav">
                            @include('frontend.user.usermenu')
                        </div>
                        <div class="col-lg-9 ">
                            <div class="whitebox rightprofile p-3">

                                <!-- row -->
                                <div class="row">
                                    @if(count($ordersList)>0)

                                        @foreach( $ordersList as $item)
                                            <?php
                                            $orderCrrency = $item->ac_order_curency;
//                                            dump($ordersList);
                                            //                                    $paymentResponcive = unserialize($item->ac_order_payment_response_dump);
                                            ?>


                                        <!-- small column -->
                                            <div class="col-lg-12">
                                                <div class="smallcol mb-3">
                                                    <div class="row px-4">
                                                        <div class="col-md-2 col-6">
                                                            <span class="price">
                                                               <small> Price</small>
                                                                <span class="d-block w-100 cur">
                                                                    {!! currencySymbol($orderCrrency)  !!}
                                                                    {{  $item->ac_order_total_price }}
                                                                </span>
                                                            </span>
                                                        </div>

                                                        <div class="col-md-2 col-6">
                                                             <span class="price">
                                                                 <small>Weight</small>
                                                                 <span class="d-block w-100 cur">
                                                                    <?php
                                                                    $ordersWhights = array();
                                                                    if (count($item->orderItems)) {
                                                                        foreach ($item->orderItems AS $ordItem) {
                                                                            $ordersWhights[] = $ordItem->acoitem_weight;
                                                                        }
                                                                    }
                                                                    ?>
                                                                    {{ array_sum($ordersWhights) }}   Kgs
                                                                 </span>
                                                             </span>
                                                        </div>
                                                        <div class="col-md-2 col-6">
                                                            <span class="price">
                                                                <small>Item </small>
                                                                <span class="d-block w-100 cur">

                                                                {{ count($ordersWhights) }}
                                                                </span>
                                                             </span>
                                                        </div>
                                                        <div class="col-md-5 col-6">
                                                            <span class="price">
                                                               <small>Payment Status</small>
                                                                <span class="d-block w-100 ">
                                                                {!! PayapPaymentStatusShow($item->ac_order_payment_status) !!}
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-1 col-12">
                                                            {{--  {{ $item->ac_order_id }}--}}
                                                            <a href="{{route('AmmachethiVantaOrderView', ['id'=>$item->ac_order_id])}}" class="greenlink">
                                                                View
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- small column -->
                                        @endforeach
                                    @else



                                        <div class="col-lg-12">
                                            <h5 class="sectitle fbold pb-3">Amma Chethi Vanta Orders</h5>


                                            No Data Found
                                        </div>
                                    @endif


                                </div>
                            </div>
                            <!--/ row -->
                        </div>

                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main -->


@endsection
@section('footerScripts')
@endsection