<form method="POST" action="{{ route('userAddAddressBook') }}" accept-charset="UTF-8" class="userAddress" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="ua_id">
    <input type="hidden" name="flag" value="5">
    <!-- Modal Header -->
    <div class="modal-header">
        <h4 class="modal-title">Add New Delivery Address</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body">

        <div class="row">
            <div class="col-md-6">

                <div class="form-group">
                    <label>Name</label>
                    <input type="text" placeholder="Write Your Name" class="form-control" name="ua_name">
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="text" placeholder="Enter email" class="form-control" name="ua_email">
                </div>

                <div class="form-group">
                    <label>Phone Number</label>
                    <input type="text" placeholder="Enter Phone Number" class="form-control"
                           name="ua_phone">
                </div>

                <div class="form-group">
                    <label>Select Country</label>
                    <select name="ua_country" id="ua_country" class="form-control">
                        <?php
                        $countries = getCountries(); ?>
                        @foreach($countries as $ks=>$s)
                            <option value="{{ $ks }}">
                                {{ $s }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Select State</label>
                    <input type="text" placeholder="State Name" class="form-control" name="ua_state">
                </div>

            </div>
            <div class="col-md-6">

                <div class="form-group">
                    <label>Select City</label>
                    <input type="text" placeholder="City Name" class="form-control" name="ua_city">
                </div>

                <div class="form-group">
                    <label>Address</label>
                    <input type="text" placeholder="H.No/Street Name" class="form-control"
                           name="ua_address">
                </div>

                <div class="form-group">
                    <label>Landmark</label>
                    <input type="text" placeholder="Land Mark (Ex: Near Hospital)" class="form-control"
                           name="ua_landmark">
                    <small>Entering a Land Mark will help us in identifying your location faster</small>
                </div>

                <div class="form-group">
                    <label>Area Pincode</label>
                    <input type="text" placeholder="Area Pincode" class="form-control" name="ua_pincode">
                </div>

            </div>
        </div>
        <div class="form-group">
            <input type="checkbox" name="ua_defult"
                   value="1">
            <label>Default Address</label>
        </div>

    </div>
    <!-- Modal footer -->
    <div class="modal-footer">
        <button type="submit" class="btn btn-success">Add Address</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
    </div>
</form>