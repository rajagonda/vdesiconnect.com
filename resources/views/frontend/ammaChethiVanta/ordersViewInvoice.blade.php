@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')


    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Review Order</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{ route('home') }}">Home</a></li>
                                <li><a href="{{ route('ammaChethiVanta') }}">Back To List</a></li>
                                <li><a>Review Order </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="whiterow mb-3 pb-0">
                        <div class="row">
                            <div class="col-lg-6 col-6">
                                <h4 class="pl-2">Items ({{ count($items) }})</h4>
                            </div>
                            <div class="col-lg-6 col-6 text-right">
                                {{--<h5 class="pr-2">{{ currencySymbol('USD') }} {{ Cart::getSubTotal() }}</h5>--}}
                            </div>
                        </div>
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row">
                        <!--left col -->
                        <div class="col-lg-9 col-md-7">
                            <!-- row -->
                            <div class="row">

                            @if(count($items)>0)
                                <?php
                                $total = 0;
                                ?>
                                @foreach( $items as $key=>$item)

                                    <?php
                                    $itemInfo = getAmmachethivantaParentInfo($key);
                                    $total += getAmmachethivantaProductPrice($key, $itemweight[$key])->as_price;
                                    ?>
                                    <!-- small column -->
                                        <div class="col-lg-12">
                                            <div class="smallcol mb-3">
                                                <div class="row">
                                                    <div class="col-lg-3  col-sm-3">
                                                        <figure>
                                                            <img src="/uploads/ammachethivanta/{{ $itemInfo->ac_image }}"
                                                                 alt=""
                                                                 title="" class="img-fluid w-100">
                                                        </figure>
                                                    </div>
                                                    <div class="col-lg-9 col-sm-9 pl-0">

                                                        <div class="row mpx-15">
                                                            <div class="col-lg-2">
                                                                <span class="price">
                                                                   <small> Price</small>
                                                                    <span class="d-block w-100 cur"> {{ currencySymbol('USD') }}
                                                                        {{ getAmmachethivantaProductPrice($key,$itemweight[$key])->as_price }}</span>



                                                                </span>


                                                            </div>

                                                            <div class="col-lg-2">
                                                                 <span class="price">
                                                                <small>Weight</small>
                                                                <span class="d-block w-100 cur"> {{ $itemweight[$key] }}
                                                                    Kgs</span>
                                                                 </span>
                                                            </div>

                                                            <div class="col-lg-2">
                                                                <span class="price">
                                                                   <small>Sub Total</small>
                                                                    <span class="d-block w-100 cur"> {{ currencySymbol('USD') }}
                                                                        {{ getAmmachethivantaProductPrice($key,$itemweight[$key])->as_price }}</span>
                                                                </span>
                                                            </div>


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                        </div>


                                        <!-- small column -->
                                    @endforeach
                                @else
                                    <div class="col-lg-12">
                                        No Data Found
                                    </div>
                            @endif

                            <!--/ row -->
                                <!-- address column -->
                                <div class="col-lg-12 pt-2">
                                    <div class="whitebox p-3">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <p class="fblue">Delivery Address</p>
                                                <p>{{ $address_info->ua_name }}</p>
                                                <p>{{ $address_info->ua_address }}
                                                    , {{ $address_info->ua_landmark }},
                                                    {{ $address_info->ua_city }},{{ $address_info->ua_state }}
                                                    ,{{ $address_info->getCountry->country_name }}
                                                    - {{ $address_info->ua_pincode }} <a
                                                            href="{{ route('ammachethivanta_address_page') }}">Change
                                                        Address</a></p>
                                                <p>Phone: {{ $address_info->ua_phone }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- address column -->
                            </div>
                            <!--/ row -->
                        </div>
                        <!--/left col -->
                        <!-- right cart -->
                        <div class="col-lg-3 col-md-5">
                            <div class="rtcart">
                                <h6 class="pb-2">Items ({{ count($items) }}) Items</h6>
                                <h4 class="fgreen fbold py-2 h4">You Pay <span
                                            class="float-right fgreen fbold">

                                    {{ currencySymbol('USD') }} {{ $total }}



                                 </span>
                                </h4>
                                <form class="mb-2" method="POST" id="saveOrders"
                                      action="{{ route('ammachethivantaOrdersSave') }}"
                                      accept-charset="UTF-8">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="address_id" value="{{ $address_id }}">
                                    <input type="hidden" name="total" value="{{ $total }}">
                                    <button type="submit" class="greenlink w-100">Proceed to Payment</button>
                                </form>
                                <a href="{{route('ammaChethiVanta')}}" class="whitebtn text-center w-100">Continue
                                    Shopping</a>


                            </div>


                        </div>
                        <!--/ right cart -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main -->


@endsection
@section('footerScripts')
@endsection