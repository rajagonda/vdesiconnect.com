@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')


    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="img/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="img/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Delivery Address</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a href="{{route('ammaChethiVanta')}}">Back to List </a></li>
                                <li><a>Delivery Address </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->

            <!--sub page main -->
            <section class="subpagemain">


                <!-- container -->
                <div class="container">

                {{ laravelReturnMessageShow() }}

                <!-- row -->
                    <div class="row">
                        <div class="col-lg-12 text-right">
                            <a data-toggle="modal" data-target="#newaddress" href="javascript:voidf(0)" class="addbtn">+
                                Add Address</a>
                        </div>
                    </div>
                    <!--/ row --->
                    <!-- row -->
                    <div class="row">
                        <!--left col -->
                        <div class="col-lg-12 col-sm-12">
                            <!-- row -->
                            <div class="row">
                            @if(count($userAddresses)>0)
                                @foreach($userAddresses as $adds)
                                    <!-- col -->
                                        <div class="col-lg-4">
                                            <div class="addresscol whitebox">
                                                <p class="fgreen fbold">@if($adds->ua_defult==1) Default @endif</p>
                                                <p>{{ $adds->ua_name }}</p>
                                                <p>{{ $adds->ua_address }}, {{ $adds->ua_landmark }},
                                                    @if( isset( $adds->getCity->city_name ) ){{ isset($adds->getCity->city_name) ?  $adds->getCity->city_name:''}}, @endif
                                                    @if( isset( $adds->getState->state_name ) ){{ isset($adds->getState->state_name) ?  $adds->getState->state_name:''}}, @endif
                                                    {{ $adds->getCountry->country_name }}
                                                    - {{ $adds->ua_pincode }}</p>
                                                <p>Phone: {{ $adds->ua_phone }}</p>

                                                <form method="POST" id="checkoutPage"
                                                      action="{{ route('acvorderConfirmation') }}"
                                                      accept-charset="UTF-8">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="address_id" value="{{ $adds->ua_id }}">
                                                    <input type="submit" value="Deliver Here" class="greenlink w-100">
                                                </form>
                                                <p class="pt-3 float-left">
                                                    <a class="float-left" href="javascript:void(0)" data-toggle="modal"
                                                       data-target="#updateAddress{{ $adds->ua_id }}"><span
                                                                class="icon-edit icomoon"></span>Edit
                                                        address </a>

                                                <form class="float-left mt-3 ml-2" method="POST" id="useraddress"
                                                      action="{{ route('userAddressBook') }}"
                                                      accept-charset="UTF-8">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="flag" value="5"/>
                                                    <input type="hidden" name="ua_id"
                                                           value="{{ $adds->ua_id }}"/>
                                                    <button class="nobrd" type="submit"><span
                                                                class="icon-bin icomoon "></span>Delete
                                                    </button>
                                                </form>

                                                @if($adds->ua_defult!=1)
                                                    <form method="POST" action="{{ route('makeDefaultAddress') }}"
                                                          accept-charset="UTF-8"
                                                          class="form-horizontal" id="makeDefaultAddress"
                                                          enctype="multipart/form-data">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="ua_id" value="{{ $adds->ua_id }}">
                                                        <input type="hidden" name="flag" value="5">
                                                        <input type="hidden" name="ua_defult" value="1">
                                                        <button type="submit"
                                                                class="btn"> Set as
                                                            Default
                                                        </button>
                                                    </form>
                                                @endif

                                                <div class="modal fade" id="updateAddress{{ $adds->ua_id }}">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            @include('frontend.user.includs.update_adress_form')
                                                        </div>
                                                    </div>
                                                </div>
                                                </p>
                                            </div>
                                        </div>
                                        <!--/ col -->

                                    @endforeach
                                @endif
                            </div>
                            <!--/ row -->
                        </div>
                        <!--/left col -->
                        <!-- right cart -->
                    {{--<div class="col-lg-3 col-md-5">
                        <div class="rtcart">
                            @include('frontend._partials.checkout_right')

                            --}}{{--<p class="py-3">The price and availability of items at Vdesiconnect are subject to--}}{{--
                                --}}{{--change. The shopping cart is a temporary place to store a list of your items and--}}{{--
                                --}}{{--reflects each item's most recent price.</p>--}}{{--
                        </div>
                    </div>--}}

                    <!--/ right cart -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main -->



    <!--/ Add New Address -->
    <div class="modal fade" id="newaddress">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                @include('frontend.user.includs.adress_form')
            </div>
        </div>
    </div>



    </div>
    <!--/ Add New Address -->

@endsection
@section('footerScripts')
    @include('frontend.user.scripts')

    <script>
        $(function () {
            $('.ua_country').on('change', function () {
                var addresFormID = $(this).parents('form').attr('id');
                selectcountry(addresFormID, 'country')
            });

            $('.ua_state').on('change', function () {
                var addresFormID = $(this).parents('form').attr('id');
                console.log(addresFormID);
                selectcountry(addresFormID, 'state')
            });


            $('.modelbtn').on('click', function () {
                var modelId = $(this).data('target');
                console.log(modelId + " => model")

                $(modelId).on('shown.bs.modal', function () {
                    var addresFormID = $(modelId).find('form').attr('id');
                    selectcountry(addresFormID, 'country')
                })

            });
        })

    </script>

@endsection