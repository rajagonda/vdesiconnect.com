@extends('frontend.layout')
@section('title', $title)
@section('headerStyles')
@endsection
@section('content')


    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Review Order</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{ route('home') }}">Home</a></li>
                                <li><a href="{{ route('ammaChethiVanta') }}">Ammachethi Vanta</a></li>
                                <li><a href="{{ route('AmmachethiVantaOrders') }}">Ammachethi Vanta Orders</a></li>
                                <li><a>Ammachethi Vanta Order View </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->
            <section class="subpagemain">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="whiterow mb-3 pb-0">
                        <div class="row">
                            <div class="col-lg-6 col-6">
                                <h4 class="pl-2">
                                    Order View Items
                                </h4>
                            </div>
                            <div class="col-lg-6 col-6 text-right">
                                {{--<h5 class="pr-2">{{ currencySymbol('USD') }} {{ Cart::getSubTotal() }}</h5>--}}
                            </div>
                        </div>
                    </div>
                    <!--/ row -->

                    <!-- row -->
                    <div class="row">

                        <div class="col-lg-3 userleftnav">
                            @include('frontend.user.usermenu')
                        </div>
                        <div class="col-lg-9">

                            <!-- row -->
                            <div class="row">
                                <?php
                                $paymentResponcive = paypalResponce($orderData->ac_order_payment_response_dump);

                                //                                dd($orderData);
                                //                                dump($paymentResponcive->amount->total);


                                $orderCrrency = $orderData->ac_order_curency;


                                $ordersWhights = array();
                                if (count($orderData->orderItems)) {
                                    foreach ($orderData->orderItems AS $ordItem) {
                                        $ordersWhights[] = $ordItem->acoitem_weight;
                                    }
                                }
                                ?>


                                <div class="col-lg-12">
                                    <div class="smallcol mb-3">

                                        <div class="row">
                                            <div class="col-lg-2">
                                                Order No
                                                <div>
                                                    #{{ $orderData->ac_order_id }}
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                Payment Status <br/>

                                                {!! PayapPaymentStatusShow($orderData->ac_order_payment_status) !!}

                                            </div>
                                            <div class="col-lg-2">
                                                Payment With
                                                {{ $orderData->ac_order_payment_mode }}
                                            </div>
                                            <div class="col-lg-2">
                                                Paid Amount
                                                <div>
                                                    {!! currencySymbol($orderCrrency)  !!}

                                                    {{ $orderData->ac_order_total_price }}
                                                </div>

                                            </div>

                                            <div class="col-lg-2">
                                                Wight
                                                <div>
                                                    {{ array_sum($ordersWhights) }} Kgs
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5>Order Items</h5>
                                            </div>
                                        </div>

                                        @if (count($orderData->orderItems))
                                            @foreach ($orderData->orderItems AS $ordItem)
                                                <?php

                                                //                                            dump($ordItem);
                                                ?>
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        image

                                                        <div>

                                                            <img width="50"
                                                                 src="{{ \App\Models\Ammachethivanta::getImage($ordItem->getProduct->ac_image) }}"/>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-8">
                                                        Name
                                                        <div>
                                                            {{ $ordItem->getProduct->ac_name }}
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        weight
                                                        <div>
                                                            {{ $ordItem->acoitem_weight }} Kgs
                                                        </div>
                                                    </div>

                                                </div>

                                            @endforeach

                                        @else
                                            <div class="text-center">Products Not avalable </div>
                                        @endif


                                    </div>
                                </div>


                            </div>
                            <!--/ row -->
                        </div>


                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </section>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>
    <!--/ main -->


@endsection
@section('footerScripts')
@endsection