@extends('frontend.layout')
@section('title', $title)
@section('meta_keywords', $meta_keywords)
@section('meta_description', $meta_description)

@section('header_styles')

    {{--<link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"/>--}}
@endsection


@section('content')


    <main>
        <!-- sub apge -->
        <section class="subpage">
            <!-- sub page header -->
            <section class="subpageheader">
                <span class="pattern01 position-absolute"><img src="/frontend/images/pageleftpattern.png"></span>
                <span class="pattern02 position-absolute"><img src="/frontend/images/pagerightpattern.png"></span>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <article class="pagetitle">
                                <h1 class="px20 py20">Amma Chethi Vanta</h1>
                            </article>
                            <!-- brudcrumb -->
                            <ul class="brcrumb">
                                <li><a href="{{route('home')}}"> Home </a></li>
                                <li><a> Amma Chethi Vanta </a></li>
                            </ul>
                            <!--/ brudcrumb -->
                        </div>
                    </div>
                </div>
            </section>
            <!--/ sub page header -->
            <!--sub page main -->

            <?php



            //            $priceInfp = ammaChethiVantaWightBasePrice(100);






            //            dd($priceInfp);
            ?>


            <form class="mb-2" method="POST" id="orderssave"
                  action="{{ route('saveRecordsIntoSessions') }}"
                  accept-charset="UTF-8">
                {{ csrf_field() }}


                <section class="subpagemain">
                    <!-- container -->
                    <div class="container stpage vantalupage">
                        <!-- row -->
                        <div class="row">
                            <!-- col 9 -->
                            <div class="col-lg-9">
                                <h3 class="h3 text-center pagesubtitle">
                                    Amma cheti vanta miss avuthunana ra special
                                    package for <br/>
                                    {{--                                    <span class="greencolor">200$-10kgs</span>--}}

                                    <?php
                                    $pricelists = ammaChethiVantaWightsData();
                                    if (count($pricelists) > 0) {
                                    foreach ($pricelists AS $kgs=>$price) {
                                    ?>
                                    <span class="greencolor">
                                            {!! currencySymbol(getCurency()) !!}{{ curencyConvert(getCurency(), $price)  }}
                                            -
                                            {{ $kgs }}kgs,
                                        </span>
                                    <?php


                                    }
                                    }
                                    ?>
                                    <br/>


                                    with free shipping to choose from a variety of pickles/sweets/snacks.
                                </h3>
                                <!-- responsive table -->
                                <table class="rtable" id="table-container-breakpoint">
                                    <thead>
                                    <tr>
                                        <th>Select</th>
                                        <th>Image</th>
                                        <th>Name of Food</th>
                                        <th>Weight</th>
                                        {{--<th>Price</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($records)>0)
                                        @foreach($records as $item)
                                            <tr id="{{ $item->ac_id }}">
                                                <td>
                                                    <input type="checkbox"
                                                           class="selectproduct_{{ $item->ac_id }} productsselection"
                                                           name="options[{{ $item->ac_id }}]">
                                                </td>
                                                <td>
                                                    <img src="{{ \App\Models\Ammachethivanta::getImage($item->ac_image) }}" class="img-fluid" width="120">
                                                </td>
                                                <td>{{ $item->ac_name }}</td>
                                                <td>
                                                    1KG
                                                    <input type="hidden" name="weight[{{ $item->ac_id }}]"  class="weightcaliclation itemextraoptions_{{ $item->ac_id }}" value="1">
                                                </td>

                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4">No records found</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <!--/ responsive table -->
                            </div>
                            <!--/ col 9-->
                        @if(count($records)>0)
                            <!-- right price -->
                                <div class="col-lg-3 foodvalue">
                                    <div class="foodvalue-in">
                                        <div>
                                            <p>Total Weight You Selected</p>
                                            <h3 class="h3"><strong class="total_weight">0</strong> kg</h3>
                                        </div>
                                        <div>
                                            <p>Total Items You Selected</p>
                                            <h3 class="h3"><strong class="total_items">0</strong></h3>
                                        </div>
                                        {{--<div>--}}
                                        {{--<p>Total Value</p>--}}
                                        {{--<h3 class="h3">$<strong class="total_value">0</strong></h3>--}}
                                        {{--</div>--}}
                                        <div>
                                            <p>Grand Total</p>
                                            <h3 class="h3">
                                            <span class="total_curency">
                                                {!! currencySymbol(getCurency()) !!}
                                            </span>
                                                <strong class="forange total_value">0</strong>
                                            </h3>
                                        </div>
                                        <div class="foodbtn">
                                            <button type="submit" class="greenlink btn submitbutton">Pay Now</button>
                                        </div>

                                        <p class="py-3 error weightsuccesmessage" style="color:green; display: none">
                                            You Have Choosen Package is <strong class="total_weight">0</strong>Kg, Proced To
                                            Checkout
                                        </p>

                                        <p class="py-3 error weighterrormessage" style="color:red;">
                                            Purchase Items Required By
                                            <?php
                                            $pricelists = ammaChethiVantaWightsData();
                                            if (count($pricelists) > 0) {
                                            foreach ($pricelists AS $kgs=>$price) {
                                            ?>
                                            <span class="greencolor">

                                            {{ $kgs }}kgs,
                                        </span>
                                            <?php


                                            }
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                                <!--/ right price -->
                            @endif
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ container -->
                </section>

            </form>
            <!--/ sub page main -->
        </section>
        <!--/ sub page -->
    </main>

@endsection

@section('footerScripts')
    <script>


        function checkboxlist() {

            // var limit = 5;

            $('.weighterrormessage').show();
            $('.submitbutton').prop('disabled', true);

            var totalitems = 0;
            var total_weight = 0;
            var totalvalue = 0;
            $('.total_items').text(totalitems);
            $('.total_value').text(totalvalue);
            $('.total_weight').text(total_weight);


            $('.productsselection').on('change', function (evt) {

                var checkedLenth = $('.productsselection:checked').length;


                $.ajax({
                    url: '{{route('ajax_ammchathivanta_wightbased_price')}}',
                    type: 'GET',
                    data: {'wight': checkedLenth},
                    success: function (response) {

                        console.log(response)

                        response = JSON.parse(response);


                        // console.log(response.current_wight);
                        $('.total_items').text(parseFloat(response.current_wight));
                        $('.total_weight').text(parseFloat(response.current_wight));
                        if (response.checkout_btn == 'on') {

                            $('.total_value').text(response.price);
                            $('.total_curency').html(response.currency);

                            $('.submitbutton').attr("disabled", false);
                            $('.weighterrormessage').hide();
                            $('.weightsuccesmessage').show();
                        } else {
                            $('.submitbutton').attr("disabled", true);
                            $('.weighterrormessage').show();
                            $('.weightsuccesmessage').hide();
                        }


                    }
                });


                // totalitems = parseFloat(checkedLenth);


                /* if (checkedLenth > limit) {
                     if (checkedLenth > limit + 1) {
                         this.checked = false;
                         alert("select only " + (limit + 1) + " items ")
                     }
{{--                    totalvalue = parseFloat({{ curencyConvert('USD', 100) }});--}}
                } else {
                    $('.submitbutton').attr("disabled", true);
                    $('.weighterrormessage').show();
                }*/
            });


        }


        $(function () {
            checkboxlist();
            $('.productsselection').on('click', function () {
                checkboxlist();
            });


            $('#contactform').validate({
                ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    s_contact_person: {
                        required: true
                    },
                    s_address_line_1: {
                        required: true
                    },
                    s_address_line_2: {
                        required: true
                    },
                    s_country: {
                        required: true
                    },
                    s_state: {
                        required: true
                    },
                    s_city: {
                        required: true
                    },
                    s_options: {
                        required: true
                    },
                    s_date: {
                        required: true
                    },
                    s_time: {
                        required: true
                    },
                    s_email: {
                        required: true,
                        email: true
                    },
                    s_phone: {
                        required: true
                    },
                    s_msg: {
                        required: true
                    }
                },
                messages: {
                    s_contact_person: {
                        required: 'Contact Person is required'
                    },
                    s_address_line_1: {
                        required: 'Addres line 1 is required'
                    },
                    s_address_line_2: {
                        required: 'Addres line 1 is required'
                    },
                    s_country: {
                        required: 'country is required'
                    },
                    s_state: {
                        required: 'State is required'
                    },
                    s_city: {
                        required: 'City is required'
                    },
                    s_options: {
                        required: 'Select Medicle Service'
                    },
                    s_date: {
                        required: 'Date is required'
                    },
                    s_time: {
                        required: 'Time is required'
                    },
                    s_email: {
                        required: 'Enter email',
                        email: 'Enter a valid email'
                    },
                    s_phone: {
                        required: 'Phone number is required',
                    },
                    s_msg: {
                        required: 'Message is required',
                    }

                }
            });


        });

    </script>

    <script>
        $(document).ready(function(){
            var s = $(".foodvalue");
            var pos = s.position();
            $(window).scroll(function(){
                var windowpos = $(window).scrollTop();
                if(windowpos >= pos.top & windowpos <=1800){
                    s.addClass("fixedfood");
                }else{
                    s.removeClass("fixedfood");
                }
            });
        });

    </script>



    <script>
        $(function () {
            $('.datepicker').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                yearRange: "-100:+0"
            });

            $('#table').basictable();

            $('#table-breakpoint').basictable({
                breakpoint: 768
            });

            $('#table-container-breakpoint').basictable({
                containerBreakpoint: 485
            });

            $('#table-swap-axis').basictable({
                swapAxis: true
            });

            $('#table-force-off').basictable({
                forceResponsive: false
            });

            $('#table-no-resize').basictable({
                noResize: true
            });

            $('#table-two-axis').basictable();

            $('#table-max-height').basictable({
                tableWrapper: true
            });

        });
    </script>




@endsection