@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')

    {{--    //@if($category->category_parent_id!=0)--}}
    @if(isset($category_id))


        <ul class="breadcrumb ">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('categories')}}">Categories</a>
            </li>
            @if(isset($category->getCategoryName->category_name))
                <li class="breadcrumb-item">
                    <a href="{{url('admin/categories?search=search&category_name=&search_category='.$category->getCategoryName->category_id)}}">{{$category->getCategoryName->category_name}}</a>
                </li>
            @endif

            @if(isset($category->category_id))
                <li class="breadcrumb-item">
                    <a href="{{url('admin/categories?search=search&category_name=&search_category='.(isset($category->category_id)?$category->category_id:''))}}">{{$category->category_name}}</a>
                </li>
            @endif

            @if(isset($category->category_parent_id) && $category->category_parent_id!=0)
                <li class="breadcrumb-item active">{{$category->category_name}}</li>
            @endif
        </ul>


    @else
        {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'categories'=>'Categories',
               ''=>'Add New'
               ),'Add New Category'
            ) !!}

    @endif



    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="categories" action="{{ route('addNewCategories') }}"
                                  accept-charset="UTF-8" class="form-horizontal categoriesValid"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="category_id" value="{{ $category_id }}">

                                <input type="hidden"
                                       class="edit_current_category_alias"
                                       value="{{ (isset($category->category_alias)) ? $category->category_alias : old('category_alias')}}"/>


                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Category (is this Parent
                                            Category or SubCategory)</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="category_parent_id" id="category_parent_id" class="form-control">
                                            <option value="">--Select Parent or SubCategory--</option>
                                            <option value="0" {{ (!empty(old('category_parent_id')) && old('category_parent_id')==0)  ? 'selected' : ((($category) && ($category->category_parent_id == 0)) ? 'selected' : '') }}>
                                                Parent
                                            </option>
                                            <?php
                                            $categories = getCategories($category_id);
                                            ?>
                                            @foreach($categories as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('category_parent_id')) && old('category_parent_id')==$ks)  ? 'selected' : ((($category) && ($category->category_parent_id == $ks)) ? 'selected' : '') }}>
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('category_parent_id'))
                                            <span class="text-danger help-block">{{ $errors->first('category_parent_id') }}</span>
                                        @endif
                                    </div>
                                </div>


                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Shipped by</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="category_shipped" id="category_shipped" class="form-control">
                                            <?php
                                            $catShipped = categoryShipped(); ?>
                                            @foreach($catShipped as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('category_shipped')) && old('category_shipped')==$ks)  ? 'selected' : ((($category) && ($category->category_shipped == $ks)) ? 'selected' : '') }}
                                                >
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('category_shipped'))
                                            <span class="text-danger help-block">{{ $errors->first('category_shipped') }}</span>
                                        @endif
                                    </div>
                                </div>


                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Category Name</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control titleCreateAlias" id="category_name" type="text"
                                               placeholder="Category Name"
                                               name="category_name"
                                               value="{{ !empty(old('category_name')) ? old('category_name') : ((($category) && ($category->category_name)) ? $category->category_name : '') }}">
                                        @if ($errors->has('category_name'))
                                            <span class="text-danger help-block">{{ $errors->first('category_name') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Category Alias</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="category_alias" type="text"
                                               placeholder="Category Alias"
                                               name="category_alias" readonly
                                               value="{{ !empty(old('category_alias')) ? old('category_alias') : ((($category) && ($category->category_alias)) ? $category->category_alias : '') }}">
                                        @if ($errors->has('category_alias'))
                                            <span class="text-danger help-block">{{ $errors->first('category_alias') }}</span>
                                        @endif
                                    </div>
                                </div>


                                <div class="row form-group categoryoptions"
                                     <?php if((@$category->category_parent_id == '0') || (empty($category))){ ?> style="display: none" <?php } ?> >
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Category Options</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="category_options" id="category_options"
                                                class="form-control category_options">
                                            <option value="">--Select Parent or SubCategory--</option>
                                            <option value="types" {{ (!empty(old('category_options')) && old('category_options')=='types')  ? 'selected' : ((($category) && ($category->category_options == 'types')) ? 'selected' : '') }}>
                                                Types
                                            </option>
                                            <option value="others" {{ (!empty(old('category_options')) && old('category_options')=='others')  ? 'selected' : ((($category) && ($category->category_options == 'others')) ? 'selected' : '') }}>
                                                Others
                                            </option>
                                        </select>
                                        @if ($errors->has('category_options'))
                                            <span class="text-danger help-block">{{ $errors->first('category_options') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Category Order</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="category_order" type="number" min="0" max="100"
                                               placeholder="Category order"
                                               name="category_order"
                                               value="{{ !empty(old('category_order')) ? old('category_order') : ((($category) && ($category->category_order)) ? $category->category_order : 0) }}">
                                        @if ($errors->has('category_order'))
                                            <span class="text-danger help-block">{{ $errors->first('category_order') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group categoryimage">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class="form-control-label">Category Image</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control {{ (($category) && ($category->category_image)) ? '' : 'categoryimage'}}"
                                               id="categoryimage" type="file" name="category_image"/>
                                        <small>Size: 1400/500</small>
                                        @if ($errors->has('category_image'))
                                            <span class="text-danger">{{ $errors->first('category_image') }}</span>
                                        @endif
                                        @if(($category) && ($category->category_image))
                                            <div class="imagediv{{ $category->category_id }}">
                                                <img width="150"
                                                     src="/uploads/categories/thumbs/{{$category->category_image}}"/>
                                                <a href="#" id="{{$category->category_id}}"
                                                   class="delete_category_image btn btn-danger btn-xs"
                                                   onclick="return confirm('Confirm delete?')"><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i>Delete </a>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Meta Title</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input class="form-control"
                                               id="category_meta_title"
                                               type="text"
                                               placeholder="Meta Title"
                                               name="category_meta_title"
                                               value="{{ !empty(old('category_meta_title')) ? old('category_meta_title') : ((($category) && ($category->category_meta_title)) ? $category->category_meta_title : '') }}">


                                        @if ($errors->has('category_meta_title'))
                                            <span class="text-danger help-block">{{ $errors->first('category_meta_title') }}</span>
                                        @endif
                                    </div>

                                </div>


                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Meta Keywords</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input class="form-control"
                                               id="category_meta_keywords"
                                               type="text"
                                               placeholder="Meta Keywords"
                                               name="category_meta_keywords"
                                               value="{{ !empty(old('category_meta_keywords')) ? old('category_meta_keywords') : ((($category) && ($category->category_meta_keywords)) ? $category->category_meta_keywords : '') }}">


                                        @if ($errors->has('category_meta_keywords'))
                                            <span class="text-danger help-block">{{ $errors->first('category_meta_keywords') }}</span>
                                        @endif
                                    </div>

                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Meta Description</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                    <textarea name="category_meta_description"
                                              id="category_meta_description"
                                              rows="5"
                                              placeholder="Meta Description"
                                              class="form-control">{!! !empty(old('category_meta_description')) ? old('category_meta_description') : ((($category) && ($category->category_meta_description)) ? $category->category_meta_description : '') !!}</textarea>
                                        @if ($errors->has('category_meta_description'))
                                            <span class="text-danger help-block">{{ $errors->first('category_meta_description') }}</span>
                                        @endif
                                    </div>

                                </div>


                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Status</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="category_status" id="category_status" class="form-control">
                                            <?php
                                            $status = allStatuses('general'); ?>
                                            @foreach($status as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('category_status')) && old('category_status')==$ks)  ? 'selected' : ((($category) && ($category->category_status == $ks)) ? 'selected' : '') }}
                                                >
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('category_status'))
                                            <span class="text-danger help-block">{{ $errors->first('category_status') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger">
                                        Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    @include('backend.categories.script')

@endsection