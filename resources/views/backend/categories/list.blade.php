@extends('backend.layout')
@section('title', $title)

@section('headerStyles')



@endsection

@section('content')


    @if(app('request')->input('search_category')!='')

        <ul class="breadcrumb ">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="">Categories</a>
            </li>


        </ul>

    @else
    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Categories'
              ),'Categories'
           ) !!}
    @endif

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif

                        <form method="GET" action="{{ route('categories') }}" accept-charset="UTF-8"
                              class="navbar-form navbar-right" role="search" autocomplete="off">
                            <input type="hidden" name="search" value="search">

                            <div class="row">

                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="category_name"
                                           placeholder="Enter Category Name to Search..."
                                           value="{{ request('category_name') }}">
                                </div>
                                &nbsp;&nbsp;

                                <div class="col-md-2">
                                    <select name="search_category" id="search_category"
                                            class="form-control">
                                        <option value="">All</option>
                                        <option value="0" {{ (app('request')->input('search_category')=='0')  ? 'selected' : ''}}>Parent Categories</option>
                                        <?php
                                        $categorysearch = getCategories(); ?>
                                        @foreach($categorysearch as $ks=>$s)
                                            <option value="{{ $ks }}" {{ (app('request')->input('search_category')==$ks)  ? 'selected' : ''}}>
                                                {{ $s }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>




                                <div class="col-md-1">
                                             <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                                </div>
                            </div>

                        </form>
                        <br/>


                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Categories</strong>
                            <a href="{{route('addNewCategories')}}" class="btn btn-primary" style="float:right;">+ Add
                                Categories
                            </a>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Category Name / SubCategory Name</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Options</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Products</th>
                                    <th scope="col">Shipped in</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($categories)>0)
                                    @foreach($categories as $item)
                                        <tr>
                                            <th scope="row">{{ $item->category_id }}</th>
                                            <td>{{ $item->category_name }}</td>
                                            <td>
                                                @if(isset($item->category_image))

                                                    <img width="100"
                                                         src="{{ '/uploads/categories/thumbs/'.$item->category_image }}"/>
                                                @else
                                                    ----
                                                @endif
                                            </td>
                                            <td>{{ (@$item->getCategoryName->category_name) ? @$item->getCategoryName->category_name:'--' }}</td>
                                            <td>{{ (@$item->category_options) ? @$item->category_options:'--' }}</td>
                                            <td>{{ allStatuses('general',$item->category_status) }}</td>
                                            <td> 0</td>
                                            <td>
                                                <?php
//                                                dump(categoryShipped($item->category_shipped))
                                                ?>

                                                {!! categoryShipped($item->category_shipped) !!}
                                            </td>
                                            <td>
                                                <div class="dropdown">
                                                    <a class="btn btn-outline-primary dropdown-toggle" href="#"
                                                       role="button"
                                                       data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-h"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item"
                                                           href="{{ route('addNewCategories',['id'=>$item->category_id]) }}"><i
                                                                    class="fa fa-pencil"></i> Edit</a>
                                                        <form method="POST" id="categories"
                                                              action="{{ route('categories') }}"
                                                              accept-charset="UTF-8" class="form-horizontal"
                                                              style="display:inline">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="category_id"
                                                                   value="{{ $item->category_id }}"/>
                                                            <button type="submit" class="dropdown-item"
                                                                    title="Delete Category"
                                                                    onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                <i
                                                                        class="fa fa-trash"></i> Delete
                                                            </button>

                                                        </form>

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7" align="center">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row px-3">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $categories->firstItem() }}
                        to {{ $categories->lastItem() }} of {{ $categories->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $categories->appends(['filters' => Request::get('filters'),'search' => Request::get('search'),'status' => Request::get('category_status')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

@endsection