<!-- Side Navbar -->
<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
            <!-- User Info-->
            <div class="sidenav-header-inner text-center">
                {{--<img src="/admin/images/avatar-7.jpg" alt="person" class="img-fluid rounded-circle"/>--}}
                <a class="navbar-brand" href="{{route('dashboard')}}"><img
                            src="/vendors/images/db/logothumb.svg" alt=""></a>
                <h2 class="h5">{!! Auth::guard('admin')->user()->name !!}</h2>
            </div>
            <!-- Small Brand information, appears on minimized sidebar-->
            <div class="sidenav-header-logo">
                <a href="{{route('dashboard')}}" class="brand-small text-center">
                    <strong>B</strong>
                    <strong class="text-primary">D</strong>
                </a>
            </div>
        </div>
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu">
            <h5 class="sidenav-heading">Main</h5>
            <ul id="side-main-menu" class="side-menu list-unstyled">
                <li>
                    <a href="{{route('home')}}" target="_blank">
                        <i class="icon-screen"> </i>
                        <span> Visit Site </span>
                    </a>
                </li>
                <li class="{{  $active_menu=='dashboard'?'active':'' }}">
                    <a href="{{route('dashboard')}}"> <i class="icon-home"></i> Dashboard </a>
                </li>
                <li class="{{  $active_menu=='banners'?'active':'' }}">
                    <a href="{{route('banners')}}">
                        <i class="icon-home"> </i>
                        Banners
                    </a>
                </li>
                <li class="{{  $active_menu=='service_banners'?'active':'' }}">
                    <a href="{{route('serviceBanners')}}"> <i class="icon-home"></i> Service Banners </a>
                </li>

                <li class="{{  $active_menu=='ammachethivanta'?'active':'' }}">
                    <a href="#ammachethivanta"
                       aria-expanded="{{  $active_menu=='ammachethivanta'?'true':'false' }}"
                       data-toggle="collapse">
                        <i class="icon-interface-windows"></i>
                        Amma Chethi Vanta
                    </a>

                    <ul id="ammachethivanta"
                        class="collapse  {{  $active_menu=='ammachethivanta'?'show':'' }}   list-unstyled ">
                        <li class="{{  $sub_active_menu=='ammachethivanta-list'?'active':'' }}">
                            <a href="{{route('ammachethivanta')}}">Products</a>
                        </li>
                        <li class="{{  $sub_active_menu=='ammachethivanta-orders'?'active':'' }}">
                            <a href="{{route('ammachethivantaorders')}}">Orders</a>
                        </li>
                    </ul>
                </li>

                <li class="{{  $active_menu=='blogs'?'active':'' }}">
                    <a href="{{route('blogs')}}"> <i class="icon-home"></i> Blogs </a>
                </li>
                <li class="{{  $active_menu=='coupons'?'active':'' }}">
                    <a href="{{route('coupons')}}"> <i class="icon-home"></i> Coupons </a>
                </li>
                <li class="{{  $active_menu=='categories'?'active':'' }}">
                    <a href="{{route('categories')}}"> <i class="icon-home"></i> Categories </a>
                </li>
                <li class="{{  $active_menu=='products'?'active':'' }}">
                    <a href="{{route('admin_products')}}"> <i class="icon-home"></i> Products </a>
                </li>
                <li class="{{  $active_menu=='countries'?'active':'' }}">
                    <a href="{{route('countries')}}"> <i class="icon-home"></i> Locations </a>
                </li>
                {{--<li>--}}
                {{--<a href="tables.html">--}}
                {{--<i class="icon-grid"></i>--}}
                {{--Tables--}}
                {{--</a>--}}
                {{--</li>--}}
                <li class="{{  $active_menu=='service'?'active':'' }}">
                    <a href="#exampledropdownDropdown" aria-expanded="{{  $active_menu=='service'?'true':'false' }}"
                       data-toggle="collapse">
                        <i class="icon-interface-windows"></i>
                        Services
                    </a>

                    <ul id="exampledropdownDropdown"
                        class="collapse  {{  $active_menu=='service'?'show':'' }}   list-unstyled ">



                        @if(count(serviceInfoData()))
                            @foreach(serviceInfoData() AS $serviceInfo)
                                <li class="{{  $sub_active_menu==$serviceInfo?'active':'' }}"><a
                                            href="{{route('admin_service', ['service'=>$serviceInfo])}}">
                                        {{ wordSort($serviceInfo) }}

                                    </a>
                                </li>
                            @endforeach

                        @endif



                    </ul>
                </li>

                <li class="{{  $active_menu=='users'?'active':'' }}"><a href="{{route('users')}}"> <i
                                class="icon-home"></i> Users </a></li>
                <li class="{{  $active_menu=='vendors'?'active':'' }}"><a href="{{route('vendorslist')}}"> <i
                                class="icon-home"></i> Vendors </a></li>
                <li class="{{  $active_menu=='orders'?'active':'' }}"><a href="{{route('adminorders')}}"> <i
                                class="icon-home"></i> Orders </a></li>
               <li class="{{  $active_menu=='reports'?'active':'' }}"><a href="{{route('adminreports')}}"> <i
                                class="icon-home"></i> Reports </a></li>
                <li class="{{  $active_menu=='subscriptions'?'active':'' }}"><a href="{{route('subscriptions')}}"> <i
                                class="icon-home"></i> Subscribers </a></li>
                <li class="{{  $active_menu=='product_enquiries'?'active':'' }}"><a
                            href="{{route('product_enquiries')}}"> <i
                                class="icon-home"></i> Product Enquiries </a></li>
                <li class="{{  $active_menu=='reviews'?'active':'' }}"><a
                            href="{{route('Reviews')}}"> <i
                                class="icon-home"></i> Reviews & Ratings </a></li>
                {{--<li>--}}
                {{--<a href="login.html">--}}
                {{--<i class="icon-interface-windows"></i>--}}
                {{--Login page--}}
                {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<a href="#">--}}
                {{--<i class="icon-mail"></i> Demo--}}
                {{--<div class="badge badge-warning">6 New</div>--}}
                {{--</a>--}}
                {{--</li>--}}
            </ul>
        </div>
        {{--<div class="admin-menu">--}}
        {{--<h5 class="sidenav-heading">Second menu</h5>--}}
        {{--<ul id="side-admin-menu" class="side-menu list-unstyled">--}}
        {{--<li><a href="#"> <i class="icon-screen"> </i>Demo</a></li>--}}
        {{--<li><a href="#"> <i class="icon-flask"> </i>Demo--}}
        {{--<div class="badge badge-info">Special</div>--}}
        {{--</a></li>--}}
        {{--<li><a href=""> <i class="icon-flask"> </i>Demo</a></li>--}}
        {{--<li><a href=""> <i class="icon-picture"> </i>Demo</a></li>--}}
        {{--</ul>--}}
        {{--</div>--}}
    </div>
</nav>