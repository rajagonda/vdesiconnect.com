<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Bedrooms</label>
    </div>
    <div class="col-12 col-md-9">
        <select name="options[po_bedrooms]" id="po_bedrooms" class="form-control">
            <?php
            $bedrooms = allOptions('bedrooms'); ?>
            <option value="">Select</option>
            @foreach($bedrooms as $ks=>$s)
                <option value="{{ $ks }}" {{ (!empty(old('po_bedrooms')) && old('po_bedrooms')==$ks)  ? 'selected' : ((($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_bedrooms == $ks)) ? 'selected' : '') }}
                >
                    {{ $s }}
                </option>
            @endforeach
        </select>
        @if ($errors->has('options[po_bedrooms]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_bedrooms]') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Bathrooms</label>
    </div>
    <div class="col-12 col-md-9">
        <select name="options[po_bathrooms]" id="po_bathrooms" class="form-control">
            <?php
            $bathrooms = allOptions('bathrooms'); ?>
            <option value="">Select</option>
            @foreach($bathrooms as $ks=>$s)
                <option value="{{ $ks }}" {{ (!empty(old('po_bathrooms')) && old('po_bathrooms')==$ks)  ? 'selected' : ((($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_bathrooms == $ks)) ? 'selected' : '') }}
                >
                    {{ $s }}
                </option>
            @endforeach
        </select>
        @if ($errors->has('options[po_bathrooms]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_bathrooms]') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Balcony</label>
    </div>
    <div class="col-12 col-md-9">
        <select name="options[po_balcony]" id="po_balcony" class="form-control">
            <?php
            $balcony = allOptions('balcony'); ?>
            <option value="">Select</option>
            @foreach($balcony as $ks=>$s)
                <option value="{{ $ks }}" {{ (!empty(old('po_balcony')) && old('po_balcony')==$ks)  ? 'selected' : ((($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_balcony == $ks)) ? 'selected' : '') }}
                >
                    {{ $s }}
                </option>
            @endforeach
        </select>
        @if ($errors->has('options[po_balcony]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_balcony]') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Other Rooms</label>
    </div>
    <div class="col-12 col-md-9">

        @php

            if (($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_other_rooms)){
            $otherRooms=explode(',',$property->propertyOptions[0]->po_other_rooms);
            }else{
            $otherRooms='';
            }
        @endphp

        <select name="options[po_other_rooms][]" id="po_other_rooms"
                class="form-control" multiple>
            <?php
            $rooms = allOptions('other_rooms'); ?>
            @foreach($rooms as $ks=>$s)
                <option value="{{ $ks }}" {{ (!empty(old('po_other_rooms')) && old('po_other_rooms')==$ks)  ? 'selected' : ((($property) && ($otherRooms) && (in_array($ks,$otherRooms))) ? 'selected' : '') }}
                >
                    {{ $s }}
                </option>
            @endforeach
        </select>
        @if ($errors->has('options[po_other_rooms]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_other_rooms]') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Total Floors</label>
    </div>
    <div class="col-12 col-md-9">
        <select name="options[po_total_floors]" id="po_total_floors"
                class="form-control">
            <?php
            $totlarooms = allOptions('total_floors'); ?>
            @foreach($totlarooms as $ks=>$s)
                <option value="{{ $ks }}" {{ (!empty(old('po_total_floors')) && old('po_total_floors')==$ks)  ? 'selected' : ((($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_total_floors == $ks)) ? 'selected' : '') }}
                >
                    {{ $s }}
                </option>
            @endforeach
        </select>
        @if ($errors->has('options[po_other_rooms]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_other_rooms]') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Property on Floor</label>
    </div>
    <div class="col-12 col-md-9">
        <select name="options[po_property_on_floor]" id="po_property_on_floor"
                class="form-control">
            <?php
            $ponfloor = allOptions('property_on_floor'); ?>
            @foreach($ponfloor as $ks=>$s)
                <option value="{{ $ks }}" {{ (!empty(old('po_property_on_floor')) && old('po_property_on_floor')==$ks)  ? 'selected' : ((($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_property_on_floor == $ks)) ? 'selected' : '') }}
                >
                    {{ $s }}
                </option>
            @endforeach
        </select>
        @if ($errors->has('options[po_property_on_floor]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_property_on_floor]') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Reserved Parking</label>
    </div>
    <div class="col-12 col-md-9">

        @php

            if (($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_reserved_on_parking)){
            $parking=explode(',',$property->propertyOptions[0]->po_reserved_on_parking);
            }else{
            $parking='';
            }
        @endphp

        <select name="options[po_reserved_on_parking][]" id="po_reserved_on_parking"
                class="form-control" multiple="multiple">
            <?php
            $resered_parking = allOptions('resered_parking'); ?>
            @foreach($resered_parking as $ks=>$s)
                <option value="{{ $ks }}" {{ (!empty(old('po_reserved_on_parking')) && old('po_reserved_on_parking')==$ks)  ? 'selected' : ((($parking) && (in_array($ks,$parking))) ? 'selected' : '') }}
                >
                    {{ $s }}
                </option>
            @endforeach
        </select>
        @if ($errors->has('options[po_reserved_on_parking]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_reserved_on_parking]') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Availability</label>
    </div>
    <div class="col-12 col-md-9">
        <select name="options[po_availability]" id="po_availability"
                class="form-control">
            <?php
            $availability = allOptions('availability'); ?>
            @foreach($availability as $ks=>$s)
                <option value="{{ $ks }}" {{ (!empty(old('po_availability')) && old('po_availability')==$ks)  ? 'selected' : ((($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_availability == $ks)) ? 'selected' : '') }}
                >
                    {{ $s }}
                </option>
            @endforeach
        </select>
        @if ($errors->has('options[po_availability]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_availability]') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Possession By</label>
    </div>
    <div class="col-12 col-md-9">
        <select name="options[po_possession_by]" id="po_possession_by"
                class="form-control">
            <?php
            $possession_by = allOptions('possession_by'); ?>
            @foreach($possession_by as $ks=>$s)
                <option value="{{ $ks }}" {{ (!empty(old('po_possession_by')) && old('po_possession_by')==$ks)  ? 'selected' : ((($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_possession_by == $ks)) ? 'selected' : '') }}
                >
                    {{ $s }}
                </option>
            @endforeach
        </select>
        @if ($errors->has('options[po_possession_by]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_possession_by]') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Owner Ship</label>
    </div>
    <div class="col-12 col-md-9">
        <select name="options[po_owner_ship]" id="po_owner_ship"
                class="form-control">
            <?php
            $owner_ship = allOptions('owner_ship'); ?>
            @foreach($owner_ship as $ks=>$s)
                <option value="{{ $ks }}" {{ (!empty(old('po_owner_ship')) && old('po_owner_ship')==$ks)  ? 'selected' : ((($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_owner_ship == $ks)) ? 'selected' : '') }}
                >
                    {{ $s }}
                </option>
            @endforeach
        </select>
        @if ($errors->has('options[po_owner_ship]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_owner_ship]') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Expected Price</label>
    </div>
    <div class="col-12 col-md-9">

        <input class="form-control" id="po_expected_price" type="text"
               placeholder="Location"
               name="options[po_expected_price]"
               value="{{ !empty(old('po_expected_price')) ? old('po_expected_price') : ((($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_expected_price)) ? $property->propertyOptions[0]->po_expected_price : '') }}">
        @if ($errors->has('options[po_expected_price]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_expected_price]') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">All Inclusive
            price</label>
    </div>
    <div class="col-12 col-md-9">

        <input class="form-control" id="po_inclusive_price" type="checkbox"

               name="options[po_inclusive_price]" {{ !empty(old('po_inclusive_price')) ? 'checked' : ((($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_inclusive_price)) ? 'checked' : '') }} >
        @if ($errors->has('options[po_inclusive_price]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_inclusive_price]') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Price Negotiable</label>
    </div>
    <div class="col-12 col-md-9">

        <input class="form-control" id="po_price_negotiable" type="checkbox"

               name="options[po_price_negotiable]" {{ !empty(old('po_price_negotiable')) ? 'checked' : ((($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_price_negotiable)) ? 'checked' : '') }} >
        @if ($errors->has('options[po_price_negotiable]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_price_negotiable]') }}</span>
        @endif
    </div>
</div>


<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Price per Sq.yards</label>
    </div>
    <div class="col-12 col-md-9">

        <input class="form-control" id="po_price_per_sqyards" type="text"
               placeholder="Price per Sq.yards"
               name="options[po_price_per_sqyards]"
               value="{{ !empty(old('po_price_per_sqyards')) ? old('po_price_per_sqyards') : ((($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_price_per_sqyards)) ? $property->propertyOptions[0]->po_price_per_sqyards : '') }}">
        @if ($errors->has('options[po_price_per_sqyards]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_price_per_sqyards]') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Advance Booking
            Amount</label>
    </div>
    <div class="col-12 col-md-9">

        <input class="form-control" id="po_advance_booking_amount" type="text"
               placeholder="Advance Booking Amount"
               name="options[po_advance_booking_amount]"
               value="{{ !empty(old('po_advance_booking_amount')) ? old('po_advance_booking_amount') : ((($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_advance_booking_amount)) ? $property->propertyOptions[0]->po_advance_booking_amount : '') }}">
        @if ($errors->has('options[po_advance_booking_amount]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_advance_booking_amount]') }}</span>
        @endif
    </div>
</div>


<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Maintenance</label>
    </div>
    <div class="col-12 col-md-9">
        <select name="options[po_maintenance]" id="po_maintenance"
                class="form-control">
            <?php
            $maintenance = allOptions('maintenance'); ?>
            @foreach($maintenance as $ks=>$s)
                <option value="{{ $ks }}" {{ (!empty(old('po_maintenance')) && old('po_maintenance')==$ks)  ? 'selected' : ((($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_maintenance == $ks)) ? 'selected' : '') }}
                >
                    {{ $s }}
                </option>
            @endforeach
        </select>
        @if ($errors->has('options[po_maintenance]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_maintenance]') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Amenties</label>
    </div>
    <div class="col-12 col-md-9">

        @php

            if (($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_amenties)){
            $amenities=explode(',',$property->propertyOptions[0]->po_amenties);
            }else{
            $amenities='';
            }
        @endphp

        <select name="options[po_amenties][]" id="po_amenties"
                class="form-control" multiple>
            <?php
            $amenties = allOptions('amenties'); ?>
            @foreach($amenties as $ks=>$s)
                <option value="{{ $ks }}" {{ (!empty(old('po_amenties')) && old('po_amenties')==$ks)  ? 'selected' : ((($amenities) && (in_array($ks,$amenities))) ? 'selected' : '') }}
                >
                    {{ $s }}
                </option>
            @endforeach
        </select>
        @if ($errors->has('options[po_amenties]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_amenties]') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Power Backup</label>
    </div>
    <div class="col-12 col-md-9">
        <select name="options[po_power_backup]" id="po_power_backup"
                class="form-control">
            <?php
            $power_backup = allOptions('yesno'); ?>
            @foreach($power_backup as $ks=>$s)
                <option value="{{ $ks }}" {{ (!empty(old('po_power_backup')) && old('po_power_backup')==$ks)  ? 'selected' : ((($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_power_backup == $ks)) ? 'selected' : '') }}
                >
                    {{ $s }}
                </option>
            @endforeach
        </select>
        @if ($errors->has('options[po_power_backup]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_power_backup]') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Water Source</label>
    </div>
    <div class="col-12 col-md-9">
        <select name="options[po_water_source]" id="po_water_source"
                class="form-control">
            <?php
            $water_source = allOptions('water_source'); ?>
            @foreach($water_source as $ks=>$s)
                <option value="{{ $ks }}" {{ (!empty(old('po_water_source')) && old('po_water_source')==$ks)  ? 'selected' : ((($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_water_source == $ks)) ? 'selected' : '') }}
                >
                    {{ $s }}
                </option>
            @endforeach
        </select>
        @if ($errors->has('options[po_water_source]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_water_source]') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Property Facing</label>
    </div>
    <div class="col-12 col-md-9">
        <select name="options[po_property_facing]" id="po_property_facing"
                class="form-control">
            <?php
            $property_facing = allOptions('property_facing'); ?>
            @foreach($property_facing as $ks=>$s)
                <option value="{{ $ks }}" {{ (!empty(old('po_property_facing')) && old('po_property_facing')==$ks)  ? 'selected' : ((($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_property_facing == $ks)) ? 'selected' : '') }}
                >
                    {{ $s }}
                </option>
            @endforeach
        </select>
        @if ($errors->has('options[po_property_facing]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_property_facing]') }}</span>
        @endif
    </div>
</div>

<div class="row form-group">
    <div class="col col-md-3">
        <label for="hf-email" class=" form-control-label">Furniture</label>
    </div>
    <div class="col-12 col-md-9">
        <select name="options[po_furniture]" id="po_furniture"
                class="form-control">
            <?php
            $furniture = allOptions('furniture'); ?>
            @foreach($furniture as $ks=>$s)
                <option value="{{ $ks }}" {{ (!empty(old('po_furniture')) && old('po_furniture')==$ks)  ? 'selected' : ((($property) && (count($property->propertyOptions)==1) && ($property->propertyOptions[0]->po_furniture == $ks)) ? 'selected' : '') }}
                >
                    {{ $s }}
                </option>
            @endforeach
        </select>
        @if ($errors->has('options[po_furniture]'))
            <span class="text-danger help-block">{{ $errors->first('options[po_furniture]') }}</span>
        @endif
    </div>
</div>