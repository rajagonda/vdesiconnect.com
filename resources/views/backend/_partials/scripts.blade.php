<!-- JavaScript files-->
<script src="/admin/js/adminScripts.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>--}}


<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>


<script>
    // var $ = jQuery.noConflict();
    $('.datepicker').datepicker({
        changeMonth: true,
        changeYear: true,
        format: 'dd-mm-yyyy'
    });

    $('.datepickerFront').each(function () {
        var dateId = $(this).attr('id')

        if (dateId) {
            $('#' + dateId).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date()
            });
        } else {
            $(this).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                minDate: new Date()
            });
        }


    });


    // $('.datepickerFront').each(function(){
    //     $(this).datepicker({
    //         changeMonth: true,
    //         changeYear: true,
    //         dateFormat: 'yy-mm-dd',
    //         minDate: new Date()
    //     });
    // });


</script>

