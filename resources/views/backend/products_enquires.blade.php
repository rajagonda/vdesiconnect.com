@extends('backend.layout')
@section('title', $title)

@section('headerStyles')



@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'products_enquires'
              ),'products_enquires'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">


                    {{ laravelReturnMessageShow() }}

                    <?php
                    //                    dump($product_enquiries);
                    ?>

                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Orders</strong>

                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">name</th>
                                    <th scope="col">email Number</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">message</th>
                                    <th scope="col">product id</th>
                                    <th scope="col">user id</th>
                                    <th scope="col">otp</th>
                                    <th scope="col">IP Address</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">create date</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($product_enquiries)>0)
                                    @foreach($product_enquiries as $item)

                                        <tr>
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>
                                                {{ $item->se_name }}
                                            </td>
                                            <td>{{ $item->se_email }}</td>
                                            <td>
                                                {{ $item->se_phone }}</td>
                                            <td>

                                                {{ $item->se_message }}
                                            </td>
                                            <td>
                                                {{--                                                {{ $item->se_product_id}}--}}
                                                {{ isset($item->getProduct->p_name)?$item->getProduct->p_name:'' }}
                                                <?php
                                                //                                                dump($item->getProduct->p_name);
                                                ?>

                                            </td>
                                            <td>{{ $item->se_user_id }}</td>
                                            <td>{{ $item->se_otp }}</td>
                                            <td>{{ $item->se_ip_address }}</td>
                                            <td>
                                                @if ($item->se_status=='')
                                                    New
                                                @elseif($item->se_status==2)
                                                    Unavalable
                                                @elseif($item->se_status==1)
                                                    Avalable
                                                @endif

                                            </td>
                                            <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i:s')}}</td>
                                            <td>

                                                <!-- Button to Open the Modal -->
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                        data-target="#view_{{$item->se_id}}">
                                                    <i class="fa fa-eye"></i> View
                                                </button>

                                                <form method="POST" id="enquiryDelete"
                                                      action="{{ route('enquiryDelete') }}"
                                                      accept-charset="UTF-8" class="form-horizontal"
                                                      style="display:inline">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="se_id"
                                                           value="{{ $item->se_id }}"/>
                                                    <button type="submit" class="btn btn-primary"
                                                            title="Delete Enquiry"
                                                            onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                        <i
                                                                class="fa fa-trash"></i> Delete
                                                    </button>

                                                </form>


                                                <!-- The Modal -->
                                                <div class="modal" id="view_{{$item->se_id}}">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">

                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Modal Heading</h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                            </div>

                                                            <!-- Modal body -->
                                                            <div class="modal-body">


                                                                <table class="table table-bordered table-hover">
                                                                    <tr>
                                                                        <td>Name</td>
                                                                        <td>{{ $item->se_name }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Email</td>
                                                                        <td>{{ $item->se_email }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Phone</td>
                                                                        <td>{{ $item->se_phone }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Product</td>
                                                                        <td>
                                                                            {{ isset($item->getProduct->p_name)?$item->getProduct->p_name:'' }}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>User</td>
                                                                        <td>{{ $item->se_user_id }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>OTP</td>
                                                                        <td>{{ $item->se_otp }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Message</td>
                                                                        <td>{{ $item->se_message }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Create By</td>
                                                                        <td>
                                                                            {{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i:s')}}
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <form action="" method="post">
                                                                            {{ csrf_field() }}
                                                                            <input type="hidden" name="se_id"
                                                                                   value="{{ $item->se_id }}"/>


                                                                            <select class="form-control statuschange" name="action" id="{{ $item->se_id }}">
                                                                                <option {{ ($item->se_status=='')?'selected':'' }}  value="">
                                                                                    New
                                                                                </option>
                                                                                <option {{ ($item->se_status==1)?'selected':'' }}  value="1">
                                                                                    Avalable
                                                                                </option>
                                                                                <option {{ ($item->se_status==2)?'selected':'' }}   value="2">
                                                                                    Unavalable
                                                                                </option>
                                                                            </select>




                                                                            <div class="form-group adminreply{{ $item->se_id }}" style="display: none">
                                                                                <label>Admin Reply :</label>
                                                                                <textarea class="form-control" name="se_reply_msg">{{ $item->se_reply_msg }}</textarea>
                                                                            </div>






                                                                            <button class="btn btn-primary"
                                                                                    type="submit">Submit
                                                                            </button>
                                                                        </form>
                                                                    </div>
                                                                    <div class="col-md-6"></div>
                                                                </div>

                                                            </div>

                                                            <!-- Modal footer -->
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>


                                            </td>


                                        </tr>



                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="11">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row px-3">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $product_enquiries->firstItem() }}
                        to {{ $product_enquiries->lastItem() }} of {{ $product_enquiries->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $product_enquiries->appends(['filters' => Request::get('filters'),'search' => Request::get('search')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')
<script>




    $(".statuschange").change(function () {
        var idval=$(this).attr('id');
        var status = this.value;
        if(status=='2'){
             $('.adminreply'+idval).show();
        }else{
            $('.adminreply'+idval).hide();
        }

    });
</script>


@endsection
