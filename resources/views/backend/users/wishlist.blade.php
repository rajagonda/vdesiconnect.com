@extends('backend.layout')
@section('title', $title)

@section('headerStyles')



@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              'users'=>'Users',
              ''=>'User Orders'
              ),'User Orders'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">


                <div class="col-lg-3">
                    @include('backend.users.userinfo')
                </div>
                <div class="col-lg-9">

                    @include('backend.users.nav')

                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">User Orders</strong>

                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Product Name</th>

                                    <th scope="col">
                                        availability
                                    </th>
                                    <th scope="col">Added On</th>
                                    <th scope="col">View More</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($wishlist)>0)
                                    @foreach($wishlist as $item)

                                        <?php
                                        //dd($item->getProduct->p_availability);

                                        //                                        $item->getProduct->p_availability
                                        if (isset($item->getProduct->p_availability) && $item->getProduct->p_availability == 'in_stock') {

                                            $producturl = route('productPage', ['category' => $item->getProduct->getCategory->category_alias, 'product' => $item->getProduct->p_alias]);
                                        } else {
                                            $producturl = '';
                                        }

                                        ?>

                                        @if(isset($item->getProduct))

                                            <tr>
                                                <th scope="row">{{ $loop->iteration }}</th>
                                                <th scope="row">

                                                    @if(count($item->getProduct->productImages)>0)
                                                        <img class="img-fluid" width="60" src="/uploads/products/thumbs/{{ $item->getProduct->productImages[0]->pi_image_name }}">
                                                    @endif
                                                </th>
                                                <td>

                                                    <?php
                                                    //                                                dump($item);
                                                    ?>
                                                    <a href="{{ $producturl }}" target="_blank">
                                                        {{ $item->getProduct->p_name }}
                                                    </a>

                                                </td>

                                                <td>

                                                    {{ $item->getProduct->p_availability }}
                                                </td>
                                                <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i:s')}}</td>
                                                <td>

                                                    <a href="#" data-toggle="modal"
                                                       data-target="#productDetails{{ $item->ua_id }}"
                                                       class="text-uppercase pr-3 btn btn-default btn-sm">
                                                        <i class="far fa-eye"></i> View more
                                                    </a>

                                                    <!-- Modal -->
                                                    <div id="productDetails{{ $item->ua_id }}" class="modal fade"
                                                         role="dialog">
                                                        <div class="modal-dialog">

                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <!-- Modal Header -->
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title">
                                                                    </h4>
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal">
                                                                        &times;
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="row">
                                                                        <table class="table table-striped">
                                                                            <tr>
                                                                                <td>
                                                                                    @if(count($item->getProduct->productImages)>0)
                                                                                        <img src="/uploads/products/thumbs/{{ $item->getProduct->productImages[0]->pi_image_name }}">
                                                                                    @endif


                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Product Name
                                                                                    :
                                                                                    {{ $item->getProduct->p_name }}
                                                                                </td>
                                                                            </tr>

                                                                            <tr>

                                                                                <td>Availability
                                                                                    : {{ $item->getProduct->p_availability }}</td>
                                                                            </tr>

                                                                            <tr>

                                                                                <a class="btn btn-danger"
                                                                                   target="_blank"
                                                                                   href="{{ $producturl }}">
                                                                                    View
                                                                                </a>
                                                                            </tr>

                                                                        </table>
                                                                    </div>

                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Close
                                                                    </button>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </td>

                                            </tr>
                                        @endif


                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10">
                            <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                                 aria-live="polite">Showing {{ $wishlist->firstItem() }}
                                to {{ $wishlist->lastItem() }} of {{ $wishlist->total() }} entries
                            </div>
                        </div>
                        <div class="col-md-1 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                                {!! $wishlist->appends(['filters' => Request::get('filters'),'search' => Request::get('search')])->render() !!}
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

@endsection