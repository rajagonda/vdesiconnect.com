<ul class="nav nav-tabs" id="myTab" role="tablist">

    @if (!empty($product_id))

        <li class="nav-item">
            <a href="{{ route('addNewProducts',['id'=>$product_id]) }}" class="nav-link @if ($sub_active_menu=='manage-products') active @endif">Product Information</a>
        </li>

        <li class="nav-item">
            <a href="{{ route('addProductImages',['id'=>$product_id]) }}"
               class="nav-link @if ($sub_active_menu=='products-images') active @endif">Product Images</a>
        </li>
        <li class="nav-item">
            <a href="{{ route('productExtraInfo',['id'=>$product_id]) }}" class="nav-link @if ($sub_active_menu=='products-extra-info') active @endif">Product Variations</a>
        </li>

    @else

        <li class="nav-item">
            <a href="{{ route('addNewProducts') }}" class="nav-link @if ($sub_active_menu=='manage-products') active @endif">Product Info</a>
        </li>
        <li class="nav-item">
            <a href="{{ route('sellerImportListings') }}" class="nav-link @if ($sub_active_menu=='import-products') active @endif">Products Import</a>
        </li>
    @endif
</ul>