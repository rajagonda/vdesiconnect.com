@extends('backend.layout')


@section('content')




    @if ($product->p_id!='')
        <ul class="breadcrumb ">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('admin_products')}}">Products</a>
            </li>
            @if(isset($product->getCategory->category_name))
                <li class="breadcrumb-item">
                    <a href="{{url('admin/products?search=search&product_name=&search_category='.$product->getCategory->category_id.'&search_vendor=&p_status=')}}">{{$product->getCategory->category_name}}</a>
                </li>
            @endif
            <li class="breadcrumb-item">
                <a href="javascript:void(0)">{{$product->p_name}}</a>
            </li>


        </ul>
    @else
        {!! getBreadcrumbs(
                     array(
                     'dashboard'=>'Home',
                     'admin_products'=>'Products',
                     ''=>'View'
                     ),'View Product'
                  ) !!}
    @endif




    <!-- dashboard main -->


        <!-- page content -->
        <div class="page-content d-flex align-items-stretch">
            <!-- content inner -->
            <div class="content-inner">
                <!-- page header -->
                <div class="page-header">
                    <div class="container-fluid">
                       <div class="row pt-3">
                           <div class="col-lg-6">
                               <h2 class="">{{$product->p_name}}</h2>
                           </div>
                           <div class="col-lg-6">
                               <input type="button" value="Edit Product" class="btn btn-success"
                                      onclick="window.location.href='{{route('addNewProducts',['id'=>$product->p_id])}}'">
                               <input type="button" value="Back to listing" class="btn btn-success"
                                      onclick="window.location.href='{{route('admin_products')}}'">
                           </div>
                       </div>
                    </div>
                </div>
                <!--/ page header -->
                <!-- Breadcrumb-->

                <!--/ Breadcrumb-->
                <!-- Forms Section-->
                <!-- page content main-->
                <div class="content-main p-3">
                    <!-- container-fluid-->
                    <div class="container-fluid">
                        <!-- row -->
                        <div class="row">
                            <!-- col left -->
                            <div class="col-lg-6">
                                <!-- card -->
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="border-bottom py-3">Product Details</h5>
                                        <!-- row -->

                                        <div class="row py-2">
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Product Name</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <p class="text-justify">{{$product->p_name}}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>

                                        <div class="row py-2">
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Product Overview</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <p class="text-justify">{!! $product->p_overview !!}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col -->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Image Galleery</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-9">
                                                <!-- row image gallery -->
                                                <div class="row images-listview">
                                                    @if(count($product_images)>0)
                                                        @foreach($product_images as $item)
                                                            <div class="col-md-2 px-1 position-relative imagediv_{{ $item->pi_id }}">
                                                                <a data-fancybox="gallery" href="/uploads/products/{{ $item->pi_image_name }}">
                                                                    <img class="img-fluid imagecover position-relative"
                                                                         height="150"
                                                                         src="/uploads/products/thumbs/{{ $item->pi_image_name }}"/>
                                                                </a>
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <div class="col-md-2">
                                                            No Images found
                                                        </div>
                                                    @endif
                                                </div>


                                                <!--/ row iamge gallery -->
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="border-bottom py-3">Product Specifications</h5>
                                        <!-- row -->

                                        <div class="row py-2">

                                            <div class="col-lg-12">
                                                <p class="text-justify"> <p>{!!  $product->p_specifications !!}</p></p>
                                            </div>
                                            <!--/ col -->
                                        </div>

                                    </div>
                                </div>
                                <!--/ card -->

                                <!-- card -->
                                <div class="card">
                                    <!-- card body -->
                                    <div class="card-body">
                                        <h5 class="border-bottom py-3">Product SKUs</h5>

                                        <table class="table table-breakpoint">
                                            <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Type</th>
                                                <th>Option</th>
                                                <th>Value</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(isset($product->productSKUs))
                                                @if(count($product->productSKUs)>0)
                                                    @foreach($product->productSKUs as $options)
                                                        <tr>
                                                            <td>{{ $loop->iteration }}</td>
                                                            <td>{{ $options->sku_type }}</td>
                                                            <td>{{ $options->sku_option }}</td>
                                                            <td> {!! currencySymbol('INR') !!} {{ $options->sku_vendor_price }}</td>

                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="4">No SKUs Found</td>

                                                    </tr>
                                                @endif
                                            @else
                                                <tr>
                                                    <td colspan="4">No SKUs Found</td>

                                                </tr>
                                            @endif
                                            </tbody>

                                        </table>


                                    </div>
                                    <!--/ card body -->
                                </div>
                                <!--/ card -->

                            </div>
                            <!--/ col left -->
                            <!-- col right -->
                            <div class="col-lg-6">
                                <!-- card -->
                                <div class="card">
                                    <!-- card body -->
                                    <div class="card-body">
                                        <h5 class="border-bottom py-3">Product primary Details</h5>

                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Product Type</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p>{{ (isset($product->getSubCategory->category_name))? $product->getSubCategory->category_name:'' }}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Product Spl</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p>{{ (isset($product->getSplCat->category_name))? $product->getSplCat->category_name:'' }}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2">


                                            @switch($product->p_cat_id)
                                                @case(1)
                                                <div class="col-lg-3 form-control-label">Dishtype</div>
                                                <div class="col-lg-9"><p>{{$product->p_dishtype}}</p></div>

                                                <div class="col-lg-3 form-control-label">Weight</div>
                                                <div class="col-lg-9">
                                                    <p> {{$product->p_weight}} {{$product->p_weight_type}}</p></div>

                                                @break
                                                @case(2)

                                                <div class="col-lg-3 form-control-label">Color</div>
                                                <div class="col-lg-9"><p>{{$product->p_color}}</p></div>

                                                @break
                                                @case(3)

                                                @break
                                                @case(4)

                                                <div class="col-lg-3 form-control-label">Weight</div>
                                                <div class="col-lg-9">
                                                    <p>{{$product->p_weight}} {{$product->p_weight_type}}</p></div>

                                                @break
                                                @case(5)

                                                <div class="col-lg-3 form-control-label">Weight</div>
                                                <div class="col-lg-9">
                                                    <p>{{$product->p_weight}} {{$product->p_weight_type}}</p></div>

                                                @break
                                                @case(6)
                                                <div class="col-lg-3 form-control-label">Weight</div>
                                                <div class="col-lg-9">
                                                    <p>{{$product->p_weight}} {{$product->p_weight_type}}</p></div>
                                                @break
                                                @case(7)
                                                7
                                                @break
                                                @case(8)
                                                8
                                                @break
                                            @endswitch

                                        </div>
                                        <!--/ row -->


                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Availability</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p>{{ ($product->p_availability=='in_stock') ? 'In Stock':'Out of Stock'}}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->

                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Eggless / Egg</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p>{{$product->p_dishtype}}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->




                                        <!-- row -->
                                        <div class="row py-2">
                                            <!-- col 3-->
                                            <div class="col-lg-3 form-control-label">
                                                <p>Quality Information</p>
                                            </div>
                                            <!--/ col -->
                                            <!-- col 3-->
                                            <div class="col-lg-9">
                                                <p>{!!  $product->p_quality_care_info !!}</p>
                                            </div>
                                            <!--/ col -->
                                        </div>
                                        <!--/ row -->


                                    </div>
                                    <!--/ card body -->
                                </div>
                                <!--/ card -->
                            </div>



                            <!--/ col right -->


                        </div>
                        <!--/ row -->
                        <div class="row">
                            <!-- col right -->
                            <div class="col-md-6">

                            </div>
                            <!--/ col right -->
                        </div>


                    </div>
                    <!-- container - fluid -->
                </div>
                <!--/ page content main-->
            </div>
            <!--/ content inner -->
        </div>
        <!--/ page content -->

    <!--/ dashboard main -->

@endsection



