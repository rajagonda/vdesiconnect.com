{!! javascriptFunctions() !!}

<script>

        <?php
        $imagelimits = 5;
        if (isset($product_images) && count($product_images) < 6) {
            $currentImages = count($product_images);
            $imagelimits = $imagelimits - $currentImages;
        }
        ?>
    var imagelimit = '{{ $imagelimits}}';
    imageUploadValidation('#productimage', imagelimit);


    var i = 1;
        <?php
        if($product){ ?>
    var i = {{ count($product->productSKUs) }}
            ++i;

    <?php  } ?>


    function validateSKU() {

        $('.sku_vendor_price').each(function () {
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please Eneter Price",
                }
            });
        });

        $('.sku_option').each(function () {
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please Eneter Optionm",
                }
            });
        });
        $('.sku_type').each(function () {
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Please Select SKU Type",
                }
            });
        });


    }


    $("#add").click(function () {

        ++i;

        $("#dynamicTable").append('<tr>' +
            '<td class="form-group">' +
            '<input type="hidden" name="product_options[' + i + '][for]" value="new"/>' +

            '<input type="hidden" name="product_options[' + i + '][sku_id]" value=""/>' +
            '<select name="product_options[' + i + '][sku_type]" class="form-control sku_type">' +

            '@foreach(skuTypes() AS $skuTypes)' +
            '<option>{{$skuTypes}}</option>' +
            '@endforeach' +
            '</select>\n' +
            '</td>' +
            '<td class="form-group">' +
            '<input type="text" name="product_options[' + i + '][sku_option]" placeholder="Ex: 2kg, 3kg, 5flowers, 5ltrs" class="form-control sku_option" />' +
            '</td>' +
            '<td class="form-group">' +
            '<div class="input-group">' +
            '<div class="input-group-prepend">' +
            '<div class="input-group-text">' +
            '  {!! currencySymbol("INR",'test')  !!}' +
            '</div>' +
            '</div>' +
            '<input type="text" name="product_options[' + i + '][sku_vendor_price]" placeholder="Enter Value or Price" class="form-control sku_vendor_price" />' +

            '</div>' +
            '</td>' +
            '<td>' +
            '<button type="button" class="btn btn-danger remove-tr">Remove</button>' +
            '</td>' +
            '</tr>');

        validateSKU()
    });

    $(document).on('click', '.remove-tr', function () {

        var confirmMsg = confirm("Are you sure want to remove?");
        if (confirmMsg == true) {

            $(this).parents('tr').remove();
            var id = $(this).closest('tr').prop('id');
            if (id != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{ route('seller_remove_options') }}',
                    type: 'POST',
                    data: {'id': id},
                    success: function (response) {
                    }
                });
            }


        } else {
            // txt = "You pressed Cancel!";
        }


    });


    function catOptions(catID) {
        if (catID == 1) {  //cake
            $('.itemspecial').html('Flavour');
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').hide();
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').find(":input").attr("disabled", true);
            $('#cake').show();
            $('#cake').find(":input").attr("disabled", false);


        } else if (catID == 2) {  //flowers
            $('.itemspecial').html('Occation');
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').hide();
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').find(":input").attr("disabled", true);
            $('#flowers').show();
            $('#flowers').find(":input").attr("disabled", false);


        } else if (catID == 3) { //Gifts
            $('.itemspecial').html('Recipient');
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').hide();
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').find(":input").attr("disabled", true);
            $('#gifts').show();
            $('#gifts').find(":input").attr("disabled", false);

        } else if (catID == 4) { // chacolates
            $('.itemspecial').html('Recipient');
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').hide();
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').find(":input").attr("disabled", true);
            $('#chocolates').show();
            $('#chocolates').find(":input").attr("disabled", false);
        } else if (catID == 5) { // jweleey
            $('.itemspecial').html('Recipient');
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').hide();
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').find(":input").attr("disabled", true);
            $('#jewllary').show();
            $('#jewllary').find(":input").attr("disabled", false);

        } else if (catID == 6) {  // millets
            $('.itemspecial').html('Product Name');
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').hide();
            $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').find(":input").attr("disabled", true);
            $('#millets').show();
            $('#millets').find(":input").attr("disabled", false);
        }

    }


    $(function () {

        $('.discount_type, .p_vdc_vendor_discount, .p_regular_price').on("input", function () {

            discountCalc('.p_regular_price', '.discount_type', '.p_vdc_vendor_discount', '.p_vdc_price_vendor')
        });


        $('.delete_product_image').on('click', function () {
            console.log("sdgasdgasdgasdgsad");
            var image = $(this).attr('id');
            var token = $(this).data('token');
            console.log(image);
            console.log(token);
            if (image != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    // contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{route('VendordeleteProductimage')}}',
                    type: 'POST',
                    data: {'imageid': image, _token: token},
                    success: function (response) {
                        $('.imagediv_' + image).remove();
                    }
                });
            }
        });


        $('#adProducts').validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            errorPlacement: function (error, e) {
                e.parents('.form-group').append(error);
            },
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.text-danger').remove();
            },
            success: function (e) {
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.text-danger').remove();
            },
            rules: {
                p_vendor_id: {
                    required: true,
                },
                p_name: {
                    required: true,
                    minlength: 10,
                    maxlength: 100,
                },
                p_cat_id: {
                    required: true
                },
                p_sub_cat_id: {
                    required: true
                },
                // p_item_spl: {
                //     required: true
                // },
                // cack
                // p_dishtype: {
                //     required: true
                // },
                p_weight: {
                    required: true,

                },
                p_color: {
                    required: true
                },

                p_availability: {
                    required: true
                },
                p_regular_price: {
                    required: true,
                    number: true
                },
                p_vendor_discount_type: {
                    required: true
                },
                p_vdc_vendor_discount: {
                    required: true,
                    number: true
                },
                p_vdc_price_vendor: {
                    required: true,
                    number: true
                },
                p_overview: {
                    required: true,
                },
                p_specifications: {
                    required: true
                },
                p_quality_care_info: {
                    required: true
                },
                // p_status: {
                //     required: true
                // }
            },
            messages: {
                p_vendor_id: {
                    required: 'Select Vendor'
                },
                p_name: {
                    required: 'Enter Proper Product Name',
                    minlength: 'Enter minimam {0}',
                    maxlength: 'Enter max {0}',
                },
                p_cat_id: {
                    required: 'Select Primary Category'
                },
                p_sub_cat_id: {
                    required: 'Select Sub Category'
                },
                // p_item_spl: {
                //     required: 'Select SPL'
                // },
                // cack
                // p_dishtype: {
                //     required: 'Select Dish Type'
                // },
                p_weight: {
                    required: 'Enter Weight',

                },
                p_color: {
                    required: 'Enter color name'
                },

                p_availability: {
                    required: 'Select availability'
                },
                p_regular_price: {
                    required: 'Enter Price',
                    number: 'Enter numbers only'
                },
                p_vendor_discount_type: {
                    required: 'select Discount type',

                },
                p_vdc_vendor_discount: {
                    required: 'Enter Discount Price',
                    number: 'Enter numbers only'
                },
                p_vdc_price_vendor: {
                    required: 'Enter Price',
                    number: 'Enter numbers only'
                },
                p_overview: {
                    required: 'Please Enter Overview',
                },
                p_specifications: {
                    required: 'Please Enter Specifications'
                },
                p_quality_care_info: {
                    required: 'Please Enter Product Information'
                },
                // p_status: {
                //     required: 'Select Status'
                // }

            }
        });

        imageRules();
        validateSKU();

        function imageRules() {
            $(".productimage").rules("add", {
                required: true,
                messages: {
                    required: "Minimum one image is required"
                }
            });

        }


        $('.titleCreateAlias').on('input', function () {
            // var aliasbox = $(this).data('aliasbox');
            var product_name = $(this).val();
            var alias = product_name.trim().replace(/[^a-zA-Z0-9]/g, '-').replace(/-{2,}/g, '-').toLowerCase();
            $('#p_alias').val(alias);
        });


        // $('#cake,#flowers,#gifts,#chocolates,#jewllary,#millets').hide();

        var ExistCatId = $('#p_cat_id').val();

        if (ExistCatId) {
            catOptions(ExistCatId);
        }

        $('#p_cat_id').on('change', function () {
            // alert("testest");

            // console.log('asd');

            var catID = $(this).val();
            if (catID) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '{{route('showCategoryTypes')}}',
                    type: "POST",
                    data: {'catID': catID},
                    dataType: "json",
                    success: function (data) {

                        var Types = data.types;
                        var Others = data.others;


                        $('#p_sub_cat_id').empty();
                        $('#p_sub_cat_id').append('<option value="">--Select SubCategory--</option>');
                        $.each(Types, function (key, value) {
                            if ($('.p_sub_cat_id').val() == key) {
                                $('#p_sub_cat_id').append('<option value="' + key + '" selected>' + value + '</option>');
                            } else {
                                $('#p_sub_cat_id').append('<option value="' + key + '">' + value + '</option>');
                            }
                        });


                        $('#p_item_spl').empty();
                        $('#p_item_spl').append('<option value="">--Select Special--</option>');
                        $.each(Others, function (key, value) {
                            if ($('.p_item_spl').val() == key) {
                                $('#p_item_spl').append('<option value="' + key + '" selected>' + value + '</option>');
                            }else{
                                $('#p_item_spl').append('<option value="' + key + '">' + value + '</option>');
                            }
                        });


                    }
                });

                catOptions(catID)

            } else {
                $('#p_sub_cat_id').empty();
                $('#p_item_spl').empty();
            }
        }).change();


    })
</script>


<script>
    jQuery(document).ready(function () {


        $('.delete_productExtra_image').on('click', function () {
            var image = $(this).attr('id');
            if (image != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '/admin/ajax/deleteproductextraimage',
                    type: 'POST',
                    data: {'image': image},
                    success: function (response) {
                        $('.imagediv' + image).remove();
                        $('#productExtraimage' + image).addClass('productExtraimage');
                        imageRules();
                    }
                });
            }
        });

        imageRules();


    });

    function imageRulesForProducts() {
        $(".productimage").rules("add", {
            required: true,
            messages: {
                required: "Upload image to save"
            }
        });

    }

    function imageRules() {
        $(".productExtraimage").each(function () {
            $(this).rules("add", {
                required: true,
                messages: {
                    required: "Upload image to save"
                }
            });
        })


    }

</script>