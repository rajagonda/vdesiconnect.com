@extends('backend.layout')
@section('title', $title)

@section('headerStyles')
@endsection

@section('content')




    @if ($product_id!='')
        <ul class="breadcrumb ">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('admin_products')}}">Products</a>
            </li>
            @if(isset($product->getCategory->category_name))
                <li class="breadcrumb-item">
                    <a href="{{url('admin/products?search=search&product_name=&search_category='.$product->getCategory->category_id.'&search_vendor=&p_status=')}}">{{$product->getCategory->category_name}}</a>
                </li>
            @endif
            <li class="breadcrumb-item">
                <a href="javascript:void(0)">{{$product->p_name}}</a>
            </li>


        </ul>
    @else
        {!! getBreadcrumbs(
                     array(
                     'dashboard'=>'Home',
                     'admin_products'=>'Products',
                     ''=>'Add New'
                     ),'Add New Product'
                  ) !!}
    @endif




    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif

                    {{--                    @include('backend.products.nav')--}}
                    <div class="tab-content">
                        <div class="tab-pane fade show active">
                            <div class="card">


                                <div class="card-body card-block">

                                    <?php
                                    //                                    dump($product);
                                    ?>

                                    <form method="POST" id="adProducts" action="{{ route('addNewProducts') }}"
                                          accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="p_id" value="{{ $product_id }}">


                                        {{--<input type="hidden"--}}
                                        {{--class="edit_current_product_alias"--}}
                                        {{--value="{{ (isset($product->p_alias)) ? $product->p_alias : old('p_alias')}}"/>--}}

                                        <div class="row">
                                            <!-- col -->
                                            <div class="col-lg-6 col-md-6 ">


                                                @if ($product_id!='')
                                                    <input type="hidden" value="{{$product->p_vendor_id}}"
                                                           name="p_vendor_id"/>

                                                @else

                                                    <?php
                                                    //                                            dump($vendor_list);
                                                    ?>


                                                    <div class="form-group">

                                                        <label>Select Vendor</label>

                                                        <div class="input-group">
                                                            <select class="form-control" name="p_vendor_id">
                                                                <option value="">Select Vendor</option>
                                                                @if(count($vendor_list) > 0)
                                                                    @foreach($vendor_list AS $vendor)
                                                                        <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                                                                    @endforeach

                                                                @endif

                                                            </select>

                                                        </div>


                                                    </div>

                                                @endif


                                                <div class="card">
                                                    <div class="card-header">
                                                        Product Details
                                                    </div>
                                                    <div class="card-body">

                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">

                                                                    <label for="hf-email" class=" form-control-label">Product
                                                                        name</label>


                                                                    <input class="form-control titleCreateAlias"
                                                                           id="p_name"
                                                                           type="text"
                                                                           placeholder="Product name"
                                                                           name="p_name"
                                                                           value="{{ !empty(old('p_name')) ? old('p_name') : ((($product) && ($product->p_name)) ? $product->p_name : '') }}">
                                                                    @if ($errors->has('p_name'))
                                                                        <span class="text-danger help-block">{{ $errors->first('p_name') }}</span>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row" style="display: none">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">

                                                                    <label for="hf-email" class=" form-control-label">Product
                                                                        Alias</label>


                                                                    <input class="form-control" id="p_alias" type="text"
                                                                           readonly
                                                                           placeholder="Product Alias"
                                                                           name="p_alias"
                                                                           value="">
                                                                    @if ($errors->has('p_alias'))
                                                                        <span class="text-danger help-block">{{ $errors->first('p_alias') }}</span>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">

                                                            <div class="col-lg-4">

                                                                <div class="form-group">

                                                                    <label for="hf-email"
                                                                           class="form-control-label">Category</label>


                                                                    <select name="p_cat_id" id="p_cat_id"
                                                                            class="form-control">
                                                                        <option value="">Select</option>
                                                                        <?php
                                                                        $categories = getCategoriesByID(); ?>
                                                                        @foreach($categories as $ks=>$s)
                                                                            <option value="{{ $ks }}" {{ (!empty(old('p_cat_id')) && old('p_cat_id')==$ks)  ? 'selected' : ((($product) && ($product->p_cat_id == $ks)) ? 'selected' : '') }}>
                                                                                {{ $s }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                    @if ($errors->has('p_cat_id'))
                                                                        <span class="text-danger help-block">{{ $errors->first('p_cat_id') }}</span>
                                                                    @endif

                                                                </div>
                                                            </div>

                                                            @if(!empty($product))
                                                                <input type="hidden" class="p_sub_cat_id"
                                                                       value="{{ $product->p_sub_cat_id }}">

                                                                <input type="hidden" class="p_item_spl"
                                                                       value="{{ $product->p_item_spl }}">
                                                        @endif

                                                            <!--/ col -->

                                                            <!-- col -->
                                                            <div class="col-lg-4">
                                                                <div class="form-group">

                                                                    <label for="hf-email" class="form-control-label">Sub Category</label>


                                                                    <select name="p_sub_cat_id" id="p_sub_cat_id"
                                                                            class="form-control">
                                                                        <option value="">Select Sub Cat</option>

                                                                    </select>
                                                                    @if ($errors->has('p_sub_cat_id'))
                                                                        <span class="text-danger help-block">{{ $errors->first('p_sub_cat_id') }}</span>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                            <!--/ col -->

                                                            <div class="col-lg-4">
                                                                <div class="form-group">

                                                                    <label for="hf-email" class="form-control-label">Item
                                                                        Special</label>


                                                                    <select name="p_item_spl" id="p_item_spl"
                                                                            class="form-control">
                                                                        <option value="">Select Specials</option>
                                                                    </select>
                                                                    @if ($errors->has('p_item_spl'))
                                                                        <span class="text-danger help-block">{{ $errors->first('p_item_spl') }}</span>
                                                                    @endif

                                                                </div>
                                                            </div>


                                                        </div>

                                                        <div class="row">

                                                            <div class="col-lg-12" id="cake" style="display: none">

                                                                <div class="row">
                                                                    <div class="col-lg-4">

                                                                        <label>Dish type</label>
                                                                        <div class="input-group">

                                                                            <div class="form-check-inline">
                                                                                <label class="form-check-label">
                                                                                    <input type="radio"
                                                                                           class="form-check-input"
                                                                                           value="Egg"
                                                                                           name="p_dishtype" {{ !empty(old('p_dishtype')) ? old('p_dishtype') : ((($product) && ($product->p_dishtype=='Egg')) ? 'checked=""': '') }} />
                                                                                    Egg
                                                                                </label>
                                                                            </div>
                                                                            <div class="form-check-inline">
                                                                                <label class="form-check-label">
                                                                                    <input type="radio"
                                                                                           class="form-check-input"
                                                                                           value="Egg Less"
                                                                                           name="p_dishtype" {{ !empty(old('p_dishtype')) ? old('p_dishtype') : ((($product) && ($product->p_dishtype=='Egg Less')) ? 'checked=""': '') }}>
                                                                                    Egg Less
                                                                                </label>
                                                                            </div>

                                                                        </div>

                                                                        @if ($errors->has('p_dishtype'))
                                                                            <span class="text-danger help-block">{{ $errors->first('p_dishtype') }}</span>
                                                                        @endif

                                                                    </div>


                                                                    <!--/ col -->

                                                                </div>

                                                            </div>

                                                        </div>


                                                        <!-- row -->
                                                        <div class="row">

                                                            <div class="col-lg-4">
                                                                <div class="form-group">

                                                                    <label for="hf-email" class=" form-control-label">Product
                                                                        Availability</label>


                                                                    <select name="p_availability" id="p_availability"
                                                                            class="form-control">
                                                                        <?php
                                                                        $availability = availability(); ?>
                                                                        @foreach($availability as $ks=>$s)
                                                                            <option value="{{ $ks }}" {{ (!empty(old('p_availability')) && old('p_availability')==$ks)  ? 'selected' : ((($product) && ($product->p_availability == $ks)) ? 'selected' : '') }}>{{ $s }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                    @if ($errors->has('p_availability'))
                                                                        <span class="text-danger help-block">{{ $errors->first('p_availability') }}</span>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                            <!--/ col -->


                                                        </div>


                                                        {{--<div class="form-group">--}}
                                                        {{--<label class="form-control-label">Product Overview</label>--}}
                                                        {{--<!-- editor-->--}}
                                                        {{--<textarea id="txtEditor"></textarea>--}}
                                                        {{--<!-- editor-->--}}
                                                        {{--</div>--}}


                                                    </div>
                                                </div>


                                                <div class="card">
                                                    <div class="card-header">
                                                        Product Price SKU Info
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="row">

                                                            <table class="table table-bordered" id="dynamicTable">
                                                                <tr>

                                                                    <th>SKU TYPE</th>
                                                                    <th>SKU OPTION</th>
                                                                    <th>SKU VALUE</th>
                                                                    <th>Action</th>
                                                                </tr>

                                                                <?php
                                                                $j = 0;
                                                                ?>
                                                                @if(isset($product->productSKUs))
                                                                    @if(count($product->productSKUs)>0)
                                                                        @foreach($product->productSKUs as $options)
                                                                            <?php
                                                                            $j = $loop->iteration;
                                                                            ?>

                                                                            <tr id="{{ $options->sku_id }}">
                                                                                <input type="hidden"
                                                                                       name="product_options[{{ $loop->iteration }}][for]"
                                                                                       value="edit"/>
                                                                                <input type="hidden"
                                                                                       name="product_options[{{ $loop->iteration }}][sku_id]"
                                                                                       value="{{$options->sku_id }}"/>


                                                                                <td class="form-group">
                                                                                    <div class="input-group">
                                                                                        <select name="product_options[{{ $loop->iteration }}][sku_type]"
                                                                                                class="form-control sku_type">

                                                                                            @foreach(skuTypes() AS $skuTypes)
                                                                                                <option @if($skuTypes==$options->sku_type) selected @endif>{{$skuTypes}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </td>


                                                                                <td class="form-group">
                                                                                    <input type="text"
                                                                                           name="product_options[{{ $loop->iteration }}][sku_option]"
                                                                                           placeholder="Enter your Qty"
                                                                                           class="form-control sku_option"
                                                                                           value="{{ $options->sku_option }}"/>
                                                                                    <small>(if weight in grams write "500 grams" or Enter Number only for Kgs)</small>
                                                                                </td>


                                                                                <td class="form-group">
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-prepend">
                                                                                            <div class="input-group-text">
                                                                                                {!! currencySymbol('INR')  !!}
                                                                                            </div>
                                                                                        </div>
                                                                                        <input type="text"
                                                                                               name="product_options[{{ $loop->iteration }}][sku_vendor_price]"
                                                                                               placeholder="Enter your Price "
                                                                                               class="form-control sku_vendor_price"
                                                                                               value="{{ $options->sku_vendor_price }}"/>
                                                                                    </div>
                                                                                </td>
                                                                                <td>
                                                                                    @if($j==1)
                                                                                        *
                                                                                    @else
                                                                                        <button type="button"
                                                                                                class="btn btn-danger remove-tr">Remove
                                                                                        </button>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @endif
                                                               @else
                                                                <tr>
                                                                    <input type="hidden"
                                                                           name="product_options[{{ $j+1 }}][for]"
                                                                           value="new"/>
                                                                    <input type="hidden"
                                                                           name="product_options[{{ $j+1 }}][sku_id]"
                                                                           value=""/>

                                                                    <td class="form-group">
                                                                        <div class="input-group">
                                                                            <select name="product_options[{{ $j+1 }}][sku_type]"
                                                                                    class="form-control sku_type">
                                                                                @foreach(skuTypes() AS $skuTypes)
                                                                                    <option>{{$skuTypes}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>

                                                                    </td>
                                                                    <td class="form-group">
                                                                        <input type="text"
                                                                               name="product_options[{{ $j+1 }}][sku_option]"
                                                                               placeholder="Ex: 2kg, 3kg, 5flowers, 5ltrs"
                                                                               class="form-control sku_option"/>
                                                                    </td>
                                                                    <td class="form-group">

                                                                        <div class="input-group">
                                                                            <div class="input-group-prepend">
                                                                                <div class="input-group-text">
                                                                                    {!! currencySymbol('INR')  !!}
                                                                                </div>
                                                                            </div>

                                                                            <input type="text"
                                                                                   name="product_options[{{ $j+1 }}][sku_vendor_price]"
                                                                                   placeholder="Enter Value or Price"
                                                                                   class="form-control sku_vendor_price"/>


                                                                        </div>


                                                                    </td>
                                                                    <td>
                                                                        *
                                                                    </td>
                                                                </tr>
                                                               @endif

                                                            </table>

                                                            <div class="col-md-12 text-right">
                                                                <button type="button" name="add" id="add"
                                                                        class="btn btn-success">Add New
                                                                </button>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="card">
                                                    <div class="card-header">
                                                        Product Gallery Info
                                                    </div>
                                                    <div class="card-body">

                                                        <!-- image gallery  -->
                                                        <div class="img-gallery">
                                                            <div class="form-group">
                                                                <label class="form-control-label">Product
                                                                    Gallery </label>
                                                                <input class="form-control {{ (($product_images) && (count($product_images)>0)) ? '' : 'productimage'}}" id="productimage"
                                                                       type="file"
                                                                       name="productimages[]" multiple>
                                                                @if ($errors->has('productimages[]'))
                                                                    <span class="text-danger">{{ $errors->first('productimages[]') }}</span>
                                                                @endif
                                                                <small>
                                                                    <span>Required Minimum 1 Image, Maximum 5 Images</span> <span>Width=578px, height=578px</span> <span>Each Image should be Less than 120 kb</span>
                                                                </small>
                                                            </div>

                                                            <div class="row">

                                                                @if(count($product_images)>0)
                                                                    @foreach($product_images as $item)
                                                                        <div class="col-md-2 imagediv_{{ $item->pi_id }}">

                                                                            <a data-fancybox="gallery" href="/uploads/products/{{ $item->pi_image_name }}">
                                                                                <img  class="img-fluid imagecover"
                                                                                      height="150"
                                                                                      src="/uploads/products/thumbs/{{ $item->pi_image_name }}"/>
                                                                            </a>



                                                                            <a  id="{{$item->pi_id}}"
                                                                               class="delete_product_image btn btn-danger btn-xs"

                                                                               onclick="return confirm('Confirm delete')"><i
                                                                                        class="fa fa-trash-o"
                                                                                        aria-hidden="true"></i>


                                                                            </a>
                                                                        </div>
                                                                    @endforeach
                                                                @else
                                                                    <div class="col-md-2">
                                                                        No Images found
                                                                    </div>
                                                                @endif
                                                            </div>

                                                        </div>
                                                        <!--/ image gallery -->

                                                    </div>
                                                </div>


                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">

                                                                <label for="hf-email" class=" form-control-label">Related Products (with comma separated, Ex: 1,2,10)</label>


                                                                <input class="form-control"
                                                                       id="p_related"
                                                                       type="text"
                                                                       placeholder="Products Related Id's"
                                                                       name="p_related"
                                                                       value="{{ !empty(old('p_related')) ? old('p_related') : ((($product) && ($product->p_related)) ? $product->p_related : '') }}">
                                                                @if ($errors->has('p_related'))
                                                                    <span class="text-danger help-block">{{ $errors->first('p_related') }}</span>
                                                                @endif

                                                            </div>
                                                        </div>
                                                    </div>


                                            </div>

                                            <!--/ col -->
                                            <!-- col -->
                                            <div class="col-lg-6 col-md-6">
                                                <!-- card -->
                                                <div class="card">
                                                    <!-- card body -->
                                                    <div class="card-header">
                                                        Product Options
                                                    </div>
                                                    <div class="card-body">


                                                        <div class="form-group">

                                                            <label for="hf-email" class=" form-control-label">
                                                                Product Overview
                                                            </label>


                                                            <div class="input-group">
                                                <textarea name="p_overview" id="p_overview" rows="5"
                                                          placeholder="Product Overview..."
                                                          class="form-control ckeditor">{!! !empty(old('p_overview')) ? old('p_overview') : ((($product) && ($product->p_overview)) ? $product->p_overview : '') !!}</textarea>
                                                                @if ($errors->has('p_overview'))
                                                                    <span class="text-danger help-block">{{ $errors->first('p_overview') }}</span>
                                                                @endif

                                                            </div>
                                                        </div>


                                                        <div class="form-group">

                                                            <label for="hf-email" class=" form-control-label">Product
                                                                Specifications</label>

                                                            <div class="input-group">
                                                <textarea name="p_specifications" id="p_specifications" rows="5"
                                                          placeholder="Product Specifications..."
                                                          class="form-control ckeditor">{!! !empty(old('p_specifications')) ? old('p_specifications') : ((($product) && ($product->p_specifications)) ? $product->p_specifications : '') !!}</textarea>
                                                                @if ($errors->has('p_specifications'))
                                                                    <span class="text-danger help-block">{{ $errors->first('p_specifications') }}</span>
                                                                @endif

                                                            </div>
                                                        </div>


                                                        <div class="form-group">

                                                            <label for="hf-email" class=" form-control-label">Product
                                                                Quality Info</label>


                                                            <textarea name="p_quality_care_info"
                                                                      id="p_quality_care_info"
                                                                      rows="5"
                                                                      placeholder="Product Quality Info Care..."
                                                                      class="form-control ckeditor">{!! !empty(old('p_quality_care_info')) ? old('p_quality_care_info') : ((($product) && ($product->p_quality_care_info)) ? $product->p_quality_care_info : '') !!}</textarea>
                                                            @if ($errors->has('p_quality_care_info'))
                                                                <span class="text-danger help-block">{{ $errors->first('p_quality_care_info') }}</span>
                                                            @endif

                                                        </div>


                                                        <div class="form-group">

                                                            <label for="hf-email" class=" form-control-label">Product Meta Title</label>


                                                            <input class="form-control"
                                                                   id="p_meta_title"
                                                                   type="text"
                                                                   placeholder="Product Meta Title"
                                                                   name="p_meta_title"
                                                                   value="{{ !empty(old('p_meta_title')) ? old('p_meta_title') : ((($product) && ($product->p_meta_title)) ? $product->p_meta_title : '') }}">


                                                            @if ($errors->has('p_meta_title'))
                                                                <span class="text-danger help-block">{{ $errors->first('p_meta_title') }}</span>
                                                            @endif

                                                        </div>


                                                        <div class="form-group">

                                                            <label for="hf-email" class=" form-control-label">Product Meta Keywords</label>


                                                            <input class="form-control"
                                                                   id="p_meta_keywords"
                                                                   type="text"
                                                                   placeholder="Product Meta Keywords"
                                                                   name="p_meta_keywords"
                                                                   value="{{ !empty(old('p_meta_keywords')) ? old('p_meta_keywords') : ((($product) && ($product->p_meta_keywords)) ? $product->p_meta_keywords : '') }}">


                                                            @if ($errors->has('p_meta_keywords'))
                                                                <span class="text-danger help-block">{{ $errors->first('p_meta_keywords') }}</span>
                                                            @endif

                                                        </div>

                                                        <div class="form-group">

                                                            <label for="hf-email" class=" form-control-label">Product Meta Description</label>


                                                            <textarea name="p_meta_description"
                                                                      id="p_meta_description"
                                                                      rows="5"
                                                                      placeholder="Product Meta Description"
                                                                      class="form-control">{!! !empty(old('p_meta_description')) ? old('p_meta_description') : ((($product) && ($product->p_meta_description)) ? $product->p_meta_description : '') !!}</textarea>
                                                            @if ($errors->has('p_meta_description'))
                                                                <span class="text-danger help-block">{{ $errors->first('p_meta_description') }}</span>
                                                            @endif

                                                        </div>


                                                        <div clas="col-lg-12 py-4">


                                                            <div class="row">


                                                                <div class="col-lg-4">

                                                                </div>

                                                                <div class="col-lg-8 text-right">
                                                                    <br/>
                                                                    <button type="submit" class="btn btn-success">
                                                                        @if ($product_id!='')
                                                                            <i class="fa fa-dot-circle-o"></i>
                                                                            Update  Now
                                                                        @else
                                                                            <i class="fa fa-dot-circle-o"></i> Add
                                                                            Product
                                                                            Now
                                                                        @endif
                                                                    </button>
                                                                </div>
                                                            </div>


                                                        </div>


                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </form>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    {{--    <script src="{{url('backend/modules/products/js/products.js')}}"></script>--}}



    @include('backend.products.scripts')






@endsection