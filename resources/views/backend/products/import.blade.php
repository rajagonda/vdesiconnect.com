@extends('backend.layout')


@section('content')


    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              'admin_products'=>'Products',
              ''=>'Import Products'
              ),'Import Products'
           ) !!}



    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif

                    @include('backend.products.nav')
                    <div class="tab-content">
                        <div class="tab-pane fade show active">
                            <div class="card">





                                <div class="card-body card-block">
                                    <form method="POST" id="products"
                                          action="{{ route('sellerImportListings', ['id'=>$cats_id]) }}"
                                          class="form-horizontal" enctype="multipart/form-data">
                                        {{ csrf_field() }}

                                        <div class="row">

                                            <div class="col-6">

                                                @if (count($cats)> 0)




                                                    <div class="input-group">
                                                        <select class="form-control " name="category_id" id="catChange">
                                                            <option value="">Select category</option>
                                                            @foreach($cats AS $cat)
                                                                <option {{($cats_id==$cat->category_id)?'selected':''}}  data-url="{{route('sellerImportListings', ['id'=>$cat->category_id])}}"
                                                                        value="{{$cat->category_id}}">
                                                                    {{$cat->category_name}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>



                                                @endif



                                                @if ($cats_id)

                                                    <a href="/uploads/sample/admin/cakes_products_sample.csv"
                                                       download="cakes_products_sample.csv">Download sample for
                                                        <b>{{getCategory($cats_id)->category_name }}</b> </a>


                                                    <div class="form-group">
                                                        <label>Import file</label>
                                                        <input class="form-control" name="import_file" type="file"
                                                               value=""/>
                                                    </div>

                                                    <input type="submit" value="Inport"/>

                                                @else


                                                    <div class="">
                                                        please select cat
                                                    </div>

                                                @endif


                                            </div>

                                        </div>


                                    </form>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->



@endsection

@section('footerScripts')
    <script>
        $(function () {

            $("#catChange").on('change', function () {
                var selectedItem = $(this).val();
                var urlData = $('option:selected', this).data('url');
                // console.log('adsads');
                console.log(urlData);


                window.location.href = urlData;
            });
        });

    </script>
@endsection
