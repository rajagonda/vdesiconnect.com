@extends('backend.layout')
@section('title', $title)

@section('headerStyles')


@endsection

@section('content')


    @if(app('request')->input('search_category')!='')

        <?php
//        dd(array_first($products)->getCategory->category_name);
?>
        <ul class="breadcrumb ">
            <li class="breadcrumb-item">
                <a href="{{route('dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('admin_products')}}">Products</a>
            </li>

            <li class="breadcrumb-item">
                <a href="{{url('admin/products?search=search&product_name=&search_category='.array_first($products)->getCategory->category_id.'&search_vendor=&p_status=')}}">{{array_first($products)->getCategory->category_name}}</a>
            </li>


        </ul>

    @else
    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Products'
              ),'Products'
           ) !!}
    @endif

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif


                    <form method="GET" action="{{ route('admin_products') }}" accept-charset="UTF-8"
                          class="navbar-form navbar-right" role="search" autocomplete="off">
                        <input type="hidden" name="search" value="search">

                        <div class="row">
                            &nbsp;&nbsp;
                            <div class="col-md-3">
                                <input type="text" class="form-control" name="product_name"
                                       placeholder="Enter Product Name to Search..."
                                       value="{{ request('product_name') }}">
                            </div>
                            <div class="col-md-2">
                                <select name="search_category" id="search_category"
                                        class="form-control">
                                    <option value="">Select Category</option>
                                    <?php
                                    $categories = getCategories(); ?>
                                    @foreach($categories as $ks=>$s)
                                        <option value="{{ $ks }}" {{ (app('request')->input('search_category')==$ks)  ? 'selected' : ''}}>
                                            {{ $s }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-2">
                                <select class="form-control" name="search_vendor" id="search_vendor">
                                    <option value="">Select Vendor</option>
                                    @if(count($vendor_list) > 0)
                                        @foreach($vendor_list AS $vendor)
                                            <option value="{{$vendor->id}}" {{ (app('request')->input('search_vendor')==$vendor->id)  ? 'selected' : ''}}>{{$vendor->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="col-md-2">
                                <select class="form-control" name="p_status" id="p_status">
                                    <option value="">Select Status</option>
                                    <option value="0" {{ (app('request')->input('p_status')=='0')  ? 'selected' : ''}}>
                                        Pending Approval
                                    </option>
                                    <option value="1" {{ (app('request')->input('p_status')=='1')  ? 'selected' : ''}}>
                                        Approved
                                    </option>
                                    <option value="2" {{ (app('request')->input('p_status')=='2')  ? 'selected' : ''}}>
                                        Rejected
                                    </option>
                                </select>
                            </div>
                            <div class="col-md-1">
                                             <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </div>

                    </form>

                    <br/>
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Products</strong>
                            <a href="{{route('addNewProducts')}}" class="btn btn-primary" style="float:right;">+ Add
                                Products
                            </a>

                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Unique Id</th>
                                    <th>Vendor</th>
                                    <th>Product Picture</th>
                                    <th>Product Name</th>
                                    <th>Category</th>

                                    <th>View on Website</th>
                                    <th>Approval Status</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if(count($products) > 0)
                                    @foreach($products AS $product_item)

                                        <?php
                                        //                                                                                dump($product_item->productSKUs);
                                        ?>

                                        <tr>
                                            <th scope="row">{{ $product_item->p_id }}</th>
                                            <td>
                                                {{ $product_item->p_unique_id }}
                                            </td>
                                            <td>
                                                @if(isset($product_item->getVendor->name))
                                                    <a href="{{route('admin_vendor_account_details', ['id'=>$product_item->getVendor->id])}}">
                                                        {{$product_item->getVendor->name}}
                                                    </a>
                                                @else
                                                    --
                                                @endif
                                            </td>
                                            <td>
                                                <?php
                                                $images = getImagesByProduct($product_item->p_id);

                                                //                                                        dump($images);

                                                //                                                        dd($images);

                                                if (!empty($images) && count($images) > 0) {
                                                    $image_src = $images[0];
                                                } else {
                                                    $image_src = 'https://dummyimage.com/600x400/f2f2f2/000000';
                                                }

                                                ?>

                                                <a href="{{route('AdminProductView',['id'=>$product_item->p_id])}}">
                                                    <img width="50"
                                                         src="{{$image_src}}"
                                                         alt=""
                                                         class="img-thumb">
                                                </a>
                                            </td>

                                            <td>
                                                <a href="{{route('AdminProductView',['id'=>$product_item->p_id])}}">
                                                    {{$product_item->p_name}}
                                                </a>
                                            </td>
                                            <td>

                                                <?php
                                                $categorieData = getCategory($product_item->p_cat_id);
                                                ?>

                                                {{$categorieData->category_name}}

                                            </td>


                                            <td>


                                                @if($product_item->p_status==1)
                                                    <a href="{{ route('productPage', ['category'=>$categorieData->category_alias,'product'=>$product_item->p_alias]) }}"
                                                       target="_blank" data-toggle="tooltip"
                                                       title="View on vdesiconnect.com">
                                                        <i class="fa fa-desktop" aria-hidden="true"></i>
                                                    </a>
                                                @endif

                                                {{--                                                {{ $product_item->p_status }}--}}


                                            </td>
                                            <td>
                                                {{ productStatus($product_item->p_status) }}

                                                {{--                                                {{$product_item->p_status}}--}}
                                            </td>
                                            <td class="options">


                                                <a href="{{route('AdminProductView',['id'=>$product_item->p_id])}}"
                                                   class="dropdown-item">
                                                    <i class="fa fa-eye"></i> View
                                                </a>


                                                <a class="dropdown-item"
                                                   href="{{ route('addNewProducts',['id'=>$product_item->p_id])}}"><i
                                                            class="fa fa-pencil"></i> Edit</a>


                                                {{--<a class="dropdown-item"--}}
                                                {{--href="">--}}
                                                {{--<i class="fa fa-eye"></i>--}}
                                                {{--View--}}
                                                {{--</a>--}}


                                                <a href="#" data-toggle="modal"
                                                   data-target="#productDetails{{ $product_item->p_id }}"
                                                   class="dropdown-item">
                                                    <i class="fa fa-eye"></i> Manage Approval
                                                </a>


                                                <div id="productDetails{{ $product_item->p_id }}"
                                                     class="modal fade productDetails"
                                                     role="dialog">
                                                    <div class="modal-dialog modal-xl">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">

                                                                    <a href="{{route('AdminProductView',['id'=>$product_item->p_id])}}">
                                                                        <img width="50"
                                                                             src="{{$image_src}}"
                                                                             alt=""
                                                                             class="img-thumb">
                                                                    </a>

                                                                    {{ $product_item->p_name     }}

                                                                </h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">
                                                                    &times;
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">


                                                                <form method="POST"
                                                                      id="productdetails_box_{{ $product_item->p_id }}"
                                                                      action="{{ route('ProductApprove') }}"
                                                                      accept-charset="UTF-8"
                                                                      class="form-horizontal productApprove"
                                                                      style="display:inline">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="p_id"
                                                                           value="{{$product_item->p_id}}"/>

                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <table class="table table-bordered">

                                                                                <tbody>

                                                                                <?php
                                                                                //                                                                                dump($product_item->productSKUs)
                                                                                ?>
                                                                                @if (count($product_item->productSKUs) > 0)
                                                                                    @foreach($product_item->productSKUs AS $skus)

                                                                                        <tr id="sku_{{$skus->sku_id}}"
                                                                                            class="sku_{{$skus->sku_id}}">
                                                                                            <td>

                                                                                                <?php
                                                                                                //                                                                                                dump($product_item->productSKUs);
                                                                                                ?>

                                                                                                <div class="form-group">
                                                                                                    <label>
                                                                                                        VDC Vendor Price
                                                                                                    </label>

                                                                                                    <div class="input-group">
                                                                                                        <div class="input-group-prepend">
                                                                                                            <div class="input-group-text">
                                                                                                                {!! currencySymbol('INR')  !!}
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <input type="text"
                                                                                                               name="options[{{$skus->sku_id}}][sku_vendor_price]"
                                                                                                               readonly
                                                                                                               value="{{$skus->sku_vendor_price}}"
                                                                                                               class="form-control  vdc_vendor_price"/>
                                                                                                    </div>

                                                                                                </div>

                                                                                                <div>
                                                                                                    {{ $skus->sku_type }}
                                                                                                    :
                                                                                                    <small>
                                                                                                        {{ $skus->sku_option }}
                                                                                                    </small>
                                                                                                </div>


                                                                                            </td>
                                                                                            <td>

                                                                                                <div class="form-group">
                                                                                                    <label>VDC
                                                                                                        commission</label>

                                                                                                    <div class="input-group">

                                                                                                        <div class="input-group-prepend">
                                                                                                            <div class="input-group-text">
                                                                                                                <select class="vdc_commission_type"
                                                                                                                        name="options[{{$skus->sku_id}}][sku_vdc_commission_type]">
                                                                                                                    <option value="">

                                                                                                                        Type
                                                                                                                    </option>
                                                                                                                    <option value="fixed" {{ (isset($skus->sku_vdc_commission_type) && $skus->sku_vdc_commission_type == 'fixed' ) ? 'selected' : ''}}>
                                                                                                                        fixed
                                                                                                                    </option>
                                                                                                                    <option value="percentage" {{ (isset($skus->sku_vdc_commission_type) && $skus->sku_vdc_commission_type == 'percentage' ) ? 'selected' : ''}}>
                                                                                                                        %
                                                                                                                    </option>
                                                                                                                </select>

                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="input-group-prepend">
                                                                                                            <div class="input-group-text">
                                                                                                                {!! currencySymbol('INR')  !!}
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <input class="form-control vdc_commission"
                                                                                                               id="p_vdc_vendor_discount"
                                                                                                               type="text"
                                                                                                               placeholder="VDC Vendor commission"
                                                                                                               name="options[{{$skus->sku_id}}][sku_vdc_commission_value]"
                                                                                                               value="{{$skus->sku_vdc_commission_value}}"
                                                                                                               value="">

                                                                                                    </div>
                                                                                                </div>


                                                                                            </td>
                                                                                            {{--<td>--}}
                                                                                            {{--{{ $skus->sku_vdc_commission_value }}--}}
                                                                                            {{--</td>--}}
                                                                                            <td>

                                                                                                <div class="form-group">
                                                                                                    <label>
                                                                                                        VDC Store Final
                                                                                                        Price
                                                                                                    </label>

                                                                                                    <div class="input-group">
                                                                                                        <div class="input-group-prepend">
                                                                                                            <div class="input-group-text">
                                                                                                                {!! currencySymbol('INR')  !!}
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <input type="text"
                                                                                                               readonly
                                                                                                               value="{{$skus->sku_vdc_final_price}}"
                                                                                                               name="options[{{$skus->sku_id}}][sku_vdc_final_price]"
                                                                                                               class="form-control vdc_store_price"/>
                                                                                                    </div>

                                                                                                </div>


                                                                                                {{--                                                                                                {{ $skus->sku_vdc_final_price }}--}}
                                                                                            </td>
                                                                                            <td>

                                                                                                <div class="form-group">
                                                                                                    <label>
                                                                                                        VDC Store Lising
                                                                                                        Discount
                                                                                                    </label>

                                                                                                    <div class="input-group">

                                                                                                        <div class="input-group-prepend">
                                                                                                            <div class="input-group-text">
                                                                                                                <select class="vdc_store_discount_type"
                                                                                                                        name="options[{{$skus->sku_id}}][sku_store_discount_type]">
                                                                                                                    <option value="">

                                                                                                                        Type
                                                                                                                    </option>
                                                                                                                    <option value="fixed" {{ (isset($skus->sku_store_discount_type) && $skus->sku_store_discount_type == 'fixed' ) ? 'selected' : ''}}>
                                                                                                                        fixed
                                                                                                                    </option>
                                                                                                                    <option value="percentage" {{ (isset($skus->sku_store_discount_type) && $skus->sku_store_discount_type == 'percentage' ) ? 'selected' : ''}}>
                                                                                                                        %
                                                                                                                    </option>
                                                                                                                </select>

                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="input-group-prepend">
                                                                                                            <div class="input-group-text">
                                                                                                                {!! currencySymbol('INR')  !!}
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <input class="form-control vdc_store_discount"
                                                                                                               id="vdc_store_discount"
                                                                                                               type="text"
                                                                                                               placeholder="VDC Vendor commission"
                                                                                                               name="options[{{$skus->sku_id}}][sku_store_discount_value]"
                                                                                                               value="{{$skus->sku_store_discount_value}}"
                                                                                                               value="">

                                                                                                    </div>
                                                                                                </div>
                                                                                                {{--                                                                                                {{ $skus->sku_store_discount_type }}--}}

                                                                                                <div>
                                                                                                    <small>
                                                                                                        Created At
                                                                                                        : {{ $skus->created_at }}
                                                                                                    </small>
                                                                                                </div>

                                                                                            </td>
                                                                                            {{--<td>--}}
                                                                                            {{--{{ $skus->sku_store_discount_value }}--}}
                                                                                            {{--</td>--}}
                                                                                            <td>
                                                                                                <div class="form-group">
                                                                                                    <label>
                                                                                                        VDC Store Price
                                                                                                    </label>

                                                                                                    <div class="input-group">
                                                                                                        <input type="text"
                                                                                                               readonly
                                                                                                               value="{{$skus->sku_store_price}}"
                                                                                                               name="options[{{$skus->sku_id}}][sku_store_price]"
                                                                                                               class="form-control vdc_store_discount_price"/>
                                                                                                    </div>

                                                                                                </div>
                                                                                                {{--                                                                                                {{ $skus->sku_store_price }}--}}

                                                                                                <div>
                                                                                                    <small>
                                                                                                        Updated At
                                                                                                        : {{ $skus->updated_at }}
                                                                                                    </small>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>

                                                                                    @endforeach



                                                                                @endif


                                                                                </tbody>
                                                                            </table>


                                                                        </div>


                                                                        <div class="col-md-6">

                                                                            <div class="card">
                                                                                <div class="card-header">
                                                                                    Additional Price
                                                                                </div>
                                                                                <div class="card-body">


                                                                                    <div class="form-group">
                                                                                        <label> Delivery Charges</label>

                                                                                        <div class="input-group">
                                                                                            <input type="number"
                                                                                                   name="p_delivery_charges"
                                                                                                   value="{{$product_item->p_delivery_charges}}"
                                                                                                   class="form-control p_delivery_charges"/>
                                                                                        </div>

                                                                                    </div>


                                                                                </div>

                                                                            </div>

                                                                        </div>
                                                                        <div class="col-md-6">


                                                                            <div class="card">
                                                                                <div class="card-header">
                                                                                    Product Status
                                                                                </div>
                                                                                <div class="card-body">
                                                                                    <div class="form-group">

                                                                                        <div class="input-group">
                                                                                            <select class="p_status form-control"
                                                                                                    required
                                                                                                    name="p_status">
                                                                                                <option value="">
                                                                                                    --Select
                                                                                                    Status--
                                                                                                </option>
                                                                                                <option value="1" {{ (isset($product_item->p_status) && $product_item->p_status == '1' ) ? 'selected' : ''}}>
                                                                                                    Approved
                                                                                                </option>
                                                                                                <option value="2" {{ (isset($product_item->p_status) && $product_item->p_status == '2' ) ? 'selected' : ''}}>
                                                                                                    Rejected
                                                                                                </option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                        </div>
                                                                    </div>

                                                                    <div class="row text-center">
                                                                        <div class="col-md-12">
                                                                            <button type="submit"
                                                                                    class="btn btn-success">
                                                                                Submit
                                                                            </button>
                                                                        </div>

                                                                    </div>


                                                                </form>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>


                                                <form method="POST" id="productbox_form_{{ $product_item->p_id }}"
                                                      action="{{ route('admin_products') }}"
                                                      accept-charset="UTF-8" class="form-horizontal product_box_form"
                                                      style="display:inline">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="p_id"
                                                           value="{{ $product_item->p_id }}"/>
                                                    <button type="submit" class="dropdown-item"
                                                            title="Delete Product"
                                                            onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                        <i class="fa fa-trash"></i> Delete
                                                    </button>

                                                </form>


                                            </td>
                                        </tr>



                                    @endforeach

                                @else
                                    <tr>
                                        <td colspan="9" align="center">

                                            No results

                                        </td>

                                    </tr>

                                @endif

                                </tbody>
                            </table>




                        </div>


                    </div>


                </div>

                <div class="row px-3">
                    <div class="col-md-10">
                        <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                             aria-live="polite">
                            Showing {{ $products->firstItem() }}
                            to {{ $products->lastItem() }}
                            of {{ $products->total() }} entries
                        </div>
                    </div>
                    <div class="col-md-1 text-right">
                        <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                            {!! $products->appends(['search' => Request::get('search'),'product_name' => Request::get('product_name'),'search_category' => Request::get('search_category')])->render() !!}
                        </div>
                    </div>
                </div>

            </div>

        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')
    {!! javascriptFunctions() !!}
    <script>
        $(function () {

// Vendor calc
            $('.vdc_vendor_price, .vdc_commission').on("input", function () {

                var productDetails = $(this).closest('tr').attr('id');

                discountCalcWithParent('#' + productDetails, '.vdc_vendor_price', '.vdc_commission_type', '.vdc_commission', '.vdc_store_price', 'add');
            });

            $('.vdc_commission_type').on('change', function () {

                var productDetails = $(this).closest('tr').attr('id');

                discountCalcWithParent('#' + productDetails, '.vdc_vendor_price', '.vdc_commission_type', '.vdc_commission', '.vdc_store_price', 'add');
            });

            // store calc

            $('.vdc_store_price, .vdc_store_discount').on("input", function () {
                var productDetails = $(this).closest('tr').attr('id');

                discountCalcWithParent('#' + productDetails, '.vdc_store_price', '.vdc_store_discount_type', '.vdc_store_discount', '.vdc_store_discount_price');
            });

            $('.vdc_store_discount_type').on('change', function () {
                var productDetails = $(this).closest('tr').attr('id');
                discountCalcWithParent('#' + productDetails, '.vdc_store_price', '.vdc_store_discount_type', '.vdc_store_discount', '.vdc_store_discount_price');
            });


            $('.productdetails_box_form tr').each(function () {

                var productDetails = $(this).attr('id');

                discountCalcWithParent('#' + productDetails, ' .vdc_vendor_price', ' .vdc_commission_type', ' .vdc_commission', ' .vdc_store_price', 'add');

                discountCalcWithParent('#' + productDetails, ' .vdc_store_price', ' .vdc_store_discount_type', ' .vdc_store_discount', ' .vdc_store_discount_price');
            });


            $('.productApprove').each(function () {

                $(this).validate({
                    ignore: [],
                    errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                    errorElement: 'div',
                    errorPlacement: function (error, e) {
                        e.parents('.form-group').append(error);
                    },
                    highlight: function (e) {
                        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                        $(e).closest('.text-danger').remove();
                    },
                    success: function (e) {
                        // You can use the following if you would like to highlight with green color the input after successful validation!
                        e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        e.closest('.text-danger').remove();
                    },
                });
            })


            function validateSKUApruve() {

                $('.vdc_commission_type').each(function () {
                    $(this).rules("add", {
                        required: true,
                        messages: {
                            required: "Select commission type",
                        }
                    });
                });

                $('.vdc_commission').each(function () {
                    $(this).rules("add", {
                        required: true,
                        messages: {
                            required: "Enter commission Value",
                        }
                    });
                });
                $('.vdc_store_discount_type').each(function () {
                    $(this).rules("add", {
                        required: true,
                        messages: {
                            required: "Select discount type ",
                        }
                    });
                });
                $('.vdc_store_discount').each(function () {
                    $(this).rules("add", {
                        required: true,
                        messages: {
                            required: "Enter discount Value",
                        }
                    });
                });


            }

            validateSKUApruve()

        });
    </script>


@endsection
