@extends('backend.layout')
@section('title', $title)

@section('headerStyles')


@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Reveiws & Ratings'
              ),'Reveiws & Ratings'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Reveiws & Ratings</strong>

                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">User Name</th>
                                    <th scope="col">Product Name</th>
                                    <th scope="col">Rating</th>
                                    <th scope="col">Comment</th>
                                    <th scope="col">Created at</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($reviews)>0)
                                    @foreach($reviews as $item)
                                        <tr>
                                            <th scope="row">{{ $item->rt_id }}</th>
                                            <td>
                                                @if(isset($item->getUser->name))
                                                    <a href="{{route('userAddress',['id'=> $item->order_user_id ])}}">
                                                        {{ $item->getUser->name}}
                                                    </a>
                                                @else
                                                    ---
                                                @endif
                                            </td>
                                            <td>

                                                @if(isset($item->getProduct->getCategory))
                                                    <a href="{{ route('productPage', ['category'=>$item->getProduct->getCategory->category_alias,'product'=>$item->getProduct->p_alias]) }}"
                                                       target="_blank">
                                                        {{ $item->getProduct->p_name }}
                                                    </a>

                                                @endif
                                            </td>
                                            <td>{{ $item->rt_rating }}</td>
                                            <td>{{ $item->rt_comment }}</td>
                                            <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i:s')}}</td>
                                            <td>


                                                <form method="POST" id="orders"
                                                      action="{{ route('Reviews') }}"
                                                      accept-charset="UTF-8" class="form-horizontal"
                                                      style="display:inline">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="rt_id"
                                                           value="{{ $item->rt_id }}"/>
                                                    <button type="submit" class="dropdown-item"
                                                            title="Delete Rating"
                                                            onclick="return confirm('Confirm delete?')">
                                                        <i
                                                            class="fa fa-trash"></i> Delete
                                                    </button>

                                                </form>


                                            </td>

                                        </tr>



                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="11">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row px-3">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $reviews->firstItem() }}
                        to {{ $reviews->lastItem() }} of {{ $reviews->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $reviews->appends(['filters' => Request::get('filters'),'search' => Request::get('search')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

@endsection
