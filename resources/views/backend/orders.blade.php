@extends('backend.layout')
@section('title', $title)

@section('headerStyles')


@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Orders'
              ),'Orders'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif


                    <form method="GET" action="{{ route('adminorders') }}" accept-charset="UTF-8"
                          class="navbar-form navbar-right" role="search" autocomplete="off">
                        <input type="hidden" name="search" value="search">

                        <div class="row">
                            &nbsp;&nbsp;
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="search_orderid"
                                       placeholder="Order ID"
                                       value="{{ request('search_orderid') }}">
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control datepicker_hidepastdates" name="search_from_date"
                                       placeholder="From Date"
                                       value="{{ request('search_from_date') }}">
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control datepicker_hidepastdates" name="search_to_date"
                                       placeholder="To Date"
                                       value="{{ request('search_to_date') }}">
                            </div>
                            <div class="col-md-2">
                                <select name="search_country" id="search_country"
                                        class="form-control">
                                    <option value="">Select Country</option>
                                    <?php
                                    $countries = getCountries(); ?>
                                    @foreach($countries as $ks=>$s)
                                        <option
                                            value="{{ $ks }}" {{ (app('request')->input('search_country')==$ks)  ? 'selected' : ''}}>
                                            {{ $s }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            {{--<div class="col-md-2">--}}
                            {{--<select class="form-control" name="search_vendor" id="search_vendor">--}}
                            {{--<option value="">Select Vendor</option>--}}
                            {{--@if(count($vendor_list) > 0)--}}
                            {{--@foreach($vendor_list AS $vendor)--}}
                            {{--<option value="{{$vendor->id}}" {{ (app('request')->input('search_vendor')==$vendor->id)  ? 'selected' : ''}}>{{$vendor->name}}</option>--}}
                            {{--@endforeach--}}
                            {{--@endif--}}
                            {{--</select>--}}
                            {{--</div>--}}

                            <div class="col-md-2">
                                <select class="form-control" name="search_status" id="search_status">
                                    <option value="">Select Status</option>
                                    <option
                                        value="PendingPayment" {{ (app('request')->input('search_status')=='PendingPayment')  ? 'selected' : ''}}>
                                        Payment Failed
                                    </option>
                                    <option
                                        value="Paid" {{ (app('request')->input('search_status')=='Paid')  ? 'selected' : ''}}>
                                        Placed
                                    </option>

                                </select>
                            </div>
                            <div class="col-md-1">
                                             <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                                 <a href="{{route('adminorders')}}" class="btn btn-default">Reset</a>
                                </span>
                            </div>
                        </div>

                    </form>

                    <br/>


                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Orders</strong>

                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">OrderID</th>
                                    <th scope="col">User</th>
                                    <th scope="col">Reference Number</th>
                                    {{--                                    <th scope="col">Address</th>--}}
                                    <th scope="col">Total</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Orders items</th>
                                    <th scope="col">Orders Price</th>
                                    <th scope="col">Order Status</th>
                                    <th scope="col">Payment Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($orders)>0)
                                    <?php
                                    $ordersPrice = 0;
                                    $order_sku_vendor_price = 0;
                                    $order_sku_vdc_final_price = 0;
                                    $order_sku_store_price = 0;
                                    ?>
                                    @foreach($orders as $item)
                                        @php
                                            $address=unserialize($item->order_delivery_address);

                                         $order_currency = $item->order_currency;




                                        @endphp
                                        <tr>
                                            <th scope="row">{{ $item->order_id }}</th>
                                            <td>
                                                @if(isset($item->getUser->name))
                                                    <a href="{{route('userAddress',['id'=> $item->order_user_id ])}}">
                                                        {{ $item->getUser->name}}
                                                    </a>
                                                @else
                                                    ---
                                                @endif
                                            </td>
                                            <td>{{ $item->order_reference_number }}</td>
                                            {{--                                            <td>--}}
                                            {{--                                                {{ $address['ua_name'] }},{{ $address['ua_address'] }}--}}
                                            {{--                                                ,{{ $address['ua_city'] }},{{ $address['ua_state'] }}--}}
                                            {{--                                                ,{{ $address['ua_country'] }}--}}
                                            {{--                                            </td>--}}
                                            <td>


                                                {!! currencySymbol($order_currency)  !!}

                                                {{  $item->order_total_price }}
                                            </td>
                                            <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i:s')}}</td>
                                            <td>

                                                {{--                                                {{ $item->orderItems->count() }}--}}


                                                @if ($item->orderItems->count() > 0)

                                                    <?php

                                                    $oitem_sku_vendor_price = 0;
                                                    $oitem_sku_vdc_final_price = 0;
                                                    $oitem_sku_store_price = 0;

                                                    ?>

                                                    @foreach($item->orderItems AS $orderItem)

                                                        <?php

                                                        $oitem_sku_vendor_price = $oitem_sku_vendor_price + $orderItem->oitem_item_sku_vendor_price;
                                                        $oitem_sku_vdc_final_price = $oitem_sku_vdc_final_price + $orderItem->oitem_item_sku_vdc_final_price;
                                                        $oitem_sku_store_price = $oitem_sku_store_price + $orderItem->oitem_item_sku_store_price;


                                                        $oitem_item_sku_vendor_price = curencyConvert($item->order_currency, $orderItem->oitem_item_sku_vendor_price);
                                                        $oitem_item_sku_vdc_final_price = curencyConvert($item->order_currency, $orderItem->oitem_item_sku_vdc_final_price);
                                                        $oitem_item_sku_store_price = curencyConvert($item->order_currency, $orderItem->oitem_item_sku_store_price);
                                                        ?>

                                                        <div class="card">

                                                            <div class="card-header">
                                                                {{ isset($orderItem->getProduct)? $orderItem->getProduct->p_name: '' }}
                                                                <?php
                                                                //                                                                 dump($item->order_currency)
                                                                //                                                                 dump($orderItem)
                                                                ?>
                                                            </div>

                                                            <div class="card-body">
                                                                <div>
                                                                    Vendor Price :
                                                                    {{--                                                                    : {{ $orderItem->oitem_item_sku_vendor_price }}--}}


                                                                    {!! currencySymbol($item->order_currency)  !!}
                                                                    {{  $oitem_item_sku_vendor_price  }}
                                                                </div>
                                                                <div>
                                                                    VDC Price
                                                                    : {!! currencySymbol($item->order_currency)  !!} {{ $oitem_item_sku_vdc_final_price }} </div>
                                                                <div>
                                                                    Sale Price
                                                                    : {!! currencySymbol($item->order_currency)  !!} {{ $oitem_item_sku_store_price }} </div>
                                                            </div>
                                                        </div>


                                                    @endforeach
                                                @endif


                                            </td>
                                            <td>


                                                <div class="card">
                                                    <div class="card-body">
                                                        <div>
                                                            Vendor Price :
                                                            {{--                                                                    : {{ $orderItem->oitem_item_sku_vendor_price }}--}}


                                                            {!! currencySymbol($item->order_currency)  !!}
                                                            {{ curencyConvert($item->order_currency,$oitem_sku_vendor_price) }}
                                                        </div>
                                                        <div>
                                                            VDC Price
                                                            : {!! currencySymbol($item->order_currency)  !!}
                                                            {{ curencyConvert($item->order_currency, $oitem_sku_vdc_final_price) }} </div>
                                                        <div>
                                                            Sale Price
                                                            : {!! currencySymbol($item->order_currency)  !!}
                                                            {{ curencyConvert($item->order_currency, $oitem_sku_store_price) }} </div>

                                                        <div>

                                                            <?php
                                                            $iten_store_price = curencyConvert("USD", ($oitem_sku_store_price));
                                                            $item_sku_vendor_price = curencyConvert("USD", ($oitem_sku_vendor_price));

                                                            ?>


                                                            Profit Price
                                                            :  {!! currencySymbol('USD')  !!}
                                                            {{ ($iten_store_price-$item_sku_vendor_price) }}
                                                        </div>
                                                    </div>
                                                </div>


                                            </td>
                                            <td>{!! OrderStatus($item->order_status) !!} </td>
                                            <td>{{ OrderPaymentStatus($item->order_payment_status) }}</td>
                                            <td>


                                                <a class="dropdown-item"
                                                   href="{{ route('userOrdersDetails',['id'=>$item->order_id]) }}"><i
                                                        class="fa fa-eye"></i> View</a>
                                                <form method="POST" id="orders"
                                                      action="{{ route('adminorders') }}"
                                                      accept-charset="UTF-8" class="form-horizontal"
                                                      style="display:inline">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="order_id"
                                                           value="{{ $item->order_id }}"/>
                                                    <button type="submit" class="dropdown-item"
                                                            title="Delete Order"
                                                            onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                        <i
                                                            class="fa fa-trash"></i> Delete
                                                    </button>

                                                </form>

                                            </td>

                                        </tr>


                                        <?php
                                        $ordersPrice = $ordersPrice + $item->order_total_price;

                                        $order_sku_vendor_price = $order_sku_vendor_price + $oitem_sku_vendor_price;
                                        $order_sku_vdc_final_price = $order_sku_vdc_final_price + $oitem_sku_vdc_final_price;
                                        $order_sku_store_price = $order_sku_store_price + $oitem_sku_store_price;
                                        ?>


                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="11">No records found</td>
                                    </tr>
                                @endif
                                </tbody>


                                <tfoot>
                                <tr>
                                    <th scope="col">

                                    </th>
                                    <th scope="col">

                                    </th>
                                    <th scope="col">

                                    </th>
                                    <th scope="col">
                                        Total :  {!! currencySymbol('USD')  !!}
                                        {{ $ordersPrice }}
                                    </th>
                                    <th scope="col">

                                    </th>
                                    <th scope="col">

                                    </th>
                                    <th scope="col">

                                        <div class="card">
                                            <div class="card-body">
                                                <div>
                                                    Total Vendor Price :
                                                    {{--                                                                    : {{ $orderItem->oitem_item_sku_vendor_price }}--}}


                                                    {!! currencySymbol('USD')  !!}
                                                    {{ curencyConvert('USD', $order_sku_vendor_price) }}
                                                </div>
                                                <div>
                                                    Total  VDC Price
                                                    :  {!! currencySymbol('USD')  !!}
                                                    {{ curencyConvert("USD", $order_sku_vdc_final_price) }} </div>
                                                <div>
                                                    Total Sale Price
                                                    :  {!! currencySymbol('USD')  !!}
                                                    {{ curencyConvert("USD", $order_sku_store_price) }} </div>
                                                <div>

                                                    <?php
                                                    $total_store_price = curencyConvert("USD", ($order_sku_store_price));
                                                    $total_sku_vendor_price = curencyConvert("USD", ($order_sku_vendor_price));

                                                    ?>


                                                        Total  Profit Price
                                                    :  {!! currencySymbol('USD')  !!}
                                                    {{ ($total_store_price-$total_sku_vendor_price) }}
                                                </div>
                                            </div>
                                        </div>


                                    </th>
                                    <th scope="col">

                                    </th>
                                    <th scope="col">

                                    </th>
                                    <th scope="col">

                                    </th>
                                </tr>
                                </tfoot>

                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row px-3">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $orders->firstItem() }}
                        to {{ $orders->lastItem() }} of {{ $orders->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $orders->appends(['search' => Request::get('search'),'search_orderid' => Request::get('search_orderid'),'search_from_date' => Request::get('search_from_date'),'search_to_date' => Request::get('search_to_date'),'search_country' => Request::get('search_country'),'search_status' => Request::get('search_status')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

    <script>
        $('.datepicker_hidepastdates').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy',
            yearRange: "-100:+0",
        });
    </script>
@endsection
