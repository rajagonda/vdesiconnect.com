<!doctype html>
<head>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Lolokplease - Admin">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    @include('backend._partials.stylesheets')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">

    @yield('headerStyles')
</head>

<body>

@include('backend._partials.leftsidemenu')


<div class="page">


@include('backend._partials.header')



@yield('content')

<!-- end page container -->
    @include('backend._partials.footer')


</div>


@include('backend._partials.scripts')
<!-- include summernote css/js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>

<script>
    $(function () {
        $('.ckeditor').summernote({
            placeholder: 'Enter Message..',
            tabsize: 2,

            height: 150,
            popover: {
                image: [],
                link: [],
                air: []
            }
        });
    })





</script>

@yield('footerScripts')

</body>
