@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')
    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'countries'=>'Countries',
               ''=>'Add New'
               ),'Add New Country'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="countries" action="{{ route('addNewCountries') }}"
                                  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="country_id" value="{{ $country_id }}">

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Country Name</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="country_name" type="text"
                                               placeholder="Country Name"
                                               name="country_name"
                                               value="{{ !empty(old('country_name')) ? old('country_name') : ((($country) && ($country->country_name)) ? $country->country_name : '') }}">
                                        @if ($errors->has('country_name'))
                                            <span class="text-danger help-block">{{ $errors->first('country_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Country Code</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="country_code" type="text"
                                               placeholder="Country Code"
                                               name="country_code"
                                               value="{{ !empty(old('country_code')) ? old('country_code') : ((($country) && ($country->country_code)) ? $country->country_code : '') }}">
                                        @if ($errors->has('country_code'))
                                            <span class="text-danger help-block">{{ $errors->first('country_code') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Mobile Prefix</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="country_phone_prefix" type="text"
                                               placeholder="Mobile Prefix"
                                               name="country_phone_prefix"
                                               value="{{ !empty(old('country_phone_prefix')) ? old('country_phone_prefix') : ((($country) && ($country->country_phone_prefix)) ? $country->country_phone_prefix : '') }}">
                                        @if ($errors->has('country_phone_prefix'))
                                            <span class="text-danger help-block">{{ $errors->first('country_phone_prefix') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Country currency</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="country_currency" type="text"
                                               placeholder="Country currency"
                                               name="country_currency"
                                               value="{{ !empty(old('country_currency')) ? old('country_currency') : ((($country) && ($country->country_currency)) ? $country->country_currency : '') }}">
                                        @if ($errors->has('country_currency'))
                                            <span class="text-danger help-block">{{ $errors->first('country_currency') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Country language</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="country_language" type="text"
                                               placeholder="Country language"
                                               name="country_language"
                                               value="{{ !empty(old('country_language')) ? old('country_language') : ((($country) && ($country->country_language)) ? $country->country_language : '') }}">
                                        @if ($errors->has('country_language'))
                                            <span class="text-danger help-block">{{ $errors->first('country_language') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Country Weight Description</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="country_weight_description" type="text"
                                               placeholder="Country Weight Description"
                                               name="country_weight_description"
                                               value="{{ !empty(old('country_weight_description')) ? old('country_weight_description') : ((($country) && ($country->country_weight_description)) ? $country->country_weight_description : '') }}">
                                        @if ($errors->has('country_weight_description'))
                                            <span class="text-danger help-block">{{ $errors->first('country_weight_description') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Status</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="country_status" id="country_status" class="form-control">
                                            <?php
                                            $status = allStatuses('general'); ?>
                                            @foreach($status as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('country_status')) && old('country_status')==$ks)  ? 'selected' : ((($country) && ($country->country_status == $ks)) ? 'selected' : '') }}
                                                >
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('country_status'))
                                            <span class="text-danger help-block">{{ $errors->first('country_status') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    <script src="{{url('backend/modules/countries/js/countries.js')}}"></script>

    @include('backend.countries.script')

@endsection

