<script>
    $(function () {
        $('#countries').validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            errorPlacement: function (error, e) {
                e.parents('.form-group').append(error);
            },
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.text-danger').remove();
            },
            success: function (e) {
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.text-danger').remove();
            },
            rules: {
                country_name: {
                    required: true
                },
                country_code: {
                    required: true
                },
                country_phone_prefix: {
                    required: true
                },
                country_currency: {
                    required: true
                },
                country_language: {
                    required: true
                },
                country_status: {
                    required: true
                }
            },
            messages: {
                country_name: {
                    required: 'Please enter Country Name'
                },
                country_code: {
                    required: 'Please enter Country Code'
                },
                country_phone_prefix: {
                    required: 'Please enter Phone Prefix'
                },
                country_language: {
                    required: 'Please select Country Language'
                },
                country_status: {
                    required: 'Please select status'
                }
            },
        });

    });


</script>