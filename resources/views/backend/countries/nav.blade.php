<div class="btn-group px-3" role="group" aria-label="Basic example">
    <a href="{{ route('countries') }}" class="btn btn-secondary @if ($sub_active_menu=='countries-list') active @endif">
        Countries
    </a>
    <a href="{{ route('States',['search'=>'search','search_country'=>'1']) }}" class="btn btn-secondary @if ($sub_active_menu=='states-list') active @endif">
        States
    </a>
    <a href="{{ route('Cities') }}" class="btn btn-secondary @if ($sub_active_menu=='cities-list') active @endif">
        Cities
    </a>
</div>