<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice From VdesiConnect</title>
</head>

<body style="margin:0; padding:0;">

<?php
$service_currency = 'USD';

//dd($bladeData);

?>

<table cellpadding="0", cellspacing="0" style="width:650px; margin:20px auto; font-family: sans-serif; font-size:14px;">
    <!-- header row -->
    <tr>
        <td width="50%">&nbsp;</td>
        <td width="50%" align="right">
            <a href="{{route('ServiceInvoices',['action'=>'viewInvoice','id'=>$bladeData->id])}}" style="text-decoration:none; font-size:14px; text-transform:uppercase; color:#8BC34A; border:1px solid #8BC34A; padding:10px;">
                Pay Now
            </a>
        </td>
    </tr>
    <!-- heght row -->
    <tr height="20px">
        <td colspan="2"></td>
    </tr>
    <!--/ height row -->
    <tr>
        <td>
            <table>

                <tr>
                    <td><img src="https://www.vdesiconnect.com/logo.png" width="50"></td>
                    <td style="padding:10px">
                        <h3 style="margin:0; padding:0 0 5px 0; color:#8BC34A;">VDESI CONNECT</h3>
                        <div style="margin:0; padding:0;">
                            {!! vdcSettings()->address !!}
                        </div>
                    </td>
                </tr>
            </table>
        </td>
        <td bgcolor="#8BC34A" align="right">
            <table width="100%" style="height:65px;" >
                <tr>
                    <td style="color:#fff" align="right">+91-9100311263</td>
                    <td style="color:#fff" align="right"><a href="javascript:void(0)" style="color: #fff; text-decoration: none;">{{commonSettings('adminEmail')}}</a></td>
                </tr>
            </table>
        </td>
    </tr>
    <!--/ table row -->

    <!-- heght row -->
    <tr height="100px">
        <td colspan="2"></td>
    </tr>
    <!--/ height row -->

    <tr>
        <td>

            <h4><b>INVOICE TO</b></h4>
            <p style="margin:0; padding:0 0 5px 0; color:#8BC34A; font-size:16px;">{{$bladeData->s_contact_person}}</p>
            <p style="line-height:20px; padding:0; margin:0;">{{$bladeData->s_address_line_1}},{{$bladeData->s_address_line_2}}</p>
            <p style="line-height:20px; padding:0; margin:0;">Phone No: {{$bladeData->s_phone}}</p>
            <p style="line-height:20px; padding:0; margin:0;"><a href="javascript:void(0)" style="text-decoration: none;">{{$bladeData->s_email}}</a></p>
        </td>
        <td align="right">
            <h1 style="color:#8BC34A; margin:0 0 10px 0; padding:0;">INVOICE</h1>

            <p>INVOICE NO: <strong>VDC-SERVICE-{{ $bladeData->s_id }}</strong></p>

            Date of Invoice: {{$bladeData->created_at}}<br>

            Due Date: {{$bladeData->s_duedate}}


{{--            <p style="margin:0; padding:0;">Date of Invoice: {{$bladeData->created_at}}</p>--}}
{{--            <p style="margin:0; padding:5px 0;"> Due Date: {{$bladeData->invoice->s_duedate}}</p>--}}
        </td>
    </tr>
    <!-- heght row -->
    <tr height="50px">
        <td colspan="2"></td>
    </tr>
    <!--/ height row -->

    <tr>
        <td colspan="2">
            <table width="100%" cellpadding="10" cellspacing="2" border="0">
                <thead>
                <tr>
                    <th width="55%" bgcolor="#8BC34A" align="left" style="color:#fff">Description</th>
                    <th width="15%" bgcolor="#8BC34A" align="center" style="color:#fff">Quantity</th>
                    <th width="15%" bgcolor="#8BC34A" align="center" style="color:#fff">Unit price</th>
                    <th width="15%" bgcolor="#8BC34A" align="center" style="color:#fff">Total</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td bgcolor="E8F3DB" style="border-bottom:1px solid #ccc; line-height: 20px">
                        <h3 style="margin:0; padding:0 0 10px 0; color:#8BC34A;">

                            {{ $bladeData->getService->s_options  }} ( {{ wordSort($bladeData->getService->s_type)  }} )


                        </h3>
                        {{ $bladeData->s_duration  }}</td>
                    <td bgcolor="E8F3DB" align="center">
                        {{ $bladeData->s_no_of_days  }}
                        {{ $bladeData->s_unit_type }}s
                    </td>
                    <td bgcolor="E8F3DB" align="center"> {!! currencySymbol($bladeData->service_currency)  !!} {{ $bladeData->s_unit_price  }} / {{ $bladeData->s_unit_type  }}</td>
                    <td bgcolor="E8F3DB" align="center">
                        <?php
                        $subtotal[] = $bladeData->s_no_of_days * $bladeData->s_unit_price;
                        ?>
                            {!! currencySymbol($service_currency)  !!} {{ array_sum($subtotal) }}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2" bgcolor="E8F3DB" align="right">Sub Total</td>
                    <td bgcolor="E8F3DB" align="center" style="color:#8BC34A;"> {!! currencySymbol($service_currency)  !!} {{ array_sum($subtotal)  }}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2" bgcolor="E8F3DB" align="right">TAX {{$bladeData->s_tax}}%:</td>
                    <td bgcolor="E8F3DB" align="center" style="color:#8BC34A;">
                        <?php
                        $taxAmount = array_sum($subtotal) /  100 * $bladeData->s_tax;
                        ?>
                            {!! currencySymbol($service_currency)  !!} {{ $taxAmount  }}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2" align="right" bgcolor="#8BC34A"><h3 style="margin:0; padding:0; color:#fff;">Grand Total</h3></td>
                    <td align="center" bgcolor="#8BC34A"><h3 style="margin:0; padding:0; color:#fff;">
                            <?php
                            $total = array_sum($subtotal) + $taxAmount;
                            ?>
                                {!! currencySymbol($service_currency)  !!} {{$total}}</h3></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <!-- heght row -->
    <tr height="50px">
        <td colspan="2"></td>
    </tr>
    <!--/ height row -->

    <tr>
        <td  style="color:#8BC34A;"><h4>Thank You</h4></td>
        <td align="right"><h4>Payment Status: <span style="color:#8BC34A;">{{$bladeData->s_status}}</span></h4></td>
    </tr>

    <!-- heght row -->
    <tr height="50px">
        <td colspan="2"></td>
    </tr>
    <!--/ height row -->

    <tr>
        <td colspan="2">
            <h3 style="color:#8BC34A; margin:0; padding:0 0 10px 0;">Notice</h3>
{{--            <p style="margin:0; padding:0 0 5px 0;"> A finance charge of 1.5% will be made on unpaid balances after 30 days.</p>--}}
            <p style="margin:0; padding:0 0 5px 0;"> Invoice was created on a computer and is valid without the signature and seal.</p>
        </td>

    </tr>
</table>
</body>

</html>
