@extends('backend.layout')
@section('title', $title)

@section('headerStyles')
@endsection

@section('footerScripts')


@endsection


@section('content')

    <!-- Counts Section -->
    <section class="dashboard-counts section-padding">
        <div class="container-fluid">
            <div class="row">
                <!-- Count item widget-->

                <div class="col-xl-2 col-md-4 col-6">
                    <div class="wrapper count-title d-flex">
                        <div class="icon"><i class="icon-user"></i></div>
                        <div class="name"><strong class="text-uppercase">Active Users</strong>
                            <div class="count-number"><a href="{{route('users')}}">{{$userscount}}</a></div>
                        </div>
                    </div>
                </div>

                <!-- Count item widget-->
                <div class="col-xl-2 col-md-4 col-6">
                    <div class="wrapper count-title d-flex">
                        <div class="icon"><i class="icon-padnote"></i></div>
                        <div class="name"><strong class="text-uppercase">Active Vendors</strong>
                            <div class="count-number"><a href="{{route('vendorslist')}}">{{$vendorscount}}</a></div>
                        </div>
                    </div>
                </div>
                <!-- Count item widget-->
                <div class="col-xl-2 col-md-4 col-6">
                    <div class="wrapper count-title d-flex">
                        <div class="icon"><i class="icon-check"></i></div>
                        <div class="name"><strong class="text-uppercase">Active Products</strong>
                            <div class="count-number"><a href="{{route('admin_products')}}">{{$productscount}}</a></div>
                        </div>
                    </div>
                </div>
                <!-- Count item widget-->
                <div class="col-xl-2 col-md-4 col-6">
                    <div class="wrapper count-title d-flex">
                        <div class="icon"><i class="icon-bill"></i></div>
                        <div class="name"><strong class="text-uppercase">Total Orders</strong>
                            <div class="count-number"><a href="{{route('adminorders')}}">{{$orderscount}}</a></div>
                        </div>
                    </div>
                </div>
                <!-- Count item widget-->
                <div class="col-xl-2 col-md-4 col-6">
                    <div class="wrapper count-title d-flex">
                        <div class="icon"><i class="icon-list"></i></div>
                        <div class="name"><strong class="text-uppercase">Product Enquiries</strong>
                            <div class="count-number"><a href="{{route('product_enquiries')}}">{{$productenquiries}}</a></div>
                        </div>
                    </div>
                </div>
                <!-- Count item widget-->
                <div class="col-xl-2 col-md-4 col-6">
                    <div class="wrapper count-title d-flex">
                        <div class="icon"><i class="icon-list-1"></i></div>
                        <div class="name"><strong class="text-uppercase">Service Enquiries</strong>
                            <div class="count-number">{{$serviceenquiries}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- Header Section-->

    <!-- Statistics Section-->
    {{--<section class="statistics">--}}
        {{--<div class="container-fluid">--}}
            {{--<div class="row d-flex">--}}
                {{--<div class="col-lg-4">--}}
                    {{--<!-- Income-->--}}
                    {{--<div class="card income text-center">--}}
                        {{--<div class="icon"><i class="icon-line-chart"></i></div>--}}
                        {{--<div class="number">126,418</div>--}}
                        {{--<strong class="text-primary">All Income</strong>--}}
                        {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do.</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-4">--}}
                    {{--<!-- Monthly Usage-->--}}
                    {{--<div class="card data-usage">--}}
                        {{--<h2 class="display h4">Monthly Usage</h2>--}}
                        {{--<div class="row d-flex align-items-center">--}}
                            {{--<div class="col-sm-6">--}}
                                {{--<div id="progress-circle"--}}
                                     {{--class="d-flex align-items-center justify-content-center"></div>--}}
                            {{--</div>--}}
                            {{--<div class="col-sm-6"><strong class="text-primary">80.56 Gb</strong>--}}
                                {{--<small>Current Plan</small>--}}
                                {{--<span>100 Gb Monthly</span></div>--}}
                        {{--</div>--}}
                        {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing.</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-4">--}}
                    {{--<!-- User Actibity-->--}}
                    {{--<div class="card user-activity">--}}
                        {{--<h2 class="display h4">User Activity</h2>--}}
                        {{--<div class="number">210</div>--}}
                        {{--<h3 class="h4 display">Social Users</h3>--}}
                        {{--<div class="progress">--}}
                            {{--<div role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0"--}}
                                 {{--aria-valuemax="100" class="progress-bar progress-bar bg-primary"></div>--}}
                        {{--</div>--}}
                        {{--<div class="page-statistics d-flex justify-content-between">--}}
                            {{--<div class="page-statistics-left"><span>Pages Visits</span><strong>230</strong></div>--}}
                            {{--<div class="page-statistics-right"><span>New Visits</span><strong>73.4%</strong></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}
    <!-- Updates Section -->
    <section class="mt-30px mb-30px">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <!-- Recent Updates Widget          -->
                    <div id="new-updates" class="card updates recent-updated">
                        <div id="updates-header" class="card-header d-flex justify-content-between align-items-center">
                            <h2 class="h5 display"><a data-toggle="collapse" data-parent="#new-updates"
                                                      href="#updates-box" aria-expanded="true"
                                                      aria-controls="updates-box">New Vendors</a></h2>
                            {{--<a data-toggle="collapse" data-parent="#new-updates" href="#updates-box"--}}
                                    {{--aria-expanded="true" aria-controls="updates-box"><i--}}
                                        {{--class="fa fa-angle-down"></i></a>--}}
                        </div>
                        <div id="updates-box" role="tabpanel" class="collapse show">
                            <ul class="news list-unstyled">
                                <!-- Item-->

                                @if(count($newvendorlist)>0)
                                    @foreach($newvendorlist as $newvendor)
                                <li class="d-flex justify-content-between">
                                    <div class="left-col d-flex">
                                        <div class="icon"><i class="icon-rss-feed"></i></div>
                                        <div class="title"><strong><a href="{{route('admin_vendor_account_details',['id'=>$newvendor->id])}}" target="_blank">{{$newvendor->name}}</a></strong>
                                            <p>{{$newvendor->email}}</p>
                                        </div>
                                    </div>
                                    <div class="right-col text-right">
                                        <div class="update-date">{{ \Carbon\Carbon::parse($newvendor->created_at)->format('d')}}<span class="month">{{ \Carbon\Carbon::parse($newvendor->created_at)->format('M')}}</span></div>
                                    </div>
                                </li>
                                 @endforeach
                                @endif

                            </ul>
                        </div>
                    </div>
                    <!-- Recent Updates Widget End-->
                </div>
                <div class="col-lg-4 col-md-12">
                    <!-- Recent Updates Widget          -->
                    <div id="new-updates" class="card updates recent-updated">
                        <div id="updates-header" class="card-header d-flex justify-content-between align-items-center">
                            <h2 class="h5 display"><a data-toggle="collapse" data-parent="#new-updates"
                                                      href="#updates-box" aria-expanded="true"
                                                      aria-controls="updates-box">New Users</a></h2>
                            {{--<a data-toggle="collapse" data-parent="#new-updates" href="#updates-box"--}}
                                    {{--aria-expanded="true" aria-controls="updates-box"><i--}}
                                        {{--class="fa fa-angle-down"></i></a>--}}
                        </div>
                        <div id="updates-box" role="tabpanel" class="collapse show">
                            <ul class="news list-unstyled">
                                <!-- Item-->

                                @if(count($newuserslist)>0)
                                    @foreach($newuserslist as $newuser)
                                        <li class="d-flex justify-content-between">
                                            <div class="left-col d-flex">
                                                <div class="icon"><i class="icon-rss-feed"></i></div>
                                                <div class="title"><strong><a href="{{route('userAddress',['id'=>$newuser->id])}}" target="_blank">{{$newuser->name}}</a></strong>
                                                    <p>{{$newuser->email}}</p>
                                                </div>
                                            </div>
                                            <div class="right-col text-right">
                                                <div class="update-date">{{ \Carbon\Carbon::parse($newuser->created_at)->format('d')}}<span class="month">{{ \Carbon\Carbon::parse($newuser->created_at)->format('M')}}</span></div>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif

                            </ul>
                        </div>
                    </div>
                    <!-- Recent Updates Widget End-->
                </div>

                <div class="col-lg-4 col-md-12">
                    <!-- Recent Updates Widget          -->
                    <div id="new-updates" class="card updates recent-updated">
                        <div id="updates-header" class="card-header d-flex justify-content-between align-items-center">
                            <h2 class="h5 display"><a data-toggle="collapse" data-parent="#new-updates"
                                                      href="#updates-box" aria-expanded="true"
                                                      aria-controls="updates-box">New Orders</a></h2>
                            {{--<a data-toggle="collapse" data-parent="#new-updates" href="#updates-box"--}}
                                    {{--aria-expanded="true" aria-controls="updates-box"><i--}}
                                        {{--class="fa fa-angle-down"></i></a>--}}
                        </div>
                        <div id="updates-box" role="tabpanel" class="collapse show">
                            <ul class="news list-unstyled">
                                <!-- Item-->

                                @if(count($neworders)>0)
                                    @foreach($neworders as $neworder)



                                        <li class="d-flex justify-content-between">
                                            <div class="left-col d-flex">
                                                <div class="icon"><i class="icon-rss-feed"></i></div>
                                                <div class="title">
                                                    <strong>
                                                        <a href="{{route('userOrdersDetails',['id'=>$neworder->order_id])}}" target="_blank">

                                                            {{ (isset($neworder->getUser))? $neworder->getUser->name :'' }}


                                                        </a>
                                                    </strong>
                                                    <p>
                                                        {{ (isset($neworder->getUser))? $neworder->getUser->email :'' }}

                                                    </p>
                                                </div>
                                            </div>
                                            <div class="right-col text-right">
                                                <div class="update-date">
                                                    {!! currencySymbol('USD')  !!} {{ curencyConvert('USD', $neworder->order_total_price) }}
{{--                                                    Rs. {{ $neworder->order_total_price }} /---}}
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif

                            </ul>
                        </div>
                    </div>


                    <!-- Recent Updates Widget End-->
                </div>
            </div>
        </div>
    </section>



@endsection


