@extends('backend.layout')
@section('title', $title)

@section('headerStyles')


@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Reports'
              ),'Reports'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif


                    <form method="GET" action="{{ route('adminreports') }}" accept-charset="UTF-8"
                          class="navbar-form navbar-right" role="search" autocomplete="off">
                        <input type="hidden" name="search" value="search">

                        <div class="row">
                            &nbsp;&nbsp;


                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Order ID</label>
                                    <div class="input-group">
                                        <input type="text" name="order_id"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Product Name</label>
                                    <div class="input-group">
                                        <input type="text" name="order_id"/>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-2">

                                <div class="form-group">
                                    <label>Vendor</label>
                                    <div class="input-group">
                                        <select class="form-control" name="search_vendor" id="search_vendor">
                                            <option value="">Select Vendor</option>
                                            @if(count($vendor_list) > 0)
                                                @foreach($vendor_list AS $vendor)
                                                    <option value="{{$vendor->id}}" {{ (app('request')->input('search_vendor')==$vendor->id)  ? 'selected' : ''}}>{{$vendor->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>


                            </div>
                            <div class="col-md-2">

                                <div class="form-group">
                                    <label>From date</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker_hidepastdates"
                                               name="search_from_date"
                                               placeholder="From Date"
                                               value="{{ request('search_from_date') }}">
                                    </div>
                                </div>


                            </div>
                            <div class="col-md-2">


                                <div class="form-group">
                                    <label>To date</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker_hidepastdates"
                                               name="search_to_date"
                                               placeholder="To Date"
                                               value="{{ request('search_to_date') }}">
                                    </div>
                                </div>


                            </div>


                            <div class="col-md-1">
                                &nbsp;
                                <div class="form-group">
                                    <div class="input-group">
                                                     <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
{{--                                                 <a href="{{route('adminreports')}}" class="btn btn-default">Reset</a>--}}
                                </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>

                    <br/>


                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Products</strong>

                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Order ID</th>
                                    <th scope="col">Product</th>
                                    <th scope="col">Vendor</th>
                                    <th scope="col">Vendor Price</th>
                                    <th scope="col">sel Price</th>
                                    <th scope="col">Profit Price</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($orderItems)>0)
                                    @foreach($orderItems as $item)
                                        <tr>
                                            <th scope="row">

                                                <?php
//                                                                                                dump($item);
                                                ?>

                                              <a href="{{ route('userOrdersDetails',['id'=>$item->oitem_order_id]) }}">
                                                  {{ $item->oitem_order_id }}
                                              </a>
                                            </th>
                                            <th scope="row">
                                                {{ isset($item->getProduct)? $item->getProduct->p_name : '' }}
                                            </th>
                                            <th scope="row">
                                                {{ isset( $item->getProduct->getVendor) ? $item->getProduct->getVendor->name : '' }}
                                            </th>
                                            <th scope="row">
{{--                                                Vendor Price--}}

                                                {{ $item->oitem_item_sku_vendor_price }}
                                            </th>
                                            <th scope="row">
{{--                                                sel Price--}}
                                                {{ $item->oitem_item_sku_store_price }}
                                            </th>
                                            <th scope="row">
{{--                                                Profit Price--}}
                                                {{ $item->oitem_item_sku_store_price - $item->oitem_item_sku_vendor_price }}
                                            </th>

                                        </tr>



                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="11">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row px-3">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $orderItems->firstItem() }}
                        to {{ $orderItems->lastItem() }} of {{ $orderItems->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $orderItems->appends(['search' => Request::get('search'),'search_vendor' => Request::get('search_vendor')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

    <script>
        $('.datepicker_hidepastdates').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy',
            yearRange: "-100:+0",
        });
    </script>
@endsection
