@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')
    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'blogs'=>'Blogs',
               ''=>'Add New'
               ),'Add New Blog'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="blogs" action="{{ route('addNewBlogs') }}"
                                  accept-charset="UTF-8" class="form-horizontal"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="blog_id" value="{{ $blog_id }}">


                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Title</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="blog_title" type="text"
                                               placeholder="Title"
                                               name="blog_title"
                                               value="{{ !empty(old('blog_title')) ? old('blog_title') : ((($blog) && ($blog->blog_title)) ? $blog->blog_title : '') }}">
                                        @if ($errors->has('blog_title'))
                                            <span class="text-danger help-block">{{ $errors->first('blog_title') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group ">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class="form-control-label">Image</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control {{ (($blog) && ($blog->blog_image)) ? '' : 'blogimage'}}"
                                               id="blogimage" type="file" name="blog_image"/>
                                        <small>Size: 1400/500</small>
                                        @if ($errors->has('blog_image'))
                                            <span class="text-danger">{{ $errors->first('blog_image') }}</span>
                                        @endif

                                        @if(($blog) && ($blog->blog_image))
                                            <div class="imagebox imagediv{{ $blog->blog_id }}">
                                                <img width="150" src="/uploads/blogs/{{$blog->blog_image}}"/>
                                                <a href="#" id="{{$blog->blog_id}}"
                                                   class="delete_blog_image btn btn-danger btn-xs"
                                                   onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i>Delete </a>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Description</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <textarea name="blog_description" id="blog_description" rows="9"
                                                  placeholder="Description..."
                                                  class="form-control ckeditor">{{ !empty(old('blog_description')) ? old('blog_description') : ((($blog) && ($blog->blog_description)) ? $blog->blog_description : '') }}</textarea>
                                        @if ($errors->has('blog_description'))
                                            <span class="text-danger help-block">{{ $errors->first('blog_description') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Status</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="blog_status" id="blog_status" class="form-control">
                                            <?php
                                            $status = allStatuses('general'); ?>
                                            @foreach($status as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('blog_status')) && old('blog_status')==$ks)  ? 'selected' : ((($blog) && ($blog->blog_status == $ks)) ? 'selected' : '') }}
                                                >
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('blog_status'))
                                            <span class="text-danger help-block">{{ $errors->first('blog_status') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    @include('backend.blogs.script')

@endsection