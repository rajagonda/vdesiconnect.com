@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #3c8dbc;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'States'
              ),'States'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                        @include('backend.countries.nav')
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">City</strong>

                            <a href="#" data-toggle="modal" style="float: right"  data-target="#addCity"  class="btn btn-primary"> + Add City </a>

                            <div id="addCity" class="modal fade"
                                 role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title">
                                                Add City
                                            </h4>
                                            <button type="button" class="close"
                                                    data-dismiss="modal">
                                                &times;
                                            </button>
                                        </div>
                                        <form method="POST"
                                              action="{{ route('addNewCity') }}" autocomplete="off">
                                            <div class="modal-body">

                                                <div class="modal-body">
                                                    {{ csrf_field() }}
                                                    <div class="form-group">

                                                        <label>Select Country</label>
                                                            <select class="form-control" name="city_country_id" required>
                                                                <option value="">Select Country</option>
                                                                @if(count($countries) > 0)
                                                                    @foreach($countries AS $country)
                                                                        <option value="{{$country->country_id}}">{{$country->country_name}}</option>
                                                                    @endforeach

                                                                @endif

                                                            </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Select State</label>
                                                        <select class="form-control" name="city_state_id" required>
                                                            <option value="">Select State</option>
                                                            @if(count($states) > 0)
                                                                @foreach($states AS $state)
                                                                    <option
                                                                            value="{{$state->state_id}}">{{$state->state_name}}</option>
                                                                @endforeach

                                                            @endif

                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>City Name</label>
                                                        <input id="city_name" type="text"
                                                               placeholder="City Name"
                                                               class="form-control" required
                                                               name="city_name">
                                                    </div>


                                                    <div class=" form-group">

                                                        <label >Status</label>

                                                            <select name="city_status" id="city_status"
                                                                    class="form-control" required>
                                                                <?php
                                                                $status = allStatuses('general'); ?>
                                                                @foreach($status as $ks=>$s)
                                                                    <option value="{{ $ks }}">
                                                                        {{ $s }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                            @if ($errors->has('city_status'))
                                                                <span class="text-danger help-block">{{ $errors->first('city_status') }}</span>
                                                            @endif
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close </button>
                                                <button type="submit" name="submit" class="btn btn-primary">Create
                                                </button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">City</th>
                                    <th scope="col">State</th>
                                    <th scope="col">Country</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($cities)>0)
                                    @foreach($cities as $item)
                                        <tr>
                                            <th scope="row">{{ $item->city_id }}</th>
                                            <td>{{ $item->city_name }}</td>
                                            <td>{{ $item->city_state_id }}</td>
                                            <td>{{ $item->city_country_id }}</td>
                                            <td>{{ allStatuses('general',$item->city_status) }}</td>
                                            <td>
                                                <a href="#" data-toggle="modal"
                                                   data-target="#editCity_{{$item->city_id}}"
                                                   class="dropdown-item">
                                                    <i class="fa fa-pencil"></i> Update </a>

                                                <div id="editCity_{{$item->city_id}}" class="modal fade"
                                                     role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">
                                                                    Update State
                                                                </h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">
                                                                    &times;
                                                                </button>
                                                            </div>

                                                            <form method="POST"
                                                                  action="{{ route('editCity') }}" autocomplete="off">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="city_id"
                                                                       value="{{$item->city_id}}">
                                                                <div class="modal-body">

                                                                    <div class="modal-body">

                                                                        <div class="form-group">

                                                                            <label>Select Country</label>


                                                                            <select class="form-control" name="state_country_id" required>
                                                                                <option value="">Select Country</option>
                                                                                @if(count($countries) > 0)
                                                                                    @foreach($countries AS $country)
                                                                                        <option value="{{$country->country_id}}" {{ (!empty(old('city_country_id')) && old('city_country_id')==$country->country_id)  ? 'selected' : ((($item->city_country_id) && ($item->city_country_id == $country->country_id)) ? 'selected' : '') }}>{{$country->country_name}}</option>
                                                                                    @endforeach

                                                                                @endif

                                                                            </select>
                                                                        </div>


                                                                        <div class="form-group">

                                                                            <label>Select State</label>


                                                                            <select class="form-control" name="state_country_id" required>
                                                                                <option value="">Select State</option>
                                                                                @if(count($states) > 0)
                                                                                    @foreach($states AS $state)
                                                                                        <option
                                                                                                value="{{$state->state_id}}" {{ (!empty(old('city_state_id')) && old('city_state_id')==$state->state_id)  ? 'selected' : ((($item->city_state_id) && ($item->city_state_id == $state->state_id)) ? 'selected' : '') }}>{{$state->state_name}}</option>
                                                                                    @endforeach

                                                                                @endif

                                                                            </select>
                                                                        </div>


                                                                        <div class="form-group">
                                                                            <label>City Name</label>
                                                                            <input id="city_name" type="text"
                                                                                   placeholder="City Name"
                                                                                   value="{{$item->city_name}}"
                                                                                   class="form-control" required
                                                                                   name="city_name">
                                                                        </div>


                                                                        <div class=" form-group">

                                                                            <label >Status</label>

                                                                            <select name="city_status" id="state_status"
                                                                                    class="form-control" required>
                                                                                <?php
                                                                                $status = allStatuses('general'); ?>
                                                                                @foreach($status as $ks=>$s)
                                                                                    <option value="{{ $ks }}" {{ (!empty(old('city_status')) && old('city_status')==$ks)  ? 'selected' : ((($item->city_status) && ($item->city_status == $ks)) ? 'selected' : '') }}>
                                                                                        {{ $s }}
                                                                                    </option>
                                                                                @endforeach
                                                                            </select>
                                                                            @if ($errors->has('city_status'))
                                                                                <span class="text-danger help-block">{{ $errors->first('city_status') }}</span>
                                                                            @endif

                                                                        </div>


                                                                    </div>

                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary"
                                                                            data-dismiss="modal">Close
                                                                    </button>
                                                                    <button type="submit" name="submit" class="btn btn-primary">Updae
                                                                    </button>
                                                                </div>
                                                            </form>

                                                        </div>

                                                    </div>
                                                </div>


                                                <form method="POST" id="productbox_form_{{ $item->city_id }}"
                                                      action="{{ route('Cities') }}"
                                                      accept-charset="UTF-8" class="form-horizontal product_box_form"
                                                      style="display:inline">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="city_id"
                                                           value="{{ $item->city_id }}"/>
                                                    <button type="submit" class="dropdown-item"
                                                            title="Delete City"
                                                            onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                        <i class="fa fa-trash"></i> Delete
                                                    </button>

                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="9">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row px-3">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $cities->firstItem() }}
                        to {{ $cities->lastItem() }} of {{ $cities->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $cities->appends(['filters' => Request::get('filters'),'search' => Request::get('search'),'status' => Request::get('status')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

@endsection