@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')
    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'serviceBanners'=>'Service Banners',
               ''=>'Add New'
               ),'Add New Banner'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">

                        <div class="card-body card-block">
                            {{ laravelReturnMessageShow() }}

                            <form method="POST" id="banners" action="{{ route('addNewServiceBanners') }}"
                                  accept-charset="UTF-8" class="form-horizontal"
                                  enctype="multipart/form-data" autocomplete="off">
                                {{ csrf_field() }}
                                <input type="hidden" name="banner_id" value="{{ $banner_id }}">


                                <div class=" form-group">
                                    <label for="hf-email" class=" form-control-label">Banner Title</label>
                                    <div class="input-group">
                                        <input class="form-control" id="banner_title" type="text"
                                               placeholder="Banner Title"
                                               name="banner_title"
                                               value="{{ !empty(old('banner_title')) ? old('banner_title') : ((($banner) && ($banner->banner_title)) ? $banner->banner_title : '') }}">

                                    </div>
                                    @if ($errors->has('banner_title'))
                                        <span class="text-danger help-block">{{ $errors->first('banner_title') }}</span>
                                    @endif

                                </div>

                                <div class=" form-group">

                                    <label for="hf-email" class=" form-control-label">Banner Url</label>
                                    <div class="input-group">
                                        <input class="form-control" id="banner_title" type="text"
                                               placeholder="Banner Url"
                                               name="banner_link"
                                               value="{{ !empty(old('banner_link')) ? old('banner_link') : ((($banner) && ($banner->banner_link)) ? $banner->banner_link : '') }}">
                                    </div>
                                    @if ($errors->has('banner_link'))
                                        <span class="text-danger help-block">{{ $errors->first('banner_link') }}</span>

                                    @endif
                                </div>


                                <div class=" form-group ">

                                    <label for="hf-email" class="form-control-label">Image</label>
                                    <div class="input-group">
                                        <input class="{{ (($banner) && ($banner->banner_image)) ? '' : 'bannerimage'}}"
                                               id="bannerimage" type="file" name="banner_image"/>
                                        <div>

                                            <small>Size: 1400/500</small>
                                        </div>
                                    </div>
                                    @if ($errors->has('banner_image'))
                                        <span class="text-danger">{{ $errors->first('banner_image') }}</span>
                                    @endif

                                    @if(($banner) && ($banner->banner_image))
                                        <div class="imagebox imagediv{{ $banner->banner_id }}">
                                            <img width="150" src="/uploads/service_banners/{{$banner->banner_image}}"/>
                                            <a href="#" id="{{$banner->banner_id}}"
                                               class="delete_banner_image btn btn-danger btn-xs"
                                               onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                        class="fa fa-trash-o" aria-hidden="true"></i>Delete </a>
                                        </div>
                                    @endif


                                </div>

                                <div class=" form-group">

                                    <label for="hf-email" class=" form-control-label">Status</label>
                                    <div class="input-group">
                                        <select name="banner_status" id="banner_status" class="form-control">
                                            <?php
                                            $status = allStatuses('general'); ?>
                                            @foreach($status as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('banner_status')) && old('banner_status')==$ks)  ? 'selected' : ((($banner) && ($banner->category_status == $ks)) ? 'selected' : '') }}
                                                >
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('banner_status'))
                                        <span class="text-danger help-block">{{ $errors->first('banner_status') }}</span>
                                    @endif

                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    @include('backend.serviceBanners.sctipt')


@endsection