<script>
    $(document).ready(function () {
        $('.delete_banner_image').on('click', function () {
            var image = $(this).attr('id');
            if (image != '') {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    url: '/admin/ajax/deleteservicebannerimage',
                    type: 'POST',
                    data: {'image': image},
                    success: function (response) {
                        $('.imagediv' + image).remove();
                        $('#bannerimage').addClass('bannerimage');
                        imageRules();
                    }
                });
            }
        });
        $('#banners').validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            errorPlacement: function (error, e) {
                e.parents('.form-group').append(error);
            },
            highlight: function (e) {
                $(e).closest('.input-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.text-danger').remove();
            },
            success: function (e) {
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.text-danger').remove();
            },
            rules: {
                banner_title: {
                    required: true
                },
                banner_link: {
                    required: true
                },

                banner_status: {
                    required: true
                }
            },
            messages: {
                banner_title: {
                    required: 'Please enter title'
                },
                banner_link: {
                    required: 'Please enter Url'
                },

                banner_status: {
                    required: 'Please select status'
                }
            },
        });
        imageRules();
    });

    function imageRules() {
        $(".bannerimage").rules("add", {
            required: true,
            messages: {
                required: "Upload image to save"
            }
        });
    }
</script>