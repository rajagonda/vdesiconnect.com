@extends('backend.layout')
@section('title', $title)

@section('headerStyles')



@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              'admin_service'=> 'Services',
              ),'User Address'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">


                @if (Session::has('flash_message'))
                    <br/>
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ Session::get('flash_message' ) }}</strong>
                    </div>
                @endif


                <div class="col-lg-12">
                    @include('backend.services.includs.nav')
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    Service Information
                                </div>
                                <div class="card-body">
                                    @include('backend.services.includs.updateRequestForm')
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>

        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

    <script>

        function cityShowInput(formId, countryValue, cityClass) {
            if (countryValue == 1) {
                $('#' + formId).find('.citybox').show();
                // console.log("india "+countryValue);
                // $('#' + formId).find('.ua_city')
            } else {
                // console.log("others "+countryValue);
                $('#' + formId).find('.citybox').hide();

                $(cityClass).empty();
                $(cityClass).append('<option value="">--Select City--</option>');
            }
        }


        function selectcountry(formId, inputtype) {
            var countryValue = $('#' + formId).find('.ua_country').val();
            var stateClass = $('#' + formId).find('.ua_state');
            var cityClass = $('#' + formId).find('.ua_city');

            console.log("testest " + countryValue);

            cityShowInput(formId, countryValue, cityClass);


            if (inputtype == 'country') {
                if (countryValue) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                        url: '{{ route('showStates') }}',
                        type: "POST",
                        data: {'catID': countryValue},
                        dataType: "json",
                        success: function (data) {
                            var Types = data.states;
                            $(stateClass).empty();
                            $(stateClass).append('<option value="">--Select State--</option>');
                            var stareSelected = false;
                            $.each(Types, function (key, value) {
                                var stateValue = $(stateClass).data('state');

                                if (stateValue == key) {
                                    var selected = (stateValue == key) ? "selected" : "";
                                    // console.log('Old data =>' + stateValue);
                                    $(stateClass).append('<option ' + selected + ' value="' + key + '">' + value + '</option>');
                                    stareSelected = true;
                                } else {
                                    $(stateClass).append('<option  value="' + key + '">' + value + '</option>');
                                }
                            });
                            if (stareSelected) {
                                selectcountry(formId, 'state');
                                console.log('pused state')
                            } else {
                                $(cityClass).empty();
                                $(cityClass).append('<option value="">--Select City--</option>');
                            }
                        }
                    });
                } else {
                    $(stateClass).empty();
                    $(stateClass).append('<option value="">--Select State--</option>');
                    $(cityClass).empty();
                    $(cityClass).append('<option value="">--Select City--</option>');
                }

            } else if (inputtype == 'state') {

                if (countryValue == 1) {
                    $('#' + formId).find('.citybox').show();
                    // console.log("india "+countryValue);
                    // $('#' + formId).find('.ua_city')
                } else {
                    // console.log("others "+countryValue);
                    $('#' + formId).find('.citybox').hide();

                    $(cityClass).empty();
                    $(cityClass).append('<option value="">--Select City--</option>');
                }


                var stateValue = $('#' + formId).find('.ua_state').val();

                // var stateValue = $(this).val();
                if (stateValue) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                        url: '{{ route('showCities') }}',
                        type: "POST",
                        data: {'catID': stateValue},
                        dataType: "json",
                        success: function (data) {

                            var Types = data.cities;
                            $(cityClass).empty();
                            $(cityClass).append('<option value="">--Select City--</option>');
                            $.each(Types, function (key, value) {

                                var cityValue = $(cityClass).data('city');
                                var selected = (cityValue == key) ? "selected" : "";
                                $(cityClass).append('<option ' + selected + ' value="' + key + '">' + value + '</option>');
                            });


                        }
                    });

                } else {
                    $(cityClass).empty();
                    $(cityClass).append('<option value="">--Select City--</option>');
                }

            }
        }


        $(function () {
            $('.ua_country').on('change', function () {
                var addresFormID = $(this).parents('form').attr('id');
                selectcountry(addresFormID, 'country')
            });


            var addresFormID = $('#editServices').parents('form').attr('id');
            selectcountry(addresFormID, 'country')


        })
    </script>

@endsection
