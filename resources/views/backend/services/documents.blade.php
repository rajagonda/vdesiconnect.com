@extends('backend.layout')
@section('title', $title)

@section('headerStyles')



@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              'admin_service'=> 'Services',
              ),'User Address'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">


                @if (Session::has('flash_message'))
                    <br/>
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ Session::get('flash_message' ) }}</strong>
                    </div>
                @endif



                <div class="col-lg-12">
                    @include('backend.services.includs.nav')
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="card">
                                <div class="card-header">
                                    Documents List
                                    <span style="float: right">
                                        <a href="{{route('admin_service_documents_send',['id'=>$item->s_id])}}"
                                           class="dropdown-item">
                                                            <i class="fa fa-location-arrow"></i> Send Email
                                                        </a>
                                    </span>
                                </div>
                                <div class="card-body">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Document Name</th>
                                            <th scope="col">Download</th>
                                            <th scope="col">Uploaded at</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($servicedoc)>0)
                                            @foreach($servicedoc as $servicedocs)

                                                <?php
                                                //        dump($item);
                                                ?>
                                                <tr>
                                                    <th scope="row">{{ $loop->iteration }}</th>
                                                    <td>{{$servicedocs->sd_title}}</td>
                                                    <td><a href="{{url('/uploads/service_documents/'.$servicedocs->sd_doc_name)}}">Download</a> </td>

                                                    <td>{{ \Carbon\Carbon::parse($servicedocs->created_at)->format('d/m/Y H:i:s')}}</td>


                                                    <td>



                                                        <form method="POST" id=""
                                                              action="{{ route('ServiceDocumentDelete') }}"
                                                              accept-charset="UTF-8" class="form-horizontal"
                                                              style="display:inline">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="sd_id"
                                                                   value="{{ $servicedocs->sd_id }}"/>
                                                            <button type="submit" class="dropdown-item"
                                                                    title="Delete Service Document"
                                                                    onclick="return confirm('Confirm delete')">
                                                                <i class="fa fa-trash"></i> Delete
                                                            </button>

                                                        </form>


                                                    </td>



                                                </tr>



                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="11">No records found</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="card">
                                <div class="card-header">
                                    Upload Documents
                                </div>
                                <div class="card-body">
                                    <form method="POST" id="banners" action="{{ route('admin_service_documents',['service'=>$serviceType]) }}"
                                          accept-charset="UTF-8" class="form-horizontal"
                                          enctype="multipart/form-data" autocomplete="off">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="s_id" value="{{ $item->s_id }}">






                                        <div class=" form-group ">
                                            <label>Document Title</label>
                                            <div class="input-group">

                                                <input required class="form-control" type="text" name="sd_title"/>
                                            </div>
                                            @if ($errors->has('sd_title'))
                                                <span class="text-danger">{{ $errors->first('sd_title') }}</span>
                                            @endif
                                        </div>

                                        <div class=" form-group ">
                                            <label>Document Upload</label>
                                            <div class="input-group">
                                                <input required class="" type="file" name="sd_doc_name"/>
                                            </div>
                                            @if ($errors->has('sd_doc_name'))
                                                <span class="text-danger">{{ $errors->first('sd_doc_name') }}</span>
                                            @endif
                                        </div>


                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary btn-sm">
                                                <i class="fa fa-dot-circle-o"></i> Upload
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               

            </div>

        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

@endsection