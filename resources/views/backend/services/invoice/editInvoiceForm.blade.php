<form method="post" id="ServiceInvoice"
      action="{{ route('adminServiceCreateInvoice') }}"
      class="form-horizontal ServiceInvoice"
      enctype="multipart/form-data">
    {{ csrf_field() }}

    <input type="hidden" name="s_id"
           value="{{$item->s_id}}"/>
    <input type="hidden" name="s_type"
           value="{{$item->s_type}}"/>

    <div class="row">

        <div class="col-12">

            <div class=" form-group">
                <label for="hf-email"
                       class=" form-control-label">Duration (If Applicable)</label>

                <input class="form-control"
                       id="s_duration"
                       type="text"
                       value="{{$item->s_duration}}"
                       placeholder="From To Date"
                       name="s_duration">
                @if ($errors->has('s_duration'))
                    <span class="text-danger help-block">{{ $errors->first('s_duration') }}</span>
                @endif
            </div>
        </div>

        <div class="col-12">

            <div class=" form-group">
                <label for="hf-email"
                       class=" form-control-label">
                    No of Days / No of Kgs</label>
                <input class="form-control"
                       id="s_no_of_days"
                       type="number"
                       value="{{$item->s_no_of_days}}"
                       placeholder="No of Days / No of Kgs"
                       name="s_no_of_days">
                @if ($errors->has('s_no_of_days'))
                    <span class="text-danger help-block">{{ $errors->first('s_no_of_days') }}</span>
                @endif
            </div>
        </div>

        <div class="col-12">

            <div class="form-group">
                <label for="hf-email"
                       class=" form-control-label">Unit Price (in {{getCurency()}})</label>
                <input class="form-control"
                       id="s_unit_price"
                       type="number"
                       value="{{$item->s_unit_price}}"
                       placeholder="Price"
                       name="s_unit_price">
                @if ($errors->has('s_unit_price'))
                    <span class="text-danger help-block">{{ $errors->first('s_unit_price') }}</span>
                @endif
            </div>
        </div>

        <div class="col-12 ">

            <div class=" form-group">
                <label for="hf-email"
                       class=" form-control-label">
                    Unit
                    Type</label>

                <select class="form-control" id="s_unit_type" name="s_unit_type">
                    <option value="Day" {{$item->s_unit_type=='Day'?'selected':''}} > Day</option>
                    <option value="Month" {{$item->s_unit_type=='Month'?'selected':''}}> Month</option>
                    <option value="Year" {{$item->s_unit_type=='Year'?'selected':''}}> Year</option>
                </select>

                @if ($errors->has('s_unit_type'))
                    <span class="text-danger help-block">{{ $errors->first('s_unit_type') }}</span>
                @endif


                {{--                <input class="form-control"--}}
                {{--                       id="s_unit_type"--}}
                {{--                       type="text"--}}
                {{--                       value="{{$item->s_unit_type}}"--}}
                {{--                       placeholder="Day/Month/Year"--}}
                {{--                       name="s_unit_type">--}}

            </div>
        </div>

        <div class="col-12 ">

            <div class=" form-group">
                <label for="hf-email"
                       class=" form-control-label">Tax (%)</label>
                <input class="form-control"
                       id="s_tax" type="number"
                       value="{{$item->s_tax}}"
                       placeholder="Tax% (5 or 10 or 15)"
                       name="s_tax">
                @if ($errors->has('s_tax'))
                    <span class="text-danger help-block">{{ $errors->first('s_tax') }}</span>
                @endif
            </div>
        </div>

        <div class="col-12 ">

            <div class=" form-group">
                <label for="hf-email"
                       class=" form-control-label">Due date </label>
                <input class="form-control datepickerFrontModel"
                       id="s_duedate_{{$item->s_id}}" type="text"
                       value="{{$item->s_duedate}}"
                       placeholder="Select Due date"
                       name="s_duedate">
                @if ($errors->has('s_duedate'))
                    <span class="text-danger help-block">{{ $errors->first('s_duedate') }}</span>
                @endif
                <small>Ex: YYYY-mm-dd</small>

            </div>
        </div>

    </div>

    <input type="submit" class="btn btn-primary"
           value="Create Invoice"/>
</form>
