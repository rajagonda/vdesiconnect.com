@extends('backend.layout')
@section('title', $title)

@section('headerStyles')



@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              'admin_service'=> 'Services',
              ),'User Address'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">


                @if (Session::has('flash_message'))
                    <br/>
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>{{ Session::get('flash_message' ) }}</strong>
                    </div>
                @endif


                <div class="col-lg-12">
                    @include('backend.services.includs.nav')
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="card">
                                <div class="card-header">
                                    Service Information
                                </div>
                                <div class="card-body">

                                    <table class="table table-striped" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Service Type</th>
                                            <th>Due date</th>
                                            <th>Unit Type</th>
{{--                                            <th>Email</th>--}}
{{--                                            <th>Phone</th>--}}
{{--                                            <th>Message</th>--}}
                                            <th>description</th>
                                            <th>Requested On</th>
                                            <th>Payment Status</th>
                                            <th>Invoice</th>
{{--                                            <th>Action</th>--}}
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php
//                                        dump($lists);
                                        ?>

                                        @if(count($lists)> 0)

                                            @foreach($lists AS $list)

                                                <tr>
                                                    <td>#</td>
                                                    <td>{{ $list->getService->s_type }}</td>
                                                    <td>{{ $list->s_duedate }}</td>
                                                    <td>{{ $list->s_unit_type }}</td>
{{--                                                    <td>{{ $list->getService->s_email }}</td>--}}
{{--                                                    <td>{{ $list->getService->s_phone }}</td>--}}
                                                    <td>{{ $list->s_description }}</td>

                                                    <td>{{ date('d-m-Y', strtotime($list->created_at)) }}</td>
                                                    <td>{!! serviceInvoiceStatus($list->s_status) !!}</td>
                                                    <td>
{{--                                                        @if($list->s_status!='')--}}
{{--                                                            ------}}
{{--                                                        @else--}}
                                                            <a href="{{route('ServiceInvoice',['id'=>$list->id])}}"
                                                               class="dropdown-item">
                                                                <i class="fa fa-file"></i> Invoice
                                                            </a>
{{--                                                        @endif--}}
                                                    </td>
{{--                                                    <td>Action</td>--}}
                                                </tr>

                                            @endforeach
                                        @endif


                                        </tbody>
                                    </table>


                                    {{--                                    @include('backend.services.includs.updateRequestForm')--}}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card">
                                <div class="card-header">
                                    Create Invoice
                                </div>
                                <div class="card-body">
                                    @include('backend.services.invoice.createInvoiceForm')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

    <script>
        $(function () {
            $('.ServiceInvoice').each(function () {
                $(this).validate({
                    ignore: [],
                    errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                    errorElement: 'div',
                    errorPlacement: function (error, e) {
                        e.parents('.form-group').append(error);
                    },
                    highlight: function (e) {
                        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                        $(e).closest('.text-danger').remove();
                    },
                    success: function (e) {
                        // You can use the following if you would like to highlight with green color the input after successful validation!
                        e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        e.closest('.text-danger').remove();
                    },
                    rules: {
                        s_duration: {
                            required: true
                        },
                        s_no_of_days: {
                            required: true,
                            number: true
                        },
                        s_unit_price: {
                            required: true,
                            number: true
                        },
                        s_unit_type: {
                            required: true
                        },
                        s_tax: {
                            required: true
                        },
                        s_duedate: {
                            required: true
                        }
                    },
                    messages: {
                        s_duration: {
                            required: 'Please enter duration'
                        },
                        s_no_of_days: {
                            required: 'Please enter No of days',
                            number: 'position must be a number'
                        },
                        s_unit_price: {
                            required: 'Please enter Unit Price',
                            number: 'position must be a number'
                        },
                        s_unit_type: {
                            required: 'Unit Type is required'
                        },
                        s_tax: {
                            required: 'Tax is required'
                        },
                        s_duedate: {
                            required: 'Due date is required'
                        }
                    },
                });
            });

        })
    </script>

@endsection
