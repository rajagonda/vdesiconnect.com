<form method="post" id="ServiceInvoice"
      action="{{ route('adminServiceCreateInvoice') }}"
      class="form-horizontal ServiceInvoice"
      enctype="multipart/form-data">
    {{ csrf_field() }}


<!--    --><?php
//    dd($item);
//    ?>

    <input type="hidden" name="s_id"
           value="{{$item->s_id}}"/>
    <input type="hidden" name="s_type"
           value="{{$item->s_type}}"/>
    <input type="hidden" name="s_user_id"
           value="{{ isset($item->getService)? $item->getService->s_user_id : ''}}"/>

    <div class="row">

        <div class="col-12">

            <div class=" form-group">
                <label for="hf-email"
                       class=" form-control-label">Duration (If Applicable)</label>

                <input class="form-control"
                       id="s_duration"
                       type="text"

                       placeholder="From To Date"
                       name="s_duration">
                {{ formValidationError($errors, 's_duration') }}
            </div>
        </div>

        <div class="col-12">

            <div class=" form-group">
                <label for="hf-email"
                       class=" form-control-label">
                    No of Days / No of Kgs</label>
                <input class="form-control"
                       id="s_no_of_days"
                       type="number"

                       placeholder="No of Days / No of Kgs"
                       name="s_no_of_days">
                {{ formValidationError($errors, 's_no_of_days') }}
            </div>
        </div>

        <div class="col-12">

            <div class="form-group">
                <label for="hf-email"
                       class=" form-control-label">Unit Price (in {{getCurency()}})</label>
                <input class="form-control"
                       id="s_unit_price"
                       type="number"

                       placeholder="Price"
                       name="s_unit_price">
                {{ formValidationError($errors, 's_unit_price') }}
            </div>
        </div>

        <div class="col-12 ">

            <div class=" form-group">
                <label for="hf-email"
                       class=" form-control-label">
                    Unit
                    Type</label>

                <select class="form-control" id="s_unit_type" name="s_unit_type">
                    <option value="Day"> Day</option>
                    <option value="Month"> Month</option>
                    <option value="Year"> Year</option>
                </select>

                {{ formValidationError($errors, 's_unit_type') }}


                {{--                <input class="form-control"--}}
                {{--                       id="s_unit_type"--}}
                {{--                       type="text"--}}
                {{--                       value="{{$item->s_unit_type}}"--}}
                {{--                       placeholder="Day/Month/Year"--}}
                {{--                       name="s_unit_type">--}}

            </div>
        </div>

        <div class="col-12 ">

            <div class=" form-group">
                <label for="hf-email"
                       class=" form-control-label">Tax (%)</label>
                <input class="form-control"
                       id="s_tax" type="number"

                       placeholder="Tax% (5 or 10 or 15)"
                       name="s_tax">
                {{ formValidationError($errors, 's_tax') }}
            </div>
        </div>

        <div class="col-12 ">

            <div class=" form-group">
                <label for="hf-email"
                       class=" form-control-label">Due date </label>
                <input class="form-control datepicker"
                       id="s_duedate_{{$item->id}}" type="text"
                       autocomplete="off"
                       placeholder="Select Due date"
                       name="s_duedate">

                {{ formValidationError($errors, 's_duedate') }}

                <small>Ex: YYYY-mm-dd</small>

            </div>
        </div>
        <div class="col-12 ">

            <div class=" form-group">
                <label for="hf-email"
                       class=" form-control-label"> Description </label>

                <textarea name="s_description" class="form-control"></textarea>

                {{ formValidationError($errors, 's_description') }}

                <small>Ex: YYYY-mm-dd</small>

            </div>
        </div>

    </div>

    <input type="submit" class="btn btn-primary"
           value="Create Invoice"/>
</form>
