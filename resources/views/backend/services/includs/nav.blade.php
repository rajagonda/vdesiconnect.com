<div class="btn-group" role="group" aria-label="Basic example">
    <a href="{{route('admin_service_view',['id'=>$item->s_id,'service'=>$serviceType])}}"
       class="btn btn-secondary {{ $sub_active_menu=='view-service' ? 'active' : '' }}">
        View Service
    </a>
    <a href="{{route('admin_service_documents',['id'=>$item->s_id,'service'=>$serviceType])}}"
       class="btn btn-secondary {{ $sub_active_menu=='service-documents' ? 'active' : '' }}">
        Documents
    </a>
    <a href="{{route('admin_service_invoice',['id'=>$item->s_id,'service'=>$serviceType])}}"
       class="btn btn-secondary {{ $sub_active_menu=='invoice' ? 'active' : '' }}">
        Invoices
    </a>
</div>



