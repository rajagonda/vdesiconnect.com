<form method="post" id="editServices"
      action="{{route('admin_service', ['service'=>$serviceType])}}"
      class="form-horizontal editServices"
      autocomplete="off"
      enctype="multipart/form-data">
    {{ csrf_field() }}


    <input type="hidden" name="s_id"
           value="{{$item->s_id}}"/>
    <input type="hidden" name="s_type"
           value="{{$item->s_type}}"/>


    <div class="row">

        <div class="col-lg-6">

            <div class="form-group">
                <label for="hf-email"
                       class=" form-control-label">Contact
                    Person</label>
                <input class="form-control"
                       id="s_contact_person"
                       type="text"
                       placeholder="Contact Person"
                       value="{{$item->s_contact_person}}"
                       name="s_contact_person">
                @if ($errors->has('s_contact_person'))
                    <span class="text-danger help-block">{{ $errors->first('s_contact_person') }}</span>
                @endif
            </div>
        </div>

        <div class="col-lg-6">

            <div class=" form-group">
                <label for="hf-email"
                       class=" form-control-label">Address
                    Line 1</label>
                <input class="form-control"
                       id="s_address_line_1"
                       type="text"
                       value="{{$item->s_address_line_1}}"
                       placeholder="Address Line 1"
                       name="s_address_line_1">
                @if ($errors->has('s_address_line_1'))
                    <span class="text-danger help-block">{{ $errors->first('s_address_line_1') }}</span>
                @endif
            </div>
        </div>

        <div class="col-lg-6">

            <div class="  form-group">
                <label for="hf-email"
                       class=" form-control-label">Address
                    Line 2</label>
                <input class="form-control"
                       id="s_address_line_2"
                       type="text"
                       value="{{$item->s_address_line_2}}"
                       placeholder="If Product has Weight"
                       name="s_address_line_2">
                @if ($errors->has('s_address_line_2'))
                    <span class="text-danger help-block">{{ $errors->first('s_address_line_2') }}</span>
                @endif
            </div>
        </div>


        <div class="col-lg-6">

            <div class=" form-group">
                <label for="hf-email"
                       class=" form-control-label">City</label>
                <input class="form-control"
                       id="s_city" type="text"
                       value="{{$item->s_city}}"
                       placeholder="City"
                       name="s_city">
                @if ($errors->has('s_city'))
                    <span class="text-danger help-block">{{ $errors->first('s_city') }}</span>
                @endif
            </div>
        </div>



        <div class="col-lg-6">

            <div class="  form-group">
                <label for="hf-email"
                       class=" form-control-label">Country</label>


                <select name="s_country" class="form-control ua_country">
                    <option value="">--Select Country--</option>
                    <?php
                    $countries = getCountries(); ?>
                    @foreach($countries as $ks=>$s)
                        <option {{$item->s_country ==$ks ? 'selected':'' }} value="{{ $ks }}">
                            {{ $s }}
                        </option>
                    @endforeach
                </select>
{{--                <input class="form-control"--}}
{{--                       id="s_country"--}}
{{--                       type="text"--}}
{{--                       value="{{$item->s_country}}"--}}
{{--                       placeholder="Country"--}}
{{--                       name="s_country">--}}
                @if ($errors->has('s_country'))
                    <span class="text-danger help-block">{{ $errors->first('s_country') }}</span>
                @endif
            </div>
        </div>

        <div class="col-lg-6">

            <div class="  form-group">
                <label for="hf-email"
                       class=" form-control-label">State</label>

                <select class="form-control ua_state" required name="s_state">
                    <option value="">--Select State--</option>
                </select>

{{--                <input class="form-control"--}}
{{--                       id="s_state" type="text"--}}
{{--                       value="{{$item->s_state}}"--}}
{{--                       placeholder="State"--}}
{{--                       name="s_state">--}}
                @if ($errors->has('s_state'))
                    <span class="text-danger help-block">{{ $errors->first('s_state') }}</span>
                @endif
            </div>
        </div>
        <div class="col-lg-6">

            <div class=" form-group">

                <label for="hf-email"
                       class=" form-control-label">Service
                    For</label>
                <input class="form-control"
                       id="s_options"
                       type="text"
                       value="{{$item->s_options}}"
                       placeholder="Service For"
                       name="s_options">
                @if ($errors->has('s_options'))
                    <span class="text-danger help-block">{{ $errors->first('s_options') }}</span>
                @endif
            </div>
        </div>

        <div class="col-lg-6">

            <div class=" form-group">
                <label for="hf-email"
                       class=" form-control-label">Date</label>
                <input class="form-control"
                       id="s_date" type="text"
                       value="{{$item->s_date}}"
                       placeholder="Date"
                       name="s_date">
                @if ($errors->has('s_date'))
                    <span class="text-danger help-block">{{ $errors->first('s_date') }}</span>
                @endif
            </div>
        </div>

        <div class="col-lg-6">

            <div class=" form-group">
                <label for="hf-email"
                       class=" form-control-label">Time</label>
                <input class="form-control"
                       id="s_time" type="text"
                       value="{{$item->s_time}}"
                       placeholder="Time"
                       name="s_time">
                @if ($errors->has('s_time'))
                    <span class="text-danger help-block">{{ $errors->first('s_time') }}</span>
                @endif
            </div>
        </div>

        <div class="col-lg-6">

            <div class=" form-group">
                <label for="hf-email"
                       class=" form-control-label">Email</label>
                <input class="form-control"
                       id="s_email" type="text"
                       value="{{$item->s_email}}"
                       placeholder="Email"
                       name="s_email">
                @if ($errors->has('s_email'))
                    <span class="text-danger help-block">{{ $errors->first('s_email') }}</span>
                @endif
            </div>
        </div>


        <div class="col-lg-6">

            <?php
            list($mobilePrefix, $mobile) = getPhoneNumber($item->s_phone);
            ?>

            <div class=" form-group">
                <label for="hf-email"
                       class=" form-control-label">Phone</label>

                <div class="input-group">

                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            <select name="se_mobile_prefix" required
                                    class="">
                                <option value="">Select</option>
                                @if(sizeof(getCountrycodes())>0)
                                    @foreach(getCountrycodes() as $key=>$value)
                                        <option {{ ($mobilePrefix==$key)?'selected':'' }}  value="{{ $key }}">+{{ $value }}</option>
                                    @endforeach
                                @endif
                            </select>

                        </span>
                    </div>

                    <input class="form-control"
                           id="s_phone" type="text"
                           value="{{$mobile}}"
                           placeholder="Phone"
                           name="s_phone">

                </div>
                @if ($errors->has('s_phone'))
                    <span class="text-danger help-block">{{ $errors->first('s_phone') }}</span>
                @endif
            </div>
        </div>

        <div class="col-lg-12">

            <div class=" form-group">
                <label for="hf-email"
                       class=" form-control-label">Message</label>
                <textarea class="form-control"
                          rows="5"
                          id="s_msg" type="text"
                          name="s_msg">{{$item->s_msg}}</textarea>
                @if ($errors->has('s_msg'))
                    <span class="text-danger help-block">{{ $errors->first('s_msg') }}</span>
                @endif
            </div>
        </div>
        <input type="submit" class="btn btn-primary" value="Update Service"/>
    </div>


</form>
