@extends('backend.layout')
@section('title', $title)

@section('headerStyles')



@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Orders'
              ),'Orders'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Web Developement Assistance</strong>

                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Contact Person</th>
                                    <th scope="col">Medical Type</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Time</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">Message</th>
                                    <th scope="col">Requested On</th>
                                    <th scope="col">Action</th>
                                    <th scope="col">Invoice</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($service)>0)
                                    @foreach($service as $item)
                                        <tr>
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>{{ $item->s_contact_person }}</td>
                                            {{--<td>--}}
                                            {{--<p>{{ $item['s_address_line_1'] }}</p>--}}
                                            {{--<p>{{ $item['s_address_line_2'] }}</p>--}}
                                            {{--<p>City : {{ $item['s_city'] }}</p>--}}
                                            {{--<p>State : {{ $item['s_state'] }}</p>--}}
                                            {{--<p>Country : {{ $item['s_country'] }}</p>--}}
                                            {{--</td>--}}
                                            <td>{{ $item->s_options }}</td>
                                            <td>{{ $item->s_date }}</td>
                                            <td>{{ $item->s_time }}</td>
                                            <td>{{ $item->s_email }}</td>
                                            <td>{{ $item->s_phone }}</td>
                                            <td>{{ $item->s_msg }}</td>
                                            <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i:s')}}</td>
                                            <td>
                                                <a href="#" data-toggle="modal"
                                                   data-target="#otherDetails{{ $item->s_id }}"
                                                   class="dropdown-item">
                                                    <i class="fa fa-file"></i> View
                                                </a>

                                                <!-- Modal -->
                                                <div id="otherDetails{{ $item->s_id }}" class="modal fade"
                                                     role="dialog">
                                                    <div class="modal-dialog modal-lg">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Web Developement Assistance
                                                                    from {{ $item->s_contact_person }}</h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">
                                                                    &times;
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">


                                                                <div class="row">
                                                                    <div class="col-lg-8">
                                                                        <div class="card">
                                                                            <div class="card-header">
                                                                                Service Information
                                                                            </div>
                                                                            <div class="card-body">
                                                                                @include('backend.services.includs.updateRequestForm')
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-4">
                                                                        <div class="card">
                                                                            <div class="card-header">
                                                                                Create Invoice
                                                                            </div>
                                                                            <div class="card-body">
                                                                             @include('backend.services.invoice.createInvoiceForm')
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary"
                                                                            data-dismiss="modal">
                                                                        Close
                                                                    </button>
                                                                    {{--<button type="submit" class="btn btn-primary">Submit</button>--}}
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>


                                                <form method="POST" id="products"
                                                      action="{{ route('ServiceDelete') }}"
                                                      accept-charset="UTF-8" class="form-horizontal"
                                                      style="display:inline">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="s_id"
                                                           value="{{ $item->s_id }}"/>
                                                    <input type="hidden" name="s_type"
                                                           value="{{ $item->s_type }}"/>
                                                    <button type="submit" class="dropdown-item"
                                                            title="Delete Service"
                                                            onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                        <i class="fa fa-trash"></i> Delete
                                                    </button>

                                                </form>


                                            </td>
                                            <td>

                                                @if($item->s_status=='')
                                                    ----
                                                @else
                                                    <a href="{{route('showInvoice',['id'=>$item->s_id])}}"
                                                       class="dropdown-item">
                                                        <i class="fa fa-file"></i> Invoice
                                                    </a>
                                                @endif
                                            </td>


                                        </tr>



                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="11">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row px-3">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $service->firstItem() }}
                        to {{ $service->lastItem() }} of {{ $service->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $service->appends(['filters' => Request::get('filters'),'search' => Request::get('search')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

    @include('backend.services.includs.script')

@endsection
