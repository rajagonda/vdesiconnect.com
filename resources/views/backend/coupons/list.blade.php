@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #3c8dbc;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Coupons'
              ),'Coupons'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Coupons</strong>
                            <a href="{{route('addNewCoupons')}}" class="btn btn-primary btn-xs" style="float:right;">Add
                                Coupons
                            </a>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Code</th>
                                    <th scope="col">Discount</th>
                                    <th scope="col">Discount Type</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Country</th>
                                    <th scope="col">Coupon Usage Type</th>
                                    <th scope="col">Coupon Expiry date</th>
                                    <th scope="col">
                                        Ussage info
                                    </th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($coupons)>0)
                                    @foreach($coupons as $item)
                                        <tr>
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>{{ $item->coupon_code }}</td>
                                            <td>{{ $item->coupon_discount_value }}</td>
                                            <td>{{ couponDiscountTypes($item->coupon_discount_type) }}</td>
                                            <td>{{ $item->coupon_quantity }}</td>
                                            <td>{{ $item->getCountry->country_name }}</td>
                                            <td>{{ couponUsageTypes($item->coupon_usage_type) }}</td>
                                            <td>{{ \Carbon\Carbon::parse($item->coupon_expiry_date)->format('d-m-Y') }}</td>
                                            <td>
                                                <a href="#" data-toggle="modal"
                                                   data-target="#manageCoupons{{ $item->coupon_id }}"
                                                   class="dropdown-item">
                                                    <i class="fa fa-eye"></i> Manage Coupons
                                                </a>

                                                <div id="manageCoupons{{ $item->coupon_id }}"
                                                     class="modal fade productDetails"
                                                     role="dialog">
                                                    <div class="modal-dialog modal-xl">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">

                                                                    {{ $item->coupon_code     }}

                                                                </h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">
                                                                    &times;
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                $item->coupon_code

                                                                <div class="card-body table-responsive">
                                                                    <table class="tablesaw table-striped table-bordered table-hover table"
                                                                           data-tablesaw-mode="stack" id="">
                                                                        @foreach($item->getCoupon as $couponusers)
                                                                            <tr>
                                                                                <td>{{$couponusers->cu_id}}</td>
                                                                                <td>{{dump($couponusers->getUser)}}</td>
                                                                                <td>{{$couponusers->cu_coupan_code}}</td>
                                                                                <td>{{$couponusers->created_at}}</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <?php
//                                                dump($item->getCoupon);
                                                ?>
                                            </td>
                                            <td>{{ allStatuses('general',$item->coupon_status) }}</td>
                                            <td>
                                                <div class="dropdown">
                                                    <a class="btn btn-outline-primary dropdown-toggle" href="#"
                                                       role="button"
                                                       data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-h"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item"
                                                           href="{{ route('addNewCoupons',['id'=>$item->coupon_id]) }}"><i
                                                                    class="fa fa-pencil"></i> Edit</a>
                                                        <form method="POST" id="coupons"
                                                              action="{{ route('coupons') }}"
                                                              accept-charset="UTF-8" class="form-horizontal"
                                                              style="display:inline">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="coupon_id"
                                                                   value="{{ $item->coupon_id }}"/>
                                                            <button type="submit" class="dropdown-item"
                                                                    title="Delete Coupon"
                                                                    onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                                <i
                                                                        class="fa fa-trash"></i> Delete
                                                            </button>

                                                        </form>

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="10">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $coupons->firstItem() }}
                        to {{ $coupons->lastItem() }} of {{ $coupons->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $coupons->appends(['filters' => Request::get('filters'),'search' => Request::get('search')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

@endsection