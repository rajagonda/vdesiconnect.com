<script>

    $(function () {

        $('.datepicker_hidepastdates').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy',
            yearRange: "-100:+0",
            minDate: 4
        });

        $('#couponss').validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            errorPlacement: function (error, e) {
                e.parents('.form-group').append(error);
            },
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.text-danger').remove();
            },
            success: function (e) {
                // You can use the following if you would like to highlight with green color the input after successful validation!
                e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                e.closest('.text-danger').remove();
            },
            rules: {
                coupon_code: {
                    required: true,
                    remote: function (element) {
                        return {
                            url: '{{route('checkCouponcode')}}',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type: 'POST',
                            data: {
                                current_coupon_code: function (form, event) {
                                    var formID = $(element).closest('form').attr('id');
                                    var currentAlias = $('#' + formID).find('.edit_current_coupon_code');
                                    return $(currentAlias).val();
                                },
                                type: function (form, event) {
                                    return 'edit';
                                }
                            }
                        }
                    }
                },
                coupon_discount_value: {
                    required: true
                },
                coupon_discount_type: {
                    required: true
                },
                coupon_quantity: {
                    required: true
                },
                coupon_country: {
                    required: true
                },
                coupon_usage_type: {
                    required: true
                },
                coupon_expiry_date: {
                    required: true
                },
                coupon_status: {
                    required: true
                }
            },
            messages: {
                coupon_code: {
                    required: 'Please enter coupon code',
                    remote: $.validator.format("This Coupon Code is already exist")
                },
                coupon_discount_value: {
                    required: 'Please enter discount'
                },
                coupon_discount_type: {
                    required: 'Please select discount type'
                },
                coupon_quantity: {
                    required: 'Please enter coupon quantity'
                },
                coupon_country: {
                    required: 'Please select coupon country'
                },
                coupon_usage_type: {
                    required: 'Please select usage type'
                },
                coupon_expiry_date: {
                    required: 'Please select expiry date'
                },
                coupon_status: {
                    required: 'Please select status'
                }
            },
        });
    });

</script>