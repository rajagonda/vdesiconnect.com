@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')
    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'coupons'=>'Coupons',
               ''=>'Add New'
               ),'Add New Coupon'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">

                        <div class="card-body card-block">

                            <form method="POST" id="coupons" action="{{ route('addNewCoupons') }}"
                                  accept-charset="UTF-8" class="form-horizontal"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="coupon_id" value="{{ $coupon_id }}">


                                <input type="hidden" class="edit_current_coupon_code" value="{{ !empty(old('coupon_code')) ? old('coupon_code') : ((($coupon) && ($coupon->coupon_code)) ? $coupon->coupon_code : '') }}" />
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Code</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="coupon_code" type="text"
                                               placeholder="Code"
                                               name="coupon_code"
                                               value="{{ !empty(old('coupon_code')) ? old('coupon_code') : ((($coupon) && ($coupon->coupon_code)) ? $coupon->coupon_code : '') }}">
                                        @if ($errors->has('coupon_code'))
                                            <span class="text-danger help-block">{{ $errors->first('coupon_code') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Expiry date</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control datepicker_hidepastdates" id="coupon_expiry_date"
                                               type="text"
                                               placeholder="Coupon expiry date"
                                               name="coupon_expiry_date"
                                               value="{{ !empty(old('coupon_expiry_date')) ? old('coupon_expiry_date') : ((($coupon) && ($coupon->coupon_expiry_date)) ? $coupon->coupon_expiry_date : '') }}">
                                        @if ($errors->has('coupon_expiry_date'))
                                            <span class="text-danger help-block">{{ $errors->first('coupon_expiry_date') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Discount value</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="coupon_discount_value" type="text"
                                               placeholder="Coupon discount value"
                                               name="coupon_discount_value"
                                               value="{{ !empty(old('coupon_discount_value')) ? old('coupon_discount_value') : ((($coupon) && ($coupon->coupon_discount_value)) ? $coupon->coupon_discount_value : '') }}">
                                        @if ($errors->has('coupon_discount_value'))
                                            <span class="text-danger help-block">{{ $errors->first('coupon_discount_value') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Discount Type</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="coupon_discount_type" id="coupon_discount_type"
                                                class="form-control">
                                            <?php
                                            $types = couponDiscountTypes(); ?>
                                            @foreach($types as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('coupon_discount_type')) && old('coupon_discount_type')==$ks)  ? 'selected' : ((($coupon) && ($coupon->coupon_discount_type == $ks)) ? 'selected' : '') }}
                                                >
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('coupon_discount_type'))
                                            <span class="text-danger help-block">{{ $errors->first('coupon_discount_type') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Quantity</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                        <input class="form-control" id="coupon_quantity" type="text"
                                               placeholder="quantity"
                                               name="coupon_quantity"
                                               value="{{ !empty(old('coupon_quantity')) ? old('coupon_quantity') : ((($coupon) && ($coupon->coupon_quantity)) ? $coupon->coupon_quantity : '') }}">
                                        @if ($errors->has('coupon_quantity'))
                                            <span class="text-danger help-block">{{ $errors->first('coupon_quantity') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Coupon country</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="coupon_country" id="coupon_country" class="form-control">
                                            <option value="">--Select Country--</option>
                                            <?php
                                            $countries = getCountries(); ?>
                                            @foreach($countries as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('coupon_country')) && old('coupon_country')==$ks)  ? 'selected' : ((($coupon) && ($coupon->coupon_country == $ks)) ? 'selected' : '') }}>
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('coupon_country'))
                                            <span class="text-danger help-block">{{ $errors->first('coupon_country') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Coupon usage Type</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="coupon_usage_type" id="coupon_usage_type"
                                                class="form-control">
                                            <?php
                                            $types = couponUsageTypes(); ?>
                                            @foreach($types as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('coupon_usage_type')) && old('coupon_usage_type')==$ks)  ? 'selected' : ((($coupon) && ($coupon->coupon_usage_type == $ks)) ? 'selected' : '') }}
                                                >
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('coupon_usage_type'))
                                            <span class="text-danger help-block">{{ $errors->first('coupon_usage_type') }}</span>
                                        @endif
                                    </div>
                                </div>


                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="hf-email" class=" form-control-label">Status</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="coupon_status" id="coupon_status" class="form-control">
                                            <?php
                                            $status = allStatuses('general'); ?>
                                            @foreach($status as $ks=>$s)
                                                <option value="{{ $ks }}" {{ (!empty(old('coupon_status')) && old('coupon_status')==$ks)  ? 'selected' : ((($coupon) && ($coupon->coupon_status == $ks)) ? 'selected' : '') }}
                                                >
                                                    {{ $s }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('coupon_status'))
                                            <span class="text-danger help-block">{{ $errors->first('coupon_status') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    @include('backend.coupons.script')

@endsection