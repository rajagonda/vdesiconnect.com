@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #3c8dbc;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'States'
              ),'States'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                       <div class="row pb-3 mx-0">

                           <div clas="col-lg-4 mx-3">
                               @include('backend.countries.nav')
                           </div>
                           <div class="col-lg-6">

                               <form method="GET" action="{{ route('States') }}" accept-charset="UTF-8"
                                     class="navbar-form navbar-right" role="search" autocomplete="off">
                                   <input type="hidden" name="search" value="search">

                                   <div class="row">                               &nbsp;&nbsp;

                                       <div class="col-md-6">
                                           <select name="search_country" id="search_country"
                                                   class="form-control">
                                               <option value="">Select Country</option>
                                               <?php
                                               $categories = getCountries(); ?>
                                               @foreach($categories as $ks=>$s)
                                                   <option value="{{ $ks }}" {{ (app('request')->input('search_country')==$ks)  ? 'selected' : ''}}>
                                                       {{ $s }}
                                                   </option>
                                               @endforeach
                                           </select>
                                       </div>

                                       <div class="col-md-1">
                                             <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                                       </div>
                                   </div>
                               </form>
                           </div>
                       </div>



                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">States</strong>

                            <a href="#" data-toggle="modal" style="float: right"
                               data-target="#addState"
                               class="btn btn-primary">
                               + Add State
                            </a>

                            <div id="addState" class="modal fade"
                                 role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title">
                                                Add State
                                            </h4>
                                            <button type="button" class="close"
                                                    data-dismiss="modal">
                                                &times;
                                            </button>
                                        </div>
                                        <form method="POST"
                                              action="{{ route('addNewState') }}" autocomplete="off">
                                            <div class="modal-body">

                                                <div class="modal-body">
                                                    {{ csrf_field() }}
                                                    <div class="form-group">

                                                        <label>Select Country</label>


                                                            <select class="form-control" name="state_country_id" required>
                                                                <option value="">Select Country</option>
                                                                @if(count($countries) > 0)
                                                                    @foreach($countries AS $country)
                                                                        <option value="{{$country->country_id}}">{{$country->country_name}}</option>
                                                                    @endforeach

                                                                @endif

                                                            </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>State Name</label>
                                                        <input id="state_name" type="text"
                                                               placeholder="State Name"
                                                               class="form-control" required
                                                               name="state_name">
                                                    </div>


                                                    <div class=" form-group">

                                                        <label >Status</label>

                                                            <select name="state_status" id="state_status"
                                                                    class="form-control" required>
                                                                <?php
                                                                $status = allStatuses('general'); ?>
                                                                @foreach($status as $ks=>$s)
                                                                    <option value="{{ $ks }}">
                                                                        {{ $s }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                            @if ($errors->has('state_status'))
                                                                <span class="text-danger help-block">{{ $errors->first('state_status') }}</span>
                                                            @endif

                                                    </div>


                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close
                                                </button>
                                                <button type="submit" name="submit" class="btn btn-primary">Create
                                                </button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>




                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">State</th>
                                    <th scope="col">Country</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($states)>0)
                                    @foreach($states as $item)
                                        <tr>
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>{{ $item->state_name }}</td>
                                            <td>{{ $item->state_country_id }}</td>
                                            <td>{{ allStatuses('general',$item->state_status) }}</td>
                                            <td>
                                                <a href="#" data-toggle="modal"
                                                   data-target="#editState_{{$item->state_id}}"
                                                   class="dropdown-item">
                                                    <i class="fa fa-pencil"></i> Update </a>

                                                <div id="editState_{{$item->state_id}}" class="modal fade"
                                                     role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">
                                                                    Update State
                                                                </h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">
                                                                    &times;
                                                                </button>
                                                            </div>

                                                            <form method="POST"
                                                                  action="{{ route('editStates') }}" autocomplete="off">
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="state_id" value="{{$item->state_id}}">
                                                                <div class="modal-body">

                                                                    <div class="modal-body">

                                                                        <div class="form-group">

                                                                            <label>Select Country</label>


                                                                            <select class="form-control" name="state_country_id" required>
                                                                                <option value="">Select Country</option>
                                                                                @if(count($countries) > 0)
                                                                                    @foreach($countries AS $country)
                                                                                        <option value="{{$country->country_id}}" {{ (!empty(old('state_country_id')) && old('state_country_id')==$country->country_id)  ? 'selected' : ((($item->state_country_id) && ($item->state_country_id == $country->country_id)) ? 'selected' : '') }}>{{$country->country_name}}</option>
                                                                                    @endforeach

                                                                                @endif

                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>State Name</label>
                                                                            <input id="state_name" type="text"
                                                                                   placeholder="State Name"
                                                                                   value="{{$item->state_name}}"
                                                                                   class="form-control" required
                                                                                   name="state_name">
                                                                        </div>


                                                                        <div class=" form-group">

                                                                            <label >Status</label>

                                                                            <select name="state_status" id="state_status"
                                                                                    class="form-control" required>
                                                                                <?php
                                                                                $status = allStatuses('general'); ?>
                                                                                @foreach($status as $ks=>$s)
                                                                                    <option value="{{ $ks }}" {{ (!empty(old('state_status')) && old('state_status')==$ks)  ? 'selected' : ((($item->state_status) && ($item->state_status == $ks)) ? 'selected' : '') }}>
                                                                                        {{ $s }}
                                                                                    </option>
                                                                                @endforeach
                                                                            </select>
                                                                            @if ($errors->has('state_status'))
                                                                                <span class="text-danger help-block">{{ $errors->first('state_status') }}</span>
                                                                            @endif

                                                                        </div>


                                                                    </div>

                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary"
                                                                            data-dismiss="modal">Close
                                                                    </button>
                                                                    <button type="submit" name="submit" class="btn btn-primary">Updae
                                                                    </button>
                                                                </div>
                                                            </form>

                                                        </div>

                                                    </div>
                                                </div>


                                                <form method="POST" id="productbox_form_{{ $item->state_id }}"
                                                      action="{{ route('States') }}"
                                                      accept-charset="UTF-8" class="form-horizontal product_box_form"
                                                      style="display:inline">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="state_id"
                                                           value="{{ $item->state_id }}"/>
                                                    <button type="submit" class="dropdown-item"
                                                            title="Delete State"
                                                            onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                        <i class="fa fa-trash"></i> Delete
                                                    </button>

                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="9">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row px-3">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $states->firstItem() }}
                        to {{ $states->lastItem() }} of {{ $states->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $states->appends(['filters' => Request::get('filters'),'search' => Request::get('search'),'status' => Request::get('status')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

@endsection