@extends('backend.layout')
@section('title', $title)

@section('headerStyles')


@endsection

@section('content')

    <?php
//            dump($settings);

    $setting = (isset($settings)) ? $settings : '';
    ?>

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Settings'
              ),'Basic'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">

                    {{ laravelReturnMessageShow() }}

                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Settings</strong>


                        </div>
                        <div class="card-body">
                            <?php
                            //                            dump($settings);
                            //                            dump(vdcSettings());
                            ?>


                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#Settings">Settings</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#EditSettings">EditSettings</a>
                                </li>

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane container active" id="Settings">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="card">
                                                <div class="card-header">
                                                    Admin
                                                </div>
                                                <div class="card-body">

                                                    {{--<div class="form-group">--}}
                                                    {{--<label>Vendor logo</label>--}}
                                                    {{--<div class="input-group">--}}
                                                    {{--<input type="file" name="vendor_logo" />--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="form-group">--}}
                                                    {{--<label>Admin logo</label>--}}
                                                    {{--<div class="input-group">--}}
                                                    {{--<input type="file" name="admin_logo" />--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}
                                                    <div class="form-group">
                                                        <label>Admin Email</label>
                                                        <div class="input-group">
                                                            {{ isset($setting->admin_email) ?$setting->admin_email:'' }}

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Admin Name</label>
                                                        <div class="input-group">
                                                            {{ isset($setting->admin_name) ?$setting->admin_name:'' }}

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Address</label>
                                                        <div class="input-group">
                                                            {{ (isset($setting->address)) ?$setting->address:'' }}

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Toll free Number</label>
                                                        <div class="input-group">

                                                            {{ (isset($setting->tolfree_email))?$setting->tolfree_email:'' }}


                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="card">
                                                <div class="card-header">
                                                    currency
                                                </div>
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label>currency USD to INR</label>
                                                        <div class="input-group">
                                                            {{ (isset($setting->currency_usd_to_inr))?$setting->currency_usd_to_inr : '' }}

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Ammachethi vanta Package Price (INR)</label>
                                                        <div class="input-group">
                                                            {{ (isset($setting->ammachethi_vanta_price))?$setting->ammachethi_vanta_price : '' }}

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card">
                                                <div class="card-header">
                                                    Social Networks
                                                </div>
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label>Social FB URL</label>
                                                        <div class="input-group">
                                                            {{ (isset($setting->social_fb)) ? $setting->social_fb : '' }}

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Social Twitter URL</label>
                                                        <div class="input-group">
                                                            {{ (isset($setting->social_tw)) ? $setting->social_tw : '' }}

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Social Linkedin</label>
                                                        <div class="input-group">
                                                            {{ (isset($setting->social_in))? $setting->social_in :'' }}

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>


                                    </div>


                                </div>
                                <div class="tab-pane container fade" id="EditSettings">

                                    <form class="settings" method="post" action="{{route('admin_settings')}}">
                                        {{ csrf_field() }}


                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="card">
                                                    <div class="card-header">
                                                        Admin
                                                    </div>
                                                    <div class="card-body">

                                                        {{--<div class="form-group">--}}
                                                        {{--<label>Vendor logo</label>--}}
                                                        {{--<div class="input-group">--}}
                                                        {{--<input type="file" name="vendor_logo" />--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="form-group">--}}
                                                        {{--<label>Admin logo</label>--}}
                                                        {{--<div class="input-group">--}}
                                                        {{--<input type="file" name="admin_logo" />--}}
                                                        {{--</div>--}}
                                                        {{--</div>--}}
                                                        <div class="form-group">
                                                            <label>Admin Email</label>
                                                            <div class="input-group">
                                                                <input class="form-control" type="text"
                                                                       value="{{ !empty(old('admin_email')) ? old('admin_email') : ((($setting) && ($setting->admin_email)) ? $setting->admin_email : '') }}"
                                                                       name="admin_email"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Admin Name</label>
                                                            <div class="input-group">
                                                                <input class="form-control" type="text"
                                                                       value="{{ !empty(old('admin_name')) ? old('admin_name') : ((($setting) && ($setting->admin_name)) ? $setting->admin_name : '') }}"
                                                                       name="admin_name"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Address</label>
                                                            <div class="input-group">
                                                                <input class="form-control" type="text" name="address"
                                                                       value="{{ !empty(old('address')) ? old('address') : ((($setting) && ($setting->address)) ? $setting->address : '') }}"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Toll free Number</label>
                                                            <div class="input-group">
                                                                <input class="form-control" type="text"
                                                                       value="{{ !empty(old('tolfree_email')) ? old('tolfree_email') : ((($setting) && ($setting->tolfree_email)) ? $setting->tolfree_email : '') }}"
                                                                       name="tolfree_email"/>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="card">
                                                    <div class="card-header">
                                                        currency
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="form-group">
                                                            <label>currency USD to INR</label>
                                                            <div class="input-group">
                                                                <input class="form-control" type="text"
                                                                       value="{{ !empty(old('currency_usd_to_inr')) ? old('currency_usd_to_inr') : ((($setting) && ($setting->currency_usd_to_inr)) ? $setting->currency_usd_to_inr : '') }}"
                                                                       name="currency_usd_to_inr"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Amma Chethi Vanta Price (INR)</label>
                                                            <div class="input-group">
                                                                <input class="form-control" type="text"
                                                                       value="{{ !empty(old('ammachethi_vanta_price')) ? old('ammachethi_vanta_price') : ((($setting) && ($setting->ammachethi_vanta_price)) ? $setting->ammachethi_vanta_price : '') }}"
                                                                       name="ammachethi_vanta_price"/>


                                                            </div>
                                                            <small>
                                                                EX: 5:12000,10:14000,15:20000 <br />
                                                                5kg:Rs.12000, 10kg:Rs.14000, 15kg:Rs.20000

                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="card">
                                                    <div class="card-header">
                                                        Social Networks
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="form-group">
                                                            <label>Social FB URL</label>
                                                            <div class="input-group">
                                                                <input class="form-control" type="text"
                                                                       value="{{ !empty(old('social_fb')) ? old('social_fb') : ((($setting) && ($setting->social_fb)) ? $setting->social_fb : '') }}"
                                                                       name="social_fb"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Social Twitter URL</label>
                                                            <div class="input-group">
                                                                <input class="form-control" type="text"
                                                                       value="{{ !empty(old('social_tw')) ? old('social_tw') : ((($setting) && ($setting->social_tw)) ? $setting->social_tw : '') }}"
                                                                       name="social_tw"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Social Linkedin</label>
                                                            <div class="input-group">
                                                                <input class="form-control" type="text"
                                                                       value="{{ !empty(old('social_in')) ? old('social_in') : ((($setting) && ($setting->social_in)) ? $setting->social_in : '') }}"
                                                                       name="social_in"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="col-md-12">

                                                <button type="submit" class="btn btn-primary">Update</button>

                                            </div>

                                        </div>


                                    </form>


                                </div>


                            </div>
                        </div>
                    </div>

                </div>

            </div><!-- .animated -->
        </div><!-- .content -->
    </div>

@endsection
@section('footerScripts')
    <script>
        $(function () {

            $('.settingsss').validate({
                // ignore: [],
                errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.text-danger').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.text-danger').remove();
                },
                rules: {
                    admin_email: {
                        required: true
                    },
                    admin_name: {
                        required: true
                    },
                    address: {
                        required: true,
                    },

                    tolfree_email: {
                        required: true
                    },
                    currency_usd_to_inr: {
                        required: true,
                    },
                },
                messages: {
                    admin_email: {
                        required: 'Please enter name'
                    },
                    admin_name: {
                        required: 'Please Select Type'
                    },
                    address: {
                        required: 'Please Select Date'
                    },

                    tolfree_email: {
                        required: 'Please Select Remind '
                    },
                    currency_usd_to_inr: {
                        required: 'Please Enter Description'
                    }
                },
            });
        });
    </script>
@endsection