@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #3c8dbc;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Products'
              ),'Products'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">


                <div class="col-lg-4">
                    @include('backend.vendors.userinfo')
                </div>
                <div class="col-lg-8">
                    @include('backend.vendors.nav')

                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Products</strong>
                            <a href="{{route('addNewProducts')}}" class="btn btn-primary" style="float:right;">+ Add
                                Products
                            </a>

                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th>Vendor</th>
                                    <th>Product Picture</th>
                                    <th>Product Name</th>
                                    <th>Category</th>

                                    <th>View on Website</th>
                                    <th>Approval Status</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if(count($products) > 0)
                                    @foreach($products AS $product_item)

                                        <?php
                                        //                                                                                dump($product_item->productSKUs);
                                        ?>

                                        <tr>
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>
                                                @if(isset($product_item->getVendor->name))
                                                    <a href="{{route('admin_vendor_account_details', ['id'=>$product_item->getVendor->id])}}">
                                                        {{$product_item->getVendor->name}}
                                                    </a>
                                                @else
                                                    --
                                                @endif
                                            </td>
                                            <td>
                                                <?php
                                                $images = getImagesByProduct($product_item->p_id);

                                                //                                                        dump($images);

                                                //                                                        dd($images);

                                                if (!empty($images) && count($images) > 0) {
                                                    $image_src = $images[0];
                                                } else {
                                                    $image_src = 'https://dummyimage.com/600x400/f2f2f2/000000';
                                                }

                                                ?>

                                                <a href="">
                                                    <img width="50"
                                                         src="{{$image_src}}"
                                                         alt=""
                                                         class="img-thumb"></a>
                                            </td>

                                            <td>
                                                <a href="{{route('AdminProductView',['id'=>$product_item->p_id])}}">
                                                    {{$product_item->p_name}}
                                                </a>
                                            </td>
                                            <td>

                                                {{getCategory($product_item->p_cat_id)->category_name}}

                                            </td>


                                            <td>
                                                @if($product_item->p_status==1)
                                                    <a href="{{ route('productPage', ['category'=>getCategory($product_item->p_cat_id)->category_alias,'product'=>$product_item->p_alias]) }}"
                                                       target="_blank" data-toggle="tooltip"
                                                       title="View on vdesiconnect.com">
                                                        <i class="fa fa-desktop" aria-hidden="true"></i>
                                                    </a>
                                                @endif
                                            </td>
                                            <td>
                                                {{ productStatus($product_item->p_status) }}
                                            </td>
                                            <td class="options">


                                                <a href="{{route('AdminProductView',['id'=>$product_item->p_id])}}" class="dropdown-item">
                                                    <i class="fa fa-eye"></i> View
                                                </a>


                                                <a class="dropdown-item"
                                                   href="{{ route('addNewProducts',['id'=>$product_item->p_id])}}"><i
                                                            class="fa fa-pencil"></i> Edit</a>


                                                {{--<a class="dropdown-item"--}}
                                                {{--href="">--}}
                                                {{--<i class="fa fa-eye"></i>--}}
                                                {{--View--}}
                                                {{--</a>--}}




                                                <a href="#" data-toggle="modal"
                                                   data-target="#productDetails{{ $product_item->p_id }}"
                                                   class="dropdown-item">
                                                    <i class="fa fa-eye"></i> Manage Approval
                                                </a>


                                                <div id="productDetails{{ $product_item->p_id }}"
                                                     class="modal fade productDetails"
                                                     role="dialog">
                                                    <div class="modal-dialog modal-xl">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">
                                                                </h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">
                                                                    &times;
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">


                                                                <form method="POST"
                                                                      id="productdetails_box_{{ $product_item->p_id }}"
                                                                      action="{{ route('ProductApprove') }}"
                                                                      accept-charset="UTF-8" class="form-horizontal "
                                                                      style="display:inline">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="p_id"
                                                                           value="{{$product_item->p_id}}"/>

                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <table class="table table-bordered">
                                                                                <thead>
                                                                                <tr>
                                                                                    <td>Vendor Price</td>
                                                                                    <td>vdc Commission</td>
                                                                                    <td>vdc sku_vdc_final_price</td>
                                                                                    <td>vdc store discount</td>
                                                                                    <td>vdc store price</td>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>

                                                                                <?php
                                                                                //                                                                                dump($product_item->productSKUs)
                                                                                ?>
                                                                                @if (count($product_item->productSKUs) > 0)
                                                                                    @foreach($product_item->productSKUs AS $skus)

                                                                                        <tr id="sku_{{$skus->sku_id}}"
                                                                                            class="sku_{{$skus->sku_id}}">
                                                                                            <td>
                                                                                                <div class="form-group">
                                                                                                    <label>
                                                                                                        VDC Vendor  Price
                                                                                                    </label>

                                                                                                    <div class="input-group">
                                                                                                        <div class="input-group-prepend">
                                                                                                            <div class="input-group-text">
                                                                                                                {!! currencySymbol('INR')  !!}
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <input type="text"
                                                                                                               name="options[{{$skus->sku_id}}][sku_vendor_price]"
                                                                                                               readonly
                                                                                                               value="{{$skus->sku_vendor_price}}"
                                                                                                               class="form-control  vdc_vendor_price"/>
                                                                                                    </div>

                                                                                                </div>

                                                                                            </td>
                                                                                            <td>

                                                                                                <div class="form-group">
                                                                                                    <label>VDC
                                                                                                        commission</label>

                                                                                                    <div class="input-group">

                                                                                                        <div class="input-group-prepend">
                                                                                                            <div class="input-group-text">
                                                                                                                <select class="vdc_commission_type"
                                                                                                                        name="options[{{$skus->sku_id}}][sku_vdc_commission_type]">
                                                                                                                    <option value="">

                                                                                                                        Type
                                                                                                                    </option>
                                                                                                                    <option value="fixed" {{ (isset($skus->sku_vdc_commission_type) && $skus->sku_vdc_commission_type == 'fixed' ) ? 'selected' : ''}}>
                                                                                                                        fixed
                                                                                                                    </option>
                                                                                                                    <option value="percentage" {{ (isset($skus->sku_vdc_commission_type) && $skus->sku_vdc_commission_type == 'percentage' ) ? 'selected' : ''}}>
                                                                                                                        %
                                                                                                                    </option>
                                                                                                                </select>

                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="input-group-prepend">
                                                                                                            <div class="input-group-text">
                                                                                                                {!! currencySymbol('INR')  !!}
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <input class="form-control vdc_commission"
                                                                                                               id="p_vdc_vendor_discount"
                                                                                                               type="text"
                                                                                                               placeholder="VDC Vendor commission"
                                                                                                               name="options[{{$skus->sku_id}}][sku_vdc_commission_value]"
                                                                                                               value="{{$skus->sku_vdc_commission_value}}"
                                                                                                               value="">

                                                                                                    </div>
                                                                                                </div>

                                                                                            </td>
                                                                                            {{--<td>--}}
                                                                                            {{--{{ $skus->sku_vdc_commission_value }}--}}
                                                                                            {{--</td>--}}
                                                                                            <td>

                                                                                                <div class="form-group">
                                                                                                    <label>
                                                                                                        VDC Store Final Price
                                                                                                    </label>

                                                                                                    <div class="input-group">
                                                                                                        <div class="input-group-prepend">
                                                                                                            <div class="input-group-text">
                                                                                                                {!! currencySymbol('INR')  !!}
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <input type="text"
                                                                                                               readonly
                                                                                                               value="{{$skus->sku_vdc_final_price}}"
                                                                                                               name="options[{{$skus->sku_id}}][sku_vdc_final_price]"
                                                                                                               class="form-control vdc_store_price"/>
                                                                                                    </div>

                                                                                                </div>

                                                                                                {{--                                                                                                {{ $skus->sku_vdc_final_price }}--}}
                                                                                            </td>
                                                                                            <td>

                                                                                                <div class="form-group">
                                                                                                    <label>
                                                                                                        VDC Store  Lising  Discount
                                                                                                    </label>

                                                                                                    <div class="input-group">

                                                                                                        <div class="input-group-prepend">
                                                                                                            <div class="input-group-text">
                                                                                                                <select class="vdc_store_discount_type"
                                                                                                                        name="options[{{$skus->sku_id}}][sku_store_discount_type]">
                                                                                                                    <option value="">

                                                                                                                        Type
                                                                                                                    </option>
                                                                                                                    <option value="fixed" {{ (isset($skus->sku_store_discount_type) && $skus->sku_store_discount_type == 'fixed' ) ? 'selected' : ''}}>
                                                                                                                        fixed
                                                                                                                    </option>
                                                                                                                    <option value="percentage" {{ (isset($skus->sku_store_discount_type) && $skus->sku_store_discount_type == 'percentage' ) ? 'selected' : ''}}>
                                                                                                                        %
                                                                                                                    </option>
                                                                                                                </select>

                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="input-group-prepend">
                                                                                                            <div class="input-group-text">
                                                                                                                {!! currencySymbol('INR')  !!}
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <input class="form-control vdc_store_discount"
                                                                                                               id="vdc_store_discount"
                                                                                                               type="text"
                                                                                                               placeholder="VDC Vendor commission"
                                                                                                               name="options[{{$skus->sku_id}}][sku_store_discount_value]"
                                                                                                               value="{{$skus->sku_store_discount_value}}"
                                                                                                               value="">

                                                                                                    </div>
                                                                                                </div>


                                                                                                {{ $skus->sku_store_discount_type }}
                                                                                            </td>
                                                                                            {{--<td>--}}
                                                                                            {{--{{ $skus->sku_store_discount_value }}--}}
                                                                                            {{--</td>--}}
                                                                                            <td>
                                                                                                <div class="form-group">
                                                                                                    <label>
                                                                                                        VDC Store Price
                                                                                                    </label>

                                                                                                    <div class="input-group">
                                                                                                        <input type="text"
                                                                                                               readonly
                                                                                                               value="{{$skus->sku_store_price}}"
                                                                                                               name="options[{{$skus->sku_id}}][sku_store_price]"
                                                                                                               class="form-control vdc_store_discount_price"/>
                                                                                                    </div>

                                                                                                </div>


                                                                                                {{ $skus->sku_store_price }}
                                                                                            </td>
                                                                                        </tr>

                                                                                    @endforeach


                                                                                @endif


                                                                                </tbody>
                                                                            </table>


                                                                        </div>


                                                                        <div class="col-md-6">

                                                                            <div class="card">
                                                                                <div class="card-header">
                                                                                    Additional Price
                                                                                </div>
                                                                                <div class="card-body">


                                                                                    <div class="form-group">
                                                                                        <label> Delivery Charges</label>

                                                                                        <div class="input-group">
                                                                                            <input type="text"
                                                                                                   name="p_delivery_charges"
                                                                                                   value="{{$product_item->p_delivery_charges}}"
                                                                                                   class="form-control p_delivery_charges"/>
                                                                                        </div>

                                                                                    </div>


                                                                                </div>

                                                                            </div>

                                                                        </div>
                                                                        <div class="col-md-6">


                                                                            <div class="card">
                                                                                <div class="card-header">
                                                                                    Product Status
                                                                                </div>
                                                                                <div class="card-body">
                                                                                    <div class="form-group">

                                                                                        <select class="p_status form-control"
                                                                                                required
                                                                                                name="p_status">
                                                                                            <option value="">--Select
                                                                                                Status--
                                                                                            </option>
                                                                                            <option value="1" {{ (isset($product_item->p_status) && $product_item->p_status == '1' ) ? 'selected' : ''}}>
                                                                                                Approved
                                                                                            </option>
                                                                                            <option value="2" {{ (isset($product_item->p_status) && $product_item->p_status == '2' ) ? 'selected' : ''}}>
                                                                                                Rejected
                                                                                            </option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                        </div>
                                                                    </div>

                                                                    <div class="row text-center">
                                                                        <div class="col-md-12">
                                                                            <button type="submit"
                                                                                    class="btn btn-success">
                                                                                Submit
                                                                            </button>
                                                                        </div>

                                                                    </div>


                                                                </form>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>


                                                <form method="POST" id="productbox_form_{{ $product_item->p_id }}"
                                                      action="{{ route('admin_products') }}"
                                                      accept-charset="UTF-8" class="form-horizontal product_box_form"
                                                      style="display:inline">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="p_id"
                                                           value="{{ $product_item->p_id }}"/>
                                                    <button type="submit" class="dropdown-item"
                                                            title="Delete Product"
                                                            onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                        <i class="fa fa-trash"></i> Delete
                                                    </button>

                                                </form>


                                            </td>
                                        </tr>

                                    @endforeach

                                @else
                                    <tr>
                                        <td colspan="9" align="center">

                                            No results

                                        </td>

                                    </tr>

                                @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10">
                            <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                                 aria-live="polite">Showing {{ $products->firstItem() }}
                                to {{ $products->lastItem() }} of {{ $products->total() }} entries
                            </div>
                        </div>
                        <div class="col-md-1 text-right">
                            <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                                {!! $products->appends(['search' => Request::get('search'),'product_name' => Request::get('product_name'),'search_category' => Request::get('search_category')])->render() !!}
                            </div>
                        </div>
                    </div>
                </div>



            </div>

        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')
    {!! javascriptFunctions() !!}
    <script>
        $(function () {

// Vendor calc
            $('.vdc_vendor_price, .vdc_commission').on("input", function () {

                var productDetails = $(this).closest('tr').attr('id');

                discountCalcWithParent('#' + productDetails, '.vdc_vendor_price', '.vdc_commission_type', '.vdc_commission', '.vdc_store_price', 'add');
            });

            $('.vdc_commission_type').on('change', function () {

                var productDetails = $(this).closest('tr').attr('id');

                discountCalcWithParent('#' + productDetails, '.vdc_vendor_price', '.vdc_commission_type', '.vdc_commission', '.vdc_store_price', 'add');
            });

            // store calc

            $('.vdc_store_price, .vdc_store_discount').on("input", function () {
                var productDetails = $(this).closest('tr').attr('id');

                discountCalcWithParent('#' + productDetails, '.vdc_store_price', '.vdc_store_discount_type', '.vdc_store_discount', '.vdc_store_discount_price');
            });

            $('.vdc_store_discount_type').on('change', function () {
                var productDetails = $(this).closest('tr').attr('id');
                discountCalcWithParent('#' + productDetails, '.vdc_store_price', '.vdc_store_discount_type', '.vdc_store_discount', '.vdc_store_discount_price');
            });


            $('.productdetails_box_form tr').each(function () {

                var productDetails = $(this).attr('id');

                discountCalcWithParent('#' + productDetails, ' .vdc_vendor_price', ' .vdc_commission_type', ' .vdc_commission', ' .vdc_store_price', 'add');

                discountCalcWithParent('#' + productDetails, ' .vdc_store_price', ' .vdc_store_discount_type', ' .vdc_store_discount', ' .vdc_store_discount_price');
            });


        });
    </script>


@endsection