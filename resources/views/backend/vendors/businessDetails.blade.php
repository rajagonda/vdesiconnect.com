@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #3c8dbc;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

@endsection

@section('content')

    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'vendorslist'=>'vendorslist',
               ''=>'Vendor Info'
               ),'Vendor Info'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-4">
                    @include('backend.vendors.userinfo')
                </div>
                <div class="col-lg-8">

                    @include('backend.vendors.nav')


                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Vendor Business Details</strong>

                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Vendor Business Name</th>
                                    <th scope="col">Bank Account Name</th>
                                    <th scope="col">Bank Account Number</th>
                                    <th scope="col">Status</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($businessDetails)
                                    <tr>
                                        <th scope="row">{{ ($businessDetails->vb_id) ?  $businessDetails->vb_id:'--'}}</th>
                                        <td>{{ ($businessDetails->vb_name) ?  $businessDetails->vb_name:'--'}}</td>

                                        <td>{{ ($businessDetails->vb_account_name) ?  $businessDetails->vb_account_name:'--'}}</td>
                                        <td>{{ ($businessDetails->vb_account_number) ?  $businessDetails->vb_account_number:'--'}}</td>

                                        <td>{{ ($businessDetails->vb_status) ?  $businessDetails->vb_status:'--'}}</td>
                                        <td>
                                            <a href="#" data-toggle="modal"
                                               data-target="#vendorDetailsEdit{{ $businessDetails->vb_id }}"
                                               class="text-uppercase pr-3 btn btn-primary btn-sm">
                                               Edit
                                            </a>
                                            <a href="#" data-toggle="modal"
                                               data-target="#vendorDetails{{ $businessDetails->vb_id }}"
                                               class="text-uppercase pr-3 btn btn-default btn-sm">
                                                 View
                                            </a>


                                            <!-- Modal -->
                                            <div id="vendorDetailsEdit{{ $businessDetails->vb_id }}" class="modal fade"
                                                 role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">
                                                                Update Business details
                                                            </h4>
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">
                                                                &times;
                                                            </button>
                                                        </div>
                                                        <form method="POST"
                                                              action="{{ route('BusinessDetailsInfo',['id'=>$businessDetails->vb_vendor_id]) }}">
                                                            <div class="modal-body">

                                                                <div class="modal-body">
                                                                    {{ csrf_field() }}
                                                                    <div class="form-group">
                                                                        <label>Name</label>
                                                                        <input id="	vb_name" type="text"
                                                                               placeholder="Name"
                                                                               class="form-control"
                                                                               name="vb_name"
                                                                               value="{{ !empty(old('vb_name')) ? old('vb_name') : ((($businessDetails) && ($businessDetails->vb_name)) ? $businessDetails->vb_name : '') }}">
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label>VB TAN</label>
                                                                        <input id="vb_tan" type="text"
                                                                               placeholder="VB TAN"
                                                                               class="form-control"
                                                                               name="vb_tan"
                                                                               value="{{ !empty(old('vb_tan')) ? old('vb_tan') : ((($businessDetails) && ($businessDetails->vb_tan)) ? $businessDetails->vb_tan : '') }}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>GST</label>
                                                                        <input id="vb_gst" type="text"
                                                                               placeholder="GST"
                                                                               class="form-control"
                                                                               name="vb_gst"
                                                                               value="{{ !empty(old('vb_gst')) ? old('vb_gst') : ((($businessDetails) && ($businessDetails->vb_gst)) ? $businessDetails->vb_gst : '') }}">
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label>Address Line 01</label>
                                                                        <input id="vb_address1" type="text"
                                                                               placeholder="Address"
                                                                               class="form-control"
                                                                               name="vb_address1"
                                                                               value="{{ !empty(old('vb_address1')) ? old('vb_address1') : ((($businessDetails) && ($businessDetails->vb_address1)) ? $businessDetails->vb_address1 : '') }}"
                                                                        >
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Address Line 02</label>
                                                                        <input id="vb_address2" type="text"
                                                                               placeholder="Address"
                                                                               class="form-control"
                                                                               name="vb_address2"
                                                                               value="{{ !empty(old('vb_address2')) ? old('vb_address2') : ((($businessDetails) && ($businessDetails->vb_address2)) ? $businessDetails->vb_address2 : '') }}"
                                                                        >
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>State</label>
                                                                        <select class="form-control" name="vb_state">
                                                                            <option value="Telangana" {{ (!empty(old('vb_state')) && old('vb_state')=='Telangana')  ? 'selected' : (((@$businessDetails) && (@$businessDetails->vb_state == 'Telangana')) ? 'selected' : '') }}>
                                                                                Telangana
                                                                            </option>
                                                                            <option value="Andhra Pradesh" {{ (!empty(old('vb_state')) && old('vb_state')=='Andhra Pradesh')  ? 'selected' : (((@$businessDetails) && (@$businessDetails->vb_state == 'Andhra Pradesh')) ? 'selected' : '') }}>
                                                                                Andhra Pradesh
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>City</label>
                                                                        <select class="form-control" name="vb_city">
                                                                            <option value="Hyderabad" {{ (!empty(old('vb_city')) && old('vb_city')=='Hyderabad')  ? 'selected' : (((@$businessDetails) && (@$businessDetails->vb_city == 'Hyderabad')) ? 'selected' : '') }}>
                                                                                Hyderabad
                                                                            </option>
                                                                            <option value="Secunderabad" {{ (!empty(old('vb_city')) && old('vb_city')=='Secunderabad')  ? 'selected' : (((@$businessDetails) && (@$businessDetails->vb_city == 'Secunderabad')) ? 'selected' : '') }}>
                                                                                Secunderabad
                                                                            </option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Pincode</label>
                                                                    <input id="vb_pincode" type="text"
                                                                           placeholder="Pincode"
                                                                           class="form-control"
                                                                           name="vb_pincode"
                                                                           value="{{ !empty(old('vb_pincode')) ? old('vb_pincode') : ((($businessDetails) && ($businessDetails->vb_pincode)) ? $businessDetails->vb_pincode : '') }}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Account name</label>
                                                                    <input id="vb_account_name" type="text"
                                                                           placeholder="Account name"
                                                                           class="form-control"
                                                                           name="vb_account_name"
                                                                           value="{{ !empty(old('vb_account_name')) ? old('vb_account_name') : ((($businessDetails) && ($businessDetails->vb_account_name)) ? $businessDetails->vb_account_name : '') }}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Account Number</label>
                                                                    <input id="vb_account_number" type="text"
                                                                           placeholder="Account number"
                                                                           class="form-control"
                                                                           name="vb_account_number"
                                                                           value="{{ !empty(old('vb_account_number')) ? old('vb_account_number') : ((($businessDetails) && ($businessDetails->vb_account_number)) ? $businessDetails->vb_account_number : '') }}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>IFSC</label>
                                                                    <input id="vb_ifsc" type="text"
                                                                           placeholder="IFSC"
                                                                           class="form-control"
                                                                           name="vb_ifsc"
                                                                           value="{{ !empty(old('vb_ifsc')) ? old('vb_ifsc') : ((($businessDetails) && ($businessDetails->vb_ifsc)) ? $businessDetails->vb_ifsc : '') }}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Bank</label>
                                                                    <input id="vb_bank" type="text"
                                                                           placeholder="bank"
                                                                           class="form-control"
                                                                           name="vb_bank"
                                                                           value="{{ !empty(old('vb_bank')) ? old('vb_bank') : ((($businessDetails) && ($businessDetails->vb_bank)) ? $businessDetails->vb_bank : '') }}">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Branch</label>
                                                                    <input id="vb_branch" type="text"
                                                                           placeholder="Branch"
                                                                           class="form-control"
                                                                           name="vb_branch"
                                                                           value="{{ !empty(old('vb_branch')) ? old('vb_branch') : ((($businessDetails) && ($businessDetails->vb_branch)) ? $businessDetails->vb_branch : '') }}">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label>Bank State</label>
                                                                    <input id="vb_bank_state" type="text"
                                                                           placeholder="bank state"
                                                                           class="form-control"
                                                                           name="vb_bank_state"
                                                                           value="{{ !empty(old('vb_bank_state')) ? old('vb_bank_state') : ((($businessDetails) && ($businessDetails->vb_bank_state)) ? $businessDetails->vb_bank_state : '') }}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Bank City</label>
                                                                    <input id="vb_bank_city" type="text"
                                                                           placeholder="bank city"
                                                                           class="form-control"
                                                                           name="vb_bank_city"
                                                                           value="{{ !empty(old('vb_bank_city')) ? old('vb_bank_city') : ((($businessDetails) && ($businessDetails->vb_bank_city)) ? $businessDetails->vb_bank_city : '') }}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Type</label>
                                                                    <input id="vb_type" type="text"
                                                                           placeholder="type"
                                                                           class="form-control"
                                                                           name="vb_type"
                                                                           value="{{ !empty(old('vb_type')) ? old('vb_type') : ((($businessDetails) && ($businessDetails->vb_type)) ? $businessDetails->vb_type : '') }}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Pan card</label>
                                                                    <input id="vb_pan" type="text"
                                                                           placeholder="Pan card"
                                                                           class="form-control"
                                                                           name="vb_pan"
                                                                           value="{{ !empty(old('vb_pan')) ? old('vb_pan') : ((($businessDetails) && ($businessDetails->vb_pan)) ? $businessDetails->vb_pan : '') }}">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Cheque</label>
                                                                    <input id="vb_cheque" type="text"
                                                                           placeholder="Cheque"
                                                                           class="form-control"
                                                                           name="vb_cheque"
                                                                           value="{{ !empty(old('vb_cheque')) ? old('vb_cheque') : ((($businessDetails) && ($businessDetails->vb_cheque)) ? $businessDetails->vb_cheque : '') }}">
                                                                </div>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                                <button type="submit" class="btn btn-primary">Update
                                                                    Details
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>


                                            <!-- Modal -->
                                            <div id="vendorDetails{{ $businessDetails->vb_id }}" class="modal fade"
                                                 role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">
                                                            </h4>
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">
                                                                &times;
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <table class="table table-striped">

                                                                    <tbody>

                                                                    <tr>
                                                                        <td>Business Name
                                                                            : {{($businessDetails->vb_name) ? $businessDetails->vb_name:'--'}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>TAN
                                                                            : {{($businessDetails->vb_tan) ? $businessDetails->vb_tan:'--'}}</td>
                                                                    </tr>
                                                                    <tr>

                                                                        <td>GST Number
                                                                            : {{($businessDetails->vb_gst) ? $businessDetails->vb_gst:'--'}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Address 1
                                                                            : {{($businessDetails->vb_address1) ? $businessDetails->vb_address1:'--'}}</td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td>Address 2
                                                                            : {{($businessDetails->vb_address2) ? $businessDetails->vb_address2:'--'}}</td>

                                                                    </tr>
                                                                    <tr>

                                                                        <td>State
                                                                            : {{($businessDetails->vb_state) ? $businessDetails->vb_state:'--'}}</td>
                                                                    </tr>
                                                                    <tr>

                                                                        <td>City
                                                                            : {{($businessDetails->vb_city) ? $businessDetails->vb_city:'--'}}</td>
                                                                    </tr>


                                                                    <tr>

                                                                        <td>Pincode
                                                                            : {{($businessDetails->vb_pincode) ? $businessDetails->vb_pincode:'--'}}</td>
                                                                    </tr>

                                                                    <tr>

                                                                        <td>Account Name
                                                                            : {{($businessDetails->vb_account_name) ? $businessDetails->vb_account_name:'--'}}</td>
                                                                    </tr>

                                                                    <tr>

                                                                        <td>Account Number
                                                                            : {{($businessDetails->vb_account_number) ? $businessDetails->vb_account_number:'--'}}</td>
                                                                    </tr>

                                                                    <tr>

                                                                        <td>IFSC
                                                                            : {{($businessDetails->vb_ifsc) ? $businessDetails->vb_ifsc:'--'}}</td>
                                                                    </tr>

                                                                    <tr>

                                                                        <td>Bank Name
                                                                            : {{($businessDetails->vb_bank) ? $businessDetails->vb_bank:'--'}}</td>
                                                                    </tr>

                                                                    <tr>

                                                                        <td>Branch
                                                                            : {{($businessDetails->vb_branch) ? $businessDetails->vb_branch:'--'}}</td>
                                                                    </tr>

                                                                    <tr>

                                                                        <td>Bank State
                                                                            : {{($businessDetails->vb_bank_state) ? $businessDetails->vb_bank_state:'--'}}</td>
                                                                    </tr>

                                                                    <tr>

                                                                        <td>Bank City
                                                                            : {{($businessDetails->vb_bank_city) ? $businessDetails->vb_bank_city:'--'}}</td>
                                                                    </tr>

                                                                    <tr>

                                                                        <td>Business Type
                                                                            : {{($businessDetails->vb_type) ? $businessDetails->vb_type:'--'}}</td>
                                                                    </tr>
                                                                    <tr>

                                                                        <td>PAN
                                                                            : {{($businessDetails->vb_pan) ? $businessDetails->vb_pan:'--'}}</td>
                                                                    </tr>

                                                                    <tr>

                                                                        <td>Image (Address Proof) :

                                                                            @if(isset($businessDetails->vb_address_proof))
                                                                                @php
                                                                                    $scode='';
                                                                                    $image= (@$businessDetails->vb_address_proof) ? '<img src="/uploads/vendor/proofs/thumbs/'.@$businessDetails->vb_address_proof.'" width="50px" />' : '-';
                                                                                @endphp
                                                                                @if($image!='-')
                                                                                    <div class="imagediv">
                                                                                        <a data-fancybox=""
                                                                                           class="popupimages"
                                                                                           href="{{url('/uploads/vendor/proofs/'.@$businessDetails->vb_address_proof)}}">
                                                                                            {!! $image !!}
                                                                                        </a>
                                                                                    </div>
                                                                                @endif
                                                                            @else
                                                                                --
                                                                            @endif


                                                                        </td>
                                                                    </tr>

                                                                    <tr>

                                                                        <td>Image (Cancelled Cheque) :
                                                                            @if(isset($businessDetails->vb_cheque))
                                                                                @php
                                                                                    $scode='';
                                                                                    $image= (@$businessDetails->vb_cheque) ? '<img src="/uploads/vendor/proofs/thumbs/'.@$businessDetails->vb_cheque.'" width="50px" />' : '-';
                                                                                @endphp
                                                                                @if($image!='-')
                                                                                    <div class="imagediv">
                                                                                        <a data-fancybox=""
                                                                                           class="popupimages"
                                                                                           href="{{url('/uploads/vendor/proofs/'.@$businessDetails->vb_cheque)}}">
                                                                                            {!! $image !!}
                                                                                        </a>
                                                                                    </div>
                                                                                @endif
                                                                            @else
                                                                                --
                                                                            @endif

                                                                        </td>
                                                                    </tr>


                                                                    <tr>

                                                                        <td>Status
                                                                            : {{ $businessDetails->va_status }}</td>
                                                                    </tr>
                                                                </table>
                                                            </div>

                                                            @if($businessDetails->vb_status!='approved')




                                                                <form method="POST" id="approvedbutton"
                                                                      action="{{ route('BusinessDetailsApprove') }}"
                                                                      accept-charset="UTF-8" class="form-horizontal"
                                                                      style="display:inline">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="vb_id"
                                                                           value="{{$businessDetails->vb_id}}"/>
                                                                    <input type="hidden" name="vb_type_new"
                                                                           value="approved"/>
                                                                    <input type="hidden" name="vb_vendor_id"
                                                                           value="{{$businessDetails->vb_vendor_id}}"/>
                                                                    <button type="submit" class="btn btn-success"
                                                                            title="Confirm Approve"
                                                                            onclick="return confirm('Confirm Approve')">
                                                                        Approve
                                                                    </button>

                                                                </form>


                                                            @else

                                                                <a class="btn btn-success" href="javascript:void(0)">Approved
                                                                    Successfully</a>


                                                                <form method="POST" id="approvedbutton"
                                                                      action="{{ route('BusinessDetailsApprove') }}"
                                                                      accept-charset="UTF-8" class="form-horizontal"
                                                                      style="display:inline">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="vb_id"
                                                                           value="{{$businessDetails->vb_id}}"/>
                                                                    <input type="hidden" name="vb_type_new"
                                                                           value="rejected"/>
                                                                    <input type="hidden" name="vb_vendor_id"
                                                                           value="{{$businessDetails->vb_vendor_id}}"/>
                                                                    <button type="submit" class="btn btn-success"
                                                                            title="Confirm Reject"
                                                                            onclick="return confirm('Confirm Reject')">
                                                                        Reject
                                                                    </button>

                                                                </form>


                                                            @endif


                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="6">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footer_scripts')

@endsection