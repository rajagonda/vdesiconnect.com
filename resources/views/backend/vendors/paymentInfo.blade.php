@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #3c8dbc;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              'vendorslist'=>'Vendors',
              ''=>'Vendor Info'
              ),'Vendor Info'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-4">
                    @include('backend.vendors.userinfo')
                </div>
                <div class="col-lg-8">

                    @include('backend.vendors.nav')


                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Vendor Payment Info</strong>
                            <a href="#" data-toggle="modal"
                               data-target="#vendorDetailsAdd"
                               class="text-uppercase pr-3 btn btn-primary float-right">
                                + Add Payment
                            </a>
                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Current balance</th>
                                    <th scope="col">Paying amount</th>
                                    <th scope="col">Balance amount</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($payments)>0)
                                    @foreach($payments as $paymentInfo )
                                        <tr>
                                            <th scope="row">{{ $loop->iteration }}</th>

                                            <td>{{ ($paymentInfo->pi_current_balance) ?  $paymentInfo->pi_current_balance:'--'}}</td>
                                            <td>{{ ($paymentInfo->pi_paying_amount) ?  $paymentInfo->pi_paying_amount:'--'}}</td>

                                            <td>{{ ($paymentInfo->pi_balance_amount) ?  $paymentInfo->pi_balance_amount:'--'}}</td>
                                            <td>{{ ($paymentInfo->pi_date) ? \Carbon\Carbon::parse($paymentInfo->pi_date)->format('d-m-Y'):'--'}}</td>
                                            <td>
                                                <a href="#" data-toggle="modal"
                                                   data-target="#vendorDetailsEdit{{ $paymentInfo->pi_id }}"
                                                   class="text-uppercase pr-3 btn btn-primary btn-sm">
                                                    Edit
                                                </a>


                                                <!-- Modal -->
                                                <div id="vendorDetailsEdit{{ $paymentInfo->pi_id }}" class="modal fade"
                                                     role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">
                                                                    Update Payment Info
                                                                </h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">
                                                                    &times;
                                                                </button>
                                                            </div>
                                                            <form method="POST"
                                                                  action="{{ route('paymentinfo',['id'=>$userInfo->id]) }}">
                                                                <div class="modal-body">
                                                                    <input name="pi_id" type="hidden"
                                                                           value="{{ $paymentInfo->pi_id }}">
                                                                    <input name="pi_vendor_id" type="hidden"
                                                                           value="{{ $paymentInfo->pi_vendor_id }}">
                                                                    <div class="modal-body">
                                                                        {{ csrf_field() }}
                                                                        <div class="form-group">
                                                                            <label>Current balance</label>
                                                                            <input id="pi_current_balance" type="text"
                                                                                   placeholder="Current balance"
                                                                                   class="form-control"
                                                                                   name="pi_current_balance"
                                                                                   value="{{ !empty(old('pi_current_balance')) ? old('pi_current_balance') : ((($paymentInfo) && ($paymentInfo->pi_current_balance)) ? $paymentInfo->pi_current_balance : '') }}">
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label>Paying Amount</label>
                                                                            <input id="pi_paying_amount" type="text"
                                                                                   placeholder="Paying Amount"
                                                                                   class="form-control"
                                                                                   name="pi_paying_amount"
                                                                                   value="{{ !empty(old('pi_paying_amount')) ? old('pi_paying_amount') : ((($paymentInfo) && ($paymentInfo->pi_paying_amount)) ? $paymentInfo->pi_paying_amount : '') }}">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>Balance Amount</label>
                                                                            <input id="pi_balance_amount" type="text"
                                                                                   placeholder="Balance Amount"
                                                                                   class="form-control"
                                                                                   name="pi_balance_amount"
                                                                                   value="{{ !empty(old('pi_balance_amount')) ? old('pi_balance_amount') : ((($paymentInfo) && ($paymentInfo->pi_balance_amount)) ? $paymentInfo->pi_balance_amount : '') }}">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>Date</label>
                                                                            <input id="pi_date_ID" type="text"
                                                                                   placeholder="Date"
                                                                                   class="form-control"
                                                                                   name="pi_date"
                                                                                   value="{{ !empty(old('pi_date')) ? old('pi_date') : ((($paymentInfo) && ($paymentInfo->pi_date)) ? \Carbon\Carbon::parse($paymentInfo->pi_date)->format('d-m-Y') : '') }}">
                                                                        </div>


                                                                    </div>

                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary"
                                                                            data-dismiss="modal">Close
                                                                    </button>
                                                                    <button type="submit" name="submit"
                                                                            class="btn btn-primary">Update
                                                                        Details
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>  <!-- Modal -->

                                                <form method="POST" id="paymentsremove"
                                                      action="{{ route('paymentinfo',['id'=>$userInfo->id]) }}"
                                                      accept-charset="UTF-8" class="form-horizontal"
                                                      style="display:inline">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="pi_id"
                                                           value="{{ $paymentInfo->pi_id }}"/>
                                                    <button type="submit" class="dropdown-item"
                                                            title="Delete Payments"
                                                            onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                        <i
                                                                class="fa fa-trash"></i> Delete
                                                    </button>

                                                </form>

                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div><!-- .animated -->
    </div><!-- .content -->


    <!-- Modal -->
    <div id="vendorDetailsAdd" class="modal fade"
         role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">
                        Add Payment Info
                    </h4>
                    <button type="button" class="close"
                            data-dismiss="modal">
                        &times;
                    </button>
                </div>
                <form method="POST" autocomplete="off"
                      action="{{ route('paymentinfo',['id'=>$userInfo->id]) }}">
                    <div class="modal-body">
                        <input name="pi_id" type="hidden">
                        <input name="pi_vendor_id" type="hidden" value="{{ $userInfo->id }}">
                        <div class="modal-body">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Current balance</label>
                                <input id="pi_current_balance" type="text"
                                       placeholder="Current balance"
                                       class="form-control"
                                       name="pi_current_balance"
                                >
                            </div>

                            <div class="form-group">
                                <label>Paying Amount</label>
                                <input id="pi_paying_amount" type="text"
                                       placeholder="Paying Amount"
                                       class="form-control"
                                       name="pi_paying_amount"
                                >
                            </div>
                            <div class="form-group">
                                <label>Balance Amount</label>
                                <input id="pi_balance_amount" type="text"
                                       placeholder="Balance Amount"
                                       class="form-control"
                                       name="pi_balance_amount"
                                >
                            </div>
                            <div class="form-group">
                                <label>Date</label>
                                <input id="pi_date" type="text"
                                       placeholder="Date"
                                       class="form-control datepicker"
                                       name="pi_date"
                                >
                            </div>


                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"
                                data-dismiss="modal">Close
                        </button>
                        <button type="submit" name="submit" class="btn btn-primary">Add
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>  <!-- Modal -->


@endsection


@section('footerScripts')
    <script>
        $(function () {
            console.log("tet");
            $('#pi_date').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                yearRange: "-100:+0"
            });
        });

    </script>

@endsection