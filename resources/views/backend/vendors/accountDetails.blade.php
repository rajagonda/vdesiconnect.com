@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            display: none;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #3c8dbc;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              'vendorslist'=>'Vendors',
              ''=>'Vendor Info'
              ),'Vendor Info'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-4">
                    @include('backend.vendors.userinfo')
                </div>
                <div class="col-lg-8">

                    @include('backend.vendors.nav')


                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Vendor Account Details</strong>

                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Display Name</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Status</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($accountDetails)
                                    <tr>
                                        <th scope="row">{{ ($accountDetails->va_id) ?  $accountDetails->va_id:'--'}}</th>
                                        <td>{{ ($accountDetails->va_display_name) ?  $accountDetails->va_display_name:'--'}}</td>

                                        <td>{{ ($accountDetails->va_name) ?  $accountDetails->va_name:'--'}}</td>
                                        <td>{{ ($accountDetails->va_email) ?  $accountDetails->va_email:'--'}}</td>

                                        <td>{{ ($accountDetails->va_status) ?  $accountDetails->va_status:'--'}}</td>
                                        <td>
                                            <a href="#" data-toggle="modal"
                                               data-target="#vendorDetailsEdit_{{ $accountDetails->va_id }}"
                                               class="text-uppercase pr-3 btn btn-primary btn-sm">
                                                Edit
                                            </a>

                                            <a href="#" data-toggle="modal"
                                               data-target="#vendorDetails_{{ $accountDetails->va_id }}"
                                               class="text-uppercase pr-3 btn btn-default btn-sm">
                                                View
                                            </a>

                                            <!-- Modal -->
                                            <div id="vendorDetailsEdit_{{ $accountDetails->va_id }}" class="modal fade"
                                                 role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">
                                                                Update Vendor account details
                                                            </h4>
                                                            <button type="button" class="close" data-dismiss="modal">
                                                                &times;
                                                            </button>
                                                        </div>
                                                        <form method="POST"
                                                              action="{{ route('admin_vendor_account_details',['id'=>$accountDetails->va_vendor_id]) }}">
                                                            <div class="modal-body">

                                                                <div class="modal-body">
                                                                    {{ csrf_field() }}
                                                                    <div class="form-group">
                                                                        <label>Display Name</label>
                                                                        <input id="va_display_name" type="text"
                                                                               placeholder="Display Name"
                                                                               class="form-control"
                                                                               name="va_display_name"
                                                                               value="{{ !empty(old('va_display_name')) ? old('va_display_name') : ((($accountDetails) && ($accountDetails->va_display_name)) ? $accountDetails->va_display_name : '') }}">
                                                                    </div>


                                                                    <div class="form-group">
                                                                        <label>Name</label>
                                                                        <input id="va_name" type="text"
                                                                               placeholder="Name"
                                                                               class="form-control"
                                                                               name="va_name"
                                                                               value="{{ !empty(old('va_name')) ? old('va_name') : ((($accountDetails) && ($accountDetails->va_name)) ? $accountDetails->va_name : '') }}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Mobile</label>
                                                                        <input id="va_mobile" type="text"
                                                                               placeholder="Mobile"
                                                                               class="form-control"
                                                                               name="va_mobile"
                                                                               value="{{ !empty(old('va_mobile')) ? old('va_mobile') : ((($accountDetails) && ($accountDetails->va_mobile)) ? $accountDetails->va_mobile : '') }}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Email</label>
                                                                        <input id="va_email" type="text"
                                                                               placeholder="Email"
                                                                               class="form-control"
                                                                               name="va_email"
                                                                               value="{{ !empty(old('va_email')) ? old('va_email') : ((($accountDetails) && ($accountDetails->va_email)) ? $accountDetails->va_email : '') }}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>From</label>
                                                                        <select class="form-control"
                                                                                name="va_time_from">
                                                                            <option value="09:00 hrs" {{ (!empty(old('va_time_from')) && old('va_time_from')=='09:00 hrs')  ? 'selected' : (((@$accountDetails) && (@$accountDetails->va_time_from == '09:00 hrs')) ? 'selected' : '') }}>
                                                                                09:00 hrs
                                                                            </option>
                                                                            <option value="10:00 hrs" {{ (!empty(old('va_time_from')) && old('va_time_from')=='10:00 hrs')  ? 'selected' : (((@$accountDetails) && (@$accountDetails->va_time_from == '10:00 hrs')) ? 'selected' : '') }}>
                                                                                10:00 hrs
                                                                            </option>
                                                                            <option value="11:00 hrs" {{ (!empty(old('va_time_from')) && old('va_time_from')=='11:00 hrs')  ? 'selected' : (((@$accountDetails) && (@$accountDetails->va_time_from == '11:00 hrs')) ? 'selected' : '') }}>
                                                                                11:00 hrs
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>To</label>
                                                                        <select class="form-control" name="va_time_to">
                                                                            <option value="09:00 hrs" {{ (!empty(old('va_time_to')) && old('va_time_to')=='09:00 hrs')  ? 'selected' : (((@$accountDetails) && (@$accountDetails->va_time_to == '09:00 hrs')) ? 'selected' : '') }}>
                                                                                09:00 hrs
                                                                            </option>
                                                                            <option value="10:00 hrs" {{ (!empty(old('va_time_to')) && old('va_time_to')=='10:00 hrs')  ? 'selected' : (((@$accountDetails) && (@$accountDetails->va_time_to == '10:00 hrs')) ? 'selected' : '') }}>
                                                                                10:00 hrs
                                                                            </option>
                                                                            <option value="11:00 hrs" {{ (!empty(old('va_time_to')) && old('va_time_to')=='11:00 hrs')  ? 'selected' : (((@$accountDetails) && (@$accountDetails->va_time_to == '11:00 hrs')) ? 'selected' : '') }}>
                                                                                11:00 hrs
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Description</label>
                                                                        <textarea id="va_business_desc" type="text"
                                                                                  placeholder="Description"
                                                                                  class="form-control"
                                                                                  name="va_business_desc"
                                                                        >{{ !empty(old('va_business_desc')) ? old('va_business_desc') : ((($accountDetails) && ($accountDetails->va_business_desc)) ? $accountDetails->va_business_desc : '') }}</textarea>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Address Line 01</label>
                                                                        <input id="va_address_1" type="text"
                                                                               placeholder="Address"
                                                                               class="form-control"
                                                                               name="va_address_1"
                                                                               value="{{ !empty(old('va_address_1')) ? old('va_address_1') : ((($accountDetails) && ($accountDetails->va_address_1)) ? $accountDetails->va_address_1 : '') }}"
                                                                        >
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Address Line 02</label>
                                                                        <input id="va_address_2" type="text"
                                                                               placeholder="Address"
                                                                               class="form-control"
                                                                               name="va_address_2"
                                                                               value="{{ !empty(old('va_address_2')) ? old('va_address_2') : ((($accountDetails) && ($accountDetails->va_address_2)) ? $accountDetails->va_address_2 : '') }}"
                                                                        >
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>State</label>
                                                                        <select class="form-control" name="va_state">
                                                                            <option value="Telangana" {{ (!empty(old('va_state')) && old('va_state')=='Telangana')  ? 'selected' : (((@$accountDetails) && (@$accountDetails->va_state == 'Telangana')) ? 'selected' : '') }}>
                                                                                Telangana
                                                                            </option>
                                                                            <option value="Andhra Pradesh" {{ (!empty(old('va_state')) && old('va_state')=='Andhra Pradesh')  ? 'selected' : (((@$accountDetails) && (@$accountDetails->va_state == 'Andhra Pradesh')) ? 'selected' : '') }}>
                                                                                Andhra Pradesh
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>City</label>
                                                                        <select class="form-control" name="va_city">
                                                                            <option value="Hyderabad" {{ (!empty(old('va_city')) && old('va_city')=='Hyderabad')  ? 'selected' : (((@$accountDetails) && (@$accountDetails->va_city == 'Hyderabad')) ? 'selected' : '') }}>
                                                                                Hyderabad
                                                                            </option>
                                                                            <option value="Secunderabad" {{ (!empty(old('va_city')) && old('va_city')=='Secunderabad')  ? 'selected' : (((@$accountDetails) && (@$accountDetails->va_city == 'Secunderabad')) ? 'selected' : '') }}>
                                                                                Secunderabad
                                                                            </option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Pincode</label>
                                                                    <input id="va_pincode" type="text"
                                                                           placeholder="Pincode"
                                                                           class="form-control"
                                                                           name="va_pincode"
                                                                           value="{{ !empty(old('va_pincode')) ? old('va_pincode') : ((($accountDetails) && ($accountDetails->va_pincode)) ? $accountDetails->va_pincode : '') }}">
                                                                </div>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                                <button type="submit" class="btn btn-primary">Update
                                                                    Details
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>  <!-- Modal -->
                                            <div id="vendorDetails_{{ $accountDetails->va_id }}" class="modal fade"
                                                 role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">
                                                            </h4>
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">
                                                                &times;
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <table class="table table-striped">

                                                                    <tbody>

                                                                    <tr>
                                                                        <td>Name
                                                                            : {{($accountDetails->va_display_name) ? $accountDetails->va_display_name:'--'}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Business Description
                                                                            : {{($accountDetails->va_business_desc) ? $accountDetails->va_business_desc:'--'}}</td>
                                                                    </tr>
                                                                    <tr>

                                                                        <td>Address 1
                                                                            : {{($accountDetails->va_address_1) ? $accountDetails->va_address_1:'--'}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Address 2
                                                                            : {{($accountDetails->va_address_2) ? $accountDetails->va_address_2:'--'}}</td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td>State
                                                                            : {{($accountDetails->va_state) ? $accountDetails->va_state:'--'}}</td>

                                                                    </tr>
                                                                    <tr>

                                                                        <td>City
                                                                            : {{($accountDetails->va_city) ? $accountDetails->va_city:'--'}}</td>
                                                                    </tr>
                                                                    <tr>

                                                                        <td>Pincode
                                                                            : {{($accountDetails->va_pincode) ? $accountDetails->va_pincode:'--'}}</td>
                                                                    </tr>


                                                                    <tr>

                                                                        <td>Name
                                                                            : {{($accountDetails->va_name) ? $accountDetails->va_name:'--'}}</td>
                                                                    </tr>

                                                                    <tr>

                                                                        <td>Mobile
                                                                            : {{($accountDetails->va_mobile) ? $accountDetails->va_mobile:'--'}}</td>
                                                                    </tr>

                                                                    <tr>

                                                                        <td>Email
                                                                            : {{($accountDetails->va_email) ? $accountDetails->va_email:'--'}}</td>
                                                                    </tr>

                                                                    <tr>

                                                                        <td>Preferred Time to Call
                                                                            : {{($accountDetails->va_time_from) ? $accountDetails->va_time_from:'--'}}
                                                                            to {{($accountDetails->va_time_to) ? $accountDetails->va_time_to:'--'}}</td>
                                                                    </tr>


                                                                    <tr>

                                                                        <td>Status
                                                                            : {{ $accountDetails->va_status }}</td>
                                                                    </tr>
                                                                </table>
                                                            </div>

                                                            @if($accountDetails->va_status!='approved')



                                                                <form method="POST" id="approvedbutton"
                                                                      action="{{ route('AccountDetailsApprove') }}"
                                                                      accept-charset="UTF-8" class="form-horizontal"
                                                                      style="display:inline">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="va_id"
                                                                           value="{{$accountDetails->va_id}}"/>
                                                                    <input type="hidden" name="va_type_new"
                                                                           value="approved"/>
                                                                    <input type="hidden" name="va_vendor_id"
                                                                           value="{{$accountDetails->va_vendor_id}}"/>
                                                                    <button type="submit" class="btn btn-success"
                                                                            title="Confirm Approve"
                                                                            onclick="return confirm('Confirm Approve')">
                                                                        Approve
                                                                    </button>

                                                                </form>


                                                            @else

                                                                <a class="btn btn-success"
                                                                   href="javascript:void(0)">Approved
                                                                    Successfully</a>

                                                                <form method="POST" id="approvedbutton"
                                                                      action="{{ route('AccountDetailsApprove') }}"
                                                                      accept-charset="UTF-8" class="form-horizontal"
                                                                      style="display:inline">
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name="va_id"
                                                                           value="{{$accountDetails->va_id}}"/>
                                                                    <input type="hidden" name="va_type_new"
                                                                           value="rejected"/>
                                                                    <input type="hidden" name="va_vendor_id"
                                                                           value="{{$accountDetails->va_vendor_id}}"/>
                                                                    <button type="submit" class="btn btn-success"
                                                                            title="Confirm Reject"
                                                                            onclick="return confirm('Confirm Reject')">
                                                                        Reject
                                                                    </button>

                                                                </form>


                                                            @endif


                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="6">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footer_scripts')

@endsection