<div class="px-3">

    <table class="table table-hover table-bordered">
        <tr>
            <td>Name</td>
            <td>{{ $userInfo->name }}</td>
        </tr>
        <tr>
            <td>email</td>
            <td>{{ $userInfo->email }}</td>
        </tr>
        <tr>
            <td>Mobile</td>
            <td>{{ $userInfo->mobile }}</td>
        </tr>
        <tr>
            <td>status</td>
            <td>{{ $userInfo->status }}</td>
        </tr>
    </table>


    <a href="#" data-toggle="modal"
       data-target="#updateUserdata"
       class="text-uppercase pr-3 btn btn-primary">
        Update Vendor Info
    </a>

</div>
<!-- Modal -->
<div id="updateUserdata" class="modal fade"
     role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">
                    Update Vendor data
                </h4>
                <button type="button" class="close"
                        data-dismiss="modal">
                    &times;
                </button>
            </div>
            <form method="POST"
                  action="{{ route('vendorslist') }}">
                <div class="modal-body">
                    <input name="id" type="hidden" value="{{ $userInfo->id }}">
                    <input name="URL" type="hidden" value="{{ \Illuminate\Support\Facades\URL::current() }}">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Name</label>
                            <input id="name" type="text"
                                   placeholder="Name"
                                   class="form-control"
                                   name="name"
                                   value="{{ !empty(old('name')) ? old('name') : ((($userInfo) && ($userInfo->name)) ? $userInfo->name : '') }}"
                            >
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input id="name" type="text"
                                   placeholder="email"
                                   class="form-control" readonly
                                   name="email"
                                   value="{{ !empty(old('email')) ? old('email') : ((($userInfo) && ($userInfo->email)) ? $userInfo->email : '') }}"
                            >
                        </div>
                        <div class="form-group">
                            <label>Mobile</label>
                            <input id="mobile" type="text"
                                   placeholder="mobile"
                                   class="form-control"
                                   name="mobile"
                                   value="{{ !empty(old('mobile')) ? old('mobile') : ((($userInfo) && ($userInfo->mobile)) ? $userInfo->mobile : '') }}"
                            >
                        </div>


                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control" name="status">
                                <option value="active" {{ ($userInfo->status == 'active')?'selected':'' }}>
                                    Active
                                </option>
                                <option {{ ($userInfo->status == 'inactive')?'selected':'' }}  value="inactive">
                                    IN Active
                                </option>
                            </select>
                        </div>


                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                            data-dismiss="modal">Close
                    </button>
                    <button type="submit" name="submit" class="btn btn-primary">Update now
                    </button>
                </div>
            </form>
        </div>

    </div>
</div>  <!-- Modal -->
