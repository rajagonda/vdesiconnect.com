<div class="btn-group" role="group" aria-label="Basic example">
    <a href="{{ route('admin_vendor_account_details',['id'=>$userInfo->id]) }}" class="btn btn-secondary @if ($sub_active_menu=='vendor-account-details') active @endif">
        Account Details
    </a>
    <a href="{{ route('BusinessDetailsInfo',['id'=>$userInfo->id]) }}" class="btn btn-secondary @if ($sub_active_menu=='vendor-business-details') active @endif">
        Business Details
    </a>
    <a href="{{ route('VendorProducts',['id'=>$userInfo->id]) }}" class="btn btn-secondary @if ($sub_active_menu=='venodr-products-list') active @endif">
        Products
    </a>
    <a href="{{ route('vendorAdminOrders',['id'=>$userInfo->id]) }}" class="btn btn-secondary @if ($sub_active_menu=='vendor-admin-orders') active @endif">
        Orders
    </a>
    <a href="{{ route('paymentinfo',['id'=>$userInfo->id]) }}" class="btn btn-secondary @if ($sub_active_menu=='vendor-payment-info') active @endif">
        Payment Info
    </a>
</div>