@extends('backend.layout')
@section('title', $title)

@section('headerStyles')



@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Vendors'
              ),'Vendors'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Vendor</strong>

                        </div>
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Email Status</th>
                                    <th scope="col">Business Status </th>
                                    <th scope="col">Acount Status </th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($vendors)>0)
                                    @foreach($vendors as $item)
                                        <tr>
                                            <th scope="row">{{ $loop->iteration }}</th>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>
                                                @if(!empty($item->email_verified_at))
                                                   Email Verified
                                                @else

                                                    <form style="text-align:right; margin-bottom:20px;" method="post" id=""
                                                          action="{{ route('VendorVerify') }}"
                                                          class=""
                                                          autocomplete="off"
                                                          enctype="multipart/form-data">
                                                        {{ csrf_field() }}

                                                        <input type="hidden" value="{{$item->id}}" name="id">


                                                        <input type="submit"
                                                               class="btn btn-primary"
                                                               value="Verify"/>

                                                    </form>

                                                @endif

                                            </td>
                                            <td>


                                                {{ (vendorValidation($item->id))?'Verified':'pending' }}

                                            </td>
                                            <td>


                                                {{ $item->status }}

                                            </td>
                                            <td>
                                                <div class="">
                                                    {{--<a class="btn btn-outline-primary dropdown-toggle" href="#"--}}
                                                       {{--role="button"--}}
                                                       {{--data-toggle="dropdown">--}}
                                                        {{--<i class="fa fa-ellipsis-h"></i>--}}
                                                    {{--</a>--}}
                                                    {{--<div class="dropdown-menu dropdown-menu-right">--}}
                                                        <a class="dropdown-item"
                                                           href="{{ route('admin_vendor_account_details',['id'=>$item->id]) }}"><i
                                                                    class="fa fa-eye"></i> View</a>

                                                    <form method="POST" id="storeDelete"
                                                          action="{{ route('storeDelete') }}"
                                                          accept-charset="UTF-8" class="form-horizontal"
                                                          style="display:inline">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id"
                                                               value="{{ $item->id }}"/>
                                                        <button type="submit" class="dropdown-item"
                                                                title="Delete Vendor"
                                                                onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                            <i
                                                                    class="fa fa-trash"></i> Delete
                                                        </button>

                                                    </form>
                                                       {{--<a class="dropdown-item"--}}
                                                           {{--href="{{ route('userOrders',['id'=>$item->id]) }}"><i--}}
                                                                    {{--class="fa fa-eye"></i> Orders </a>--}}

                                                    {{--</div>--}}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row px-3">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $vendors->firstItem() }}
                        to {{ $vendors->lastItem() }} of {{ $vendors->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $vendors->appends(['filters' => Request::get('filters'),'search' => Request::get('search'),'status' => Request::get('status')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footer_scripts')

@endsection