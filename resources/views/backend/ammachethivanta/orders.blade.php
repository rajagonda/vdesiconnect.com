@extends('backend.layout')
@section('title', $title)

@section('headerStyles')



@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              ''=>'Amma Chethi Vanta orders'
              ),'Amma Chethi Vanta orders'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-lg-12">
                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Order NO</th>
                                    <th scope="col">User</th>
                                    <th scope="col">Address</th>
                                    <th scope="col">Total Weight</th>
                                    <th scope="col">Total Price</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Payment Status</th>
                                    <th scope="col">Order Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($records)>0)
                                    @foreach($records as $item)
                                        @php
                                            $address=unserialize($item->ac_order_delivery_address);
                                            $orderCrrency = $item->ac_order_curency;
                                        @endphp
                                        <tr>
                                            <th scope="row">#{{ $item->ac_order_id }}</th>
                                            <td>{{ $item->getUser->name }}</td>
                                            <td>{{ $address['ua_name'] }},{{ $address['ua_address'] }}
                                                ,{{ $address['ua_city'] }},{{ $address['ua_state'] }}
                                                ,{{ $address['ua_country'] }}</td>
                                            <td>{{ $item->ac_order_total_weight }} Kgs</td>
                                            <td>
                                                {!! currencySymbol($orderCrrency)  !!}
                                                {{ $item->ac_order_total_price }}
                                            </td>
                                            <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i:s')}}</td>
                                            <td>

                                                {!! PayapPaymentStatusShow($item->ac_order_payment_status) !!}
                                            </td>
                                            <td>
                                                {{ wordSort($item->ac_order_status) }}

                                            </td>
                                            <td>
                                                <a class="dropdown-item" href="{{ route('ammachethivantaOrdersDetails',['id'=>$item->ac_order_id]) }}">
                                                    <i class="fa fa-eye"></i> View
                                                </a>
                                                <form method="POST" id="aorders"
                                                      action="{{ route('ammachethivantaorders') }}"
                                                      accept-charset="UTF-8" class="form-horizontal"
                                                      style="display:inline">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="ac_order_id"
                                                           value="{{ $item->ac_order_id }}"/>
                                                    <button type="submit" class="dropdown-item"
                                                            title="Delete Order"
                                                            onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                        <i
                                                                class="fa fa-trash"></i> Delete
                                                    </button>

                                                </form>
                                            </td>

                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5">No records found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row px-3">
                <div class="col-md-10">
                    <div class="dataTables_info p-t-10" id="editable-datatable_info" role="status"
                         aria-live="polite">Showing {{ $records->firstItem() }}
                        to {{ $records->lastItem() }} of {{ $records->total() }} entries
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="editable-datatable_paginate">
                        {!! $records->appends(['filters' => Request::get('filters'),'search' => Request::get('search')])->render() !!}
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

@endsection