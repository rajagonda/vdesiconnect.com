@extends('backend.layout')
@section('title', $title)

@section('headerStyles')

@endsection

@section('content')
    {!! getBreadcrumbs(
               array(
               'dashboard'=>'Home',
               'ammachethivanta'=>'Amma Chethi Vanta',
               ''=>'Add New'
               ),'Add New Record'
            ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card">

                        <div class="card-body card-block ">

                            <form method="POST" id="records" action="{{ route('addNewRecords') }}"
                                  accept-charset="UTF-8" class="form-horizontal"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden"  name="ac_id" value="{{ $ac_id }}">

                                <div class="row">
                                    <div class="col-md-6">

                                        <div class=" form-group">

                                            <label for="hf-email" class=" form-control-label">Name</label>
                                            <div class="input-group">

                                                <input class="form-control" id="ac_name" type="text"
                                                       placeholder="Name"
                                                       name="ac_name"
                                                       value="{{ !empty(old('ac_name')) ? old('ac_name') : ((($record) && ($record->ac_name)) ? $record->ac_name : '') }}">
                                                @if ($errors->has('ac_name'))
                                                    <span class="text-danger help-block">{{ $errors->first('ac_name') }}</span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class=" form-group ">

                                            <label for="hf-email" class="form-control-label">Image</label>
                                            <div class="input-group">

                                                <input class="form-control {{ (($record) && ($record->ac_image)) ? '' : 'image'}}"
                                                       id="image" type="file" name="ac_image"/>
                                                <small>Size: 1400/500</small>
                                                @if ($errors->has('ac_image'))
                                                    <span class="text-danger">{{ $errors->first('ac_image') }}</span>
                                                @endif

                                                @if(($record) && ($record->ac_image))
                                                    <div class="imagebox imagediv{{ $record->ac_id }}">
                                                        <img width="150"
                                                             src="/uploads/ammachethivanta/{{$record->ac_image}}"/>
                                                        <a href="#" id="{{$record->ac_id}}"
                                                           class="delete_image btn btn-danger btn-xs"
                                                           onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                                    class="fa fa-trash-o" aria-hidden="true"></i>Delete
                                                        </a>
                                                    </div>
                                                @endif

                                            </div>
                                        </div>

                                        <div class=" form-group">

                                            <label for="hf-email" class=" form-control-label">Description</label>
                                            <div class="input-group">
                                        <textarea name="ac_description" id="ac_description" rows="9"
                                                  placeholder="Description..."
                                                  class="form-control ">{{ !empty(old('ac_description')) ? old('ac_description') : ((($record) && ($record->ac_description)) ? $record->ac_description : '') }}</textarea>
                                                @if ($errors->has('ac_description'))
                                                    <span class="text-danger help-block">{{ $errors->first('ac_description') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class=" form-group">

                                            <label for="hf-email" class=" form-control-label">Status</label>
                                            <div class="input-group">
                                                <select name="banner_status" id="banner_status" class="form-control">
                                                    <?php
                                                    $status = allStatuses('general'); ?>
                                                    @foreach($status as $ks=>$s)
                                                        <option value="{{ $ks }}" {{ (!empty(old('ac_status')) && old('ac_status')==$ks)  ? 'selected' : ((($record) && ($record->ac_status == $ks)) ? 'selected' : '') }}
                                                        >
                                                            {{ $s }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('ac_status'))
                                                    <span class="text-danger help-block">{{ $errors->first('ac_status') }}</span>
                                                @endif
                                            </div>
                                        </div>


                                    </div>

                                </div>


                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
@section('footerScripts')

    @include('backend.ammachethivanta.script')
    <script type="text/javascript">



    </script>

@endsection