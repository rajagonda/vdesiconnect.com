@extends('backend.layout')
@section('title', $title)

@section('headerStyles')


    <style type="text/css">


        ol, ul {
            list-style: none;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
        }

        caption, th, td {
            text-align: left;
            font-weight: normal;
            vertical-align: middle;
        }

        q, blockquote {
            quotes: none;
        }

        q:before, q:after, blockquote:before, blockquote:after {
            content: "";
            content: none;
        }

        a img {
            border: none;
        }

        article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary {
            display: block;
        }

        body {
            font-family: 'Poppins', sans-serif;
            font-weight: 300;
            font-size: 13px;
            margin: 0;
            padding: 0;
        }

        body a {
            text-decoration: none;
            color: inherit;
        }

        body a:hover {
            color: inherit;
            opacity: 0.7;
        }

        body .container {
            /*width: 900px;*/
            margin: 0 auto;
            padding: 0 20px;
        }

        body .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        body .left {
            float: left;
        }

        body .right {
            float: right;
        }

        body .helper {
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }

        body .no-break {
            page-break-inside: avoid;
        }

        header {
            margin-top: 20px;
            margin-bottom: 50px;
        }

        header figure {
            float: left;
            width: 60px;
            height: 60px;
            margin-right: 15px;
            text-align: center;
        }

        header .company-address {
            float: left;
            max-width: 150px;
            line-height: 1.7em;
        }

        header .company-address .title {
            color: #8BC34A;
            font-weight: 400;
            font-size: 1.5em;
            text-transform: uppercase;
        }

        header .company-contact {
            float: right;
            height: 60px;
            padding: 0 10px;
            background-color: #8BC34A;
            color: white;
        }

        header .company-contact span {
            display: inline-block;
            vertical-align: middle;
        }

        header .company-contact .circle {
            width: 20px;
            height: 20px;
            background-color: white;
            border-radius: 50%;
            text-align: center;
        }

        header .company-contact .circle img {
            vertical-align: middle;
        }

        header .company-contact .phone {
            height: 100%;
            margin-right: 20px;
        }

        header .company-contact .email {
            height: 100%;
            min-width: 100px;
            text-align: right;
        }

        .date {
            line-height: 20px;
        }

        section .details {
            margin-bottom: 55px;
        }

        section .details .client {
            width: 50%;
            line-height: 20px;
        }

        section .details .client .name {
            color: #8BC34A;
        }

        section .details .data {
            width: 50%;
            text-align: right;
        }

        section .details .title {
            margin-bottom: 15px;
            color: #8BC34A;
            font-size: 3em;
            font-weight: 400;
            text-transform: uppercase;
        }

        section table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;

        }

        section table .qty, section table .unit, section table .total {
            width: 15%;
        }

        section table .desc {
            width: 55%;
        }

        section table thead {
            display: table-header-group;
            vertical-align: middle;
            border-color: inherit;
        }

        section table thead th {
            padding: 5px 10px;
            background: #8BC34A;
            border-bottom: 5px solid #FFFFFF;
            border-right: 4px solid #FFFFFF;
            text-align: right;
            color: white;
            font-weight: 400;
            text-transform: uppercase;
        }

        section table thead th:last-child {
            border-right: none;
        }

        section table thead .desc {
            text-align: left;
        }

        section table thead .qty {
            text-align: center;
        }

        section table tbody td {
            padding: 10px;
            background: #E8F3DB;
            color: #000;
            text-align: right;
            border-bottom: 5px solid #FFFFFF;
            border-right: 4px solid #E8F3DB;
        }

        section table tbody td:last-child {
            border-right: none;
        }

        section table tbody h3 {
            margin-bottom: 5px;
            color: #8BC34A;
            font-weight: 600;
            font-size: 15px;
        }

        section table tbody .desc {
            text-align: left;
            line-height: 20px;
            font-size: 13px;
        }

        section table tbody .qty {
            text-align: center;
        }

        section table.grand-total {
            margin-bottom: 45px;
        }

        section table.grand-total td {
            padding: 5px 10px;
            border: none;
            color: #777777;
            text-align: right;
            line-height: 25px;
        }

        section table.grand-total .desc {
            background-color: transparent;
        }

        section table.grand-total tr:last-child td {
            font-weight: 600;
            color: #8BC34A;
            font-size: 1.18181818181818em;
        }

        footer {
            margin-bottom: 20px;
        }

        footer .thanks {
            margin-bottom: 40px;
            color: #8BC34A;
            font-size: 1.16666666666667em;
            font-weight: 600;
        }

        footer .notice {
            margin-bottom: 25px;
            line-height: 22px;
        }

        footer .end {
            padding-top: 5px;
            border-top: 2px solid #8BC34A;
            text-align: center;
        }

    </style>

    <link href="//cdn.kendostatic.com/2013.1.319/styles/kendo.common.min.css" rel="stylesheet"/>
    <link href="//cdn.kendostatic.com/2013.1.319/styles/kendo.default.min.css" rel="stylesheet"/>

@endsection

@section('content')

    {!! getBreadcrumbs(
              array(
              'dashboard'=>'Home',
              'ammachethivantaorders'=>'Amma Chethi Vanta Orders',
              ''=>'Order Details'
              ),'Order Details'
           ) !!}

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">


                <div class="col-lg-12">


                    @if (Session::has('flash_message'))
                        <br/>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{{ Session::get('flash_message' ) }}</strong>
                        </div>
                    @endif
                    <div class="card">

                        <div class="card-body table-responsive">


                            <section>
                                <div class="container">


                                    <div class="card">
                                        <div class="card-header">

                                            <?php
//                                            dump($orders);
                                            ?>

                                            <form method="post" id="editServices"
                                                  action="{{ route('ammachethivanta_changeOrderStatus',['id'=>$orders->ac_order_id]) }}"
                                                  class="form-horizontal editServices"
                                                  autocomplete="off"
                                                  enctype="multipart/form-data">
                                                {{ csrf_field() }}


                                                <input type="hidden" name="ac_order_id"
                                                       value="{{$orders->ac_order_id}}"/>

                                                <div class="row">

                                                    <div class="col-4  ">

                                                        <div class="form-group">
                                                            <label for="hf-email"
                                                                   class=" form-control-label">Status Update</label>
                                                            <select class="form-control"
                                                                    id="ac_order_status"
                                                                    type="text"
                                                                    placeholder="Contact Person"
                                                                    value=""
                                                                    name="ac_order_status">
                                                                <option value="">--Select Status</option>
                                                                <option value="placed" {{ isset($orders) && ($orders->ac_order_status == 'placed') ? 'selected' : ''}}>
                                                                    placed
                                                                </option>
                                                                <option value="shipped" {{ isset($orders) && ($orders->ac_order_status == 'shipped') ? 'selected' : ''}}>
                                                                    Shipped
                                                                </option>
                                                                <option value="out-for-delivery" {{ isset($orders) && ($orders->ac_order_status == 'out-for-delivery') ? 'selected' : ''}}>
                                                                    Out for delivery
                                                                </option>
                                                                <option value="delivered" {{ isset($orders) && ($orders->ac_order_status == 'delivered') ? 'selected' : ''}}>
                                                                    Delivered
                                                                </option>
                                                            </select>

                                                        </div>
                                                    </div>
                                                    <input type="submit"
                                                           class="btn btn-primary"
                                                           value="Update"/>

                                                </div>


                                            </form>

                                            <div class="btn-group" role="group" aria-label="Basic example"
                                                 style="float: right;">
                                                <a href="#"
                                                   class="btn btn-secondary {{ isset($orders) && ($orders->ac_order_status =='placed') ? 'active' : ''}}">
                                                    Paid
                                                </a>
                                                <a href="#"
                                                   class="btn btn-secondary {{ isset($orders) && ($orders->ac_order_status == 'shipped') ? 'active' : ''}}">
                                                    Shipped
                                                </a>
                                                <a href="#"
                                                   class="btn btn-secondary {{ isset($orders) && ($orders->ac_order_status == 'out-for-delivery') ? 'active' : ''}}">
                                                    Out
                                                    for delivery
                                                </a>
                                                <a href="#"
                                                   class="btn btn-secondary {{ isset($orders) && ($orders->ac_order_status == 'delivered') ? 'active' : ''}}">
                                                    Delivered
                                                </a>
                                            </div>

                                        </div>
                                        <div class="card-body table-responsive">


                                            <div class="details clearfix">
                                                <div class="client left">
                                                    <?php
                                                    $userAddress = unserialize($orders->ac_order_delivery_address);
                                                    $orderCrrency = $orders->ac_order_curency;

                                                    ?>

                                                    <p>Shipping TO:</p>
                                                    <p class="name">
                                                        <strong style="font-size:16px;">
                                                            {{--{!!  !!}--}}
                                                            {{$userAddress['ua_name']}},
                                                        </strong>
                                                    </p>
                                                    <p>

                                                        {{$userAddress['ua_address']}},
                                                        {{$userAddress['ua_landmark']}},
                                                        {{$userAddress['ua_city']}},
                                                        {{$userAddress['ua_state']}},
                                                        {{$userAddress['ua_country']}},
                                                        {{$userAddress['ua_pincode']}},
                                                        {{$userAddress['ua_phone']}},

                                                    </p>
                                                    <a href="mailto:{{$userAddress['ua_email']}}">
                                                        {{$userAddress['ua_email']}}
                                                    </a>
                                                </div>
                                                <div class="data right">
                                                    <div class="date">
                                                        <p>INVOICE NO: <b>VDC-CART-{{ $orders->ac_order_id }}</b></p>
                                                        Date of Order: {{$orders->created_at}}<br>
                                                        {{--Due Date: 30/06/2014--}}
                                                    </div>

                                                    <p class="name">
                                                        <strong>Order status</strong> <br/>
                                                        {{ wordSort($orders->ac_order_status) }}
                                                    </p>


                                                    <p class="name">
                                                        <strong> Payment Status</strong> <br/>
                                                        {!! PayapPaymentStatusShow($orders->ac_order_payment_status) !!}
                                                    </p>


                                                </div>
                                            </div>

                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <thead>
                                                <tr>
                                                    <th class="desc">Image</th>
                                                    <th class="desc">Product Name</th>
                                                    <th class="qty">Weight</th>
                                                    <th class="unit">Price</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $subtotal = array();

                                                ?>

                                                @if (count($orders->orderItems)> 0)
                                                    @foreach($orders->orderItems AS $items)

                                                        <tr>
                                                            <td class="desc"> <?php
                                                                if (isset($items->ac_image)) {
                                                                    $item_img = url('/uploads/ammachethivanta/' . $items->ac_image);

                                                                } else {
                                                                    $item_img = '/frontend/images/no-image.png';
                                                                }


                                                                ?>
                                                                <img src="{{$item_img}}" alt="" title=""
                                                                     class="img-fluid"
                                                                     width="60px">
                                                            </td>
                                                            <td class="desc">
                                                                <h6>
                                                                    {{ $items->getProduct->ac_name  }}
                                                                </h6>


                                                                @if (isset($items->ac_order_delivery_date) && $items->ac_order_delivery_date != ''
                                                                || isset($items->ac_order_delivery_time) && $items->ac_order_delivery_time != '')

                                                                    <p class="py-3">
                                                                        <b>
                                                                            User Request Delivery time.

                                                                        </b> <br/>
                                                                        {{ $items->ac_order_delivery_date }}
                                                                        {{ $items->ac_order_delivery_time }}

                                                                        {{--                                                        {{  (isset($orderItems->oitem_delivery_date))?\Carbon\Carbon::parse($orderItems->oitem_delivery_date)->add('3 days')->format('d-m-Y') :'' }}--}}
                                                                    </p>

                                                                @endif


                                                            </td>

                                                            <td class="qty">
                                                                {{ $items->acoitem_weight  }}
                                                            </td>
                                                            <td class="unit">


                                                                {!! ($items->acoitem_product_price) ? $items->acoitem_product_price : '-' !!}

                                                            </td>


                                                        </tr>
                                                    @endforeach
                                                @endif


                                                </tbody>
                                            </table>
                                            <div class="no-break">
                                                <table class="grand-total">
                                                    <tbody>
                                                    <tr>
                                                        <td class="desc"></td>
                                                        <td class="qty"></td>
                                                        <td class="unit">Total Weight:</td>
                                                        <td class="total">

                                                            {{  $orders->ac_order_total_weight  }}Kgs

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="desc"></td>
                                                        <td class="qty"></td>
                                                        <td class="unit">SUBTOTAL:</td>
                                                        <td class="total">
                                                            {!! currencySymbol($orderCrrency)  !!}
                                                            {{  $orders->ac_order_sub_total_price  }}

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="desc"></td>
                                                        <td class="qty"></td>
                                                        <td class="unit">
                                                            TAX: {{$orders->s_tax}}%:
                                                        </td>
                                                        <td class="total">
                                                            0
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="desc"></td>
                                                        <td class="unit" colspan="2">GRAND TOTAL:</td>
                                                        <td class="total">
                                                            {!! currencySymbol($orderCrrency)  !!}
                                                            {{  $orders->ac_order_total_price  }}
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>


                                        </div>
                                    </div>


                                </div>
                            </section>


                        </div>
                    </div>

                </div>

            </div>

        </div><!-- .animated -->
    </div><!-- .content -->



@endsection


@section('footerScripts')

@endsection