<!DOCTYPE html>
<html>

<head>
    <title>Thanks from Vdesiconnect</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        table tr td {
            padding: 5px;
        }

        @media only screen and (max-width: 800px) {
            div {
                width: 100% !important;
            }
        }
    </style>
</head>

<body style="font-family: sans-serif">
<div style="width:600px; margin:0 auto; padding:25px; background:#eaeff2;">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3" align="center">
                <a href="https://www.vdesiconnect.com/"><img src="https://www.vdesiconnect.com/logo.png"></a>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <h1 style="font-weight: normal; margin:0;">Hi <strong style="color:#0098eb">{{$name}}</strong></h1>
                <h3 style="padding:10px 50px; line-height: 30px; margin: 0;">Thank you for Your Submission, We will contact you soon</h3>
            </td>
        </tr>
        <tr>
            <td style="color:#888;">Service</td>
            <td width="10" style="padding:10px;">:</td>
            <td style="padding:10px;">{{$s_options}}</td>
        </tr>
        <tr>
            <td style="color:#888;">Date of Service Required</td>
            <td width="10" style="padding:10px;">:</td>
            <td style="padding:10px;">{{$s_date}}</td>
        </tr>
        <tr>
            <td style="color:#888;">Preferred Time to Contact</td>
            <td width="10" style="padding:10px;">:</td>
            <td style="padding:10px;">{{$s_time}}</td>
        </tr>
        <tr>
            <td style="color:#888;">Service Type</td>
            <td width="10" style="padding:10px;">:</td>
            <td style="padding:10px;">{{wordSort($s_type)}}</td>
        </tr>
        <tr>
            <td style="color:#888;">Phone</td>
            <td width="10" style="padding:10px;">:</td>
            <td style="padding:10px;">{{$s_phone}}</td>
        </tr>



    </table>
</div>
</body>

</html>
