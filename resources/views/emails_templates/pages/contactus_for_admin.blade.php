<!DOCTYPE html>
<html>

<head>
    <title>Thanks from VdesiConnect</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        table tr td {
        }

        @media only screen and (max-width: 800px) {
            div {
                width: 100% !important;
            }
        }
    </style>
</head>

<body style="font-family: sans-serif">
<div style="width:600px; margin:0 auto; background:#f3f3f3;">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center" style="padding:10px;">
                <a href="https://www.vdesiconnect.com/"><img src="https://www.vdesiconnect.com/logo.png"></a>
            </td>
        </tr>
        <tr>
            <td align="center" style="background:#f9f9f9;">
                <table width="75%">
                    <tr>
                        <td align="center">
                            <h2>Submission Information</h2>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table width="100%" align="center">
                                <tr>
                                    <td style="color:#888;">Contact Person</td>
                                    <td width="10" style="padding:10px;">:</td>
                                    <td style="padding:10px;">{{$emailData->first_name}} {{$emailData->last_name}}</td>
                                </tr>
                                <tr>
                                    <td style="color:#888;">Email</td>
                                    <td width="10" style="padding:10px;">:</td>
                                    <td style="padding:10px;">{{$emailData->email}}</td>
                                </tr>
                                <tr>
                                    <td style="color:#888;">Mobile</td>
                                    <td width="10" style="padding:10px;">:</td>
                                    <td style="padding:10px;">{{$emailData->mobile_prefix}}
                                        - {{$emailData->phone_number}}</td>
                                </tr>

                                <tr>
                                    <td style="color:#888;">Subject</td>
                                    <td width="10" style="padding:10px;">:</td>
                                    <td style="padding:10px;">{{$emailData->subject}}</td>
                                </tr>
                                <tr>
                                    <td style="color:#888;">Message</td>
                                    <td width="10" style="padding:10px;">:</td>
                                    <td style="padding:10px;">{{$emailData->message}}</td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <p style="font-size:12px; color:#666;"><strong>Note</strong> This information
                                            sent by user using Contact form in vdesiconnect.com</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>

</html>