<!DOCTYPE html>
<html>

<head>
    <title>Thanks from Vdesiconnect</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        table tr td {
            padding: 5px;
        }

        @media only screen and (max-width: 800px) {
            div {
                width: 100% !important;
            }
        }
    </style>
</head>

<body style="font-family: sans-serif">
<div style="width:600px; margin:0 auto; padding:25px; background:#eaeff2;">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center">
                <a href="https://www.vdesiconnect.com/"><img src="https://www.vdesiconnect.com/logo.png"></a>
            </td>
        </tr>
        <tr>
            <td align="left">
                <h1 style="font-weight: normal; margin:0;">Hi <strong
                            style="color:#0098eb">{{$emaildata->se_name}}</strong></h1>


                <div>Product name: {{ $emaildata->getProduct->p_name }}

                </div>

                <div>Product Image:

                    <?php
                    $image_url = getImagesById($emaildata->getProduct->productImages[0]->pi_id);
                    if ($image_url == '') {
                        $image_url = '/frontend/images/noproduct-available.jpg';
                    }



                    ?>

                    <img src="{{url($image_url)}}" alt="" title="" class="img-fluid"
                         width="150px">

                </div>
                <h3 style="padding:10px 50px; line-height: 30px; margin: 0;">
                    Thank you for Your Submission, We will
                    contact you soon
                </h3>
            </td>
        </tr>


    </table>
</div>
</body>

</html>
