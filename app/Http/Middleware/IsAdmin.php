<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

//        dd(Auth::guard('admin')->user());

        if (Auth::guard('admin')->user() == '') {
            return redirect()->route('adminLogin');
//        if (Auth::guard('admin')->check()) {
        }
//        elseif (Auth::guard('store')->check()) {
//            return redirect()->route('vendorlogin');
//        }
//        elseif (Auth::guard('web')->check()) {
//            return redirect()->route('userlogin');
//        }
//        else {
//            return redirect()->route('adminLogin');
//        }
        return $next($request);

    }
}
