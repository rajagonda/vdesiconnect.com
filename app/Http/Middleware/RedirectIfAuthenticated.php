<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        $routname = request()->segment(1);

        if ($routname == 'admin') {
            $adminuser = Auth::guard('admin')->user();
        } elseif ($routname == 'store') {
            $vendoruser = Auth::guard('store')->user();
        } else {
            $user = Auth::guard('web')->user();
        }

        if (!empty($adminuser)) {
            return redirect()->route('dashboard');
        }
        if (!empty($vendoruser)) {
            return redirect()->route('sellerPreDashbord');
        }
        if (!empty($user)) {
            return redirect()->route('userprofile');
        }


//        if (Auth::check()) {
//            if ($userAuth->role == '404') {
//                return redirect()->route('dashboard');
//            } elseif ($userAuth->role == '405') {
//                return redirect()->route('sellerPreDashbord');
//            } else {
//                return redirect()->route('userprofile');
//            }
//
//        }


        return $next($request);
    }


}
