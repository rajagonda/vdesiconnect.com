<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsUser
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        dd(Auth::check());

//        if (Auth::guard('web')->user() == '') {
//        if (Auth::guard('web')->check()) {
        if (Auth::check() && Auth::user()->role != 1) {
            return redirect()->route('userlogin');
        }
//        elseif (Auth::guard('store')->check()) {
//            return redirect()->route('vendorlogin');
//        }
//        elseif (Auth::check()) {
//            return redirect()->route('adminLogin');
//        }
//        else {
//            return redirect()->route('userlogin');
//        }
        return $next($request);
    }
}
