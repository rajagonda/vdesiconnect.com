<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class IsVendor
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

//        dump(Auth::guard('store')->user());
//        dump(Auth::guard('admin')->user());
//        dump(Auth::guard('web')->user());
//        dd(Auth::user());

        if (Auth::guard('store')->user() == '') {


            return redirect()->route('vendorlogin');
        }


//        echo 'asdas';

//        dd(Auth::guard('store')->user());

//        elseif (Auth::guard('admin')->check()) {
//            return redirect()->route('adminLogin');
//        }
//        elseif (Auth::guard('web')->check()) {
//            return redirect()->route('userlogin');
//        }
//        else {
//            return redirect()->route('vendorlogin');
//        }
        return $next($request);
    }
}
