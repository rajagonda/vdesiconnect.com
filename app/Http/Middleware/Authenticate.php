<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        $routname = request()->segment(1);

//        dd($routname);

        if ($routname == 'admin') {
            return route('adminLogin');
        }elseif ($routname == 'store'){
            return route('vendorlogin');
        } else {
            return route('userlogin');
        }

        return $next($request);
    }
}
