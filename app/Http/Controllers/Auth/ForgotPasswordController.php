<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        $routname = request()->segment(1);
        if ($routname == 'store') {
            $data = array();
            $data['active_menu'] = 'forgot_Password';
            $data['sub_active_menu'] = 'forgot_Password';
            $data['title'] = 'Forgot Password';
            return view('auth.store.passwords.email', $data);
        } else {
            $data = array();
            $data['active_menu'] = 'forgot_Password';
            $data['sub_active_menu'] = 'forgot_Password';
            $data['title'] = 'Forgot Password';
            return view('auth.user.passwords.email', $data);
        }
    }

    protected function validationErrorMessages()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];
    }
}
