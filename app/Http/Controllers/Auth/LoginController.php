<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected function redirectTo()
    {

        $routname = request()->segment(1);

//        dd($routname);

        if ($routname == 'admin') {
            $user = Auth::guard('admin')->user();
        } elseif ($routname == 'store') {
            $user = Auth::guard('store')->user();
        } else {
            $user = Auth::user();
        }


//        dd(session('link'));


        if ($user->role == 404) {
            $url = route('dashboard');
            return $url;
        } elseif ($user->role == 405) {
            $url = route('sellerPreDashbord');
            return $url;
        } else {
            $url = route('userprofile');
            return session('link');
//            return $url;
        }



    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $routname = request()->segment(1);
        if ($routname == 'admin') {
            $this->middleware('guest:admin')->except('logout');
        } elseif ($routname == 'store') {
            $this->middleware('guest:store')->except('logout');
        } else {
            $this->middleware('guest:web')->except('logout');
        }
    }

    protected function guard()
    {
        $routname = request()->segment(1);


        if ($routname == 'admin') {
            return Auth::guard('admin');
        } elseif ($routname == 'store') {
            return Auth::guard('store');
        } else {
            return Auth::guard('web');
        }
    }

    public function showLoginForm($location = null)
    {


        if (session('link')) {

            $myPath     = session('link');
            $loginPath  = url('/login');
            $previous   = url()->previous();

//            dump($myPath);
//            dump($loginPath);
//            dump($previous);


            if ($previous == $loginPath) {
                session(['link' => $myPath]);
            }
            else{
                session(['link' => $previous]);
            }
        }
        else{
            session(['link' => url()->previous()]);
        }


//        dump(session('link'));


        $routname = request()->segment(1);

//        dd($routname);

        if ($routname == 'admin') {
            $data = array();
            $data['active_menu'] = 'login';
            $data['sub_active_menu'] = 'login';
            $data['title'] = 'Login';
            return view('auth.admin.login', $data);
        } elseif ($routname == 'store') {
            $data = array();
            $data['active_menu'] = 'login';
            $data['sub_active_menu'] = 'login';
            $data['title'] = 'Login';
            return view('auth.store.login', $data);
        } else {
            $data = array();
            $data['active_menu'] = 'login';
            $data['sub_active_menu'] = 'login';
            $data['title'] = 'Login';
            return view('auth.user.login', $data);
        }
    }

    protected function credentials(Request $request)
    {


        $routname = request()->segment(1);


        $credentials = $request->only($this->username(), 'password');
        $credentials['status'] = 'active';
        if ($routname == 'admin') {
            $credentials['role'] = 404;
        } elseif ($routname == 'store') {
            $credentials['role'] = 405;
        } else {
            $credentials['role'] = 1;
        }
//        print_r($credentials);
//        exit();
        return $credentials;
    }

    protected function sendFailedLoginResponse(Request $request)
    {

        $routname = request()->segment(1);
        $errors = [$this->username() => trans('auth.failed')];

//        dd($request->{$this->username()});

        // Load user from database

        $roleWhere = '';

        if ($routname == 'admin') {
            $roleWhere = 404;


        } elseif ($routname == 'store') {
            $roleWhere = 405;
        } else {
            $roleWhere = 1;
        }


        $user = User::where($this->username(), strtolower($request->{$this->username()}))->where('role',$roleWhere)->first();
        // Check if user was successfully loaded, that the password matches
        // and active is not 1. If so, override the default error message.




        if (!$user || $errors) {
//            $errors = [$this->username() => 'Invalid credentials'];
        }

        if ($user && Hash::check($request->password, $user->password) && $user->status != 'active') {
            $errors = [$this->username() => 'Your account is not in active.'];
        }




//        dump($roleWhere);
//        dump($routname);
//        dd($user);

        if ($routname == 'admin') {
            if ($user && $user->role != 404) {
                $errors = [$this->username() => 'You do not have admin account access.'];
            }
        } elseif ($routname == 'store') {
            if ($user && $user->role != 405) {
                $errors = [$this->username() => 'You do not have vendor account access.'];
            }
        } else {
            if ($user && $user->role != 1) {
                $errors = [$this->username() => 'You do not have user access.'];
            }
        }


//        dd($errors);

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    public function authenticated(Request $request, $user)
    {
        if (!$user->email_verified_at) {
            $routname = request()->segment(1);
            if ($routname == 'store') {
                $this->guard('store')->logout();
                $route = 'vendorlogin';
            } else {
                $this->guard()->logout();
                $route = 'userlogin';
            }
            return redirect()->route($route)->with('flash_message', "Please verify your account to access account or  <a href='javascript:void(0)' class='btn btn-primary' data-toggle='modal' data-target='#resenactivationlink'>
                       Resend link
                    </a>");
        }
    }


    public function logout(Request $request)
    {
        $routname = request()->segment(1);
        $this->guard()->logout();
        if ($routname == 'admin') {
            $this->guard('admin')->logout();
            $route = 'adminLogin';
        } elseif ($routname == 'store') {
            $this->guard('store')->logout();
            $route = 'vendor_home';
        } else {
            $this->guard('web')->logout();
            $route = 'home';
        }
        return redirect()->route($route);
    }


}
