<?php

namespace App\Http\Controllers\Auth;

use App\Mail\WelcomeMail;
use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\Rule;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected function redirectTo()
    {

        $routname = request()->segment(1);
        if ($routname == 'store') {
            $url = route('vendorRegister');
            return $url;
        } else {
            $url = route('register');
            return $url;
        }


    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    public function showRegistrationForm($location = null)
    {
        $routname = request()->segment(1);

        if ($routname == 'store') {
            $data = array();
            $data['active_menu'] = 'signup';
            $data['sub_active_menu'] = 'signup';
            $data['title'] = 'Sign up';
            return view('auth.store.seller_rigister', $data);

        } else {
            $data = array();
            $data['active_menu'] = 'signup';
            $data['sub_active_menu'] = 'signup';
            $data['title'] = 'Sign up';
            return view('auth.user.signup', $data);

        }


    }

    protected function validator(array $data)
    {

        $uniqueRule = Rule::unique('users')->where(function ($query) use ($data) {
            return $query->where('email', $data['email'])
                ->where('role', $data['role']);
        });


        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => ['required', 'string', 'email', 'max:255', $uniqueRule],
            'password' => 'required|string|min:6|max:20|confirmed',
//            'password' => 'required|string|min:6|max:20|confirmed',

        ], [
            'name.required' => 'The :attribute field is required.',
            'email.required' => 'The :attribute field is required.',
            'email.max' => 'Email contains maximum 255 digits',
            'name.max' => 'Name contains maximum 255 digits',
            'password.min' => 'Password number contains minimum 6 digits',
            'password.max' => 'Password number contains maximum 12 digits',
            'password.required' => 'The :attribute field is required.',
//            'password.regex' => 'Passwords must have at least 6 characters and contains atleast one uppercase letter, lowercase letter, number, and specl symbols ',
            'password.confirmed' => 'Password and confirm password must be same',
            'email.unique' => 'This email already exist',
        ]);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));//        $this->guard()->login($user);

        $sortmobile = explode('-', $user->mobile);


        if ($user->role == 1) {

            $userMobile = $sortmobile[1];


            $variables = array(
                '{USER_NAME}' => $user->name
            );

            $MSG_USER_REGISTER_NOTFICATION_ADMIN = strtr(MSG_USER_REGISTER_NOTFICATION_ADMIN, $variables);
            sendSms(MSG_ADMIN_NUMBER, trim($MSG_USER_REGISTER_NOTFICATION_ADMIN));


            $MSG_USER_REGISTER_NOTFICATION_USER = strtr(MSG_USER_REGISTER_NOTFICATION_USER, $variables);

            sendSms($userMobile, trim($MSG_USER_REGISTER_NOTFICATION_USER), $sortmobile[0]);


        }

        if ($user->role == 405) {

            if (isset($sortmobile)) {


                if (isset($sortmobile[1])) {
                    $userMobile = $sortmobile[1];
                    $variables = array(
                        '{USER_NAME}' => $user->name
                    );

                    $MSG_VENDOR_REGISTER_NOTFICATION_ADMIN = strtr(MSG_VENDOR_REGISTER_NOTFICATION_ADMIN, $variables);
                    sendSms(MSG_ADMIN_NUMBER, trim($MSG_VENDOR_REGISTER_NOTFICATION_ADMIN));



                    $MSG_VENDOR_REGISTER_NOTFICATION_VENDOR = strtr(MSG_VENDOR_REGISTER_NOTFICATION_VENDOR, $variables);
                    sendSms($userMobile, trim($MSG_VENDOR_REGISTER_NOTFICATION_VENDOR));
                }


            }


        }


        return $this->registered($request, $user) ?: redirect($this->redirectPath())->with('flash_message', 'Registartion successfull !! Please check your email to verify your email address');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {


        Session::flash('flash_message', 'Welcome to VDesiConnect !');
        $user = User::create([
            'name' => $data['name'],
            'role' => $data['role'],
            'status' => $data['status'],
            'email' => strtolower($data['email']),
            'mobile' => $data['mobile_prefix'] . '-' . $data['mobile'],
            'password' => Hash::make($data['password']),
        ]);


//       Mail::to($data['email'])->send(new WelcomeMail($user));

//       $user->notify(new UserRegisteredNotification($user));
        return $user;
    }


}
