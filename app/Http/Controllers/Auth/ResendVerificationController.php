<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResendVerificationController extends Controller
{
    public function resend(Request $request)
    {
        $user = User::where('email',$request->email)->where('role',$request->role)->firstOrFail();

//dd($user);


        if(!empty($user)){
            if ($user->email_verified_at) {
                $mes='Your account is already verified please login';
            } else {
                $user->sendEmailVerificationNotification();
                $mes='Please check your email to verify your email address';
            }
        }else{
            $mes='Not registered email';
        }
        return back()->with('flash_message',$mes);


    }
}
