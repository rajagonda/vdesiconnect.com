<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\User;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected function redirectTo()
    {
        $routname = request()->segment(1);

//        dd($routname);

        if ($routname == 'store') {
            $url = route('vendorlogin');
            return $url;
        } else {
            $url = route('userlogin');
            return $url;
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    public function showResetForm(Request $request, $token = null)
    {
        $routname = request()->segment(1);

//        dd($routname);

        if ($routname == 'store') {
            $data = array();
            $data['active_menu'] = 'reset_password';
            $data['sub_active_menu'] = 'reset_password';
            $data['title'] = 'Reset Password';
            return view('auth.store.passwords.reset', $data)->with(
                ['token' => $token, 'email' => $request->email]
            );
        } else {
            $data = array();
            $data['active_menu'] = 'reset_password';
            $data['sub_active_menu'] = 'reset_password';
            $data['title'] = 'Reset Password';
            return view('auth.user.passwords.reset', $data)->with(
                ['token' => $token, 'email' => $request->email]
            );
        }

    }

    protected function resetPassword($user, $password)
    {


        $routname = request()->segment(1);
        $role = '';
        if ($routname == 'admin') {
            $role = 404;
        } elseif ($routname == 'store') {
            $role = 405;
        } else {
            $role = 1;
        }


        $user = User::where('email', $user->email)->where('role', $role)->first();


//        dd($user);

        $user->password = Hash::make($password);

        $user->setRememberToken(Str::random(60));

        $user->save();

        event(new PasswordReset($user));
    }

    protected function sendResetResponse(Request $request, $response)
    {
        return redirect($this->redirectPath())
            ->with('flash_message', 'Your passwotrd successfully changed');
    }
}
