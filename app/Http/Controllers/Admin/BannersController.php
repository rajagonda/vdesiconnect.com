<?php

namespace App\Http\Controllers\Admin;

use App\Models\Banners;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Validator;

class BannersController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $action = Banners::removeBanner($requestData['banner_id']);

            if ($action == true) {
                $msg = "Banner deleted successfully!";
            } else {
                $msg = "Banner delete Error!";

            }
            return redirect()->route('banners')->with('flash_message', $msg);
        } else {

            $banners = Banners::orderBy('banner_id', 'DESC')
                ->paginate(PAGE_LIMIT);
        }
        $data = array();
        $data['active_menu'] = 'banners';
        $data['sub_active_menu'] = 'banners-list';
        $data['title'] = 'Banners';
        $data['banners'] = $banners;
        return view('backend.banners.list', $data);
    }

    public function addNewBanners(Request $request, $id = null)
    {
        $tableInfo = new Banners();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['banner_id'] != '') {
                $banners = Banners::findOrFail($requestData['banner_id']);
                $imageRule = empty($banners->banner_image) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }

            Validator::make($request->all(), [
                'banner_title' => 'required',
                'banner_description' => 'required',
                'banner_tag' => 'required',
                'banner_image' => $imageRule,
                'banner_status' => 'required'
            ], [
                'banner_title.required' => 'Please enter title',
                'banner_description.required' => 'Please enter description',
                'banner_tag.required' => 'Please enter link',
                'banner_image.required' => 'select image to upload',
                'banner_status.required' => 'Select status',
            ]);



            if ($requestData['banner_id'] == '') {
                if ($request->hasFile('banner_image')) {
                    $fileName = Banners::imageUpload($request['banner_image']);
                    $requestData['banner_image'] = $fileName;
                }

                if ($request->hasFile('banner_bg_image')) {
                    $fileName = Banners::imageUpload($request['banner_bg_image']);
                    $requestData['banner_bg_image'] = $fileName;
                }

                Banners::create($requestData);

                $mes = 'Banner added successfully!';
            } else {

                if ($request->hasFile('banner_image')) {
                    $fileName = Banners::imageUpload($request['banner_image'], $banners->banner_image);
                    $requestData['banner_image'] = $fileName;
                }

                if ($request->hasFile('banner_bg_image')) {
                    $fileName = Banners::imageUpload($request['banner_bg_image'], $banners->banner_bg_image);
                    $requestData['banner_bg_image'] = $fileName;
                }

                $banners->update($requestData);
                $mes = 'Banner updated successfully!';
            }
            return redirect()->route('banners')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['banner_id'] = '';
            $data['banner'] = '';
            if ($id) {
                $data['banner_id'] = $id;
                $data['banner'] = Banners::findOrFail($id);
            }
            $data['active_menu'] = 'banners';
            $data['sub_active_menu'] = 'manage-banners';
            $data['title'] = 'Manage banners';
            return view('backend.banners.add', $data);
        }
    }

    public function deleteBannerimage(Request $request)
    {
        $image = Banners::findOrFail($request['image']);

        File::delete('uploads/banners/' . $image->banner_image);

        $update_data = array();
        $update_data['banner_image'] = '';
        $image->update($update_data);
        exit();
    }

    public function deleteBgBannerimage(Request $request)
    {
        $image = Banners::findOrFail($request['image']);

        File::delete('uploads/banners/' . $image->banner_bg_image);

        $update_data = array();
        $update_data['banner_bg_image'] = '';
        $image->update($update_data);
        exit();
    }
}
