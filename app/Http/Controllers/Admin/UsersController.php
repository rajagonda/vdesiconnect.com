<?php

namespace App\Http\Controllers\Admin;

use App\Models\OrderItems;
use App\Models\Orders;
use App\Models\Ratings;
use App\Models\UserAddress;
use App\Models\Wishlist;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 100);
    }

    public function index(Request $request)
    {


        if ($request->isMethod('post')) {
            $requestData = $request->all();

            User::destroy($requestData['id']);

            $orderid = Orders::where('order_user_id', $requestData['id'])->first();
            if ($orderid) {
                DB::table('order_items')->where('oitem_order_id', $orderid->order_id)->delete();
            }

            DB::table('user_address')->where('ua_user_id', $requestData['id'])->delete();
            DB::table('orders')->where('order_user_id', $requestData['id'])->delete();


            return redirect()->back()->with('flash_message', 'User deleted successfully!');

        }

        $users = User::where('role', 1)->orderBy('id', 'desc')
            ->paginate(PAGE_LIMIT);


        $data = array();
        $data['active_menu'] = 'users';
        $data['sub_active_menu'] = 'users-list';
        $data['title'] = 'Users';
        $data['users'] = $users;
        return view('backend.users.list', $data);
    }

    public function userAddress($id)
    {

        $userAddress = UserAddress::where('ua_user_id', $id)->orderBy('ua_id', 'desc')
            ->paginate(PAGE_LIMIT);

        $data = array();
        $data['active_menu'] = 'users';
        $data['userInfo'] = User::where('id', $id)->first();
        $data['sub_active_menu'] = 'users-address';
        $data['title'] = 'Users Address';
        $data['userAddress'] = $userAddress;
        return view('backend.users.address', $data);
    }


    public function vendorAdminOrders($id)
    {

        $orders = OrderItems::with('getProduct.productImages', 'getOrder')->join('products', 'oitem_product_id', '=', 'products.p_id')->where('products.p_vendor_id', $id)
            ->orderBy('oitem_id', 'desc')->paginate(PAGE_LIMIT);



        $data = array();
        $data['active_menu'] = 'vendors';
        $data['userInfo'] = User::where('id', $id)->first();
        $data['sub_active_menu'] = 'vendor-admin-orders';
        $data['title'] = 'Users Orders';
        $data['orders'] = $orders;
        return view('backend.vendors.orders', $data);
    }


    public function vendorOrdersDetails($id)
    {

//        $orders = Orders::with('orderItems.getProduct.productImages')->where('order_id', $id)->first();
        $orders = OrderItems::with('getProduct.productImages', 'getOrder')->where('oitem_id', $id)->first();

//        dd($orders);

//        dd($orders->orderItems);

        $data = array();
        $data['active_menu'] = 'vendors';
        $data['sub_active_menu'] = 'vendor-orders-details';
        $data['title'] = 'Vendor Orders Details';
        $data['orders'] = $orders;
        return view('backend.vendors.orderDetails', $data);
    }

    public function vendorStatusUpdate(Request $request)
    {

        if ($request->isMethod('post')) {

            $id = $request->oitem_id;
            $requestData = $request->all();

            $order = OrderItems::findOrFail($id);
            $order->update($requestData);
            $mes = 'Order Status updated successfully!';
            return redirect()->route('vendorOrdersDetails', ['id' => $id])->with('flash_message', $mes);
        }

    }

    public function userOrders($id)
    {

        $orders = Orders::with('orderItems.getProduct.productImages')->where('order_user_id', $id)->orderBy('order_id', 'desc')->paginate(PAGE_LIMIT);



        $data = array();
        $data['active_menu'] = 'users';
        $data['userInfo'] = User::where('id', $id)->first();
        $data['sub_active_menu'] = 'users-orders';
        $data['title'] = 'Users Orders';
        $data['orders'] = $orders;

        return view('backend.users.orders', $data);
    }

    public function userOrdersDetails($id)
    {

        $orders = Orders::with('orderItems.getProduct.productImages')->where('order_id', $id)->first();

//        dd($orders);

//        dd($orders->orderItems);

        $data = array();
        $data['active_menu'] = 'users';
        $data['sub_active_menu'] = 'users-orders-details';
        $data['title'] = 'Users Orders Details';
        $data['orders'] = $orders;
        return view('backend.users.orderDetails', $data);
    }


    public function userWishlist($id)
    {

        $wishlist = Wishlist::with('getProduct.productImages', 'getProduct.getCategory')->where('wishlist_user_id', $id)->orderBy('wishlist_id', 'desc')->paginate(PAGE_LIMIT);

        $data = array();
        $data['active_menu'] = 'users';
        $data['userInfo'] = User::where('id', $id)->first();
        $data['sub_active_menu'] = 'users-wishlist';
        $data['title'] = 'Users Orders';
        $data['wishlist'] = $wishlist;
        return view('backend.users.wishlist', $data);
    }

    public function orders(Request $request)
    {
        $keyword = $request->get('search');
        if (!empty($keyword)) {


            if ($request->get('search_orderid') != '') {
                $orderid = $request->get('search_orderid');
            } else {
                $orderid = null;
            }

            $daterange = 0;
            if ($request->get('search_from_date') != '') {
                $daterange = 1;
                $fromdate = Carbon::parse($request->get('search_from_date'))->format('Y-m-d');
            } else {
                $fromdate = null;
            }
            if ($request->get('search_to_date') != '') {
                $daterange = 1;
                $todate = Carbon::parse($request->get('search_to_date'))->addDay()->format('Y-m-d');
            } else {
                $todate = null;
            }

            if ($request->get('search_status') != '') {
                $status = $request->get('search_status');
            } else {
                $status = null;
            }

            if ($request->get('search_country') != '') {
                $country = $request->get('search_country');
            } else {
                $country = null;
            }


            $orders = Orders::with('orderItems.getProduct.productImages', 'getUser')
                ->when($orderid, function ($query) use ($orderid) {
                    return $query->where('order_id', '=', $orderid);
                })
                ->when($daterange, function ($query) use ($fromdate, $todate) {
                    return $query->whereBetween('created_at', [$fromdate, $todate]);
                })
                ->when($status, function ($query) use ($status) {
                    return $query->where('order_status', '=', $status);
                })
                ->when($country, function ($query) use ($country) {
                    return $query->where('order_delivery_country', '=', $country);
                })
                ->orderBy('order_id', 'desc')->paginate(PAGE_LIMIT);

//            echo "<pre>";
//            print_r($orders);
//            exit();

        } elseif ($request->isMethod('post')) {
            $requestData = $request->all();

            Orders::destroy($requestData['order_id']);
            OrderItems::where('oitem_order_id', $requestData['order_id'])->delete();
            return redirect()->route('adminorders')->with('flash_message', 'Order deleted successfully!');

        } else {

            $orders = Orders::with('orderItems.getProduct.productImages', 'getUser')->orderBy('order_id', 'desc')->paginate(PAGE_LIMIT);
//            dd($orders);
        }

        $data = array();
        $data['active_menu'] = 'orders';
        $data['sub_active_menu'] = 'orders-list';
        $data['title'] = 'Orders';
        $data['orders'] = $orders;
        $data['vendor_list'] = User::Where('role', '405')->get();
        return view('backend.orders', $data);
    }


    public function reports(Request $request)
    {
        $keyword = $request->get('search');
        if (!empty($keyword)) {

            if ($request->get('search_vendor') != '') {
                $vendor = $request->get('search_vendor');
            } else {
                $vendor = null;
            }

            $orderItems = OrderItems::with('getProduct.productImages', 'getProduct.getVendor')
                ->whereHas('getOrder', function ($q) {
                    $q->where('order_payment_status', '=', 'approved');
                });

            if ($vendor) {
                $orderItems->whereHas('getProduct', function ($q) use ($vendor) {
                    $q->where('p_vendor_id', '=', $vendor);
                });

            }

            $orderItems = $orderItems->orderBy('oitem_id', 'desc')->paginate(PAGE_LIMIT);

        } else {

            $orderItems = OrderItems::with('getProduct.productImages', 'getProduct.getVendor')
                ->whereHas('getOrder', function ($q) {
                    $q->where('order_payment_status', '=', 'approved');
                })->orderBy('oitem_id', 'desc')->paginate(PAGE_LIMIT);
        }

        $data = array();
        $data['active_menu'] = 'reports';
        $data['sub_active_menu'] = 'reports';
        $data['title'] = 'Reports';
        $data['orderItems'] = $orderItems;
        $data['vendor_list'] = User::Where('role', '405')->get();
        return view('backend.reports', $data);
    }


    public function Reviews(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            Ratings::destroy($requestData['rt_id']);
            return redirect()->back()->with('flash_message', 'Order deleted successfully!');

        }
        $reveiws = Ratings::with('getUser', 'getProduct')->orderBy('rt_id', 'desc')->paginate(PAGE_LIMIT);
        $data = array();
        $data['active_menu'] = 'reviews';
        $data['sub_active_menu'] = 'reviews-list';
        $data['title'] = 'Reviews';
        $data['reviews'] = $reveiws;
        return view('backend.reviews', $data);
    }


    public function orderStatusUpdate(Request $request)
    {
        if ($request->isMethod('post')) {

            $id = $request->order_id;
            $requestData = $request->all();

            $order = Orders::findOrFail($id);
            $order->update($requestData);
            $mes = 'Order Status updated successfully!';
            return redirect()->route('userOrdersDetails', ['id' => $id])->with('flash_message', $mes);
        }
    }
}
