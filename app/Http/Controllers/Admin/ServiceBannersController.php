<?php

namespace App\Http\Controllers\Admin;

use App\Models\ServiceBanners;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Validator;

class ServiceBannersController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $file = ServiceBanners::findOrFail($requestData['banner_id']);

            if ($file->banner_image != '') {
                File::delete('uploads/service_banners/' . $file->banner_image);
            }
            ServiceBanners::destroy($requestData['banner_id']);
            return redirect()->route('serviceBanners')->with('flash_message', 'Banner deleted successfully!');
        } else {
            $banners = ServiceBanners::orderBy('banner_id', 'desc')
                ->paginate(PAGE_LIMIT);
        }
        $data = array();
        $data['active_menu'] = 'service_banners';
        $data['sub_active_menu'] = 'banners-list';
        $data['title'] = 'Banners';
        $data['banners'] = $banners;
        return view('backend.serviceBanners.list', $data);
    }

    public function addNewBanners(Request $request, $id = null)
    {
        $tableInfo = new ServiceBanners();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['banner_id'] != '') {
                $banners = ServiceBanners::findOrFail($requestData['banner_id']);
                $imageRule = empty($banners->banner_image) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }

            Validator::make($request->all(), [
                'banner_title' => 'required',
                'banner_link' => 'required',
                'banner_image' => $imageRule,
                'banner_status' => 'required'
            ], [
                'banner_title.required' => 'Please enter title',
                'banner_link.required' => 'Please enter Url',
                'banner_image.required' => 'select image to upload',
                'banner_status.required' => 'Select status',
            ]);

            $fileName = '';
            if ($request->hasFile('banner_image')) {
                $uploadPath = public_path('/uploads/service_banners/');

                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                $extension = $request['banner_image']->getClientOriginalName();

                $fileName = time() . $extension;

                $request['banner_image']->move($uploadPath, $fileName);
                $requestData['banner_image'] = $fileName;
            }

            if ($requestData['banner_id'] == '') {

                ServiceBanners::create($requestData);

                $mes = 'Banner added successfully!';
            } else {

                if ($banners->banner_image != '' && $fileName != '') {

                    File::delete('uploads/service_banners/' . $banners->banner_image);
                }

                $banners->update($requestData);
                $mes = 'Banner updated successfully!';
            }
            return redirect()->route('serviceBanners')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['banner_id'] = '';
            $data['banner'] = '';
            if ($id) {
                $data['banner_id'] = $id;
                $data['banner'] = ServiceBanners::findOrFail($id);
            }
            $data['active_menu'] = 'serviceBanners';
            $data['sub_active_menu'] = 'manage-banners';
            $data['title'] = 'Manage banners';
            return view('backend.serviceBanners.add', $data);
        }
    }

    public function deleteBannerimage(Request $request)
    {
        $image = ServiceBanners::findOrFail($request['image']);

        File::delete('uploads/service_banners/' . $image->banner_image);

        $update_data = array();
        $update_data['banner_image'] = '';
        $image->update($update_data);
        exit();
    }
}
