<?php

namespace App\Http\Controllers\Admin;

use App\Models\Categories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class CategoriesController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {

        $data = array();
        $keyword = $request->get('search');
        $catname = $request->get('category_name');
        $category = $request->get('search_category');






        if (!empty($keyword)) {

//            dd($category);
//            dd($status);

                        DB::enableQueryLog();

                        if($category == "0"){
//                            dd("zeri");
                            $category = $category.' ';
                        }elseif($category=='')
                        {
//                            dd("notzeri");
                            $category = null;
                        }



//dump($category);

            $categories = Categories::with('getCategoryName')
                ->when($category, function ($query) use ($category) {
//                    dd('asdas');
                    return $query->where('category_parent_id',  $category);
                })
                ->when($catname, function ($query) use ($catname) {
                    return $query->whereRaw("LOWER(category_name) LIKE ?", '%' . strtolower($catname) . '%');
                })
                ->paginate(PAGE_LIMIT)->appends(request()->query());

//            dd($queries = DB::getQueryLog());


        } elseif ($request->isMethod('post')) {
            $requestData = $request->all();

            $file = Categories::findOrFail($requestData['category_id']);

            if ($file->category_image != '') {
                File::delete('uploads/categories/' . $file->category_image);
                File::delete('uploads/categories/thumbs/' . $file->category_image);
            }
            Categories::destroy($requestData['category_id']);
            return redirect()->route('categories')->with('flash_message', 'Category deleted successfully!');
        } else {
            $categories = Categories::with('getCategoryName')->orderBy('category_id', 'desc')->paginate(PAGE_LIMIT);
        }

        $data['active_menu'] = 'categories';
        $data['sub_active_menu'] = 'categories-list';
        $data['title'] = 'Categories';
        $data['categories'] = $categories;
        return view('backend.categories.list', $data);
    }

    public function addNewCategories(Request $request, $id = null)
    {
        $tableInfo = new Categories();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['category_id'] != '') {
                $categories = Categories::findOrFail($requestData['category_id']);
                $imageRule = empty($categories->category_image) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }
            $request->validate([
                'category_name' => 'required',
                'category_status' => 'required'
            ], [
                'category_name.required' => 'Please enter name',
                'category_status.required' => 'Select status',
            ]);

            $fileName = '';
            if ($request->hasFile('category_image')) {
                $uploadPath = public_path('/uploads/categories/');

                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                $extension = $request['category_image']->getClientOriginalName();

                $fileName = time() . $extension;

                $thumbPath = public_path('/uploads/categories/thumbs/');

                if (!file_exists($thumbPath)) {
                    mkdir($thumbPath, 0777, true);
                }

                $thumb_img = Image::make($request['category_image']->getRealPath())->resize(400, 400, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($thumbPath . '/' . $fileName, 60);

                $request['category_image']->move($uploadPath, $fileName);
                $requestData['category_image'] = $fileName;
            }


            if (!isset($requestData['category_order'])) {
                $requestData['category_order'] = 0;
            }
            if ($requestData['category_id'] == '') {

//                dd($requestData);
                Categories::create($requestData);

                $mes = 'Banner added successfully!';
                return redirect()->route('categories')->with('flash_message', $mes);
            } else {

                if ($categories->category_image != '' && $fileName != '') {

                    File::delete('uploads/categories/' . $categories->category_image);
                    File::delete('uploads/categories/thumbs/' . $categories->category_image);
                }

                $categories->update($requestData);
                $mes = 'Category updated successfully!';

                return redirect()->back()->with('flash_message', $mes);

            }


        } else {
            $data = array();
            $data['category_id'] = '';
            $data['category'] = '';
            if ($id) {
                $data['category_id'] = $id;
                $data['category'] = Categories::with('getCategoryName')->findOrFail($id);
            }
            $data['active_menu'] = 'categories';
            $data['sub_active_menu'] = 'manage-categories';
            $data['title'] = 'Manage categories';
            return view('backend.categories.add', $data);
        }
    }

    public function deleteCategoryimage(Request $request)
    {
        $image = Categories::findOrFail($request['image']);

        File::delete('uploads/categories/' . $image->category_image);
        File::delete('uploads/categories/thumbs/' . $image->category_image);

        $update_data = array();
        $update_data['category_image'] = '';
        $image->update($update_data);
        exit();
    }


    public function checkNewCategoryAlias(Request $request)
    {
        switch ($request['type']) {
            case 'edit':
                $current_category_alias = $request['current_category_alias'];
                if ($request['category_alias'] === $current_category_alias) {
                    echo 'true';
                } else {
                    $countWeb = Categories::where('category_alias', $request['category_alias'])->count();
                    if ($countWeb > 0) {
                        echo 'false';
                    } else {
                        echo 'true';
                    }
                }
                break;
            case 'new':
                $countWeb = Webseries::where('w_alias', $request['w_alias'])->count();
                if ($countWeb > 0) {
                    echo 'false';
                } else {
                    echo 'true';
                }
                break;
            default:
                break;
        }
    }

}
