<?php

namespace App\Http\Controllers\Admin;

use App\Models\ServiceBanners;
use App\Models\ServiceDocuments;
use App\Models\Services;
use App\ServiceInvoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use File;
use Validator;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public static $adminEmail = 'info@vdesiconnect.com';


    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }


    public function Service($serviceType, Request $request)
    {

        if ($request->isMethod('post')) {

            $requestData = $request->all();

            $requestData['s_phone'] = $requestData['se_mobile_prefix'] . '-' . $requestData['s_phone'];

//            dd($requestData);

            $serviceupdate = Services::findOrFail($requestData['s_id']);
            $serviceupdate->update($requestData);
            $mes = 'Service Updated Successfully!';

            return redirect()->back()->with('flash_message', $mes);

        }


        $service = Services::where('s_type', $serviceType)->orderBy('s_id', 'desc')->paginate(PAGE_LIMIT);
        $data = array();
        $data['active_menu'] = 'service';
        $data['sub_active_menu'] = $serviceType;
        $data['serviceType'] = $serviceType;
        $data['title'] = 'Service';
        $data['service'] = $service;
        return view('backend.services.list', $data);
    }

    public function ServiceView($serviceType, Request $request)
    {

//        echo $serviceType;
//        dd($request->id);


        if ($request->isMethod('post')) {

            $requestData = $request->all();

            $requestData['s_phone'] = $requestData['se_mobile_prefix'] . '-' . $requestData['s_phone'];

//            dd($requestData);

            $serviceupdate = Services::findOrFail($requestData['s_id']);
            $serviceupdate->update($requestData);
            $mes = 'Service Updated Successfully!';

            return redirect()->back()->with('flash_message', $mes);

        }


        $service = Services::where('s_type', $serviceType)->where('s_id', $request->id)->first();
//        dd($service);
        $data = array();
        $data['active_menu'] = 'service';
        $data['sub_active_menu'] = 'view-service';
        $data['serviceType'] = $serviceType;
        $data['title'] = 'Service';
        $data['item'] = $service;
        return view('backend.services.view', $data);
    }


    public function invoice($serviceType, Request $request)
    {

//        echo $serviceType;
//        dd($request->id);


        if ($request->isMethod('post')) {

            $requestData = $request->all();

            $item = Services::where('s_id', $requestData['s_id'])->first();;


            $requestData['s_duedate'] = $requestData['s_duedate']!= '' ? date('Y-m-d', strtotime($requestData['s_duedate'])): '';
            $requestData['s_status'] = 'new';
            $requestData['s_sub_total'] = $subtotal = $requestData['s_no_of_days'] * $requestData['s_unit_price'];
            $requestData['s_tax'] = $s_tax = $subtotal / 100 * $requestData['s_tax'];
            $requestData['s_total'] = $subtotal + $s_tax;
            $requestData['service_id'] =  $requestData['s_id'];
            $requestData['s_user_id'] = $item->s_user_id;

            ServiceInvoice::create($requestData);

//            $serviceupdate = Services::findOrFail($requestData['s_id']);
//            $serviceupdate->update($requestData);
            $mes = 'Service Updated Successfully!';

            return redirect()->back()->with('flash_message', $mes);

        }


        $serviceInvoiceLists = ServiceInvoice::with('getService')->where('service_id', $request->id)->get();
//        dd($service);
        $data = array();
        $data['active_menu'] = 'service';
        $data['sub_active_menu'] = 'invoice';
        $data['serviceType'] = $serviceType;
        $data['title'] = 'Service';
        $data['lists'] = $serviceInvoiceLists;
        $data['item']  = Services::where('s_type', $serviceType)->where('s_id', $request->id)->first();;
        return view('backend.services.invoice.lists', $data);
    }


    public function ServiceDocumentsEmailSend(Request $request)
    {

//        echo $request->id;
//        dd("test");

        $docData = ServiceInvoice::with('getService.getUser')->where('id', $request->id)->first();


//        dd($docData);

//        AdminEmail

        $data['emailData'] = $docData;

        try {
            Mail::send('emails_templates.serviceDocuments.document_send_success_to_user', $data, function ($message) use ($docData) {

                $message->from(self::$adminEmail, 'vdesiconnect.com');
                $message->to($docData->s_email);
                $message->subject($docData->s_options . " Documents Placed on Your profile!");
            });

        }

        catch (\Exception $e) {
            //display custom message
//            echo $e->errorMessage();
        }


        $mes = 'Email Send successfully!';

        return redirect()->back()->with('flash_message', $mes);

    }

    public function ServiceDocuments($serviceType, Request $request)
    {

//        echo $serviceType;
//        dd($request->id);


        if ($request->isMethod('post')) {

            $requestData = $request->all();

            $imageRule = 'required';


            Validator::make($request->all(), [
                'sd_title' => 'required',
                'sd_doc_name' => $imageRule,
            ], [
                'sd_title.required' => 'Title is required',
                'sd_doc_name.required' => 'select document to upload',
            ]);

            $fileName = '';
            if ($request->hasFile('sd_doc_name')) {
                $uploadPath = public_path('/uploads/service_documents/');

                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                $extension = $request['sd_doc_name']->getClientOriginalName();

                $fileName = time() . $extension;

                $request['sd_doc_name']->move($uploadPath, $fileName);
                $requestData['sd_doc_name'] = $fileName;
            }


            $requestData['sd_service_id'] = $request->s_id;
            $requestData['sd_status'] = 1;

            ServiceDocuments::create($requestData);
            $mes = 'Document added successfully!';

            return redirect()->back()->with('flash_message', $mes);

        }


        $service = Services::where('s_type', $serviceType)->where('s_id', $request->id)->first();
//        dd($service);
        $servicedoc = ServiceDocuments::where('sd_service_id', $request->id)->get();
        $data = array();
        $data['active_menu'] = 'service';
        $data['sub_active_menu'] = 'service-documents';
        $data['serviceType'] = $serviceType;
        $data['title'] = 'Service';
        $data['item'] = $service;
        $data['servicedoc'] = $servicedoc;
        return view('backend.services.documents', $data);
    }


    public function adminServiceCreateInvoice(Request $request)
    {

        if ($request->isMethod('post')) {

            $requestData = $request->all();



            $medicalupdate = Services::findOrFail($requestData['s_id']);

//            dump($requestData);
//            dd($medicalupdate);

            $requestData['s_duedate'] = date('Y-m-d', strtotime($requestData['s_duedate']));


            $requestData['s_status'] = 'pending';
            $medicalupdate->update($requestData);
            $mes = 'Invoice Generated Successfully!';


            return redirect()->route('showInvoice', ['id' => $request->s_id])->with('flash_message', $mes);


//            if($request->s_type=='medical'){
//                return redirect()->route('Medical')->with('flash_message', $mes);
//            }elseif($request->s_type=='tutor'){
//                return redirect()->route('Tutor')->with('flash_message', $mes);
//            }elseif($request->s_type=='property'){
//                return redirect()->route('Property')->with('flash_message', $mes);
//            }elseif($request->s_type=='visa'){
//                return redirect()->route('Visa')->with('flash_message', $mes);
//            }elseif($request->s_type=='insurance'){
//                return redirect()->route('Insurance')->with('flash_message', $mes);
//            }elseif($request->s_type=='realEstate'){
//                return redirect()->route('realEstate')->with('flash_message', $mes);
//            }


        }

    }

    public function showInvoice(Request $request)
    {

//        dd('ss');


        $invoice = Services::with('getUser')->where('s_id', $request->id)->first();


//        dd($invoice);

        if ($request->isMethod('post')) {

//            dd($invoice->s_type);

            $id = $invoice['s_id'];
//            $contactpersonname = $invoice['s_contact_person'];
//            $address1 = $invoice['s_address_line_1'];
//            $address2 = $invoice['s_address_line_2'];
//            $country = $invoice['s_country'];
//            $state = $invoice['s_state'];
//            $city = $invoice['s_city'];
//            $options = $invoice['s_options'];
//            $date = $invoice['s_date'];
//            $time = $invoice['s_time'];
//            $email = $invoice['s_email'];
//            $phone = $invoice['s_phone'];
//            $msg = $invoice['s_msg'];
//            $type = $invoice['s_type'];
//
//
//            $duration = $invoice['s_duration'];
//            $nodays = $invoice['s_no_of_days'];
//            $unitprice = $invoice['s_unit_price'];
//            $unittype = $invoice['s_unit_type'];
//            $tax = $invoice['s_tax'];
//            $status = $invoice['s_status'];
//            $createdDate = $invoice['created_at'];


            $data = array('bladeData' => $invoice);

//            dd($data);

            $emails = $invoice->s_email;

            try {
                Mail::send('backend.emails.invoice.Invoicetouser', $data, function ($message) use ($request, $emails) {
                    $message->from(commonSettings('adminEmail'), 'vdesiconnect.com');
//            $message->to( $request->input('email') );
                    $message->to($emails);
                    //Add a subject
                    $message->subject("Invoice From VdesiConnect.com");
                });
//            dd("sdgsdgsgd");
            }

            catch (\Exception $e) {
                //display custom message
//                echo $e->errorMessage();
            }



            $sortmobile = explode('-', $invoice->getUser->mobile);
            $userMobile = $sortmobile[1];
            $variables = array(
                '{SERVICE_NAME}' => $invoice->s_type
            );
            $MSG_SERVICE_INVOICE_CREATE_USER = strtr(MSG_SERVICE_INVOICE_CREATE_USER, $variables);
            sendSms($userMobile, trim($MSG_SERVICE_INVOICE_CREATE_USER), $sortmobile[0]);


            return redirect()->route('showInvoice', ['id' => $id])->with('flash_message', 'Invoice Send Successfully');

        }

        $data = array();
        $data['active_menu'] = 'invoice';
        $data['sub_active_menu'] = 'invoice';
        $data['title'] = 'Show Invoice';
        $data['invoice'] = $invoice;
        $data['item']  = Services::where('s_id', $request->id)->first();
        return view('backend.services.show-invoice', $data);
    }


    public function ServiceInvoice(Request $request)
    {

//        dd('ss');


        $invoice = ServiceInvoice::with('getService.getUser')->where('id', $request->id)->first();


//        dd($invoice);

        if ($request->isMethod('post')) {

//            dd($invoice->s_type);

            $id = $invoice['s_id'];

            $data = array('bladeData' => $invoice);

//            dd($data);

            $emails = $invoice->getService->s_email;


//            return view('backend.emails.invoice.Invoicetouser', $data);

            try {
                Mail::send('backend.emails.invoice.Invoicetouser', $data, function ($message) use ($request, $emails) {
                    $message->from(commonSettings('adminEmail'), 'vdesiconnect.com');
//            $message->to( $request->input('email') );
                    $message->to($emails);
                    //Add a subject
                    $message->subject("Invoice From VdesiConnect.com");
                });
//            dd("sdgsdgsgd");
            }

            catch (\Exception $e) {
                //display custom message
//                echo $e->errorMessage();
            }



            $sortmobile = explode('-', $invoice->getService->getUser->mobile);
            $userMobile = $sortmobile[1];
            $variables = array(
                '{SERVICE_NAME}' => $invoice->getService->s_type
            );
            $MSG_SERVICE_INVOICE_CREATE_USER = strtr(MSG_SERVICE_INVOICE_CREATE_USER, $variables);
            sendSms($userMobile, trim($MSG_SERVICE_INVOICE_CREATE_USER), $sortmobile[0]);


            return redirect()->back()->with('flash_message', 'Invoice Send Successfully');

        }

        $data = array();
        $data['active_menu'] = 'invoice';
        $data['sub_active_menu'] = 'invoice';
        $data['title'] = 'Show Invoice';
        $data['invoice'] = $invoice;
        $data['item']  = Services::where('s_id', $request->id)->first();
        return view('backend.services.invoice.show_invoice', $data);
    }


    public function ServiceDelete(Request $request)
    {
        $requestData = $request->all();
        Services::destroy($requestData['s_id']);
//            return redirect()->route('eventsindex')->with('flash_message', 'Event deleted successfully!');
        return redirect()->back()->with('flash_message', 'Service Deleted Successfully!');

    }

    public function ServiceDocumentDelete(Request $request)
    {
        $requestData = $request->all();
        $docs = ServiceDocuments::findOrFail($requestData['sd_id']);

//        dd($docs);

        File::delete('uploads/service_documents/' . $docs->sd_doc_name);
        ServiceDocuments::destroy($requestData['sd_id']);
//            return redirect()->route('eventsindex')->with('flash_message', 'Event deleted successfully!');
        return redirect()->back()->with('flash_message', 'Service Document Deleted Successfully!');

    }


}
