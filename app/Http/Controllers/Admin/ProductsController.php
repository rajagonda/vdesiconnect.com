<?php

namespace App\Http\Controllers\Admin;

use App\Models\Categories;
use App\Models\ProductImages;
use App\Models\Products;
use App\Models\ProductSkus;
use App\Models\ProductVariations;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use League\Csv\Reader;
use League\Csv\Statement;


class ProductsController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        $data = array();

        $keyword = $request->get('search');
        $pname = $request->get('product_name');
        $category = $request->get('search_category');
        $vendor = $request->get('search_vendor');
        $pstatus = $request->get('p_status');

//        dump($pstatus);

        if (!empty($keyword)) {

//            dd($category);
//            dd($status);

//                        DB::enableQueryLog();

            $products = Products::with('getVendor','getCategory')
                ->when("'".$pstatus."'", function ($query) use ($pstatus) {
//                    dd($pstatus);

                    if($pstatus !=''){
                    return $query->where('p_status', $pstatus);

                    }

                })
                ->when($pname, function ($query) use ($pname) {
                    return $query->whereRaw("LOWER(products.p_name) LIKE ?", '%' . strtolower($pname) . '%');
                })
                ->when($category, function ($query) use ($category) {
                    return $query->where('p_cat_id', $category);
                })
//
                ->when($vendor, function ($query) use ($vendor) {
                    return $query->where('p_vendor_id', $vendor);
                })

                ->orderBy('p_id', 'desc')
                ->paginate(PAGE_LIMIT)->appends(request()->query());

//            dd($queries = DB::getQueryLog());

//            dd($products);


        } elseif ($request->isMethod('post')) {
            $requestData = $request->all();

            $productimages = ProductImages::where('pi_product_id', $requestData['p_id'])->get();

            foreach ($productimages as $image) {

                File::delete('uploads/products/' . $image->pi_image_name);
                File::delete('uploads/products/thumbs/' . $image->pi_image_name);
                ProductImages::destroy($image->pi_id);
            }


            $file = Products::findOrFail($requestData['p_id']);

            if ($file->product_image != '') {
                File::delete('uploads/products/' . $file->product_image);
                File::delete('uploads/products/thumbs/' . $file->product_image);
            }

            Products::destroy($requestData['p_id']);

            return redirect()->back()->with('flash_message', 'Product deleted successfully!');

        } else {

            $products = Products::with('productSKUs', 'getCategory', 'getSplCat', 'getVendor')->orderBy('p_id', 'desc')
                ->paginate(PAGE_LIMIT);


        }


        $data['active_menu'] = 'products';
        $data['sub_active_menu'] = 'products-list';
        $data['title'] = 'Products';
        $data['products'] = $products;
        $data['vendor_list'] = User::Where('role', '405')->get();

        return view('backend.products.list', $data);
    }


    public function sellerImportListings(Request $request, $id = null)
    {

        if ($request->isMethod('post')) {


//            dump($request->all());


            $category_id = $request->category_id;


            $csv = Reader::createFromPath($request['import_file'], 'r');
            $csv->setDelimiter(',');
            $csv->setHeaderOffset(0);

            $stmt = (new Statement());
//                ->offset(10)
//                ->limit(25);

//query your records from the document
            $records = $stmt->process($csv);

            $product_list = array();

            foreach ($records as $record) {

//                dump($record);
//                dd( auth()->id());


                $exist_product = Products::where('p_alias', str_slug($record['name']))->get();


//                dd(count($exist_product));

                if (count($exist_product) == 0) {
                    $products_inser_array = array(
                        'p_sku' => uniqid(),
                        'p_name' => $record['name'],
                        'p_alias' => str_slug($record['name']),
                        'p_cat_id' => $category_id,
                        'p_type' => $record['type'],
                        'p_item_spl' => $record['item_spl'],
//                    'p_main_image'=>$record['name'],
                        'p_mrp_price' => $record['mrp_price'],
                        'p_selling_price' => $record['selling_price'],
                        'p_delivery_charges' => $record['delivery_charges'],
                        'p_availability' => $record['availability'],
                        'p_overview' => $record['overview'],
                        'p_specifications' => $record['specifications'],
                        'p_quality_care_info' => $record['quality_care_info'],
//                    'p_delivery_date_time'=>$record['name'],
//                    'p_gift_msg'=>$record['name'],
                        'p_vendor_id' => $record['vendor_id'],
                        'p_status' => $record['status'],
                    );

                    $created_product = Products::create($products_inser_array);

                    $product_list[] = $created_product->p_id;


                }


            }

            return redirect()->route('sellerImportListings', ['id' => $category_id])->with('flash_message', count($product_list) . ' Product Imported successfully!');


        } else {

        }


        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'products';
        $data['sub_active_menu'] = 'import-products';
        $data['cats'] = Categories::all();
        $data['cats_id'] = $id;


        return view('backend.products.import', $data);
    }


    public function addNewProducts(Request $request, $id = null)
    {


//        $tableInfo = new Products();


        if ($request->isMethod('post')) {

            $requestData = $request->all();


            $returnData = Products::createProduct($request);


            if ($returnData['status'] == 'created') {
                $mes = 'Product added successfully!';
//                return redirect()->route('addProductImages', ['id' => $product_id])->with('flash_message', $mes);
                return redirect()->route('admin_products')->with('flash_message', $mes);
            } elseif ($returnData['status'] == 'updated') {
                $mes = 'Product updated successfully!';
                return redirect()->back()->with('flash_message', $mes);
            }





        } else {
            $data = array();
            $data['product_id'] = '';
            $data['product'] = '';
            if ($id) {
                $data['product_id'] = $id;
                $data['product'] = Products::findOrFail($id);
            }
            $data['active_menu'] = 'products';
            $data['sub_active_menu'] = 'manage-products';
            $data['title'] = 'Manage products';
            $data['product_images'] = ProductImages::where('pi_product_id', $id)->get();

            $data['vendor_list'] = User::Where('role', '405')->get();

            return view('backend.products.add', $data);
        }
    }

    public function addProductImages(Request $request, $id)
    {
        $tableInfo = new ProductImages();
        if ($request->isMethod('post')) {

            if ($request->hasFile('productimages')) {
                foreach ($request['productimages'] as $file) {
                    $uploadPath = public_path('/uploads/products/');

                    if (!file_exists($uploadPath)) {
                        mkdir($uploadPath, 0777, true);
                    }
                    $extension = $file->getClientOriginalName();

                    $fileName = time() . $extension;

                    $thumbPath = public_path('/uploads/products/thumbs/');

                    if (!file_exists($thumbPath)) {
                        mkdir($thumbPath, 0777, true);
                    }

                    $thumb_img = \Intervention\Image\Facades\Image::make($file->getRealPath())->resize(400, 400, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $thumb_img->save($thumbPath . '/' . $fileName, 60);

                    $file->move($uploadPath, $fileName);

                    $requestdata = array();
                    $requestdata['pi_image_name'] = $fileName;
                    $requestdata['pi_status'] = 'active';
                    $requestdata['pi_product_id'] = $id;
                    ProductImages::create($requestdata);
                }
            }

            return redirect()->route('addProductImages', ['id' => $id])->with('flash_message', 'Product Images added successfully');

        } else {
            $data = array();

            $data['product_id'] = $id;
            $data['product_images'] = ProductImages::where('pi_product_id', $id)->get();
            $data['active_menu'] = 'products';
            $data['sub_active_menu'] = 'products-images';
            $data['title'] = 'Products';
            return view('backend.products.images', $data);
        }
    }


    public function AdminProductView(Request $request, $id)
    {

        $data = array();
        $data['product'] = Products::with('getCategory', 'getSubCategory', 'getSplCat', 'productImages')->findOrFail($id);
        $data['product_images'] = ProductImages::where('pi_product_id', $id)->get();
        $data['title'] = 'Product View';
        $data['active_menu'] = 'products';
        $data['sub_active_menu'] = 'product-view';
        return view('backend.products.view', $data);
    }

    public function deleteProductimage(Request $request)
    {
        $image = ProductImages::findOrFail($request['image']);

        File::delete('uploads/products/' . $image->pi_image_name);
        File::delete('uploads/products/thumbs/' . $image->pi_image_name);

        ProductImages::destroy($request['image']);
        exit();
    }


    public function checkProductAlias(Request $request)
    {
        switch ($request['type']) {
            case 'edit':
                $current_product_alias = $request['current_product_alias'];
                if ($request['p_alias'] === $current_product_alias) {
                    echo 'true';
                } else {
                    $countWeb = Products::where('p_alias', $request['p_alias'])->count();
                    if ($countWeb > 0) {
                        echo 'false';
                    } else {
                        echo 'true';
                    }
                }
                break;
            case 'new':
                $countWeb = Webseries::where('w_alias', $request['w_alias'])->count();
                if ($countWeb > 0) {
                    echo 'false';
                } else {
                    echo 'true';
                }
                break;
            default:
                break;
        }
    }


    public function productExtraInfo($id, Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $file = ProductVariations::findOrFail($requestData['pe_id']);

            if ($file->pe_image != '') {
                File::delete('uploads/products/' . $file->pe_image);
                File::delete('uploads/products/thumbs/' . $file->pe_image);
            }

            ProductExtraInformation::destroy($requestData['pe_id']);
            return redirect()->route('productExtraInfo', ['id' => $id])->with('flash_message', 'Product Information Deleted successfully!');
        }

        $data = array();
        $data['active_menu'] = 'products';
        $data['sub_active_menu'] = 'products-extra-info';
        $data['title'] = 'Products';
        $data['product_id'] = $id;
        $data['product_details'] = ProductVariations::where('pv_prod_id', $id)->paginate(PAGE_LIMIT);
        return view('backend.products.extraDetails', $data);
    }

    public function addProductExtraInfo(Request $request, $id)
    {
        if ($request->isMethod('post')) {

            $requestData = $request->all();


            $this->validate($request, [
                'pv_sku' => 'required',
                'pv_mrp' => 'required',
                'pv_selling_price' => 'required',
                'pv_status' => 'required'
            ], [
                'pv_sku.required' => 'Enter title',
                'pv_mrp.required' => 'Enter description',
                'pv_selling_price.required' => 'Enter position',
                'pv_status.required' => 'Select status',
            ]);


            if ($requestData['pv_id'] == '') {
                ProductVariations::create($requestData);
                $mes = 'Product Variation added successfully!';
            } else {
                $product = ProductVariations::findOrFail($requestData['pv_id']);
                $product->update($requestData);
                $mes = 'Product Info updated successfully!';
            }
            return redirect()->route('productExtraInfo', ['id' => $id])->with('flash_message', $mes);

        }
    }

    public function showCategoryTypes(Request $request)
    {
        $types = Categories::where('category_status', 'active')->where('category_id', $request->catID)->where('category_options', 'types')->orderBy('category_id', 'asc')->lists('category_name', 'category_id');
        return json_encode($types);
    }


    public function ProductApprove(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $id = $request->p_id;
            $products = Products::findOrFail($id);
            $products->update($requestData);

            if (isset($requestData['options'])) {
                foreach ($requestData['options'] as $key => $options) {
                    $productOptions = ProductSkus::findOrFail($key);
                    $productOptions->update($options);
                }
            }

            Products::ProductApproveMail($id);


            $mes = 'Updated Successfully!';
            return redirect()->back()->with('flash_message', $mes);
        }

    }


    public function VendorVerify(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

//            dd($requestData);

            $id = $request->id;

            $vendor = User::findOrFail($id);
            $requestData['email_verified_at']= now();
//            dd($requestData);
            $vendor->update($requestData);
            $mes = 'Vendor Verified Successfully!';
            return redirect()->route('vendorslist')->with('flash_message', $mes);
        }

    }


}
