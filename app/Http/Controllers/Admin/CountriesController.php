<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Models\Countries;
use App\Models\States;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CountriesController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
            Countries::destroy($requestData['country_id']);
            return redirect()->route('countries')->with('flash_message', 'Country deleted successfully!');
        } else {
            $countries = Countries::orderBy('country_id', 'desc')
                ->paginate(PAGE_LIMIT);
        }
        $data = array();
        $data['active_menu'] = 'countries';
        $data['sub_active_menu'] = 'countries-list';
        $data['title'] = 'Countries';
        $data['countries'] = $countries;
        return view('backend.countries.list', $data);
    }

    public function addNewCountries(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();




            if ($requestData['country_id'] == '') {
                Countries::create($requestData);
                $mes = 'Country added successfully!';
            } else {
                $countries = Countries::findOrFail($requestData['country_id']);
                $countries->update($requestData);
                $mes = 'Country updated successfully!';
            }
            return redirect()->route('countries')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['country_id'] = '';
            $data['country'] = '';
            if ($id) {
                $data['country_id'] = $id;
                $data['country'] = Countries::findOrFail($id);
            }
            $data['active_menu'] = 'countries';
            $data['sub_active_menu'] = 'manage-countries';
            $data['title'] = 'Manage Countries';
            return view('backend.countries.add', $data);
        }
    }


    public function States(Request $request)
    {


        $data = array();

        $keyword = $request->get('search');
        $country = $request->get('search_country');





        if (!empty($keyword)) {


            $states = States::when($country, function ($query) use ($country) {
                    return $query->where('state_country_id', $country);
                    })
                    ->orderBy('state_id', 'desc')
                    ->paginate(PAGE_LIMIT)->appends(request()->query());


        }

        elseif ($request->isMethod('post')) {
            $requestData = $request->all();

            $id=$request->state_id;

//            dd($requestData);

            States::destroy($id);

            return redirect()->back()->with('flash_message', 'State deleted successfully!');

        }else{

            $states = States::orderBy('state_id', 'desc')
                ->paginate(PAGE_LIMIT);


        }


        $data['active_menu'] = 'states';
        $data['sub_active_menu'] = 'states-list';
        $data['title'] = 'States';
        $data['states'] = $states;
        $data['countries'] = Countries::Where('country_status', 'active')->get();
        return view('backend.states.list', $data);



    }

    public function addNewStates(Request $request){

        if ($request->isMethod('post')) {
            $requestData = $request->all();

//            dd($requestData);

            States::create($requestData);
            $mes = 'State added successfully!';

        }

        return redirect()->route('States')->with('flash_message', $mes);

    }

    public function editStates(Request $request){

        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $id=$request->state_id;

            $states = States::findOrFail($id);
            $states->update($requestData);
            $mes = 'State updated successfully!';

        }

        return redirect()->route('States')->with('flash_message', $mes);

    }


    public function Cities(Request $request)
    {



        $cities = City::paginate(PAGE_LIMIT);

        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $id=$request->city_id;

//            dd($requestData);

            City::destroy($id);

            return redirect()->back()->with('flash_message', 'City deleted successfully!');

        }else{

            $data = array();
            $data['active_menu'] = 'cities';
            $data['sub_active_menu'] = 'cities-list';
            $data['title'] = 'Cities';
            $data['cities'] = $cities;
            $data['countries'] = Countries::Where('country_status', 'active')->get();
            $data['states'] = States::Where('state_status', 'active')->get();
            return view('backend.cities.list', $data);


        }



    }

    public function addNewCity(Request $request){

        if ($request->isMethod('post')) {
            $requestData = $request->all();

//            dd($requestData);

            City::create($requestData);
            $mes = 'City added successfully!';

        }

        return redirect()->route('Cities')->with('flash_message', $mes);

    }

    public function editCity(Request $request){

        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $id=$request->city_id;

            $city = City::findOrFail($id);
            $city->update($requestData);
            $mes = 'City updated successfully!';

        }

        return redirect()->route('Cities')->with('flash_message', $mes);

    }

}
