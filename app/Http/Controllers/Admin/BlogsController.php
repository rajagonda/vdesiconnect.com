<?php

namespace App\Http\Controllers\Admin;

use App\Models\Blogs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use File;

class BlogsController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('IsAdmin');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $action = Blogs::removeBlog($requestData['blog_id']);

            if ($action == true) {
                $msg = "Blog deleted successfully!";
            } else {
                $msg = "Blog delete Error!";

            }
            return redirect()->route('blogs')->with('flash_message', $msg);
        } else {

            $blogs =Blogs::orderBy('blog_id', 'DESC')
                ->paginate(PAGE_LIMIT);
        }
        $data = array();
        $data['active_menu'] = 'blogs';
        $data['sub_active_menu'] = 'blogs-list';
        $data['title'] = 'blogs';
        $data['blogs'] = $blogs;
        return view('backend.blogs.list', $data);
    }

    public function addNewBlogs(Request $request, $id = null)
    {
        $tableInfo = new Blogs();
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            if ($requestData['blog_id'] != '') {
                $blogs = Blogs::findOrFail($requestData['blog_id']);
                $imageRule = empty($blogs->blog_image) ? 'required' : '';
            } else {
                $imageRule = 'required';
            }

            Validator::make($request->all(), [
                'blog_title' => 'required',
                'blog_description' => 'required',
                'blog_image' => $imageRule,
                'blog_status' => 'required'
            ], [
                'blog_title.required' => 'Please enter title',
                'blog_description.required' => 'Please enter description',
                'blog_image.required' => 'select image to upload',
                'blog_status.required' => 'Select status',
            ]);


            $requestData['blog_user_id']=Auth::user()->id;


            if ($requestData['blog_id'] == '') {
                if ($request->hasFile('blog_image')) {
                    $fileName = Blogs::imageUpload($request['blog_image']);
                    $requestData['blog_image'] = $fileName;
                }

                Blogs::create($requestData);

                $mes = 'Blog added successfully!';
            } else {

                if ($request->hasFile('blog_image')) {
                    $fileName = Blogs::imageUpload($request['blog_image'], $blogs->blog_image);
                    $requestData['blog_image'] = $fileName;
                }

                $blogs->update($requestData);
                $mes = 'Blog updated successfully!';
            }
            return redirect()->route('blogs')->with('flash_message', $mes);

        } else {
            $data = array();
            $data['blog_id'] = '';
            $data['blog'] = '';
            if ($id) {
                $data['blog_id'] = $id;
                $data['blog'] = Blogs::findOrFail($id);
            }
            $data['active_menu'] = 'blogs';
            $data['sub_active_menu'] = 'manage-blogs';
            $data['title'] = 'Manage blogs';
            return view('backend.blogs.add', $data);
        }
    }

    public function deleteBlogimage(Request $request)
    {
        $image = Blogs::findOrFail($request['image']);

        File::delete('uploads/blogs/' . $image->blog_image);

        $update_data = array();
        $update_data['blog_image'] = '';
        $image->update($update_data);
        exit();
    }
}
