<?php

namespace App\Http\Controllers;

use App\Models\Contactus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function Contactus(Request $request)
    {



        if ($request->isMethod('post')) {

            $requestData = $request->all();
            request()->validate([
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required | email',
                'mobile_prefix' => 'required',
                'phone_number' => 'required | numeric',
                'subject' => 'required',
                'message' => 'required',
                'hiddenRecaptcha' => 'required|recaptcha'
            ], [
                'first_name.required' => 'Enter First Name',
                'last_name.required' => 'Enter Last Name',
                'email.required' => 'Enter email',
                'email.email' => 'Enter a valid email',
                'mobile_prefix.required' => 'Select Code',
                'phone_number.required' => 'Enter Phone Numbers',
                'phone_number.numeric' => 'Enter Numbers Only',
                'subject.required' => 'Enter Subject',
                'message.required' => 'Enter Message',
                'hiddenRecaptcha.required' => 'Select captcha',
                'hiddenRecaptcha.recaptcha' => 'Select Invalid Captcha',
            ]);


            $conatct_id = Contactus::create($requestData)->cu_id;

            Contactus::contactUsEmails($conatct_id);


            $mes = "Your request sent successfully. We will get back to you soon.";
            return redirect()->back()->with('flash_message', $mes);

        }


        $data = array();
        $data['title'] = 'vdesiconnect.com Home';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'vdesiconnect.com';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('frontend.pages.contactus', $data);
    }

}
