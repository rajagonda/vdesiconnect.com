<?php

namespace App\Http\Controllers\frontend;

use App\Models\Ammachethivanta;
use App\Models\AmmachethivantaOrderItems;
use App\Models\AmmachethivantaOrders;
use App\Models\CartCouponsUsage;
use App\Models\City;
use App\Models\Countries;
use App\Models\Coupons;
use App\Models\OccassionReminder;
use App\Models\OrderItems;
use App\Models\Orders;
use App\Models\Products;
use App\Models\ProductSkus;
use App\Models\Services;
use App\Models\States;
use App\Models\UserAddress;
use App\Models\Wishlist;
use App\ServiceInvoice;
use App\User;
use Carbon\Carbon;
use Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use File;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('IsUser');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($locale = null, Request $request)
    {
//        if(Auth::guard('web')->check()){
//            exit();
//        }
        if ($request->isMethod('post')) {

            $requestData = $request->all();

            $users = User::findOrFail(Auth::id());


            $tableInfo = new User();
            request()->validate([
                'name' => 'required',
                'email' => ['required'],
                'mobile' => 'required',
                'gender' => 'required',
                'dob' => 'required',
            ], [
                'name.required' => 'Please enter name',
                'email.required' => 'Please enter email',
                'mobile.required' => 'Enter mobile',
                'gender.required' => 'Select Gender',
                'dob.required' => 'Enter DOB',
            ]);


//            dd($requestData);
//            dd($users);


            if ($request->hasFile('image')) {
                $fileName = User::uploadProfilePic($request['image'], $users->image);
                $requestData['image'] = $fileName;
            }

            $requestData['mobile'] = $requestData['mobile_prefix'] . '-' . $requestData['mobile'];
//dd($requestData['dob']);


            $requestData['dob'] = date("Y-m-d", strtotime($requestData['dob']));
//            dd($requestData);

            $users->update($requestData);
            $mes = 'User updated successfully!';
            return redirect()->route('userprofile')->with('flash_message', $mes);
        } else {
            $data = array();
            $data['active_menu'] = 'profile';
            $data['sub_active_menu'] = 'profile';
            $data['title'] = 'User Profile';
            $data['meta_keywords'] = '';
            $data['meta_description'] = 'vdesiconnect.com';
            $data['user'] = User::where('id', Auth::id())->first();
            return view('frontend.user.profile', $data);
        }
    }


    public function ServiceInvoices($locale = null, Request $request)
    {

        $data = array();
        $data['active_menu'] = 'service-invoice';
        $data['sub_active_menu'] = 'service-invoice';
        $data['title'] = 'Service Invoice';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'vdesiconnect.com';


        switch ($request->action) {
            case 'view':


//                dump(Auth::id());
//                dump($request->id);

                $data['user'] = User::where('id', Auth::id())->first();
                $data['invoiceLists'] = ServiceInvoice::with('getService.getServiceDocuments')
                    ->where('s_user_id', Auth::id())
                    ->where('service_id', $request->id)
                    ->get();
                return view('frontend.user.services.invoice_lists', $data);


                break;

            case 'viewInvoice':


//                dump(Auth::id());
//                dump($request->id);

                $data['user'] = User::where('id', Auth::id())->first();
                $data['invoice'] = ServiceInvoice::with('getService.getServiceDocuments')
                    ->where('s_user_id', Auth::id())
                    ->where('id', $request->id)
                    ->first();
                return view('frontend.user.services.show-invoice', $data);


                break;
            default:
                $data['user'] = User::where('id', Auth::id())->first();
                $data['services'] = Services::with('getServiceDocuments')->where('s_user_id', Auth::id())->get();
//        dd($data['services']);
                return view('frontend.user.services.lists', $data);

                break;
        }


    }


    public function showServiceInvoice(Request $request)
    {
        $invoice = Services::where('s_user_id', Auth::id())->where('s_id', $request->id)->first();
        $data = array();
        $data['active_menu'] = 'service-invoice';
        $data['sub_active_menu'] = 'service-invoice';
        $data['title'] = 'Show Invoice';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'vdesiconnect.com';
        $data['invoice'] = $invoice;
        return view('frontend.user.show-invoice', $data);
    }


    public function deleteUserimage()
    {
        $image = User::findOrFail(Auth::id());

        File::delete('uploads/users/' . $image->image);
        File::delete('uploads/users/thumbs/' . $image->image);

        $update_data = array();
        $update_data['image'] = '';
        $image->update($update_data);
        exit();
    }


    public function changePassword($locale = null, Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
//            $request->validate([
//                'password' => 'required',
//                'new_password' => 'required|string|min:6|max:12|confirmed',
//                'confirm_password' => 'required|same:new_password'
//            ], [
//                'password.required' => 'Please enter old password',
//                'new_password.required' => 'Please enter new password',
//                'new_password.min' => 'Password number contains minimum 6 digits',
//                'new_password.max' => 'Password number contains maximum 12 digits',
//                'new_password.confirmed' => 'Password and confirm password must be same',
//                'confirm_password.required' => 'Please confirm password'
//            ]);


            $id = Auth::id();
            $user = User::findOrFail($id);
            if (Hash::check($requestData['password'], $user->password)) {
                $requestData['password'] = Hash::make($requestData['new_password']);
                $user->update($requestData);
                $mes = 'Password updated successfully!';
                return redirect()->route('userChangePassword')->with('flash_message', $mes);
            } else {
                $mes = 'Old password is wrong ...try again';
                return redirect()->route('userChangePassword')->with('flash_message_error', $mes);
            }

        } else {
            $data = array();
            $data['active_menu'] = 'changePassword';
            $data['sub_active_menu'] = 'changePassword';
            $data['title'] = 'Change Password';
            $data['meta_keywords'] = '';
            $data['meta_description'] = 'vdesiconnect.com';
            $data['countries'] = getCountriesByCode($locale);
            $data['categories'] = getCategories();
            return view('frontend.user.changePassword', $data);
        }
    }

    public function orders($locale = null, Request $request)
    {

        $status = $request->status;

//dd($status);

        $data = array();
        $data['active_menu'] = 'orders';
        $data['sub_active_menu'] = 'orders';
        $data['title'] = 'Orders';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'vdesiconnect.com';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
//        $data['pending_orders'] = Orders::with('getUser', 'orderItems.getProduct.productImages')->where('order_user_id', Auth::id())->where('order_status', 'Pending')->orderBy('order_id', 'desc')->get();
//        $data['completed_orders'] = Orders::with('orderItems.getProduct.productImages')->where('order_user_id', Auth::id())->where('order_status', 'Paid')->orderBy('order_id', 'desc')->get();
//        $data['cancel_orders'] = Orders::with('orderItems.getProduct.productImages')->where('order_user_id', Auth::id())->where('order_status', 'Cancel')->orderBy('order_id', 'desc')->get();


        if ($status == '') {
            $data['active_status'] = 'all';
            $data['all_orders'] = Orders::with('getUser', 'orderItems.getProduct.productImages')->where('order_user_id', Auth::id())->orderBy('order_id', 'desc')->get();
        } else if ($status == 'paid') {
            $data['active_status'] = 'paid';
            $data['all_orders'] = Orders::with('getUser', 'orderItems.getProduct.productImages')->where('order_status', 'Paid')->where('order_user_id', Auth::id())->orderBy('order_id', 'desc')->get();
        } else {
            $data['active_status'] = 'failed';
            $data['all_orders'] = Orders::with('getUser', 'orderItems.getProduct.productImages')->where('order_status', '!=', 'Paid')->where('order_user_id', Auth::id())->orderBy('order_id', 'desc')->get();
        }


//       dd($data['all_orders']);
        return view('frontend.user.orders.orders', $data);
    }


    function getSiteCurency($set = null)
    {
        if ($set == null) {
            if (Session::has('siteCurency')) {

            } else {
                Session::put('siteCurency', 'USD');
            }

//            dd(Session::all());

            return Session::get('siteCurency');
        } else {
            Session::put('email', 'faa@gmail.com'); // a string
            Session::put('siteCurency', $set);
        }
    }


    public function orderDetails($order, $locale = null)
    {


        $data = array();
        $data['active_menu'] = 'orders';
        $data['sub_active_menu'] = 'orders';
        $data['title'] = 'Order Details';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'vdesiconnect.com';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        $data['orderDetails'] = Orders::with('orderItems.getProduct.productImages')->where('order_reference_number', $order)->first();
//        dd($data['orderDetails']);
        return view('frontend.user.orders.orders_details', $data);
    }


    public function unProcessedOrders($locale = null)
    {
        $data = array();
        $data['active_menu'] = 'unProcessedOrders';
        $data['sub_active_menu'] = 'unProcessedOrders';
        $data['title'] = 'Un Completeed Orders';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'vdesiconnect.com';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        return view('frontend.user.unProcessedOrders', $data);
    }



//    public function userAddressBookRemove($locale = null, Request $request)
//    {
//
//        if ($request->isMethod('post')) {
//            $requestData = $request->all();
//            if ($requestData['action'] == 'remove') {
//
//                UserAddress::destroy($requestData['addres_id']);
//            }
//
//
//
//        }
//        return redirect()->back()->with('flash_message', 'Address deleted successfully!');
//
//    }


    public function userAddressBook($locale = null, Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
            UserAddress::destroy($requestData['ua_id']);

            $userPrimaryAddressInfo = UserAddress::where('ua_user_id', Auth::id())->get();
            $userDefaultPrimaryAddressInfo = UserAddress::where('ua_user_id', Auth::id())->where('ua_defult', 1)->get();

            if (count($userDefaultPrimaryAddressInfo) == 0 && count($userPrimaryAddressInfo) > 0) {
                if ($userPrimaryAddressInfo[0]->ua_defult == 0) {
                    $updateData = array();
                    $updateData['ua_defult'] = 1;
                    $userAddress = UserAddress::findOrFail($userPrimaryAddressInfo[0]->ua_id);
                    $userAddress->update($updateData);
                }
            }


            if ($requestData['flag'] == 1) {
                return redirect()->route('cartAdressPage')->with('flash_message', 'Address deleted successfully!');
            } elseif ($requestData['flag'] == 3) {
                return redirect()->back()->with('flash_message', 'Address deleted successfully!');
            } elseif ($requestData['flag'] == 5) {
                return redirect()->back()->with('flash_message', 'Address deleted successfully!');
            } else {
                return redirect()->route('userAddressBook')->with('flash_message', 'Address deleted successfully!');
            }
        }
        $data = array();
        $data['active_menu'] = 'userAddressBook';
        $data['sub_active_menu'] = 'userAddressBook';
        $data['title'] = 'User Address Book';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'vdesiconnect.com';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        $data['country'] = 0;
        $data['userAddresses'] = UserAddress::with('getState', 'getCity')->where('ua_user_id', Auth::id())->orderBy('ua_id', 'desc')->get();
        return view('frontend.user.address', $data);
    }

    public function userAddAddressBook(Request $request)
    {
        if ($request->isMethod('post')) {

            $requestData = $request->all();


            request()->validate([
                'ua_name' => 'required',
                'ua_address' => 'required',
                'ua_state' => 'required',
                'ua_email' => 'required',
                'ua_phone' => 'required',
                'ua_country' => 'required'
            ], [
                'ua_name.required' => 'Please enter name',
                'ua_address.required' => 'Please enter address',
                'ua_state.required' => 'Please enter state',
                'ua_email.required' => 'Please enter email',
                'ua_phone.required' => 'Please enter phone',
                'ua_country.required' => 'Please select Country'
            ]);

//            dd($requestData);

            $userPrimaryAddressInfo = UserAddress::where('ua_user_id', Auth::id())->first();

            if (empty($userPrimaryAddressInfo)) {
                $requestData['ua_defult'] = 1;
            } else {
                if (isset($requestData['ua_defult']) && $requestData['ua_defult'] == 1) {
                    UserAddress::where('ua_user_id', Auth::id())->update(['ua_defult' => 0]);
                } else {
                    $requestData['ua_defult'] = 0;
                }
            }

            if ($requestData['ua_id'] == '') {
                $requestData['ua_user_id'] = Auth::id();
                $requestData['ua_status'] = 'active';
                $requestData['ua_phone'] = $request->mobile_prefix . '-' . $request->ua_phone;

                UserAddress::create($requestData);
                $mes = 'Address added successfully!';
            } else {


                $userAddress = UserAddress::findOrFail($requestData['ua_id']);
                $requestData['ua_phone'] = $request->mobile_prefix . '-' . $request->ua_phone;
                $userAddress->update($requestData);

                $userPrimaryAddressInfo = UserAddress::where('ua_user_id', Auth::id())->get();
                $userDefaultPrimaryAddressInfo = UserAddress::where('ua_user_id', Auth::id())->where('ua_defult', 1)->get();

                if (count($userDefaultPrimaryAddressInfo) == 0 && count($userPrimaryAddressInfo) > 0) {
                    if ($userPrimaryAddressInfo[0]->ua_defult == 0) {
                        $updateData = array();
                        $updateData['ua_defult'] = 1;
                        $userAddress = UserAddress::findOrFail($userPrimaryAddressInfo[0]->ua_id);
                        $userAddress->update($updateData);
                    }
                }


                $mes = 'Address updated successfully!';
            }

            return redirect()->back()->with('flash_message', $mes);


        }
    }

    public function makeDefaultAddress($locale = null, Request $request)
    {
        $requestData = $request->all();

        UserAddress::where('ua_user_id', Auth::id())->update(['ua_defult' => 0]);

        $userAddress = UserAddress::findOrFail($requestData['ua_id']);
        $userAddress->update($requestData);
        $mes = 'Address updated successfully!';
        if ($requestData['flag'] == 5) {
            return redirect()->back()->with('flash_message', $mes);
        } else {
            return redirect()->route('cartAdressPage')->with('flash_message', $mes);
        }

    }

    public function wishList(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
            Wishlist::destroy($requestData['wishlist_id']);
            return redirect()->route('wishList')->with('flash_message', 'Wishlist Product deleted successfully!');
        }
        $data = array();
        $data['active_menu'] = 'wishList';
        $data['sub_active_menu'] = 'wishList';
        $data['title'] = 'Wish List';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'vdesiconnect.com';
        $data['wishlistProducts'] = Wishlist::with(['getProduct.getCategory', 'getProduct.productSKUs', 'getProduct.productImages'])->where('wishlist_user_id', Auth::id())->get();
        return view('frontend.user.wishlist', $data);
    }

    public function orderConfirmation($locale = null)
    {
        if (Cart::content()->count()) {
            $data = array();
            $data['active_menu'] = 'orderConfirmation';
            $data['sub_active_menu'] = 'orderConfirmation';
            $data['title'] = 'Order Confirmation';
            $data['meta_keywords'] = '';
            $data['meta_description'] = 'vdesiconnect.com';
            $data['countries'] = getCountriesByCode($locale);
            $data['categories'] = getCategories();
            $data['userAddresses'] = UserAddress::where('ua_user_id', Auth::id())->get();
            return view('frontend.user.orderConfirmation', $data);
        } else {
            return redirect()->route('home');
        }

    }


    public function showOrderInvoice(Request $request)
    {
        $invoice = Orders::with('orderItems')->where('order_user_id', Auth::id())->where('order_id', $request->id)->first();

//        dd($invoice);

        $data = array();
        $data['active_menu'] = 'invoice';
        $data['sub_active_menu'] = 'invoice';
        $data['title'] = 'Show Invoice';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'vdesiconnect.com';
        $data['invoice'] = $invoice;
        return view('frontend.user.orders.show_invoice', $data);
    }


    public function paymentConfirmation($locale = null)
    {

        $data = array();
        $data['active_menu'] = 'paymentConfirmation';
        $data['sub_active_menu'] = 'paymentConfirmation';
        $data['title'] = 'Payment Confirmation';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'vdesiconnect.com';
        $data['countries'] = getCountriesByCode($locale);
        $data['categories'] = getCategories();
        $data['latest_order'] = Orders::with('orderItems')->where('order_user_id', Auth::id())->orderBy('order_id', 'desc')->first();
        $data['latest_order_address'] = unserialize($data['latest_order']->order_delivery_address);
        return view('frontend.user.paymentConfirmation', $data);
    }

    public function addToWishList(Request $request)
    {

        $returndata = array();
        $requestData = $request->all();

        $wishlist = array();
        $wishlist['wishlist_product_id'] = $requestData['id'];
        $wishlist['wishlist_user_id'] = Auth::id();

        $checkExisting = Wishlist::where('wishlist_product_id', $requestData['id'])->where('wishlist_user_id', Auth::id())->first();
        if (!empty($checkExisting)) {
            Wishlist::destroy($checkExisting->wishlist_id);
            $returndata['status'] = 2;


        } else {
            $id = Wishlist::create($wishlist)->wishlist_id;
            if ($id) {
                $returndata['status'] = 1;
            } else {
                $returndata['status'] = 0;
            }
        }
        $returndata['count'] = count(getWishlistProducts());

        return $returndata;
    }


    public function occassionReminder(Request $request)
    {
        if ($request->isMethod('post')) {


            if ($request->post('action') == 'create') {

                request()->validate([
                    'or_name' => 'required',
                    'or_type' => 'required',
                    'or_date' => 'required',
                    'or_remind' => 'required',
                    'or_desc' => 'required',
                ], [
                    'or_name.required' => 'Enter name',
                    'or_type.required' => 'Select Type',
                    'or_date.required' => 'Enter date',
                    'or_remind.required' => 'Select ',
                    'or_desc.required' => 'Enter Description',
                ]);

                $requestData = $request->all();
                $requestData['or_user_id'] = Auth::id();
                $requestData['or_date'] = date('Y-m-d H:i:s', strtotime($requestData['or_date']));

//            dd($requestData);

                OccassionReminder::create($requestData);

                $mes = "Create successfully ";

                return redirect()->back()->with('flash_message', $mes);


            } else if ($request->post('action') == 'create') {

            } else if ($request->post('action') == 'remove') {

//                dd($request->all());

                $reminder_id = $request->post('reminder_id');
                OccassionReminder::destroy($reminder_id);

                $mes = "Removed successfully ";

                return redirect()->back()->with('flash_message', $mes);

            }


//            dd($request->all());


        } else {


            $data = array();
            $data['active_menu'] = 'occassionReminder';
            $data['sub_active_menu'] = 'occassionReminder';
            $data['title'] = 'Payment Confirmation';
            $data['meta_keywords'] = '';
            $data['meta_description'] = 'vdesiconnect.com';
            $data['meta_keywords'] = '';
            $data['meta_description'] = 'vdesiconnect.com';
            $data['OccassionReminder'] = OccassionReminder::where('or_user_id', Auth::id())->orderby('or_id', 'desc')->get();

            return view('frontend.user.occassion_reminder', $data);
        }
    }

    public function cartAdressPage()
    {
        if (Cart::getContent()->count()) {

//            foreach (Cart::getContent() as $item){
//                $coutryId=$item->attributes->countryShipped;
//                break;
//            }

            $coutryId = getCountry();

//            dd(getCountry());

            $data = array();
            $data['country'] = getCountry();
            $data['title'] = 'vdesiconnect.com cart Address page';
            $data['meta_keywords'] = '';
            $data['meta_description'] = 'vdesiconnect.com';
            $data['active_menu'] = 'cacart_adressrt';
            $data['sub_active_menu'] = 'cart_adress';
            $data['userAddresses'] = UserAddress::where('ua_country', $coutryId)->where('ua_user_id', Auth::id())->get();
            return view('frontend.cart_adress', $data);
        } else {
            return redirect()->route('home');
        }

    }

    public function checkoutPage(Request $request)
    {
        if (Cart::getContent()->count()) {
//            foreach (Cart::getContent() as $item){
//                $coutryId=$item->attributes->countryShipped;
//                break;
//            }
            $coutryId = getCountry();


            $requestData = $request->all();
            $data = array();
            $data['title'] = 'vdesiconnect.com Home';
            $data['meta_keywords'] = '';
            $data['meta_description'] = 'vdesiconnect.com';
            $data['country'] = $coutryId;
            $data['active_menu'] = 'home';
            $data['address_id'] = $requestData['address_id'];
            $data['address_info'] = UserAddress::where('ua_id', $requestData['address_id'])->first();
            $data['sub_active_menu'] = 'home';
            return view('frontend.checkout', $data);
        } else {
            return redirect()->route('home');
        }

    }

    public function saveOrders(Request $request)
    {
        $order_reference_number = Auth::id() . '-' . time();
        $requestData = $request->all();

        $userAddress = UserAddress::with('getCountry', 'getCity', 'getState')->findOrFail($requestData['address_id']);

//        dd($requestData);
//        dd(Cart::getContent());


        $userAdd = array();
        $userAdd['ua_name'] = $userAddress->ua_name;
        $userAdd['ua_address'] = $userAddress->ua_address;
        $userAdd['ua_landmark'] = $userAddress->ua_landmark;
        $userAdd['ua_city'] = (isset($userAddress->getCity->city_name)) ? $userAddress->getCity->city_name : '';
        $userAdd['ua_state'] = $userAddress->getState->state_name;
        $userAdd['ua_email'] = $userAddress->ua_email;
        $userAdd['ua_phone'] = $userAddress->ua_phone;
        $userAdd['ua_pincode'] = $userAddress->ua_pincode;
        $userAdd['ua_country'] = $userAddress->getCountry->country_name;
        $orders = array();
        $orders['order_user_id'] = Auth::id();
        $orders['order_delivery_address'] = serialize($userAdd);
        $orders['order_delivery_country'] = $userAddress->ua_country;;
        if ($requestData['addr'] == 1) {
            $billingAdd = array();
            $billingAdd['ua_name'] = $requestData['ua_name'];
            $billingAdd['ua_address'] = $requestData['ua_address'];
            $billingAdd['ua_phone'] = '+ ' . $requestData['mobile_prefix'] . ' ' . $requestData['ua_phone'];
            $billingAdd['ua_pincode'] = $requestData['ua_pincode'];
            if ($requestData['ua_city']) {
                $cityInfo = City::where('city_id', $requestData['ua_city'])->first();
                $billingAdd['ua_city'] = $cityInfo->city_name;
            }
            $stateInfo = States::where('state_id', $requestData['ua_state'])->first();
            $billingAdd['ua_state'] = $stateInfo->state_name;
            $countryInfo = Countries::where('country_id', $requestData['ua_country'])->where('country_status', 'active')->first();
            $billingAdd['ua_country'] = $countryInfo->country_name;
            $orders['order_billing_address'] = serialize($billingAdd);
        } else {
            $orders['order_billing_address'] = serialize($userAdd);
        }
        $orders['order_status'] = 'PendingPayment';
        $orders['order_sub_total_price'] = str_replace(',', '', Cart::getTotal());
        $orders['order_reference_number'] = $order_reference_number;
        $orders['order_total_weight'] = $requestData['totalweight'];
        $orders['order_shipping_price'] = $requestData['shippingprice'];

        $orders['order_coupon_code'] = $requestData['couponcode'];
        $couponInfo = Coupons::where('coupon_code', $requestData['couponcode'])->first();
        if (!empty($couponInfo)) {
            if ($couponInfo->coupon_status == 'active') {
                $orders['order_coupon_discount'] = $couponInfo->coupon_discount_value;
                $orders['order_coupon_discount_type'] = $couponInfo->coupon_discount_type;
                $orders['order_coupon_discount_amount'] = $requestData['coupondiscount'];
            }
        }
        if ($requestData['amount_payable']) {
            $orders['order_total_price'] = $requestData['amount_payable'];
        } else {
            $orders['order_total_price'] = Cart::getTotal() + $requestData['shippingprice'];
        }


        if (count(Cart::getContent()) > 0) {
            $orders['order_currency'] = array_first(Cart::getContent())->attributes->currency;
        }


//        dd($orders);


        $order_id = Orders::create($orders)->order_id;

        foreach (Cart::getContent() as $row) {

//            dd($row->attributes->options);


            $getProductInfo = Products::with('productSKUs')->where('p_id', $row->id)->first();

            $getSkUId = strSkuToGetSkuId($row->attributes->options);

            $getSkUInfo = ProductSkus::where('sku_id', $getSkUId)->first();

            if (!empty($getSkUInfo)) {

//                dump($row);
//                dump($row->attributes->options);
//                dump($getSkUInfo);
//                dd($getSkUId);
//                dd($getProductInfo);

                $itemSubtotal = $row->price + $row->attributes->delivery_charge;
//            $itemSubtotal = $row->getPriceSumWithConditions();

                $orderItems = array();
                $orderItems['oitem_order_id'] = $order_id;
                $orderItems['oitem_product_id'] = $row->id;
                $orderItems['oitem_product_name'] = $row->name;
                $orderItems['oitem_product_options'] = $row->attributes->options;
                $orderItems['oitem_delivery_charge'] = $row->attributes->delivery_charge;
                $orderItems['oitem_qty'] = str_replace(',', '', $row->quantity);
                $orderItems['oitem_product_price'] = str_replace(',', '', $row->price);
                $orderItems['oitem_sub_total'] = str_replace(',', '', $itemSubtotal);
                $orderItems['oitem_delivery_time'] = $row->attributes->delivery_time;
                $orderItems['oitem_message'] = $row->attributes->message;
                $orderItems['oitem_nameongift'] = $row->attributes->nameongift;
                $orderItems['oitem_delivery_date'] = $row->attributes->delivery_date;

                $orderItems['oitem_item_sku_id'] = $getSkUInfo->sku_id;
                $orderItems['oitem_item_sku_vendor_price'] = $getSkUInfo->sku_vendor_price;
                $orderItems['oitem_item_sku_vdc_commission_type'] = $getSkUInfo->sku_vdc_commission_type;
                $orderItems['oitem_item_sku_vdc_commission_value'] = $getSkUInfo->sku_vdc_commission_value;
                $orderItems['oitem_item_sku_vdc_final_price'] = $getSkUInfo->sku_vdc_final_price;
                $orderItems['oitem_item_sku_store_discount_type'] = $getSkUInfo->sku_store_discount_type;
                $orderItems['oitem_item_sku_store_discount_value'] = $getSkUInfo->sku_store_discount_value;
                $orderItems['oitem_item_sku_store_price'] = $getSkUInfo->sku_store_price;

//            dd($row->attributes->delivery_date);

//            $orderItems['oitem_delivery_date'] = (isset($row->attributes->delivery_date)) ? Carbon::parse($row->attributes->delivery_date)->format('d-m-Y') : '';


//            if (isset($row->attributes->delivery_date)) {
//                dd($row->attributes->delivery_date);
//                $orderItems['oitem_delivery_date'] = date('Y-m-d H:i:s', strtotime($row->attributes->delivery_date));
//            }

//            dump($row->attributes->delivery_date);
//            dd($orderItems);

                OrderItems::create($orderItems);


            }


        }
//        Cart::clear();


        $mes = 'Your Order has been successfully submitted !';
        return redirect()->route('payWithpaypalCart', ['ref_id' => $order_reference_number])->with('flash_message', $mes);
    }

    public function paymentSuccessPage()
    {
        $data = array();
        $data['title'] = 'vdesiconnect.com Payment Success';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'vdesiconnect.com';
        $data['active_menu'] = 'PaymentSuccess';
        $data['sub_active_menu'] = 'PaymentSuccess';
        $data['latest_order'] = Orders::with('orderItems')->orderBy('order_id', 'desc')->first();
        $data['latest_order_address'] = unserialize($data['latest_order']->order_delivery_address);
        return view('frontend.payment_success', $data);

    }

    public function saveRecordsIntoSessions(Request $request)
    {
        $requestData = $request->all();

//        dd($requestData);

        \Session::put('acv_selected_records', $requestData['options']);
        \Session::put('acv_weight_info', $requestData['weight']);
        return redirect()->route('ammachethivanta_address_page');
    }

    public function ammachethivantaAddressPage()
    {
        $items = \Session::get('acv_selected_records');
        $itemweight = \Session::get('acv_weight_info');
        if (!empty($items) && !empty($itemweight)) {
            $data = array();
            $data['country'] = getCountry();
            $data['title'] = 'vdesiconnect.com Address page';
            $data['meta_keywords'] = '';
            $data['meta_description'] = 'vdesiconnect.com';
            $data['active_menu'] = 'address_page';
            $data['sub_active_menu'] = 'address_page';

            $data['userAddresses'] = UserAddress::with('getState', 'getCity')->where('ua_country', $data['country'])->where('ua_user_id', Auth::id())->get();
            return view('frontend.ammaChethiVanta.adress', $data);
        } else {
            return redirect()->route('ammaChethiVanta');
        }

    }

    public function acvorderConfirmation(Request $request)
    {
        $items = \Session::get('acv_selected_records');
        $itemweight = \Session::get('acv_weight_info');
        if (!empty($items) && !empty($itemweight)) {
            $requestData = $request->all();

//            dd($requestData);

            $data = array();
            $data['title'] = 'vdesiconnect.com Home';
            $data['meta_keywords'] = '';
            $data['meta_description'] = 'vdesiconnect.com';
            $data['active_menu'] = 'home';
            $data['items'] = \Session::get('acv_selected_records');


            $data['itemweight'] = \Session::get('acv_selected_records');
//            $data['itemweight'] = \Session::get('acv_weight_info');
//            dd($data['itemweight']);
            $data['address_id'] = $requestData['address_id'];
            $data['address_info'] = UserAddress::where('ua_id', $requestData['address_id'])->first();
            $data['sub_active_menu'] = 'home';

//            dd($data);

            return view('frontend.ammaChethiVanta.checkout', $data);
        } else {
            return redirect()->route('ammaChethiVanta');
        }
    }

    public function ammachethivantaOrdersSave(Request $request)
    {
        $items = \Session::get('acv_selected_records');
        $itemweight = \Session::get('acv_weight_info');
        if (!empty($items) && !empty($itemweight)) {


            $order_id = AmmachethivantaOrders::createOrder($request);
//            dd($order_id);

            return redirect()->route('payWithPaypalAmmachethiVanta', ['id' => $order_id]);

            exit();
        } else {
            return redirect()->route('ammaChethiVanta');
        }

    }


    public function AmmachethiVantaOrders(Request $request)
    {
//        $requestData = $request->all();
        $data = array();
        $data['title'] = 'vdesiconnect.com Home';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'vdesiconnect.com';
        $data['active_menu'] = 'ammachethi-vanta-orders';
        $data['ordersList'] = AmmachethivantaOrders::with('orderItems.getProduct')->where('ac_order_user_id', Auth::id())->get();
        $data['sub_active_menu'] = 'ammachethi-vanta-orders-list';
        return view('frontend.ammaChethiVanta.orders', $data);


    }

    public function AmmachethiVantaOrderView(Request $request, $inv_id)
    {
//        $requestData = $request->all();
        $data = array();
        $data['title'] = 'vdesiconnect.com Home';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'vdesiconnect.com';
        $data['active_menu'] = 'ammachethi-vanta-orders';
        $data['order_id'] = $inv_id;
        $data['orderData'] = AmmachethivantaOrders::with('orderItems.getProduct')->where('ac_order_id', $inv_id)->where('ac_order_user_id', Auth::id())->first();
        $data['sub_active_menu'] = 'ammachethi-vanta-orders-view';
        return view('frontend.ammaChethiVanta.ordersView', $data);

    }

    public function AmmachethiVantaInvoice(Request $request, $inv_id)
    {

//        dd($inv_id);


        $requestData = $request->all();
        $data = array();
        $data['title'] = 'vdesiconnect.com Home';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'vdesiconnect.com';
        $data['active_menu'] = 'ammachethi-vanta-orders';
        $data['ordersList'] = AmmachethivantaOrders::where('ac_order_user_id', Auth::id())->orderBy('ac_order_id', 'DESC')->get();
//        $data['itemweight'] = \Session::get('acv_weight_info');
//        $data['address_id'] = $requestData['address_id'];
//        $data['address_info'] = UserAddress::where('ua_id', $requestData['address_id'])->first();
        $data['sub_active_menu'] = 'ammachethi-vanta-orders-list';
        return view('frontend.ammaChethiVanta.orders', $data);

    }

    public function checkCouponcodes(Request $request)
    {

        $returnArray = array();

        $requestData = $request->all();
        $couponInfo = Coupons::where('coupon_quantity', '>', 0)
            ->where('coupon_country', $requestData['country'])
            ->whereDate('coupon_expiry_date', '>=', date('Y-m-d'))
            ->where('coupon_code', $requestData['coupon'])
            ->first();
//        $total = 0;
        if (!empty($couponInfo)) {
            if ($couponInfo->coupon_status == 'inactive') {
                $returnArray['status'] = 1;
                $returnArray['msg'] = 'Coupon expired';


            } else {
                $usageInfo = false;
                if ($couponInfo->coupon_usage_type == 'Once') {

                    $getUsage = CartCouponsUsage::where('cu_user_id', Auth::id())
                        ->where('cu_coupan_code', $couponInfo->coupon_code)
                        ->count();
                    if ($getUsage > 0) {
                        $usageInfo = false;
                        $returnArray['status'] = 3;
                        $returnArray['msg'] = 'This coupan you have used.';
                    } else {
                        $usageInfo = true;
                    }
                } else {
                    $usageInfo = true;
                }
                if ($usageInfo == true) {
                    $returnArray['status'] = 200;
                    $returnArray['curency'] = getCurency();
                    $returnArray['curency_symble'] = currencySymbol(getCurency());

                    if ($couponInfo->coupon_discount_type == 'Percentage') {
                        $total = ($requestData['total'] * $couponInfo->coupon_discount_value) / 100;
                        $returnArray['price'] = round($total, 1);

                    } else {
                        $total = $couponInfo->coupon_discount_value;
                        $returnArray['price'] = round($total, 1);;
                    }
                    $returnArray['msg'] = 'Discount of ' . $returnArray['curency_symble'] . $returnArray['price'] . ' added to your account';

                }

            }

        } else {
            $returnArray['status'] = 2;
            $returnArray['msg'] = 'Invalid coupon';

        }

        echo json_encode($returnArray);

    }


}
