<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Ammachethivanta;
use App\Models\AmmachethivantaSKUS;
use App\Models\Banners;
use App\Models\Categories;
use App\Models\Products;
use App\Models\Ratings;
use App\Models\SendEnquiry;
use App\Models\ServiceBanners;
use App\Models\Services;
use App\Models\Subscriptions;

//use \Darryldecode\Cart\Cart;


use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Mail;
use Cart;

class FrontendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('IsVendor');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index($county = null)
    {


//        dd(session('siteCurency'));

//        dd(getCountryID('country_name'));

        $data = array();
        $data['title'] = 'Vdesiconnect::Rakhis, Cakes, Flowers, Gifts, Chocolates, Foods, Boutique';
        $data['meta_keywords'] = 'Rakhis, Cakes, Flowers, Gifts, Chocolates, Foods, Boutique';
        $data['meta_description'] = 'Find best local store for Gifts, Cakes, Chocolates, Homefoods, Boutique, Silver Items, Millets, Jewellery and Flowers Buy USA to India, You can buy from usa to deliver India.';
        $data['active_menu'] = 'home';

//        ::with('getProducts.productImages')

        $data['categories_products'] = Categories::with(['getProducts' => function ($query) {
            $query->where('p_status', 1);
        }])
            ->where('category_parent_id', 0)
            ->where('category_status', 'active')
            ->get()
            ->map(function ($category) {
                $category->getProducts = $category->getProducts->take(4);
                return $category;
            });


//        $data['related_items'] = Products::with('getCategory','productImages')->where('p_alias', '!=', $productAlias)->limit(4)->get();


        $data['newArrival'] = Products::with('getCategory', 'productImages')
            ->where('p_status', 1)
            ->orderBy('created_at', 'DESC')
            ->limit(4)
            ->get();


        $data['sub_active_menu'] = 'home';
        $data['banners'] = Banners::where('banner_status', 'active')->orderBy('banner_id', 'DESC')->get();
        $data['serviceBanners'] = ServiceBanners::where('banner_status', 'active')->get();
        $data['wishlistProducts'] = getWishlistProducts();
//
        return view('frontend.home', $data);

    }


    public function cartPage()
    {
//        echo "<pre>";
//        print_r(Cart::getContent());
//        exit();

//        dd(Cart::getContent());


//        if (count(Cart::getContent()) <= 0) {
//            return redirect()->route('home');
//        }


        $data = array();
        $data['title'] = 'vdesiconnect.com Home';
        $data['meta_keywords'] = '';
        $data['meta_description'] = 'vdesiconnect.com';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        $data['wishlistProducts'] = getWishlistProducts();
        return view('frontend.cart', $data);

    }


    // Cart operations

    public function addToCart(Request $request)
    {
        $requestData = $request->all();

//        dd($requestData);

        $productInfo = Products::where('p_id', $requestData['id'])->first();
        $cartorder = Cart::getContent()->count() + 1;


        $delivery_charge = curencyConvert(getCurency(), $requestData['delivery_charge']);

        // lets create first our condition instance
        $deliverychargeConditions = new \Darryldecode\Cart\CartCondition(array(
            'name' => 'DeliverCharge' . $delivery_charge,
            'type' => 'DeliverCharge',
            'value' => '+' . $delivery_charge,
            'order' => $cartorder,
        ));

        Cart::add(array(
            array(
                'id' => $requestData['id'],
                'name' => $productInfo->p_name,
                'price' => curencyConvert(getCurency(), $requestData['price']),
                'quantity' => $requestData['qty'],

                'attributes' => array(
                    'currency' => getCurency(),
                    'options' => $requestData['options'],
                    'nameongift' => $requestData['nameongift'],
                    'delivery_charge' => $delivery_charge,
                    'overview' => $productInfo->p_overview,
                    'message' => $requestData['message'],
                    'delivery_date' => $requestData['delivery_date'],
                    'countryShipped' => $requestData['countryShipped'],
                    'delivery_time' => $requestData['delivery_time']
                ),

                'conditions' => $deliverychargeConditions,

            ),
        ));


//        dd(Cart::getContent());


        echo Cart::getContent()->count();


    }

    public function buyProduct(Request $request)
    {
        $requestData = $request->all();

//        dd($requestData);

        $productInfo = Products::where('p_id', $requestData['id'])->first();


        $cartorder = Cart::getContent()->count() + 1;

        $delivery_charge = curencyConvert(getCurency(), $requestData['delivery_charge']);

        // lets create first our condition instance
        $deliverychargeConditions = new \Darryldecode\Cart\CartCondition(array(
            'name' => 'DeliverCharge' . $delivery_charge,
            'type' => 'DeliverCharge',
            'value' => '+' . $delivery_charge,
            'order' => $cartorder,
        ));

        Cart::add(array(
            array(
                'id' => $requestData['id'],
                'name' => $productInfo->p_name,
                'price' => curencyConvert(getCurency(), $requestData['price']),
                'quantity' => $requestData['qty'],
                'attributes' => array(
                    'currency' => getCurency(),
                    'options' => $requestData['options'],
                    'nameongift' => $requestData['nameongift'],
                    'delivery_charge' => $delivery_charge,
                    'overview' => $productInfo->p_overview,
                    'message' => $requestData['message'],
                    'delivery_date' => $requestData['delivery_date'],
                    'countryShipped' => $requestData['countryShipped'],
                    'delivery_time' => $requestData['delivery_time']
                ),
                'conditions' => $deliverychargeConditions,
            ),
        ));


//        echo Cart::getContent()->count();
        return redirect()->route('cartAdressPage');
    }


    public function updateItemFromCart(Request $request)
    {

        $requestData = $request->all();


//        dd($requestData);

        $CartItem = Cart::get($requestData['id']);


        $attributes = $CartItem->attributes;

        $attributes->put('countryShipped', $requestData['countryShipped']);


//        dd($attributes);


        Cart::update($requestData['id'], array(
            'quantity' => array(
                'relative' => false,
                'value' => $requestData['qty']
            ),
            'attributes' => $attributes
            // so if the current product has a quantity of 4, another 2 will be added so this will result to 6
        ));
    }

    public function removeItemFromCart(Request $request)
    {
        $requestData = $request->all();

        Cart::remove($requestData['id']);

        Cart::clearItemConditions($requestData['id']);

        echo $requestData['id'];

    }


    public function categoriesSplPage($local = 'en', $alias, $option = '', Request $request)
    {
//        dd($alias, $option);
        $limit = 12;

        $cat_products = Products::with(['productSKUs' => function ($quiry) {
            $quiry->where('sku_store_price', '!=', '');
        }])
            ->join('categories', 'categories.category_id', '=', 'products.p_item_spl')
            ->where('categories.category_options', 'others')
            ->where('categories.category_alias', $option)
            ->where('products.p_status', 1)
            ->orderBy('products.created_at', 'DESC')
            ->paginate($limit);
//            ->get();

        $catname = Categories::where('category_alias', $alias)->first();
        $catoption = Categories::where('category_alias', $option)->first();


        if (!empty($catoption)) {
            $meta_title = $catoption->category_meta_title;
            $meta_keywords = $catoption->category_meta_keywords;
            $meta_description = $catoption->category_meta_description;


        } else {
            $meta_title = $catname->category_meta_title;
            $meta_keywords = $catname->category_meta_keywords;
            $meta_description = $catname->category_meta_description;
        }


        $data = array();
        if (!isset($meta_title)) {
            $data['title'] = $meta_title;
        } else {
            $data['title'] = 'Home';
        }
        $data['meta_keywords'] = $meta_keywords;
        $data['meta_description'] = $meta_description;
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        $data['cat_name'] = $catname;
        $data['cat_option'] = $catoption;
        $data['cat_alias'] = $alias;
        $data['wishlistProducts'] = getWishlistProducts();
        $data['cat_products'] = $cat_products;
        return view('frontend.categories', $data);

    }

    public function categoriesPage($local = 'en', $alias, $option = '', Request $request)
    {


        $limit = 12;

        $filters = request()->get('filters');

        $catname = Categories::where('category_alias', $alias)->first();

//        dd($option);


//        dd($filters);

        $catoption = '';
        if ($option != '') {




//            DB::enableQueryLog();


            $cat_products = Products::with(['productSKUs' => function ($quiry) {
                $quiry->where('sku_store_price', '!=', '');
            }])
                ->join('categories', 'categories.category_id', '=', 'products.p_sub_cat_id')
                ->where('categories.category_options', 'types')
                ->where('categories.category_alias', $option)
                ->where('products.p_status', 1)
                ->orderBy('products.created_at', 'DESC')
                ->paginate($limit);

            $catoption = Categories::where('category_alias', $option)->first();

            $meta_title = $catoption->category_meta_title;
            $meta_keywords = $catoption->category_meta_keywords;
            $meta_description = $catoption->category_meta_description;




//            dd(  $queries = DB::getQueryLog());

        } else {

            $cat_products = Products::with(['productSKUs' => function ($quiry) {
                $quiry->where('sku_store_price', '!=', '');
            }])
                ->join('categories', 'categories.category_id', '=', 'products.p_cat_id')
//                ->when($pricelow, function ($query) use ($pricelow) {
//                    return $query->orderBy('product_price', $pricelow);
//                })
//                ->when($fresharraivals, function ($query) use ($fresharraivals) {
//                    return $query->orderBy('product_id', $fresharraivals);
//                })
//                ->when($popular, function ($query) use ($popular) {
//                    return $query->orderBy('product_id', $popular);
//                })
//                ->when($filters, function ($query) use ($filters) {
//                    $query->orderBy('product_skus.sku_store_price', 'desc');
//                })
                ->where('categories.category_alias', $alias)
                ->where('products.p_status', 1);

            if ($filters) {

//                dump($filters);

                if ($filters == 'order_asc') {
                    $cat_products = $cat_products->orderBy('products.created_at', 'ASC');
                } elseif ($filters == 'price_desc') {
                    $cat_products = $cat_products->join('product_skus', 'product_skus.sku_product_id', '=', 'products.p_id');
                    $cat_products = $cat_products->orderBy('product_skus.sku_store_price', 'DESC');
                } elseif ($filters == 'price_asc') {
                    $cat_products = $cat_products->join('product_skus', 'product_skus.sku_product_id', '=', 'products.p_id');
                    $cat_products = $cat_products->orderBy('product_skus.sku_store_price', 'ASC');

                } else {
                    $cat_products = $cat_products->orderBy('products.created_at', 'DESC');
                }

            } else {

                $cat_products = $cat_products->orderBy('products.created_at', 'DESC');
            }


            $cat_products = $cat_products->paginate($limit);
//                ->get();

            $meta_title = $catname->category_meta_title;
            $meta_keywords = $catname->category_meta_keywords;
            $meta_description = $catname->category_meta_description;

        }




//        if($meta_keywords =='' || $meta_description == ''){








        $data = array();
        if (isset($meta_title)) {
            $data['title'] = $meta_title;
        } else {
            $data['title'] = $catname->category_name;
        }

//        echo $data['title']; echo "<br/>";
//        echo $meta_keywords;
//        dd("sdgsdggsa");


        $data['meta_keywords'] = $meta_keywords;
        $data['meta_description'] = $meta_description;
        $data['active_menu'] = $alias;
        $data['sub_active_menu'] = $option;
        $data['cat_alias'] = $alias;
        $data['cat_name'] = $catname;
        $data['cat_option'] = $catoption;
        $data['cat_products'] = $cat_products;
        $data['wishlistProducts'] = getWishlistProducts();
        return view('frontend.categories', $data);

    }

    public function productPage($alias, $productAlias = '', $sku = null, $otp = null, Request $request)
    {


//        dd($sku);
//        dd( $alias,$option );

//        dd($productAlias);

        $product_info = Products::with(['productSKUs' => function ($quiry) {
//            $quiry->where('sku_store_price', '!=', '');
        }, 'getCategory', 'getSubCategory', 'getSplCat', 'productImages', 'productReviews'])
            ->where('p_alias', $productAlias)
            ->where('p_status', 1)
            ->get();

//        $catoption = Categories::where('category_id', $product_info->p_sub_cat_id)->first();
//        $catname = Categories::where('category_id', $product_info->p_cat_id)->first();


//        dd(count($product_info));


        if (count($product_info) < 1) {
//        dd($product_info);
            return redirect()->route('home');
        }

        $data = array();
        if (isset($product_info[0]->p_meta_title)) {
            $data['title'] = $product_info[0]->p_meta_title;
        } else {
            $data['title'] = $product_info[0]->p_name;
        }
        $data['meta_keywords'] = $product_info[0]->p_meta_keywords;
        $data['meta_description'] = $product_info[0]->p_meta_description;
        $data['active_menu'] = $alias;
        $data['sub_active_menu'] = $productAlias;


        $data['product_info'] = (count($product_info) > 0) ? $product_info[0] : '';
        $data['getsku'] = $sku;

        $data['wishlistProducts'] = getWishlistProducts();


        if (isset($data['product_info']->p_cat_id)) {
            $data['related_items'] = Products::with(['productSKUs' => function ($quiry) {
                $quiry->where('sku_store_price', '!=', '');
            }, 'getCategory', 'productImages'])
                ->where('p_cat_id', $data['product_info']->p_cat_id)
                ->where('p_status', 1)
                ->where('p_alias', '!=', $productAlias)
                ->orderBy('created_at', 'DESC')
                ->limit(4)
                ->get();
        }


        $data['getparams'] = $request->query();
        $data['verify_otp_products'] = Session::get('verify_otp_products');


        $validateCountry = productOrderValiidation($data['product_info']);


        if ($validateCountry['code'] == 'false') {
//            dd($validateCountry);
            return redirect()->route('home');
        }


//        dd($data['related_items']);


        return view('frontend.products.productDetails', $data);

    }

    public function servicePage($service = null, Request $request)
    {


        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $requestData['s_user_id'] = Auth::id();
            $requestData['s_currency'] = "USD";
            $requestData['s_phone'] = $requestData['se_mobile_prefix'] . '-' . $requestData['s_phone'];
//            $requestData['se_mobile_prefix']


//            dd($requestData);
//            dd($request->getRequestUri());


//SMS
            $user = Auth::user();

            $sortmobile = explode('-', Auth::user()->mobile);
            $userMobile = $sortmobile[1];
            $variables = array(
                '{USER_NAME}' => $user->name,
                '{SERVICE_NAME}' => $service
            );
            $MSG_SERVICE_REQUEST_ADMIN = strtr(MSG_SERVICE_REQUEST_ADMIN, $variables);
            sendSms(MSG_ADMIN_NUMBER, trim($MSG_SERVICE_REQUEST_ADMIN));

            $MSG_SERVICE_REQUEST_USER = strtr(MSG_SERVICE_REQUEST_USER, $variables);
            sendSms($userMobile, trim($MSG_SERVICE_REQUEST_USER), $sortmobile[0]);
//SMS END


            Services::create($requestData);


            $contactpersonname = $request->s_contact_person;
            $address1 = $request->s_address_line_1;
            $address2 = $request->s_address_line_2;
            $country = $request->s_country;
            $state = $request->s_state;
            $city = $request->s_city;
            $options = $request->s_options;
            $date = $request->s_date;
            $time = $request->s_time;
            $email = $request->s_email;
            $phone = $request->s_phone;
            $msg = $request->s_msg;
            $type = $request->s_type;


            try {
                Mail::send('emails_templates.services.usermail', ['name' => $contactpersonname, 's_options' => $options, 's_date' => $date, 's_time' => $time, 'email' => $email, 's_phone' => $phone, 's_type' => $type], function ($message) use ($request) {
                    $message->from(commonSettings('adminEmail'), 'vdesiconnect.com');
                    $message->to($request->s_email);
                    $message->subject("Thank you for contacting us!");
                });


                $emails = [commonSettings('adminEmail'), commonSettings('adminGmail')];
//            $emails = ['itzursai@gmail.com'];
//            $emails = ['info@vdesiconnect.com'];
                Mail::send('emails_templates.services.usermailtoadmin', ['s_contact_person' => $contactpersonname, 's_address_line_1' => $address1, 's_address_line_2' => $address2, 's_country' => $country, 's_state' => $state, 's_city' => $city, 's_options' => $options, 's_date' => $date, 's_time' => $time, 's_email' => $email, 's_phone' => $phone, 's_msg' => $msg, 's_type' => $type], function ($message) use ($request, $emails) {
                    $message->from($request->s_email, $request->s_contact_person);
//            $message->to( $request->input('email') );
                    $message->to($emails);
                    //Add a subject
                    $message->subject("Service enquiry from " . $request->s_contact_person);
                });

            }

            catch (\Exception $e) {

            }




            /*if ($request->s_type == 'medical') {
                return redirect()->route('servicePage', ['service' => 'medical'])->with('flash_message', 'Thank you for your submission, we will contact you soon!');
            } elseif ($request->s_type == 'online-tutor') {
                return redirect()->route('servicePage', ['service' => 'online-tutor'])->with('flash_message', 'Thank you for your submission, we will contact you soon!');
            } elseif ($request->s_type == 'property-management') {
                return redirect()->route('servicePage', ['service' => 'property-management'])->with('flash_message', 'Thank you for your submission, we will contact you soon!');
            } elseif ($request->s_type == 'visa-support-services') {
                return redirect()->route('servicePage', ['service' => 'visa-support-services'])->with('flash_message', 'Thank you for your submission, we will contact you soon!');
            } elseif ($request->s_type == 'visitors-insurance') {
                return redirect()->route('servicePage', ['service' => 'visitors-insurance'])->with('flash_message', 'Thank you for your submission, we will contact you soon!');
            } elseif ($request->s_type == 'real-estate') {
                return redirect()->route('servicePage', ['service' => 'real-estate'])->with('flash_message', 'Thank you for your submission, we will contact you soon!');
            } elseif ($request->s_type == 'summer-enrichment') {
                return redirect()->route('servicePage', ['service' => 'summer-enrichment'])->with('flash_message', 'Thank you for your submission, we will contact you soon!');
            } elseif ($request->s_type == 'web-developement') {
                return redirect()->route('servicePage', ['service' => 'web-developement'])->with('flash_message', 'Thank you for your submission, we will contact you soon!');
            }*/

            return redirect()->back()->with('flash_message', 'Thank you for your submission, we will contact you soon!');


        }


        if ($service == null) {
            $service = 'all';
        }


        $service_data = serviceInfoData($service);

//        dd($service);
//        dd($service_data);

        $data = array();
        if (isset($service_data['meta'])) {
            $data['title'] = $service_data['meta']['title'];
            $data['meta_keywords'] = $service_data['meta']['keywords'];
            $data['meta_description'] =  $service_data['meta']['description'];
        } else {
            $data['title'] = 'vdesiconnect.com | ' . $service;
            $data['meta_keywords'] = '';
            $data['meta_description'] = '';
        }
        $data['service_type'] = $service;
        $data['active_menu'] = 'servicePage';
        $data['sub_active_menu'] = $service;


//        dd($service);
//        dd($data);


        $data['service_options'] = (isset($service_data['list']))? $service_data['list']:'';

        return view('frontend.service', $data);


//        dd( $alias,$option );

//        dd($productAlias);


    }


    public function subscriptionSave(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $request->validate([
                'email' => 'required | email'
            ], [
                'email.required' => 'Enter email',
                'email.email' => 'Enter a valid email'
            ]);

            $email = Subscriptions::where('email', $request->email)->count();

            if ($email == 0) {
                $id = Subscriptions::create($requestData)->id;
            }

            if (empty($id)) {
                $id = 0;
            }
            echo $id;
            exit();
        }

    }

    public function ammaChethiVanta(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

//            dd($requestData);

//            $request->validate([
//                'email' => 'required | email'
//            ], [
//                'email.required' => 'Enter email',
//                'email.email' => 'Enter a valid email'
//            ]);
//
//            $email = Subscriptions::where('email', $request->email)->count();
//
//            if ($email == 0) {
//                $id = Subscriptions::create($requestData)->id;
//            }
//
//            if (empty($id)) {
//                $id = 0;
//            }
//            echo $id;
//            exit();
        }

        $data = array();
        $data['title'] = 'Order Amma Cheti Vanta at Vdesiconnect';
        $data['meta_keywords'] = 'Order Amma Cheti Vanta';
        $data['meta_description'] = 'Order Amma Cheti Vanta at vdesiconnect we deliver to amma chethi vantaa special indian food. Amma cheti vanta is the special food in Vdesiconnect.';
//        $data['service'] = $service;
        $data['active_menu'] = 'amma-chethi-vanta';
        $data['sub_active_menu'] = 'amma-chethi-vanta';
        $data['records'] = Ammachethivanta::where('ac_status', 'active')->orderBy('ac_id', 'DESC')->get();
//        $data['sub_active_menu'] = $service;


        return view('frontend.ammaChethiVanta.ammaChethiVantaView', $data);


    }

    public function productSendEnquiry(Request $request)
    {


        $requestData = $request->all();

//        dd($requestData);

        request()->validate([
            'se_name' => 'required',
            'se_email' => ['required', 'string', 'email', 'max:255'],
            'se_phone' => ['required', 'numeric'],
            'se_message' => 'required'
        ], [
            'se_name.required' => 'Please enter name',
            'se_email.required' => 'Please enter email',
            'se_email.email' => 'Email Valid Email',
            'se_email.max' => 'Email contains maximum 255 digits',
            'se_phone.required' => 'Enter mobile',
            'se_phone.numeric' => 'Enter Numbers Only',
            'se_message.required' => 'Enter Message'
        ]);

//                dd($requestData);

        $requestData['se_phone'] = $request->se_mobile_prefix . '-' . $request->se_phone;
        $requestData['se_ip_address'] = $request->ip();



        $enqid = SendEnquiry::create($requestData)->se_id;

        SendEnquiry::newSendEnquiryEmail($enqid);

        $mes = "Successfully Sent";

        return redirect()->back()->with('flash_message', $mes);


    }

    public function productSendEnquiryVerify(Request $request)
    {


        $requestData = $request->all();


        request()->validate([
            'se_otp' => ['required', 'numeric']
        ], [
            'se_otp.required' => 'Enter mobile',
            'se_otp.numeric' => 'Enter Numbers Only',
        ]);

//        dump($requestData);
        $resultsData = SendEnquiry::where('se_product_id', $requestData['se_product_id'])
            ->where('se_user_id', $requestData['se_user_id'])
            ->orderBy('se_id', 'desc')->first();

//        dd($resultsData);

        if (isset($resultsData->se_otp) && $resultsData->se_otp != '' && $resultsData->se_otp == $requestData['se_otp']) {
            echo '1';

            Session::push('verify_otp_products', $requestData['se_product_id']);

            $mes = "Successfully Verified";

        } else {
            echo 0;
            $mes = "Invalid OTP";
        }
//        dd($resultsData);


        return redirect()->back()->with('flash_message', $mes);


    }


    public function addNewRating(Request $request)
    {

        if ($request->isMethod('post')) {
            $requestData = $request->all();

//            dd($requestData);

            Ratings::create($requestData);
            $mes = 'Review & Rating added successfully!';

        }

        return redirect()->back()->with('flash_message', $mes);

    }


}
