<?php

namespace App\Http\Controllers;

use App\Models\AmmachethivantaOrders;
use App\Models\CartCouponsUsage;
use App\Models\Coupons;
use App\Models\OrderItems;
use App\Models\Orders;
use App\Models\Services;
use App\ServiceInvoice;
use Carbon\Carbon;
use Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;

class PaymentController extends Controller
{
    public function __construct()
    {
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
                $paypal_conf['client_id'],
                $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
    }


    public function payWithpaypalCart(Request $request, $ref_id)
    {


//dd($ref_id);


        $requestData = $request->all();

        $orderDetails = Orders::with('orderItems.getProduct.productImages')->where('order_reference_number', $ref_id)->first();

        if (!empty($orderDetails)) {

            $curency = $orderDetails->order_currency;
//            $curency = getCurency();

//            dump($orderDetails);


            $payer = new Payer();
            $payer->setPaymentMethod("paypal");

            $itemslists = array();
            $itemslists_price = array();


            if (count($orderDetails->orderItems) > 0) {
                foreach ($orderDetails->orderItems AS $orderItems) {

//                    dump($orderItems);

                    $item = new Item();

//                    $itemPrice = $orderItems->oitem_product_price + $orderItems->oitem_delivery_charge;
//                    $itemPrice = ($orderItems->oitem_product_price * $orderItems->oitem_qty) + $orderItems->oitem_delivery_charge;

                    $itemPrice_inCurency = ($orderItems->oitem_product_price * $orderItems->oitem_qty) + $orderItems->oitem_delivery_charge;
//                    $itemPrice_inCurency = ($orderItems->oitem_product_price * $orderItems->oitem_qty) ;


                    $item->setName($orderItems->oitem_product_name)/** item name **/
                    ->setCurrency($curency)
                        ->setQuantity(1)
//                        ->setQuantity($orderItems->oitem_qty)
                        ->setSku($orderItems->oitem_product_id)
                        ->setPrice($itemPrice_inCurency);
                    $itemslists[] = $item;
                    $itemslists_price[] = $itemPrice_inCurency;

                }
            }

            $itemList = new ItemList();
            $itemList->setItems($itemslists);


            $subtotalPrice = array_sum($itemslists_price);
            $grandTotalPrice = array_sum($itemslists_price) + $orderDetails->order_shipping_price;
            $grandTotalPrice =  $grandTotalPrice- $orderDetails->order_coupon_discount_amount;

            $details = new Details();
            $details->setShipping($orderDetails->order_shipping_price)
                ->setShippingDiscount($orderDetails->order_coupon_discount_amount)
                ->setSubtotal($subtotalPrice);


            $amount = new Amount();
            $amount->setCurrency($curency)
                ->setTotal($grandTotalPrice)
                ->setDetails($details);


            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($itemList)
                ->setDescription("Payment description")
                ->setInvoiceNumber(uniqid() . '_' . $orderDetails->order_id);

//            dump($orderDetails);
//            dump($requestData);

//            dd($transaction);


            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(URL::route('getPaymentCartStatus'))/** Specify return URL **/
            ->setCancelUrl(URL::route('home'));
            $payment = new Payment();
            $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));

//            dd($payment);


            try {
                $payment->create($this->_api_context);
            } catch (\PayPalConnectionException $ex) {
//                dd($ex);
                if (\Config::get('app.debug')) {
                    \Session::put('error', 'Connection timeout');
                    return Redirect::route('cartPage');
                } else {
                    \Session::put('error', 'Some error occur, sorry for inconvenient');
                    return Redirect::route('cartPage');
                }
            }


            foreach ($payment->getLinks() as $link) {
                if ($link->getRel() == 'approval_url') {
                    $redirect_url = $link->getHref();
                    break;
                }
            }
            /** add payment ID to session **/
            \Session::put('paypal_payment_id', $payment->getId());
            if (isset($redirect_url)) {
                /** redirect to paypal **/
                return Redirect::away($redirect_url);
            }
            \Session::put('error', 'Unknown error occurred');

//            dd($payment->getLinks());

            return Redirect::route('payWithpaypalCart');


        } else {
            echo 'invalid Order';
        }

    }

    public function getPaymentCartStatus(Request $request)
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');

//        dd($payment_id);

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error', 'Payment failed');
            return Redirect::route('/');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);


        $datainsert = array();

        $invoice_number = array_first($result->getTransactions())->invoice_number;


        $invoice_data = explode('_', $invoice_number);


        $serviceupdate = Orders::findOrFail($invoice_data[1]);

//        dump($serviceupdate);


//        dd(Orders::findOrFail($invoice_data[1]));


        if ($result->getState() == 'approved') {
            \Session::put('success', 'Payment success');

            $orderUpdate['order_payment_invoice_num'] = $invoice_number;
            $orderUpdate['order_payment_transaction_id'] = $result->getId();
            $orderUpdate['order_payment_status'] = $result->getState();
            $orderUpdate['order_payment_mode'] = $result->getPayer()->payment_method;
            $orderUpdate['order_payment_date'] = Carbon::now()->toDateTimeString();
            $orderUpdate['order_payment_response_dump'] = serialize($result);
            $orderUpdate['order_status'] = 'Paid';
            $serviceupdate->update($orderUpdate);


            OrderItems::where('oitem_order_id', $invoice_data[1])
                ->update(['oitem_payment_status' => 'Paid', 'oitem_status' => 'Paid']);


            Orders::orderPlacedSuccessEmails($invoice_data[1]);

            Cart::clear();


            $updateStatus = CartCouponsUsage::coupanUpdateCartOrder($serviceupdate->order_coupon_code, $invoice_data[1]);
//            $updateStatus;


            $mes = 'Payment successfully!';
            return redirect()->route('cartThankYouPage', ['order' => $invoice_data[1]])->with('flash_message', $mes);
//            return redirect()->route('showOrderInvoice', ['order' => $invoice_data[1]])->with('flash_message', $mes);

        } else {

            $orderUpdate['order_status'] = 'Failed';
            $serviceupdate->update($orderUpdate);

            OrderItems::where('oitem_order_id', $invoice_data[1])->update(['oitem_payment_status' => 'Failed', 'oitem_status' => 'Failed']);


            Orders::orderPlacedFailedEmails($invoice_data[1]);


            $mes = 'Payment Failled!';
            return redirect()->route('showOrderInvoice', ['order' => $invoice_data[1]])->with('flash_message', $mes);

        }


//        \Session::put('error', 'Payment failed');
//        return Redirect::route('showServiceInvoice', ['order' => $invoice_data[1]]);

    }


    public function servicePayWithpaypal(Request $request, $order)
    {


        $curency = getCurency();

        $orderid = $order;

        $invoice = ServiceInvoice::with('getService')->where('s_user_id', Auth::id())->where('id', $orderid)->where('s_status', '!=', 'approved')->first();




        if (!empty($invoice)) {


            $payer = new Payer();
            $payer->setPaymentMethod("paypal");

            $itemslists = array();


            $item = new Item();
            $item->setName($invoice->s_type)/** item name **/
            ->setCurrency($curency)
                ->setQuantity($invoice->s_no_of_days)
                ->setSku($invoice->s_id)
                ->setPrice($invoice->s_unit_price);


            $itemslists[] = $item;


            $itemList = new ItemList();
            $itemList->setItems($itemslists);

            $sub_totalamount = $invoice->s_no_of_days * $invoice->s_unit_price;
            $tax_price = $sub_totalamount / 100 * $invoice->s_tax;

            $details = new Details();
            $details->setTax($tax_price)
                ->setSubtotal($sub_totalamount);

            $totalAmount = $sub_totalamount + $tax_price;


            $amount = new Amount();
            $amount->setCurrency($curency)
                ->setTotal($totalAmount)
                ->setDetails($details);


            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($itemList)
                ->setDescription("Payment description")
                ->setInvoiceNumber(uniqid() . '_' . $invoice->id);


            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(URL::route('getPaymentStatus'))/** Specify return URL **/
            ->setCancelUrl(URL::route('home'));
            $payment = new Payment();
            $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));

            try {
                $payment->create($this->_api_context);
            } catch (\PayPal\Exception\PayPalConnectionException $ex) {
//                dd($ex);
                if (\Config::get('app.debug')) {
                    \Session::put('error', 'Connection timeout');
                    return Redirect::route('paywithpaypal');
                } else {
                    \Session::put('error', 'Some error occur, sorry for inconvenient');
                    return Redirect::route('paywithpaypal');
                }
            }


            foreach ($payment->getLinks() as $link) {
                if ($link->getRel() == 'approval_url') {
                    $redirect_url = $link->getHref();
                    break;
                }
            }
            /** add payment ID to session **/
            \Session::put('paypal_payment_id', $payment->getId());
            if (isset($redirect_url)) {
                /** redirect to paypal **/
                return Redirect::away($redirect_url);
            }
            \Session::put('error', 'Unknown error occurred');
            return Redirect::route('paywithpaypal');
//
        } else {
            $mes = 'Payment already paid!';
            return redirect()->route('showServiceInvoice', ['order' => $orderid])->with('flash_message', $mes);
        }


    }


    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');

//        dd($payment_id);

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error', 'Payment failed');
            return Redirect::route('/');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);


        $datainsert = array();

        $datainsert['s_payment_invoice_num'] = array_first($result->getTransactions())->invoice_number;
        $datainsert['s_payment_transaction_id'] = $result->getId();
        $datainsert['s_status'] = $result->getState();
        $datainsert['s_payment_payer'] = json_encode($result->getPayer());
        $datainsert['s_payment_transactions'] = json_encode($result->getTransactions());

        $invoice_data = explode('_', $datainsert['s_payment_invoice_num']);


//        update data
//        $datainsert;

//        dd($datainsert);
//        dd($result->getTransactions());


        $serviceupdate = ServiceInvoice::findOrFail($invoice_data[1]);
        $serviceupdate->update($datainsert);


        if ($result->getState() == 'approved') {
            \Session::put('success', 'Payment success');


//            dump($invoice_data);
//
//            dd($datainsert);


            $mes = 'Payment successfully!';
//            return redirect()->route('showServiceInvoice', ['order' => $invoice_data[1]])->with('flash_message', $mes);

            return redirect()->route('serviceThankYouPage', ['order' => $invoice_data[1]])->with('flash_message', $mes);


        }

        $mes = 'Payment Failled!';
        return redirect()->route('showServiceInvoice', ['order' => $invoice_data[1]])->with('flash_message', $mes);
//        \Session::put('error', 'Payment failed');
//        return Redirect::route('showServiceInvoice', ['order' => $invoice_data[1]]);
    }


    public function payWithPaypalAmmachethiVanta(Request $request, $ammaVantaId)
    {
        if ($ammaVantaId) {
            $curency = getCurency();

            $ammaChethiVantaInvoice = AmmachethivantaOrders::with('orderItems.getProduct')->where('ac_order_user_id', Auth::id())->where('ac_order_id', $ammaVantaId)->first();
//            dd($ammaChethiVantaInvoice);

            $ac_order_id = $ammaChethiVantaInvoice->ac_order_id;
            $ac_order_total_price = $ammaChethiVantaInvoice->ac_order_total_price;


            $payer = new Payer();
            $payer->setPaymentMethod("paypal");


            $amount = packagePriseSort(vdcSettings('ammachethi_vanta_price'), $ammaChethiVantaInvoice->ac_order_total_weight);
//            $amount = curencyConvert('USD', vdcSettings('ammachethi_vanta_price'));

            $itemslists = array();

            $item = new Item();
            $item->setName('Orders - ' . $ac_order_id)/** item name **/
            ->setCurrency($curency)
                ->setQuantity(1)
//                ->setQuantity(count($ammaChethiVantaInvoice->orderItems))
                ->setSku(random_int(1000, 9999))
//                        ->setPrice($rderItems->acoitem_product_price)
                ->setPrice($amount)
                ->setDescription(count($ammaChethiVantaInvoice->orderItems) . ' Kg');


            $itemslists[] = $item;


            $itemList = new ItemList();
            $itemList->setItems($itemslists);


            $details = new Details();
            $details
//                ->setTax($tax_price)
                ->setSubtotal($amount);

            $totalAmount = $amount;
//            $totalAmount = $sub_totalamount + $tax_price;


            $amount = new Amount();
            $amount->setCurrency($curency)
                ->setTotal($totalAmount)
                ->setDetails($details);


//            dump($itemList);
//            dd($amount);

            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($itemList)
                ->setDescription("Payment description")
                ->setInvoiceNumber(uniqid() . '_' . $ammaChethiVantaInvoice->ac_order_id);


            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(URL::route('payWithPaypalAmmachethiVantaStatus', ['id' => $ammaChethiVantaInvoice->ac_order_id]))/** Specify return URL **/
            ->setCancelUrl(URL::route('home'));
            $payment = new Payment();
            $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));

            try {
                $payment->create($this->_api_context);
            } catch (\PayPal\Exception\PayPalConnectionException $ex) {
//                dd($ex);
                if (\Config::get('app.debug')) {
                    \Session::put('error', 'Connection timeout');
                    return Redirect::route('payWithPaypalAmmachethiVanta');
                } else {
                    \Session::put('error', 'Some error occur, sorry for inconvenient');
                    return Redirect::route('payWithPaypalAmmachethiVanta');
                }
            }


            foreach ($payment->getLinks() as $link) {
                if ($link->getRel() == 'approval_url') {
                    $redirect_url = $link->getHref();
                    break;
                }
            }
            /** add payment ID to session **/
            \Session::put('paypal_payment_id', $payment->getId());
            if (isset($redirect_url)) {
                /** redirect to paypal **/
                return Redirect::away($redirect_url);
            }
            \Session::put('error', 'Unknown error occurred');
            return Redirect::route('payWithPaypalAmmachethiVanta');
//
        } else {
            $mes = 'Payment already paid!';
            return redirect()->route('AmmachethiVantaInvoice', ['order' => $ammaVantaId])->with('flash_message', $mes);
        }


//        $mes = "this transaction has been declined";
//        return redirect()->route('ammaChethiVanta')->with('flash_message', $mes);


    }

    public function payWithPaypalAmmachethiVantaStatus()
    {


        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');

//        dd($payment_id);

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error', 'Payment failed');
            return Redirect::route('/');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

//        dump($result->getPayer()->payment_method);
//        dd($result);


        if ($result->getState() == 'approved') {
            \Session::put('success', 'Payment success');


            $datainsert = array();

            $datainsert['ac_order_payment_invoice_num'] = array_first($result->getTransactions())->invoice_number;
            $datainsert['ac_order_payment_transaction_id'] = $result->getId();
            $datainsert['ac_order_payment_status'] = $result->getState();
            $datainsert['ac_order_payment_mode'] = $result->getPayer()->payment_method;
            $datainsert['ac_order_payment_response_dump'] = serialize($result->getTransactions());
            $invoice_data = explode('_', $datainsert['ac_order_payment_invoice_num']);
            $datainsert['ac_order_status'] = "placed";

//            dd($datainsert);

            $results = AmmachethivantaOrders::orderPaymentStatus($invoice_data[1], $datainsert);


//            $serviceupdate = AmmachethivantaOrders::findOrFail($invoice_data[1]);
//            $serviceupdate->update($datainsert);


            $mes = 'Payment successfully!';
//            return redirect()->route('AmmachethiVantaInvoice', ['order' => $invoice_data[1]])->with('flash_message', $mes);
            return redirect()->route('ammachethivantaThankYouPage', ['order' => $invoice_data[1]])->with('flash_message', $mes);

        }

        $mes = 'Payment Failled!';
        return redirect()->route('AmmachethiVantaInvoice', ['order' => $invoice_data[1]])->with('flash_message', $mes);

    }

}
