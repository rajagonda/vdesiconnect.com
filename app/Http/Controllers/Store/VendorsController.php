<?php

namespace App\Http\Controllers\Store;

use App\Http\Controllers\Controller;
use App\Models\OrderItems;
use App\Models\ProductImages;
use App\Models\ProductSkus;
use App\Models\Products;
use App\Models\VendorAccount;
use App\Models\VendorBusiness;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Validator;


class VendorsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        dd('vendor cnt');
//        $this->middleware('auth');
        $this->middleware('IsVendor');
        define('PAGE_LIMIT', 30);


    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function sellerPreDashbord()
    {



        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        $data['profileAccount'] = VendorAccount::where('va_vendor_id', Auth::guard('store')->user()->id)->first();
        $data['profileBusiness'] = VendorBusiness::where('vb_vendor_id', Auth::guard('store')->user()->id)->first();
        $data['vendorOrderscount'] = OrderItems::with('getProduct.productImages','getOrder')->join('products','oitem_product_id','=','products.p_id')->where('products.p_vendor_id', Auth::guard('store')->user()->id)
            ->where('oitem_status', '!=', 'completed')
            ->where('oitem_payment_status', 'Paid')
            ->count();

//        dd($data['vendorOrderscount']);

        $data['ProductsApprove'] = Products::where('p_status','1')->where('p_vendor_id',Auth::guard('store')->user()->id)->count();
        $data['ProductsNonApprove'] = Products::where('p_status','0')->where('p_vendor_id',Auth::guard('store')->user()->id)->count();


        $data['completedorders'] = OrderItems::with('getProduct.productImages', 'getOrder')->join('products', 'oitem_product_id', '=', 'products.p_id')->where('products.p_vendor_id', Auth::guard('store')->user()->id)
            ->where('oitem_status', 'completed')
            ->where('oitem_payment_status', 'Paid')
            ->count();

        $data['processingorders'] = OrderItems::with('getProduct.productImages', 'getOrder')->join('products', 'oitem_product_id', '=', 'products.p_id')->where('products.p_vendor_id', Auth::guard('store')->user()->id)
            ->where('oitem_status', '!=', 'completed')
            ->where('oitem_payment_status', 'Paid')
            ->count();


        return view('store.pre_dashbord', $data);
    }

    public function sellerDashbord()
    {



        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.dashbord', $data);
    }

    public function sellerListings(Request $request)
    {

        $data = array();

        $keyword = $request->get('search');
        $pname = $request->get('product_name');
        $category = $request->get('search_category');
        $vendor = $request->get('search_vendor');
        $pstatus = $request->get('p_status');



        if (!empty($keyword)) {





//            $listings = Products::where('p_vendor_id', Auth::guard('store')->user()->id)->when($categorysearch, function ($query) use ($categorysearch) {
//                return $query->where('p_cat_id', 'LIKE', "%$categorysearch%");
//            })
//                ->orderBy('p_id', 'DESC')
//                ->paginate($perPage);

            $listings = Products::where('p_vendor_id', Auth::guard('store')->user()->id)
                ->when("'".$pstatus."'", function ($query) use ($pstatus) {
//                    dd($pstatus);

                    if($pstatus !=''){
                        return $query->where('p_status', $pstatus);

                    }

                })
                ->when($pname, function ($query) use ($pname) {
                    return $query->whereRaw("LOWER(products.p_name) LIKE ?", '%' . strtolower($pname) . '%');
                })
                ->when($category, function ($query) use ($category) {
                    return $query->where('p_cat_id', $category);
                })
//
                ->when($vendor, function ($query) use ($vendor) {
                    return $query->where('p_vendor_id', $vendor);
                })

                ->orderBy('p_id', 'desc')
                ->paginate(PAGE_LIMIT)->appends(request()->query());




        } else {
            $listings = Products::where('p_vendor_id', Auth::guard('store')->user()->id)->orderBy('p_id', 'DESC')->get();
        }



        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';


        $data['product_list'] = $listings;


        return view('store.listings.list', $data);
    }

    public function sellerListingsView(Request $request, $id)
    {

        $data = array();
        $data['product'] = Products::with('getCategory', 'getSubCategory', 'getSplCat', 'productImages')->where('p_vendor_id', Auth::guard('store')->user()->id)->findOrFail($id);
        $data['product_images'] = ProductImages::where('pi_product_id', $id)->get();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.listings.view', $data);
    }


    public function vendorAccount()
    {
        $data = array();
        $data['title'] = 'Vendor Account';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.Details.vendorAccount', $data);
    }


    public function sellerNewListings(Request $request, $id = null)
    {

        if ($request->isMethod('post')) {



//            dd($request);


            $request->request->add(['p_vendor_id' => Auth::guard('store')->user()->id]);


            $returnData = Products::createProduct($request);

//dd($returnData);

            if ($returnData['status'] == 'created') {
                $mes = 'Product added successfully!';
//                return redirect()->route('addProductImages', ['id' => $product_id])->with('flash_message', $mes);
                return redirect()->route('seller_listings')->with('flash_message', $mes);
            } elseif ($returnData['status'] == 'updated') {
                $mes = 'Product updated successfully!';
                return redirect()->back()->with('flash_message', $mes);
            }


        } else {
            $data = array();
            $data['product_id'] = '';
            $data['product'] = '';
            if ($id) {
                $data['product_id'] = $id;
                $data['product'] = Products::findOrFail($id);
            }
            $data['active_menu'] = 'products';
            $data['sub_active_menu'] = 'manage-products';
            $data['title'] = 'Manage products';
            $data['product_images'] = ProductImages::where('pi_product_id', $id)->get();

            return view('store.listings.new', $data);
        }
//        $data = array();
//        $data['title'] = 'Seller pre_dashbord';
//        $data['active_menu'] = 'home';
//        $data['sub_active_menu'] = 'home';
//
//
//        return view('store.listings.new', $data);
    }

    public function profileAccount(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            if ($requestData['va_id'] != '') {
                $profile = VendorAccount::findOrFail($requestData['va_id']);
            }

            if ($requestData['va_id'] == '') {


                $requestData['va_vendor_id'] = Auth::guard('store')->user()->id;
                $requestData['va_status'] = 'pending';

                $vendor_id = VendorAccount::create($requestData)->va_id;

                VendorAccount::AccountDetails($requestData['va_vendor_id']);


                $mes = 'Vendor Details Updated Successfully!';
//                return redirect()->route('addProductImages', ['id' => $product_id])->with('flash_message', $mes);
                return redirect()->route('profileAccount')->with('flash_message', $mes);
            } else {

                $profile->update($requestData);
                $vendor_id = $requestData['va_id'];
                $mes = 'Vendor Details Updated Successfully!';
                return redirect()->route('profileAccount')->with('flash_message', $mes);
            }

        }

        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        $data['profile'] = VendorAccount::where('va_vendor_id', Auth::guard('store')->user()->id)->first();
        $data['userdetails'] = User::where('id', Auth::guard('store')->user()->id)->first();
        return view('store.account.profile', $data);
    }

    public function vendorSettings(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();
//            $request->validate([
//                'password' => 'required',
//                'new_password' => 'required|string|min:6|max:12|confirmed',
//                'confirm_password' => 'required|same:new_password'
//            ], [
//                'password.required' => 'Please enter old password',
//                'new_password.required' => 'Please enter new password',
//                'new_password.min' => 'Password number contains minimum 6 digits',
//                'new_password.max' => 'Password number contains maximum 12 digits',
//                'new_password.confirmed' => 'Password and confirm password must be same',
//                'confirm_password.required' => 'Please confirm password'
//            ]);

            if ($requestData['id'] != '') {

                $requestData['mobile']='91-'.$request->mobile;
                $profile = User::findOrFail($requestData['id']);
                $profile->update($requestData);
                $mes = 'Vendor Details Updated Successfully!';
                return redirect()->route('vendorSettings')->with('flash_message', $mes);

            }



            $id = Auth::guard('store')->user()->id;
            $user = User::findOrFail($id);
            if (Hash::check($requestData['password'], $user->password)) {
                $requestData['password'] = Hash::make($requestData['new_password']);
                $user->update($requestData);
                $mes = 'Password updated successfully!';
                return redirect()->route('vendorSettings')->with('flash_message', $mes);
            } else {
                $mes = 'Old password is wrong ...try again';
                return redirect()->route('vendorSettings')->with('flash_message_error', $mes);
            }

        }

        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        $data['profile'] = VendorAccount::where('va_vendor_id', Auth::guard('store')->user()->id)->first();
        $data['userdetails'] = User::where('id', Auth::guard('store')->user()->id)->first();
        return view('store.account.settings', $data);
    }

    public function sellerOrdersNewList()
    {
        $orders = OrderItems::with('getProduct.productImages', 'getOrder')->join('products', 'oitem_product_id', '=', 'products.p_id')->where('products.p_vendor_id', Auth::guard('store')->user()->id)
            ->where('oitem_status', '!=', 'completed')
            ->where('oitem_payment_status', 'Paid')
            ->orderBy('oitem_id', 'desc')->paginate(PAGE_LIMIT);

        $data = array();
        $data['title'] = 'Vendor Orders';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        $data['orders'] = $orders;

        return view('store.orders.new-orders', $data);
    }

    public function sellerOrdersCompletedList()
    {
        $orders = OrderItems::with('getProduct.productImages', 'getOrder')->join('products', 'oitem_product_id', '=', 'products.p_id')->where('products.p_vendor_id', Auth::guard('store')->user()->id)
            ->where('oitem_status', 'completed')
            ->where('oitem_payment_status', 'Paid')
            ->orderBy('oitem_id', 'desc')->paginate(PAGE_LIMIT);

        $data = array();
        $data['title'] = 'Vendor Orders';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        $data['orders'] = $orders;

        return view('store.orders.completed-orders', $data);
    }

    public function sellerOrdersNewListView($id)
    {
        $orders = OrderItems::with('getProduct.productImages', 'getOrder')->where('oitem_id', $id)->first();

        $data = array();
        $data['title'] = 'Vendor Order View';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        $data['orders'] = $orders;
        return view('store.orders.new-order-view', $data);
    }


    public function sellerOrdersCompletedListView($id)
    {
        $orders = OrderItems::with('getProduct.productImages', 'getOrder')->where('oitem_id', $id)->first();

        $data = array();
        $data['title'] = 'Vendor Order View';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        $data['orders'] = $orders;
        return view('store.orders.completed-order-view', $data);
    }

    public function paymentsOverview()
    {
        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.payments.overview', $data);
    }

    public function paymentsInvoice()
    {
        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.payments.invoice', $data);
    }

    public function paymentsInvoiceView()
    {
        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        return view('store.payments.invoice_view', $data);
    }


    public function BusinessDetails(Request $request)
    {
        if ($request->isMethod('post')) {
            $requestData = $request->all();

            $fileName = '';
            if ($request->hasFile('vb_address_proof')) {
                $uploadPath = public_path('/uploads/vendor/proofs');

                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                $extension = $request['vb_address_proof']->getClientOriginalName();

                $fileName = time() . $extension;

                $thumbPath = public_path('/uploads/vendor/proofs/thumbs/');

                if (!file_exists($thumbPath)) {
                    mkdir($thumbPath, 0777, true);
                }

                $thumb_img = \Intervention\Image\Facades\Image::make($request['vb_address_proof']->getRealPath())->resize(400, 400, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($thumbPath . '/' . $fileName, 60);

                $request['vb_address_proof']->move($uploadPath, $fileName);
                $requestData['vb_address_proof'] = $fileName;
            }

            $fileName2 = '';
            if ($request->hasFile('vb_cheque')) {
                $uploadPath = public_path('/uploads/vendor/proofs');

                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                $extension = $request['vb_cheque']->getClientOriginalName();

                $fileName2 = time() . $extension;

                $thumbPath = public_path('/uploads/vendor/proofs/thumbs/');

                if (!file_exists($thumbPath)) {
                    mkdir($thumbPath, 0777, true);
                }

                $thumb_img = \Intervention\Image\Facades\Image::make($request['vb_cheque']->getRealPath())->resize(400, 400, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumb_img->save($thumbPath . '/' . $fileName2, 60);

                $request['vb_cheque']->move($uploadPath, $fileName2);
                $requestData['vb_cheque'] = $fileName2;
            }

            if ($requestData['vb_id'] != '') {
                $profile = VendorBusiness::findOrFail($requestData['vb_id']);
            }

            if ($requestData['vb_id'] == '') {


                $requestData['vb_vendor_id'] = Auth::guard('store')->user()->id;
                $requestData['vb_status'] = 'pending';
                $vendor_id = VendorBusiness::create($requestData)->vb_id;

                VendorBusiness::BusinessDetails($requestData['vb_vendor_id']);


                $mes = 'Vendor Details Updated Successfully!';
//                return redirect()->route('addProductImages', ['id' => $product_id])->with('flash_message', $mes);
                return redirect()->route('BusinessDetails')->with('flash_message', $mes);
            } else {

                $profile->update($requestData);
                $vendor_id = $requestData['vb_id'];
                $mes = 'Vendor Details Updated Successfully!';
                return redirect()->route('BusinessDetails')->with('flash_message', $mes);
            }

        }

        $data = array();
        $data['title'] = 'Seller pre_dashbord';
        $data['active_menu'] = 'home';
        $data['sub_active_menu'] = 'home';
        $data['profile'] = VendorBusiness::where('vb_vendor_id', Auth::guard('store')->user()->id)->first();
        return view('store.account.business_details', $data);
    }



    public function vendorStatusUpdateByVendor(Request $request)
    {

        if ($request->isMethod('post')) {

            $id = $request->oitem_id;
            $requestData = $request->all();

            $order = OrderItems::findOrFail($id);
            $order->update($requestData);
            $mes = 'Order Status updated successfully!';
            return redirect()->route('sellerOrdersNewListView', ['id' => $id])->with('flash_message', $mes);
        }

    }

}
