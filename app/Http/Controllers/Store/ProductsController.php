<?php

namespace App\Http\Controllers\Store;

use App\Models\Categories;
use App\Models\ProductImages;
use App\Models\Products;
use App\Models\ProductVariations;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use League\Csv\Reader;
use League\Csv\Statement;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        define('PAGE_LIMIT', 30);
    }

    public function index(Request $request)
    {

    }




    public function showCategoryTypes(Request $request)
    {
        return Categories::categoryTypes($request->catID);


    }


}
