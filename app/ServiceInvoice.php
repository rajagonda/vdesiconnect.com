<?php

namespace App;

use App\Models\Services;
use Illuminate\Database\Eloquent\Model;

class ServiceInvoice extends Model
{
    protected $fillable = [
        'service_id',
        's_user_id',
        's_duration',
        's_duedate',
        's_no_of_days',
        's_unit_price',
        's_unit_type',
        's_sub_total',
        's_tax',
        's_total',
        's_description',
        's_status',
        's_payment_transaction_id',
        's_payment_invoice_num',
        's_payment_payer',
        's_payment_transactions'
    ];

    public function getService()
    {
        return $this->belongsTo(Services::Class,  'service_id', 's_id');
    }
}
