<?php

namespace App\Console\Commands;

use App\Models\OccassionReminder;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class OccationRemainder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'occationremaindar:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


//      $remaindar= OccassionReminder::where('or_date >= NOW() -  INTERVAL 1 DAY')->get();

//        $remaindar = OccassionReminder::where('or_date', '>=', 'NOW() -  INTERVAL 10 DAY')->get();
//        $remaindar = OccassionReminder::where('or_date > DATE_ADD(NOW(), INTERVAL -1 DAY)')->get();


        $nextday = date('Y-m-d', strtotime(' +1 day'));




        $remaindar = OccassionReminder::where( 'or_date', $nextday)->get();






        foreach ($remaindar as $remaind){


//            echo $remaind->or_name; echo "<br/>";

            $userData = User::where('id', $remaind->or_user_id)->first();
            $data['userData'] = $userData;

            $data = [
                'occation_date' => $remaind->or_date,
                'occation_name' => $remaind->or_name,
                'occation_type' => $remaind->or_type,
                'occation_description' => $remaind->or_desc,
                'user_name' => $userData->name
            ];

//            dd($data['occation_name']);

            $subject = "Occation Remaindar : ".$data['occation_name'];

//            dd($userData);

            try {

                Mail::send('emails_templates.occationRemainder.occation_remainder_user', $data, function ($message) use ($userData,$subject) {
                    $message->from(commonSettings('adminEmail'), 'vdesiconnect');
                    $message->to($userData->email);
                    $message->subject($subject);
                });
                echo 'Send..';
            }

            catch (\Exception $e) {
                //display custom message
//                echo $e->errorMessage();
            }



        }




    }
}
