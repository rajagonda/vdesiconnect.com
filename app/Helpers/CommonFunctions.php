<?php

use App\Models\Countries;
use App\Models\Products;

define('ADMIN_EMAIL', 'info@idconnect.com');


function catagoriesNotes($cat)
{
    $list = array(
        'cakes' => 'Deliver Available only Hyderabad / Secunderabad of India',
        'flower-bouquets' => 'Deliver Available only Hyderabad / Secunderabad of India',
    );

    if (isset($list[$cat])) {

        return $list[$cat];
    } else {

        return '';
    }
}

function cartItemsValidationForCheckOut($items)
{
    $collection = array();

    if (count($items) > 0) {
        foreach ($items AS $item) {
            $collection[] = $item->attributes->countryShipped;
        }
    }

    $unique_country_list = array_unique($collection);

//    dump($unique_country_list);


    $coutryId = getCountry();


    if (in_array($coutryId, $unique_country_list)) {

        return (count(array_unique($collection)) === 1) ? 'true' : 'false';

//        echo 'asd';
    } else {
        return 'false';
    }


}

function category_shipped($status)
{
    $array_data = array();
    $countries = getCountries($status);
    $array_data = $countries;
    return $array_data;

}

function productOrderValiidation($product)
{

    $countryShipped = category_shipped($product->getCategory->category_shipped);


    if (isset($countryShipped[getCountry()])) {

        $data = array(
            'code' => 'true',
            'info' => $countryShipped[getCountry()]
        );

        return $data;
    } else {
        $returnMessage = implode(',', $countryShipped->toArray()) . ' Only ';

        $data = array(
            'code' => 'false',
            'info' => $returnMessage
        );

        return $data;
    }

}


function productCategoryShipped($pid)
{

    $array_data = array();

    $product_info = Products::with(['getCategory'])
        ->where('p_id', $pid)
        ->first();

    $countryShipped = category_shipped($product_info->getCategory->category_shipped);

    $array_data = $countryShipped;

    return $array_data;

}


function getCountryID($sid = null)
{
    if ($sid != null) {
        return strtolower(str_slug(getSiteCurency()['country_info']->$sid));
    } else {
        return getSiteCurency()['country_info'];
    }

}

function getSiteCurency($set = null)
{

//    dd($set);

    if ($set == null) {
        if (session('siteCurency')) {
        } else {

            $getcountry = Countries::where('country_id', 1)->where('country_status', 'active')->first();

//            dd($getcountry);

            session(['siteCurency' => $getcountry->country_currency]);
            session(['siteCountryId' => $getcountry->country_id]);
            session(['siteCountryinfo' => $getcountry]);
        }
        return array(
            'curency' => session('siteCurency'),
            'country_id' => session('siteCountryId'),
            'country_info' => session('siteCountryinfo')
        );
    } else {
//        session(['siteCurency' => $set]);

        session(['siteCurency' => $set->country_currency]);
        session(['siteCountryId' => $set->country_id]);
        session(['siteCountryinfo' => $set]);

        Cart::clear();
//        Cart::remove(167);
    }
}


function getCountry($info = null)
{

    if ($info != '') {

    } else {

        return getSiteCurency()['country_id'];
    }

}


function getCurency()
{
//    dd(getSiteCurency());
    return getSiteCurency()['curency'];
}


function availability($flag = null)
{

    $return_data = array('in_stock' => 'In Stock', 'out_of_stock' => 'Out Of Stock');
    if ($flag) {
        $return_data = $return_data[$flag];
    }
    return $return_data;

}

function splitPrice($string)
{
    $retinarray = array();
    $priceListArrays = explode(',', $string);
    if (count($priceListArrays) > 0) {
        foreach ($priceListArrays AS $priceListArray) {
            $orgArray = explode(':', $priceListArray);
            list($wight, $price) = $orgArray;
            $retinarray[$wight] = $price;


        }
    }

    return $retinarray;
}

function packagePriseSort($priceList, $curentWight)
{


    $priceLists = splitPrice($priceList);


    $returmPrice = '';
    $returmRangePrice = array();

    if (count($priceLists) > 0) {
        foreach ($priceLists AS $wightRange => $priceRange) {
            $getwight = $wightRange - $curentWight;
//                        $returmRangePrice[$wightRange] = $getwight;
            $returmRangePrice[$wightRange] = ($getwight < 0) ? "" : $getwight;
        }
        $returmRangePrice = array_keys(array_filter($returmRangePrice, 'strlen'));
        if (count($returmRangePrice) > 0) {
            $returmPrice = $priceLists[array_first($returmRangePrice)];
        } else {
            $returmPrice = $priceLists[array_last(array_keys($priceLists))];
        }
    }

//    dd($returmPrice);

    return $returmPrice;
}


function ammaChethiVantaWightsData()
{
    return splitPrice(vdcSettings('ammachethi_vanta_price'));
}

function ammaChethiVantaWightBasePrice($wight)
{
    return packagePriseSort(vdcSettings('ammachethi_vanta_price'), $wight);
}


function sendSms($no, $msg, $country = 91)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.msg91.com/api/sendhttp.php");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "mobiles=$no&authkey=281207Ap4gZfODekY5d05c521&route=4&sender=VDESIC&message=$msg&country=$country");
//    curl_setopt($ch, CURLOPT_POSTFIELDS, "username=sonygolden&password=info1234&to=$no&from=SGSCHT&message=$msg");
    $buffer = curl_exec($ch);

    if (empty($buffer)) {
        echo "buffer is empty";
    }

    curl_close($ch);
}


function vdcSettings($colum = null)
{

    $settings = \App\Models\Settings::get()->first();

//    dd($settings);

    if ($colum != '' && isset($settings->$colum)) {
        $settings = $settings->$colum;
    }


    return $settings;


}

function commonSettings($option)
{
    switch ($option) {
        case 'adminEmail':
            return 'info@vdesiconnect.com';
            break;
        case 'adminGmail':
            return 'vdesiconnect@gmail.com';
            break;
        case 'adminName':
            return 'vdesiconnect.com';
            break;
    }
}


function wordSort($word)
{

    $string = str_replace('-', ' ', $word);

    return ucwords($string);

}

function getPhoneNumber($number)
{

    $expolde = explode('-', $number);

    if (count($expolde) == 2) {

        return list($prefix, $mobile) = $expolde;
    } else {

        array_unshift($expolde, 91);
        return list($prefix, $mobile) = $expolde;
    }

//    dd($expolde);


}


function serviceInfoData($name = null)
{

    $returndata = array();

    $list = array(
        'medical-assistance' => array(
            'list' => array(
                'Wellness Check ups',
                'Diagnostics Services',
                'Dental (Eg: Root Canal, Fillings, Regular cleanings)',
                'Physio Therapy',
                'Vision (Eg: Laser Surgery, vision check ups)',
            ),
            'meta' => array(
                'title' => 'Online Healthcare Service Provider|Medical Assistance Services',
                'keywords' => 'Online Healthcare Service Provider, Medical Assistance Services',
                'description' => 'Vdesiconnect is think like innovative services for NRI families and Online Healthcare Service Provider,Medical Assistance Servicesfrom usa to Hyderabad India.',
            )

        ),
        'online-tutor' => array(
            'list' => array(
                'Music Vocal',
                'Music-Piano',
                'Music - Guitar',
                'Vedic Math',
                'Maths',
                'Physics',
                'Chemistry',
                'Others',
            ),
            'meta' => array(
                'title' => 'Vdesiconnect: Best Online teachers, Home tutors, Assignment help',
                'keywords' => 'Best Online teachers, Home tutors, Assignment help',
                'description' => 'We are providing Best Online teachers, Home tutors, Assignment help for students. We have best online teachers and assist for students and online teachers in hyderabad and usa.',
            ),


        ),
        'property-management' => array(
            'list' => array(
                'Rent',
                'Maintenance',
                'Vacate/Cleaning',
                'Property Taxes',
            ),
            'meta' => array(
                'title' => 'USA Real Estate and Property Management Services',
                'keywords' => 'USA Real Estate and Property Management Services',
                'description' => 'USA Real Estate and Property Management Services',
            ),

        ),
        'visa-support-services' => array(
            'list' => array(
                'Student Visas (Eg: UK, USA, Australia, Canada etc.,)',
                'Work Visas (Eg: USA, UAE, UK, Australia etc.,)',
                'Vistor Visas',
                'Other Visas',
            ),
            'meta' => array(
                'title' => 'Visa Support Services US, India, UK, Canada, Australia - Vdesiconnect',
                'keywords' => 'Visa Support Services US, India, UK, Canada, Australia',
                'description' => 'we are providing Visa Support Services US, India, UK, Canada, Australia and end to end visa support services.',
            ),

        ),
        'visitors-insurance' => array(
            'list' => array(
                'Domestic Medical Insurance',
                'USA Isnurance',
                'UK Insurance',
                'UAE Insurance',
                'Australia',
                'Canada',
            ),
            'meta' => array(
                'title' => 'Visitors insurance for USA, Visitor health insurance for USA',
                'keywords' => 'Visitors insurance for USA, Visitor health insurance for USA',
                'description' => 'visitors insurance for parents from india Visitors insurance for USA, Visitor health insurance for USA, visitors insurance for indian parents.',
            ),

        ),
        'real-estate' => array(
            'list' => array(
                'New Independent House',
                'New Apartment',
                'Used Independent House',
                'Used Apartment',
                'Flats in Hyderabad',
                'Flats-Other Places',
            ),
            'meta' => array(
                'title' => 'Trusted Real Estate Advisor| Real Estate Consulting Service',
                'keywords' => 'Trusted Real Estate Advisor, Real Estate Consulting Service',
                'description' => 'We are the Trusted Real Estate Advisor and Real Estate Consulting Service in Hyderabad, USA and from Usa to India Real Estate Consulting Services.',
            ),

        ),
        'summer-enrichment' => array(
            'list' => array(
                'Vocal',
                'Dance Classical (Kuchipudi or Bharatanatyam or Bollywood)',
                'Guitar',
                'Piano',
                'Violin',
                'Chess',
                'Drums',
            ),
            'meta' => array(
                'title' => 'Summer Enrichment Program 2019 Vdesiconnect',
                'keywords' => 'Summer Enrichment Program 2019',
                'description' => 'Vdesiconnect deals with Summer Enrichment Program 2019 and findout the best summer enrichment courcess in USA.',
            ),

        ),
        'web-developement' => array(
            'list' => array(
                'SEO',
                'Web Developement',
            ),
            'meta' => array(
                'title' => 'Web Development Hyderabad India, USA, Australia',
                'keywords' => 'Web Development Hyderabad India, USA, Australia',
                'description' => 'Web Development Hyderabad India, USA, Australia',
            ),

        ),
        'event-organization' => array(
            'list' => array(
                'Wedding',
                'Birthday',
                'Corporate Events',
                'Annual Celebrations',
            ),
            'meta' => array(
                'title' => 'Corporate Event Organization Company',
                'keywords' => 'Corporate Event Organization Company',
                'description' => 'Corporate Event Organization Company',
            ),

        ),
        'online-radio' => array('list' => array(),
            'meta' => array(
                'title' => ' The Best Internet Radio Stations',
                'keywords' => 'The Best Internet Radio Stations',
                'description' => 'The Best Internet Radio Stations',
            ),),
        'international-courier' => array(
            'list' => array(
                'India to USA',
                'India to Canada',
                'India to Australia',
                'India to Singapore',
            ),
            'meta' => array(
                'title' => 'International Courier Services| International Shipping, Parcel Delivery',
                'keywords' => 'International Courier Services, International Shipping, Parcel Delivery',
                'description' => 'International Courier Services, International Shipping, Parcel Delivery',
            ),

        ),
        'franchise-enquiry' => array(
            'list' => array(),
            'meta' => array(
                'title' => 'Vdesiconnect Franchise| Vdesiconnect Business Partner',
                'keywords' => 'Vdesiconnect Franchise, Vdesiconnect Business Partner',
                'description' => 'Vdesiconnect Franchise, Vdesiconnect Business Partner',
            ),
        ),
    );


    if ($name != '' && $name != 'all') {
        $returndata = $list[$name];
    } else {
        $returndata = array_keys($list);
    }


    return $returndata;
}


function preferTimeings()
{
    return array(
        '1 AM TO 3 AM',
        '3 AM TO 6 AM',
        '6 AM TO 9 AM',
        '9 AM TO 12 PM',
        '12 PM TO 3 PM',
        '3 PM TO 6 PM',
        '6 PM TO 9 PM',
        '9 PM TO 12 PM',
    );
}

function orderItemStatus($status)
{

//    $orderItems->oitem_status
    ?>
    <div class="col-md-12">
        <div class="progress">
            <?php
            switch ($status) {
                case('Shipped'):
                    ?>
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                         style="width:25%">
                        Payment Received
                    </div>
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning"
                         style="width:25%">
                        <?= $status ?>
                    </div>
                    <?php
                    break;
                case('OutForDelivery'):
                    ?>
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                         style="width:25%">
                        Payment Received
                    </div>
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning"
                         style="width:25%">
                        Shipped
                    </div>
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info"
                         style="width:25%">
                        <?= $status ?>
                    </div>
                    <?php
                    break;
                case('Delivered'):
                    ?>
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                         style="width:25%">
                        Payment Received
                    </div>
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning"
                         style="width:25%">
                        Shipped
                    </div>

                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info"
                         style="width:25%">
                        Out For Delivery
                    </div>

                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                         style="width:25%">
                        <?= $status ?>
                    </div>
                    <?php
                    break;
                case('PendingPaymnet'):
                    ?>
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                         style="width:100%">
                        Failed Payment
                    </div>
                    <?php
                    break;

                default:
                    ?>
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                         style="width:25%">
                        Placed
                    </div>
                    <?php
                    break;
                    ?>


                <?php
            }
            ?>
        </div>
    </div>
    <?php
}


function currencySymbol($type, $dd = null)
{
    if ($type == 'USD') {
        return '$';
    } elseif ($type == 'INR') {
        if ($dd != '') {
            return " &#8377; ";

        } else {
            return "<span class='currency_symbol'> &#8377; </span> ";

        }

    } else {
        return '$';
    }

}


function usdPriceINR()
{
    $currency_usd_to_inr = vdcSettings('currency_usd_to_inr');

    if (isset($currency_usd_to_inr) && $currency_usd_to_inr != '') {

    } else {
        $currency_usd_to_inr = 67.09;
    }


    return $currency_usd_to_inr;
}


function roundUp($number, $nearest)
{

    if ($number == 0) {
        return $number;
    } else {

        return $number + ($nearest - fmod($number, $nearest));
    }

}


function productWightFarmart($options, $calc = null)
{

    if (!empty($options) && strpos(strtolower($options), 'gram') !== false) {

        if ($calc != '') {
            $optionsinfo = ($options);
//            dd($optionsinfo);
            $options = (int)($options);
            $weight = number_format($options / 1000, 2);

            $optionsinfo = trim(preg_replace('/[^a-z]/', '', $optionsinfo));

//            return array('weight'=>$weight, 'type'=> $optionsinfo);
            return array('weight' => $weight, 'type' => 'Kgs');
        } else {
            return "";
        }

    } else {

//        dd(array('weight'=>$options, 'type'=> 'Kgs'));
        return array('weight' => $options, 'type' => 'Kgs');
//        return "Kgs";
    }


//    return $returnData;
}


function curencyConvert($type, $price)
{

//    dd(usdPriceINR());

    $returnData = '0';
    if ($price != null) {
        if ($type == 'USD') {

//            dd($price);

            $returnData = $price / usdPriceINR();
            $returnData = roundUp($returnData, 0.5);
        } else if ($type == 'INR') {
            $returnData = $price;
        }
    }
    return $returnData;
}


function getCategory($id = '')
{

    $list = '';

    if ($id != '') {

        $list = \App\Models\Categories::findOrFail($id);
    } else {
//        \Illuminate\Support\Facades\DB::enableQueryLog();
        $list = \App\Models\Categories::with(['getSubCategoryTypes' => function ($query) {
            $query->where('category_status', 'active');
        }, 'getSubCategoryOthers' => function ($query) {
            $query->where('category_status', 'active');
        }])->where('category_parent_id', 0)->where('category_status', 'active')->get();
//dd(\Illuminate\Support\Facades\DB::getQueryLog());

    }

    return $list;

}

function getImagesByProduct($id)
{
    $image_path = '/uploads/products/';
    $imagedata = \App\Models\ProductImages::where('pi_product_id', $id)->get();


    $images_fullpath = array();
    if (count($imagedata) > 0) {
        foreach ($imagedata AS $imageItem) {
            if ($imageItem->pi_image_name != '') {
                $images_fullpath[] = $image_path . $imageItem->pi_image_name;
            }

        }
    }

    if (count($images_fullpath) > 0) {
        return $images_fullpath;
    } else {
        return '';
    }


    dump($imagedata);
}


function discounttype($type)
{
    if ($type == 'percentage') {
        return '%';
    } elseif ($type == 'fixed') {
        return 'fixed';
    }
}


function vendorDashbordAccountStatus($status)
{
    switch ($status) {
        case 'pending':
            return '<span class="label-red position-absolute">' . $status . '</span>';
            break;
        case 'reject':
            return '<span class="label-red position-absolute">' . $status . '</span>';
            break;
        case 'approved':
            return '<span class="label-green position-absolute">' . $status . '</span>';
            break;
        default:
            return '<span class="label-red position-absolute">Pending</span>';
            break;
    }
}

function vendorDashbordBusinessStatus($status)
{
    switch ($status) {
        case 'pending':
            return '<span class="label-red position-absolute">' . $status . '</span>';
            break;
        case 'reject':
            return '<span class="label-red position-absolute">' . $status . '</span>';
            break;
        case 'approved':
            return '<span class="label-green position-absolute">' . $status . '</span>';
            break;
        default:
            return '<span class="label-red position-absolute">Pending</span>';
            break;
    }
}


function productStatus($id)
{
    switch ($id) {
        case '1':
            return 'Approved';
            break;
        case '2':
            return 'Rejected';
            break;
        case '0':
            return 'Pending Approval';
            break;
    }
}


function getImagesById($id)
{
    $image_path = '/uploads/products/';
    $imagedata = \App\Models\ProductImages::where('pi_id', $id)->get();


    $images_fullpath = array();
    if (count($imagedata) > 0) {
        foreach ($imagedata AS $imageItem) {
            if ($imageItem->pi_image_name != '') {
                $images_fullpath[] = $image_path . $imageItem->pi_image_name;
            }

        }
    }

    if (count($images_fullpath) == 1) {
        return $images_fullpath[0];
    } else {
        return $images_fullpath;
    }


}


function allStatuses($module, $status = null)
{
    $return_data = "";
    $displayStatus =
        array(
            "general" => array('active' => 'Active', 'inactive' => 'Inactive'),
            "order" => array('Pending' => 'Pending', 'Completed' => 'Completed', 'Cancel' => 'Cancel'),
        );

    if ($module && $status) {


        $return_data = $displayStatus[$module][$status];
    } else {
        $return_data = $displayStatus[$module];
    }
    return $return_data;

}


function categoryShipped($id = '')
{
    $return_data = "";
    $displayStatus = array(
        0 => 'All Countries',
        1 => 'Only India',
        2 => 'Out of India',
    );

    if ($id !== '') {
        if (isset($displayStatus[$id])) {
            $return_data = $displayStatus[$id];
        } else {
            $return_data = '--';
        }


    } else {
        $return_data = $displayStatus;
    }

//    dd($return_data);


    return $return_data;

}

function couponDiscountTypes($type = null)
{
    $return_data = "";
    $displayStatus = array(
        'Percentage' => 'Percentage',
        'Fixed' => 'Fixed'
    );

    if ($type) {
        $return_data = $displayStatus[$type];
    } else {
        $return_data = $displayStatus;
    }
    return $return_data;

}

function couponUsageTypes($type = null)
{
    $return_data = "";
    $displayStatus = array('Once' => 'Once', 'Multi' => 'Multi');

    if ($type) {
        $return_data = $displayStatus[$type];
    } else {
        $return_data = $displayStatus;
    }
    return $return_data;

}

function getBreadcrumbs($parameters, $title, $url = null)
{
    if ($url) {
        $img = $url;
    } else {
        $img = '/plugins/images/heading-title-bg.jpg';
    }


    ?>

    <?php


    $html = '<div class="breadcrumb-holder">
        <div class="container-fluid">

                    <ul class="breadcrumb ">';
    foreach ($parameters as $key => $value) {
        if (is_array($value)) {

            $route = $key;
            $name = '';
            $options = array();

            foreach ($value as $akey => $avalue) {
                if ($akey == 'name') {
                    $name = $avalue;
                } else {
                    $options = $avalue;
                }
            }

            $url = route($route, $options);

            if (!next($parameters)) {
                $html .= '<li class="breadcrumb-item active">' . $name . '</li>';

            } else {
                $html .= '<li class="breadcrumb-item"><a href=' . $url . '>' . $name . '</a></li>';
            }

        } else {
            if (!next($parameters)) {
                $html .= '<li class="breadcrumb-item active">' . $value . '</li>';
            } else {
                $url = route($key);
                $html .= '<li class="breadcrumb-item"><a href=' . $url . '>' . $value . '</a></li>';
            }
        }
    }
    $html .= '</ul>

        </div>
    </div>';
    return $html;
}


function snedSMS($mobile, $message, $county = 91, $ruote = 'SKVAPE', $authkey = '248828AyDlWFTTn95bf8f4b8')
{
// {{snedSMS(9848090537, 'Hi test mesasage By developer \n\n\n By skvapee.com')}}

    $mobile = $mobile;
    $message = str_replace('\n', '%0a', $message);
    $message = str_replace('\r', '%0a', $message);
    $message = str_replace('<br/>', '%0a', $message);
    $message = str_replace('<br />', '%0a', $message);
    $message = urlencode($message);

    $response = '';
    if (is_array($mobile)) {
        if (is_numeric($mobile)) {
            $response = \Ixudra\Curl\Facades\Curl::to('http://api.msg91.com/api/sendhttp.php?country=' . $county . '&sender=' . $ruote . '&route=4&mobiles=' . implode(',', $mobile) . '&authkey=' . $authkey . '&message=' . $message . '&unicode=1')->get();
        }
    } else {
        if (is_numeric($mobile)) {
            $response = \Ixudra\Curl\Facades\Curl::to('http://api.msg91.com/api/sendhttp.php?country=' . $county . '&sender=' . $ruote . '&route=4&mobiles=' . $mobile . '&authkey=' . $authkey . '&message=' . $message . '&unicode=1')->get();
        }
    }
    return $response;
}

function gender($id = null)
{
    $results = array();
    $list = array(
        'Male' => 'Male',
        'Female' => 'Female'
    );
    if ($id != '') {
        $results = $list[$id];
    } else {
        $results = $list;
    }
    return $results;

}

function productDescPosottion($id = null)
{

    $results = array();
    $list = array(
        '1' => 'Top',
        '2' => 'Left',
        '3' => 'Right',
        '4' => 'Bottom'
    );
    if ($id != '') {
        $results = $list[$id];
    } else {
        $results = $list;
    }
    return $results;

}

function productInftroTheme($arraydata)
{
    $returnarray = '';
    if (count($arraydata) > 0) {
        foreach ($arraydata AS $arrayItam) {

            switch ($arrayItam->pe_position) {
                case '1':

                    $returnarray .= '<div class="py-4">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8 text-center">
                            <h2 class="h3 fmed">' . $arrayItam->pe_title . '</h2>
                            <p class="py-3">' . $arrayItam->pe_description . '</p>
                        </div>
                        <div class="col-lg-12 text-center">
                            <figure class="py-5">
                                <img src="/uploads/products/' . $arrayItam->pe_image . '" class="img-fluid" alt="" title="">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>';
                    break;

                case '2':
                    $returnarray .= '<div class="graybg reviewpad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 align-self-center">
                            <h3 class="reviewsectitle pb-2">' . $arrayItam->pe_title . '</h3>
                            <p>' . $arrayItam->pe_description . '</p>
                        </div>
                        <div class="col-lg-6">
                            <img src="/uploads/products/' . $arrayItam->pe_image . '" class="img-fluid" alt="" title="">
                        </div>
                    </div>
                </div>
            </div>';

                    break;
                case '3':

                    $returnarray .= '<div class="reviewpad">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 align-self-center order-last">
                            <p>' . $arrayItam->pe_description . '</p>
                        </div>
                        <div class="col-lg-6">
                            <img src="/uploads/products/' . $arrayItam->pe_image . '" class="img-fluid" alt="" title="">
                        </div>
                    </div>
                </div>
            </div>';

                    break;

                case '4':

                    $returnarray .= '<div class="graybg reviewpadtop">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-12 text-center">
                            <h2 class="h3 fmed pb-5">' . $arrayItam->pe_description . '</h2>
                        </div>
                        <div class="col-lg-12 text-center">
                            <figure class="pt-5">
                                <img src="/uploads/products/' . $arrayItam->pe_image . '" class="img-fluid" alt="" title="">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>';

                    break;

                default:

                    $returnarray .= '<div class="graybg reviewpadtop">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-12 text-center">
                            <h2 class="h3 fmed pb-5">' . $arrayItam->pe_description . '</h2>
                        </div>
                        <div class="col-lg-12 text-center">
                            <figure class="pt-5">
                                <img src="/uploads/products/' . $arrayItam->pe_image . '" class="img-fluid" alt="" title="">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>';

                    break;
            }
        }
    }

    return $returnarray;
}

function g_print($array)
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}


function javascriptFunctions()
{
    ?>
    <script type="text/javascript">
        function discountCalcWithParent(parent, original, type, discount, discountValue, add = null) {

            var p_regular_price = $(parent + ' ' + original).val();
            var discount_type = $(parent + ' ' + type).val();
            var p_vdc_vendor_discount = $(parent + ' ' + discount).val();


            var percentage;

            if (discount_type == 'fixed') {

                if (add) {
                    percentage = (parseInt(p_regular_price) + parseInt(p_vdc_vendor_discount));
                } else {
                    percentage = (parseInt(p_regular_price) - parseInt(p_vdc_vendor_discount));
                }


            } else if (discount_type == 'percentage') {
                var p_vdc_vendor_discount_price = (p_regular_price / 100 * p_vdc_vendor_discount);

                if (add) {
                    percentage = (parseInt(p_regular_price) + parseInt(p_vdc_vendor_discount_price));
                } else {
                    percentage = (parseInt(p_regular_price) + -parseInt(p_vdc_vendor_discount_price));
                }

            }

            $(parent + ' ' + discountValue).val(percentage);

            console.log(discount_type + ' - discountCalc- ' + percentage + 'p_regular_price ' + p_regular_price + 'p_vdc_vendor_discount ' + p_vdc_vendor_discount);

            // console.log(p_regular_price + " <> " + discount_type + " <> " + p_vdc_vendor_discount + " <> " + percentage);


        }


        function discountCalc(original, type, discount, discountValue) {

            var p_regular_price = $(original).val();
            var discount_type = $(type).val();
            var p_vdc_vendor_discount = $(discount).val();


            var percentage;

            if (discount_type == 'fixed') {

                percentage = (p_regular_price - p_vdc_vendor_discount);


            } else if (discount_type == 'percentage') {
                var p_vdc_vendor_discount_price = (p_regular_price / 100 * p_vdc_vendor_discount);
                percentage = (p_regular_price - p_vdc_vendor_discount_price);
            }

            $(discountValue).val(percentage);


        }

        function imageUploadValidation(input, limit) {
            console.log(limit);

            $(input).on("change", function () {
                if ($(input)[0].files.length > limit) {
                    alert("Select Maximum 5 Images");
                    // alert("Select Maximum " + limit + " Images");
                    $(input).val('');
                } else {

                }
            });
        }

    </script>
    <?php
}


function skuTypes()
{
    return [
        'weight',
        'color',
        'quantity',
        'Size'
    ];
}


function product_skus($action, $skues, $id = null)
{
    $array_return = array();

    if ($action = 'sort') {
        if (count($skues) > 0) {
            foreach ($skues AS $sku) {
//                    dd($sku);

                $array_return[$sku['sku_type']][$sku['sku_id']] = $sku;

            }
        }


        return $array_return;

    }


}


function getProduct_Price_calc($sortingSkus, $getparams)
{

    $return_array = array();

    if (count($getparams) > 0) {
        foreach ($getparams AS $getparamKye => $getparam) {
//                dump($sortingSkus);
//                dump($getparamKye);
            if (array_key_exists($getparamKye, $sortingSkus)) {

                if (isset($sortingSkus[$getparamKye][$getparam])) {
                    $finaPriceData = $sortingSkus[$getparamKye][$getparam];


                    $return_array['vdcPrice'][] = $finaPriceData->sku_vdc_final_price;
                    $return_array['storePrice'][] = $finaPriceData->sku_store_price;
                } else {

//                        dd($sortingSkus);

                    $finaPriceData = array_first($sortingSkus[$getparamKye]);


                    $return_array['vdcPrice'][] = $finaPriceData->sku_vdc_final_price;
                    $return_array['storePrice'][] = $finaPriceData->sku_store_price;

                }


//                    dump($sortingSkus[$getparamKye][$getparam]);

            }
        }
    } else {
        if (count($sortingSkus) > 0) {
            foreach ($sortingSkus AS $sortingSku) {
//                    dump($sortingSku);


                $finaPriceData = array_first($sortingSku);


                $return_array['vdcPrice'][] = $finaPriceData->sku_vdc_final_price;
                $return_array['storePrice'][] = $finaPriceData->sku_store_price;

            }
        }

    }


    $return_array['vdcPrice'] = array_sum($return_array['vdcPrice']);
    $return_array['storePrice'] = array_sum($return_array['storePrice']);


    return $return_array;
}


function catSubSortByTypes($catid, $alias = null)
{
    $returndata = '';

    if ($alias == 'alias') {
        switch ($catid) {
            case '':
                break;
            default:
                break;
        }
    } else {

        switch ($catid) {
            case '117':
                $returndata = "Home Made Sweets";

                break;

//            case '6':
//                $returndata = "By millers";
//
//                break;
            default:
                $returndata = "By Type";

                break;
        }
    }

    return $returndata;


}

function catSubSortNames($catid, $alias = null)
{

    $returndata = '';

    if ($alias == 'alias') {
        switch ($catid) {
            case '':
                break;
            default:
                break;
        }
    } else {

        switch ($catid) {
            case '1':
                $returndata = "By Flavour";

                break;
            case '2':
            case '3':
                $returndata = "By Recipient";

                break;
            case '4':
                $returndata = "By Occasion";

                break;

            case '5':
                $returndata = "Artificial Jewellery";

                break;

            case '117':
                $returndata = "Amma Chethi Vanta";

                break;

//            case '6':
//                $returndata = "By millers";
//
//                break;
            default:
                $returndata = "By Others";

                break;
        }
    }

    return $returndata;

}


function formValidationValue($inputname, $value = null)
{

    if ($value != null) {
        echo !empty(old($inputname)) ? old($inputname) : ((($value) && ($value->$inputname)) ? $value->$inputname : '');

    } else {

        echo !empty(old($inputname)) ? old($inputname) : '';
    }


}

function formValidationError($errors, $inputname)
{
    if ($errors->has($inputname)) {
        ?>
        <span class="invalid-feedback" role="alert">
           <strong>
               <?php
               echo $errors->first($inputname)
               ?>
            </strong>
        </span>
        <?php
    }
}


function laravelReturnMessageShow()
{
    if (Session::has('flash_message')) {
        ?>
        <br/>
        <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><?= Session::get('flash_message') ?></strong>
        </div>
        <?php
    }

    if (Session::has('flash_message_error')) {
        ?>

        <br/>
        <div class="alert alert-danger alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><?= Session::get('flash_message_error') ?></strong>
        </div>

        <?php
    }

}


function shippingPriceList($country)
{
    $returnAmount = null;

    $countryInfo = \App\Models\Countries::where('country_id', $country)->first();
    if ($countryInfo->country_weight_description) {
        $returnAmount = splitPrice($countryInfo->country_weight_description);
    }

    return $returnAmount;
}

function caliclateShippingAmount($weight, $country)
{

    $returnAmount = '';

    $countryInfo = \App\Models\Countries::where('country_id', $country)->first();
    if ($countryInfo->country_weight_description) {
        $returnAmount = packagePriseSort($countryInfo->country_weight_description, $weight);
    } else {
        $returnAmount = 0;
    }


    $returnAmount = curencyConvert(getCurency(), $returnAmount);

    return $returnAmount;


}

function sortProductOption($options, $option = null)
{
    $splitPriceData = explode(',', $options);

    $splitPriceDataList = array();

    if (count($splitPriceData) > 0) {
        foreach ($splitPriceData AS $splitPriceItem) {

            $otiponsInfo = explode('/', $splitPriceItem);

            if ($option == 'weight') {

                $splitPriceDataList[$otiponsInfo[0]] = $otiponsInfo[1];
//                $splitPriceDataList[$otiponsInfo[0]] = $otiponsInfo[1];
            } else {

                $splitPriceDataList[$otiponsInfo[0]] = $otiponsInfo[1];
            }

        }
    }


    if (isset($splitPriceDataList[$option])) {
        return $splitPriceDataList[$option];
    } else {
        return 0;
    }
}

function strSkuToGetSkuId($strSku)
{
    $returnArray = array();


    $arraySplit = explode('/', $strSku);
    if(count($arraySplit) == 3){
        $returnArray = $arraySplit[2];
    }else{
        $returnArray = null;
    }


//    dd($arraySplit);


    return $returnArray;
}

function sortProductOptions($options)
{


    $returnData = array();


    if (is_array($options)) {

        $optionslists = $options;
    } else {

        $optionslists = explode(',', $options);
    }


    $optionslistsFinal = array();

    if (count(array_filter($optionslists)) > 0) {
        foreach ($optionslists AS $optionslist) {


            $optionArray = explode('/', $optionslist);

            list($optName, $optval, $optID) = $optionArray;

//            dump($optName);

            if ($optName == 'weight') {
//                $optionslistsFinal[$optName] = $optval . 'Kgs';
//                dump($optval);
//                dd(productWightFarmart($optval));

                if (empty(productWightFarmart($optval))) {
                    $optionslistsFinal[$optName] = $optval . productWightFarmart($optval);
                } else {
//                    dd(productWightFarmart($optval));
                    $optionslistsFinal[$optName] = productWightFarmart($optval)['weight'] . productWightFarmart($optval)['type'];
//                    $optionslistsFinal[$optName] = $optval.productWightFarmart($optval)['weight'];

                }


            } else {
                $optionslistsFinal[$optName] = $optval;
            }


        }
    }

//    dd($optionslistsFinal);

    $returnData = http_build_query($optionslistsFinal, ' ', ', ');

    $returnData = str_replace('=', ': ', $returnData);
    $returnData = str_replace('+', ' ', $returnData);

    return $returnData;

}

function PayapPaymentStatusShow($status)
{
    $returnData = '';

    switch ($status) {
        case 'approved':
            $returnData = "<span class='greencolor'> Payment Completed </span>";
            break;
        default:
            $returnData = "<span class='redcolor'> Payment Failed</span>";
            break;
    }

    return $returnData;
}

function OrderPaymentStatus($status)
{
    $returnData = '';

    switch ($status) {
        case 'approved':
            $returnData = "Paid";
            break;
        default:
            $returnData = "";
            break;
    }

    return $returnData;
}

function OrderStatus($status)
{
    $returnData = '';

    switch ($status) {
        case 'PendingPayment':
            $returnData = "<span class='redcolor'> Payment Failed </span>";
            break;
        case 'Paid':
            $returnData = "<span class='redcolor'> Placed </span>";
            break;
        default:
            $returnData = "<span class='greencolor'> $status </span>";
            break;
    }

    return $returnData;
}

function serviceInvoiceStatus($status)
{
    $returnData = '';

    switch ($status) {
        case 'approved':
            $returnData = "<span class='redcolor'> Paid </span>";
            break;
        case 'pending':
            $returnData = "<span class='redcolor'> Requested </span>";
            break;
        default:
            $returnData = "<span class='greencolor'> New </span>";
            break;
    }

    return $returnData;
}


//ALL SMS 9032615531
define('MSG_ADMIN_NUMBER', '+919032615531');
//define('MSG_ADMIN_NUMBER', '8977111142');
define('MSG_USER_REGISTER_NOTFICATION_ADMIN', '{USER_NAME} Registered with you as User');
define('MSG_USER_REGISTER_NOTFICATION_USER', 'Dear Vdesiconnect Customer, you have registered successfully. Please check your email for verification link.');


define('MSG_VENDOR_REGISTER_NOTFICATION_ADMIN', '{USER_NAME} Registered with you as Vdesiconnect Vendor');
define('MSG_VENDOR_REGISTER_NOTFICATION_VENDOR', 'Dear Vdesiconnect Vendor, you have registered successfully. Please check your email for verification link.');


define('MSG_SERVICE_REQUEST_ADMIN', 'Dear Vdesiconnect Admin, You received a "{SERVICE_NAME}" enquiry from "{USER_NAME}". Please review the request.');
define('MSG_SERVICE_REQUEST_USER', 'Dear Vdesiconnect Customer, we received enquiry for "{SERVICE_NAME}". We will review and get back to you soon.');

define('MSG_SERVICE_INVOICE_CREATE_USER', 'Dear Vdesiconnect Customer, you have received incovice for {SERVICE_NAME}. Please login to vdeisconnect.com to make payment.');

define('MSG_PRODUCT_ENQUERY_ACCEPT_USER', 'Dear Vdesiconnect Customer, your enquiry for "{PRODUCT_NAME}" is received. We have the product available please use this OTP {OTP} number to purchase. visit to to https://www.vdesiconnect.com');
define('MSG_PRODUCT_ENQUERY_ACCEPT_VENDOR', 'Dear Vdesiconnect Vendor, you received an enquiry for "{PRODUCT_NAME}". Please check and notify Vdesiconnect Admin on the availability of the product.');
define('MSG_PRODUCT_ENQUERY_ACCEPT_ADMIN', 'Dear Vdesiconnect Admin, you received an enquiry by {USER_NAME} for "{PRODUCT_NAME}". Please check the availability with Vendor.');

define('MSG_PRODUCT_ENQUERY_REJECT_USER', 'Dear Vdesiconnect Customer, the "{PRODUCT_NAME}" you enquired is not available. Check Vdesiconnect.com for more products. ({ADMINMSG})');


define('MSG_CART_PAYMENT_DONE_USER', 'Dear Vdesiconnect Customer, your order for {PRODUCT_NAME} is successfully placed. Your Order id is #{ORDER_ID}. Login to vdesiconnect.com for checking the order status.');
define('MSG_CART_PAYMENT_DONE_VENDOR', 'Dear Vdesiconnect Vendor, you got an order for {PRODUCT_NAME}. Order id is #{ORDER_ID}. For more details please, login to https://www.vdesiconnect.com/store/login.');
define('MSG_CART_PAYMENT_DONE_ADMIN', 'Dear Vdesiconnect Admin, you got an order for {PRODUCT_NAME}. Order id is #{ORDER_ID}. For more details please, login to https://www.vdesiconnect.com/admin/login.');


//'https://api.msg91.com/api/sendhttp.php?mobiles=9642123254,7995165789&authkey=281207Ap4gZfODekY5d05c521&route=4&sender=VDESIC&message=Hello!%20This%20is%20a%20test%20message&country=91';

