<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Orders extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */

    public static $adminEmail = 'info@vdesiconnect.com';
//    public static $adminEmail = 'rajagonda@gmail.com';
//    public static $adminEmail = 'itsmesai09@gmail.com';


    protected $table = 'orders';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_user_id',
        'order_currency',
        'order_total_weight',
        'order_delivery_charge',

        'order_sub_total_price',
        'order_shipping_price',

        'order_coupon_code',
        'order_coupon_discount',
        'order_coupon_discount_type',
        'order_coupon_discount_amount',

        'order_total_price',

        'order_billing_address',
        'order_delivery_address',
        'order_delivery_country',
        'order_delivery_state',

        'order_reference_number',
        'order_status',
        'order_note',

        'order_payment_invoice_num',
        'order_payment_transaction_id',

        'order_payment_status',
        'order_payment_mode',
        'order_payment_date',
        'order_payment_response_dump',



    ];

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'order_id';

    public function orderItems()
    {
        return $this->hasMany(OrderItems::Class, 'oitem_order_id', 'order_id');
    }

    public function getUser()
    {
        return $this->belongsTo(User::Class, 'order_user_id', 'id');
    }

    public static function orderPlacedSuccessEmails($ord_id)
    {

        $ordersfirst = Orders::with('orderItems.getProduct.productImages', 'getUser')->where('order_id', $ord_id)->first();

        $userAddress = unserialize($ordersfirst->order_delivery_address);

        $data['orders'] = $ordersfirst;


//        dump($ordersfirst->order_id);


//
        //User Email

        try {


            Mail::send('emails_templates.cart.success.user', $data, function ($message) use ($userAddress) {

                $message->from(self::$adminEmail, 'vdesiconnect.com');
                $message->to($userAddress['ua_email']);
                $message->subject("Order Placed Successfully!");
            });

            //Admin Email

            Mail::send('emails_templates.cart.success.admin', $data, function ($message) use ($userAddress) {

                $message->from($userAddress['ua_email'], $userAddress['ua_name']);
                $message->to(self::$adminEmail);
                $message->subject($userAddress['ua_name'] . " placed new order successfully!");
            });


        }

        catch (\Exception $e) {
            //display custom message
//            echo $e->errorMessage();
        }



        //Vendor Emails

        $ordersItems = OrderItems::with('getProduct.productImages', 'getOrder', 'getProduct.getVendor.vendorAccount')->join('products', 'oitem_product_id', '=', 'products.p_id')->where('oitem_order_id', $ord_id)->get();


        foreach ($ordersItems as $ordersItem) {


            if (isset($ordersItem->getProduct->getVendor->vendorAccount)) {


                $vendorMobile = $ordersItem->getProduct->getVendor->vendorAccount->va_mobile;
                $productname = $ordersItem->getProduct->p_name;


                $variables = array(
                    '{PRODUCT_NAME}' => $productname,
                    '{ORDER_ID}' => $ordersfirst->order_id
                );

                if (isset($vendorMobile) && $vendorMobile != '') {

                    $MSG_CART_PAYMENT_DONE_VENDOR = strtr(MSG_CART_PAYMENT_DONE_VENDOR, $variables);
                    sendSms($vendorMobile, trim($MSG_CART_PAYMENT_DONE_VENDOR));
                }


                $MSG_CART_PAYMENT_DONE_ADMIN = strtr(MSG_CART_PAYMENT_DONE_ADMIN, $variables);
                sendSms(MSG_ADMIN_NUMBER, trim($MSG_CART_PAYMENT_DONE_ADMIN));


                $sortmobile = explode('-', $ordersfirst->getUser->mobile);
                $userMobile = $sortmobile[1];

                $variables = array(
                    '{PRODUCT_NAME}' => $productname,
                    '{ORDER_ID}' => $ordersfirst->order_id
                );

                $MSG_CART_PAYMENT_DONE_USER = strtr(MSG_CART_PAYMENT_DONE_USER, $variables);
                sendSms($userMobile, trim($MSG_CART_PAYMENT_DONE_USER), $sortmobile[0]);


            }

//            dump($ordersItem->getProduct->getVendor);


            $data['orders'] = $ordersItem;

            $userAddress = unserialize($ordersItem->getOrder->order_delivery_address);

            try {
                Mail::send('emails_templates.cart.success.vendor', $data, function ($message) use ($userAddress, $ordersItem) {
                    $message->from($userAddress['ua_email'], $userAddress['ua_name']);
                    $message->to($ordersItem->getProduct->getVendor->email);
                    $message->subject($userAddress['ua_name'] . " placed new order successfully!");
                });
            }

            catch (\Exception $e) {
                //display custom message
//                echo $e->errorMessage();
            }




        }


//        dd($ordersItems);


//        dd($ord_id);

    }

    public static function orderPlacedFailedEmails($ord_id)
    {

        $ordersfirst = Orders::with('orderItems.getProduct.productImages', 'getUser')->where('order_id', $ord_id)->first();

        $userAddress = unserialize($ordersfirst->order_delivery_address);

        $data['orders'] = $ordersfirst;


        //User Email

        try {

            Mail::send('emails_templates.cart.success.user', $data, function ($message) use ($userAddress) {

                $message->from(self::$adminEmail, 'vdesiconnect.com');
                $message->to($userAddress['ua_email']);
                $message->subject("Your Order has been failed, once again try!");
            });

            //Admin Email

            Mail::send('emails_templates.cart.success.admin', $data, function ($message) use ($userAddress) {

                $message->from($userAddress['ua_email'], $userAddress['ua_name']);
                $message->to(self::$adminEmail);
                $message->subject($userAddress['ua_name'] . " placed new order, but order faild!");
            });
        }

        catch (\Exception $e) {
            //display custom message
//            echo $e->errorMessage();
        }


    }

}
