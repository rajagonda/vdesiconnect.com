<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class ProductImages extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_images';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'pi_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['pi_product_id', 'pi_image_name', 'pi_status'];


    public static function removeImage($imgId)
    {



        $images = ProductImages::where('pi_id', $imgId)->first();

//        dd($images);

        if (isset($images) && !empty($images)) {
            ProductImages::destroy($imgId);
            File::delete('uploads/products/' . $images->pi_image_name);
            File::delete('uploads/products/thumbs/' . $images->pi_image_name);
        }
        return true;
    }

}
