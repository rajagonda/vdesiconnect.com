<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Ratings extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ratings';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'rt_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['rt_user_id', 'rt_product_id', 'rt_rating', 'rt_comment'];

    public function getUser()
    {
        return $this->belongsTo(User::Class, 'rt_user_id', 'id');
    }

    public function getProduct()
    {
        return $this->belongsTo(Products::Class, 'rt_product_id');
    }
}
