<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmmachethivantaOrderItems extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ammachethivanta_order_items';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'acoitem_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['acoitem_order_id',
        'acoitem_product_id',
        'acoitem_weight',
        'acoitem_product_price'
    ];

    public function getProduct()
    {
        return $this->belongsTo(Ammachethivanta::Class, 'acoitem_product_id');
    }

    public function getOrder()
    {
        return $this->belongsTo(AmmachethivantaOrders::Class, 'acoitem_order_id');
    }
}
