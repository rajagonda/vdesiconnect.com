<?php

namespace App\Models;

use App\ServiceInvoice;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'services';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 's_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        's_contact_person',
        's_address_line_1',
        's_address_line_2',
        's_country',
        's_state',
        's_city',
        's_options',
        's_date',
        's_time',
        's_email',
        's_phone',
        's_msg',
        's_user_id',
        's_type',
        's_duration',
        's_duedate',
        's_no_of_days',
        's_unit_price',
        's_unit_type',
        's_sub_total',
        's_tax',
        's_total',
        's_status',
        's_payment_transaction_id',
        's_payment_invoice_num',
        's_payment_payer',
        's_payment_transactions'
    ];


    public function getUser()
    {
        return $this->belongsTo(User::Class, 's_user_id', 'id');
    }



    public function getServiceInvoice()
    {
        return $this->hasMany(ServiceInvoice::Class, 'service_id','s_id');
    }

    public function getServiceDocuments()
    {
        return $this->hasMany(ServiceDocuments::Class, 'sd_service_id','s_id');
    }
}
