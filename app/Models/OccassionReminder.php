<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class OccassionReminder extends Model
{

    protected $table = 'occassion_reminder';


    protected $fillable = [
        'or_user_id',
        'or_name',
        'or_type',
        'or_date',
        'or_remind',
        'or_desc'
    ];


    protected $primaryKey = 'or_id';

    public function getUser()
    {
        return $this->belongsTo(User::Class, 'or_user_id', 'id');
    }



}
