<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'category_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['category_name',
        'category_alias',
        'category_image',
        'category_status',
        'category_order',
        'category_parent_id',
        'category_options',
        'category_shipped',
        'category_meta_title',
        'category_meta_keywords',
        'category_meta_description'];

    public function getProducts()
    {
        return $this->hasMany(Products::Class, 'p_cat_id', 'category_id');
    }


    public function getCategoryName()
    {
        return $this->belongsTo(Categories::Class, 'category_parent_id', 'category_id');

    }

    public function getSubCategoryTypes()
    {
        return $this->hasMany(Categories::Class, 'category_parent_id', 'category_id')->where('category_options', 'types');

    }

    public function getSubCategoryOthers()
    {
        return $this->hasMany(Categories::Class, 'category_parent_id', 'category_id')->where('category_options', 'others');

    }


    public static function categoryTypes($catid)
    {
        $types = Categories::where('category_status', 'active')
            ->where('category_id', $catid)
            ->where('category_options', 'types')
            ->orderBy('category_id', 'asc')
            ->lists('category_name', 'category_id');
        return json_encode($types);
    }

}
