<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class VendorAccount extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */

    public static $adminEmail = 'info@vdesiconnect.com';
    protected $table = 'vendorsAccount';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'va_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'va_vendor_id',
        'va_display_name',
        'va_business_desc',
        'va_address_1',
        'va_address_2',
        'va_state',
        'va_city',
        'va_pincode',
        'va_name',
        'va_mobile',
        'va_email',
        'va_time_from',
        'va_time_to',
        'va_status'
    ];

    public static function AccountDetails($vid)
    {

        $vendorData = VendorAccount::where('va_vendor_id', $vid)->first();
        $userData = User::where('id', $vid)->first();


//        AdminEmail

        $data['emailData'] = $vendorData;
        $data['userData'] = $userData;



        try {
            Mail::send('emails_templates.vendor.verify_account_details', $data, function ($message) use ($vendorData, $userData) {

                $message->from(self::$adminEmail, 'vdesiconnect.com');
                $message->to($userData->email);
                $message->subject($userData->name." updated account details, please verify!");
            });
        }

        catch (\Exception $e) {
            //display custom message
//            echo $e->errorMessage();
        }






//        dd($contactData);

    }



}
