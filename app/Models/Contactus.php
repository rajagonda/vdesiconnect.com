<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Contactus extends Model
{

    public static $adminEmail = 'info@vdesiconnect.com';


    protected $table = 'contactus';

    protected $primaryKey = 'cu_id';


    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'mobile_prefix',
        'phone_number',
        'subject',
        'message',
        'login_user_id'
    ];


    public static function contactUsEmails($conatct_id)
    {

        $contactData = Contactus::where('cu_id', $conatct_id)->first();


//        AdminEmail

        $data['emailData'] = $contactData;


        try {

            Mail::send('emails_templates.pages.contactus_for_user', $data, function ($message) use ($contactData) {

                $message->from(self::$adminEmail, 'vdesiconnect.com');
                $message->to($contactData->email);
                $message->subject("Thank you for contacting us!");
            });

//        userEmail

            Mail::send('emails_templates.pages.contactus_for_admin', $data, function ($message) use ($contactData) {

                $message->from($contactData->email, $contactData->first_name . ' ' . $contactData->last_name);
//            $message->to('rajagonda@gmail.com');
                $message->to(self::$adminEmail);
                //Add a subject
                $message->subject("Contact Form Message From " . $contactData->first_name . ' ' . $contactData->last_name);
            });


        }

        catch (\Exception $e) {
            //display custom message
//            echo $e->errorMessage();
        }



//        dd($contactData);

    }

}
