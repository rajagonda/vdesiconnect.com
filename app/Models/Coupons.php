<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupons extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'coupons';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'coupon_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['coupon_code', 'coupon_discount_value', 'coupon_discount_type', 'coupon_quantity', 'coupon_country','coupon_usage_type','coupon_expiry_date','coupon_status'];

    public function getCountry()
    {
        return $this->belongsTo(Countries::Class, 'coupon_country');
    }
    public function getCoupon()
    {
        return $this->hasMany(CartCouponsUsage::Class, 'cu_coupan_code', 'coupon_code');
    }



}
