<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class SendEnquiry extends Model
{
    public static $adminEmail = 'info@vdesiconnect.com';


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'send_enquiries';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'se_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'se_name',
        'se_email',
        'se_phone',
        'se_message',
        'se_product_id',
        'se_user_id',
        'se_otp',
        'se_status',
        'se_reply_msg',
        'se_ip_address'
    ];

    public function getProduct()
    {
        return $this->belongsTo(Products::Class, 'se_product_id');
    }

    public function getUser()
    {
        return $this->belongsTo(User::Class, 'se_user_id', 'id');
    }


    public static function unAvalSendEnquiryEmail($enqid)
    {

        $SendEnquiryData = SendEnquiry::with('getUser', 'getProduct.getCategory')->where('se_id', $enqid)->get()->first();


        $adminEmail = self::$adminEmail;

        $data['emaildata'] = $SendEnquiryData;


        $sortmobile = explode('-', $SendEnquiryData->se_phone);
        $userMobile = $sortmobile[1];

        $variables = array(
            '{PRODUCT_NAME}' => $SendEnquiryData->getProduct->p_name,
            '{ADMINMSG}' => $SendEnquiryData->se_reply_msg
        );
        $MSG_PRODUCT_ENQUERY_REJECT_USER = strtr(MSG_PRODUCT_ENQUERY_REJECT_USER, $variables);
        sendSms($userMobile, trim($MSG_PRODUCT_ENQUERY_REJECT_USER), $sortmobile[0]);

        try {

            Mail::send('emails_templates.product_enquiries.admin_approve.product_enquiries_diseble', $data, function ($message) use ($SendEnquiryData, $adminEmail) {
                $userEmail = 'rajagonda@gmail.com';
//            $userEmail = $SendEnquiryData->se_email;

                $message->from($adminEmail, 'vdesiconnect.com');
                $message->to($userEmail);
                $message->subject("Product Availability Enquiry Reply ");
            });
        }

        catch (\Exception $e) {
            //display custom message
//            echo $e->errorMessage();
        }


    }

    public static function otpSendEnquiryEmail($enqid)
    {

        $SendEnquiryData = SendEnquiry::with('getUser', 'getProduct.getCategory', 'getProduct.getVendor.vendorAccount','getProduct.productImages')->where('se_id', $enqid)->get()->first();


//        dump($SendEnquiryData->getProduct->p_name);
//        dump($SendEnquiryData->getProduct->getVendor->vendorAccount->va_mobile);

        $variables = array(
            '{PRODUCT_NAME}' => $SendEnquiryData->getProduct->p_name,
            '{OTP}' => $SendEnquiryData->se_otp
        );


//        dd($SendEnquiryData);

//        if (isset($SendEnquiryData->getProduct->getVendor->vendorAccount)) {
//            $vendorSortmobile = $SendEnquiryData->getProduct->getVendor->vendorAccount->va_mobile;
//            $MSG_PRODUCT_ENQUERY_ACCEPT_VENDOR = strtr(MSG_PRODUCT_ENQUERY_ACCEPT_VENDOR, $variables);
//            sendSms($vendorSortmobile, trim($MSG_PRODUCT_ENQUERY_ACCEPT_VENDOR));
//        }


//        dump($vendorSortmobile);
//        dd($SendEnquiryData);


//        $MSG_PRODUCT_ENQUERY_ACCEPT_ADMIN = strtr(MSG_PRODUCT_ENQUERY_ACCEPT_ADMIN, $variables);
//        sendSms(MSG_ADMIN_NUMBER, trim($MSG_PRODUCT_ENQUERY_ACCEPT_ADMIN));




        $sortmobile = explode('-', $SendEnquiryData->se_phone);
        $userMobile = $sortmobile[1];
        $MSG_PRODUCT_ENQUERY_ACCEPT_USER = strtr(MSG_PRODUCT_ENQUERY_ACCEPT_USER, $variables);
        sendSms($userMobile, trim($MSG_PRODUCT_ENQUERY_ACCEPT_USER), $sortmobile[0]);


        $adminEmail = self::$adminEmail;

        $data['emaildata'] = $SendEnquiryData;


        try {

            Mail::send('emails_templates.product_enquiries.admin_approve.product_enquiries_user', $data, function ($message) use ($SendEnquiryData, $adminEmail) {
//            $userEmail = 'rajagonda@gmail.com';
                $userEmail = $SendEnquiryData->se_email;

                $message->from($adminEmail, 'vdesiconnect.com');
                $message->to($userEmail);
                $message->subject("Product Availability OTP");
            });
        }

        catch (\Exception $e) {
            //display custom message
//            echo $e->errorMessage();
        }




    }






    public static function newSendEnquiryEmail($enqid)
    {
        $enqData = SendEnquiry::with('getUser', 'getProduct.getCategory', 'getProduct.getVendor.vendorAccount','getProduct.productImages')->where('se_id', $enqid)->get()->first();

        $adminEmail = self::$adminEmail;

        $data['emaildata'] = $enqData;

//        dd($enqData);

        $variables = array(
            '{PRODUCT_NAME}' => $enqData->getProduct->p_name,
            '{USER_NAME}' => $enqData->se_name
        );
 //tetest
        //send to Admin
        $MSG_PRODUCT_ENQUERY_ACCEPT_ADMIN = strtr(MSG_PRODUCT_ENQUERY_ACCEPT_ADMIN, $variables);
        sendSms(MSG_ADMIN_NUMBER, trim($MSG_PRODUCT_ENQUERY_ACCEPT_ADMIN));


        sendSms('+16462413704', trim($MSG_PRODUCT_ENQUERY_ACCEPT_ADMIN));
        sendSms('+18628127929', trim($MSG_PRODUCT_ENQUERY_ACCEPT_ADMIN));

        //send to vendor
//        if (isset($enqData->getProduct->getVendor->vendorAccount)) {
//            $vendorSortmobile = $enqData->getProduct->getVendor->vendorAccount->va_mobile;
//            $MSG_PRODUCT_ENQUERY_ACCEPT_VENDOR = strtr(MSG_PRODUCT_ENQUERY_ACCEPT_VENDOR, $variables);
//            sendSms($vendorSortmobile, trim($MSG_PRODUCT_ENQUERY_ACCEPT_VENDOR));
//        }







//        dd($image_url);

        try {

            Mail::send('emails_templates.product_enquiries.product_view_send.product_enquiries_admin', $data, function ($message) use ($enqData, $adminEmail) {
//            $userEmail = 'info@vdesiconnect.com';
                $userEmail = $enqData->se_email;

//            $message->from($adminEmail, 'vdesiconnect.com');
//            $message->to($userEmail);

                $message->from($userEmail, 'vdesiconnect.com');
                $message->to($adminEmail);
                $message->subject("Product Send Enquiry");
            });


            Mail::send('emails_templates.product_enquiries.product_view_send.product_enquiries_user', $data, function ($message) use ($enqData, $adminEmail) {
//            $userEmail = 'info@vdesiconnect.com';
                $userEmail = $enqData->se_email;

                $message->from($adminEmail, 'vdesiconnect.com');
                $message->to($userEmail);
                $message->subject("Thanks for Sending Product Availability Enquiry");
            });
        }

        catch (\Exception $e) {
            //display custom message
//            echo $e->errorMessage();
        }



    }

}
