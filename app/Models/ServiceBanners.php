<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceBanners extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'service_banners';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'banner_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['banner_title', 'banner_image','banner_status','banner_link'];
}
