<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CartCouponsUsage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cart_coupons_usage';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'cu_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['cu_user_id', 'cu_coupan_code', 'cu_order_id'];

    public function getUser()
    {
        return $this->belongsTo(User::Class, 'id', 'cu_user_id');
    }

    public function getCoupan()
    {
        return $this->belongsTo(Coupons::Class, 'coupon_code', 'cu_coupan_code');
    }

    public static function coupanUpdateCartOrder($code, $orderid)
    {

        if ($code != '') {
            $couponInfo = Coupons::where('coupon_code', $code)->first();

//            dd($couponInfo);

            if (!empty($couponInfo)) {
                $updateCoupon = array();
                $updateCoupon['coupon_quantity'] = ($couponInfo->coupon_quantity) - 1;
                $couponupdate = Coupons::findOrFail($couponInfo->coupon_id);
                $couponupdate->update($updateCoupon);

                $CartCouponsUsageData = array(
                    'cu_user_id' => Auth::id(),
                    'cu_order_id' => $orderid,
                    'cu_coupan_code' => $couponInfo->coupon_code
                );

                CartCouponsUsage::create($CartCouponsUsageData);

            }

            return 1;


        }

    }


}
