<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class Ammachethivanta extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ammachethivanta';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'ac_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['ac_name', 'ac_image', 'ac_description', 'ac_status'];

    public function getSKUs()
    {
        return $this->hasMany(AmmachethivantaSKUS::Class, 'as_ac_id', 'ac_id');
    }


    public static function getImage($imagename){
        $uploadPath = url('/uploads/ammachethivanta/');

//        dd($uploadPath.'/'.$imagename);

        return $uploadPath.'/'.$imagename;
    }


    public static function removeRecord($id)
    {
        $file = Ammachethivanta::findOrFail($id);

        if ($file->ac_image != '') {
            File::delete('uploads/ammachethivanta/' . $file->ac_image);
        }

//        DB::table('ammachethivanta_skus')->where('as_ac_id', $id)->delete();

        Ammachethivanta::destroy($id);

        return true;
    }

    public static function imageUpload($inputname, $oldImage = null)
    {
        $uploadPath = public_path('/uploads/ammachethivanta/');

        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }


        if ($oldImage != '') {
            if ($oldImage != '') {
                File::delete($uploadPath . $oldImage);
            }

        }


        $extension = $inputname->getClientOriginalName();
        $fileName = time() . $extension;
        $inputname->move($uploadPath, $fileName);

        return $fileName;
    }

}
