<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentInfo extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payment_info';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'pi_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['pi_vendor_id', 'pi_current_balance', 'pi_paying_amount', 'pi_balance_amount', 'pi_date'];
}
