<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Products extends Model
{
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */

    public static $adminEmail = 'info@vdesiconnect.com';
//    public static $adminEmail = 'gogikarsairam@gmail.com';


    protected $fillable = [
        'p_unique_id',
        'p_vendor_id',
        'p_name',
        'p_alias',

        'p_cat_id',
        'p_sub_cat_id',

        'p_item_spl',
        'p_main_image',
        'p_dishtype',
//        'p_weight',
//        'p_weight_type',
//        'p_color',
        'p_availability',

        'p_overview',
        'p_specifications',
        'p_quality_care_info',

//        'p_vdc_commission_type',
//        'p_vdc_commission',
//        'p_vdc_store_price',

//        'p_vdc_store_discount_type',
//        'p_vdc_store_discount',
//        'p_vdc_store_discount_price',

        'p_delivery_charges',
        'p_status',
        'p_related',
        'p_meta_title',
        'p_meta_keywords',
        'p_meta_description'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'p_id';


    public function getCategory()
    {
        return $this->belongsTo(Categories::Class, 'p_cat_id', 'category_id');
    }


    public function getSubCategory()
    {
        return $this->belongsTo(Categories::Class, 'p_sub_cat_id', 'category_id');
    }

    public function getSplCat()
    {
        return $this->belongsTo(Categories::Class, 'p_item_spl', 'category_id');
    }


    public function productImages()
    {
        return $this->hasMany(ProductImages::Class, 'pi_product_id', 'p_id');
    }

    public function productSKUs()
    {
        return $this->hasMany(ProductSkus::Class, 'sku_product_id', 'p_id');
    }

    public function getVendor()
    {
        return $this->belongsTo(User::Class, 'p_vendor_id', 'id');
    }

    public function productReviews()
    {
        return $this->hasMany(Ratings::Class, 'rt_product_id', 'p_id')->orderBy('rt_id', 'desc');
    }

    public static function fileUpload($fileinput, $product_id)
    {
        $uploadPath = public_path('/uploads/products/');

        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }

        $thumbPath = public_path('/uploads/products/thumbs/');

        if (!file_exists($thumbPath)) {
            mkdir($thumbPath, 0777, true);
        }
        $thumb_width = 400;
        $thumb_height = 400;
        $thumb_quality = 60;


        foreach ($fileinput as $file) {


            $extension = $file->getClientOriginalExtension();
            $fileName = time() .'_'. uniqid() . '.' . $extension;


            $thumb_img = \Intervention\Image\Facades\Image::make($file->getRealPath())->resize($thumb_width, $thumb_height, function ($constraint) {
                $constraint->aspectRatio();
            });
            $thumb_img->save($thumbPath . '/' . $fileName, $thumb_quality);

            $file->move($uploadPath, $fileName);

            $requestdata = array();
            $requestdata['pi_image_name'] = $fileName;
            $requestdata['pi_status'] = 'active';
            $requestdata['pi_product_id'] = $product_id;
            ProductImages::create($requestdata);
        }

    }

    public static function ProductEmail($proid)
    {

        $productData = Products::with('productImages')->where('p_id', $proid)->first();

        $userData = User::where('id', $productData->p_vendor_id)->first();


//        Vendor

        $data['emailData'] = $productData;
        $data['userData'] = $userData;


        try {
            Mail::send('emails_templates.product_approval.create_product_approval_vendor', $data, function ($message) use ($productData, $userData) {

                $message->from(self::$adminEmail, 'vdesiconnect.com');
                $message->to($userData->email);
                $message->subject("Thank you for creating product, we will verify soon you product!");
            });

//        Admin

            Mail::send('emails_templates.product_approval.create_product_approval_admin', $data, function ($message) use ($productData, $userData) {

                $message->from($userData->email, $userData->name);
//            $message->to('rajagonda@gmail.com');
                $message->to(self::$adminEmail);
                $message->to('vdesiconnect@gmail.com');
                //Add a subject
                $message->subject($userData->name . "(vendor) created new product, approval pending!");
            });


        }

        catch (\Exception $e) {
            //display custom message
//            echo $e->errorMessage();
        }


//        dd($contactData);

    }


    public static function createProduct($request)
    {


        $returnarray = array(
            'status' => 'start',
            'values' => '',

        );

        $requestData = $request->all();

        if ($requestData['p_id'] != '') {
//            $products = Products::findOrFail($requestData['p_id']);
            $products = Products::where('p_vendor_id', $requestData['p_vendor_id'])->findOrFail($requestData['p_id']);
        }


        if ($requestData['p_id'] == '') {

            $validatedData = $request->validate([
                'p_name' => 'required|unique:products',
//            'p_name' => 'required|unique:products,p_name,' . $id . ',p_id,',
                'p_cat_id' => 'required',
                'p_availability' => 'required',
            ], [
                'p_name.required' => 'Name is required',
                'p_name.unique' => 'This product name is already exist',
                'p_cat_id.required' => 'Name is required',
                'p_availability.required' => 'Name is required',
            ]);


//                dd($requestData);

            $requestData['p_alias'] = str_slug($requestData['p_name']);
            $requestData['p_vendor_id'] = $requestData['p_vendor_id'];
            $productData = Products::create($requestData);


            $product_id = $productData->p_id;


            $productUpdate['p_unique_id'] = 'VC-' . $product_id;


            $productInfo = Products::where('p_id', $product_id)->update($productUpdate);

            ProductSkus::skusInsert($requestData['product_options'], $product_id);
            if ($request->hasFile('productimages')) {
                Products::fileUpload($request['productimages'], $product_id);
            }


            Products::ProductEmail($product_id);


            $returnarray['status'] = 'created';
            $returnarray['values'] = $productData;


        } else {

            $validatedData = $request->validate([
//                'p_name' => 'required|unique:products',
                'p_name' => 'required|unique:products,p_name,' . $requestData['p_id'] . ',p_id',
                'p_cat_id' => 'required',
                'p_availability' => 'required',
            ], [
                'p_name.required' => 'Name is required',
                'p_name.unique' => 'This product name is already exist',
                'p_cat_id.required' => 'Name is required',
                'p_availability.required' => 'Name is required',
            ]);


            $requestData['p_sku'] = uniqid();
            $requestData['p_alias'] = str_slug($requestData['p_name']);
            $requestData['p_vendor_id'] = $requestData['p_vendor_id'];


            $products->update($requestData);


            $product_id = $requestData['p_id'];

            if ($request->hasFile('productimages')) {

                Products::fileUpload($request['productimages'], $product_id);


            }

            if (count($requestData['product_options']) > 0) {
                $skusList = ProductSkus::updateInSkusFilters($requestData['product_options']);
                $serverSkus = ProductSkus::where('sku_product_id', $product_id)->get()->toArray();
                if (isset($skusList['edit']) && count($skusList['edit']) > 0) {
                    ProductSkus::skusUpdate($skusList['edit'], $serverSkus, $product_id);
                }
                if (isset($skusList['new']) && count($skusList['new']) > 0) {
                    ProductSkus::skusInsert($skusList['new'], $product_id);
                }
            }


            $returnarray['status'] = 'updated';
            $returnarray['values'] = $products;

//            $mes = 'Product updated successfully!';
//            return redirect()->back()->with('flash_message', $mes);


        }


        return $returnarray;
    }


    public static function ProductApproveMail($proid)
    {

        $productData = Products::with('productImages')->where('p_id', $proid)->first();

        $userData = User::where('id', $productData->p_vendor_id)->first();


        $data['emailData'] = $productData;
        $data['userData'] = $userData;

//vendor

        try {
            Mail::send('emails_templates.product_approval.approved_product_by_admin_to_vendor', $data, function ($message) use ($productData, $userData) {

                $message->from(self::$adminEmail, 'vdesiconnect.com');
                $message->to($userData->email);
                $message->subject("Congratulations, your product approved successfully!");
            });

        }

        catch (\Exception $e) {
            //display custom message
//            echo $e->errorMessage();
        }



//        dd($contactData);

    }


}
