<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class VendorBusiness extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    public static $adminEmail = 'info@vdesiconnect.com';
    protected $table = 'vendorBusinessDetails';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'vb_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vb_vendor_id',
        'vb_name',
        'vb_tan',
        'vb_gst',
        'vb_address1',
        'vb_address2',
        'vb_state',
        'vb_city',
        'vb_pincode',
        'vb_account_name',
        'vb_account_number',
        'vb_ifsc',
        'vb_bank',
        'vb_branch',
        'vb_bank_state',
        'vb_bank_city',
        'vb_type',
        'vb_pan',
        'vb_address_proof',
        'vb_cheque',
        'vb_status'
    ];

    public static function BusinessDetails($vid)
    {

        $vendorData = VendorBusiness::where('vb_vendor_id', $vid)->first();
        $userData = User::where('id', $vid)->first();


//        AdminEmail

        $data['emailData'] = $vendorData;
        $data['userData'] = $userData;

        try {
            Mail::send('emails_templates.vendor.verify_business_details', $data, function ($message) use ($vendorData, $userData) {

                $message->from(self::$adminEmail, 'vdesiconnect.com');
                $message->to($userData->email);
                $message->subject($userData->name." (vendor) updated Business details, please verify!");
            });
        }

        catch (\Exception $e) {
            //display custom message
//            echo $e->errorMessage();
        }






//        dd($contactData);

    }



}
