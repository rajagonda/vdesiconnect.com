<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'states';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'state_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['state_country_id', 'state_name','state_status'];
}
