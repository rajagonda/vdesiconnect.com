<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Earnings extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'earnings';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'e_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['e_vendor_id', 'e_orders_id', 'e_product_id', 'e_user_id', 'e_product_price', 'e_date'];

}
