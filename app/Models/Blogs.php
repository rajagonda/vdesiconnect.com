<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use File;

class Blogs extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'blogs';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'blog_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['blog_title', 'blog_user_id', 'blog_description', 'blog_image', 'blog_status'];

    public function getUser()
    {
        return $this->belongsTo(User::Class, 'blog_user_id');
    }

    public static function removeBlog($id)
    {
        $file = Blogs::findOrFail($id);

        if ($file->blog_image != '') {
            File::delete('uploads/blogs/' . $file->blog_image);
        }
        Blogs::destroy($id);

        return true;
    }

    public static function imageUpload($inputname, $oldImage = null)
    {
        $uploadPath = public_path('/uploads/blogs/');

        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }


        if ($oldImage != '') {
            if ($oldImage != '') {
                File::delete($uploadPath . $oldImage);
            }

        }


        $extension = $inputname->getClientOriginalName();
        $fileName = time() . $extension;
        $inputname->move($uploadPath, $fileName);

        return $fileName;
    }

}
