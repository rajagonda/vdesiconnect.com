<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class AmmachethivantaOrders extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ammachethivanta_orders';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ac_order_user_id',
        'ac_order_delivery_address',
        'ac_order_delivery_date',
        'ac_order_delivery_time',


        'ac_order_reference_number',
        'ac_order_curency',
        'ac_order_shipping_price',
        'ac_order_price',
        'ac_order_sub_total_price',
        'ac_order_total_price',

        'ac_order_total_weight',
        'ac_order_status',

        'ac_order_note',
        'ac_order_payment_invoice_num',
        'ac_order_payment_transaction_id',
        'ac_order_payment_status',
        'ac_order_payment_mode',
        'ac_order_payment_date',
        'ac_order_payment_response_dump',
    ];

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'ac_order_id';

    public function orderItems()
    {
        return $this->hasMany(AmmachethivantaOrderItems::Class, 'acoitem_order_id', 'ac_order_id');
    }

    public function getUser()
    {
        return $this->belongsTo(User::Class, 'ac_order_user_id', 'id');
    }


    public static function orderPaymentStatus($order_id, $dataUpdate)
    {


//        dump(unserialize($dataUpdate['ac_order_payment_response_dump']));

//        dd($dataUpdate);
        $serviceupdate = AmmachethivantaOrders::findOrFail($order_id);
        $serviceupdate->update($dataUpdate);

        $orders = AmmachethivantaOrders::with('orderItems.getProduct','getUser')->where('ac_order_id', $order_id)->first();


//        dd($orders->orderItems);

//        $userAddress = unserialize($orders->getOrder->order_delivery_address);


//        echo $orders->getUser->email;

//        dd($orders);


        $data['orders'] = $orders;


        try {

            Mail::send('emails_templates.ammaChesthiVanta.user_payment_success', $data, function ($message) use ($orders) {

                $message->from([commonSettings('adminEmail'),commonSettings('adminGmail')], 'vDesiconnect.com');
                $message->to($orders->getUser->email);
                $message->subject($orders->getUser->name . " placed new order (Ammachethivanta) successfully!");
            });


            Mail::send('emails_templates.ammaChesthiVanta.admin_payment_success', $data, function ($message) use ($orders) {

                $message->from($orders->getUser->email, $orders->getUser->name);
                $message->to([commonSettings('adminEmail'),commonSettings('adminGmail')]);
                $message->subject($orders->getUser->name . " placed new order (Ammachethivanta) successfully!");
            });


        }

        catch (\Exception $e) {
            //display custom message
//            echo $e->errorMessage();
        }






        // send email
        // send sms

        return 1;
    }


    public static function changeOrderStatus($request)
    {
        $id = $request['ac_order_id'];

        $ammachethiOrder = AmmachethivantaOrders::with('orderItems')->where('ac_order_id', $id)->first();

        $updateData['ac_order_status'] = $request['ac_order_status'];

        $ammachethiOrder->update($updateData);

        // send email
        // send sms


        return 1;
    }

    public static function createOrder($request)
    {

        $items = \Session::get('acv_selected_records');
        $itemweight = \Session::get('acv_weight_info');

        $order_reference_number = md5(time() . rand());
        $requestData = $request->all();

        $userAddress = UserAddress::with('getCountry')->findOrFail($requestData['address_id']);

        $userAdd = array();
        $userAdd['ua_name'] = $userAddress->ua_name;
        $userAdd['ua_address'] = $userAddress->ua_address;
        $userAdd['ua_landmark'] = $userAddress->ua_landmark;
        $userAdd['ua_city'] = $userAddress->ua_city;
        $userAdd['ua_state'] = $userAddress->ua_state;
        $userAdd['ua_email'] = $userAddress->ua_email;
        $userAdd['ua_phone'] = $userAddress->ua_phone;
        $userAdd['ua_pincode'] = $userAddress->ua_pincode;
        $userAdd['ua_country'] = $userAddress->getCountry->country_name;

        $orders = array();
        $orders['ac_order_user_id'] = Auth::id();
        $orders['ac_order_delivery_address'] = serialize($userAdd);
        $orders['ac_order_status'] = 'PendingPayment';
        $orders['ac_order_total_price'] = $requestData['ac_order_total_price'];
        $orders['ac_order_total_weight'] = $requestData['ac_order_total_weight'];
        $orders['ac_order_curency'] = $requestData['ac_order_curency'];
        $orders['ac_order_sub_total_price'] = str_replace(',', '', $requestData['ac_order_total_price']);
        $orders['ac_order_reference_number'] = $order_reference_number;


//        dd($orders);
        $order_id = self::create($orders)->ac_order_id;


        foreach ($items as $key => $row) {

            $itemInfo = getAmmachethivantaParentInfo($key);
            $orderItems = array();
            $orderItems['acoitem_order_id'] = $order_id;
            $orderItems['acoitem_product_id'] = $key;
            $orderItems['acoitem_weight'] = $itemweight[$key];
//                $orderItems['acoitem_product_price'] = 20;

            AmmachethivantaOrderItems::create($orderItems);
        }

        \Session::forget('acv_selected_records');
        \Session::forget('acv_weight_info');


//        dd($order_id);
        return $order_id;

    }


}
