<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Banners extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'banners';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'banner_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['banner_title', 'banner_bg_image','banner_image', 'banner_description', 'banner_tag', 'banner_status','banner_link'];


    public static function removeBanner($id)
    {
        $file = Banners::findOrFail($id);

        if ($file->banner_image != '') {
            File::delete('uploads/banners/' . $file->banner_image);
        }
        Banners::destroy($id);

        return true;
    }

    public static function imageUpload($inputname, $oldImage = null)
    {
        $uploadPath = public_path('/uploads/banners/');

        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }


        if ($oldImage != '') {
            if ($oldImage != '') {
                File::delete($uploadPath . $oldImage);
            }

        }


        $extension = $inputname->getClientOriginalName();
        $fileName = time() . $extension;
        $inputname->move($uploadPath, $fileName);

        return $fileName;
    }


}
